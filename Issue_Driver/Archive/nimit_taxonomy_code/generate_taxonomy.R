# This script will only generate the taxonomy and add the verb terms also into it
# It can be a standard process that has to be run every time, before we start developing vector representation of PPS projects

# Set the working directory and load the required data and libraries
setwd("~/Downloads/Solve")

library(readxl)
library(dplyr)
library(text2vec)

input_taxo <- read_excel("02222018_Input_taxonomy_1.xlsx", sheet = 'Sheet1')
green_terms <- read_excel("~/Downloads/Solve/iterate_taxonomy/Word_review_1_unassigned_terms.xlsx", sheet = 'Sheet1')

# Now this input taxonomy file needs to go under a few transoformations before it reaches a state where it can be used as a taxonomy
input_taxo <- t(input_taxo)
input_taxo <- tibble::rownames_to_column(as.data.frame(input_taxo), "Bucket_name")
input_taxo <- data.table::melt(data = input_taxo, id.vars = 'Bucket_name')
input_taxo <- input_taxo %>% select(Bucket_name, value) %>%
  na.omit() %>% 
  rename('Terms' = 'value') %>% 
  arrange(Bucket_name) %>% unique()

# Now we need to extract the information from the green_terms file in the same format
df <- green_terms[,1:15]
for(i in 1:nrow(df)){
  for(j in 2:ncol(df)){
    if(!is.na(df[i,j])){
      df[i,j] <- df[i, 'Unassigned Terms']
    }
  }
}

df <- df %>% select(-c(`Unassigned Terms`)) %>%
  mutate(rownumber = 1:nrow(df)) %>%
  data.table::melt(., id.vars = 'rownumber') %>%
  select(-rownumber) %>% rename('Bucket_name' = 'variable','Terms' = 'value') %>%
  na.omit() %>% unique()

input_taxo <- rbind(input_taxo, df) %>% unique()
