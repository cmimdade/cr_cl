## Load in Online Incident Tracker ##
import pyarrow.parquet as pq
import pandas as pd
import os
 
path = '/Users/pr891/Downloads/Solve/pps_extract/'
files = os.listdir(path)
 
parquet_files = []
for file in files:
    if ".parquet" in file:
        parquet_files.append(pq.read_table(path + file).to_pandas())
 
output_file = pd.concat(parquet_files).reset_index(drop = True)

output_file.to_csv('/Users/pr891/Downloads/Solve/pps_extract/20180220_pps_extract.csv', sep = ',')
