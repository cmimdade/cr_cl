import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
#import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
#import mifs
import statsmodels.api as sm
import scipy.stats as stat
from sklearn.preprocessing import PolynomialFeatures
from scipy.sparse import csr_matrix
import seaborn as sns

plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None
#%%
#Load Fat Table
df_ot = pd.read_csv("fat_table_no_one_hot_sample.csv", low_memory = False)
#Load new EGR Info
egr_df = pd.read_csv("egr_cooler_updated_esns.csv", names = ['ESN'])
egr_df = egr_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
#del egr_df['ESN']
#del egr_df['INCDT_ISSUE_NUMBER']
egr_df['Label'] = 1

#egr_df.to_csv('egr.csv', sep = ',')

#%%
#Get non-issues
df_not_issue = df_ot[~df_ot.ESN.isin(egr_df.ESN)]

#df_not_issue = df_ot.query("INCDT_ISSUE_NUMBER == 'not_issue'")
#df_temp = online_tracker.query("INCDT_ISSUE_NUMBER == 'not_issue'")[['ESN', 'REL_CMP_FAIL_CODE_LIST']].reset_index(drop = True)
#none_idx = [df_temp.REL_CMP_FAIL_CODE_LIST[x] is None for x in range(df_temp.shape[0])]
#df_temp = df_temp[none_idx]
#df_not_issue = df_not_issue.query('ESN in @df_temp.ESN')
#df_not_issue = df_not_issue.sample(100)
df_not_issue['Label'] = 0

df_not_issue1=df_not_issue.sample(180, random_state=1234)


#df_new.to_csv("df_new.csv")
#Concatenate issue and non-issue
df_all = pd.concat([egr_df, df_not_issue1]).reset_index(drop = True)
y = df_all['Label']
#del df_all['Label'], df_all['INCDT_ISSUE_NUMBER'], df_all['ESN']


#%%
#import dill 
#import shelve
#filename = 'globalsave.pkl'
#dill.dump_session(filename)

# and to load the session again:
#dill.load_session(filename)
missing_df = egr_df.isnull().sum(axis=0).reset_index()
missing_df.columns = ['column_name', 'missing_count']
#missing_df = missing_df.ix[missing_df['missing_count']>0]
#missing_df = missing_df.sort_values(by='missing_count')

missing_df1 = missing_df.ix[missing_df['missing_count']<59]
#missing_df2 = missing_df.ix[missing_df['missing_count']>=59]
## 223 features have missing value greater than 90%

newcols=missing_df1['column_name']
df_new = df_all[newcols]




#%%

# Missing value Plot

#missing_df = egr_df.isnull().sum(axis=0).reset_index()
#missing_df.columns = ['column_name', 'missing_count']
#missing_df['missing_ratio'] = missing_df['missing_count'] / egr_df.shape[0]
#missing_df.ix[missing_df['missing_ratio']>0.999]
#%matplotlib qt5
#miss=missing_df.ix[missing_df['missing_ratio']>0]
#plt.hist(miss['missing_ratio'])
#plt.xlabel("proportion of missing values")
#plt.ylabel("No. of counts")



#%%

## Distribution of class of features

dtype_df = df_new.dtypes.reset_index()
dtype_df.columns = ["Count", "Column Type"]
#dtype_df
dtype_df.groupby("Column Type").aggregate('count').reset_index()



#%%

#df_not_issue = df_not_issue.query("REL_BUILD_YEAR_MONTH_2017_1 == 1 | REL_BUILD_YEAR_MONTH_2017_2 == 1 | \
##        REL_BUILD_YEAR_MONTH_2017_3 == 1 | REL_BUILD_YEAR_MONTH_2017_4 == 1 | \
#        REL_BUILD_YEAR_MONTH_2017_5 == 1 | REL_BUILD_YEAR_MONTH_2017_6 == 1 | \
#        REL_BUILD_YEAR_MONTH_2017_7 == 1 | REL_BUILD_YEAR_MONTH_2017_8 == 1 | \
#        REL_BUILD_YEAR_MONTH_2017_9 == 1 | REL_BUILD_YEAR_MONTH_2017_10 == 1")

#%%
## missing values Imputation
for i in df_new.columns:
    if df_new[i].dtype == 'object':
      df_new[i] = df_new[i].fillna(df_new[i].mode().iloc[0])
    if (df_new[i].dtype == 'int' or df_new[i].dtype == 'float'):
      df_new[i] = df_new[i].fillna(np.mean(df_new[i]))

numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
df_numeric = df_new.select_dtypes(include=numerics)
#pd.DataFrame(df_numeric.columns[df_numeric.isnull().mean()==0]).T
df_numeric = df_numeric.loc[:,df_numeric.apply(pd.Series.nunique) != 1]

#%%
y = df_numeric['Label']
X = df_numeric.drop(['Label'], axis=1)
X = scale(X.values)

from sklearn.decomposition import PCA
pca = PCA(n_components=175)
X_new = pd.DataFrame(pca.fit_transform(X))

var= pca.explained_variance_ratio_
var1=np.cumsum(np.round(pca.explained_variance_ratio_, decimals=4)*100)

print(var1)
plt.plot(var1)


X = pd.DataFrame(X)
final_lis = []
for i in range(X.shape[1]):
    temp_lis = []
    for j in range(3):
        cor = X[i].corr(X_new[j])
        temp_lis.append(cor)
    final_lis.append(temp_lis)
    
correlations = pd.DataFrame(np.array(final_lis), index=df_numeric.drop(['Label'], axis=1).columns)
