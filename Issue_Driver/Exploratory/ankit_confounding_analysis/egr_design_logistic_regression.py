
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression

raw_data = pd.read_csv('egr_cooler_top_design_vars.csv')
data = raw_data.drop('Label', axis = 1)

dic = {}
for col in data.columns:
    X = data.drop(col, axis = 1)
    y = data[col]
    lr = LogisticRegression(penalty = 'l2').fit(X,y)
    odds = pd.Series(np.exp(lr.coef_)[0], index = X.columns)
    top_vars = list(odds.sort_values(ascending = False).index[:5])
    dic[col] = top_vars

final_df = pd.DataFrame(dic).T
