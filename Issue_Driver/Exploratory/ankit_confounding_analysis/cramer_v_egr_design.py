import numpy as np
import pandas as pd
from scipy.stats import chi2_contingency

def cramers_corrected_stat(confusion_matrix):
    """ calculate Cramers V statistic for categorial-categorial association.
        uses bias correction from Bergsma and Wicher, 
        https://en.wikipedia.org/wiki/Cram%C3%A9r%27s_V
    """
    chi2 = chi2_contingency(confusion_matrix)[0]
    n = confusion_matrix.sum().sum()
    phi2 = chi2/n
    r,k = confusion_matrix.shape
    phi2corr = max(0, phi2 - ((k-1)*(r-1))/(n-1))    
    rcorr = r - ((r-1)**2)/(n-1)
    kcorr = k - ((k-1)**2)/(n-1)
    return np.sqrt(phi2corr / min( (kcorr-1), (rcorr-1)))


raw_data = pd.read_csv('..//Data/egr_cooler_top_design_vars.csv')
data = raw_data.drop('Label', axis = 1)

lis = []
for col1 in data.columns:
    temp_lis = []
    for col2 in data.columns:
        if col1 != col2:
            confusion_matrix = pd.crosstab(data[col1], data[col2])
            cv = cramers_corrected_stat(confusion_matrix)
            temp_lis.append(cv)
        else:
            temp_lis.append(np.nan)
    lis.append(temp_lis)

cramerv_df = pd.DataFrame(np.array(lis), columns = data.columns, index = data.columns)


arr = cramerv_df.values
index_names = cramerv_df.index
col_names = cramerv_df.columns


R,C = np.where(np.triu(arr,1)>0.2)


out_arr = np.column_stack((index_names[R],col_names[C],arr[R,C]))
df_out = pd.DataFrame(out_arr,columns=[['row_name','col_name','value']])
