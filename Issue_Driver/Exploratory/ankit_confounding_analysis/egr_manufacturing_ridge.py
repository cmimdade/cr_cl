import numpy as np
import pandas as pd
from sklearn.linear_model import Ridge
from scipy import stats

raw_data = pd.read_csv('egr_cooler_top_manufacturing_vars.csv')
data = raw_data.drop('Label', axis = 1)
data = data.fillna(0)

dic = {}
for col in data.columns:
    X = data.drop(col, axis = 1)
    y = data[col]
    lm = Ridge()
    lm.fit(X,y)
    params = np.append(lm.intercept_,lm.coef_)
    predictions = lm.predict(X)
    
    newX = pd.DataFrame({"Constant":np.ones(len(X))}).join(pd.DataFrame(X))
    MSE = (sum((y-predictions)**2))/(len(newX)-len(newX.columns))
    
    # Note if you don't want to use a DataFrame replace the two lines above with
    # newX = np.append(np.ones((len(X),1)), X, axis=1)
    # MSE = (sum((y-predictions)**2))/(len(newX)-len(newX[0]))
    
    var_b = MSE*(np.linalg.inv(np.dot(newX.T,newX)).diagonal())
    sd_b = np.sqrt(var_b)
    ts_b = params/ sd_b
    
    p_values =[2*(1-stats.t.cdf(np.abs(i),(len(newX)-1))) for i in ts_b]
    
    sd_b = np.round(sd_b,3)
    ts_b = np.round(ts_b,3)
    p_values = np.round(p_values,3)
    params = np.round(params,4)
    
    myDF3 = pd.DataFrame()
    myDF3["Coefficients"],myDF3["Standard Errors"],myDF3["t values"],myDF3["Probabilites"] = [params,sd_b,ts_b,p_values]
#    print(myDF3)
    
    
    odds = pd.Series(p_values[1:], index = X.columns)
    top_vars = list(odds.sort_values().index[:5])
    dic[col] = top_vars

final_df = pd.DataFrame(dic).T

corr_df = data.corr()
