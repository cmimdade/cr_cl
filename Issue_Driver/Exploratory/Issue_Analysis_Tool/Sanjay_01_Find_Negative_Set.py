#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 14:17:00 2018

@author: sanjay
"""

#import sys
#import pickle
#from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.model_selection import cross_val_score
#from sklearn import feature_selection
#import copy
#from scipy.optimize import fmin
#from scipy.stats import *
from random import sample
#from sklearn.naive_bayes import GaussianNB, BernoulliNB

## Identify Positive (+) set and Randomly Sample Negative (-) Set ##

#Positive Set
issue_num = 'ho284-25600'
df_issue = df_ot.query("INCDT_ISSUE_NUMBER == 'ho284-25600'")
df_issue = df_issue.drop_duplicates(subset = 'ESN').reset_index(drop = True)
df_issue['Label'] = 1

#Only select most recent ESN
df_last_esn = df_ot.groupby('ESN').last().reset_index()

def sampleAndClassify(N):
    
    auc = []
    
    for i in range(N): 
        
        if i % 10 == 0:
            print(i)
        
        #Identify Negative Set
        df_non_issue = df_last_esn[~df_last_esn.ESN.isin(df_issue.ESN)]
        sample_idx = sample(range(df_non_issue.shape[0]), df_issue.shape[0])
        df_non_issue = df_non_issue.iloc[sample_idx,:].reset_index(drop = True)
        df_non_issue['Label'] = 0
        
        #Concatenate issues and non-issues
        df_all = pd.concat([df_issue, df_non_issue])
        y = df_all['Label']
        del df_all['ESN'], df_all['Label'], df_all['INCDT_ISSUE_NUMBER']
        
        #Calculate AUC and Accuracy by Cross-Validation
        
        clf = LogisticRegressionCV(Cs = 100, cv = 5, penalty = 'l1', solver = 'liblinear')
        auc.append(cross_val_score(clf, df_all, y, cv = 5, scoring = 'roc_auc').mean())
    
    return auc

auc = sampleAndClassify(100)

plt.boxplot(auc)
plt.title("Boxplot of AUCs for {} sampled Negative Datasets".format(100))
plt.ylabel("AUC")


## Identify Positive (+) set and Unknown (??) Set ##
    
df_non_issue = df_last_esn[~df_last_esn.ESN.isin(df_issue.ESN)]
df_non_issue['Label'] = 0

## Spy Technique ##

# Hyperparameters
# s - percentage of + examples to put in unlabeled set
# l - percentage of spy's to be below threshold t
s = 0.20
l = .05

#Randomly sample spy observations
spy_idx = sample(range(df_issue.shape[0]), int(df_issue.shape[0]*s))
spy_obs = df_issue.iloc[spy_idx,:]
non_spy_obs = df_issue[~df_issue.index.isin(spy_obs)]

#Add label to identify spy's
spy_obs['Spy'] = 1
non_spy_obs['Spy'] = 0
df_non_issue['Spy'] = 0
df_issue['Spy'] = 0

#Add spy observations to non-issue unlabeled set
df_non_issue = pd.concat([df_non_issue, spy_obs])
df_non_issue['Label'] = 0

#Concatenate everything!
df_all = pd.concat([df_issue, df_non_issue]).reset_index(drop = True)
y = df_all['Label']
spy = df_all['Spy']
del df_all['Label'], df_all['Spy'], df_all['ESN'], df_all['INCDT_ISSUE_NUMBER']

for col in numeric_cols:
    finite_vals = df_all[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        df_all.iloc[finite_vals.index.values, np.where(df_all.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df_all[col] = df_all[col].replace(np.nan, 0)

#Identify indices in unlabeled set that are NOT spys
#Will need to filter on these when identifying negative labels 
no_spy_no_issue = (spy == 0) == (y == 0)
no_spy_no_issue = no_spy_no_issue.index[no_spy_no_issue].values


## Fit Weighted Logistic Regression ##

clf = LogisticRegression(penalty = 'l2', solver = 'saga', class_weight = 'balanced', max_iter = 100, tol = 0.01)
clf.fit(df_all, y)

# Get class probabilities for each observations
probs = clf.predict_proba(df_all)
probs = probs[:,1]

#Sort spy probabilities
sorted_spy_probs = np.sort(probs[np.where(spy == 1)[0]])

#Identify threshold under which we have reliable negatives
def findThreshold(l):
    thresholds = np.arange(0, 1, .001)
    for t in thresholds:
        if np.mean(sorted_spy_probs < t) > l:
            return(t)    
    
t = findThreshold(l)

# Identify Reliable Negative Incidents #

reliable_N = np.where(probs < t)[0]
reliable_N = np.intersect1d(reliable_N, no_spy_no_issue)
df_reliable_N = df_all.iloc[reliable_N, :].reset_index(drop = True)


# Identify potential positive incidents

potential_P = np.where(probs > t)[0]
potential_P = np.intersect1d(potential_P, no_spy_no_issue)
df_potential_P = df_all.iloc[potential_P, :].reset_index(drop = True)







del df_issue['Spy']
del df_issue['INCDT_ISSUE_NUMBER']
del df_issue['ESN']

auc_N = []
auc_P = []
for i in range(100):
    if i % 10 == 0:
        print(i)

    # Negatives
    sample_idx = sample(range(df_reliable_N.shape[0]), df_issue.shape[0])
    df_non_issue = df_reliable_N.iloc[sample_idx,:].reset_index(drop = True)
    df_non_issue['Label'] = 0

    df_all = pd.concat([df_non_issue, df_issue])
    y = df_all['Label']
    del df_all['Label']

    clf = LogisticRegressionCV(Cs = 100, cv = 5, penalty = 'l2', solver = 'liblinear')
    auc_N.append(cross_val_score(clf, df_all, y, cv = 5, scoring = 'roc_auc').mean())
    
    #Positives
    sample_idx = sample(range(df_potential_P.shape[0]), df_issue.shape[0])
    df_non_issue = df_potential_P.iloc[sample_idx,:].reset_index(drop = True)
    df_non_issue['Label'] = 0

    df_all = pd.concat([df_non_issue, df_issue])
    y = df_all['Label']
    del df_all['Label']

    clf = LogisticRegressionCV(Cs = 100, cv = 5, penalty = 'l2', solver = 'liblinear')
    auc_P.append(cross_val_score(clf, df_all, y, cv = 5, scoring = 'roc_auc').mean())

plt.boxplot([auc_N, auc_P])
plt.title("Boxplot of AUCs under {} Sampled Datasets".format(100))
plt.xticks([1,2], ["Reliable Negatives", "Potential Positives"])

plt.boxplot([auc, auc_N])
plt.title("Boxplot of AUCs under {} Sampled Datasets".format(100))
plt.xticks([1,2], ["Unlabeled Negatives", "Reliable Negatives"])













