#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 10:29:51 2018

@author: sanjay
"""

from sklearn.preprocessing import scale
from sklearn import feature_selection
import mifs
import seaborn as sns
from sklearn.linear_model import LogisticRegressionCV
from tqdm import tqdm


## Get EGR Cooler ESNs ##

# Load and merge with features
def loadAndMerge():
    egr_df = pd.read_csv("Desktop/ds/Solve/egr_cooler_labels.csv")
    egr_df = egr_df.rename(columns = {"Engine Serial Number": "ESN"})
    egr_df = egr_df.query("Population != 'X1'") #Remove X1
    del egr_df['Population']
    egr_df = egr_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
    del egr_df['ESN']
    y = egr_df.Label.astype(int)
    #del egr_df['Label']
    del egr_df['INCDT_ISSUE_NUMBER']
    return egr_df, y

egr_df, y = loadAndMerge()

def scaleNumeric(df):
    #Scale numeric features
    for col in numeric_cols:
        finite_vals = df[col].dropna()
        if len(finite_vals) > 0:
            scaled_vals = scale(finite_vals)
            df.iloc[finite_vals.index.values, np.where(df.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df[col] = df[col].replace(np.nan, 0)
    
    return df

for col in numeric_cols:
    finite_vals = egr_df[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        egr_df.iloc[finite_vals.index.values, np.where(egr_df.columns.values == col)[0][0]] = scaled_vals

#Replace NaN with 0's (Mean)
    egr_df[col] = egr_df[col].replace(np.nan, 0)

#Scale numeric features
egr_df = scaleNumeric(egr_df)

## Use MIC to rank variables ##
    
N = 15 #top N variables 

#Mask of categorical columns
cat_cols_mask = [egr_df.columns.values[x] not in numeric_cols for x in range(egr_df.shape[1])]

#Simple mutual information for categorical and numerical features
mic = feature_selection.mutual_info_classif(egr_df, y, discrete_features = cat_cols_mask)

#Extract top N features
top_vars = egr_df.columns.values[np.argsort(mic)[::-1]][:N]

#Load in egr data without normalization
egr_df = loadAndMerge()





#Look at overlapping histograms of top variables
for var in top_vars:
    fig, ax = plt.subplots(figsize=(12, 8))
    ax.hist(egr_df[y == 1][var].dropna(), color='lightblue', alpha=0.7, bins = 10, label = 'Target', normed = True)
    ax.hist(egr_df[y == 0][var].dropna(), color='salmon', alpha=0.7, bins = 10, label = 'Clean', normed = True)
    ax.set(title='Histogram of {} by Class'.format(var), ylabel='Count', xlabel = 'Time')
    plt.legend()
    plt.show()


## Use Joint Mutual Information (JMI) to rank variables

N = 15 #top N variables

#JMI for cat and numerical variables
feat = mifs.MutualInformationFeatureSelector(n_features = N, verbose = 2)
feat.fit(egr_df, y)

#Top variables (that are not NaN)
top_vars = egr_df.columns.values[feat.ranking_][np.isfinite(feat.mi_)]

## Get Sensor Issue ESNs ##

#Positive Set
issue_num = 'ho284-25600'
df_issue = df_ot.query("INCDT_ISSUE_NUMBER == 'ho284-25600'")
df_issue = df_issue.drop_duplicates(subset = 'ESN').reset_index(drop = True)
df_issue['Label'] = 1
del df_issue['INCDT_ISSUE_NUMBER'], df_issue['ESN']

#All Potential Negatives
df_realiable_N = df_ot.iloc[reliable_N,:]
  
#Dictionary for each column value:
col_coefs = dict()
for col in df_issue.columns.values:
    col_coefs[col] = []

for i in tqdm(range(100)):

    # Negatives
    sample_idx = sample(range(df_reliable_N.shape[0]), df_issue.shape[0])
    df_non_issue = df_reliable_N.iloc[sample_idx,:].reset_index(drop = True)
    df_non_issue['Label'] = 0
    
    #Concatenate everything
    df_all = pd.concat([df_issue, df_non_issue])
    
    #Scale and replace NAs with numeric
    df_all = scaleNumeric(df_all)
    y = df_all.Label
    del df_all['Label']
    
    #Run Logistic Regression Cross Validation
    clf = LogisticRegressionCV(penalty = 'l2', solver = 'lbfgs')
    
    #Univariate regressions for each variable
    for col in df_all.columns.values:
        clf.fit(df_all[[col]], y)
        col_coefs[col].append(clf.coef_[0][0])

d_ = []
for d in col_coefs:
    if len(col_coefs[d]) != 100:
        d_.append(d)

for d in d_:
    col_coefs.pop(d)

coef_vals = pd.DataFrame(col_coefs)


coef_val_quants = coef_vals.quantile([0.025, 0.975])
sig_cols = []
for col in coef_val_quants.columns.values:
    if coef_val_quants[col][0.025] > 0 and coef_val_quants[col][0.975] > 0:
        sig_cols.append(col)
    elif coef_val_quants[col][0.025] < 0 and coef_val_quants[col][0.975] < 0:
            sig_cols.append(col)

manual_cols = ['MES_CMP_AVG_TIME_PROD_MIN', 'MES_CMP_TOTAL_TIME_FINISH_MIN', 'MES_CMP_AVG_TIME_FINISH_MIN', \
               'MES_CMP_TOTAL_TIME_PROD_MIN']
coef_vals = coef_vals[sig_cols]
for i in np.arange(0, 100, 10):
    coef_vals.iloc[:,i:(i+10)].boxplot()
    plt.xticks(rotation=90)
    plt.show()

coef_vals[manual_cols].boxplot()
plt.xticks([1,2,3,4], ['Avg Time in Prod','Total Time in Finish','Avg Time in Finish','Total Time in Prod'], fontsize = 11.3)
plt.ylabel("Coefficient Interval", fontsize = 11.3)
#plt.xlabel("ESN Feature", fontsize = 11)
plt.title("Confidence Intervals for Univariate Regressions", fontsize = 11.3)

## Boxplots and Histograms ##
egr_df = egr_df.groupby('ESN').last()
y = egr_df.Label.astype(int)

cols = ['MES_CMP_TOTAL_TIME_FINISH_MIN', 'MES_CMP_TOTAL_TIME_PROD_MIN', \
        'MES_CMP_AVG_TIME_PROD_MIN', 'MES_CMP_TOTAL_TIME_HOLD_MIN']

def boxAndHist(col_val):

    plt.boxplot([egr_df.query("Label == 1")[col_val].dropna(),\
                 egr_df.query("Label == 0")[col_val].dropna()])
        
    fig, ax = plt.subplots(figsize=(12, 8))
    ax.hist(egr_df.query("Label == 0")[col_val].dropna(), color='lightblue', alpha=0.7, bins = 10, label = 'Clean', normed = True)
    ax.hist(egr_df.query("Label == 1")[col_val].dropna(), color='salmon', alpha=0.7, bins = 10, label = 'Target', normed = True)
    ax.set(title='Histogram of {} by Class'.format(col_val), ylabel='Count', xlabel = 'Time')
    plt.legend()
    plt.show()
    
    print("{} : {}".format("Mean of Target", egr_df.query("Label == 1")[col_val].dropna().mean()))
    print("{} : {}".format("Median of Target", egr_df.query("Label == 1")[col_val].dropna().median()))
    print("{} : {}".format("Mean of Clean", egr_df.query("Label == 0")[col_val].dropna().mean()))
    print("{} : {}".format("Median of Clean", egr_df.query("Label == 0")[col_val].dropna().median()))

for col in cols:
    boxAndHist(col)
    
boxAndHist('REL_LAGD')
    
#Step 3 and Step 6
date = '19-Dec-2016'
online_tracker.colum
