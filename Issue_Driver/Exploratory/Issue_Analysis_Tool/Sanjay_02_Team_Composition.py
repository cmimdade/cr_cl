# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt

plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

from sklearn.linear_model import LinearRegression
from scipy.sparse import csr_matrix
from statsmodels.regression import linear_model
from sklearn.preprocessing import scale

df = pd.read_csv("~/Documents/OneDrive - Cummins/Solve Use Case/00 Data/20180111 PPS Cycle Time Filtered v03.csv", encoding='latin-1')

step_cols = ['Failure Recognition Date', 'First Customer Failure Date (Tc)', 'PDF Approval Date (T1)', 'PTF Launch Date (TL)', 'Preliminary Step 2', 'Step 2 Completion Date (T2)', 'Preliminary Step 3', 'Step 3 Completion', 'Preliminary Step 4', 'Step 4 Completion(T4)', 'Preliminary Step 5', 'Step 5 Completion', 'Preliminary Step 6', 'Max Step 6 Value']
header_cols = ['PPS Problem #', 'Entities']

df = df.drop_duplicates(subset = 'PPS Problem #')
step_cols = ['Failure Recognition Date', 'First Customer Failure Date (Tc)', 'PDF Approval Date (T1)', 'PTF Launch Date (TL)', 'Preliminary Step 2', 'Step 2 Completion Date (T2)', 'Preliminary Step 3', 'Step 3 Completion', 'Preliminary Step 4', 'Step 4 Completion(T4)', 'Preliminary Step 5', 'Step 5 Completion', 'Preliminary Step 6', 'Max Step 6 Value']
header_cols = ['PPS Problem #', 'Entities', 'Project Lead']
df_times = df[header_cols + step_cols].reset_index(drop = True)
df_times.loc[df_times['Max Step 6 Value'] == '00-Jan-1900', 'Max Step 6 Value'] = float('NaN')
df_times.iloc[1078, 7] = '30-Apr-2012'
df_times.iloc[1748, 15] = '25-Sep-2012'
df_times.iloc[1748, 16] = '25-Sep-2012'    

for col in step_cols:
    df_times[col] = pd.to_datetime(df_times[col])
    
def calc_days(x, val):
    if x.days <=  val:
        return float('NaN')
    else:
        return x.days
    
df_times['TC-T0'] = (df_times['First Customer Failure Date (Tc)'] - df_times['Failure Recognition Date']).apply(calc_days, val = 0)
df_times['T0-T1'] = (df_times['Failure Recognition Date'] - df_times['PDF Approval Date (T1)']).apply(calc_days, val = 0)
df_times['T1-TL'] = (df_times['PDF Approval Date (T1)'] - df_times['PTF Launch Date (TL)']).apply(calc_days, val = 0)
df_times['TL-T2'] = (df_times['Preliminary Step 2'] - df_times['PTF Launch Date (TL)']).apply(calc_days, val = 0)
df_times['T2-T3'] = (df_times['Step 2 Completion Date (T2)'] - df_times['Preliminary Step 2']).apply(calc_days,  val = 0)
df_times['T3-T4'] = (df_times['Step 3 Completion'] - df_times['Preliminary Step 3']).apply(calc_days,  val = 0)
df_times['T4-T5'] = (df_times['Step 4 Completion(T4)'] - df_times['Preliminary Step 4']).apply(calc_days,  val = 0)
df_times['T5-T6'] = (df_times['Step 5 Completion'] - df_times['Preliminary Step 5']).apply(calc_days,  val = 0)
df_times['T6-T7'] = (df_times['Max Step 6 Value'] - df_times['Preliminary Step 6']).apply(calc_days,  val = 0)
df_times['TL-T4'] = (df_times['Step 4 Completion(T4)'] - df_times['PTF Launch Date (TL)']).apply(calc_days,  val = 0)

## Team Composition ##

num_ppl_on_project = df_times['Project Lead'].dropna().value_counts().value_counts()

plt.bar(np.arange(len(num_ppl_on_project)), num_ppl_on_project)
plt.xlabel("Total Number of Projects Per Person")
plt.ylabel("# of Project Managers")
plt.title("Histogram of Total # of Projects by Project Lead")
          
# Best/Worst People
          
num_projects_per_person = df_times['Project Lead'].dropna().value_counts()
unique_nums = np.unique(num_projects_per_person)


vals = []
for num in unique_nums:
    df_sub = df_times[df_times['Project Lead'].isin(num_projects_per_person[num_projects_per_person == num].index)]
    vals.append(df_sub['TL-T4'].dropna())
    
    print("{} Project, Mean {}, Median {}, N = {}".format(num, df_sub['TL-T4'].dropna().mean(), df_sub['TL-T4'].dropna().median(),num_projects_per_person[num_projects_per_person == num].shape[0]))
    


plt.boxplot(vals)

num_projects_per_person[num_projects_per_person == 16].shape