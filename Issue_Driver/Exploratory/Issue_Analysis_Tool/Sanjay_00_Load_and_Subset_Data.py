#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 14:01:46 2018

@author: sanjay
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
import os
import pickle
from tqdm import tqdm


plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

#df_oepl['option_group'] = df_oepl['option_assembly_num'].apply(lambda x: x[:2])

#df_oepl[['engine_serial_num', 'option_group']].groupby('option_group').count()
#df_oepl.engine_serial_num = df_oepl.engine_serial_num.astype(int)
#df_oepl_sub = df_oepl.query("engine_serial_num in @df_all.ESN.unique()")


#path to .parquet
path = 'Desktop/Data/QUALITY_FEATURES_PARQUET_NEW/'

online_tracker = pq.read_table(path).to_pandas()
online_tracker['ESN'] = online_tracker['ESN'].astype(int)

## LOAD IN FAULT SNAPSHOT ##

path = 'Desktop/Data/INS_FAULT_SNAPSHOT_MDA_FEATURES/'
fault_snapshot = pq.read_table(path).to_pandas()
fault_snapshot.ESN = fault_snapshot.ESN.astype(int)

del fault_snapshot['GREEN_WRENCH_FAULT_CODE_LIST']
## LOAD IN OPTIONS ##

#Option Description File

oepl_desc_df = pd.read_csv("Desktop/Data/ISX_Options.csv")
oepl_desc_df['OPT_ITEM_DESC_NEW'] = [oepl_desc_df['OPT_ITEM_DESC'][x].replace(",", "_") for x in range(oepl_desc_df.shape[0])]
oepl_desc_df['OPT_ITEM_DESC_NEW'] = [oepl_desc_df['OPT_ITEM_DESC_NEW'][x].replace(" ", "_") for x in range(oepl_desc_df.shape[0])]
oepl_desc_df['OPT_ITEM_DESC_NEW'] = ['OEPL_' + oepl_desc_df['OPT_ITEM_DESC_NEW'][x] for x in range(oepl_desc_df.shape[0])]
oepl_desc_df['OPT_ITEM_DESC_NEW'] = [oepl_desc_df['OPT_ITEM_DESC_NEW'][x] + ':' + oepl_desc_df['OPT_NBR'][x] for x in range(oepl_desc_df.shape[0])]

#Options for ESNs

## LOAD IN ONLINE INCIDENT TRACKER (DETECT MASTER TABLE) ##
path = 'Desktop/Data/RLD_ENGINE_OPTION_ASSEMBLY_x15'
df = pq.read_table(path).to_pandas()
df['engine_serial_num'] = df['engine_serial_num'].astype(int)

df = df.rename(columns = {'option_assembly_num': 'OPT_NBR'})
df = df.merge(oepl_desc_df, on = 'OPT_NBR', how = 'left')
df['option_count'] = 1

opt_val_type = df['shop_order_num'].apply(lambda x: type(x))
opt_str_idx = opt_val_type[opt_val_type == str].index
df = df.loc[opt_str_idx]

df_pivot = df.pivot_table(index = 'engine_serial_num', columns = 'OPT_ITEM_DESC_NEW', \
                          values = 'option_count', aggfunc='mean').fillna(0).reset_index()
df_pivot = df_pivot.rename(columns = {'engine_serial_num': 'ESN'})
#df_oepl_long = df[['engine_serial_num', 'OPT_NBR']]
#df_oepl_long = df_oepl_long.rename(columns = {'engine_serial_num': 'ESN'})
'''
#Create empty dataframe
fault_snap_new = pd.DataFrame(index = range(fault_snapshot.shape[0]), columns = fault_snapshot.columns.values)
fault_snap_new['ESN'] = fault_snapshot.ESN
fault_snap_new['EARLIEST_INDICATION_DATE'] = fault_snapshot.EARLIEST_INDICATION_DATE
fault_snap_new['INS_FS_FIRST_ECM_IMAGE_DATE_TIME'] = fault_snapshot.INS_FS_FIRST_ECM_IMAGE_DATE_TIME
fault_snap_new['INS_FS_FIRST_ECM_IMAGE_DATE'] = fault_snapshot.INS_FS_FIRST_ECM_IMAGE_DATE
del fault_snapshot['INS_FS_FIRST_ECM_IMAGE_DATE_TIME']
del fault_snapshot['INS_FS_FIRST_ECM_IMAGE_DATE']

#Extract most recent fault code value
#Note this will take ~10 mins or so
for col in tqdm(fault_snapshot.columns[3:]):
    ins = fault_snapshot[col]
    
    #If the observation is completely blank, set to NaN
    #If the observation only has fault codes and no parameters, set to NaN
    #Otherwise, take the most recent (last) fault code and extract the parameter value
    #Is this even faster...?
    fault_snap_new[col] = [ast.literal_eval(ins[x][len(ins[x]) - 1])['PARAMETER_VALUE'] \
                  if len(ins[x]) > 0 and len(ast.literal_eval(ins[x][len(ins[x]) - 1])) > 1 \
                  else float('NaN') for x in range(ins.shape[0])]
    
    
#Save to same path as .parquet files
fault_snap_new.to_csv('Desktop/fault_snapshot_most_recent_val.csv', index = False)
fault_snap_new = pd.read_csv('Desktop/fault_snapshot_most_recent_val.csv')
fault_snap_new['ESN'] = fault_snap_new.ESN.astype(int)
'''

fat_table = online_tracker.merge(fault_snapshot, how = 'left', on = ['ESN', 'EARLIEST_INDICATION_DATE'])


#Get first val of fault snapshot to decide cat or numeric
'''
type_of_val = pd.DataFrame(index = fat_table.caaa
    array_of_val = np.array(fat_table[col].dropna())
    if len(array_of_val) > 0:
        type_of_val['val'][col] = np.array(fat_table[col].dropna())[0]
'''

# Add build month to table #
fat_table['REL_BUILD_MONTH'] = ['0' + str(fat_table['REL_BUILD_MONTH'][x]) \
                                 if len(str(fat_table['REL_BUILD_MONTH'][x])) == 1 else \
                                str(fat_table['REL_BUILD_MONTH'][x]) for x in range(fat_table.shape[0])]

fat_table['REL_BUILD_YEAR_MONTH'] = fat_table['REL_BUILD_YEAR'].astype(str) + \
                        '_' + fat_table['REL_BUILD_MONTH'].astype(str)
                        
# Add computed miles table
                        
miles_temp = [fat_table['INS_TI_ENGINE_DISTANCE_MILES'][x] if ~np.isnan(fat_table['INS_TI_ENGINE_DISTANCE_MILES'][x]) \
         else fat_table['REL_CMP_ENGINE_MILES_LIST'][x] for x in range(fat_table.shape[0])]

miles_temp = [miles_temp[x][len(miles_temp[x])-1] if type(miles_temp[x]) is np.ndarray else miles_temp[x] for x in range(len(miles_temp))]

fat_table['INS_TI_ENGINE_DISTANCE_MILES'] = miles_temp
            
# Merge with OEPL Dataset above            
fat_table = fat_table.merge(df_pivot, on = 'ESN', how = 'left')
                        
## Get idle time percentage
fat_table['INS_TI_IDLE_TIME_PERCENTAGE'] = 100*fat_table['INS_TI_IDLE_TIME_HOURS']/fat_table['INS_TI_ECM_TIME_KEY_ON_TIME_HOURS']

##Clean up #

fat_table = fat_table.rename(columns = {'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3/S': \
                                        'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3S', \
                                        'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3/S': \
                                        'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3S'})
    
#VPCR

## Extract Relevant Features ##

esn_col = ['ESN']
cat_cols = ['REL_CMP_PROGRAM_GROUP_NAME', \
            'REL_BUILT_ON_WEEKEND', \
            'REL_ANALYSIS_RATE_CAT', 'REL_CPL_NUM', 'REL_DESIGN_HSP_NUM', 'REL_DESIGN_RPM_NUM', \
            'REL_ECM_PART_NO','REL_MKTG_HSP_NUM', 'REL_MKTG_RPM_NUM', 'REL_OEM_TYPE', 'REL_OEM_GROUP', 'REL_BUILD_YEAR_MONTH', \
            'REL_USER_APPL_DESC', 'REL_EMISSIONS_FAMILY_CODE', \
            'INS_FS_AFTERTREATMENT_FUEL_INJECTOR_STATUS', \
            'INS_FS_AFTERTREATMENT_FUEL_SHUTOFF_VALVE_STATUS', \
            'INS_FS_AIR_CONDITIONING_PRESSURE_SWITCH', \
            'INS_FS_BRAKE_PEDAL_POSITION_SWITCH', \
            'INS_FS_CLUTCH_PEDAL_POSITION_SWITCH', \
            'INS_FS_CRANKCASE_VENTILATION_HEATER_COMMANDED_STATE', \
            'INS_FS_CRUISE_CONTROL_ON_OFF_SWITCH', \
            'INS_FS_EGR_FLOW_DERATE', \
            'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_2', 
            'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_3',
            'INS_FS_ENGINE_COOLANT_LEVEL', \
            'INS_FS_ENGINE_OPERATING_STATE', \
            'INS_FS_ENGINE_PROTECTION_DERATE_SUPPRESS', \
            'INS_FS_ENGINE_PROTECTION_SHUTDOWN_OVERRIDE_SWITCH', \
            'INS_FS_ENGINE_SPEED_MAIN_SENSOR_SIGNAL_STATE', \
            'INS_FS_ENGINE_SPEED_MAIN_SYNCHRONIZATION_STATE', \
            'INS_FS_ENGINE_SPEED_STATUS', \
            'INS_FS_ENGINE_TORQUE_MODE', \
            'INS_FS_FAN_CONTROL_SWITCH', \
            'INS_FS_FAN_DRIVE_STATE', \
            'INS_FS_KEYSWITCH', \
            'INS_FS_PTO_ON_OFF_SWITCH', \
            'INS_FS_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_SWITCH', \
            'INS_FS_REMOTE_PTO_SWITCH', \
            'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_DERATE', \
            'INS_FS_TURBOCHARGER_SPEED_DERATE_ACTIVE', \
            'INS_FS_WATER_IN_FUEL_STATE', 'INS_FS_AFTERTREATMENT_PURGE_AIR_SOLENOID_STATUS', \
            'INS_FNP_GEAR_DOWN_PROTECTION_NA', \
            'INS_FNP_ADAPTIVE_CRUISE_NA', \
            'INS_FNP_CRUISE_CONTROL_NA', \
            'INS_FNP_ROAD_SPEED_GOVERNOR_NA', \
            'INS_FNP_IDLE_SHUTDOWN_NA', \
            'INS_FNP_REAR_AXLE_RATIO_NA', \
            'INS_FNP_TRANSMISSION_TYPE_NA', \
            'INS_FNP_ADJUSTABLE_LOW_IDLE_SPEED_NA', \
            'INS_FNP_GEAR_DOWN_TRANSMISSION_RATIO_NA', \
            'INS_FNP_TOP_GEAR_TRANSMISSION_RATIO_NA', \
            'INS_FNP_ACTIVE_REGENERATION_IN_PTO_AND_REMOTE_MODES_NA', \
            'INS_FNP_AUTOMOTIVE_MOBILE_REGENERATION_NA', \
            'INS_FNP_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_TEMPERATURE_STABILIZATION_NA', \
            'INS_FNP_LOAD_BASED_SPEED_CONTROL_NA', \
            'INS_FNP_SMARTACCEL_NA', \
            'INS_FNP_CRUISE_CONTROL_TYPE_NA', \
            'INS_FNP_FUEL_ECONOMY_ADJUSTMENT_FACTOR_NA', \
            'INS_FNP_LOAD_BASED_TORQUE_CONTROL_NA', \
            'INS_FNP_SMART_COAST_NA', \
            'INS_FNP_TORQUE_CONTROL_NA', \
            'INS_FNP_GEAR_RATIO_THRESHOLD_NA', \
            'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_KPA']

oepl_cols = df['OPT_ITEM_DESC_NEW'].unique()

## Add Green Wrench in later

numeric_cols = ['REL_CMP_SUM_NET_AMOUNT', \
                'REL_CMP_SUM_MATERIALS_AMOUNT', \
                'REL_CMP_SUM_MARKUP_AMOUNT', \
                'REL_CMP_SUM_REPAIR_LABOR_AMOUNT', \
                'REL_CMP_SUM_OTHER_EXPENSE_AMOUNT', \
                'REL_CMP_SUM_DEDUCTIBLE_AMOUNT', \
                'REL_LAGD', 'REL_LAGS','INS_EA_CRANKCASE_PRSR_SEC_SEV_1', 'INS_EA_COOLANT_TMP_SEC_SEV_1', \
                'INS_EA_OIL_PRSR_SEC_SEV_1', 'INS_EA_OIL_TMP_SEC_SEV_1', 'INS_EA_SPEED_SEC_SEV_1', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_1', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_2', 'INS_EA_COOLANT_TMP_SEC_SEV_2', 'INS_EA_OIL_PRSR_SEC_SEV_2', \
                'INS_EA_OIL_TMP_SEC_SEV_2', 'INS_EA_SPEED_SEC_SEV_2', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_2', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_3', 'INS_EA_COOLANT_TMP_SEC_SEV_3', 'INS_EA_OIL_PRSR_SEC_SEV_3', \
                'INS_EA_OIL_TMP_SEC_SEV_3', 'INS_EA_SPEED_SEC_SEV_3', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_3', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_TOTAL', 'INS_EA_COOLANT_TMP_SEC_SEV_TOTAL', 'INS_EA_OIL_PRSR_SEC_SEV_TOTAL', \
                'INS_EA_OIL_TMP_SEC_SEV_TOTAL', 'INS_EA_SPEED_SEC_SEV_TOTAL', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_TOTAL', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_TIME', 'INS_DC_CMP_PERCENT_ZERO_OR_NEGATIVE_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_LOW_TORQUE_TIME', 'INS_DC_CMP_PERCENT_LIGHT_LOAD_TIME', \
                'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', 'INS_DC_CMP_PERCENT_CLIMBING_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME', 'INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME', 'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME', \
                'INS_TI_AVERAGE_ENGINE_LOAD_PERCENT', 'INS_TI_AVERAGE_ENGINE_SPEED_RPM', 'INS_TI_AVERAGE_VEHICLE_SPEED_MILES', \
                'INS_TI_IDLE_TIME_PERCENTAGE', 'INS_TI_ENGINE_BRAKE_DISTANCE_MILES', \
                'INS_TI_ENGINE_BRAKE_TIME_HOURS', 'INS_TI_ENGINE_DISTANCE_MILES', 'INS_TI_ENGINE_RUN_TIME_HOURS', \
                'INS_TI_FUEL_USED_GALLON', 'INS_TI_FULL_LOAD_OPERATION_TIME_HOURS', 'INS_TI_IDLE_FUEL_USED_GALLON', \
                'INS_TI_ECM_TIME_KEY_ON_TIME_HOURS', 'INS_TI_IDLE_TIME_HOURS', \
                'INS_FNP_CRUISE_CONTROL_LOWER_DROOP_MPH', \
                'INS_FNP_CRUISE_CONTROL_UPPER_DROOP_MPH', \
                'INS_FNP_GEAR_DOWN_MAXIMUM_VEHICLE_SPEED_HEAVY_ENGINE_LOAD_MPH', \
                'INS_FNP_GEAR_DOWN_MAXIMUM_VEHICLE_SPEED_LIGHT_ENGINE_LOAD_MPH', \
                'INS_FNP_ROAD_SPEED_GOVERNOR_LOWER_DROOP_MPH', \
                'INS_FNP_ROAD_SPEED_GOVERNOR_UPPER_DROOP_MPH', \
                'INS_FNP_LOW_IDLE_SPEED_RPM', \
                'INS_FNP_TIME_BEFORE_SHUTDOWN_HOURS', \
                'INS_FNP_MAXIMUM_ACCELERATOR_VEHICLE_SPEED_MPH', \
                'INS_FNP_MAXIMUM_CRUISE_CONTROL_SPEED_MPH', \
                'INS_FNP_TIRE_SIZE_REVSPERMILE', \
                'INS_FNP_RPM_BREAKPOINT_RPM', \
                'INS_FNP_TORQUE_RAMP_RATE_HP', \
                'EPAT_TEST_CNT_FAIL', \
                'EPAT_TEST_CNT_PASS', \
                'EPAT_TEST_CNT_INCOMPLETE', \
                'EPAT_TEST_CNT_MATERIAL_REVIEW', \
                'EPAT_TEST_CNT_OF_TEST', \
                'EPAT_TEST_SUM_ENGINE_CELL_TIME', \
                'EPAT_TEST_CNT_SHIFT_CODE', \
                'EPAT_TEST_CNT_ABOVE_LIMIT', \
                'EPAT_TEST_CNT_BELOW_LIMIT', \
                'EPAT_TEST_MAX_BELOW_LIMIT_PERCENT', \
                'EPAT_TEST_MAX_ABOVE_LIMIT_PERCENT', \
                'EPAT_TEST_FAILED_CATEGORY_ASSEMBLY', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_M', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_M_LEAK', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_M_REP', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X_ETS', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X_LEAK', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X_REP', \
                'EPAT_TEST_FAILED_CATEGORY_ETS_STAND', \
                'EPAT_TEST_FAILED_CATEGORY_FPM', \
                'EPAT_TEST_FAILED_CATEGORY_FPM_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_FUEL_LEAK_CH', \
                'EPAT_TEST_FAILED_CATEGORY_HOT_TEST', \
                'EPAT_TEST_FAILED_CATEGORY_ISX_OFFLINE', \
                'EPAT_TEST_FAILED_CATEGORY_MANTA_STAND', \
                'EPAT_TEST_FAILED_CATEGORY_PROGRAMMING', \
                'EPAT_TEST_FAILED_CATEGORY_QR_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_REPAIR_3', \
                'EPAT_TEST_FAILED_CATEGORY_REWORK', \
                'EPAT_TEST_FAILED_CATEGORY_TEST_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_UPFIT', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_AUT', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_CNV', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_ETS', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_SPC', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_SQC', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_STD', \
                'MES_TSH_CMP_NUM_OF_STATES_TRANSITION', \
               'MES_TSH_CMP_AVG_TIME_CANCEL_MIN', \
               'MES_TSH_CMP_AVG_TIME_FINISH_MIN', 'MES_TSH_CMP_AVG_TIME_HOLD_MIN', \
               'MES_TSH_CMP_AVG_TIME_PROD_MIN', 'MES_TSH_CMP_AVG_TIME_PURGED_MIN', \
               'MES_TSH_CMP_AVG_TIME_REPAIR_MIN', 'MES_TSH_CMP_AVG_TIME_SCHED_MIN', \
               'MES_TSH_CMP_AVG_TIME_SHIP_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_CANCEL_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_FINISH_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_HOLD_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_PROD_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_PURGED_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_REPAIR_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_SCHED_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_SHIP_MIN',  \
               'MES_ES_CMP_FINISHED_COUNT', \
               'MES_ES_CMP_LINESET_COUNT', 'MES_ES_CMP_SHIPPED_COUNT', \
               'MES_DA_CMP_CNTSTATION_BY_ESN', 'MES_DA_CMP_DURATION_ESN_MIN', \
               'MES_DA_CMP_TPLOC_AS_MIN', \
               'MES_DA_CMP_TPLOC_FP_MIN', 'MES_DA_CMP_TPLOC_HD_MIN', \
               'MES_DA_CMP_TPLOC_SB_MIN', 'MES_DA_CMP_TPLOC_TS_MIN', \
               'MES_DEV_COUNT_CMP', \
               'MES_DEV_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_AVG_DURATION_CMP', \
               'MES_DEV_TOTAL_DURATION_CMP', \
               'MES_DEV_MAX_DURATION_CMP', \
               'MES_DEV_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_ADD_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_ADD_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_COUNT_CMP', \
               'MES_DEV_ACTION_OMIT_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_OMIT_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_OMIT_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_COUNT_CMP', \
               'MES_DEV_ACTION_QTY_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_QTY_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_QTY_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_COUNT_CMP', \
               'MES_DEV_ACTION_REPL_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_REPL_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_REPL_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_ACTION_OMIT_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_ACTION_REPL_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_ACTION_QTY_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_NUM_DISTINCT_PARTS_CMP', \
               'INS_FS_ACCELERATOR_PEDAL_OR_LEVER_POSITION_SENSOR_SUPPLY_VOLTAGE_V', \
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_BAR',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_PSI',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_QUALITY',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_QUALITY_PERCENT',\
               'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE_CELCIUS',\
               'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE_FAHRENHEIT', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_INHG', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_KPA', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_PSI', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_INHG', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_KPA', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_PSI', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE_CELCIUS', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE_FAHRENHEIT', \
               'INS_FS_AFTERTREATMENT_FUEL_PRESSURE', \
               'INS_FS_AFTERTREATMENT_FUEL_PRESSURE_KPA', \
               'INS_FS_AFTERTREATMENT_FUEL_PRESSURE_PSI',\
               'INS_FS_AFTERTREATMENT_INTAKE_NOX_CORRECTED', \
               'INS_FS_AFTERTREATMENT_INTAKE_NOX_CORRECTED_PPM',\
               'INS_FS_AFTERTREATMENT_OUTLET_NOX_CORRECTED',\
               'INS_FS_AFTERTREATMENT_OUTLET_NOX_CORRECTED_PPM',\
               'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE',\
               'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE_CELCIUS',\
               'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE_FAHRENHEIT',\
               'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE',\
               'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE_CELCIUS',\
               'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE_FAHRENHEIT',\
               'INS_FS_BAROMETRIC_AIR_PRESSURE',\
               'INS_FS_BAROMETRIC_AIR_PRESSURE_INHG',                                 
               'INS_FS_BAROMETRIC_AIR_PRESSURE_KPA',                                           
               'INS_FS_BAROMETRIC_AIR_PRESSURE_PSI',\
               'INS_FS_BATTERY_VOLTAGE',\
               'INS_FS_BATTERY_VOLTAGE_V', \
               'INS_FS_CRANKCASE_PRESSURE', \
               'INS_FS_CRANKCASE_PRESSURE_INH2O', \
               'INS_FS_CRANKCASE_PRESSURE_KPA', \
               'INS_FS_EGR_DIFFERENTIAL_PRESSURE', \
                'INS_FS_EGR_DIFFERENTIAL_PRESSURE_INHG', \
                'INS_FS_EGR_DIFFERENTIAL_PRESSURE_KPA', \
                'INS_FS_EGR_DIFFERENTIAL_PRESSURE_PSI',\
                'INS_FS_EGR_ORIFICE_PRESSURE',\
                'INS_FS_EGR_ORIFICE_PRESSURE_INHG',\
                'INS_FS_EGR_ORIFICE_PRESSURE_KPA',\
                'INS_FS_EGR_ORIFICE_PRESSURE_PSI',\
                'INS_FS_EGR_TEMPERATURE',\
                'INS_FS_EGR_TEMPERATURE_CELCIUS',\
                'INS_FS_EGR_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_EGR_VALVE_POSITION_COMMANDED',\
                'INS_FS_EGR_VALVE_POSITION_COMMANDED_PERCENT',\
                'INS_FS_EGR_VALVE_POSITION_MEASURED_PERCENT_OPEN',\
                'INS_FS_EGR_VALVE_POSITION_MEASURED_PERCENT_OPEN_PERCENT',\
                'INS_FS_ENGINE_COOLANT_TEMPERATURE',\
                'INS_FS_ENGINE_COOLANT_TEMPERATURE_CELCIUS',\
                'INS_FS_ENGINE_COOLANT_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_ENGINE_DISTANCE_KM',\
                'INS_FS_ENGINE_HOURS',\
                'INS_FS_ENGINE_OIL_PRESSURE',\
                'INS_FS_ENGINE_OIL_PRESSURE_KPA',\
                'INS_FS_ENGINE_OIL_PRESSURE_PSI',\
                'INS_FS_ENGINE_OIL_TEMPERATURE',\
                'INS_FS_ENGINE_OIL_TEMPERATURE_CELCIUS',\
                'INS_FS_ENGINE_OIL_TEMPERATURE_DEGREES',\
                'INS_FS_ENGINE_OIL_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_ENGINE_SPEED',\
                'INS_FS_ENGINE_SPEED_RPM',\
                'INS_FS_EXHAUST_GAS_PRESSURE',\
                'INS_FS_EXHAUST_GAS_PRESSURE_INHG',\
                'INS_FS_EXHAUST_GAS_PRESSURE_KPA',\
                'INS_FS_EXHAUST_GAS_PRESSURE_PSI',\
                'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED',\
                'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED_CELCIUS',\
                'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED_FAHRENHEIT',\
                'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE',\
                'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3S',\
                'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3S',\
                'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED',\
                'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED_BAR',\
                'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED_PSI',\
                'INS_FS_FUEL_RAIL_PRESSURE_MEASURED',\
                'INS_FS_FUEL_RAIL_PRESSURE_MEASURED_BAR',\
                'INS_FS_FUEL_RAIL_PRESSURE_MEASURED_PSI',\
                'INS_FS_INTAKE_AIR_THROTTLE_POSITION',\
                'INS_FS_INTAKE_AIR_THROTTLE_POSITION_COMMANDED',\
                'INS_FS_INTAKE_AIR_THROTTLE_POSITION_PERCENT',\
                'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE',\
                'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE_CELCIUS',\
                'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE_INHG',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE_KPA',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE_PSI',\
                'INS_FS_KEYSWITCH_OFF_COUNTS',\
                'INS_FS_KEYSWITCH_ON_COUNTS',\
                'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE',\
                'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE_CELCIUS',\
                'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_PERCENT_ACCELERATOR_PEDAL_OR_LEVER',\
                'INS_FS_PERCENT_ACCELERATOR_PEDAL_OR_LEVER_PERCENT',\
                'INS_FS_PERCENT_LOAD',\
                'INS_FS_PERCENT_LOAD_PERCENT',\
                'INS_FS_PERCENT_REMOTE_ACCELERATOR_PEDAL_OR_LEVER',\
                'INS_FS_PERCENT_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_PERCENT',\
                'INS_FS_SENSOR_SUPPLY_1_V',\
                'INS_FS_SENSOR_SUPPLY_2_V',\
                'INS_FS_SENSOR_SUPPLY_3_V',\
                'INS_FS_SENSOR_SUPPLY_4_V',\
                'INS_FS_SENSOR_SUPPLY_5_V',\
                'INS_FS_SENSOR_SUPPLY_6_V',\
                'INS_FS_TRANSMISSION_GEAR_RATIO',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_COMMANDED_PERCENT_CLOSED',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_COMMANDED_PERCENT_CLOSED_PERCENT',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_MEASURED_PERCENT_CLOSED',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_MEASURED_PERCENT_CLOSED_PERCENT',\
                'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_CALCULATED_CELCIUS',\
                'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_CALCULATED_FAHRENHEIT',\
                'INS_FS_TURBOCHARGER_SPEED',\
                'INS_FS_TURBOCHARGER_SPEED_RPM',\
                'INS_FS_VEHICLE_SPEED',\
                'INS_FS_VEHICLE_SPEED_KM/HR',\
                'INS_FS_VEHICLE_SPEED_MPH']
vpcr_cols = ['VPCR_CMP_PIVOTED_VPCR19454', \
             'VPCR_CMP_PIVOTED_VPCR19795', \
             'VPCR_CMP_PIVOTED_VPCR19801', \
             'VPCR_CMP_PIVOTED_VPCR20493', \
             'VPCR_CMP_PIVOTED_VPCR21717', \
             'VPCR_CMP_PIVOTED_VPCR22005', \
             'VPCR_CMP_PIVOTED_VPCR22588', \
             'VPCR_CMP_PIVOTED_VPCR23934', \
             'VPCR_CMP_PIVOTED_VPCR26341', \
             'VPCR_CMP_PIVOTED_VPCR26400', \
             'VPCR_CMP_PIVOTED_VPCR26770', \
             'VPCR_CMP_PIVOTED_VPCR27232', \
             'VPCR_CMP_PIVOTED_VPCR27809', \
             'VPCR_CMP_PIVOTED_VPCR30446', \
             'VPCR_CMP_PIVOTED_VPCR32125', \
             'VPCR_CMP_PIVOTED_VPCR32486', \
             'VPCR_CMP_PIVOTED_VPCR33725', \
             'VPCR_CMP_PIVOTED_VPCR33948', \
             'VPCR_CMP_PIVOTED_VPCR34483', \
             'VPCR_CMP_PIVOTED_VPCR36804', \
             'VPCR_CMP_PIVOTED_VPCR36808', \
             'VPCR_CMP_PIVOTED_VPCR36930', \
             'VPCR_CMP_PIVOTED_VPCR41042']
mes_cols = [  \
            '''
            'MES_DA_CMP_DURATION_BY_STN_000_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_001_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_002_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_003_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_004_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_005_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_006_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_007_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_008_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_009_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_010_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_011_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_012_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_013_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_014_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_015_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_016_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_017_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_018_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_019_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_022_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_023_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_024_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_025_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_026_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_027_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_028_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_029_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_030_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_031_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_032_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_033_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_034_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_035_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_036_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_037_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_038_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_039_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_040_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_041_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_042_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_043_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_044_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_045_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_046_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_047_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_048_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_049_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_050_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_051_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_052_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_053_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_054_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_055_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_056_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_057_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_058_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_059_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_060_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_061_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_062_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_063_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_064_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_065_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_066_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_067_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_068_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_069_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_070_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_071_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_072_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_073_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_074_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_075_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_076_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_077_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_078_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_079_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_080_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_081_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_082_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_083_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_084_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_085_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_088_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_089_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_090_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_091_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_092_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_093_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_105_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_107_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_181_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_182_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_183_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_184_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_186_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_187_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_S01_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_S02_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_S03_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_S04_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_S05_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_S06_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_S07_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_S08_MIN', \
               'MES_DA_CMP_DURATION_BY_STN_S09_MIN', \
               'MES_DA_CMP_DURATION_BY_STN__1_AUD_MIN',
               '''
                ]
                
good_cols = ['REL_IS_OFF_HIGHWAY']

cols_to_split = ['REL_CMP_ENGINE_MILES_LIST', 'GREEN_WRENCH_FAULT_CODE_LIST']
extra_cols = ['ID', 'ESN', 'EARLIEST_INDICATION_DATE', \
              'REL_BUILD_DATE', \
              'REL_IN_SERVICE_DATE', \
              'REL_FAILURE_DATE', \
              'REL_TRUCK_MODEL', \
              'GREEN_WRENCH_FAULT_CODE_LIST', \
              'REL_MINED_FAULT_CODES_LIST', \
              'REL_OEM_GROUP', \
              'REL_ANALYSIS_RATE_CAT', \
              'REL_CMP_FAIL_CODE_LIST', \
              'INS_TI_ENGINE_DISTANCE_MILES', \
              'MES_DA_CMP_TPLOC_AS_MIN', \
              'MES_DA_CMP_TPLOC_FP_MIN', \
              'MES_DA_CMP_TPLOC_HD_MIN', \
              'MES_DA_CMP_TPLOC_SB_MIN', \
              'MES_DA_CMP_TPLOC_TS_MIN', \
              'MES_TSH_CMP_TOTAL_TIME_CANCEL_MIN', \
              'MES_TSH_CMP_TOTAL_TIME_FINISH_MIN', \
              'MES_TSH_CMP_TOTAL_TIME_HOLD_MIN', \
              'MES_TSH_CMP_TOTAL_TIME_PROD_MIN', \
              'MES_TSH_CMP_TOTAL_TIME_PURGED_MIN', \
              'MES_TSH_CMP_TOTAL_TIME_REPAIR_MIN', \
              'MES_TSH_CMP_TOTAL_TIME_SCHED_MIN', \
              'MES_TSH_CMP_TOTAL_TIME_SHIP_MIN', \
              'INS_EA_CRANKCASE_PRSR_SEC_SEV_1', \
              'INS_EA_COOLANT_TMP_SEC_SEV_1', \
              'INS_EA_OIL_PRSR_SEC_SEV_1', \
              'INS_EA_OIL_TMP_SEC_SEV_1', \
              'INS_EA_SPEED_SEC_SEV_1', \
              'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_1', \
              'INS_EA_CRANKCASE_PRSR_SEC_SEV_2', \
              'INS_EA_COOLANT_TMP_SEC_SEV_2', \
              'INS_EA_OIL_PRSR_SEC_SEV_2', \
              'INS_EA_OIL_TMP_SEC_SEV_2', \
              'INS_EA_SPEED_SEC_SEV_2', \
              'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_2', \
              'INS_EA_CRANKCASE_PRSR_SEC_SEV_3', \
              'INS_EA_COOLANT_TMP_SEC_SEV_3', \
              'INS_EA_OIL_PRSR_SEC_SEV_3', \
              'INS_EA_OIL_TMP_SEC_SEV_3', \
              'INS_EA_SPEED_SEC_SEV_3', \
              'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_3', \
              'INS_EA_CRANKCASE_PRSR_SEC_SEV_TOTAL', \
              'INS_EA_COOLANT_TMP_SEC_SEV_TOTAL', \
              'INS_EA_OIL_PRSR_SEC_SEV_TOTAL', \
              'INS_EA_OIL_TMP_SEC_SEV_TOTAL', \
              'INS_EA_SPEED_SEC_SEV_TOTAL', \
              'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_TOTAL', \
              'INS_TI_FUEL_USED_GALLON', \
              'INS_TI_IDLE_TIME_HOURS', \
              'INS_TI_IDLE_TIME_PERCENTAGE', \
              'INS_DC_CMP_PERCENT_LOW_SPEED_TIME', \
              'INS_DC_CMP_PERCENT_HIGH_SPEED_TIME', \
              'INS_DC_CMP_PERCENT_ZERO_OR_NEGATIVE_TORQUE_TIME', \
              'INS_DC_CMP_PERCENT_LOW_SPEED_LOW_TORQUE_TIME', \
              'INS_DC_CMP_PERCENT_LIGHT_LOAD_TIME', \
              'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', \
              'INS_DC_CMP_PERCENT_CLIMBING_TIME', \
              'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME', \
              'INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME', \
              'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME', \
              'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME']

fault_code_cols = ['GREEN_WRENCH_FAULT_CODE_LIST', \
              'REL_MINED_FAULT_CODES_LIST']

#Turn VPCR Features into Binary#
for col in tqdm(vpcr_cols):
    empty_col = np.zeros(fat_table.shape[0])
    empty_col[fat_table[col].dropna().index.values] = 1
    fat_table[col] = empty_col
    

## Add Fault Codes to Table ##
    
#Get Unique Fault Codes#
unique_fault_codes = set()
for line in fat_table['GREEN_WRENCH_FAULT_CODE_LIST']:
    if line is not None:
        for fc in line:
            unique_fault_codes.add(fc)
           
#Create empty fault code DF
fc_df = pd.DataFrame(data = 0, columns = unique_fault_codes, index = fat_table.index)

#One hot coding for Fault Codes
for i, line in tqdm(enumerate(fat_table['GREEN_WRENCH_FAULT_CODE_LIST'])):
    fc_df.iloc[i,np.isin(fc_df.columns.values, line)] = 1
    
fc_lookup = pd.read_csv("Desktop/Data/fault_code_lookup_solve.csv", header = 1)
fc_lookup = fc_lookup[['Fault Code', 'Fault Code Category']]
fc_lookup['Fault Code Category'] = [fc_lookup['Fault Code Category'][x].replace("/","_") for x in range(fc_lookup.shape[0])]
fc_lookup['Fault Code Category'] = [fc_lookup['Fault Code Category'][x].replace(" ","_") for x in range(fc_lookup.shape[0])]
fc_lookup.iloc[fc_lookup[fc_lookup['Fault Code Category'] == 'Coolant___Lubrication'].index,1] = 'Coolant_Lubrication'


#Convert to String
#fc_df.columns = ['FC_' + str(fc_df.columns.values[x]) for x in range(fc_df.shape[1])]

fc_cols = []
for x in range(fc_df.shape[1]):
    if len(fc_lookup[fc_lookup['Fault Code'] == fc_df.columns.values[x]]) == 0:
        fc_cols.append('FC:{}'.format(fc_df.columns.values[x]))
    else:
        fc_cols.append('FC_' + \
                       fc_lookup[fc_lookup['Fault Code'] == fc_df.columns.values[x]]['Fault Code Category'].values[0].replace(" ", "_") + \
                       ':' + str(fc_df.columns.values[x]))
fc_df.columns = fc_cols

#Clean numeric columns
for col in tqdm(numeric_cols):
    if fat_table[col].dtype not in ['float64', 'float32']:
        fat_table[col] = fat_table[col].str.replace(',','.')
        fat_table[col] = fat_table[col].astype(float)
    if 'PERCENT' in col:
        fat_table[col][fat_table[col] < 0] = float('NaN')
        fat_table[col][fat_table[col] > 100] = float('NaN')
    else:
        fat_table[col][fat_table[col] < -10**6] = float('NaN')
        fat_table[col][fat_table[col] > 10**6] = float('NaN')

#All duration columns are negative
duration_cols = ['MES_DEV_AVG_DURATION_CMP', \
                 'MES_DEV_TOTAL_DURATION_CMP', \
                 'MES_DEV_MAX_DURATION_CMP', \
                 'MES_DEV_MIN_DURATION_CMP', \
                 'MES_DEV_ACTION_ADD_AVG_DURATION_CMP', \
                 'MES_DEV_ACTION_ADD_TOTAL_DURATION_CMP', \
                 'MES_DEV_ACTION_ADD_MAX_DURATION_CMP', \
                 'MES_DEV_ACTION_ADD_MIN_DURATION_CMP', \
                 'MES_DEV_ACTION_OMIT_AVG_DURATION_CMP', \
                 'MES_DEV_ACTION_OMIT_TOTAL_DURATION_CMP', \
                 'MES_DEV_ACTION_OMIT_MAX_DURATION_CMP', \
                 'MES_DEV_ACTION_OMIT_MIN_DURATION_CMP', \
                 'MES_DEV_ACTION_QTY_AVG_DURATION_CMP', \
                 'MES_DEV_ACTION_QTY_TOTAL_DURATION_CMP', \
                 'MES_DEV_ACTION_QTY_MAX_DURATION_CMP', \
                 'MES_DEV_ACTION_QTY_MIN_DURATION_CMP', \
                 'MES_DEV_ACTION_REPL_AVG_DURATION_CMP', \
                 'MES_DEV_ACTION_REPL_TOTAL_DURATION_CMP', \
                 'MES_DEV_ACTION_REPL_MAX_DURATION_CMP', \
                 'MES_DEV_ACTION_REPL_MIN_DURATION_CMP']


for col in tqdm(duration_cols):
    fat_table[col] = fat_table[col]*-1

# Take first val for MES line features #
'''
for col in tqdm(mes_cols):
    temp_col = [fat_table[col][x] if fat_table[col][x] is not None else np.array([]) for x in range(fat_table.shape[0])]
    del fat_table[col]
    fat_table[col] = [temp_col[x][0] if len(temp_col[x]) > 0 else float('NaN') for x in range(fat_table.shape[0])]

fat_table['mes_is_rework'] = [0 if len(temp_col[x]) <= 1 else 1 for x in range(fat_table.shape[0])]
'''
#Subset columns
#df_ot = fat_table[['ESN'] + numeric_cols + cat_cols + mes_cols]

# Get ESNs #

oepl_cols = np.delete(oepl_cols, 540)

df_esn = fat_table[['ESN', 'EARLIEST_INDICATION_DATE']]

df_cat = fat_table[cat_cols]

df_num = fat_table[numeric_cols]

df_vpcr = fat_table[vpcr_cols]

df_oepl = fat_table[oepl_cols].fillna(0)

df_issue = fat_table[['INCDT_ISSUE_NUMBER']]

df_ot = pd.concat([df_esn, df_cat, df_num, df_oepl, df_issue, df_vpcr, fc_df], axis = 1)

df_ot.to_csv("Desktop/Data/fat_table_no_one_hot_raw.csv", index = False)

# Categorical Features

df_cat = pd.DataFrame()

for col in tqdm(cat_cols):
    df_temp = pd.get_dummies(fat_table[col])
    df_temp.columns = [col + ':' + df_temp.columns.values.astype(str)[x] for x in range(df_temp.shape[1])]
    df_cat = pd.concat([df_cat, df_temp], axis = 1)

'''
for col in numeric_cols:
    finite_vals = df_num[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        df_num.iloc[finite_vals.index.values, np.where(df_num.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df_num[col] = df_num[col].replace(np.nan, 0)
'''
    
# Manual Columns #
'''
df_man = fat_table[manual_cols]
df_man[manual_cols] = 0
df_man[fat_table[manual_cols[0]] == True] = 1

# Issue #
df_issue = fat_table[['INCDT_ISSUE_NUMBER']]
'''
# Split Cols on important vals #
df_ot = pd.concat([df_esn, df_cat, df_num, df_oepl, df_issue, df_vpcr, fc_df], axis = 1)

#Save object (since that was a lot of work!)
df_ot.to_csv("Desktop/Data/fat_table_with_one_hot_raw.csv", index = False)
#pickle.dump(df_ot, open("Desktop/QUALITY_FEATURES_PARQUET_NEW/fat_table_with_fs.p", "wb"))
#del df_ot['INCDT_ISSUE_NUMBER']
#df_test = pd.concat([df_esn, df_cat], axis = 1)


##df_sub = df_ot.merge(df_test, on = 'ESN', how = 'left')
#df_ot = pd.read_csv("Desktop/fat_table_with_fs.csv")


#df_sub.to_csv("Desktop/fat_table_with_fs.csv", index = False)

#Get extra cols for universal view#

df_extra = fat_table[extra_cols] 

pickle.dump(df_extra, open("Desktop/Data/universal_view_cols.p", "wb" ) )


'''
egr_df = pd.read_csv("Desktop/egr_cooler_updated_esns.csv", names = ['ESN'])

df_issue = df_ot.query("ESN in @egr_df.ESN")
df_issue['Label'] = 1
df_not_issue = df_ot.query("ESN not in @egr_df.ESN")
df_not_issue['Label'] = 0

df_not_issue_10000 = df_not_issue.sample(10000)

df_egr_10000 = pd.concat([df_issue, df_not_issue_10000]).reset_index(drop = True)

df_egr = pd.concat([df_issue, df_not_issue]).reset_index(drop = True)

df_egr_10000.to_csv("Desktop/Data/fat_table_egr_subsample_10k_031918.csv", index = False)
df_egr.to_csv("Desktop/Data/fat_table_egr_all_031918.csv", index = False)

'''


##


ins_fs_cat_cols = ['INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_REGENERATION_PERMIT_SWITCH_STATUS_NA', \
                   'INS_FS_AFTERTREATMENT_FUEL_INJECTOR_STATUS_NA', \
                   'INS_FS_AFTERTREATMENT_FUEL_SHUTOFF_VALVE_STATUS_NA', \
                   'INS_FS_AFTERTREATMENT_PURGE_AIR_SOLENOID_STATUS_NA', \
                   'INS_FS_AIR_CONDITIONING_PRESSURE_SWITCH_NA', \
                   'INS_FS_BRAKE_PEDAL_POSITION_SWITCH_NA', \
                   'INS_FS_CLUTCH_PEDAL_POSITION_SWITCH_NA', \
                   'INS_FS_CRANKCASE_VENTILATION_HEATER_COMMANDED_STATE_NA', \
                   'INS_FS_CRUISE_CONTROL_ON_OFF_SWITCH_NA', \
                   'INS_FS_EGR_FLOW_DERATE_NA', \
                   'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_2_NA', \
                   'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_3_NA', \
                   'INS_FS_ENGINE_COOLANT_LEVEL_NA', \
                   'INS_FS_ENGINE_OPERATING_STATE_NA', \
                   'INS_FS_ENGINE_PROTECTION_DERATE_SUPPRESS_NA', \
                   'INS_FS_ENGINE_PROTECTION_SHUTDOWN_OVERRIDE_SWITCH_NA', \
                   'INS_FS_ENGINE_SPEED_MAIN_SENSOR_SIGNAL_STATE_NA', \
                   'INS_FS_ENGINE_SPEED_MAIN_SYNCHRONIZATION_STATE_NA', \
                   'INS_FS_ENGINE_SPEED_STATUS_NA', \
                   'INS_FS_ENGINE_TORQUE_MODE_NA', \
                   'INS_FS_FAN_CONTROL_SWITCH_NA', \
                   'INS_FS_FAN_DRIVE_STATE_NA', \
                   'INS_FS_KEYSWITCH_NA', \
                   'INS_FS_PTO_ON_OFF_SWITCH_NA', \
                   'INS_FS_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_SWITCH_NA', \
                   'INS_FS_REMOTE_PTO_SWITCH_NA', \
                   'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_DERATE_NA',
                   'INS_FS_TURBOCHARGER_SPEED_DERATE_ACTIVE_NA',
                   'INS_FS_WATER_IN_FUEL_STATE_NA', \
                   
                   
                   ]

numeric_fs_cols = ['INS_FS_ACCELERATOR_PEDAL_OR_LEVER_POSITION_SENSOR_SUPPLY_VOLTAGE_VOLTS',
       'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_PSI',
       'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_QUALITY_PERCENT',
       'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE_F',
       'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_PSI',
       'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_PSI',
       'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE_F',
       'INS_FS_AFTERTREATMENT_FUEL_PRESSURE_PSI',
       'INS_FS_AFTERTREATMENT_FUEL_SHUTOFF_VALVE_STATUS_NA',
       'INS_FS_AFTERTREATMENT_INTAKE_NOX_CORRECTED_PPM',
       'INS_FS_AFTERTREATMENT_OUTLET_NOX_CORRECTED_PPM',
       'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE_F',
       'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE_F',
       'INS_FS_AIR_CONDITIONING_PRESSURE_SWITCH_NA',
       'INS_FS_BAROMETRIC_AIR_PRESSURE_PSI', \
       'INS_FS_BATTERY_VOLTAGE_VOLTS',
       'INS_FS_CRANKCASE_PRESSURE_PSI',
       'INS_FS_ECM_TIME_HOURS',
       'INS_FS_ECM_TIME_KEY_ON_TIME_HOURS',
       'INS_FS_EGR_DIFFERENTIAL_PRESSURE_PSI',
       'INS_FS_EGR_ORIFICE_PRESSURE_PSI',
       'INS_FS_EGR_TEMPERATURE_F', 
       'INS_FS_EGR_VALVE_POSITION_COMMANDED_PERCENT',
       'INS_FS_EGR_VALVE_POSITION_MEASURED_PERCENT_OPEN_PERCENT',
       'INS_FS_ENGINE_COOLANT_TEMPERATURE_F',
       'INS_FS_ENGINE_HOURS_HOURS', 
       'INS_FS_ENGINE_OIL_PRESSURE_PSI',
       'INS_FS_ENGINE_OIL_TEMPERATURE_F',      
       'INS_FS_ENGINE_SPEED_RPM',
       'INS_FS_EXHAUST_GAS_PRESSURE_PSI',
       'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED_F',
       'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3PS',
       'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED_PSI',
       'INS_FS_FUEL_RAIL_PRESSURE_MEASURED_PSI',
       'INS_FS_INTAKE_AIR_THROTTLE_POSITION_COMMANDED_PERCENT',
       'INS_FS_INTAKE_AIR_THROTTLE_POSITION_PERCENT',
       'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE_F',
       'INS_FS_INTAKE_MANIFOLD_PRESSURE_PSI',
       'INS_FS_KEYSWITCH_OFF_COUNTS_NA', 'INS_FS_KEYSWITCH_ON_COUNTS_NA',
       'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE_F',
       'INS_FS_PERCENT_ACCELERATOR_PEDAL_OR_LEVER_PERCENT',
       'INS_FS_PERCENT_LOAD_PERCENT',
       'INS_FS_PERCENT_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_PERCENT',

       'INS_FS_SENSOR_SUPPLY_1_VOLTS', 'INS_FS_SENSOR_SUPPLY_2_VOLTS',
       'INS_FS_SENSOR_SUPPLY_3_VOLTS', 'INS_FS_SENSOR_SUPPLY_4_VOLTS',
       'INS_FS_SENSOR_SUPPLY_5_VOLTS', 'INS_FS_SENSOR_SUPPLY_6_VOLTS',
       'INS_FS_TRANSMISSION_GEAR_RATIO_NA',
       'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_COMMANDED_PERCENT_CLOSED_PERCENT',
       'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_MEASURED_PERCENT_CLOSED_PERCENT',
       'INS_FS_TURBOCHARGER_COMPRESSOR_INLET_AIR_TEMPERATURE_CALCULATED_F',
       'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_CALCULATED_F',
       'INS_FS_TURBOCHARGER_SPEED_RPM',
       'INS_FS_VEHICLE_SPEED_MPH'
       ]

