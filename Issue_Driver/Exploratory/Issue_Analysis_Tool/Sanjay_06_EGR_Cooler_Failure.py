#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 15:18:29 2018

@author: sanjay
"""

import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
import mifs
import statsmodels.api as sm
import scipy.stats as stat
from sklearn.preprocessing import PolynomialFeatures
from scipy.sparse import csr_matrix
import seaborn as sns

plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

#Load Fat Table
df_ot = pd.read_csv("Desktop/fat_table_with_fs.csv")

#Load new EGR Info
egr_df = pd.read_csv("Desktop/egr_cooler_updated_esns.csv", names = ['ESN'])
egr_df = egr_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
del egr_df['ESN']
del egr_df['INCDT_ISSUE_NUMBER']
egr_df['Label'] = 1

#Get non-issues
df_not_issue = df_ot.query("INCDT_ISSUE_NUMBER == 'not_issue'")
df_temp = online_tracker.query("INCDT_ISSUE_NUMBER == 'not_issue'")[['ESN', 'REL_CMP_FAIL_CODE_LIST']].reset_index(drop = True)
none_idx = [df_temp.REL_CMP_FAIL_CODE_LIST[x] is None for x in range(df_temp.shape[0])]
df_temp = df_temp[none_idx]
df_not_issue = df_not_issue.query('ESN in @df_temp.ESN')
#df_not_issue = df_not_issue.sample(100)
df_not_issue['Label'] = 0

#df_not_issue = df_not_issue.query("REL_BUILD_YEAR_MONTH_2017_1 == 1 | REL_BUILD_YEAR_MONTH_2017_2 == 1 | \
##        REL_BUILD_YEAR_MONTH_2017_3 == 1 | REL_BUILD_YEAR_MONTH_2017_4 == 1 | \
#        REL_BUILD_YEAR_MONTH_2017_5 == 1 | REL_BUILD_YEAR_MONTH_2017_6 == 1 | \
#        REL_BUILD_YEAR_MONTH_2017_7 == 1 | REL_BUILD_YEAR_MONTH_2017_8 == 1 | \
#        REL_BUILD_YEAR_MONTH_2017_9 == 1 | REL_BUILD_YEAR_MONTH_2017_10 == 1")


#Concatenate issue and non-issue
df_all = pd.concat([egr_df, df_not_issue]).reset_index(drop = True)
y = df_all['Label']
del df_all['Label'], df_all['INCDT_ISSUE_NUMBER'], df_all['ESN']


#Scale numeric features
for col in tqdm(numeric_cols + mes_cols):
    if df_all[col].dtype != 'float64':
        df_all[col] = df_all[col].str.replace(',','.')
    finite_vals = df_all[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        df_all.iloc[finite_vals.index.values, np.where(df_all.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df_all[col] = df_all[col].replace(np.nan, 0)
    

#Remove everything that starts with MES
col_idx = ~np.array([df_all.columns.values[x].startswith('MES') for x in range(df_all.shape[1])])
df_all_sub = df_all.iloc[:,col_idx]   

#Scale numeric features
for col in tqdm(numeric_cols):
    if df_all_sub[col].dtype != 'float64':
        df_all_sub[col] = df_all_sub[col].str.replace(',','.')
        df_all_sub[col] = df_all_sub[col].astype(float)
    finite_vals = df_all_sub[col].dropna()
    
    #Replace NaN with 0's (Mean)
    df_all_sub[col] = df_all_sub[col].replace(np.nan, np.mean(finite_vals))
    if len(finite_vals) == 0:
        del df_all_sub[col]


#Create interactions
poly = PolynomialFeatures(interaction_only=True)
X_new = poly.fit_transform(df_all_sub)   
X_names = poly.get_feature_names(df_all_sub.columns)

np.any([numeric_cols[x] in X_names[2000] for x in range(len(numeric_cols))])

X_new_2 = csr_matrix(X_new)

#Ridge regression with balanced classes
clf = LogisticRegression(penalty = 'l2', class_weight = 'balanced', solver = 'liblinear')
clf.fit(X_new_2, y)
coef_sort_idxs = np.argsort(np.abs(clf.coef_[0]))[::-1]
num_non_zero = np.sum(clf.coef_[0] != 0)
top_cols = np.array(X_names)[coef_sort_idxs[:num_non_zero]]
top_col_coefs = clf.coef_[0][coef_sort_idxs[:num_non_zero]]
for col, coef in zip(top_cols[:200], top_col_coefs[:200]):
        print("{},{}".format(col, coef))
        
##Re-concatenate with ESN#
df_all = pd.concat([egr_df, df_not_issue]).reset_index(drop = True)

#
col1 = 'REL_BUILD_YEAR_MONTH'
col2 = 'REL_OEM_GROUP'
merged_col = 'BUILD_YEAR_MONTH_OEM_GROUP'

df_sub = fat_table[['ESN', col1, col2]].drop_duplicates()
df_all_sub = df_all[['ESN', 'Label']].drop_duplicates()
df_sub = df_all_sub.merge(df_sub, on = 'ESN', how = 'inner').reset_index(drop = True)
df_sub[merged_col] = [None if df_sub[col1][x] is None or df_sub[col2][x] is None else \
 df_sub[col1][x] + '_' + df_sub[col2][x] for x in range(df_sub.shape[0])]


cat_prop_plot(df_sub, merged_col, "", df_sub.Label.mean()*100, sort = True, only_sig = True)


#
col1 = 'INS_EA_OIL_TMP_SEC_SEV_TOTAL'
col2 = 'REL_MKTG_HSP_NUM'
df_sub = fat_table[['ESN', col1, col2]].drop_duplicates()
df_all_sub = df_all[['ESN', 'Label']].drop_duplicates()
df_sub = df_all_sub.merge(df_sub, on = 'ESN', how = 'inner').reset_index(drop = True)
df_sub = df_sub.loc[df_sub[col1][~np.isnan(df_sub[col1])].index]
cats = [485, 505, 525, 565, 605]
df_sub = df_sub.query("REL_MKTG_HSP_NUM in @cats")

sns.violinplot(x=col2, y=col1, hue="Label", data=df_sub, split=True, scale = 'width');










       
        
        


runRegressionAndOutputFeatures(df_all, 'EDS')

df_all = pd.concat([egr_df, df_not_issue]).reset_index(drop = True)
col = 'INS_FS_FUEL_RAIL_PRESSURE_MEASURED_PSI'
fig, ax = plt.subplots(figsize=(12, 8))
#egr_df_all[col][egr_df_all[col] == 0] = float('NaN')
ax.hist(df_all.query("Label == 1")[col].dropna(),
            color='red', alpha=0.75, bins = 10, label = 'EGR Cooler Issue', normed=True)
ax.hist(df_all.query("Label == 0")[col].dropna(),
            color='blue', alpha=0.75, bins = 10, label = 'NO ISSUE', normed=True)
ax.set(title='Histogram',
       ylabel='Count', xlabel = col)
plt.legend()
plt.show() 


col = 'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE_FAHRENHEIT'
df_all[col][df_all[col] == '617,3'] = 617.3
df_all[col][df_all[col] == '32,0'] = 32.0

col = 'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_INLET_TEMPERATURE_FAHRENHEIT'
df_all[col][df_all[col] == '586,4'] = 586.4
df_all[col][df_all[col] == '32,0'] = 32.0

col = 'INS_FS_TURBOCHARGER_SPEED_RPM'
df_all[col][df_all[col] < 0] = float('NaN')

col = 'INS_EA_OIL_TMP_SEC_SEV_TOTAL'
df_all[col][df_all[col] < 0] = float('NaN')
fig, ax = plt.subplots(figsize=(12, 8))
hist, bins = np.histogram(df_all.query("Label == 1")[col].dropna().astype(float), bins = 10)
ax.bar(bins[:-1], hist.astype(np.float32)*100 / hist.sum(), width=(bins[1]-bins[0]), \
       color='red', alpha = 0.75, label = 'EGR Cooler Issue')
hist, bins = np.histogram(df_all.query("Label == 0")[col].dropna().astype(float), bins = 10)
ax.bar(bins[:-1], hist.astype(np.float32)*100 / hist.sum(), width=(bins[1]-bins[0]), \
       color='blue', alpha = 0.75, label = 'No Issue')
ax.set(title='Histogram',
       ylabel='Percentage (%)', xlabel = col)
plt.legend()
plt.show() 


col = 'INS_FS_BRAKE_PEDAL_POSITION_SWITCH'
df_sub = fault_snap_new[['ESN','EARLIEST_INDICATION_DATE', col]]
df_sub = df_sub.merge(df_all, on = ['ESN'], how = 'right')
cat_prop_plot(df_sub, col, "")


col = 'REL_BUILD_YEAR_MONTH'
df_sub = fat_table[['ESN', col]]
df_sub = df_sub.merge(df_all, on = ['ESN'], how = 'right')
cat_prop_plot(df_sub, col, col, df_sub.Label.mean()*100)

oepl_vars = ['TH1724', 'WO1129', 'FCPA18', 'WP1793', 'IC1083', 'DO10234', 'PP44342', \
             'RL1754', 'FR11349']
df_sub = df[['engine_serial_num', 'option_assembly_num']]
df_sub = df_sub.rename(columns = {'engine_serial_num': 'ESN'})
df_sub = df_sub.merge(df_all, on = 'ESN', how = 'inner')
df_sub = df_sub.query("option_assembly_num in @oepl_vars")

cat_prop_plot(df_sub, 'option_assembly_num', "option_assembly_num", df_all.Label.mean()*100)

oepl_vars = ['FCPA05', 'FCPA06', \
       'FCPA11', 'FCPA12', 'FCPA13', 'FCPA16', 'FCPA17', 'FCPA18', \
       'FCPA19', 'FCPA21', 'FCPA22', 'FCPA24', 'FCPA25', 'FCPA26', \
       'FCPA27', 'FCPA28', 'FCPA32', 'FCPA36', 'FCPA37', 'FCPA41', \
       'FCPA45', 'FCPA46', 'FCPA47', 'FCPA51', 'FCPA52', 'FCPA53', \
       'FCPA54', 'FCPA56', 'FCPA57']


oepl_vars = ['FCPA11', 'FCPA12', 'FCPA13', 'FCPA16', 'FCPA17', 'FCPA18', \
        'FCPA24', 'FCPA25', 'FCPA26', \
       'FCPA27', 'FCPA28', 'FCPA37', 'FCPA41', \
       'FCPA47', 'FCPA51', 'FCPA52', 'FCPA53', \
       'FCPA54', 'FCPA56', 'FCPA57']
df_sub = df[['engine_serial_num', 'option_assembly_num']]
df_sub = df_sub.rename(columns = {'engine_serial_num': 'ESN'})
df_sub = df_sub.merge(df_all, on = 'ESN', how = 'inner')
df_sub = df_sub.query("option_assembly_num in @oepl_vars")

cat_prop_plot(df_sub, 'option_assembly_num', "option_assembly_num", df_all.Label.mean()*100)


#Load new EGR Info
egr_df = pd.read_csv("Desktop/egr_cooler_updated_esns.csv", names = ['ESN'])
egr_df = egr_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
#del egr_df['ESN']
del egr_df['INCDT_ISSUE_NUMBER']
egr_df['Label'] = 1

#Get non-issues
df_not_issue = df_ot.query("INCDT_ISSUE_NUMBER == 'not_issue'")
df_temp = online_tracker.query("INCDT_ISSUE_NUMBER == 'not_issue'")[['ESN', 'REL_CMP_FAIL_CODE_LIST']].reset_index(drop = True)
none_idx = [df_temp.REL_CMP_FAIL_CODE_LIST[x] is None for x in range(df_temp.shape[0])]
df_temp = df_temp[none_idx]
df_not_issue = df_not_issue.query('ESN in @df_temp.ESN')
#df_not_issue = df_not_issue.sample(100)
df_not_issue['Label'] = 0

#Concatenate issue and non-issue
df_all = pd.concat([egr_df, df_not_issue]).reset_index(drop = True)

df = pd.read_csv("Desktop/oepl/oepl_x15.csv")
df['option_count'] = 1
df_pivot = df.pivot_table(index = 'engine_serial_num', columns = 'option_assembly_num', \
                          values = 'option_count', aggfunc='mean').fillna(0).reset_index()
df_pivot = df_pivot.rename(columns = {'engine_serial_num': 'ESN'})
df_all = pd.concat([egr_df, df_not_issue]).reset_index(drop = True)
egr_df_sub = df_all[['ESN', 'Label']]
egr_df_sub = egr_df_sub.merge(df_pivot, on = 'ESN', how = 'inner')
y = egr_df_sub['Label']
del egr_df_sub['Label'], egr_df_sub['ESN']
#Run Log Reg
clf = LogisticRegressionCV(Cs = 100, penalty = 'l2', class_weight = 'balanced', solver = 'liblinear')
    #clf = LogisticRegression(penalty = pen, class_weight = 'balanced', solver = 'liblinear')
clf.fit(egr_df_sub, y)


denom = (2.0*(1.0+np.cosh(clf.decision_function(egr_df_sub))))
denom = np.tile(denom,(egr_df_sub.shape[1],1)).T
F_ij = np.dot((egr_df_sub/denom).T,egr_df_sub) ## Fisher Information Matrix
F_new = F_ij + np.eye(F_ij.shape[0])*.01
Cramer_Rao = np.linalg.inv(F_new) ## Inverse Information Matrix
sigma_estimates = np.array([np.sqrt(Cramer_Rao[i,i]) for i in range(Cramer_Rao.shape[0])]) # sigma for each coefficient
z_scores = clf.coef_[0]/sigma_estimates # z-score for eaach model coefficient
p_values = [stat.norm.sf(abs(x))*2 for x in z_scores] ### two tailed test for p-values


coef_sort_idxs = np.argsort(np.abs(clf.coef_[0]))[::-1]
num_non_zero = np.sum(clf.coef_[0] != 0)
top_cols = egr_df_sub.columns.values[coef_sort_idxs[:num_non_zero]]
top_col_coefs = clf.coef_[0][coef_sort_idxs[:num_non_zero]]
top_p_values = np.array(p_values)[coef_sort_idxs[:num_non_zero]]

for col, coef, p_val in zip(top_cols, top_col_coefs, top_p_values):
    print("{},{},{}".format(col, coef, p_val))
 
    
    

# Lasso Function #

def runRegressionAndOutputFeatures(df, cols):

    #Create model object
    #Ridge Regression, 10 Folds, Balanced Classes
    clf = LogisticRegressionCV(Cs = 10, penalty = 'l2', class_weight = 'balanced', solver = 'liblinear')

    #Subset columns starting with 'cols' variable (eg: MES, REL, INS, etc.)
    col_idx = [df.columns.values[x].startswith(cols) for x in range(df.shape[1])]
    
    #Fit model!
    clf.fit(df.iloc[:,col_idx], y)
    
    denom = (2.0*(1.0+np.cosh(clf.decision_function(df.iloc[:,col_idx]))))
    denom = np.tile(denom,(df.iloc[:,col_idx].shape[1],1)).T
    F_ij = np.dot((df.iloc[:,col_idx]/denom).T,df.iloc[:,col_idx]) ## Fisher Information Matrix
    F_new = F_ij + np.eye(F_ij.shape[0])*.01
    Cramer_Rao = np.linalg.inv(F_new) ## Inverse Information Matrix
    sigma_estimates = np.array([np.sqrt(Cramer_Rao[i,i]) for i in range(Cramer_Rao.shape[0])]) # sigma for each coefficient
    z_scores = clf.coef_[0]/sigma_estimates # z-score for eaach model coefficient
    p_values = [stat.norm.sf(abs(x))*2 for x in z_scores] ### two tailed test for p-values
    
    coef_sort_idxs = np.argsort(np.abs(clf.coef_[0]))[::-1]
    num_non_zero = np.sum(clf.coef_[0] != 0)
    top_cols = df.iloc[:,col_idx].columns.values[coef_sort_idxs[:num_non_zero]]
    top_col_coefs = clf.coef_[0][coef_sort_idxs[:num_non_zero]]
    top_p_values = np.array(p_values)[coef_sort_idxs[:num_non_zero]]
    print("Variable, Coefficient Value, P-Value")
    for col, coef, p_val in zip(top_cols, top_col_coefs, top_p_values):
        print("{},{},{}".format(col, coef, p_val))
        
        
        
        






def cat_prop_plot(df1, var_name, var_desc, vline = None, sort=False, only_sig = False):
   #Forest Plot for categorical variables 
  
   dfpp = df1.groupby(var_name).Label.agg({'beta_mode':np.mean, 'ww': np.sum,
                                           'Not-ww': lambda x: np.sum(1-x),
                                           'count': lambda x: np.shape(x)[0]})
   if sort == True:
       dfpp = dfpp.sort_values(by='beta_mode', ascending=False)
      
   names = dfpp.index.values
   
   #names = ['2014_7', '2014_10','2015_10', \
   #        '2016_2', '2016_3', '2016_11', '2016_12', \
   #         '2017_1', '2017_2', '2017_3', '2017_4', '2017_5', '2017_6', \
   #         '2017_7', '2017_8', '2017_9', '2017_10'] 
   
   if only_sig == True:
       sig_names = []
       for i, val in enumerate(names):
           a = dfpp.iloc[i]['ww'] + 1
           b = dfpp.iloc[i]['Not-ww'] + 1
           hpd95 = 100*HDIofICDF(beta, credMass=0.95, a=a, b=b)
           if (vline > hpd95[0] and vline > hpd95[1]) or (vline < hpd95[0] and vline < hpd95[1]):
               sig_names.append(val)
       names = sig_names 
       dfpp = dfpp.loc[names]

   p = len(names)
   space = 0.5
   fig, ax = plt.subplots(figsize=(8,(p+1)*0.5))
   for i, val in enumerate(names):
       # parameters of beta distribution
       a = dfpp.iloc[i]['ww'] + 1
       b = dfpp.iloc[i]['Not-ww'] + 1
       
       #a = dfpp.loc[val]['ww'] + 1
       #b = dfpp.loc[val]['Not-ww'] + 1
       # plot most probable value (the mode)
       mode = (a - 1)/(a + b - 2)
       ax.plot( 100*mode, [-space -i*space],  marker='o', markersize=5, color='blue')
       # Plot the High Posterior Density regions
       hpd50 = 100*HDIofICDF(beta, credMass=0.50, a=a, b=b)
       hpd95 = 100*HDIofICDF(beta, credMass=0.95, a=a, b=b)
       ax.errorbar(x=(hpd50[0],hpd50[1]), y=(-space-i*space,-space-i*space), color='blue', 
                               linewidth= 2)
       ax.errorbar(x=(hpd95[0],hpd95[1]), y=(-space-i*space,-space-i*space), color='blue', 
                       linewidth= 1)
      
   if vline:
       ax.axvline(vline, color='k', linestyle=':', alpha=0.7, linewidth=1, label='Overall % of EGR Issues')
   ax.set_ylim(-space*(p+1),0)
   ax.set_xlim(-0.02*100,1.02*100)
   ax.set_yticks(-space-1*np.arange(p)*space)
   ax.set_yticklabels(names)
   ax.set_ylabel('{}'.format(var_desc))
   ax.set_xlabel('% of EGR Issues');
   ax.set_title('% of EGR Issues by {}'.format(var_desc));
   ax.grid(False)
   if vline:
       ax.legend(loc = 0, fontsize = 'medium')
   plt.savefig('Desktop/Solve_Results/Figs/{}.png'.format(var_desc), bbox_inches="tight") 
    
       
       
def HDIofICDF(dist_name, credMass=0.95, **args):
    # freeze distribution with given arguments
    distri = dist_name(**args)
    # initial guess for HDIlowTailPr
    incredMass =  1.0 - credMass

    def intervalWidth(lowTailPr):
        return distri.ppf(credMass + lowTailPr) - distri.ppf(lowTailPr)

    # find lowTailPr that minimizes intervalWidth
    HDIlowTailPr = fmin(intervalWidth, incredMass, ftol=1e-8, disp=False)[0]
    # return interval as array([low, high])
    return distri.ppf([HDIlowTailPr, credMass + HDIlowTailPr])


