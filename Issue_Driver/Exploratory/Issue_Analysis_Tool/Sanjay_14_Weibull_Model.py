#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 27 11:04:51 2018

@author: sanjay
"""


import pandas as pd
import numpy as np
from tqdm import tqdm
import pyarrow.parquet as pq
import matplotlib.pyplot as plt

plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

df = pd.read_csv("/Users/sanjay/Desktop/Solve Use Case/00 Data/egr_cooler_failures.csv")

df_sub = df[['ESN', 'Build date', 'Fail Date']]
df_sub = df_sub.rename(columns = {'Build date': 'BUILD_DATE', 'Fail Date': 'FAIL_DATE'})


df['Build date'] = pd.to_datetime(df['Build date'])
df['Fail Date'] = pd.to_datetime(df['Fail Date'])

df['Time to Failure'] = [(df['Fail Date'][x] - df['Build date'][x]).days for x in range(df.shape[0])]

df_sub.to_csv("/Users/sanjay/Desktop/Solve Use Case/00 Data/egr_cooler_failures.csv", index = False)

path = '/Users/sanjay/Desktop/reliability_engine_manish/'
    
build_population = pq.read_table(path).to_pandas()
online_tracker['ESN'] = online_tracker['ESN'].astype(int)

path = '/Users/sanjay/Downloads/ESN_level_table/'
build_population = pq.read_table(path).to_pandas()


build_population['engine_serial_num'] = build_population['engine_serial_num'].astype(int)

build_population.to_csv("/Users/sanjay/Desktop/build_population.csv", index = False)

df = pd.read_csv("/Users/sanjay/Desktop/turbo_sensor_esns.csv", names = ['ESN'])

online_tracker_sub = online_tracker.query("ESN in @df.ESN")

online_tracker_sub = online_tracker_sub[['ESN', 'REL_BUILD_DATE', 'EARLIEST_INDICATION_DATE']]


online_tracker_sub = online_tracker_sub.groupby('ESN').first().reset_index()

online_tracker_sub = online_tracker_sub.rename(columns = {'REL_BUILD_DATE': 'BUILD_DATE', \
                                     'EARLIEST_INDICATION_DATE': 'FAIL_DATE'})

online_tracker_sub.to_csv("/Users/sanjay/Desktop/Solve Use Case/00 Data/turbo_sensor_failures.csv", index = False)
