#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 15:32:23 2018

@author: sanjay
"""

import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
import mifs

plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None


## Load in EGR Failure and subset ##

egr_df = pd.read_csv("Desktop/ds/Solve/egr_cooler_labels.csv")
egr_df = egr_df.query("Population != 'X1'")
egr_df.columns.values[1] = 'ESN'
del egr_df['Population']
egr_df = egr_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
del egr_df['ESN']
y = egr_df.Label
del egr_df['Label']
del egr_df['INCDT_ISSUE_NUMBER']
#Remove duplicate columns
_, i = np.unique(egr_df.columns, return_index = True)
egr_df = egr_df.iloc[:,i]

#Scale numeric features
for col in tqdm(numeric_cols + mes_cols):
    finite_vals = egr_df[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        egr_df.iloc[finite_vals.index.values, np.where(egr_df.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    egr_df[col] = egr_df[col].replace(np.nan, 0)
    
## Straight Mutual Information ##
    
#Mask for categorical
cat_idx = [egr_df.columns.values[x].startswith(tuple(cat_cols)) for x in range(egr_df.shape[1])]
    
#Get Mutual information and save variables
mic = feature_selection.mutual_info_classif(egr_df, y, discrete_features = cat_idx)
var_sort = egr_df.columns.values[np.argsort(mic)[::-1]]

# Lasso Function #

def runLassoAndOutputFeatures(df, cols):
    
    if cols in ['MES']:
        pen = 'l1'
    if cols in ['INS', 'OEPL', 'REL']:
        pen = 'l2'

    clf = LogisticRegressionCV(Cs = 100, penalty = pen, class_weight = 'balanced', solver = 'liblinear')
    #clf = LogisticRegression(penalty = pen, class_weight = 'balanced', solver = 'liblinear')

    col_idx = [df.columns.values[x].startswith(cols) for x in range(df.shape[1])]
    clf.fit(df.iloc[:,col_idx], y)
    coef_sort_idxs = np.argsort(np.abs(clf.coef_[0]))[::-1]
    num_non_zero = np.sum(clf.coef_[0] != 0)
    top_cols = df.iloc[:,col_idx].columns.values[coef_sort_idxs[:num_non_zero]]
    top_col_coefs = clf.coef_[0][coef_sort_idxs[:num_non_zero]]
    for col, coef in zip(top_cols, top_col_coefs):
        print("{} : {}".format(col, coef))

cols = 'REL'
runLassoAndOutputFeatures(egr_df, cols)


## Re-Load EGR with no normalization ##

egr_df_all = pd.read_csv("Desktop/ds/Solve/egr_cooler_labels.csv")
egr_df_all = egr_df_all.query("Population != 'X1'")
egr_df_all.columns.values[1] = 'ESN'
del egr_df_all['Population']
egr_df_all = egr_df_all.merge(df_ot, on = 'ESN', how = 'inner').copy()
del egr_df_all['INCDT_ISSUE_NUMBER']

# Visualizations

#Remove duplicate rows for MES features
egr_df_sub = egr_df_all.drop_duplicates(subset = 'ESN')


col = 'INS_FS_INTAKE_AIR_THROTTLE_POSITION_PERCENT'
fig, ax = plt.subplots(figsize=(12, 8))
#egr_df_all[col][egr_df_all[col] == 0] = float('NaN')
ax.hist(egr_df_all.query("Label == 1")[col].dropna(),
            color='red', alpha=0.75, bins = 10, label = 'EGR Cooler Issue', normed=True)
ax.hist(egr_df_all.query("Label == 0")[col].dropna(),
            color='blue', alpha=0.75, bins = 10, label = 'NO ISSUE', normed=True)
ax.set(title='Histogram',
       ylabel='Count', xlabel = col)
plt.legend()
plt.show()

col = 'INS_EA_COOLANT_TMP_SEC_SEV_2'
#egr_df_all[col][egr_df_all[col] == 274] = float('NaN')
#egr_df_all[col][egr_df_all[col] == 605.25] = float('NaN')
#egr_df_all[col][egr_df_all[col] == 0] = float('NaN')
egr_df_all[col] = egr_df_all[col].astype(float)
egr_df_all[col][egr_df_all[col] < 0] = float('NaN')

egr_df_all.query("Label == 1")[col].dropna().value_counts()
egr_df_all

col = 'INS_FS_FAN_DRIVE_STATE_Off'
egr_df_sub.query("Label == 1")[col]

col = 'INS_FS_BRAKE_PEDAL_POSITION_SWITCH'
df_sub = fault_snap_new[['ESN','EARLIEST_INDICATION_DATE', col]]
df_sub = df_sub.merge(egr_df_all, on = ['ESN'], how = 'right')
cat_prop_plot(df_sub, col, "")

col = 'REL_OEM_GROUP'
df_sub = online_tracker[['ESN', col]]
df_sub = df_sub.merge(egr_df_all, on = ['ESN'], how = 'right')
cat_prop_plot(df_sub, col, "")

#Remove duplicate columns
_, i = np.unique(egr_df.columns, return_index = True)
egr_df = egr_df.iloc[:,i]

#Add in OEPL
df = pd.read_csv("Desktop/oepl/oepl_x15.csv")
df['option_count'] = 1
df_pivot = df.pivot_table(index = 'engine_serial_num', columns = 'option_assembly_num', \
                          values = 'option_count', aggfunc='mean').fillna(0).reset_index()
df_pivot = df_pivot.rename(columns = {'engine_serial_num': 'ESN'})
egr_df_sub = egr_df_all[['ESN', 'Label']]
egr_df_sub = egr_df_sub.merge(df_pivot, on = 'ESN', how = 'inner')
y = egr_df_sub['Label']
del egr_df_sub['Label'], egr_df_sub['ESN']
#Run Log Reg
clf = LogisticRegressionCV(Cs = 100, penalty = 'l2', class_weight = 'balanced', solver = 'liblinear')
    #clf = LogisticRegression(penalty = pen, class_weight = 'balanced', solver = 'liblinear')
clf.fit(egr_df_sub, y)
coef_sort_idxs = np.argsort(np.abs(clf.coef_[0]))[::-1]
num_non_zero = np.sum(clf.coef_[0] != 0)
top_cols = egr_df_sub.columns.values[coef_sort_idxs[:num_non_zero]]
top_col_coefs = clf.coef_[0][coef_sort_idxs[:num_non_zero]]
for col, coef in zip(top_cols, top_col_coefs):
    print("{} : {}".format(col, coef))


oepl_vars = ['SK1709', 'BR1762', 'ER1041', 'TH1712', 'LA1718', 'BB1950']
df_sub = df[['engine_serial_num', 'option_assembly_num']]
df_sub = df_sub.rename(columns = {'engine_serial_num': 'ESN'})
df_sub = df_sub.merge(egr_df_all, on = 'ESN', how = 'inner')
df_sub = df_sub.query("option_assembly_num in @oepl_vars")

cat_prop_plot(df_sub, 'option_assembly_num', "")








def cat_prop_plot(df1, var_name, var_desc, vline = None, sort=False, save = False):
   #Forest Plot for categorical variables 
  
   dfpp = df1.groupby(var_name).Label.agg({'beta_mode':np.mean, 'ww': np.sum,
                                           'Not-ww': lambda x: np.sum(1-x),
                                           'count': lambda x: np.shape(x)[0]})
   if sort == True:
       dfpp = dfpp.sort_values(by='beta_mode', ascending=False)
      
   names = dfpp.index.values
   p = len(names)
   space = 0.5
   fig, ax = plt.subplots(figsize=(8,(p+1)*0.5))
   for i in range(p):
       # parameters of beta distribution
       a = dfpp.iloc[i]['ww'] + 1
       b = dfpp.iloc[i]['Not-ww'] + 1
       # plot most probable value (the mode)
       mode = (a - 1)/(a + b - 2)
       ax.plot( 100*mode, [-space -i*space],  marker='o', markersize=5, color='blue')
       # Plot the High Posterior Density regions
       hpd50 = 100*HDIofICDF(beta, credMass=0.50, a=a, b=b)
       hpd95 = 100*HDIofICDF(beta, credMass=0.95, a=a, b=b)
       ax.errorbar(x=(hpd50[0],hpd50[1]), y=(-space-i*space,-space-i*space), color='blue', 
                               linewidth= 2)
       ax.errorbar(x=(hpd95[0],hpd95[1]), y=(-space-i*space,-space-i*space), color='blue', 
                       linewidth= 1)
      
   if vline:
       ax.axvline(vline, color='k', linestyle=':', alpha=0.7, linewidth=1, label='Overall % of EGR Issues')
   ax.set_ylim(-space*(p+1),0)
   ax.set_xlim(-0.02*100,1.02*100)
   ax.set_yticks(-space-1*np.arange(p)*space)
   ax.set_yticklabels(names)
   ax.set_ylabel('{}'.format(var_desc))
   ax.set_xlabel('% of EGR Issues');
   ax.set_title('% of EGR Issues by {}'.format(var_desc));
   ax.grid(False)
   if vline:
       ax.legend(loc = 0, fontsize = 'medium')
   if save:
       plt.savefig('Desktop/Solve_Results/Figs/{}.png'.format(var_name))
       
       
def HDIofICDF(dist_name, credMass=0.95, **args):
    # freeze distribution with given arguments
    distri = dist_name(**args)
    # initial guess for HDIlowTailPr
    incredMass =  1.0 - credMass

    def intervalWidth(lowTailPr):
        return distri.ppf(credMass + lowTailPr) - distri.ppf(lowTailPr)

    # find lowTailPr that minimizes intervalWidth
    HDIlowTailPr = fmin(intervalWidth, incredMass, ftol=1e-8, disp=False)[0]
    # return interval as array([low, high])
    return distri.ppf([HDIlowTailPr, credMass + HDIlowTailPr])


