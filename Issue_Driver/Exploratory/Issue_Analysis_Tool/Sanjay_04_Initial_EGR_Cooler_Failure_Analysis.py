#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 11:15:45 2018

@author: pp630
"""

import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
import mifs

plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

## Load in Online Incident Tracker ##

path = 'Desktop/QUALITY_FEATURES_PARQUET_NEW/'
end = '-218312c9-b902-43be-97b2-cc5573acc197.parquet'
pq.read_table(path + 'part-00000' + end)

file_1 = pq.read_table(path + 'part-00000' + end).\
    remove_column(11).to_pandas()
file_2 = pq.read_table(path + 'part-00001' + end).\
    remove_column(11).to_pandas()
file_3 = pq.read_table(path + 'part-00002' + end).\
    remove_column(11).to_pandas()
file_4 = pq.read_table(path + 'part-00003' + end).\
    remove_column(11).to_pandas()
file_5 = pq.read_table(path + 'part-00004' + end).\
    remove_column(11).to_pandas()
file_6 = pq.read_table(path + 'part-00005' + end).\
    remove_column(11).to_pandas()
file_7 = pq.read_table(path + 'part-00006' + end).\
    remove_column(11).to_pandas()
file_8 = pq.read_table(path + 'part-00007' + end).\
    remove_column(11).to_pandas()
online_tracker = pd.concat([file_1, file_2, file_3, file_4, file_5, file_6, file_7, file_8]).reset_index(drop = True)

## Extract Relevant Features ##

esn_col = ['ESN']
numeric_cols = ['REL_CMP_SUM_NET_AMOUNT', 'REL_LAGD', 'REL_LAGS','INS_EA_CRANKCASE_PRSR_SEC', 'INS_EA_COOLANT_TMP_SEC', \
                'INS_EA_OIL_PRSR_SEC', 'INS_EA_OIL_TMP_SEC', 'INS_EA_SPEED_SEC', 'INS_EA_MANIFOLD_AIR_TMP_SEC', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_TIME', 'INS_DC_CMP_PERCENT_ZERO_OR_NEGATIVE_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_LOW_TORQUE_TIME', 'INS_DC_CMP_PERCENT_LIGHT_LOAD_TIME', \
                'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', 'INS_DC_CMP_PERCENT_CLIMBING_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME', 'INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME', 'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME', \
                'INS_TI_AVERAGE_ENGINE_LOAD_RPM', 'INS_TI_AVERAGE_ENGINE_SPEED_RPM', 'INS_TI_AVERAGE_VEHICLE_SPEED_MILES', \
                'INS_TI_ECM_TIME_HOURS', 'INS_TI_ECM_TIME_KEY_ON_TIME_HOURS', 'INS_TI_ENGINE_BRAKE_DISTANCE_MILES', \
                'INS_TI_ENGINE_BRAKE_TIME_HOURS', 'INS_TI_ENGINE_DISTANCE_MILES', 'INS_TI_ENGINE_RUN_TIME_HOURS', \
                'INS_TI_FUEL_USED_GALLON', 'INS_TI_FULL_LOAD_OPERATION_TIME_HOURS', 'INS_TI_IDLE_FUEL_USED_GALLON', \
                'INS_TI_IDLE_TIME_HOURS']
good_cols = ['REL_IS_OFF_HIGHWAY']
cat_cols = ['REL_CMP_PROGRAM_GROUP_NAME', 'REL_ANALYSIS_RATE_CAT', 'REL_CPL_NUM', 'REL_DESIGN_HSP_NUM', 'REL_DESIGN_RPM_NUM', \
            'REL_ECM_PART_NO','REL_MKTG_HSP_NUM', 'REL_MKTG_RPM_NUM', 'REL_OEM_TYPE', \
            'REL_SERVICE_ENGINE_MODEL', 'REL_USER_APPL_DESC', 'REL_EMISSIONS_FAMILY_CODE', 'OEPL_COMPRESSOR_AIR_INLET_TYPE',\
            'OEPL_COMPRESSOR_TYPE', 'OEPL_THERMOSTAT_HOUSING', 'OEPL_CAPACITY_HIGH_LOW_RANGE_GALLONS', \
            'OEPL_POWER_ANGLE_RANGE_DEGREE', 'OEPL_PEAK_TORQUE', 'OEPL_HORSEPOWER']

manual_cols = ['OEPL_ST']

cols_to_split = ['CMP_ENGINE_MILES_LIST', 'GREEN_WRENCH_NUM_LIST']
extra_cols = ['INCDT_ISSUE_LABEL', 'CMP_FAIL_CODE_LIST']
additional_cols = ['REL_DO_OPTION', 'REL_FR_OPTION', 'REL_SC_OPTION'] #large categories..

# Get ESNs #

df_esn = online_tracker[['ESN']]
df_esn['ESN'] = df_esn['ESN'].astype(int)

# One-Hot Encoding for Categorical Features

df_cat = pd.DataFrame()

for col in cat_cols:    
    df_temp = pd.get_dummies(online_tracker[col])
    df_cat = pd.concat([df_cat, df_temp], axis = 1)

# Scale Numerical Features
df_num = online_tracker[numeric_cols]

    
# Manual Columns #

df_man = online_tracker[manual_cols]
df_man[manual_cols] = 0
df_man[online_tracker[manual_cols[0]] == True] = 1
    
# Split Cols on important vals #

df_ot = pd.concat([df_esn, df_cat, df_num, df_man], axis = 1)

## Get EGR Cooler ESNs ##

egr_df = pd.read_csv("Desktop/ds/Solve/egr_cooler_labels.csv")
egr_df.columns.values[1] = 'ESN'
del egr_df['Population']
egr_df = egr_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
egrdel egr_df['ESN']
y = egr_df.Label
del egr_df['Label']

feat = mifs.MutualInformationFeatureSelector()
feat.fit(egr_df, y.astype(int))

egr_df = copy.deepcopy(egr_df)
del egr_df[0]

for col in numeric_cols:
    finite_vals = egr_df[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        egr_df.iloc[finite_vals.index.values, np.where(egr_df.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    egr_df[col] = egr_df[col].replace(np.nan, 0)
    
    
## Categorical ##
    
egr_df_sub = egr_df[egr_df.columns.difference(numeric_cols)]
y = egr_df_sub['Label']
del egr_df_sub['Label']
p_vals = feature_selection.chi2(egr_df_sub, y)[1]
p_vals_argsort = np.argsort(p_vals)
num_nans = np.sum(np.isnan(p_vals))
top_cols = egr_df_sub.columns.values[p_vals_argsort[:(len(p_vals_argsort)-num_nans)]]


online_tracker_sub = online_tracker[cat_cols]
master_cols = list()
for col in top_cols[:20]:
    for col2 in online_tracker_sub.columns.values:
        if col in online_tracker_sub[col2].unique():
            master_cols.append(col2)
            
            
## Numeric ##

    
egr_df_sub = egr_df[numeric_cols + ['Label']]
y = egr_df_sub['Label']
del egr_df_sub['Label']

mic = feature_selection.mutual_info_classif(egr_df_sub, y)
np.sort(mic)

egr_df_sub.columns.values[np.argsort(mic)[::-1]]


egr_df_sub = egr_df[numeric_cols + ['Label']]

cols = ['REL_LAGD', 'REL_LAGS', 'REL_CMP_SUM_NET_AMOUNT']
for col in cols:
    print(egr_df[col].dropna().shape)
    fig, ax = plt.subplots(figsize=(12, 8))
    ax.hist(egr_df.query("Label == 1")[col].dropna(),
                color='lightblue', alpha=0.5, bins = 100, label = 'EGR Cooler Issue', normed=True)
    ax.hist(egr_df.query("Label == 0")[col].dropna(),
                color='orange', alpha=0.5, bins = 100, label = 'NO ISSUE', normed=True)
    ax.set(title='Histogram',
           ylabel='Count', xlabel = col)
    plt.legend()
    plt.show()






            

cols_for_R = ['X1 2017', 'X3 2017', 4343.0, 'X15 CM2350 X114B', 'X15 CM2350 X116B', 'No_Radiator_Limiter', 1800, 'Label']
egr_df_sub = egr_df[cols_for_R]
egr_df_sub.to_csv("~/Desktop/egr_cooler_sub.csv", index = False)
egr_df.to_csv("~/Desktop/egr_cooler_all.csv", index = False)


y = egr_df['Label']
del egr_df['Label']

df = pd.read_csv("~/Desktop/Sensor_Issue.csv")
y = df['Label']
del df['Label']
del df['ESN']

## Run Logistic Regression + L1 Penalization ##

clf = LogisticRegressionCV(penalty = 'l1', solver = 'liblinear')
clf = LogisticRegression(penalty = 'l2')

clf.fit(df, y)

print(df.columns.values[clf.coef_[0] != 0])

df_sub = df[['0','600','0.1','1800','ISX15 CM2350 X101', 'Label']]
df_sub.to_csv("~/Desktop/Sensor_Issue_sub.csv", index = False)


clf.coef_


print(clf.coef_[0][clf.coef_[0] != 0])
print(egr_df.columns.values[clf.coef_[0] != 0])


egr_df[cols_for_R]


selector = feature_selection.RFE(clf, 1, verbose = 1)

selector.fit(egr_df, y)

selector.score(egr_df, y)
sort_of_coefs = np.argsort(selector.ranking_)

egr_df.columns.values[sort_of_coefs]

egr_df.columns.values[selector.ranking_-1]

skb = feature_selection.SelectKBest()

egr_df[]

res = feature_selection.mutual_info_classif(egr_df, y)


plt.plot(np.sort(res)[::-1][:10])
plt.ylabel("MIC Score")
plt.xlabel("Coefficient Ranking")
plt.title("Ranking of MIC Scores (Zoomed In)")

np.sort(res)

plt.plot(np.sort(res)[::-1])
plt.ylabel("MIC Score")
plt.xlabel("Coefficient Ranking")
plt.title("Ranking of MIC Scores")


##################'

# (1) Chi Squared Test to Rank Categorical variables
# (2) MIC to Rank Numerical Variables
# (2) Forest Plot of Top 3-5 Ranked Variables


df_esn = online_tracker[['ESN']]
df_esn['ESN'] = df_esn['ESN'].astype(int)
df_sub = online_tracker[cat_cols]

df_ot = pd.concat([df_esn, df_sub], axis = 1)




egr_df = pd.read_csv("Documents/OneDrive - Cummins/Solve Use Case/00 Data/EGR_Cooler_ESNs.csv")
del egr_df['Population']
egr_df = egr_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
del egr_df['ESN']

egr_df = pd.read_csv("Documents/OneDrive - Cummins/Solve Use Case/00 Data/EGR_Cooler_ESNs.csv")
del egr_df['Population']
online_tracker['ESN'] = online_tracker['ESN'].astype(int)
egr_df = egr_df.merge(online_tracker, on = 'ESN', how = 'inner').copy()
del egr_df['ESN']


egr_df.Label.mean()
master_cols = ['REL_EMISSIONS_FAMILY_CODE', 'REL_MKTG_RPM_NUM','REL_ANALYSIS_RATE_CAT', \
               'REL_MKTG_HSP_NUM', 'OEPL_THERMOSTAT_HOUSING', 'REL_CPL_NUM']
master_desc = ['Emission Category', 'RPM Category', 'Engine Type', 'Horsepower', \
               'Thermostat Housing', 'Emission Level']

master_cols = list(master_cols)
for col, desc in zip(master_cols, master_desc):
    cat_prop_plot(egr_df, col, desc, 44.3)



def cat_prop_plot(df1, var_name, var_desc, vline = None, sort=False):
   #Forest Plot for categorical variables 
  
   dfpp = df1.groupby(var_name).Label.agg({'beta_mode':np.mean, 'ww': np.sum,
                                           'Not-ww': lambda x: np.sum(1-x),
                                           'count': lambda x: np.shape(x)[0]})
   if sort == True:
       dfpp = dfpp.sort_values(by='beta_mode', ascending=False)
      
   names = dfpp.index.values
   p = len(names)
   space = 0.5
   fig, ax = plt.subplots(figsize=(8,(p+1)*0.5))
   for i in range(p):
       # parameters of beta distribution
       a = dfpp.iloc[i]['ww'] + 1
       b = dfpp.iloc[i]['Not-ww'] + 1
       # plot most probable value (the mode)
       mode = (a - 1)/(a + b - 2)
       ax.plot( 100*mode, [-space -i*space],  marker='o', markersize=5, color='blue')
       # Plot the High Posterior Density regions
       hpd50 = 100*HDIofICDF(beta, credMass=0.50, a=a, b=b)
       hpd95 = 100*HDIofICDF(beta, credMass=0.95, a=a, b=b)
       ax.errorbar(x=(hpd50[0],hpd50[1]), y=(-space-i*space,-space-i*space), color='blue', 
                               linewidth= 2)
       ax.errorbar(x=(hpd95[0],hpd95[1]), y=(-space-i*space,-space-i*space), color='blue', 
                       linewidth= 1)
      
   if vline:
       ax.axvline(vline, color='k', linestyle=':', alpha=0.7, linewidth=1, label='Overall % of EGR Issues')
   ax.set_ylim(-space*(p+1),0)
   ax.set_xlim(-0.02*100,1.02*100)
   ax.set_yticks(-space-1*np.arange(p)*space)
   ax.set_yticklabels(names)
   ax.set_ylabel('{}'.format(var_desc))
   ax.set_xlabel('% of EGR Issues');
   ax.set_title('% of EGR Issues by {}'.format(var_desc));
   ax.grid(False)
   if vline:
       ax.legend(loc = 0, fontsize = 'medium')
       
       
def HDIofICDF(dist_name, credMass=0.95, **args):
    # freeze distribution with given arguments
    distri = dist_name(**args)
    # initial guess for HDIlowTailPr
    incredMass =  1.0 - credMass

    def intervalWidth(lowTailPr):
        return distri.ppf(credMass + lowTailPr) - distri.ppf(lowTailPr)

    # find lowTailPr that minimizes intervalWidth
    HDIlowTailPr = fmin(intervalWidth, incredMass, ftol=1e-8, disp=False)[0]
    # return interval as array([low, high])
    return distri.ppf([HDIlowTailPr, credMass + HDIlowTailPr])





df_sub = online_tracker[['ESN', 'INCDT_ISSUE_NUMBER', 'INCDT_ISSUE_LABEL']]
df_sub.groupby('INCDT_ISSUE_NUMBER').count().reset_index(drop = False).sort_values('ESN', ascending = False)

df_sub = online_tracker.query("INCDT_ISSUE_NUMBER == 'ho284-25600'")[['ESN', 'REL_CMP_FAIL_CODE_LIST']]


fc = list()
for l in df_sub.REL_CMP_FAIL_CODE_LIST:
    if l is not None:
        for i in l:
            fc.append(i)
            
from collections import Counter
Counter(fc)

df_sub








