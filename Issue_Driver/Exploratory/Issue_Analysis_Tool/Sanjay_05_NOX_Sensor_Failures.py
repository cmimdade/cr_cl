#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 11:45:12 2018

@author: sanjay
"""


plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
import mifs


#Subset on turbo speed replacement issue
proj_num = ['km576-27407','ou006-27316','esc_500','tbd_34','ia987-26584','nl145-25958']
df_issue = df_ot.query("INCDT_ISSUE_NUMBER in @proj_num")
df_issue['Label'] = 1

#Get non-issues
df_not_issue = df_ot.query("INCDT_ISSUE_NUMBER == 'not_issue'")
df_temp = online_tracker.query("INCDT_ISSUE_NUMBER == 'not_issue'")[['ESN', 'REL_CMP_FAIL_CODE_LIST']].reset_index(drop = True)
none_idx = [df_temp.REL_CMP_FAIL_CODE_LIST[x] is None for x in range(df_temp.shape[0])]
df_temp = df_temp[none_idx]
df_not_issue = df_not_issue.query('ESN in @df_temp.ESN')
#df_not_issue = df_not_issue.sample(100)
df_not_issue['Label'] = 0

#Concatenate issue and non-issue
df_all = pd.concat([df_issue, df_not_issue]).reset_index(drop = True)
y = df_all['Label']
del df_all['Label'], df_all['INCDT_ISSUE_NUMBER'], df_all['ESN']


#Scale numeric features
for col in tqdm(numeric_cols + mes_cols):
    if df_all[col].dtype != 'float64':
        df_all[col] = df_all[col].str.replace(',','.')
    finite_vals = df_all[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        df_all.iloc[finite_vals.index.values, np.where(df_all.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df_all[col] = df_all[col].replace(np.nan, 0)
    

runLassoAndOutputFeatures(df_all, 'INS')



# Remove all but ESN
df_all_and_esn = pd.concat([df_issue, df_not_issue]).reset_index(drop = True)
for col in tqdm(numeric_cols + mes_cols):
    if df_all_and_esn[col].dtype != 'float64':
        df_all_and_esn[col] = df_all_and_esn[col].str.replace(',','.')
    df_all_and_esn[col] = df_all_and_esn[col].astype(float)
    
col = 'REL_ECM_PART_NO'
df_sub = online_tracker[['ESN', col]]
df_sub = df_sub.merge(df_all_and_esn, on = ['ESN'], how = 'right')
cat_prop_plot(df_sub, col, col, df_all_and_esn.Label.mean()*100)


col = 'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME'
fig, ax = plt.subplots(figsize=(12, 8))
#egr_df_all[col][egr_df_all[col] == 0] = float('NaN')
ax.hist(df_all_and_esn.query("Label == 1")[col].dropna(),
            color='red', alpha=0.75, bins = 10, label = 'Issue', normed=True)
ax.hist(df_all_and_esn.query("Label == 0")[col].dropna(),
            color='blue', alpha=0.75, bins = 10, label = 'NO ISSUE', normed=True)
ax.set(title='Histogram',
       ylabel='Count', xlabel = col)
plt.legend()
plt.show()

col = 'INS_FS_ENGINE_OPERATING_STATE'
df_sub = fault_snap_new[['ESN', col]]
df_sub = df_sub.merge(df_all_and_esn, on = ['ESN'], how = 'right')
cat_prop_plot(df_sub, col, col, df_all_and_esn.Label.mean()*100)


