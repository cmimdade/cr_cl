INITIAL SCRIPTS

The first set of scripts are mostly from my first few weeks, doing some analysis on team composition and beginning to form the ideas behind issue driver.

Sanjay_01_Cleaning_and_EDA.ipynb - Initial cleaning of PPS data script (NOT NEEDED)
Sanjay_02_Team_Composition.py - Initial analysis of team composition (NOT NEEDED)
Sanjay_03_EGR_Cooler_Analysis.ipynb - Very first EGR cooler analysis with positive and negative labels given to us
Sanjay_04_Initial_EGR_Cooler_Failure_Analysis.py - More EGR cooler analysis/ code I played with. Not well commented
Sanjay_05_More_EGR_Cooler_Failure.py - Start building univariate model. Normalize numerical features and run lasso/ridge + logistic regression
Sanjay_06_Sensor_Issue_Analysis.py - Same as above, but with Sensor issue with unique Fail Code


ISSUE DRIVER SCRIPTS

Sanjay_00_Load_and_Subset_Data.py - this is the MOST IMPORTANT script for now, since it contains all the pre-processing done on the data, separates categorical and numerical data, and builds a final data frame. This whole script may take 15-30 mins to run initially, but I saved the main data frame in one drive. The data come from a variety of sources (Detect Master table, Fault Snapshot, OEPL options)
Sanjay_01_Find_Negative_Set.py - Found most likely negatives using Spy algorithm. Unfortunately, this only works well if features are dense and predictive.
Sanjay_02_MIC_and_JMI.py - Applying Mutual Information and (Joint) MI to univariate analysis
Sanjay_03_OEPL_EDA.py - Extracting the OEPL options associated with the detect master table incidents.
Sanjay_04_Turbo_Speed_Issue.py - Full univariate analysis on Turbo Speed issue
Sanjay_05_NOX_Sensor_Failures.py - Full univariate analysis on NOx Sensor issue
Sanjay_06_EGR_Cooler_Failure.py - Full univariate analysis on EGR Cooler Issue

Sanjay_07_Build_Excel_For_EGR.py - Build excel doc for EGR issue
Sanjay_08_Build_Excel_For_Turbo.py - Build excel doc for Turbo Issue
Sanjay_XX_Test_Script.py - Test script to play in