#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 10:16:24 2018

@author: sanjay
"""


plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
import mifs

#Subset on turbo speed replacement issue
proj_num = ['lo401-28283', 'esc_518', '504507', 'ia982-28026', 'TBD_687']
df_issue = df_ot.query("INCDT_ISSUE_NUMBER in @proj_num")
df_issue['Label'] = 1

#Get non-issues
df_not_issue = df_ot.query("INCDT_ISSUE_NUMBER == 'not_issue'")
df_temp = online_tracker.query("INCDT_ISSUE_NUMBER == 'not_issue'")[['ESN', 'REL_CMP_FAIL_CODE_LIST']].reset_index(drop = True)
none_idx = [df_temp.REL_CMP_FAIL_CODE_LIST[x] is None for x in range(df_temp.shape[0])]
df_temp = df_temp[none_idx]
df_not_issue = df_not_issue.query('ESN in @df_temp.ESN')
df_not_issue['Label'] = 0
#df_not_issue = df_not_issue.query("REL_BUILD_YEAR_MONTH_2017_7 == 1").query("REL_BUILD_YEAR_MONTH_2017_7 == 1")

#Concatenate issue and non-issue
df_all = pd.concat([df_issue, df_not_issue]).reset_index(drop = True)
y = df_all['Label']
del df_all['Label'], df_all['INCDT_ISSUE_NUMBER'], df_all['ESN']

#Scale numeric features
for col in tqdm(numeric_cols + mes_cols):
    if df_all[col].dtype != 'float64':
        df_all[col] = df_all[col].str.replace(',','.')
    finite_vals = df_all[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        df_all.iloc[finite_vals.index.values, np.where(df_all.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df_all[col] = df_all[col].replace(np.nan, 0)
    
    
## MIC ##
    
#Mask for categorical
cat_idx = [df_all.columns.values[x].startswith(tuple(cat_cols)) for x in range(df_all.shape[1])]
    
#Get Mutual information and save variables
mic = feature_selection.mutual_info_classif(df_all, y, discrete_features = cat_idx)
var_sort = df_all.columns.values[np.argsort(mic)[::-1]]

## LASSO/ RIDGE ##

def runLassoAndOutputFeatures(df, cols):
    
    if cols in ['MES']:
        pen = 'l1'
    if cols in ['INS', 'OEPL', 'REL']:
        pen = 'l2'

    clf = LogisticRegressionCV(Cs = 100, penalty = pen, class_weight = 'balanced', solver = 'liblinear')
    #clf = LogisticRegression(penalty = pen, class_weight = 'balanced', solver = 'liblinear')

    col_idx = [df.columns.values[x].startswith(cols) for x in range(df.shape[1])]
    clf.fit(df.iloc[:,col_idx], y)
    coef_sort_idxs = np.argsort(np.abs(clf.coef_[0]))[::-1]
    num_non_zero = np.sum(clf.coef_[0] != 0)
    top_cols = df.iloc[:,col_idx].columns.values[coef_sort_idxs[:num_non_zero]]
    top_col_coefs = clf.coef_[0][coef_sort_idxs[:num_non_zero]]
    for col, coef in zip(top_cols, top_col_coefs):
        print("{} : {}".format(col, coef))

runLassoAndOutputFeatures(df_all, 'INS')


# Remove all but ESN
df_all_and_esn = pd.concat([df_issue, df_not_issue]).reset_index(drop = True)
for col in tqdm(numeric_cols + mes_cols):
    if df_all_and_esn[col].dtype != 'float64':
        df_all_and_esn[col] = df_all_and_esn[col].str.replace(',','.')
    df_all_and_esn[col] = df_all_and_esn[col].astype(float)
        
col = 'REL_OEM_GROUP'
df_sub = fat_table[['ESN', col]]
df_sub = df_sub.merge(df_all_and_esn, on = ['ESN'], how = 'right')
cat_prop_plot(df_sub, col, col, df_all_and_esn.Label.mean()*100)


col = 'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE_FAHRENHEIT'
fig, ax = plt.subplots(figsize=(12, 8))
#egr_df_all[col][egr_df_all[col] == 0] = float('NaN')
ax.hist(df_all_and_esn.query("Label == 1")[col].dropna(),
            color='red', alpha=0.75, bins = 10, label = 'Issue', normed=True)
ax.hist(df_all_and_esn.query("Label == 0")[col].dropna(),
            color='blue', alpha=0.75, bins = 10, label = 'NO ISSUE', normed=True)
ax.set(title='Histogram',
       ylabel='Count', xlabel = col)
plt.legend()
plt.show()

col = 'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME'
df_sub = fault_snap_new[['ESN', col]]
df_sub = df_sub.merge(df_all_and_esn, on = ['ESN'], how = 'right')
cat_prop_plot(df_sub, col, col, df_all_and_esn.Label.mean()*100)



def cat_prop_plot(df1, var_name, var_desc, vline = None, sort=False):
   #Forest Plot for categorical variables 
  
   dfpp = df1.groupby(var_name).Label.agg({'beta_mode':np.mean, 'ww': np.sum,
                                           'Not-ww': lambda x: np.sum(1-x),
                                           'count': lambda x: np.shape(x)[0]})
   if sort == True:
       dfpp = dfpp.sort_values(by='beta_mode', ascending=False)
      
   names = dfpp.index.values
   p = len(names)
   space = 0.5
   fig, ax = plt.subplots(figsize=(8,(p+1)*0.5))
   for i in range(p):
       # parameters of beta distribution
       a = dfpp.iloc[i]['ww'] + 1
       b = dfpp.iloc[i]['Not-ww'] + 1
       # plot most probable value (the mode)
       mode = (a - 1)/(a + b - 2)
       ax.plot( 100*mode, [-space -i*space],  marker='o', markersize=5, color='blue')
       # Plot the High Posterior Density regions
       hpd50 = 100*HDIofICDF(beta, credMass=0.50, a=a, b=b)
       hpd95 = 100*HDIofICDF(beta, credMass=0.95, a=a, b=b)
       ax.errorbar(x=(hpd50[0],hpd50[1]), y=(-space-i*space,-space-i*space), color='blue', 
                               linewidth= 2)
       ax.errorbar(x=(hpd95[0],hpd95[1]), y=(-space-i*space,-space-i*space), color='blue', 
                       linewidth= 1)
      
   if vline:
       ax.axvline(vline, color='k', linestyle=':', alpha=0.7, linewidth=1, label='Overall % of Issues')
   ax.set_ylim(-space*(p+1),0)
   ax.set_xlim(-0.02*100,1.02*100)
   ax.set_yticks(-space-1*np.arange(p)*space)
   ax.set_yticklabels(names)
   ax.set_ylabel('{}'.format(var_desc))
   ax.set_xlabel('% of Issues');
   ax.set_title('% of Issues by {}'.format(var_desc));
   ax.grid(False)
   if vline:
       ax.legend(loc = 0, fontsize = 'medium')
       
       
def HDIofICDF(dist_name, credMass=0.95, **args):
    # freeze distribution with given arguments
    distri = dist_name(**args)
    # initial guess for HDIlowTailPr
    incredMass =  1.0 - credMass

    def intervalWidth(lowTailPr):
        return distri.ppf(credMass + lowTailPr) - distri.ppf(lowTailPr)

    # find lowTailPr that minimizes intervalWidth
    HDIlowTailPr = fmin(intervalWidth, incredMass, ftol=1e-8, disp=False)[0]
    # return interval as array([low, high])
    return distri.ppf([HDIlowTailPr, credMass + HDIlowTailPr])