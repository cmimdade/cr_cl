#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  5 15:32:34 2018

@author: sanjay
"""


import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
import mifs
import statsmodels.api as sm
import scipy.stats as stat
from sklearn.preprocessing import PolynomialFeatures
from scipy.sparse import csr_matrix
import seaborn as sns
from sklearn.ensemble import RandomForestClassifier
from tqdm import tqdm

def fitModelsAndOutput(df, cols, issue_type, df_all_not_normalized):
    
    #Create Random Forest Model Object
    clf = RandomForestClassifier(n_estimators = 1000, class_weight = 'balanced', n_jobs = 10, max_features = None)
    
    #Subset columns starting with 'cols' variable (eg: MES, REL, INS, etc.)
    if cols == 'OPT':
        col_idx = np.repeat(True, df.shape[1])
        col_idx[0] = False
        df_all_not_normalized = df

    else:
        col_idx = [df.columns.values[x].startswith(cols) for x in range(df.shape[1])]
    
    #Fit RF Model!
    clf.fit(df.iloc[:,col_idx], df.Label)
    print("Random Forest Classifier Fit")
    
    #Get top columns and scores
    top_cols_idx = np.argsort(clf.feature_importances_)[::-1]
    top_cols_score = np.sort(clf.feature_importances_)[::-1]
    top_cols = df.iloc[:,col_idx].columns.values[top_cols_idx]
        
    #Create Logistic Regression Model Object
    clf = LogisticRegressionCV(Cs = 100, penalty = 'l2', class_weight = 'balanced', solver = 'liblinear')
    
    #Fit LR Model!
    clf.fit(df.iloc[:,col_idx], df.Label)
    print("Logistic Regression Classifier Fit")
    
    #Find directionality of coefficients
    #dir_of_coefs = np.sign(clf.coef_[0]).astype(str)
    #dir_of_coefs[dir_of_coefs == "1.0"] = '+'
    #dir_of_coefs[dir_of_coefs == "-1.0"] = '-'
    #dir_of_coefs[dir_of_coefs == "0.0"] = 'NA'
    dir_of_coefs = clf.coef_[0]

    #num_issue = [df_all_not_normalized[df.Label==1][x].dropna().count() for x in df.iloc[:,col_idx].columns.values]
    #num_not_issue = [df_all_not_normalized[df.Label==0][x].dropna().count() for x in df.iloc[:,col_idx].columns.values]
    
    col_type = ["Numerical" if df.iloc[:,col_idx].columns.values[x] in \
                numeric_cols + mes_cols else "Categorical" for x in \
                range(len(df.iloc[:,col_idx].columns.values))]
    if cols == 'OPT':
        col_type = 'Categorical'
    
    print("Summary Statistics Found")


    df_scores = pd.DataFrame({'Variable': top_cols, 'Importance Score': top_cols_score})
    df_dir = pd.DataFrame({'Variable': df.iloc[:,col_idx].columns.values, \
                           'Directionality': dir_of_coefs, \
                           'Type': col_type})
    df_dir['Median of Issues'] = float('NaN')
    df_dir['Median of Non-Issues'] = float('NaN')
    df_dir['Mean of Issues'] = float('NaN')
    df_dir['Mean of Non-Issues'] = float('NaN')
    df_dir['SD of Issues'] = float('NaN')
    df_dir['SD of Non-Issues'] = float('NaN')
    df_dir['Categorical Incidence Rate'] = float('NaN')
    df_dir['Categorical Non-Incidence Rate'] = float('NaN')
    df_dir['Sample Size of Issues'] = float('NaN')
    df_dir['Sample Size of Non-Issues'] = float('NaN')
    
    for i in range(df_dir.shape[0]):
        if df_dir.Type[i] == "Categorical":
            df_dir['Categorical Incidence Rate'][i] = df_all_not_normalized[df.Label == 1][df_dir.Variable[i]].dropna().mean()
            #df_all_not_normalized[df_all_not_normalized[df_dir.Variable[i]] == 1].Label.mean()
            #df_dir['Categorical Non-Incidence Rate'][i] = 1 - df_all_not_normalized[df_all_not_normalized[df_dir.Variable[i]] == 1].Label.mean()
        if df_dir.Type[i] == "Numerical":
            df_dir['Median of Issues'][i] = df_all_not_normalized[df.Label==1][df_dir.Variable[i]].dropna().median()
            df_dir['Median of Non-Issues'][i] = df_all_not_normalized[df.Label==0][df_dir.Variable[i]].dropna().median()
            df_dir['Mean of Issues'][i] = df_all_not_normalized[df.Label==1][df_dir.Variable[i]].dropna().mean()
            df_dir['Mean of Non-Issues'][i] = df_all_not_normalized[df.Label==0][df_dir.Variable[i]].dropna().mean()
            df_dir['SD of Issues'][i] = np.sqrt(df_all_not_normalized[df.Label==0][df_dir.Variable[i]].dropna().var())
            df_dir['SD of Non-Issues'][i] = np.sqrt(df_all_not_normalized[df.Label==0][df_dir.Variable[i]].dropna().var())
        df_dir['Sample Size of Issues'][i] = df_all_not_normalized[df.Label==1][df_dir.Variable[i]].dropna().count()
        df_dir['Sample Size of Non-Issues'][i] = df_all_not_normalized[df.Label==0][df_dir.Variable[i]].dropna().count()

    
    df_final = df_scores.merge(df_dir, on = 'Variable', how = 'left')
    
    #df_final.to_csv("Desktop/Solve_Results/variable_results_{}_{}.csv".format(issue_type, cols), index = False)
    print("Results saved")
    return df_final

df_ot = pd.read_csv("Desktop/fat_table_with_fs.csv")

#Load new EGR Info
egr_df = pd.read_csv("Desktop/egr_cooler_updated_esns.csv", names = ['ESN'])
egr_df = egr_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
#del egr_df['ESN']
#del egr_df['INCDT_ISSUE_NUMBER']
egr_df['Label'] = 1

#Get non-issues
df_not_issue = df_ot.query("INCDT_ISSUE_NUMBER == 'not_issue'")
df_temp = online_tracker.query("INCDT_ISSUE_NUMBER == 'not_issue'")[['ESN', 'REL_CMP_FAIL_CODE_LIST']].reset_index(drop = True)
none_idx = [df_temp.REL_CMP_FAIL_CODE_LIST[x] is None for x in range(df_temp.shape[0])]
df_temp = df_temp[none_idx]
df_not_issue = df_not_issue.query('ESN in @df_temp.ESN')
df_not_issue = df_not_issue[df_not_issue["REL_ANALYSIS_RATE_CAT_X1 2017"] != 1]

df_not_issue['Label'] = 0

#Concatenate issue and non-issue
df_all = pd.concat([egr_df, df_not_issue]).reset_index(drop = True)

df = pd.read_csv("Desktop/oepl/oepl_x15.csv")
df['option_count'] = 1
df_pivot = df.pivot_table(index = 'engine_serial_num', columns = 'option_assembly_num', \
                          values = 'option_count', aggfunc='mean').fillna(0).reset_index()
df_pivot = df_pivot.rename(columns = {'engine_serial_num': 'ESN'})
egr_df_sub = df_all[['ESN', 'Label']]
egr_df_sub = egr_df_sub.merge(df_pivot, on = 'ESN', how = 'inner')
egr_df_sub = egr_df_sub.drop_duplicates().reset_index(drop = True)
del egr_df_sub['ESN']
del df_all['INCDT_ISSUE_NUMBER'], df_all['ESN']

#Scale numeric features
for col in tqdm(numeric_cols + mes_cols):
    if df_all[col].dtype != 'float64':
        df_all[col] = df_all[col].str.replace(',','.')
        df_all[col] = df_all[col].astype(float)
        df_all[col][df_all[col] < 0] = float('NaN')
        df_all[col][df_all[col] > 10**6] = float('NaN')
    finite_vals = df_all[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        df_all.iloc[finite_vals.index.values, np.where(df_all.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df_all[col] = df_all[col].replace(np.nan, 0)
    
    

df_all_not_normalized = pd.concat([egr_df, df_not_issue]).reset_index(drop = True)

for col in tqdm(numeric_cols + mes_cols):
    if df_all_not_normalized[col].dtype != 'float64':
        df_all_not_normalized[col] = df_all_not_normalized[col].str.replace(',','.')
        df_all_not_normalized[col] = df_all_not_normalized[col].astype(float)
    df_all_not_normalized[col][df_all_not_normalized[col] < 0] = float('NaN')
    df_all_not_normalized[col][df_all_not_normalized[col] > 10**6] = float('NaN')

df_ins = fitModelsAndOutput(df_all, 'INS', 'egr', df_all_not_normalized)
df_rel = fitModelsAndOutput(df_all, 'REL', 'egr', df_all_not_normalized)
df_oepl = fitModelsAndOutput(df_all, 'OEPL', 'egr', df_all_not_normalized)
df_mes = fitModelsAndOutput(df_all, 'MES', 'egr', df_all_not_normalized)
df_options = fitModelsAndOutput(egr_df_sub, 'OPT', 'egr', df_all_not_normalized)

writer = pd.ExcelWriter('Desktop/Solve_Results/variable_results_egr_cooler.xlsx', engine='xlsxwriter')

# Write each dataframe to a different worksheet.
df_ins.to_excel(writer, sheet_name='INS', index = False)
df_rel.to_excel(writer, sheet_name='REL', index = False)
df_oepl.to_excel(writer, sheet_name='OEPL', index = False)
df_mes.to_excel(writer, sheet_name='MES', index = False)
df_options.to_excel(writer, sheet_name='OPT', index = False)


# Close the Pandas Excel writer and output the Excel file.
writer.save()

##Get Visualizations##

def saveNumericPlot(col):
    df_all_not_normalized[col][df_all_not_normalized[col] < 0] = float('NaN')
    df_all_not_normalized[col][df_all_not_normalized[col] > 10**6] = float('NaN')

    fig, ax = plt.subplots(figsize=(12, 8))
    hist, bins = np.histogram(df_all_not_normalized.query("Label == 1")[col].dropna().astype(float), bins = 10)
    ax.bar(bins[:-1], hist.astype(np.float32)*100 / hist.sum(), width=(bins[1]-bins[0]), \
           color='red', alpha = 0.75, label = 'EGR Cooler Issue')
    hist, bins = np.histogram(df_all_not_normalized.query("Label == 0")[col].dropna().astype(float), bins = 10)
    ax.bar(bins[:-1], hist.astype(np.float32)*100 / hist.sum(), width=(bins[1]-bins[0]), \
           color='blue', alpha = 0.75, label = 'No Issue')
    ax.set(title='Histogram',
           ylabel='Percentage (%)', xlabel = col)
    plt.legend()
    plt.savefig('Desktop/Solve_Results/Figs/{}.png'.format(col), bbox_inches="tight") 
    
    
def saveCatPlot(col):
    df_sub = fat_table[['ESN','EARLIEST_INDICATION_DATE', col]]
    df_sub = df_sub.merge(df_all, on = ['ESN'], how = 'right')
    cat_prop_plot(df_sub, col, col, vline = df_sub.Label.mean()*100)
    
#Concatenate issue and non-issue
df_all = pd.concat([egr_df, df_not_issue]).reset_index(drop = True)

#Numerical
saveNumericPlot('INS_EA_OIL_TMP_SEC_SEV_TOTAL')
saveNumericPlot('INS_EA_OIL_TMP_SEC_SEV_1')
saveNumericPlot('INS_EA_OIL_PRSR_SEC_SEV_3')
saveNumericPlot('INS_DC_CMP_PERCENT_CRUISE_MODE_TIME')
saveNumericPlot('INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE_FAHRENHEIT')
saveNumericPlot('INS_EA_OIL_PRSR_SEC_SEV_2')
saveNumericPlot('INS_EA_OIL_PRSR_SEC_SEV_TOTAL')
saveNumericPlot('INS_EA_COOLANT_TMP_SEC_SEV_TOTAL')
saveNumericPlot('INS_EA_COOLANT_TMP_SEC_SEV_1')
saveNumericPlot('INS_FS_AFTERTREATMENT_OUTLET_NOX_CORRECTED_PPM')
saveNumericPlot('INS_FS_ENGINE_SPEED_RPM')
saveNumericPlot('INS_EA_OIL_PRSR_SEC_SEV_1')
saveNumericPlot('INS_TI_FUEL_USED_GALLON')
saveNumericPlot('INS_EA_COOLANT_TMP_SEC_SEV_2')
saveNumericPlot('INS_EA_CRANKCASE_PRSR_SEC_SEV_1')
saveNumericPlot('INS_TI_FULL_LOAD_OPERATION_TIME_HOURS')
saveNumericPlot('INS_FS_TURBOCHARGER_SPEED_RPM')
saveNumericPlot('INS_TI_ENGINE_BRAKE_TIME_HOURS')
saveNumericPlot('INS_EA_CRANKCASE_PRSR_SE_SEV_2C')

saveNumericPlot('MES_TSH_CMP_NUM_OF_STATES_TRANSITION')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_022_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_013_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_067_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN__1_AUD_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_004_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_024_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_016_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_010_MIN')

saveNumericPlot('REL_CMP_SUM_NET_AMOUNT')
saveNumericPlot('REL_LAGS')
saveNumericPlot('REL_LAGD')



#Categorical
saveCatPlot('REL_MKTG_RPM_NUM')
saveCatPlot('REL_BUILD_YEAR_MONTH')
saveCatPlot('REL_OEM_GROUP')
saveCatPlot('REL_EMISSIONS_FAMILY_CODE')

saveCatPlot('OEPL_THERMOSTAT_HOUSING')
saveCatPlot('OEPL_POWER_ANGLE_RANGE_DEGREE')
saveCatPlot('OEPL_COMPRESSOR_TYPE')
saveCatPlot('OEPL_POWER_ANGLE_RANGE_DEGREE')
saveCatPlot('OEPL_CAPACITY_HIGH_LOW_RANGE_GALLONS')

oepl_vars_1 = ['WI1125','WI1127','WI1096']          
oepl_vars_2 = ['TH1712','TH1724','TH1217','TH1222','TH1218']
oepl_vars_3 = ['FF1394','FF1397','FF1369']
oepl_vars_4 = ['RL1754','RL1771']
oepl_vars_5 = ['SK1001','SK1709','SK1710', 'SK1018', 'SK1025', 'SK1705']

var = ['Water Inlet Connection', 'Thermostat Housing', 'Fuel Filter', 'Rocker Lever', 'Shipping Bracket']


df_oepl = pd.read_csv("Desktop/oepl/oepl_x15.csv")
df_sub = df_oepl[['engine_serial_num', 'option_assembly_num']]
df_sub = df_sub.rename(columns = {'engine_serial_num': 'ESN'})
df_sub = df_sub.merge(df_all, on = 'ESN', how = 'inner')

for i in range(1, 6):
    df_options = df_sub.query("option_assembly_num in @oepl_vars_{}".format(i))
    cat_prop_plot(df_options, 'option_assembly_num', var[i-1], df_all.Label.mean()*100)


