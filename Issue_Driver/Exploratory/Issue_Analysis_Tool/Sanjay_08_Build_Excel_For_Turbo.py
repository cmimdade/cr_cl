#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 16:07:28 2018

@author: sanjay
"""

import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
import mifs

plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

#Subset on turbo speed replacement issue
proj_num = ['lo401-28283', 'esc_518', '504507', 'ia982-28026', 'TBD_687']
df_issue = df_ot.query("INCDT_ISSUE_NUMBER in @proj_num")
df_issue['Label'] = 1

#Get non-issues
df_not_issue = df_ot.query("INCDT_ISSUE_NUMBER == 'not_issue'")
df_temp = online_tracker.query("INCDT_ISSUE_NUMBER == 'not_issue'")[['ESN', 'REL_CMP_FAIL_CODE_LIST']].reset_index(drop = True)
none_idx = [df_temp.REL_CMP_FAIL_CODE_LIST[x] is None for x in range(df_temp.shape[0])]
df_temp = df_temp[none_idx]
df_not_issue = df_not_issue.query('ESN in @df_temp.ESN')
df_not_issue['Label'] = 0

df_all = pd.concat([df_issue, df_not_issue]).reset_index(drop = True)

df = pd.read_csv("Desktop/oepl/oepl_x15.csv")
df['option_count'] = 1
df_pivot = df.pivot_table(index = 'engine_serial_num', columns = 'option_assembly_num', \
                          values = 'option_count', aggfunc='mean').fillna(0).reset_index()
df_pivot = df_pivot.rename(columns = {'engine_serial_num': 'ESN'})
options_df_sub = df_all[['ESN', 'Label']]
options_df_sub = options_df_sub.merge(df_pivot, on = 'ESN', how = 'inner')
options_df_sub = options_df_sub.drop_duplicates().reset_index(drop = True)
del options_df_sub['ESN']
del df_all['INCDT_ISSUE_NUMBER'], df_all['ESN']

df_all[df_all.ESN.isin(df_pivot.query("VC1088 == 1").ESN)]


#Scale numeric features
for col in tqdm(numeric_cols + mes_cols):
    if df_all[col].dtype != 'float64':
        df_all[col] = df_all[col].str.replace(',','.')
    finite_vals = df_all[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        df_all.iloc[finite_vals.index.values, np.where(df_all.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df_all[col] = df_all[col].replace(np.nan, 0)
    

df_all_not_normalized = pd.concat([df_issue, df_not_issue]).reset_index(drop = True)

for col in tqdm(numeric_cols + mes_cols):
    if df_all_not_normalized[col].dtype != 'float64':
        df_all_not_normalized[col] = df_all_not_normalized[col].str.replace(',','.')
        df_all_not_normalized[col] = df_all_not_normalized[col].astype(float)
    df_all_not_normalized[col][df_all_not_normalized[col] < 0] = float('NaN')
    df_all_not_normalized[col][df_all_not_normalized[col] > 10**6] = float('NaN')

fitModelsAndOutput(df_all, 'INS', 'turbo', df_all_not_normalized)
fitModelsAndOutput(df_all, 'REL', 'turbo', df_all_not_normalized)
fitModelsAndOutput(df_all, 'OEPL', 'turbo', df_all_not_normalized)
fitModelsAndOutput(df_all, 'MES', 'turbo', df_all_not_normalized)
fitModelsAndOutput(options_df_sub, '', 'turbo', df_all_not_normalized)

df_all = pd.concat([df_issue, df_not_issue]).reset_index(drop = True)


#Numerical
saveNumericPlot('REL_CMP_SUM_NET_AMOUNT')
saveNumericPlot('REL_LAGS')
saveNumericPlot('REL_LAGD')

saveNumericPlot('INS_EA_OIL_TMP_SEC_SEV_1')
saveNumericPlot('INS_EA_OIL_TMP_SEC_SEV_TOTAL')
saveNumericPlot('INS_EA_OIL_PRSR_SEC_SEV_3')
saveNumericPlot('INS_EA_COOLANT_TMP_SEC_SEV_1')
saveNumericPlot('INS_EA_OIL_PRSR_SEC_SEV_2')
saveNumericPlot('INS_EA_COOLANT_TMP_SEC_SEV_TOTAL')
saveNumericPlot('INS_EA_OIL_PRSR_SEC_SEV_TOTAL')
saveNumericPlot('INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME')
saveNumericPlot('INS_EA_OIL_PRSR_SEC_SEV_1')
saveNumericPlot('INS_FS_TURBOCHARGER_ACTUATOR_POSITION_COMMANDED_PERCENT_CLOSED_PERCENT')
saveNumericPlot('INS_FS_ENGINE_OIL_PRESSURE_PSI')
saveNumericPlot('INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME')
saveNumericPlot('INS_TI_ENGINE_RUN_TIME_HOURS')
saveNumericPlot('INS_TI_ECM_TIME_KEY_ON_TIME_HOURS')
saveNumericPlot('INS_TI_FUEL_USED_GALLON')
saveNumericPlot('INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_TOTAL')
saveNumericPlot('INS_EA_OIL_TMP_SEC_SEV_2')
saveNumericPlot('INS_DC_CMP_PERCENT_HIGH_SPEED_TIME')
saveNumericPlot('INS_EA_COOLANT_TMP_SEC_SEV_2')
saveNumericPlot('INS_FS_INTAKE_MANIFOLD_PRESSURE_INHG')

saveNumericPlot('MES_TSH_CMP_TOTAL_TIME_FINISH_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_038_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_S03_MIN')
saveNumericPlot('MES_TSH_CMP_AVG_TIME_FINISH_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_006_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_004_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_044_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_019_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_065_MIN')
saveNumericPlot('MES_DA_CMP_DURATION_BY_STN_037_MIN')

#Categorical
saveCatPlot('REL_CMP_PROGRAM_GROUP_NAME')
saveCatPlot('REL_MKTG_HSP_NUM')
saveCatPlot('REL_BUILD_YEAR_MONTH')
saveCatPlot('REL_OEM_GROUP')
saveCatPlot('REL_EMISSIONS_FAMILY_CODE')

saveCatPlot('OEPL_POWER_ANGLE_RANGE_DEGREE')
saveCatPlot('OEPL_COMPRESSOR_TYPE')
saveCatPlot('OEPL_THERMOSTAT_HOUSING')
saveCatPlot('OEPL_CAPACITY_HIGH_LOW_RANGE_GALLONS')
saveCatPlot('OEPL_POWER_ANGLE_RANGE_DEGREE')


            
oepl_vars = ['VC1088', 'EM1361', 'WO1126', 'GG1769', \
             'AP1318', 'FS1148', 'BR1762', 'WR1717', 'TH1217']

df_sub = df[['engine_serial_num', 'option_assembly_num']]
df_sub = df_sub.rename(columns = {'engine_serial_num': 'ESN'})
df_sub = df_sub.merge(df_all, on = 'ESN', how = 'inner')
df_sub = df_sub.query("option_assembly_num in @oepl_vars")

cat_prop_plot(df_sub, 'option_assembly_num', "option_assembly_num", df_all.Label.mean()*100)
            
            
    
oepl_vars = ['WI1125', 'FF1394', 'SK1001', 'TH1712', 'RL1754', 'TH1724', \
             'ER1041', 'WF1293', 'SV1001', 'FF1397']

df_sub = df[['engine_serial_num', 'option_assembly_num']]
df_sub = df_sub.rename(columns = {'engine_serial_num': 'ESN'})
df_sub = df_sub.merge(df_all, on = 'ESN', how = 'inner')
df_sub = df_sub.query("option_assembly_num in @oepl_vars")

cat_prop_plot(df_sub, 'option_assembly_num', "option_assembly_num", df_all.Label.mean()*100)

