#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 10:20:25 2018

@author: sanjay
"""

import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
import mifs
import statsmodels.api as sm
import scipy.stats as stat
from sklearn.preprocessing import PolynomialFeatures
from scipy.sparse import csr_matrix
import seaborn as sns
from sklearn.ensemble import RandomForestClassifier
from tqdm import tqdm
from joblib import Parallel, delayed
import multiprocessing

proj_num = ['km576-27407','ou006-27316','esc_500','tbd_34','ia987-26584','nl145-25958']
df_issue = df_ot.query("INCDT_ISSUE_NUMBER in @proj_num")

def buildExcelAndSaveFigs(esns):
    
    issue_df = df_ot.query("ESN in @df_issue.ESN").copy()
    extra_df = extra_df.query("ESN in @df_issue.ESN").copy()

    egr_df['Label'] = 1
    extra_df['Label'] = 1
    
    #Get non-issues
    df_not_issue = df_ot[~df_ot.ESN.isin(egr_df.ESN)]
    df_extra_not_issue = df_extra[~df_extra.ESN.isin(extra_df.ESN)]
    
    df_not_issue['Label'] = 0
    df_extra_not_issue['Label'] = 0
    
    #Concatenate issues and non-issues
    df_all = pd.concat([issue_df, df_not_issue]).reset_index(drop = True)
    del df_all['INCDT_ISSUE_NUMBER'], df_all['ESN'], df_all['EARLIEST_INDICATION_DATE']
    
    
    #Scale numeric features
    for col in tqdm(numeric_cols):
        if df_all[col].dtype not in ['float64', 'float32']:
            df_all[col] = df_all[col].str.replace(',','.')
            df_all[col] = df_all[col].astype(float)
        df_all[col][df_all[col] < 0] = float('NaN')
        df_all[col][df_all[col] > 10**6] = float('NaN')
        finite_vals = df_all[col].dropna()
        if len(finite_vals) > 0:
            scaled_vals = scale(finite_vals)
            df_all.iloc[finite_vals.index.values, np.where(df_all.columns.values == col)[0][0]] = scaled_vals
        
        #Replace NaN with 0's (Mean)
        df_all[col] = df_all[col].replace(np.nan, 0)
        
    

    #Create Random Forest Model Object
    clf = RandomForestClassifier(n_estimators = 1000, class_weight = 'balanced', n_jobs = 10, verbose = 1)
    
    #Fit RF Model!
    clf.fit(df_all.iloc[:, df_all.columns != 'Label'], df_all.Label)
    
    print("Random Forest Classifier Fit")
    
    #Get top columns and scores
    top_cols_idx = np.argsort(clf.feature_importances_)[::-1]
    top_cols_score = np.sort(clf.feature_importances_)[::-1]
    top_cols = df_all.iloc[:, df_all.columns != 'Label'].columns.values[top_cols_idx]
    
    #Create Logistic Regression Model Object
    clf = LogisticRegressionCV(Cs = 10, penalty = 'l2', class_weight = 'balanced', solver = 'liblinear')
    
    #Fit LR Model!
    clf.fit(df_all.iloc[:, df_all.columns != 'Label'], df_all.Label)
