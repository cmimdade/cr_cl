#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 13:34:26 2018

@author: sanjay
"""


import pyarrow.parquet as pq
import pandas as pd
import ast
from tqdm import tqdm

path = 'Desktop/QUALITY_FEATURES_PARQUET_NEW/'
path = 'Desktop/INS_FAULT_SNAPSHOT_MDA_FEATURES/'
path = 'Desktop/MES_DISCO_COMP_SER_FEATURES/'

df = pq.read_table(path).to_pandas()

bad_cols = set()
for col in df.columns[2:]:
    if not isinstance(df[col][0], datetime.datetime):
        if df[col].apply(lambda x: len(x)).max() == 0:
            bad_cols.add(col) 
    



d = ast.literal_eval(df.INS_FS_TURBOCHARGER_SPEED_RPM[0][0])

ast


egr_df = pd.read_csv("Desktop/ds/Solve/egr_cooler_labels.csv")
egr_df = egr_df.rename(columns = {"Engine Serial Number": "engine_serial_num"})
egr_df = egr_df.query("Population != 'X1'") #Remove X1
del egr_df['Population']

len(df['INS_FS_WATER_IN_FUEL_STATE'][0])


import datetime

isinstance(df[col][0], datetime.datetime)
if not True:
    print(3)
    
    
    
# COLUMNS to use #
cols = ['INS_FS_WATER_IN_FUEL_STATE', 'INS_FS_VEHICLE_SPEED_MPH', 'INS_FS_TURBOCHARGER_SPEED_RPM', 'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_MEASURED_PERCENT_CLOSED_PERCENT']
fault_snapshot_sub = fault_snapshot[['ESN', 'INS_FS_FIRST_ECM_IMAGE_DATE_TIME'] + cols]


####### Data for Ankit ##
    
egr_df = pd.read_csv("Desktop/egr_cooler_updated_esns.csv", names = ['ESN'])
egr_df = egr_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
#del egr_df['ESN']
#del egr_df['INCDT_ISSUE_NUMBER']
egr_df['Label'] = 1

#Get non-issues
df_not_issue = df_ot[~df_ot.ESN.isin(egr_df.ESN)]
df_not_issue = df_not_issue[df_not_issue["REL_ANALYSIS_RATE_CAT:X3 2017"] == 1]

df_not_issue['Label'] = 0
#Concatenate issues and non-issues
df_all = pd.concat([egr_df, df_not_issue]).reset_index(drop = True)
del df_all['INCDT_ISSUE_NUMBER'], df_all['ESN'], df_all['EARLIEST_INDICATION_DATE']

top_cols = \
['INS_TI_ENGINE_DISTANCE_MILES', \
'REL_LAGS', \
'REL_LAGD', \
'REL_OEM_GROUP:Navistar', \
'REL_CMP_SUM_MATERIALS_AMOUNT', \
'REL_CMP_SUM_MARKUP_AMOUNT', \
'REL_CMP_SUM_NET_AMOUNT', \
'FC_OEM:3382', \
'REL_CMP_SUM_REPAIR_LABOR_AMOUNT', \
'REL_BUILD_YEAR_MONTH:2016_11', \
'REL_OEM_GROUP:Kenworth', \
'INS_TI_ENGINE_RUN_TIME_HOURS', \
'INS_TI_IDLE_FUEL_USED_GALLON', \
'REL_MKTG_HSP_NUM:525', \
'REL_BUILD_YEAR_MONTH:2016_12', \
'INS_TI_FUEL_USED_GALLON', \
'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', \
'REL_MKTG_HSP_NUM:565', \
'REL_OEM_GROUP:PETERBILT MOTOR CO', \
'INS_TI_FULL_LOAD_OPERATION_TIME_HOURS', \
'OEPL_CONNECTION_WATER_INLET:WI1125', \
'OEPL_HOUSING_THERMOSTAT:TH1712', \
'OEPL_ARRANGEMENT_EXH_RCN_AIR:ER1041', \
'OEPL_PRIMER_FUEL_SYSTEM:FS1766', \
'OEPL_PISTON_ENGINE:PP10460', \
'OEPL_ARRANGEMENT_SHIPPING:SK1001', \
'OEPL_FILTER_FUEL:FF1394', \
'OEPL_ARRANGEMENT_LIFTING:LA1112', \
'OEPL_MOUNTING_REF_COMPRESSOR:CF1072', \
'OEPL_RESISTOR_CORROSION:WF1293', \
'OEPL_PAINT:SS1097', \
'OEPL_ARRANGEMENT_OIL_FILL:OB1473', \
'OEPL_CONNECTION_AIR_INTAKE:IC1098', \
'OEPL_PLUMBING_TURBOCHARGER:TP1870', \
'OEPL_MOUNTING_LIFTING_BRACKET:LA1718', \
'OEPL_COMPRESSOR_AIR:CP1228', \
'OEPL_FLYWHEEL:FW1020', \
'OEPL_BRACKET_SHIPPING:SK1709', \
'OEPL_BRAKE_ENGINE:EB1207', \
'OEPL_PARTS_PERFORMANCE:PP44343', \
'MES_DA_CMP_TPLOC_FP_MIN', \
'EPAT_TEST_CNT_ABOVE_LIMIT', \
'MES_DA_CMP_TPLOC_TS_MIN', \
'MES_TSH_CMP_AVG_TIME_PROD_MIN', \
'MES_DEV_ACTION_REPL_MIN_DURATION_CMP', \
'MES_DEV_ACTION_REPL_TOTAL_DURATION_CMP', \
'EPAT_TEST_SUM_ENGINE_CELL_TIME', \
'EPAT_TEST_CNT_PASS', \
'MES_TSH_CMP_TOTAL_TIME_PROD_MIN', \
'MES_DA_CMP_DURATION_ESN_MIN', \
'MES_TSH_CMP_AVG_TIME_SCHED_MIN', \
'MES_DEV_ACTION_REPL_AVG_DURATION_CMP', \
'MES_DEV_ACTION_OMIT_MIN_DEVIATION_TIME_CMP', \
'MES_DEV_TOTAL_DURATION_CMP', \
'MES_TSH_CMP_TOTAL_TIME_SCHED_MIN', \
'EPAT_TEST_CNT_BELOW_LIMIT', \
'MES_TSH_CMP_TOTAL_TIME_SHIP_MIN', \
'MES_TSH_CMP_AVG_TIME_FINISH_MIN', \
'MES_TSH_CMP_AVG_TIME_SHIP_MIN', \
'MES_TSH_CMP_AVG_TIME_HOLD_MIN']

df_all = df_all[top_cols + ['Label']]

design = ('VPCR', 'OEPL')
manuf = ('MES', 'EPAT')
use = ('INS', 'REL', 'FC') 

design_cols = [df_all.columns.values[x].startswith(design + ('Label',)) for x in range(df_all.shape[1])]
manuf_cols = [df_all.columns.values[x].startswith(manuf + ('Label',)) for x in range(df_all.shape[1])]
use_cols = [df_all.columns.values[x].startswith(use + ('Label',)) for x in range(df_all.shape[1])]

df_design = df_all.iloc[:,design_cols]
df_manuf = df_all.iloc[:,manuf_cols]
df_use = df_all.iloc[:,use_cols]

df_all.to_csv("Desktop/Data/Ankit/egr_cooler_all_top_vars.csv", index = False)
df_design.to_csv("Desktop/Data/Ankit/egr_cooler_top_design_vars.csv", index = False)
df_manuf.to_csv("Desktop/Data/Ankit/egr_cooler_top_manufacturing_vars.csv", index = False)
df_use.to_csv("Desktop/Data/Ankit/egr_cooler_top_usage_vars.csv", index = False)

###

col1 = 'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME'
col2 = 'INS_TI_AVERAGE_ENGINE_LOAD_PERCENT'

fig, ax = plt.subplots(figsize=(12, 8))
ax.scatter(df_all.query("Label == 0")[col1], df_all.query("Label == 0")[col2], c = 'blue')
ax.scatter(df_all.query("Label == 1")[col1], df_all.query("Label == 1")[col2], c = 'red')




##
fault_snap_new = pd.DataFrame(index = range(fault_snapshot.shape[0]), columns = fault_snapshot.columns.values)
fault_snap_new['ESN'] = fault_snapshot.ESN
fault_snap_new['INS_FS_FIRST_ECM_IMAGE_DATE_TIME'] = fault_snapshot.INS_FS_FIRST_ECM_IMAGE_DATE_TIME
fault_snap_new['INS_FS_FIRST_ECM_IMAGE_DATE'] = fault_snapshot.INS_FS_FIRST_ECM_IMAGE_DATE

for col in tqdm(fault_snapshot.columns[3:]):
    ins = fault_snapshot[col]
        
    fault_snap_new[col] = [ast.literal_eval(ins[x][len(ins[x]) - 1])['PARAMETER_VALUE'] \
                  if len(ins[x]) > 0 and len(ast.literal_eval(ins[x][len(ins[x]) - 1])) > 1 \
                  else float('NaN') for x in range(ins.shape[0])]
    
    