#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 13 16:53:34 2018

@author: sanjay
"""

import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
import mifs
import statsmodels.api as sm
import scipy.stats as stat
from sklearn.preprocessing import PolynomialFeatures
from scipy.sparse import csr_matrix
import seaborn as sns
from sklearn.ensemble import RandomForestClassifier
from tqdm import tqdm
from joblib import Parallel, delayed
import multiprocessing

plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

## Load Data and Set Labels ##
df_ot = pd.read_csv("Desktop/Data/fat_table_with_one_hot.csv")

#Load new EGR Info
egr_df = pd.read_csv("Desktop/egr_cooler_updated_esns.csv", names = ['ESN'])
egr_df = egr_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
egr_df['Label'] = 1

#Get non-issues
df_not_issue = df_ot[~df_ot.ESN.isin(egr_df.ESN)]
df_not_issue['Label'] = 0

#Concatenate issues and non-issues
df_all = pd.concat([egr_df, df_not_issue]).reset_index(drop = True)
del df_all['ESN'], df_all['EARLIEST_INDICATION_DATE'], df_all['INCDT_ISSUE_NUMBER']

## First Pass Feature Reduction Analysis ##

## Assessing Coverage ##

def assessCoverage(df, desc):

    df_coverage = pd.DataFrame({'Variable': numeric_cols})
    df_coverage['Coverage'] = float('NaN')
    
    for col in numeric_cols:
        df_coverage.iloc[df_coverage[df_coverage.Variable == col].index, 1] = df[col].dropna().count()/df[col].shape[0]
    
    #Plot histogram of Coverages
    plt.hist(df_coverage.Coverage)
    plt.title("Histogram of Coverage Percentages for {}".format(desc))
    plt.xlabel("Coverage Percentage")
    plt.ylabel("Count of Features")
    plt.show()
    
    #Find number of variables below certain thresholds
    threshs = np.arange(0, 1.01, 0.01)
    all_vars = []
    for thresh in threshs:
        all_vars.append(df_coverage.query("Coverage <= @thresh").shape[0])
        
    plt.plot(threshs, all_vars)
    plt.axvline(x = 0.2, color = 'r', linestyle = '--')
    plt.title("Cumulative Number of Variables Below Threshold for {}".format(desc))
    plt.xlabel("Threshold")
    plt.ylabel("Number of Variables")
    plt.show()
    
assessCoverage(df_all, "Entire Dataset")
assessCoverage(d, "EGR Cooler")


## Coefficient of Variation ##

#Create empty dataframe for coefficient of variation calculation
df_ss = pd.DataFrame({'Variable': numeric_cols})
df_ss['Mean'] = float('NaN')
df_ss['Variance'] = float('NaN')

#Calculate mean and variance for numeric cols
#Convert columns to float and remove extraneous values
for col in tqdm(numeric_cols):
    if df_ot[col].dtype not in ['float64', 'float32']:
        df_ot[col] = df_ot[col].str.replace(',','.')
        df_ot[col] = df_ot[col].astype(float)
    df_ot[col][df_ot[col] < -10**6] = float('NaN')
    df_ot[col][df_ot[col] > 10**6] = float('NaN')
    
    df_ss.iloc[df_ss[df_ss.Variable == col].index, 1] = np.mean(df_ot[col].dropna())
    df_ss.iloc[df_ss[df_ss.Variable == col].index, 2] = np.var(df_ot[col].dropna())
    
#Coefficient of variation = sigma/mu
df_ss['Coef_of_Var'] = np.abs(np.sqrt(df_ss['Variance'])/df_ss['Mean'])

#plot histogram of coefs
plt.hist(df_ss.Coef_of_Var.dropna())
plt.title("Histogram of Coefficients of Variation")
plt.xlabel("Coefficient of Variation")
plt.ylabel("Count of Variables")

#Plot histogra for coefs < 10
plt.hist(df_ss.query("Coef_of_Var < 10").Coef_of_Var.dropna())
plt.title("Histogram of Coefficients of Variation")
plt.xlabel("Coefficient of Variation")
plt.ylabel("Count of Variables")

#Find numb of variables below certain threshold
threshs = np.arange(0, 10.01, 0.01)
all_vars = []
for thresh in threshs:
    all_vars.append(df_ss.query("Coef_of_Var <= @thresh").shape[0])
    
#Plot cumulative variable numbers#
plt.plot(threshs, all_vars)
plt.axvline(x = 1, color = 'r', linestyle = '--')
plt.title("Cumulative Number of Variables Below Threshold")
plt.xlabel("Threshold")
plt.ylabel("Number of Variables")

## Mutual Information ##

#Scale numeric features
for col in tqdm(numeric_cols):
    if df_all[col].dtype not in ['float64', 'float32']:
        df_all[col] = df_all[col].str.replace(',','.')
        df_all[col] = df_all[col].astype(float)
    df_all[col][df_all[col] < 0] = float('NaN')
    df_all[col][df_all[col] > 10**6] = float('NaN')
    finite_vals = df_all[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        df_all.iloc[finite_vals.index.values, np.where(df_all.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df_all[col] = df_all[col].replace(np.nan, 0)


df_issue = df_all.query("Label == 1")


df_all_sub = df_all.iloc[:,df_all.columns != 'Label']

cat_cols_mask = [df_all_sub.columns.values[x] \
                 not in numeric_cols for x in \
                 range(df_all_sub.shape[1])]

#Simple mutual information for categorical and numerical features
mic = feature_selection.mutual_info_classif(df_all_sub, \
                                            df_all.Label, discrete_features = cat_cols_mask)

#Extract top N features
top_vars = df_all_sub.columns.values[np.argsort(mic)[::-1]][:20]

    
df_ss.Coef_of_Var.hist(bins = 100)
df_ss = df_ss[~np.isnan(df_ss.Mean)]
    
df_ss = df_ss.sort_values(by = 'Coef_of_Var')

df_ot.EPAT_TEST_FAILED_CATEGORY_UPFIT.dropna().var()



df_coverage = pd.DataFrame({'Variable': numeric_cols})
df_coverage['Coverage'] = float('NaN')

for col in numeric_cols:
    df_coverage.iloc[df_coverage[df_coverage.Variable == col].index, 1] = df_all[col].dropna().count()/df_all[col].shape[0]




df_coverage.sort_values(by = "Coverage")
df_coverage.query("Coverage == 0")


scaled_vals = scale(df_all[col].dropna())

for col in cat_cols:
    print("{}:{}".format(col, df_not_normalized[col].dropna().nunique()))