#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 29 11:27:36 2018

@author: sanjay
"""

col_vals = df_one_hot.columns.values[2:]

col_type = ["Numerical" if col_vals[x] in \
                numeric_cols else "Categorical" for x in \
                range(len(col_vals))]

df = pd.DataFrame({'Variable': col_vals, 'Type': col_type})

df.to_csv("/Users/sanjay/Desktop/Solve_Results/solve_variables_03292018.csv", index = False)

for col in numeric_cols:
    
    
    if df_one_hot[col].dtype not in ['float64', 'float32']:
        df_one_hot[col] = df_one_hot[col].str.replace(',','.')
        df_one_hot[col] = df_one_hot[col].astype(float)
    
    fig, ax = plt.subplots(figsize=(12, 8))
    ax.hist(df_one_hot[col][np.isfinite(df_one_hot[col])].dropna(), color='blue', alpha=0.6, bins = 10, \
            label = 'min = {} | max = {}'.format(df_one_hot[col][np.isfinite(df_one_hot[col])].dropna().min(), df_one_hot[col][np.isfinite(df_one_hot[col])].dropna().max()))
    ax.set(title='Histogram of {}'.format(col), ylabel='Count', xlabel = col)
    plt.legend()
    col = col.replace("/","_")
    plt.savefig('/Users/sanjay/Desktop/Solve_Results/Numeric_Cols/Figs/{}.png'.format(col), bbox_inches="tight")
    plt.close()
