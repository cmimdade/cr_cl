# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pyarrow.parquet as pq
import pandas as pd
import os 
import numpy as np
import matplotlib.pyplot as plt
from sklearn import feature_selection

       
plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None
     
import pyarrow.parquet as pq
import pandas as pd
import os    

path = 'Desktop/QUALITY_FEATURES_PARQUET_NEW/'
path = 'Desktop/INS_FAULT_SNAPSHOT_MDA_MASTER/'
path = 'Desktop/MES_DISCO_COMP_SER_FEATURES/'
files = os.listdir(path)

parquet_files = []
for file in files:
    if ".parquet" in file:
        parquet_files.append(pq.read_table(path + files[0]).to_pandas())

online_tracker = pq.read_table(path).to_pandas()
online_tracker = pd.concat(parquet_files).reset_index(drop = True)
print("Online Incident Tracker Received")

path = 'Desktop/RLD_ENGINE_OPTION_ASSEMBLY/'
files = os.listdir(path)

for i, file in enumerate(files):
    print("File {} of {} Retrieved".format(i+1, len(files)))
    if ".parquet" in file:
        df = pq.read_table(path + file).to_pandas()
        df_sub = df[df.engine_serial_num.isin(online_tracker.ESN)]
        df_sub.to_csv("Desktop/oepl_x15_{}.csv".format(i), index = False)
        del df, df_sub

#### Analyze OEPL ####
      
df = pd.read_csv("Desktop/oepl/oepl_x15.csv")
def to_set(x):
    return frozenset(x)
df_oan = df.groupby('engine_serial_num')['option_assembly_num'].agg({'set': to_set}).reset_index()
df_oan

s1 = frozenset({1, 2})
s2 = frozenset({2, 1})
s3 = set({s1, s2})

frozenset(s1) == frozenset(s2)
i = 0
for i in range(df_oan.shape[0]):
    s = df_oan.set[i]
    
df['option_count'] = 1
df_pivot = df.pivot_table(index = 'engine_serial_num', columns = 'option_assembly_num', values = 'option_count', aggfunc='mean').fillna(0).reset_index()
df_pivot.to_csv("Desktop/oepl/oepl_pivot_x15.csv", index = False)

df_pivot = df.pivot_table(index = 'engine_serial_num', columns = 'shop_order_num', values = 'option_count', aggfunc='mean').fillna(0).reset_index()

df_pivot.iloc[:,1:].sum(axis = 1).hist(bins = 40)
plt.title("Histogram of Number of Shop Orders per Engine")
plt.ylabel("Count")
plt.xlabel("Number of Shop Orders")

cpl_num = [df.cpl_num[x].replace('CPL', '') for x in range(df.shape[0])]
df['CPL_NUM'] = cpl_num

df['CPL_NUM'] = df.cpl_num.apply(lambda x: x.replace('CPL', ''))


df_cpl = df[['engine_serial_num','CPL_NUM']].groupby('CPL_NUM').count().reset_index()

df_cpl
df_cpl.sort_values('engine_serial_num', ascending = False)

df['shop_order_group'] = df.shop_order_num.apply(lambda x: x[:2])
df['shop_order_group2'] = df.shop_order_num.apply(lambda x: x[2:])

df_cpl = df[['engine_serial_num','shop_order_group']].groupby('shop_order_group').count().reset_index()

####

df_pivot = df_pivot.reset_index()

egr_df = pd.read_csv("Desktop/ds/Solve/egr_cooler_labels.csv")
egr_df = egr_df.rename(columns = {"Engine Serial Number": "engine_serial_num"})
egr_df = egr_df.query("Population != 'X1'") #Remove X1
del egr_df['Population']
egr_df = egr_df.merge(df_pivot, on = 'engine_serial_num', how = 'inner').copy()
del egr_df['engine_serial_num']
y = egr_df.Label.astype(int)
del egr_df['Label']

#Simple mutual information for categorical and numerical features
mic = feature_selection.mutual_info_classif(egr_df, y, discrete_features = True)

#Extract top N features
top_vars = egr_df.columns.values[np.argsort(mic)[::-1]][:5]


for var in top_vars:
    print("Var {}, Issue - {}, Non Issue - {}".format(var, egr_df.query("Label == 1")[var].mean(), egr_df.query("Label == 0")[var].mean()))
