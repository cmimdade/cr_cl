#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 19:53:58 2018

@author: sanjay
"""


import pandas as pd
import numpy as np
import sys
import pickle
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn import feature_selection
import copy
from scipy.optimize import fmin
from scipy.stats import *
import mifs
import statsmodels.api as sm
import scipy.stats as stat
from sklearn.preprocessing import PolynomialFeatures
from scipy.sparse import csr_matrix
import seaborn as snsl
from sklearn.ensemble import RandomForestClassifier
from tqdm import tqdm
from joblib import Parallel, delayed
import multiprocessing

## Load Data and Set Labels ##
df_ot = pd.read_csv("Desktop/Data/fat_table_with_one_hot_raw.csv")
df_no_one_hot = pd.read_csv("Desktop/Data/fat_table_no_one_hot_raw.csv")
df_extra = pickle.load(open("Desktop/Data/universal_view_cols.p", "rb" ))

#OEPL
path = 'Desktop/Data/RLD_ENGINE_OPTION_ASSEMBLY_x15'
df = pq.read_table(path).to_pandas()
df['engine_serial_num'] = df['engine_serial_num'].astype(int)
df = df.rename(columns = {'option_assembly_num': 'OPT_NBR'})

#Load new EGR Info
issue_df = pd.read_csv("Desktop/Data/egr_flow_esns.csv", names = ['ESN'])
extra_df = issue_df.merge(df_extra, on = 'ESN', how = 'inner').copy()
issue_df = issue_df.merge(df_ot, on = 'ESN', how = 'inner').copy()
#del egr_df['ESN']
#del egr_df['INCDT_ISSUE_NUMBER']
issue_df['Label'] = 1
extra_df['Label'] = 1

#Get non-issues
df_not_issue = df_ot[~df_ot.ESN.isin(issue_df.ESN)]
df_not_issue = df_not_issue[df_not_issue["REL_ANALYSIS_RATE_CAT:X3 2017"] == 1]

df_extra_not_issue = df_extra[~df_extra.ESN.isin(extra_df.ESN)]
df_extra_not_issue = df_extra_not_issue[df_extra_not_issue["REL_ANALYSIS_RATE_CAT"] == "X3 2017"]

df_not_issue['Label'] = 0
df_extra_not_issue['Label'] = 0
#Concatenate issues and non-issues
df_all = pd.concat([issue_df, df_not_issue]).reset_index(drop = True)
#del df_all['INCDT_ISSUE_NUMBER'], df_all['ESN'], df_all['EARLIEST_INDICATION_DATE']
#del df_all['INS_FS_REMOTE_PTO_SWITCH']

#df_final = pd.read_csv("Desktop/usage_cols.csv")

#Remove spurious values
for col in tqdm(numeric_cols):
    if df_all[col].dtype not in ['float64', 'float32']:
        df_all[col] = df_all[col].str.replace(',','.')
        df_all[col] = df_all[col].astype(float)
    if 'PERCENT' in col:
        df_all[col][df_all[col] < 0] = float('NaN')
        df_all[col][df_all[col] > 100] = float('NaN')
    else:
        df_all[col][df_all[col] < -10**6] = float('NaN')
        df_all[col][df_all[col] > 10**6] = float('NaN')

#Scale numeric features
for col in tqdm(numeric_cols):
    finite_vals = df_all[col].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        df_all.iloc[finite_vals.index.values, np.where(df_all.columns.values == col)[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df_all[col] = df_all[col].replace(np.nan, 0)
    
df_all_not_normalized = pd.concat([issue_df, df_not_issue]).reset_index(drop = True)

#Remove spurious values
for col in tqdm(numeric_cols):
    if df_all_not_normalized[col].dtype not in ['float64', 'float32']:
        df_all_not_normalized[col] = df_all_not_normalized[col].str.replace(',','.')
        df_all_not_normalized[col] = df_all_not_normalized[col].astype(float)
    if 'PERCENT' in col:
        df_all_not_normalized[col][df_all_not_normalized[col] < 0] = float('NaN')
        df_all_not_normalized[col][df_all_not_normalized[col] > 100] = float('NaN')
    else:
        df_all_not_normalized[col][df_all_not_normalized[col] < -10**6] = float('NaN')
        df_all_not_normalized[col][df_all_not_normalized[col] > 10**6] = float('NaN')
    
new_views = ['Design', 'Manufacturing', 'Usage']
design = ('VPCR', 'OEPL')
manuf = ('MES', 'EPAT')
use = ('INS', 'REL', 'FC') 

design_cols = [df_all.columns.values[x].startswith(design + ('Label',)) for x in range(df_all.shape[1])]
manuf_cols = [df_all.columns.values[x].startswith(manuf + ('Label',)) for x in range(df_all.shape[1])]
use_cols = [df_all.columns.values[x].startswith(use + ('Label',)) for x in range(df_all.shape[1])]

df_design = df_all.iloc[:,design_cols]
df_manuf = df_all.iloc[:,manuf_cols]
df_use = df_all.iloc[:,use_cols]

df_datasets = [df_design, df_manuf, df_use]

df_final_list = []

#Create Random Forest Model Object

for df_ in df_datasets:
    
    clf = RandomForestClassifier(n_estimators = 10000, class_weight = 'balanced', n_jobs = 10, verbose = 1)

    #Fit RF Model!
    clf.fit(df_.iloc[:, df_.columns != 'Label'], df_.Label)

    print("Random Forest Classifier Fit")

    #Get top columns and scores
    top_cols_idx = np.argsort(clf.feature_importances_)[::-1]
    top_cols_score = np.sort(clf.feature_importances_)[::-1]
    top_cols = df_.iloc[:, df_.columns != 'Label'].columns.values[top_cols_idx]

    #Create Logistic Regression Model Object
    clf = LogisticRegressionCV(Cs = 10, penalty = 'l2', class_weight = 'balanced', solver = 'liblinear')

    #Fit LR Model!
    clf.fit(df_.iloc[:, df_.columns != 'Label'], df_.Label)
    print("Logistic Regression Classifier Fit")

    #Find directionality of coefficients
    dir_of_coefs = np.sign(clf.coef_[0]).astype(str)
    dir_of_coefs[dir_of_coefs == "1.0"] = '+'
    dir_of_coefs[dir_of_coefs == "-1.0"] = '-'
    dir_of_coefs[dir_of_coefs == "0.0"] = 'NA'
    #dir_of_coefs = clf.coef_[0]

    #num_issue = [df_all_not_normalized[df.Label==1][x].dropna().count() for x in df.iloc[:,col_idx].columns.values]
    #num_not_issue = [df_all_not_normalized[df.Label==0][x].dropna().count() for x in df.iloc[:,col_idx].columns.values]

    col_vals = df_.iloc[:, df_.columns != 'Label'].columns.values
    col_type = ["Numerical" if col_vals[x] in \
                numeric_cols else "Categorical" for x in \
                range(len(col_vals))]

    print("Summary Statistics Found")

    df_scores = pd.DataFrame({'Variable': top_cols, 'Importance Score': top_cols_score})
    df_dir = pd.DataFrame({'Variable': col_vals, \
                           'Directionality': dir_of_coefs, \
                           'Type': col_type})
    df_dir['Median of Issues'] = float('NaN')
    df_dir['Median of Non-Issues'] = float('NaN')
    df_dir['Mean of Issues'] = float('NaN')
    df_dir['Mean of Non-Issues'] = float('NaN')
    df_dir['SD of Issues'] = float('NaN')
    df_dir['SD of Non-Issues'] = float('NaN')
    df_dir['Rate of Issues'] = float('NaN')
    df_dir['Rate of Non-Issues'] = float('NaN')
    df_dir['Sample Size of Issues'] = float('NaN')
    df_dir['Sample Size of Non-Issues'] = float('NaN')

    df_dir = df_scores.merge(df_dir, on = 'Variable', how = 'left').reset_index(drop = True)
    
    df_dir = df_dir.query("Directionality != 'NA'").reset_index(drop = True)


    def getSummaryStats(i):
        if df_dir.Type[i] == "Categorical":
            df_dir['Rate of Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==1][df_dir.Variable[i]].mean()
            df_dir['Rate of Non-Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==0][df_dir.Variable[i]].mean()
            df_dir['Sample Size of Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==1][df_dir.Variable[i]].count()
            df_dir['Sample Size of Non-Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==0][df_dir.Variable[i]].count()
        if df_dir.Type[i] == "Numerical":
            df_dir['Median of Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==1][df_dir.Variable[i]].dropna().median()
            df_dir['Median of Non-Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==0][df_dir.Variable[i]].dropna().median()
            df_dir['Mean of Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==1][df_dir.Variable[i]].dropna().mean()
            df_dir['Mean of Non-Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==0][df_dir.Variable[i]].dropna().mean()
            df_dir['SD of Issues'][i] = np.sqrt(df_all_not_normalized[df_all_not_normalized.Label==1][df_dir.Variable[i]].dropna().var())
            df_dir['SD of Non-Issues'][i] = np.sqrt(df_all_not_normalized[df_all_not_normalized.Label==0][df_dir.Variable[i]].dropna().var())
            df_dir['Sample Size of Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==1][df_dir.Variable[i]].dropna().count()
            df_dir['Sample Size of Non-Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==0][df_dir.Variable[i]].dropna().count()
        return df_dir[df_dir.Variable == df_dir.Variable[i]]

    num_cores = multiprocessing.cpu_count()-1
    
    summary_stat_df = Parallel(n_jobs = num_cores)(delayed(getSummaryStats)(i) for i in range(df_dir.shape[0]))
    
    df_final = pd.concat(summary_stat_df)
    
    df_final_list.append(df_final)

## Automate Visualizations ##

issue_df = pd.read_csv("Desktop/Data/egr_flow_esns.csv", names = ['ESN'])
issue_df = issue_df.merge(df_no_one_hot, on = 'ESN', how = 'inner').copy()
issue_df['Label'] = 1

#Get non-issues
df_not_issue = df_no_one_hot[~df_no_one_hot.ESN.isin(issue_df.ESN)]
df_not_issue = df_not_issue[df_not_issue["REL_ANALYSIS_RATE_CAT"] == "X3 2017"]

df_not_issue['Label'] = 0

#Concatenate issues and non-issues
df_all_no_one_hot = pd.concat([issue_df, df_not_issue]).reset_index(drop = True)

#Remove spurious values
for col in tqdm(numeric_cols):
    if df_all_no_one_hot[col].dtype not in ['float64', 'float32']:
        df_all_no_one_hot[col] = df_all_no_one_hot[col].str.replace(',','.')
        df_all_no_one_hot[col] = df_all_no_one_hot[col].astype(float)
    if 'PERCENT' in col:
        df_all_no_one_hot[col][df_all_no_one_hot[col] < 0] = float('NaN')
        df_all_no_one_hot[col][df_all_no_one_hot[col] > 100] = float('NaN')
    else:
        df_all_no_one_hot[col][df_all_no_one_hot[col] < -10**6] = float('NaN')
        df_all_no_one_hot[col][df_all_no_one_hot[col] > 10**6] = float('NaN')


#Get OEPL
df_oepl_sub = df[['engine_serial_num', 'OPT_NBR']]
df_oepl_sub['OPT_CAT'] = df_oepl_sub['OPT_NBR'].apply(lambda x: x[:2])
df_oepl_sub = df_oepl_sub.rename(columns = {'engine_serial_num': 'ESN'})

df_oepl_sub = df_all_no_one_hot[['ESN', 'EARLIEST_INDICATION_DATE', 'Label']].merge(df_oepl_sub, on = 'ESN', how = 'left')
df_oepl_sub = df_oepl_sub.drop_duplicates()

#VPCR
vpcr_idx = [df_all_no_one_hot.columns.values[x].startswith(('ESN','VPCR')) \
            for x in range(df_all_no_one_hot.shape[1])]
df_vpcr = df_all_no_one_hot.iloc[:,vpcr_idx]
df_vpcr = df_vpcr.set_index('ESN').stack().reset_index().rename(columns = {'level_1': 'VPCR'})
df_vpcr = df_vpcr[df_vpcr[0] == 1].reset_index(drop = True)
df_vpcr['VPCR'] = [df_vpcr['VPCR'][x].split("_")[3] for x in range(df_vpcr.shape[0])]
del df_vpcr[0]

df_vpcr = df_all_no_one_hot[['ESN', 'Label']].merge(df_vpcr, on = 'ESN', how = 'right')

#Fault Code
fc_idx = [df_all_no_one_hot.columns.values[x].startswith(('ESN','FC')) \
            for x in range(df_all_no_one_hot.shape[1])]
df_fc = df_all_no_one_hot.iloc[:,fc_idx]
df_fc = df_fc.set_index('ESN').stack().reset_index().rename(columns = {'level_1': 'FC'})
df_fc = df_fc[df_fc[0] == 1].reset_index(drop = True)
df_fc['FC'] = [df_fc['FC'][x].split(":")[1] for x in range(df_fc.shape[0])]
df_fc['FC'] = df_fc['FC'].astype(int)
df_fc = df_all_no_one_hot[['ESN', 'Label']].merge(df_fc, on = 'ESN', how = 'right')
df_fc = df_fc.drop_duplicates()

del df_fc[0]

fc_lookup = pd.read_csv("Desktop/Data/fault_code_lookup_solve.csv", header = 1)
fc_lookup = fc_lookup[['Fault Code', 'Fault Code Category']]
fc_lookup['Fault Code Category'] = [fc_lookup['Fault Code Category'][x].replace(" ", "_") for x in range(fc_lookup.shape[0])]
fc_lookup = fc_lookup.rename(columns = {'Fault Code': 'FC'})
df_fc = df_fc.merge(fc_lookup, on = 'FC', how = 'left')

## Universal Views & Save Excel ##


df_sub_extra = pd.concat([extra_df, df_extra_not_issue]).reset_index(drop = True)
df_sub_extra.ID = df_sub_extra.index + 1

df_fault = df_sub_extra[['ID', 'GREEN_WRENCH_FAULT_CODE_LIST', 'REL_MINED_FAULT_CODES_LIST']]





df_fault['FC_LENGTH'] = [0 if df_fault['GREEN_WRENCH_FAULT_CODE_LIST'][x] \
         is None else len(df_fault['GREEN_WRENCH_FAULT_CODE_LIST'][x]) for x \
         in range(df_fault.shape[0])]


df_fault['FAULT_CODE_LIST'] = [np.unique(df_fault['REL_MINED_FAULT_CODES_LIST'][x]) \
        if df_fault['FC_LENGTH'][x] == 0 else np.unique(df_fault['GREEN_WRENCH_FAULT_CODE_LIST'][x]) \
        for x in range(df_fault.shape[0])]

df_fault_final = pd.DataFrame(columns = ['ID', 'FAULT_CODE'])
for i, line in enumerate(df_fault.FAULT_CODE_LIST):
    fault_id = np.repeat(df_fault.ID[i], len(line))
    df_temp = pd.DataFrame(data = {'ID': fault_id, 'FAULT_CODE': line})
    df_fault_final = pd.concat([df_fault_final, df_temp])
    
df_fault_final.reset_index(drop = True)

df_universal_view = df_fault_final.merge(df_sub_extra, on = 'ID', how = 'left')
del df_universal_view['GREEN_WRENCH_FAULT_CODE_LIST'], df_universal_view['REL_MINED_FAULT_CODES_LIST']

for col in df_universal_view.columns.values[4:43]:
    df_universal_view[col] = df_universal_view[col].astype(float)
    df_universal_view[col][df_universal_view[col] < 0] = float('NaN')
    df_universal_view[col][df_universal_view[col] > 10**8] = float('NaN')
    
#df_universal_view.to_csv("Desktop/Solve_Results/universal_view_031618.csv", index = False)
    

writer = pd.ExcelWriter('Desktop/Solve_Results/EGR Flow/variable_results_egr_flow_032318.xlsx', engine='xlsxwriter')

for i, view in enumerate(new_views):    
    df_final_list[i].to_excel(writer, sheet_name=view, index = False)

df_universal_view.to_excel(writer, sheet_name='Universal_View', index = False)

# Close the Pandas Excel writer and output the Excel file.
writer.save()

## Visualizations ##

def saveVisualizations(i):
    print(i)
    var = df_final.Variable[i]
    if df_final.Type[i] == "Numerical":
            saveNumericalHist(var, \
                              var, \
                              view, \
                              i+1,
                              issue_name,
                              df_all_not_normalized[df_all_not_normalized.Label==1][var].dropna().count(),
                              df_all_not_normalized[df_all_not_normalized.Label==0][var].dropna().count())
    var = var.replace("/", "_")

    if df_final.Type[i] == "Categorical":
        if 'OEPL' in df_final.Variable[i]:
            opt = var.split(":")[1][:2]
            df_sub = df_oepl_sub.query("OPT_CAT == @opt")
            df_sub = df_sub.rename(columns = {'OPT_NBR': var.split(":")[1]})
            saveCategoricalPlot(df_sub, \
                                var.split(":")[1], \
                                var.replace(":", "_"), \
                                view, \
                                i+1, \
                                issue_name, \
                                vline = 100*df_sub.Label.mean())
        elif 'VPCR' in df_final.Variable[i]:
            saveCategoricalPlot(df_vpcr, \
                                'VPCR', \
                                var.split("_")[3], \
                                view, \
                                i+1, \
                                issue_name, \
                                vline = 100*df_vpcr.Label.mean())
            
        elif 'FC' in df_final.Variable[i]:
            fc_cat = var.split(":")[0]
            if fc_cat != 'FC':
                fc_cat = fc_cat.split("_", 1)[1]
            else:
                return
            df_fc_sub = df_fc[df_fc['Fault Code Category'] == fc_cat]
            saveCategoricalPlot(df_fc_sub, \
                                'FC', \
                                var.replace(":", "_"), \
                                view, \
                                i+1, \
                                issue_name, \
                                vline = 100*df_fc_sub.Label.mean(),
                                sort = True)

        else:
            saveCategoricalPlot(df_all_no_one_hot, \
                                var.split(":")[0], \
                                var.replace(":", "_"), \
                                view, \
                                i+1, \
                                issue_name, \
                                vline = 100*df_all_no_one_hot.Label.mean())

issue_name = 'EGR Flow'
for j, view in enumerate(new_views):
    df_final = df_final_list[j]
    Parallel(n_jobs = 1)(delayed(saveVisualizations)(i) for i in range(df_final.shape[0]))

for i in range(65, df_final.shape[0]):
    saveVisualizations(i)
    
    
df_final['Type'] = "Categorical"
df_final.iloc[df_final[df_final.Variable.isin(numeric_cols)].index, 1] = "Numerical"
