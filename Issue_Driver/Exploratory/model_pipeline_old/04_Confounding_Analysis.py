#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 28 16:23:47 2018

@author: Nimit Patel
"""

"""
--- AIM ---

The aim of this piece of code is to find the top confounding variables after the first pass selection of features.

--- PROCESS ---

Based on the existing work done so far, different processes will be followed for finding top confounding variables based on the 
type of the features being compared.

1. Categorical vs categorical: Cramer's V value will be used in order to find the top confounded variables. The higher the value,
the higher the confounding.
- Another way of doing that is to build a logistic regression model, with each categorical feature being used as the response variable
turn by turn. The independed features with high p-values can be flagged as the confounders.

2. Numerical vs. numerical: Build a linear/ridge regression model, by taking each independent variables as the dependent variable
turn by turn, and use the 
    
3. Categorical vs. numerical: Build a logistic regression, taking each categorical feature one by one as the response variable, and
all the numerical features as the independent variables, and use the odds ratio to find top confounding features

--- HOW TO READ THIS CODE ---

This code has the following sections:
1. Initial section reads in the files needed. While you do this, also run the code called 'num_cat_cols.py'. That is basically just
the list of numerical and categorical columns for this dataset.

2. Perform the data alterations needed in order to do the analysis, for example select engines that have run above 1500 miles, 
remove categorical features having no variance (all zeros or ones) etc. Ideally all this should be done in the first pass feature selection.

3. Do the confounding analysis for cat vs cat, cat vs num, and num vs num.

"""

import numpy as np
import pandas as pd
from scipy.stats import chi2_contingency
from sklearn.linear_model import Ridge
from scipy import stats
from statsmodels.regression import linear_model
from matplotlib import pyplot as plt
import statsmodels.discrete.discrete_model as sm
from sklearn.linear_model import LogisticRegression
import datetime
from scipy.stats import pearsonr, spearmanr
import multiprocessing
from joblib import Parallel, delayed
from scipy.stats import f_oneway, kruskal
from statsmodels.stats.outliers_influence import variance_inflation_factor


issue_name = 'EGR_Cooler'
today = datetime.datetime.today().strftime('%m%d%Y')

# Load the top variables data, and filter out all categorical variables
# For this we basically need the data for the EGR cooler issue, which has only variables deemed as necessary after first pass removal
# of features

df = pd.read_csv("/Users/sanjay/Desktop/Solve_Results/{}/Data/{}_one_hot_norm_reduced.csv".format(issue_name, issue_name))
# Data which will be used for plots
df_plots = pd.read_csv("/Users/sanjay/Desktop/Solve_Results/{}/Data/{}_one_hot_raw.csv".format(issue_name, issue_name))
df_raw_cat = pd.read_csv("/Users/sanjay/Desktop/Solve_Results/{}/Data/{}_raw.csv".format(issue_name, issue_name))

# Read in file for columns to remove (they are features generated after the engine has come into the shop for fixing, they aren't useful drivers for analyzing the failure)
#columns_to_remove = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/columns_to_remove.csv")['columns_to_remove'].tolist()
# Also read in the features with their importance score from the univariate analysis, in order to provide a rank against them
uni_output = pd.ExcelFile("/Users/sanjay/Desktop/Solve_Results/{}/Results/variable_results_{}_041618.xlsx".format(issue_name, issue_name))


design = pd.read_excel(uni_output, 'Design')
manuf = pd.read_excel(uni_output, 'Manufacturing')
usage = pd.read_excel(uni_output, 'Usage')



results_df = pd.concat([design, manuf, usage])
#num_results_df = pd.concat([design_num, manuf_num, usage_num])

#results_df = results_df.sort_values(by = 'Chi-Sq Test').reset_index(drop = True)
#num_results_df = num_results_df.sort_values(by = 'Anderson-Darling').reset_index(drop = True)

results_df['Rank'] = np.arange(1, results_df.shape[0]+1)
#num_results_df['Rank'] = np.arange(1, num_results_df.shape[0]+1)


#all_uni_output = pd.concat([design, manuf, usage], axis = 0)
#all_uni_output['Rank'] = all_uni_output['Importance Score'].rank(method='dense', ascending=False)

# Now filter out engines which are less than 1500 miles
#normalized_limit = (1500 - df_plots['INS_TI_ENGINE_DISTANCE_MILES'].mean())/df_plots['INS_TI_ENGINE_DISTANCE_MILES'].std()
#df_plots = df_plots[(df_plots['INS_TI_ENGINE_DISTANCE_MILES'] >= 1500) | (df_plots['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]
#df = df[(df['INS_TI_ENGINE_DISTANCE_MILES'] >= normalized_limit) | (df['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]

# Remove some columns which are amount columns and are not needed
# Also remove columns about ESN, Earliest Indication Date, Label etc.
#columns_to_remove.extend(['EARLIEST_INDICATION_DATE', 'ESN', 'Label','INCDT_ISSUE_NUMBER'])
df = df.drop(['EARLIEST_INDICATION_DATE', 'ESN', 'Label','INCDT_ISSUE_NUMBER'], axis = 1)

"""
IMP NOTE: The list 'numeric_cols' (below) is a list of columns which are numerical in nature. The list has been created by Sanjay. Because of the tactical, ad-hoc
changes that kept on happening in the course of the study, this list kept on changing, so the list maintained by Sanjay and me might be
slightly different. This might create errors. So a good thing to invest time in is to create a finalized version of this list. 
"""
__, __, __, numeric_cols, __ = getColumnTypes(get_oepl = False)
# Keep only categorical variables from this data
# numeric_cols here is the big list of features, given by Sanjay
cat_df = df[df.columns.difference(numeric_cols)]

# Remove columns which are completely zero throughout or is equal to the number of rows, because that basically means it also 
# has no variance at all
#cat_df = cat_df.loc[:,cat_df.sum() != 0]
#cat_df = cat_df.loc[:,cat_df.sum() != cat_df.shape[0]]

# Generate dataframe of just numerical variables, all normalized, nulls imputed, and spurious values removed
num_data = df_plots.loc[:,df_plots.columns.isin(numeric_cols)]

# There should also be an attempt made at running this analysis by removing all the missing values instead of imputing them
# df_plots here is the entire dataset with the missing values as well
# Keep only the columns of df_plots which are in the dataset 'num_data', as those are the ones reminaing after first pass filtering, and are numerical

num_data_raw = df_plots.loc[:,df_plots.columns.isin(num_data.columns.tolist())]
# We cannot drop all the nulls, as that might need up dropping all the rows
# So first lets see how many nulls each column has and choose ones which have less than 55% missing data
# This number 50% was came to by some trials, trying to not remove too many columns

#num_data_raw = num_data_raw.loc[:,(num_data_raw.isnull().sum()/num_data_raw.shape[0] <= 0.5)]
#num_data_raw = num_data_raw.dropna(how = "any")


"""
CATEGORICAL VS. CATEGORICAL
"""

# Do CATEGORICAL VS. CATEGORICAL USING CRAMER'S V
# Define the function for calculating the Cramer's V value for each pair of categorical variables here

def cramers_corrected_stat(confusion_matrix):
    
    """ 
    Calculate Cramers V statistic for categorial-categorial association
    uses bias correction from Bergsma and Wicher, 
    https://en.wikipedia.org/wiki/Cram%C3%A9r%27s_V
    """
    chi2 = chi2_contingency(confusion_matrix)[0]
    n = confusion_matrix.sum().sum()
    phi2 = chi2/n
    r,k = confusion_matrix.shape
    phi2corr = max(0, phi2 - ((k-1)*(r-1))/(n-1))    
    rcorr = r - ((r-1)**2)/(n-1)
    kcorr = k - ((k-1)**2)/(n-1)
    return np.sqrt(phi2corr / min( (kcorr-1), (rcorr-1)))



def runCatvCat(col1, i):
    print(i)
    cv_list = np.zeros(len(cat_df.columns))
    for i, col2 in enumerate(cat_df.columns):
        if col1 != col2:
            confusion_matrix = pd.crosstab(cat_df[col1], cat_df[col2])
            if confusion_matrix.shape == (2,2):
                cv = cramers_corrected_stat(confusion_matrix)
            else:
                cv = np.nan
        else:
            cv = np.nan
        cv_list[i] = cv
    return cv_list

def runCatandSave(n = 5):
    num_cores = multiprocessing.cpu_count()-1
        
    cv_df = Parallel(n_jobs = num_cores)(delayed(runCatvCat)(col, i) for i, col in enumerate(cat_df.columns.values))
    cv_df = np.vstack(cv_df)
    
    cramerv_df = pd.DataFrame(cv_df, columns = cat_df.columns.values, index = cat_df.columns.values)
    
    # Now move across each row and pick top features confounded for each row value, but only in cases where the Cramer's V value is greater than 0.2
    
    arr = cramerv_df.values
    index_names = cramerv_df.index
    col_names = cramerv_df.columns
    
    R,C = np.where(np.triu(arr,1)>0.2)
    
    
    out_arr = np.column_stack((index_names[R],col_names[C],arr[R,C]))
    df_out = pd.DataFrame(out_arr,columns=['Feature','Top confounded feature','Cramers V value'])
    
    # Now make the output in such a manner which can be presented as a result, by adding ranks and sorting based on the feature rank
    df_out = pd.merge(df_out, results_df[['Variable','Rank']], how = 'left', left_on = 'Feature', right_on = 'Variable')
    df_out = df_out.rename(columns = {'Rank' : 'Feature rank'})
    df_out = pd.merge(df_out, results_df[['Variable','Rank']], how = 'left', left_on = 'Top confounded feature', right_on = 'Variable')
    df_out = df_out.rename(columns = {'Rank' : 'Confounded feature rank'})
    df_out = df_out.drop(['Variable_x','Variable_y'], axis = 1)
    
    df_out = df_out.sort_values(by = ['Feature rank', 'Confounded feature rank'])
    df_out = df_out.groupby('Feature').head(5)
    
    df_out = df_out[~np.isnan(df_out['Feature rank'])]
    
    # Reorder columns
    df_out = df_out.loc[:,['Feature', 'Feature rank', 'Top confounded feature', 'Confounded feature rank', 'Cramers V value']]
    
        
    return df_out

def getVIFforCat(df_out):

    def pseudoRSquaredLogit(i):
        logit = sm.Logit(all_data.iloc[:,i], all_data.iloc[:, all_data.columns.values != all_data.columns.values[i]])
        logit = logit.fit(method='bfgs', disp = False)
        return(1/(1-logit.prsquared))
    
    df_out['VIF'] = float('NaN')
    df_out['Highest_VIF'] = 0
    unique_features = df_out.Feature.unique()
    unique_features = df_out.Feature.unique()
    for col in unique_features:
        confounded_cols = df_out.query("Feature == @col")['Top confounded feature'].values
        all_cols = np.append(confounded_cols, col)
        all_data = cat_df[all_cols].dropna()
        try:
            vif_vals = [pseudoRSquaredLogit(i) for i in range(all_data.shape[1])]
        except:
            vif_vals = np.repeat(0, all_data.shape[1])
        df_out.loc[df_out['Feature'] == col, 'VIF'] = vif_vals[:len(vif_vals)-1]
        max_val = df_out[df_out['Feature'] == col]['VIF'].max()
        df_out.loc[(df_out['Feature'] == col) & (df_out['VIF'] == max_val),'Highest_VIF'] = 1
        
    return df_out

df_out = runCatandSave(5)
df_out = getVIFforCat(df_out)
df_out.to_csv("/Users/sanjay/Desktop/Solve_Results/{}/Results/cat_x_cat_{}.csv".format(issue_name, today), index = False)


def plotHeatmap(issue_name, i, col1, col2): 
    
    df = pd.DataFrame(0, columns = df_sub_col1['COL1'].unique(), index = df_sub_col2['COL2'].unique())
        
    for j in df.index:
        for k in df.columns:
            col_j = cat_df.columns.values[[str(j) in cat_df.columns.values[x] for x in range(cat_df.shape[1])]][0]
            col_k = cat_df.columns.values[[str(k) in cat_df.columns.values[x] for x in range(cat_df.shape[1])]][0]
            
            if col_j == col_k:
                df.loc[j,k] = 1
            else:
                cross_tab = pd.crosstab(cat_df[col_j], cat_df[col_k])
                if cross_tab.shape == (2,2):
                    cross_tab = cross_tab/(cross_tab.sum().sum())
                    df.loc[j,k] = cross_tab.iloc[1,1]
                else:
                    df.loc[j,k] = 0

    plt.imshow(df)
    plt.colorbar()
    plt.xticks(np.arange(df.shape[1]), df.columns, rotation=90)
    plt.yticks(np.arange(df.shape[0]), df.index)
    plt.savefig('/Users/sanjay/Desktop/Solve_Results/{}/Figs/Confounding/cat_x_cat/{}_{}_x_{}.png'.format(issue_name, i+1, col1.replace("/", "_"), col2.replace("/", "_")),bbox_inches='tight')
    plt.close()
    
def plotAllHeatmaps():
    
    unique_features = df_out.Feature.unique()
    for i, col1 in tqdm(enumerate(unique_features)):
        col2 = df_out['Top confounded feature'][i]
        if 'OEPL' in col1 and 'OEPL' in col2:
            opt1 = col1.split(":")[1][:2]
            opt2 = col2.split(":")[1][:2]
            df_sub_col1 = df_oepl_sub.query("OPT_CAT == @opt1")
            df_sub_col2 = df_oepl_sub.query("OPT_CAT == @opt2")
            
            df_sub_col1 = df_sub_col1.rename(columns = {'OPT_NBR': 'COL1'})
            df_sub_col2 = df_sub_col2.rename(columns = {'OPT_NBR': 'COL2'})
            
            plotHeatmap(issue_name, i, col1, col2)
            
        elif 'FC' in col1 and 'FC' in col2:
            fc_cat = col1.split(":")[0]
            if fc_cat != 'FC':
                fc_cat = fc_cat.split("_", 1)[1]
            else:
                continue
            df_sub_col1 = df_fc[df_fc['Fault Code Category'] == fc_cat]
            df_sub_col1 = df_sub_col1.rename(columns = {'FC': 'COL1'})
            
            fc_cat = col2.split(":")[0]
            if fc_cat != 'FC':
                fc_cat = fc_cat.split("_", 1)[1]
            else:
                continue
            df_sub_col2 = df_fc[df_fc['Fault Code Category'] == fc_cat]
            df_sub_col2 = df_sub_col2.rename(columns = {'FC': 'COL2'})               
            
            plotHeatmap(issue_name, i, col1, col2)
            
        elif ('INS' in col1 or 'REL' in col1) and ('INS' in col2 or 'REL' in col2):
            col1_sub = col1.split(":")[0]
            col2_sub = col2.split(":")[0]
            
            col1_all_cols = cat_df.columns.values[[col1_sub in cat_df.columns.values[x] for x in range(cat_df.shape[1])]]
            col2_all_cols = cat_df.columns.values[[col2_sub in cat_df.columns.values[x] for x in range(cat_df.shape[1])]]
            
            df_sub_col1 = pd.DataFrame({'COL1': col1_all_cols})
            df_sub_col2 = pd.DataFrame({'COL2': col2_all_cols})
            
            plotHeatmap(issue_name, i, col1, col2)
            
        elif ('OEPL' in col1 and 'FC' in col2) or ('FC' in col1 and 'OEPL' in col2):
            if 'FC' in col1 and 'OEPL' in col2:
                col1_temp = col1
                col1 = col2
                col2 = col1_temp
            
            
            opt1 = col1.split(":")[1][:2]
            df_sub_col1 = df_oepl_sub.query("OPT_CAT == @opt1")
            df_sub_col1 = df_sub_col1.rename(columns = {'OPT_NBR': 'COL1'})
    
            fc_cat = col2.split(":")[0]
            if fc_cat != 'FC':
                fc_cat = fc_cat.split("_", 1)[1]
            else:
                continue
            df_sub_col2 = df_fc[df_fc['Fault Code Category'] == fc_cat]
            df_sub_col2 = df_sub_col2.rename(columns = {'FC': 'COL2'})
            plotHeatmap(issue_name, i, col1, col2)
    
        elif ('OEPL' in col1 and ('REL' in col2 or 'INS' in col2)) or (('REL' in col1 or 'INS' in col1) and 'OEPL' in col2):
            if (('REL' in col1 or 'INS' in col1) and 'OEPL' in col2):
                col1_temp = col1
                col1 = col2
                col2 = col1_temp
            
            opt1 = col1.split(":")[1][:2]
            df_sub_col1 = df_oepl_sub.query("OPT_CAT == @opt1")
            df_sub_col1 = df_sub_col1.rename(columns = {'OPT_NBR': 'COL1'})
            
            col2_sub = col2.split(":")[0]
            col2_all_cols = cat_df.columns.values[[col2_sub in cat_df.columns.values[x] for x in range(cat_df.shape[1])]]
            df_sub_col2 = pd.DataFrame({'COL2': col2_all_cols})
            plotHeatmap(issue_name, i, col1, col2)
            
        elif ('FC' in col1 and ('REL' in col2 or 'INS' in col2)) or (('REL' in col1 or 'INS' in col1) and 'FC' in col2):
            if (('REL' in col1 or 'INS' in col1) and 'FC' in col2):
                col1_temp = col1
                col1 = col2
                col2 = col1_temp
            
            fc_cat = col1.split(":")[0]
            if fc_cat != 'FC':
                fc_cat = fc_cat.split("_", 1)[1]
            else:
                continue
            df_sub_col1 = df_fc[df_fc['Fault Code Category'] == fc_cat]
            df_sub_col1 = df_sub_col1.rename(columns = {'FC': 'COL1'})
            
            col2_sub = col2.split(":")[0]
            col2_all_cols = cat_df.columns.values[[col2_sub in cat_df.columns.values[x] for x in range(cat_df.shape[1])]]
            df_sub_col2 = pd.DataFrame({'COL2': col2_all_cols})
            plotHeatmap(issue_name, i, col1, col2)
        

"""
NUMERICAL VS. NUMERICAL
"""


def runNumvNum(col1, j):
    print(j)
    corr_list = np.zeros(len(num_data.columns))
    num_intersect = np.zeros(len(num_data.columns))
    for i, col2 in enumerate(num_data.columns):
        if col1 != col2:
            col1_vals = num_data[col1].dropna()
            col2_vals = num_data[col2].dropna()
            intersect_idx = np.intersect1d(col1_vals.index, col2_vals.index)
            
            intersect_vals_1 = num_data[col1][intersect_idx]
            intersect_vals_2 = num_data[col2][intersect_idx]
            if len(intersect_idx) > 1 and intersect_vals_1.nunique() > 1 and intersect_vals_2.nunique() > 1:
                corr = spearmanr(intersect_vals_1, intersect_vals_2)[0]
            else:
                corr = np.nan
        else:
            corr = np.nan
            intersect_idx = np.array([])
        corr_list[i] = corr
        num_intersect[i] = len(intersect_idx)
    return [corr_list, num_intersect]


corr_and_num = Parallel(n_jobs = num_cores)(delayed(runNumvNum)(col, j) \
                   for j, col in enumerate(num_data.columns.values))

corr_df = [corr_and_num[x][0] for x in range(len(corr_and_num))]

num_intersect_df = [corr_and_num[x][1] for x in range(len(corr_and_num))]


def wideToLong(corr_df, num_intersect_df):
    
    corr_df = np.vstack(corr_df)
    num_intersect_df = np.vstack(num_intersect_df)
    
    
    pearson_corr_df = pd.DataFrame(corr_df, columns = num_data.columns.values, index = num_data.columns.values)
    num_intersect_df = pd.DataFrame(num_intersect_df, columns = num_data.columns.values, index = num_data.columns.values)
    
    
    arr_p = pearson_corr_df.values
    index_names_p = pearson_corr_df.index
    col_names_p = pearson_corr_df.columns
    
    arr_i = num_intersect_df.values
    
    R,C = np.where(np.abs(np.triu(arr_p,1))>0.3)
    
    
    out_arr = np.column_stack((index_names_p[R],col_names_p[C],arr_p[R,C]))
    out_arr2 = np.column_stack((index_names_p[R],col_names_p[C],arr_i[R,C]))
    df_out2 = pd.DataFrame(out_arr2,columns=['Feature','Top confounded feature','Num of Samples'])

    
    df_out = pd.DataFrame(out_arr,columns=['Feature','Top confounded feature','Pearson Correlation'])
    df_out['Num of Samples'] = df_out2['Num of Samples']
    
    
    df_out = pd.merge(df_out, results_df[['Variable','Rank']], how = 'left', left_on = 'Feature', right_on = 'Variable')
    df_out = df_out.rename(columns = {'Rank' : 'Feature rank'})
    df_out = pd.merge(df_out, results_df[['Variable','Rank']], how = 'left', left_on = 'Top confounded feature', right_on = 'Variable')
    df_out = df_out.rename(columns = {'Rank' : 'Confounded feature rank'})
    df_out = df_out.drop(['Variable_x','Variable_y'], axis = 1)
    
    df_out = df_out.sort_values(by = ['Feature rank', 'Confounded feature rank'])
    df_out = df_out.groupby('Feature').head(5)
    # Get the result out in a csv
    df_out = df_out[~np.isnan(df_out['Feature rank'])]
    df_out = df_out[~np.isnan(df_out['Confounded feature rank'])]

        
    return df_out

df_out = wideToLong(corr_df, num_intersect_df)


### VIF ###

def getVIFforNum(df_out):

    df_out['VIF'] = float('NaN')
    df_out['Highest_VIF'] = 0
    
    unique_features = df_out.Feature.unique()
    for col in unique_features:
        confounded_cols = df_out.query("Feature == @col")['Top confounded feature'].values
        all_cols = np.append(confounded_cols, col)
        all_data = num_data[all_cols].dropna()
        if all_data.shape[0] > 0:
            vif_vals = [variance_inflation_factor(all_data.values, i) for i in range(all_data.shape[1])]
        else:
            vif_vals = np.repeat(0, all_data.shape[1])
        df_out.loc[df_out['Feature'] == col, 'VIF'] = vif_vals[:len(vif_vals)-1]
        max_val = df_out[df_out['Feature'] == col]['VIF'].max()
        df_out.loc[(df_out['Feature'] == col) & (df_out['VIF'] == max_val),'Highest_VIF'] = 1
    
    return df_out

df_out = getVIFforNum(df_out)
        
df_out.to_csv("/Users/sanjay/Desktop/Solve_Results/{}/Results/num_x_num_{}.csv".format(issue_name, today), index = False)



    
def plotAllPairPlots():
    def plotNumxNum(issue_name, i, col1, col2):
        fig, ax = plt.subplots()
        p1 = ax.scatter(df_plots.query("Label == 0")[col1], 
                        df_plots.query("Label == 0")[col2], 
                        c = 'blue', s = 10, marker = 'o')
        p2 = ax.scatter(df_plots.query("Label == 1")[col1], 
                        df_plots.query("Label == 1")[col2], 
                        c = 'red', s = 50, marker = 'X')
        count_p1 = sum((df_plots.query("Label == 0")[col1].notnull()) & (df_plots.query("Label == 0")[col2].notnull()))
        count_p2 = sum((df_plots.query("Label == 1")[col1].notnull()) & (df_plots.query("Label == 1")[col2].notnull()))
        plt.legend((p1, p2), ('Non-issue ' + '(Count:' + str(count_p1) + ')', 'Issue ' + '(Count:' + str(count_p2) + ')'), frameon = True)
        plt.xlabel(str(col1).replace(":","_"), fontsize=9)
        plt.ylabel(str(col2).replace(":","_"), fontsize=9)
        plt.savefig('/Users/sanjay/Desktop/Solve_Results/{}/Figs/Confounding/num_x_num/{}_{}_x_{}.png'.format(issue_name, i+1, col1.replace("/", "_"), col2.replace("/", "_")),bbox_inches='tight')
        plt.close()
        
    for i, col1 in tqdm(enumerate(df_out.Feature)):
        col2 = df_out['Top confounded feature'][i]
        plotNumxNum(issue_name, i, col1, col2)
    
plotAllPairPlots()

"""
CATEGORICAL VS. NUMERICAL
"""

# One way to find top numerical features confounded to a categorical feature will be a logistic regression.
# Each categorical feature will be taken as a response variable one by one, and all the numerical features can be the independent features

# cat_df and num_data are the two datasets we deal with. They have been created in the code above, and their index are aligned


def runCatvNum(col1, j):
    print(j)
    anova_list = np.zeros(len(num_data.columns))
    for i, col2 in enumerate(num_data.columns):
        col1_vals = num_data[cat_df[col1] == 1][col2].dropna()
        col2_vals = num_data[cat_df[col1] == 0][col2].dropna()
        if len(col1_vals) > 5 and len(col2_vals) > 5 and col1_vals.nunique() > 1 and col2_vals.nunique() > 1:
            anova_val = kruskal(col1_vals, col2_vals)[1]
        else:
            anova_val = np.nan
        anova_list[i] = anova_val
    return anova_list

def runAnovaAndSave(n=5):

    anova_df = Parallel(n_jobs = num_cores)(delayed(runCatvNum)(col, j) \
                       for j, col in enumerate(cat_df.columns.values))
    anova_df = np.vstack(anova_df)
    
    anova_df = pd.DataFrame(anova_df, columns = num_data.columns.values, index = cat_df.columns.values)
    
    df_out = pd.melt(anova_df)
    df_out['Feature'] = np.tile(anova_df.index.values, anova_df.shape[1])
    df_out = df_out.rename(columns = {'variable': 'Top Confounded Feature'})
    
    
    df_out = pd.merge(df_out, results_df[['Variable','Rank']], how = 'left', left_on = 'Feature', right_on = 'Variable')
    df_out = df_out.rename(columns = {'Rank' : 'Feature rank'})
    df_out = pd.merge(df_out, results_df[['Variable','Rank']], how = 'left', left_on = 'Top Confounded Feature', right_on = 'Variable')
    df_out = df_out.rename(columns = {'Rank' : 'Confounded feature rank'})
    df_out = df_out.drop(['Variable_x','Variable_y'], axis = 1)
    
    df_out = df_out.sort_values(by = ['Feature rank', 'Confounded feature rank'])
    df_out = df_out.groupby('Feature').head(5)
    
    df_out = df_out[~np.isnan(df_out['Feature rank'])]
    df_out = df_out[~np.isnan(df_out['Confounded feature rank'])]
    
    return df_out
    
df_out = runAnovaAndSave(n=5)
    # Get the result out in a csv
    
df_out.to_csv("/Users/sanjay/Desktop/Solve_Results/{}/Results/cat_x_num_04102018.csv".format(issue_name), index = False)
