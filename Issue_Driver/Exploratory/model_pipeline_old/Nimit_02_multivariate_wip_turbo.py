#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 25 11:30:54 2018

@author: Nimit Patel
"""

"""
This code is for generating the multivariate analysis results for the Turbocharger Sensor issue
"""

from treeinterpreter import treeinterpreter as ti, utils
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestClassifier
import numpy as np
import pandas as pd
from sklearn.preprocessing import scale
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
import seaborn as sns
from tqdm import tqdm
from pylab import rcParams


# Load the list of numeric cols and categorical cols
cat_cols = ['REL_CMP_PROGRAM_GROUP_NAME', \
            'REL_BUILT_ON_WEEKEND', \
            'REL_ANALYSIS_RATE_CAT', 'REL_CPL_NUM', 'REL_DESIGN_HSP_NUM', 'REL_DESIGN_RPM_NUM', \
            'REL_ECM_PART_NO','REL_MKTG_HSP_NUM', 'REL_MKTG_RPM_NUM', 'REL_OEM_TYPE', 'REL_OEM_GROUP', 'REL_BUILD_YEAR_MONTH', \
            'REL_USER_APPL_DESC', 'REL_EMISSIONS_FAMILY_CODE', \
            'INS_FS_AFTERTREATMENT_FUEL_INJECTOR_STATUS', \
            'INS_FS_AFTERTREATMENT_FUEL_SHUTOFF_VALVE_STATUS', \
            'INS_FS_AIR_CONDITIONING_PRESSURE_SWITCH', \
            'INS_FS_BRAKE_PEDAL_POSITION_SWITCH', \
            'INS_FS_CLUTCH_PEDAL_POSITION_SWITCH', \
            'INS_FS_CRANKCASE_VENTILATION_HEATER_COMMANDED_STATE', \
            'INS_FS_CRUISE_CONTROL_ON_OFF_SWITCH', \
            'INS_FS_EGR_FLOW_DERATE', \
            'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_2',
            'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_3',
            'INS_FS_ENGINE_COOLANT_LEVEL', \
            'INS_FS_ENGINE_OPERATING_STATE', \
            'INS_FS_ENGINE_PROTECTION_DERATE_SUPPRESS', \
            'INS_FS_ENGINE_PROTECTION_SHUTDOWN_OVERRIDE_SWITCH', \
            'INS_FS_ENGINE_SPEED_MAIN_SENSOR_SIGNAL_STATE', \
            'INS_FS_ENGINE_SPEED_MAIN_SYNCHRONIZATION_STATE', \
            'INS_FS_ENGINE_SPEED_STATUS', \
            'INS_FS_ENGINE_TORQUE_MODE', \
            'INS_FS_FAN_CONTROL_SWITCH', \
            'INS_FS_FAN_DRIVE_STATE', \
            'INS_FS_KEYSWITCH', \
            'INS_FS_PTO_ON_OFF_SWITCH', \
            'INS_FS_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_SWITCH', \
            'INS_FS_REMOTE_PTO_SWITCH', \
            'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_DERATE', \
            'INS_FS_TURBOCHARGER_SPEED_DERATE_ACTIVE', \
            'INS_FS_WATER_IN_FUEL_STATE', 'INS_FS_AFTERTREATMENT_PURGE_AIR_SOLENOID_STATUS', \
            'INS_FNP_GEAR_DOWN_PROTECTION_NA', \
            'INS_FNP_ADAPTIVE_CRUISE_NA', \
            'INS_FNP_CRUISE_CONTROL_NA', \
            'INS_FNP_ROAD_SPEED_GOVERNOR_NA', \
            'INS_FNP_IDLE_SHUTDOWN_NA', \
            'INS_FNP_REAR_AXLE_RATIO_NA', \
            'INS_FNP_TRANSMISSION_TYPE_NA', \
            'INS_FNP_ADJUSTABLE_LOW_IDLE_SPEED_NA', \
            'INS_FNP_GEAR_DOWN_TRANSMISSION_RATIO_NA', \
            'INS_FNP_TOP_GEAR_TRANSMISSION_RATIO_NA', \
            'INS_FNP_ACTIVE_REGENERATION_IN_PTO_AND_REMOTE_MODES_NA', \
            'INS_FNP_AUTOMOTIVE_MOBILE_REGENERATION_NA', \
            'INS_FNP_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_TEMPERATURE_STABILIZATION_NA', \
            'INS_FNP_LOAD_BASED_SPEED_CONTROL_NA', \
            'INS_FNP_SMARTACCEL_NA', \
            'INS_FNP_CRUISE_CONTROL_TYPE_NA', \
            'INS_FNP_FUEL_ECONOMY_ADJUSTMENT_FACTOR_NA', \
            'INS_FNP_LOAD_BASED_TORQUE_CONTROL_NA', \
            'INS_FNP_SMART_COAST_NA', \
            'INS_FNP_TORQUE_CONTROL_NA', \
            'INS_FNP_GEAR_RATIO_THRESHOLD_NA', \
            'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_KPA']
 
#oepl_cols = df['OPT_ITEM_DESC_NEW'].unique()
 
## Add Green Wrench in later
 
numeric_cols = ['REL_LAGD', 'REL_LAGS','INS_EA_CRANKCASE_PRSR_SEC_SEV_1', 'INS_EA_COOLANT_TMP_SEC_SEV_1', \
                'INS_EA_OIL_PRSR_SEC_SEV_1', 'INS_EA_OIL_TMP_SEC_SEV_1', 'INS_EA_SPEED_SEC_SEV_1', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_1', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_2', 'INS_EA_COOLANT_TMP_SEC_SEV_2', 'INS_EA_OIL_PRSR_SEC_SEV_2', \
                'INS_EA_OIL_TMP_SEC_SEV_2', 'INS_EA_SPEED_SEC_SEV_2', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_2', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_3', 'INS_EA_COOLANT_TMP_SEC_SEV_3', 'INS_EA_OIL_PRSR_SEC_SEV_3', \
                'INS_EA_OIL_TMP_SEC_SEV_3', 'INS_EA_SPEED_SEC_SEV_3', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_3', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_TOTAL', 'INS_EA_COOLANT_TMP_SEC_SEV_TOTAL', 'INS_EA_OIL_PRSR_SEC_SEV_TOTAL', \
                'INS_EA_OIL_TMP_SEC_SEV_TOTAL', 'INS_EA_SPEED_SEC_SEV_TOTAL', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_TOTAL', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_TIME', 'INS_DC_CMP_PERCENT_ZERO_OR_NEGATIVE_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_LOW_TORQUE_TIME', 'INS_DC_CMP_PERCENT_LIGHT_LOAD_TIME', \
                'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', 'INS_DC_CMP_PERCENT_CLIMBING_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME', 'INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME', 'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME', \
                'INS_TI_AVERAGE_ENGINE_LOAD_PERCENT', 'INS_TI_AVERAGE_ENGINE_SPEED_RPM', 'INS_TI_AVERAGE_VEHICLE_SPEED_MILES', \
                'INS_TI_IDLE_TIME_PERCENTAGE', 'INS_TI_ENGINE_BRAKE_DISTANCE_MILES', \
                'INS_TI_ENGINE_BRAKE_TIME_HOURS', 'INS_TI_ENGINE_DISTANCE_MILES', 'INS_TI_ENGINE_RUN_TIME_HOURS', \
                'INS_TI_FUEL_USED_GALLON', 'INS_TI_FULL_LOAD_OPERATION_TIME_HOURS', 'INS_TI_IDLE_FUEL_USED_GALLON', \
                'INS_FNP_CRUISE_CONTROL_LOWER_DROOP_MPH', \
                'INS_FNP_CRUISE_CONTROL_UPPER_DROOP_MPH', \
                'INS_FNP_GEAR_DOWN_MAXIMUM_VEHICLE_SPEED_HEAVY_ENGINE_LOAD_MPH', \
                'INS_FNP_GEAR_DOWN_MAXIMUM_VEHICLE_SPEED_LIGHT_ENGINE_LOAD_MPH', \
                'INS_FNP_ROAD_SPEED_GOVERNOR_LOWER_DROOP_MPH', \
                'INS_FNP_ROAD_SPEED_GOVERNOR_UPPER_DROOP_MPH', \
                'INS_FNP_LOW_IDLE_SPEED_RPM', \
                'INS_FNP_TIME_BEFORE_SHUTDOWN_HOURS', \
                'INS_FNP_MAXIMUM_ACCELERATOR_VEHICLE_SPEED_MPH', \
                'INS_FNP_MAXIMUM_CRUISE_CONTROL_SPEED_MPH', \
                'INS_FNP_TIRE_SIZE_REVSPERMILE', \
                'INS_FNP_RPM_BREAKPOINT_RPM', \
                'INS_FNP_TORQUE_RAMP_RATE_HP', \
                'EPAT_TEST_CNT_FAIL', \
                'EPAT_TEST_CNT_PASS', \
                'EPAT_TEST_CNT_INCOMPLETE', \
                'EPAT_TEST_CNT_MATERIAL_REVIEW', \
                'EPAT_TEST_CNT_OF_TEST', \
                'EPAT_TEST_SUM_ENGINE_CELL_TIME', \
                'EPAT_TEST_CNT_SHIFT_CODE', \
                'EPAT_TEST_CNT_ABOVE_LIMIT', \
                'EPAT_TEST_CNT_BELOW_LIMIT', \
                'EPAT_TEST_MAX_BELOW_LIMIT_PERCENT', \
                'EPAT_TEST_MAX_ABOVE_LIMIT_PERCENT', \
                'EPAT_TEST_FAILED_CATEGORY_ASSEMBLY', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_M', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_M_LEAK', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_M_REP', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X_ETS', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X_LEAK', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X_REP', \
                'EPAT_TEST_FAILED_CATEGORY_ETS_STAND', \
                'EPAT_TEST_FAILED_CATEGORY_FPM', \
                'EPAT_TEST_FAILED_CATEGORY_FPM_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_FUEL_LEAK_CH', \
                'EPAT_TEST_FAILED_CATEGORY_HOT_TEST', \
                'EPAT_TEST_FAILED_CATEGORY_ISX_OFFLINE', \
                'EPAT_TEST_FAILED_CATEGORY_MANTA_STAND', \
                'EPAT_TEST_FAILED_CATEGORY_PROGRAMMING', \
                'EPAT_TEST_FAILED_CATEGORY_QR_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_REPAIR_3', \
                'EPAT_TEST_FAILED_CATEGORY_REWORK', \
                'EPAT_TEST_FAILED_CATEGORY_TEST_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_UPFIT', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_AUT', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_CNV', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_ETS', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_SPC', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_SQC', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_STD', \
                'MES_TSH_CMP_NUM_OF_STATES_TRANSITION', \
               'MES_TSH_CMP_AVG_TIME_CANCEL_MIN', \
               'MES_TSH_CMP_AVG_TIME_FINISH_MIN', 'MES_TSH_CMP_AVG_TIME_HOLD_MIN', \
               'MES_TSH_CMP_AVG_TIME_PROD_MIN', 'MES_TSH_CMP_AVG_TIME_PURGED_MIN', \
               'MES_TSH_CMP_AVG_TIME_REPAIR_MIN', 'MES_TSH_CMP_AVG_TIME_SCHED_MIN', \
               'MES_TSH_CMP_AVG_TIME_SHIP_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_CANCEL_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_FINISH_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_HOLD_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_PROD_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_PURGED_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_REPAIR_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_SCHED_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_SHIP_MIN',  \
               'MES_ES_CMP_FINISHED_COUNT', \
               'MES_ES_CMP_LINESET_COUNT', 'MES_ES_CMP_SHIPPED_COUNT', \
               'MES_DA_CMP_CNTSTATION_BY_ESN', 'MES_DA_CMP_DURATION_ESN_MIN', \
               'MES_DA_CMP_TPLOC_AS_MIN', \
               'MES_DA_CMP_TPLOC_FP_MIN', 'MES_DA_CMP_TPLOC_HD_MIN', \
               'MES_DA_CMP_TPLOC_SB_MIN', 'MES_DA_CMP_TPLOC_TS_MIN', \
               'MES_DEV_COUNT_CMP', \
               'MES_DEV_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_AVG_DURATION_CMP', \
               'MES_DEV_TOTAL_DURATION_CMP', \
               'MES_DEV_MAX_DURATION_CMP', \
               'MES_DEV_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_ADD_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_ADD_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_COUNT_CMP', \
               'MES_DEV_ACTION_OMIT_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_OMIT_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_OMIT_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_COUNT_CMP', \
               'MES_DEV_ACTION_QTY_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_QTY_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_QTY_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_COUNT_CMP', \
               'MES_DEV_ACTION_REPL_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_REPL_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_REPL_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_ACTION_OMIT_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_ACTION_REPL_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_ACTION_QTY_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_NUM_DISTINCT_PARTS_CMP', \
               'INS_FS_ACCELERATOR_PEDAL_OR_LEVER_POSITION_SENSOR_SUPPLY_VOLTAGE_V', \
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_BAR',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_PSI',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_QUALITY',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_QUALITY_PERCENT',\
               'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE_CELCIUS',\
               'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE_FAHRENHEIT', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_INHG', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_KPA', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_PSI', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_INHG', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_KPA', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_PSI', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE_CELCIUS', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE_FAHRENHEIT', \
               'INS_FS_AFTERTREATMENT_FUEL_PRESSURE', \
               'INS_FS_AFTERTREATMENT_FUEL_PRESSURE_KPA', \
               'INS_FS_AFTERTREATMENT_FUEL_PRESSURE_PSI',\
               'INS_FS_AFTERTREATMENT_INTAKE_NOX_CORRECTED', \
               'INS_FS_AFTERTREATMENT_INTAKE_NOX_CORRECTED_PPM',\
               'INS_FS_AFTERTREATMENT_OUTLET_NOX_CORRECTED',\
               'INS_FS_AFTERTREATMENT_OUTLET_NOX_CORRECTED_PPM',\
               'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE',\
               'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE_CELCIUS',\
               'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE_FAHRENHEIT',\
               'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE',\
               'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE_CELCIUS',\
               'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE_FAHRENHEIT',\
               'INS_FS_BAROMETRIC_AIR_PRESSURE',\
               'INS_FS_BAROMETRIC_AIR_PRESSURE_INHG',                                
               'INS_FS_BAROMETRIC_AIR_PRESSURE_KPA',                                           
               'INS_FS_BAROMETRIC_AIR_PRESSURE_PSI',\
               'INS_FS_BATTERY_VOLTAGE',\
               'INS_FS_BATTERY_VOLTAGE_V', \
               'INS_FS_CRANKCASE_PRESSURE', \
               'INS_FS_CRANKCASE_PRESSURE_INH2O', \
               'INS_FS_CRANKCASE_PRESSURE_KPA', \
               'INS_FS_EGR_DIFFERENTIAL_PRESSURE', \
                'INS_FS_EGR_DIFFERENTIAL_PRESSURE_INHG', \
                'INS_FS_EGR_DIFFERENTIAL_PRESSURE_KPA', \
                'INS_FS_EGR_DIFFERENTIAL_PRESSURE_PSI',\
                'INS_FS_EGR_ORIFICE_PRESSURE',\
                'INS_FS_EGR_ORIFICE_PRESSURE_INHG',\
                'INS_FS_EGR_ORIFICE_PRESSURE_KPA',\
                'INS_FS_EGR_ORIFICE_PRESSURE_PSI',\
                'INS_FS_EGR_TEMPERATURE',\
                'INS_FS_EGR_TEMPERATURE_CELCIUS',\
                'INS_FS_EGR_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_EGR_VALVE_POSITION_COMMANDED',\
                'INS_FS_EGR_VALVE_POSITION_COMMANDED_PERCENT',\
                'INS_FS_EGR_VALVE_POSITION_MEASURED_PERCENT_OPEN',\
                'INS_FS_EGR_VALVE_POSITION_MEASURED_PERCENT_OPEN_PERCENT',\
                'INS_FS_ENGINE_COOLANT_TEMPERATURE',\
                'INS_FS_ENGINE_COOLANT_TEMPERATURE_CELCIUS',\
                'INS_FS_ENGINE_COOLANT_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_ENGINE_DISTANCE_KM',\
                'INS_FS_ENGINE_HOURS',\
                'INS_FS_ENGINE_OIL_PRESSURE',\
                'INS_FS_ENGINE_OIL_PRESSURE_KPA',\
                'INS_FS_ENGINE_OIL_PRESSURE_PSI',\
                'INS_FS_ENGINE_OIL_TEMPERATURE',\
                'INS_FS_ENGINE_OIL_TEMPERATURE_CELCIUS',\
                'INS_FS_ENGINE_OIL_TEMPERATURE_DEGREES',\
                'INS_FS_ENGINE_OIL_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_ENGINE_SPEED',\
                'INS_FS_ENGINE_SPEED_RPM',\
                'INS_FS_EXHAUST_GAS_PRESSURE',\
                'INS_FS_EXHAUST_GAS_PRESSURE_INHG',\
                'INS_FS_EXHAUST_GAS_PRESSURE_KPA',\
                'INS_FS_EXHAUST_GAS_PRESSURE_PSI',\
                'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED',\
                'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED_CELCIUS',\
                'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED_FAHRENHEIT',\
                'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE',\
                'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3S',\
                'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3S',\
                'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED',\
                'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED_BAR',\
                'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED_PSI',\
                'INS_FS_FUEL_RAIL_PRESSURE_MEASURED',\
                'INS_FS_FUEL_RAIL_PRESSURE_MEASURED_BAR',\
                'INS_FS_FUEL_RAIL_PRESSURE_MEASURED_PSI',\
                'INS_FS_INTAKE_AIR_THROTTLE_POSITION',\
                'INS_FS_INTAKE_AIR_THROTTLE_POSITION_COMMANDED',\
                'INS_FS_INTAKE_AIR_THROTTLE_POSITION_PERCENT',\
                'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE',\
                'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE_CELCIUS',\
                'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE_INHG',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE_KPA',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE_PSI',\
                'INS_FS_KEYSWITCH_OFF_COUNTS',\
                'INS_FS_KEYSWITCH_ON_COUNTS',\
                'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE',\
                'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE_CELCIUS',\
                'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_PERCENT_ACCELERATOR_PEDAL_OR_LEVER',\
                'INS_FS_PERCENT_ACCELERATOR_PEDAL_OR_LEVER_PERCENT',\
                'INS_FS_PERCENT_LOAD',\
                'INS_FS_PERCENT_LOAD_PERCENT',\
                'INS_FS_PERCENT_REMOTE_ACCELERATOR_PEDAL_OR_LEVER',\
                'INS_FS_PERCENT_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_PERCENT',\
                'INS_FS_SENSOR_SUPPLY_1_V',\
                'INS_FS_SENSOR_SUPPLY_2_V',\
                'INS_FS_SENSOR_SUPPLY_3_V',\
                'INS_FS_SENSOR_SUPPLY_4_V',\
                'INS_FS_SENSOR_SUPPLY_5_V',\
                'INS_FS_SENSOR_SUPPLY_6_V',\
                'INS_FS_TRANSMISSION_GEAR_RATIO',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_COMMANDED_PERCENT_CLOSED',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_COMMANDED_PERCENT_CLOSED_PERCENT',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_MEASURED_PERCENT_CLOSED',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_MEASURED_PERCENT_CLOSED_PERCENT',\
                'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_CALCULATED_CELCIUS',\
                'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_CALCULATED_FAHRENHEIT',\
                'INS_FS_TURBOCHARGER_SPEED',\
                'INS_FS_TURBOCHARGER_SPEED_RPM',\
                'INS_FS_VEHICLE_SPEED',\
                'INS_FS_VEHICLE_SPEED_KM/HR',\
                'INS_FS_VEHICLE_SPEED_MPH', 'INS_TI_ECM_TIME_KEY_ON_TIME_HOURS']

# The files to work off of here would be the whole dataset containing the issue labels, and the file containing the important
# features. Here we will be working with the EGR Cooler issue data

# Read in the Fat file
df = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/turbo_sensor_issue_data.csv")

# There are also some columns which we do not want to see in the analysis itself, which are amount ($$) features
columns_to_remove = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/columns_to_remove.csv")['columns_to_remove'].tolist()
df = df.drop(columns_to_remove, axis = 1)

df_full_raw = df # Save it completely so that we do not have to load it again
df_full_normalized = df

# Now normalize the numeric columns of this 'normalized' dataset
df_numeric_cols = df_full_normalized[numeric_cols]
df_numeric_cols = normalize_columns(df_numeric_cols)
df_cat_cols = df_full_normalized.drop(numeric_cols, axis = 1)
df_full_normalized = pd.concat([df_numeric_cols, df_cat_cols], axis = 1)

# Load the file containing important features for the EGR cooler issue
top_features_file = pd.ExcelFile("Downloads/Solve/OneDrive_1_3-11-2018/variable_results_turbo_sensor_032218.xlsx")
top_features_design = pd.read_excel(top_features_file, 'Design')
top_features_usage = pd.read_excel(top_features_file, 'Usage')
top_features = pd.concat([top_features_design, top_features_usage], axis = 0)

# Keep only the top 50 features
features_to_keep = list(top_features.Variable)
# Remove 'MES' features
for word in features_to_keep[:]:
        if word.startswith(tuple(['MES'] + ['EPAT'])):
            features_to_keep.remove(word)

features_to_keep = list(set(features_to_keep) - set(columns_to_remove))
features_to_keep.extend(["ESN", "EARLIEST_INDICATION_DATE", "INCDT_ISSUE_NUMBER", "id", 'Label'])

df_master = df_full_normalized.loc[:,features_to_keep]

df_master['id'] = df_master.index # Save the index as a column as it will be needed later on

# Now get the data ready for building a classifier
features = df_master[df_master.columns.difference(['id', 'Label','ESN', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'])].reset_index(drop = True)
features = features.dropna(axis = 1, how = 'all') # Remove any columns that are completely NA

#features = features.drop('INS_TI_ECM_TIME_KEY_ON_TIME_HOURS', axis = 1)
#features = features.drop('INS_TI_IDLE_TIME_HOURS', axis = 1)
labels = df_master['Label']
issue_labels = np.where(labels == 1)

# Split our data
train, test, train_labels, test_labels = train_test_split(features,
                                                          labels,
                                                          test_size = 0.25,
                                                          random_state = 777)
# Initialize the randomforest classifier 
rf = RandomForestClassifier(random_state = 777, n_estimators = 100, class_weight = "balanced", verbose = True, 
                            n_jobs = 8, min_samples_split = 10)

# Fit the model
rf.fit(X = train, y = train_labels)
predictions = rf.predict(test)

# Try a crossvalidated approach
preds = cross_val_predict(X = features, y = labels, estimator = rf, cv = 5)

# Check model performance for normal model and crossvalidated approach
pd.crosstab(test_labels, predictions, rownames=['True'], colnames=['Predicted'], margins=True)
pd.crosstab(labels, preds, rownames=['True'], colnames=['Predicted'], margins=True)

# Now first let's try and get the raw contributions of the individual features, univariate
prediction, bias, contributions = ti.predict(rf, features)

raw_contri = pd.DataFrame(contributions[:,:,1], columns = features.columns, 
                                  index = features.index)
issue_contri = raw_contri.loc[(issue_labels)]
res_df_one = pd.DataFrame(issue_contri.loc[:,:].mean(), columns = ['contri'])
res_df_one['abs_contri'] = abs(res_df_one['contri'])
res_df_one['feature'] = res_df_one.index

contributions = res_df_one

contributions = contributions.nlargest(n = 100, columns = 'abs_contri')
contributions = contributions.loc[:,'contri']
plt.figure(figsize = (30,contributions.shape[0]*0.25))
((contributions)*100).sort_values(ascending=False).plot(kind='barh')
plt.savefig('Downloads/Solve/multivariate_results/turbo_sensor_results/univariate_contributions.png',bbox_inches='tight')
plt.close()

# Now share these top univariate importances
res_df_one.to_csv("Downloads/Solve/multivariate_results/turbo_sensor_results/24032018_univariate_contributons.csv")

# Now set up the tree interpreter for cross contributions, in order to spot most interesting interactions
prediction, bias, contributions = ti.predict(rf, test, joint_contribution=True)
aggregated_contributions = utils.aggregated_contribution(contributions)

columns = np.asarray(list(features.columns))
res = []
for k in set(aggregated_contributions.keys()):
    res.append(([columns[index] for index in k] , 
               aggregated_contributions.get(k, 0)))  

res = sorted(res, reverse = True)
res_df = pd.DataFrame(np.array(res), columns = list(["feature_combination","contribution"]))
res_df['feature_combination'] = res_df['feature_combination'].astype('str') 
res_df['contribution'] = [el[1] for el in res_df['contribution']]
res_df['comma_count'] = res_df.feature_combination.str.count(',')
res_df['abs_contributions'] = res_df.contribution.abs()
# res_df_uptothree = res_df[(res_df['comma_count'] > 0) & (res_df['comma_count'] < 3)].sort_values(by = ['abs_contributions'], ascending = False)
# res_df_uptothree = res_df_uptothree.drop('comma_count', axis = 1)
# res_df_uptothree = res_df_uptothree[res_df_uptothree.feature_combination.str.contains("NUM:0") == False]

res_df_two = res_df[(res_df['comma_count'] == 1)].sort_values(by = ['abs_contributions'], ascending = False)
res_df_three = res_df[(res_df['comma_count'] == 2)].sort_values(by = ['abs_contributions'], ascending = False)

# Clean these contribution dataframes to make them shareable
res_df_two = clean_contributions_df(contributions_df = res_df_two, feature_column = 'feature_combination', drop_columns = ['feature_combination', 'comma_count'])
res_df_three = clean_contributions_df(contributions_df = res_df_three, feature_column = 'feature_combination', drop_columns = ['feature_combination', 'comma_count'])

# Now the idea is to pick these top combination of features and see if there is something interesting going on
# Pick up all two way interactions between features and categorize them into num x mum, cat x num, or cat x cat
# The idea will be to take feature_1 and feature_2 columns and detect if they are Numerical or Categorical
res_df_two.loc[res_df_two.feature_1.isin(numeric_cols), 'feature_1_type'] = 'numerical'
res_df_two['feature_1_type'].fillna('categorical', inplace = True)
res_df_two.loc[res_df_two.feature_2.isin(numeric_cols), 'feature_2_type'] = 'numerical'
res_df_two['feature_2_type'].fillna('categorical', inplace = True)

res_df_two.to_csv("Downloads/Solve/multivariate_results/turbo_sensor_results/24032018_bivariate_contributons.csv")
res_df_three.to_csv("Downloads/Solve/multivariate_results/turbo_sensor_results/24032018_trivariate_contributons.csv")

# Now generate plots based on the type of feature combination that exists
df_plots = df_full_raw

# categorical x categorical
# The results will be a stacked bar plot and a table which shows the same info along with a couple other features like sample size and
# failure rate

catxcat = res_df_two[(res_df_two['feature_1_type'] == 'categorical') & (res_df_two['feature_2_type'] == 'categorical')]

for i in range(catxcat.shape[0]):

    feature1 = catxcat['feature_1'].tolist()[i]
    feature2 = catxcat['feature_2'].tolist()[i]
    cat_df = create_categorical_table(df = df_plots, feature1 = feature1, feature2 = feature2, label_column = 'Label')

    fig, ax = plt.subplots()
    p1 = ax.barh(cat_df['feature_combination'], cat_df['non-issue'])
    p2 = ax.barh(cat_df['feature_combination'], cat_df['issue'], left = cat_df['non-issue'], color = "red")
    ax.set_xlabel('Count')
    plt.legend((p1[0], p2[0]), ('Non-issue', 'Issue'))
    plt.title('Plot of ' + str(feature1) + ' vs. ' + str(feature2))
    plt.savefig(('Downloads/Solve/multivariate_results/turbo_sensor_results/catxcat/turbo_sensor_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.png'),bbox_inches='tight')
    plt.close()
    cat_df.to_csv('Downloads/Solve/multivariate_results/turbo_sensor_results/catxcat/turbo_sensor_table_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.csv', na_rep = "NA")

# numerical vs numerical
# This will just be a scatter plot

numxnum = res_df_two[(res_df_two['feature_1_type'] == 'numerical') & (res_df_two['feature_2_type'] == 'numerical')]

for i in range(numxnum.shape[0]):
    feature1 = numxnum['feature_1'].tolist()[i]
    feature2 = numxnum['feature_2'].tolist()[i]
    
    fig, ax = plt.subplots()
    p1 = ax.scatter(df_plots.query("Label == 0")[feature1], 
           df_plots.query("Label == 0")[feature2], 
           c = 'blue', s = 10, marker = 'o')
    p2 = ax.scatter(df_plots.query("Label == 1")[feature1], 
           df_plots.query("Label == 1")[feature2], 
           c = 'red', s = 50, marker = 'X')
    
    count_p1 = sum((df_plots.query("Label == 0")[feature1].notnull()) & (df_plots.query("Label == 0")[feature2].notnull()))
    count_p2 = sum((df_plots.query("Label == 1")[feature1].notnull()) & (df_plots.query("Label == 1")[feature2].notnull()))

    plt.legend((p1, p2), ('Non-issue ' + '(Count:' + str(count_p1) + ')', 'Issue ' + '(Count:' + str(count_p2) + ')'))
    plt.title('Plot of ' + str(feature1) + ' vs. ' + str(feature2), fontsize = 11)
    plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
    plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
    plt.savefig(('Downloads/Solve/multivariate_results/turbo_sensor_results/numxnum/turbo_sensor_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.png'),bbox_inches='tight')
    plt.close()


# numerical vs categorical
# This can be a violin plot or a boxplot
# Generate first all the boxplots

catxnum = res_df_two[((res_df_two['feature_1_type'] == 'numerical') & (res_df_two['feature_2_type'] == 'categorical')) | ((res_df_two['feature_1_type'] == 'categorical') & (res_df_two['feature_2_type'] == 'numerical')) ]

for i in range(catxnum.shape[0]):
    if(catxnum['feature_1_type'].tolist()[i] == 'numerical'):
        feature2 = catxnum['feature_1'].tolist()[i]
        feature1 = catxnum['feature_2'].tolist()[i]
    else:
        feature1 = catxnum['feature_1'].tolist()[i]
        feature2 = catxnum['feature_2'].tolist()[i]
    
    sns.set_style("darkgrid")
    ax = sns.boxplot(x= feature1, y= feature2, data = df_plots, hue = "Label", 
                     palette = ['blue','red'], saturation = 0.5, fliersize = 2)
    handles, labels = ax.get_legend_handles_labels()
    labels = ['Non-issue', 'Issue']
    plt.legend(handles[0:2], labels[0:2])
    plt.title('Plot of ' + str(feature1) + ' vs. ' + str(feature2))
    
    medians = df_plots.groupby([feature1,'Label'])[feature2].median().values.tolist()
    
    nobs = []
    for i in df_plots[feature1].astype('category').cat.categories.tolist():
        counts = df_plots[(df_plots[feature1] == i) & (df_plots[feature2].notnull())]['Label'].value_counts().values.tolist()
        nobs.extend(counts)
    
    pos = list([-0.2, 0.2, 0.8,1.2])
  
    for i in range(len(nobs)):
        ax.text(pos[i], medians[i] + 0.05, 'N: ' + str(nobs[i]),
                horizontalalignment='center', size= "medium", color='w', weight='semibold')
        
    plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
    plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
    plt.savefig(('Downloads/Solve/multivariate_results/turbo_sensor_results/catxnum/turbo_sensor_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.png'),bbox_inches='tight')
    plt.close()


