#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 14:22:14 2018

@author: Nimit Patel
"""

"""
AIM

The aim of this code is to implement multivariate analysis on various issues (either coming out of the Detect phase or provided
by the SMEs), using the features available at the end of first layer selection and confounder analysis. The idea is to show
interaction between variables, and how that has an impact on the engine in terms of that particular issue (meaning does it
increase or decrease the chances of a particular issue happening)
"""

"""
PROCESS

The process here will be to build a randomforest model, put it through the tree interpreter, and see which combination of features
turn up as important, and what is their combined impact on the probability of an issue occuring
"""

from treeinterpreter import treeinterpreter as ti, utils
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestClassifier
import numpy as np
import pandas as pd
from sklearn.preprocessing import scale
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
import seaborn as sns
from tqdm import tqdm
from pylab import rcParams
from matplotlib.patches import Patch
from matplotlib.lines import Line2D


# Load the list of numeric cols and categorical cols
cat_cols = ['REL_CMP_PROGRAM_GROUP_NAME', \
            'REL_BUILT_ON_WEEKEND', \
            'REL_ANALYSIS_RATE_CAT', 'REL_CPL_NUM', 'REL_DESIGN_HSP_NUM', 'REL_DESIGN_RPM_NUM', \
            'REL_ECM_PART_NO','REL_MKTG_HSP_NUM', 'REL_MKTG_RPM_NUM', 'REL_OEM_TYPE', 'REL_OEM_GROUP', 'REL_BUILD_YEAR_MONTH', \
            'REL_USER_APPL_DESC', 'REL_EMISSIONS_FAMILY_CODE', \
            'INS_FS_AFTERTREATMENT_FUEL_INJECTOR_STATUS', \
            'INS_FS_AFTERTREATMENT_FUEL_SHUTOFF_VALVE_STATUS', \
            'INS_FS_AIR_CONDITIONING_PRESSURE_SWITCH', \
            'INS_FS_BRAKE_PEDAL_POSITION_SWITCH', \
            'INS_FS_CLUTCH_PEDAL_POSITION_SWITCH', \
            'INS_FS_CRANKCASE_VENTILATION_HEATER_COMMANDED_STATE', \
            'INS_FS_CRUISE_CONTROL_ON_OFF_SWITCH', \
            'INS_FS_EGR_FLOW_DERATE', \
            'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_2',
            'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_3',
            'INS_FS_ENGINE_COOLANT_LEVEL', \
            'INS_FS_ENGINE_OPERATING_STATE', \
            'INS_FS_ENGINE_PROTECTION_DERATE_SUPPRESS', \
            'INS_FS_ENGINE_PROTECTION_SHUTDOWN_OVERRIDE_SWITCH', \
            'INS_FS_ENGINE_SPEED_MAIN_SENSOR_SIGNAL_STATE', \
            'INS_FS_ENGINE_SPEED_MAIN_SYNCHRONIZATION_STATE', \
            'INS_FS_ENGINE_SPEED_STATUS', \
            'INS_FS_ENGINE_TORQUE_MODE', \
            'INS_FS_FAN_CONTROL_SWITCH', \
            'INS_FS_FAN_DRIVE_STATE', \
            'INS_FS_KEYSWITCH', \
            'INS_FS_PTO_ON_OFF_SWITCH', \
            'INS_FS_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_SWITCH', \
            'INS_FS_REMOTE_PTO_SWITCH', \
            'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_DERATE', \
            'INS_FS_TURBOCHARGER_SPEED_DERATE_ACTIVE', \
            'INS_FS_WATER_IN_FUEL_STATE', 'INS_FS_AFTERTREATMENT_PURGE_AIR_SOLENOID_STATUS', \
            'INS_FNP_GEAR_DOWN_PROTECTION_NA', \
            'INS_FNP_ADAPTIVE_CRUISE_NA', \
            'INS_FNP_CRUISE_CONTROL_NA', \
            'INS_FNP_ROAD_SPEED_GOVERNOR_NA', \
            'INS_FNP_IDLE_SHUTDOWN_NA', \
            'INS_FNP_REAR_AXLE_RATIO_NA', \
            'INS_FNP_TRANSMISSION_TYPE_NA', \
            'INS_FNP_ADJUSTABLE_LOW_IDLE_SPEED_NA', \
            'INS_FNP_GEAR_DOWN_TRANSMISSION_RATIO_NA', \
            'INS_FNP_TOP_GEAR_TRANSMISSION_RATIO_NA', \
            'INS_FNP_ACTIVE_REGENERATION_IN_PTO_AND_REMOTE_MODES_NA', \
            'INS_FNP_AUTOMOTIVE_MOBILE_REGENERATION_NA', \
            'INS_FNP_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_TEMPERATURE_STABILIZATION_NA', \
            'INS_FNP_LOAD_BASED_SPEED_CONTROL_NA', \
            'INS_FNP_SMARTACCEL_NA', \
            'INS_FNP_CRUISE_CONTROL_TYPE_NA', \
            'INS_FNP_FUEL_ECONOMY_ADJUSTMENT_FACTOR_NA', \
            'INS_FNP_LOAD_BASED_TORQUE_CONTROL_NA', \
            'INS_FNP_SMART_COAST_NA', \
            'INS_FNP_TORQUE_CONTROL_NA', \
            'INS_FNP_GEAR_RATIO_THRESHOLD_NA', \
            'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_KPA']
 
#oepl_cols = df['OPT_ITEM_DESC_NEW'].unique()
 
## Add Green Wrench in later
 
numeric_cols = ['REL_LAGD', 'REL_LAGS','INS_EA_CRANKCASE_PRSR_SEC_SEV_1', 'INS_EA_COOLANT_TMP_SEC_SEV_1', \
                'INS_EA_OIL_PRSR_SEC_SEV_1', 'INS_EA_OIL_TMP_SEC_SEV_1', 'INS_EA_SPEED_SEC_SEV_1', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_1', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_2', 'INS_EA_COOLANT_TMP_SEC_SEV_2', 'INS_EA_OIL_PRSR_SEC_SEV_2', \
                'INS_EA_OIL_TMP_SEC_SEV_2', 'INS_EA_SPEED_SEC_SEV_2', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_2', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_3', 'INS_EA_COOLANT_TMP_SEC_SEV_3', 'INS_EA_OIL_PRSR_SEC_SEV_3', \
                'INS_EA_OIL_TMP_SEC_SEV_3', 'INS_EA_SPEED_SEC_SEV_3', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_3', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_TOTAL', 'INS_EA_COOLANT_TMP_SEC_SEV_TOTAL', 'INS_EA_OIL_PRSR_SEC_SEV_TOTAL', \
                'INS_EA_OIL_TMP_SEC_SEV_TOTAL', 'INS_EA_SPEED_SEC_SEV_TOTAL', 'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_TOTAL', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_TIME', 'INS_DC_CMP_PERCENT_ZERO_OR_NEGATIVE_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_LOW_TORQUE_TIME', 'INS_DC_CMP_PERCENT_LIGHT_LOAD_TIME', \
                'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', 'INS_DC_CMP_PERCENT_CLIMBING_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME', 'INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME', 'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME', \
                'INS_TI_AVERAGE_ENGINE_LOAD_PERCENT', 'INS_TI_AVERAGE_ENGINE_SPEED_RPM', 'INS_TI_AVERAGE_VEHICLE_SPEED_MILES', \
                'INS_TI_IDLE_TIME_PERCENTAGE', 'INS_TI_ENGINE_BRAKE_DISTANCE_MILES', \
                'INS_TI_ENGINE_BRAKE_TIME_HOURS', 'INS_TI_ENGINE_DISTANCE_MILES', 'INS_TI_ENGINE_RUN_TIME_HOURS', \
                'INS_TI_FUEL_USED_GALLON', 'INS_TI_FULL_LOAD_OPERATION_TIME_HOURS', 'INS_TI_IDLE_FUEL_USED_GALLON', \
                'INS_FNP_CRUISE_CONTROL_LOWER_DROOP_MPH', \
                'INS_FNP_CRUISE_CONTROL_UPPER_DROOP_MPH', \
                'INS_FNP_GEAR_DOWN_MAXIMUM_VEHICLE_SPEED_HEAVY_ENGINE_LOAD_MPH', \
                'INS_FNP_GEAR_DOWN_MAXIMUM_VEHICLE_SPEED_LIGHT_ENGINE_LOAD_MPH', \
                'INS_FNP_ROAD_SPEED_GOVERNOR_LOWER_DROOP_MPH', \
                'INS_FNP_ROAD_SPEED_GOVERNOR_UPPER_DROOP_MPH', \
                'INS_FNP_LOW_IDLE_SPEED_RPM', \
                'INS_FNP_TIME_BEFORE_SHUTDOWN_HOURS', \
                'INS_FNP_MAXIMUM_ACCELERATOR_VEHICLE_SPEED_MPH', \
                'INS_FNP_MAXIMUM_CRUISE_CONTROL_SPEED_MPH', \
                'INS_FNP_TIRE_SIZE_REVSPERMILE', \
                'INS_FNP_RPM_BREAKPOINT_RPM', \
                'INS_FNP_TORQUE_RAMP_RATE_HP', \
                'EPAT_TEST_CNT_FAIL', \
                'EPAT_TEST_CNT_PASS', \
                'EPAT_TEST_CNT_INCOMPLETE', \
                'EPAT_TEST_CNT_MATERIAL_REVIEW', \
                'EPAT_TEST_CNT_OF_TEST', \
                'EPAT_TEST_SUM_ENGINE_CELL_TIME', \
                'EPAT_TEST_CNT_SHIFT_CODE', \
                'EPAT_TEST_CNT_ABOVE_LIMIT', \
                'EPAT_TEST_CNT_BELOW_LIMIT', \
                'EPAT_TEST_MAX_BELOW_LIMIT_PERCENT', \
                'EPAT_TEST_MAX_ABOVE_LIMIT_PERCENT', \
                'EPAT_TEST_FAILED_CATEGORY_ASSEMBLY', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_M', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_M_LEAK', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_M_REP', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X_ETS', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X_LEAK', \
                'EPAT_TEST_FAILED_CATEGORY_ASSY_X_REP', \
                'EPAT_TEST_FAILED_CATEGORY_ETS_STAND', \
                'EPAT_TEST_FAILED_CATEGORY_FPM', \
                'EPAT_TEST_FAILED_CATEGORY_FPM_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_FUEL_LEAK_CH', \
                'EPAT_TEST_FAILED_CATEGORY_HOT_TEST', \
                'EPAT_TEST_FAILED_CATEGORY_ISX_OFFLINE', \
                'EPAT_TEST_FAILED_CATEGORY_MANTA_STAND', \
                'EPAT_TEST_FAILED_CATEGORY_PROGRAMMING', \
                'EPAT_TEST_FAILED_CATEGORY_QR_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_REPAIR_3', \
                'EPAT_TEST_FAILED_CATEGORY_REWORK', \
                'EPAT_TEST_FAILED_CATEGORY_TEST_REPAIR', \
                'EPAT_TEST_FAILED_CATEGORY_UPFIT', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_AUT', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_CNV', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_ETS', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_SPC', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_SQC', \
                'EPAT_TEST_FAILED_ENGINE_SERIAL_STD', \
                'MES_TSH_CMP_NUM_OF_STATES_TRANSITION', \
               'MES_TSH_CMP_AVG_TIME_CANCEL_MIN', \
               'MES_TSH_CMP_AVG_TIME_FINISH_MIN', 'MES_TSH_CMP_AVG_TIME_HOLD_MIN', \
               'MES_TSH_CMP_AVG_TIME_PROD_MIN', 'MES_TSH_CMP_AVG_TIME_PURGED_MIN', \
               'MES_TSH_CMP_AVG_TIME_REPAIR_MIN', 'MES_TSH_CMP_AVG_TIME_SCHED_MIN', \
               'MES_TSH_CMP_AVG_TIME_SHIP_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_CANCEL_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_FINISH_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_HOLD_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_PROD_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_PURGED_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_REPAIR_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_SCHED_MIN', \
               'MES_TSH_CMP_TOTAL_TIME_SHIP_MIN',  \
               'MES_ES_CMP_FINISHED_COUNT', \
               'MES_ES_CMP_LINESET_COUNT', 'MES_ES_CMP_SHIPPED_COUNT', \
               'MES_DA_CMP_CNTSTATION_BY_ESN', 'MES_DA_CMP_DURATION_ESN_MIN', \
               'MES_DA_CMP_TPLOC_AS_MIN', \
               'MES_DA_CMP_TPLOC_FP_MIN', 'MES_DA_CMP_TPLOC_HD_MIN', \
               'MES_DA_CMP_TPLOC_SB_MIN', 'MES_DA_CMP_TPLOC_TS_MIN', \
               'MES_DEV_COUNT_CMP', \
               'MES_DEV_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_AVG_DURATION_CMP', \
               'MES_DEV_TOTAL_DURATION_CMP', \
               'MES_DEV_MAX_DURATION_CMP', \
               'MES_DEV_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_ADD_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_ADD_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_COUNT_CMP', \
               'MES_DEV_ACTION_OMIT_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_OMIT_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_OMIT_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_OMIT_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_COUNT_CMP', \
               'MES_DEV_ACTION_QTY_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_QTY_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_QTY_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_QTY_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_COUNT_CMP', \
               'MES_DEV_ACTION_REPL_SUMMED_ADDED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_REPL_SUMMED_OMITTED_PARTS_QTY_CMP', \
               'MES_DEV_ACTION_REPL_AVG_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_TOTAL_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_MAX_DURATION_CMP', \
               'MES_DEV_ACTION_REPL_MIN_DURATION_CMP', \
               'MES_DEV_ACTION_ADD_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_ACTION_OMIT_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_ACTION_REPL_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_ACTION_QTY_MIN_DEVIATION_TIME_CMP', \
               'MES_DEV_NUM_DISTINCT_PARTS_CMP', \
               'INS_FS_ACCELERATOR_PEDAL_OR_LEVER_POSITION_SENSOR_SUPPLY_VOLTAGE_V', \
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_BAR',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_PSI',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_QUALITY',\
               'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_QUALITY_PERCENT',\
               'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE_CELCIUS',\
               'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE_FAHRENHEIT', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_INHG', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_KPA', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_PSI', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE',\
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_INHG', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_KPA', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_PSI', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE_CELCIUS', \
               'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE_FAHRENHEIT', \
               'INS_FS_AFTERTREATMENT_FUEL_PRESSURE', \
               'INS_FS_AFTERTREATMENT_FUEL_PRESSURE_KPA', \
               'INS_FS_AFTERTREATMENT_FUEL_PRESSURE_PSI',\
               'INS_FS_AFTERTREATMENT_INTAKE_NOX_CORRECTED', \
               'INS_FS_AFTERTREATMENT_INTAKE_NOX_CORRECTED_PPM',\
               'INS_FS_AFTERTREATMENT_OUTLET_NOX_CORRECTED',\
               'INS_FS_AFTERTREATMENT_OUTLET_NOX_CORRECTED_PPM',\
               'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE',\
               'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE_CELCIUS',\
               'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE_FAHRENHEIT',\
               'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE',\
               'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE_CELCIUS',\
               'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE_FAHRENHEIT',\
               'INS_FS_BAROMETRIC_AIR_PRESSURE',\
               'INS_FS_BAROMETRIC_AIR_PRESSURE_INHG',                                
               'INS_FS_BAROMETRIC_AIR_PRESSURE_KPA',                                           
               'INS_FS_BAROMETRIC_AIR_PRESSURE_PSI',\
               'INS_FS_BATTERY_VOLTAGE',\
               'INS_FS_BATTERY_VOLTAGE_V', \
               'INS_FS_CRANKCASE_PRESSURE', \
               'INS_FS_CRANKCASE_PRESSURE_INH2O', \
               'INS_FS_CRANKCASE_PRESSURE_KPA', \
               'INS_FS_EGR_DIFFERENTIAL_PRESSURE', \
                'INS_FS_EGR_DIFFERENTIAL_PRESSURE_INHG', \
                'INS_FS_EGR_DIFFERENTIAL_PRESSURE_KPA', \
                'INS_FS_EGR_DIFFERENTIAL_PRESSURE_PSI',\
                'INS_FS_EGR_ORIFICE_PRESSURE',\
                'INS_FS_EGR_ORIFICE_PRESSURE_INHG',\
                'INS_FS_EGR_ORIFICE_PRESSURE_KPA',\
                'INS_FS_EGR_ORIFICE_PRESSURE_PSI',\
                'INS_FS_EGR_TEMPERATURE',\
                'INS_FS_EGR_TEMPERATURE_CELCIUS',\
                'INS_FS_EGR_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_EGR_VALVE_POSITION_COMMANDED',\
                'INS_FS_EGR_VALVE_POSITION_COMMANDED_PERCENT',\
                'INS_FS_EGR_VALVE_POSITION_MEASURED_PERCENT_OPEN',\
                'INS_FS_EGR_VALVE_POSITION_MEASURED_PERCENT_OPEN_PERCENT',\
                'INS_FS_ENGINE_COOLANT_TEMPERATURE',\
                'INS_FS_ENGINE_COOLANT_TEMPERATURE_CELCIUS',\
                'INS_FS_ENGINE_COOLANT_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_ENGINE_DISTANCE_KM',\
                'INS_FS_ENGINE_HOURS',\
                'INS_FS_ENGINE_OIL_PRESSURE',\
                'INS_FS_ENGINE_OIL_PRESSURE_KPA',\
                'INS_FS_ENGINE_OIL_PRESSURE_PSI',\
                'INS_FS_ENGINE_OIL_TEMPERATURE',\
                'INS_FS_ENGINE_OIL_TEMPERATURE_CELCIUS',\
                'INS_FS_ENGINE_OIL_TEMPERATURE_DEGREES',\
                'INS_FS_ENGINE_OIL_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_ENGINE_SPEED',\
                'INS_FS_ENGINE_SPEED_RPM',\
                'INS_FS_EXHAUST_GAS_PRESSURE',\
                'INS_FS_EXHAUST_GAS_PRESSURE_INHG',\
                'INS_FS_EXHAUST_GAS_PRESSURE_KPA',\
                'INS_FS_EXHAUST_GAS_PRESSURE_PSI',\
                'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED',\
                'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED_CELCIUS',\
                'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED_FAHRENHEIT',\
                'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE',\
                'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3S',\
                'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3S',\
                'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED',\
                'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED_BAR',\
                'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED_PSI',\
                'INS_FS_FUEL_RAIL_PRESSURE_MEASURED',\
                'INS_FS_FUEL_RAIL_PRESSURE_MEASURED_BAR',\
                'INS_FS_FUEL_RAIL_PRESSURE_MEASURED_PSI',\
                'INS_FS_INTAKE_AIR_THROTTLE_POSITION',\
                'INS_FS_INTAKE_AIR_THROTTLE_POSITION_COMMANDED',\
                'INS_FS_INTAKE_AIR_THROTTLE_POSITION_PERCENT',\
                'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE',\
                'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE_CELCIUS',\
                'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE_INHG',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE_KPA',\
                'INS_FS_INTAKE_MANIFOLD_PRESSURE_PSI',\
                'INS_FS_KEYSWITCH_OFF_COUNTS',\
                'INS_FS_KEYSWITCH_ON_COUNTS',\
                'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE',\
                'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE_CELCIUS',\
                'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE_FAHRENHEIT',\
                'INS_FS_PERCENT_ACCELERATOR_PEDAL_OR_LEVER',\
                'INS_FS_PERCENT_ACCELERATOR_PEDAL_OR_LEVER_PERCENT',\
                'INS_FS_PERCENT_LOAD',\
                'INS_FS_PERCENT_LOAD_PERCENT',\
                'INS_FS_PERCENT_REMOTE_ACCELERATOR_PEDAL_OR_LEVER',\
                'INS_FS_PERCENT_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_PERCENT',\
                'INS_FS_SENSOR_SUPPLY_1_V',\
                'INS_FS_SENSOR_SUPPLY_2_V',\
                'INS_FS_SENSOR_SUPPLY_3_V',\
                'INS_FS_SENSOR_SUPPLY_4_V',\
                'INS_FS_SENSOR_SUPPLY_5_V',\
                'INS_FS_SENSOR_SUPPLY_6_V',\
                'INS_FS_TRANSMISSION_GEAR_RATIO',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_COMMANDED_PERCENT_CLOSED',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_COMMANDED_PERCENT_CLOSED_PERCENT',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_MEASURED_PERCENT_CLOSED',\
                'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_MEASURED_PERCENT_CLOSED_PERCENT',\
                'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_CALCULATED_CELCIUS',\
                'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_CALCULATED_FAHRENHEIT',\
                'INS_FS_TURBOCHARGER_SPEED',\
                'INS_FS_TURBOCHARGER_SPEED_RPM',\
                'INS_FS_VEHICLE_SPEED',\
                'INS_FS_VEHICLE_SPEED_KM/HR',\
                'INS_FS_VEHICLE_SPEED_MPH', 'INS_TI_ECM_TIME_KEY_ON_TIME_HOURS', 'INS_TI_IDLE_TIME_HOURS','INS_FNP_GEAR_DOWN_TRANSMISSION_RATIO_NA', 'INS_FNP_REAR_AXLE_RATIO_NA', 'INS_FNP_TOP_GEAR_TRANSMISSION_RATIO_NA']

# The files to work off of here would be the whole dataset containing the issue labels, and the file containing the important
# features. Here we will be working with the EGR Cooler issue data

"""
# Read in the Fat file
df = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/fat_table_with_one_hot.csv")
#df_no_onehot = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/fat_table_no_one_hot.csv")

df = df.rename(columns = {'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3/S':\
                          'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3S',\
                          'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3/S':\
                          'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3S'})

# There are also some columns which we do not want to see in the analysis itself, which are amount ($$) features
columns_to_remove = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/columns_to_remove.csv")['columns_to_remove'].tolist()
df = df.drop(columns_to_remove, axis = 1)

# Take care of error values in the raw data
for col in tqdm(numeric_cols):
    if df[col].dtype not in ['float64', 'float32']:
        df[col] = df[col].str.replace(',','.')
        df[col] = df[col].astype(float)
    if 'PERCENT' in col:
        df[col][df[col] < 0] = float('NaN')
        df[col][df[col] > 100] = float('NaN')
    else:
        df[col][df[col] < -10**6] = float('NaN')
        df[col][df[col] > 10**6] = float('NaN')


df_full_raw = df # Save it completely so that we do not have to load it again
df_full_normalized = df

# Now normalize the numeric columns of this 'normalized' dataset
df_numeric_cols = df_full_normalized[numeric_cols]
df_numeric_cols = normalize_columns(df_numeric_cols)
df_cat_cols = df_full_normalized.drop(numeric_cols, axis = 1)
df_full_normalized = pd.concat([df_numeric_cols, df_cat_cols], axis = 1)

# Load new EGR Info
egr_df = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/egr_cooler_updated_esns.csv", names = ['ESN'])
# Load the file containing important features for the EGR cooler issue
top_features_file = pd.ExcelFile("Downloads/Solve/OneDrive_1_3-11-2018/variable_results_egr_cooler_031318.xlsx")
top_features = pd.read_excel(top_features_file, 'VAR')

# Keep only the features other than MES and EPAT
features_to_keep = list(top_features.Variable)
# Remove 'MES' features
for word in features_to_keep[:]:
        if word.startswith(tuple(['MES'] + ['EPAT'])):
            features_to_keep.remove(word)

features_to_keep = list(set(features_to_keep) - set(columns_to_remove))
features_to_keep.extend(["ESN", "EARLIEST_INDICATION_DATE", "INCDT_ISSUE_NUMBER", "id"])
#df = df.replace(np.inf, np.nan)
df_master = df_full_normalized.loc[df_full_normalized['REL_ANALYSIS_RATE_CAT:X3 2017'] == 1].reset_index(drop = True) # Take only X3 data into consideration
df_master = df_master.loc[:,features_to_keep]

# Create the ouput label and also identifier for each incident
df_master.loc[df_master['ESN'].isin(egr_df['ESN']), 'Label'] = 1
df_master['Label'].fillna(0, inplace = True)
df_master['id'] = df_master.index # Save the index as a column as it will be needed later on

df_full_raw.loc[df_full_raw['ESN'].isin(egr_df['ESN']), 'Label'] = 1
df_full_raw['Label'].fillna(0, inplace = True)
"""

# In simple sense, there is only need of two datasets to do the analysis of multivariate
# 1. The dataset which has no spurious values, null values have been imputed, numerical columns are normalized
# 2. The dataset which has no spurious values, null values are not removed, and numerical columns are not normalized
# Both datasets only have the columns remaining after the first pass removal of columns

# Read in both above needed datasets
# Data which will go into analysis, which is normalized
df = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/EGR_Cooler_one_hot_norm_reduced.csv")
# Data which will be used for plots
df_plots = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/EGR_Flow_one_hot_raw_numeric.csv")

# Now filter out engines which are less than 1500 miles
normalized_limit = (1500 - df_plots['INS_TI_ENGINE_DISTANCE_MILES'].mean())/df_plots['INS_TI_ENGINE_DISTANCE_MILES'].std()
df_plots = df_plots[(df_plots['INS_TI_ENGINE_DISTANCE_MILES'] >= 1500) | (df_plots['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]
df = df[(df['INS_TI_ENGINE_DISTANCE_MILES'] >= normalized_limit) | (df['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]

# Determine features in univariate output
uni_output = pd.ExcelFile("Downloads/Solve/OneDrive_1_3-11-2018/variable_results_egr_cooler_032518.xlsx")
design = pd.read_excel(uni_output, 'Design')
design['Source'] = 'Design'
manuf = pd.read_excel(uni_output, 'Manufacturing')
manuf['Source'] = 'Manufacturing'
usage = pd.read_excel(uni_output, 'Usage')
usage['Source'] = 'Usage'
all_uni_output = pd.concat([design, manuf, usage], axis = 0)
all_uni_output = all_uni_output[['Variable','Source']]
all_uni_output['Color'] = 'blue'
all_uni_output['Color'][all_uni_output['Source'] == 'Manufacturing'] = 'red'
all_uni_output['Color'][all_uni_output['Source'] == 'Usage'] = 'green'
all_uni_output = all_uni_output.rename(columns = {'Variable':'feature'})

columns_to_remove = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/columns_to_remove.csv")['columns_to_remove'].tolist()

# Remove columns about price and amounts
df_master = df.drop(columns_to_remove, axis = 1)
df_master['id'] = df_master.index

# Keep only the features other than MES and EPAT
features_to_keep = list(df_master.columns)
# Remove 'MES' features
for word in features_to_keep[:]:
        if word.startswith(tuple(['MES'] + ['EPAT'])):
            features_to_keep.remove(word)

# Keep only features other than MES and EPAT
df_master = df_master.loc[:,features_to_keep]

# Now get the data ready for building a classifier
features = df_master[df_master.columns.difference(['id', 'Label','ESN', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'])].reset_index(drop = True)
features = features.dropna(axis = 1, how = 'all') # Remove any columns that are completely NA

#features = features.drop('INS_TI_ECM_TIME_KEY_ON_TIME_HOURS', axis = 1)
#features = features.drop('INS_TI_IDLE_TIME_HOURS', axis = 1)
labels = df_master['Label']
issue_labels = np.where(labels == 1)

# Split our data
train, test, train_labels, test_labels = train_test_split(features,
                                                          labels,
                                                          test_size = 0.25,
                                                          random_state = 777)
# Initialize the randomforest classifier 
rf = RandomForestClassifier(random_state = 777, n_estimators = 100, verbose = True, class_weight = "balanced",
                            n_jobs = 8, min_samples_split = 15)

# Fit the model
rf.fit(X = train, y = train_labels)
predictions = rf.predict(test)

# Try a crossvalidated approach
preds = cross_val_predict(X = features, y = labels, estimator = rf, cv = 10)

# Check model performance for normal model and crossvalidated approach
pd.crosstab(test_labels, predictions, rownames=['True'], colnames=['Predicted'], margins=True)
pd.crosstab(labels, preds, rownames=['True'], colnames=['Predicted'], margins=True)

# Now first let's try and get the raw contributions of the individual features, univariate
prediction, bias, contributions = ti.predict(rf, features)

raw_contri = pd.DataFrame(contributions[:,:,1], columns = features.columns, 
                                  index = features.index)

issue_contri = raw_contri.loc[(issue_labels)]
res_df_one = pd.DataFrame(issue_contri.loc[:,:].mean(), columns = ['contribution'])
res_df_one['abs_contributions'] = abs(res_df_one['contribution'])
res_df_one['feature'] = res_df_one.index

# For results, it makes sense to also remove stuff with NUM:0 in them
#prob_index = [s for s in raw_contributions.index.tolist() if 'NUM:0' in s]
#raw_contributions = raw_contributions.drop(index = prob_index)
contributions = res_df_one

contributions = contributions.nlargest(n = 100, columns = 'abs_contributions')
contributions = pd.merge(contributions, all_uni_output[['feature', 'Color']], on = ['feature'])
#contributions = contributions[contributions['feature'].isin(all_uni_output['feature'].tolist())]
# Now let's create a color scheme for these features, so that we can make a better univariate importance plot
contri_list = pd.DataFrame(contributions.sort_values(by = 'contribution',ascending = True)['feature'].tolist(), columns = ["feature"])
contri_list['order'] = contri_list.index
# The names have to be arranged in reverse order of how they are while plotting

# Get the colors in the order of the contri_list
contributions = pd.merge(contributions, contri_list, how = 'inner', on = ['feature'])
contributions = contributions.sort_values(by = "order")
contributions.index = contributions['feature']
color_order = contributions['Color'].tolist()
contributions = contributions.loc[:,'contribution']
custom_legend = [Line2D([0], [0], color='b', lw=6),
                Line2D([0], [0], color='g', lw=6),
                Line2D([0], [0], color='r', lw=6)]
plt.figure(figsize = (30,contributions.shape[0]*0.25))
((contributions)*100).sort_values(ascending=True).plot(kind='barh', color = color_order)
plt.ylabel('Feature Name')
plt.xlabel('contribution')
plt.legend(custom_legend,['Design', 'Usage','Manufacturing'], loc = 'center right', markerscale = 50, fontsize = 'large', frameon = True)
plt.savefig('Downloads/Solve/multivariate_results/egr_cooler_results/univariate_contributions.png',bbox_inches='tight')
plt.close()

# Now share these top univariate importances
res_df_one['Rank'] = res_df_one['abs_contributions'].rank(method='dense', ascending=False)
res_df_one.to_csv("Downloads/Solve/multivariate_results/egr_cooler_results/24032018_univariate_contributons.csv")

# Now set up the tree interpreter for cross contributions, in order to spot most interesting interactions
prediction, bias, contributions = ti.predict(rf, test, joint_contribution=True)
aggregated_contributions = utils.aggregated_contribution(contributions)

columns = np.asarray(list(features.columns))
res = []
for k in set(aggregated_contributions.keys()):
    res.append(([columns[index] for index in k] , 
               aggregated_contributions.get(k, 0)))  

res = sorted(res, reverse = True)
res_df = pd.DataFrame(np.array(res), columns = list(["feature_combination","contribution"]))
res_df['feature_combination'] = res_df['feature_combination'].astype('str') 
res_df['contribution'] = [el[1] for el in res_df['contribution']]
res_df['comma_count'] = res_df.feature_combination.str.count(',')
res_df['abs_contributions'] = res_df.contribution.abs()
# res_df_uptothree = res_df[(res_df['comma_count'] > 0) & (res_df['comma_count'] < 3)].sort_values(by = ['abs_contributions'], ascending = False)
# res_df_uptothree = res_df_uptothree.drop('comma_count', axis = 1)
# res_df_uptothree = res_df_uptothree[res_df_uptothree.feature_combination.str.contains("NUM:0") == False]

res_df_two = res_df[(res_df['comma_count'] == 1)].sort_values(by = ['abs_contributions'], ascending = False)
res_df_three = res_df[(res_df['comma_count'] == 2)].sort_values(by = ['abs_contributions'], ascending = False)

# Clean these contribution dataframes to make them shareable
res_df_two = clean_contributions_df(contributions_df = res_df_two, feature_column = 'feature_combination', drop_columns = ['feature_combination', 'comma_count'])
res_df_three = clean_contributions_df(contributions_df = res_df_three, feature_column = 'feature_combination', drop_columns = ['feature_combination', 'comma_count'])

# The idea will be to take feature_1 and feature_2 columns and detect if they are Numerical or Categorical
res_df_two.loc[res_df_two.feature_1.isin(numeric_cols), 'feature_1_type'] = 'numerical'
res_df_two['feature_1_type'].fillna('categorical', inplace = True)
res_df_two.loc[res_df_two.feature_2.isin(numeric_cols), 'feature_2_type'] = 'numerical'
res_df_two['feature_2_type'].fillna('categorical', inplace = True)

res_df_three.loc[res_df_three.feature_1.isin(numeric_cols), 'feature_1_type'] = 'numerical'
res_df_three['feature_1_type'].fillna('categorical', inplace = True)
res_df_three.loc[res_df_three.feature_2.isin(numeric_cols), 'feature_2_type'] = 'numerical'
res_df_three['feature_2_type'].fillna('categorical', inplace = True)
res_df_three.loc[res_df_three.feature_3.isin(numeric_cols), 'feature_3_type'] = 'numerical'
res_df_three['feature_3_type'].fillna('categorical', inplace = True)

res_df_two['Rank'] = res_df_two['abs_contributions'].rank(method='dense', ascending=False)
res_df_two.to_csv("Downloads/Solve/multivariate_results/egr_cooler_results/24032018_bivariate_contributons.csv")
res_df_three.to_csv("Downloads/Solve/multivariate_results/egr_cooler_results/24032018_trivariate_contributons.csv")

# Now the idea is to pick these top combination of features and see if there is something interesting going on
# Pick up all two way interactions between features and categorize them into num x mum, cat x num, or cat x cat
# Then generate plots based on the type of feature combination that exists

# categorical x categorical
# The results will be a stacked bar plot and a table which shows the same info along with a couple other features like sample size and
# failure rate

catxcat = res_df_two[(res_df_two['feature_1_type'] == 'categorical') & (res_df_two['feature_2_type'] == 'categorical')]

for i in range(catxcat.shape[0]):

    feature1 = catxcat['feature_1'].tolist()[i]
    feature2 = catxcat['feature_2'].tolist()[i]
    cat_df = create_categorical_table(df = df_plots, feature1 = feature1, feature2 = feature2, label_column = 'Label')

    #fig, ax = plt.subplots()
    #p1 = ax.barh(cat_df['feature_combination'], cat_df['non-issue'])
    #p2 = ax.barh(cat_df['feature_combination'], cat_df['issue'], left = cat_df['non-issue'], color = "red")
    #ax.set_xlabel('Count')
    #plt.legend((p1[0], p2[0]), ('Non-issue', 'Issue'))
    #plt.title('Plot of ' + str(feature1) + ' vs. ' + str(feature2))
    #plt.savefig(('Downloads/Solve/multivariate_results/egr_cooler_results/catxcat/egr_cooler_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.png'),bbox_inches='tight')
    #plt.close()
    
    x_avg = len(df_plots[df_plots['Label'] == 1])*100/df_plots.shape[0]
    fig, ax = plt.subplots()
    p1 = ax.barh(cat_df['feature_combination'], cat_df['incident_rate'], color = "red", alpha = 0.5)
    
    for j in range(cat_df.shape[0]):
        ax.text(cat_df.loc[j,'incident_rate'] + 0.1, j, 'N: ' + str(int(cat_df.loc[j,'sample_size'])), color='black', fontweight='bold')
    
    ax.set_xlabel('Incident Rate (%)')
    plt.axvline(x = x_avg, linestyle = 'dashed')
    plt.text(x_avg + 0.1,2.2,'average incident rate', rotation = 90)

    #plt.legend((p1[1]), ('Incident Rate'))
    #plt.title('Incident rate for ' + str(feature1) + ' vs. ' + str(feature2))
    plt.savefig(('Downloads/Solve/multivariate_results/egr_cooler_results/catxcat/' + 'Rank' + str(catxcat['Rank'].tolist()[i]) + '_egr_cooler_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.png'),bbox_inches='tight', dpi = 900)
    plt.close()
    cat_df.to_csv('Downloads/Solve/multivariate_results/egr_cooler_results/catxcat/'  + 'Rank' + str(catxcat['Rank'].tolist()[i]) + '_egr_cooler_table_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.csv', na_rep = "NA")

# numerical vs numerical
# This will just be a scatter plot

numxnum = res_df_two[(res_df_two['feature_1_type'] == 'numerical') & (res_df_two['feature_2_type'] == 'numerical')]

for i in range(numxnum.shape[0]):
    feature1 = numxnum['feature_1'].tolist()[i]
    feature2 = numxnum['feature_2'].tolist()[i]
    
    fig, ax = plt.subplots()
    p1 = ax.scatter(df_plots.query("Label == 0")[feature1], 
           df_plots.query("Label == 0")[feature2], 
           c = 'blue', s = 10, marker = 'o')
    p2 = ax.scatter(df_plots.query("Label == 1")[feature1], 
           df_plots.query("Label == 1")[feature2], 
           c = 'red', s = 50, marker = 'X')
    
    count_p1 = sum((df_plots.query("Label == 0")[feature1].notnull()) & (df_plots.query("Label == 0")[feature2].notnull()))
    count_p2 = sum((df_plots.query("Label == 1")[feature1].notnull()) & (df_plots.query("Label == 1")[feature2].notnull()))
    

    plt.legend((p1, p2), ('Non-issue ' + '(Count:' + str(count_p1) + ')', 'Issue ' + '(Count:' + str(count_p2) + ')'), frameon = True)
    #plt.title('Plot of ' + str(feature1) + ' vs. ' + str(feature2), fontsize = 11)
    plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
    plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
    plt.savefig(('Downloads/Solve/multivariate_results/egr_cooler_results/numxnum/' + 'Rank' + str(numxnum['Rank'].tolist()[i]) + '_egr_cooler_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.png'),bbox_inches='tight', dpi = 900)
    plt.close()


# numerical vs categorical
# This can be a violin plot or a boxplot
# Generate first all the boxplots

catxnum = res_df_two[((res_df_two['feature_1_type'] == 'numerical') & (res_df_two['feature_2_type'] == 'categorical')) | ((res_df_two['feature_1_type'] == 'categorical') & (res_df_two['feature_2_type'] == 'numerical')) ]

for i in range(catxnum.shape[0]):
    if(catxnum['feature_1_type'].tolist()[i] == 'numerical'):
        feature2 = catxnum['feature_1'].tolist()[i]
        feature1 = catxnum['feature_2'].tolist()[i]
    else:
        feature1 = catxnum['feature_1'].tolist()[i]
        feature2 = catxnum['feature_2'].tolist()[i]
    
    
    sns.set_style("darkgrid")
    df_plots_subset = df_plots.loc[(df_plots[feature1].notnull()) & (df_plots[feature2].notnull()), :]
    ax = sns.boxplot(x= feature1, y= feature2, data = df_plots_subset, hue = "Label", 
                     palette = ['blue','red'], saturation = 0.5, fliersize = 2)
    handles, labels = ax.get_legend_handles_labels()
    labels = ['Non-issue', 'Issue']
    plt.legend(handles[0:2], labels[0:2], frameon = True)
    #plt.title('Plot of ' + str(feature1) + ' vs. ' + str(feature2))
    
    
    medians = df_plots_subset.groupby([feature1,'Label'])[feature2].median().values.tolist()
    
    nobs = []
    for k in df_plots_subset[feature1].astype('category').cat.categories.tolist():
        
        if (len(df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].astype('category').cat.categories.tolist()) == 2):
            counts = df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
            nobs.extend(counts)
        elif ((df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].astype('category').cat.categories.tolist()[0] == 0)):
            counts = df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
            counts.append(0)
            nobs.extend(counts)
        else:
            counts = [0]
            element = df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
            counts.extend(element)
            nobs.extend(counts)
    
    x = [i for i, e in enumerate(nobs) if e == 0]
    for p in x:
        medians.insert(p,0.0)

    pos = list([-0.2, 0.2, 0.8,1.2])
  
    for j in range(len(nobs)):
        ax.text(pos[j], medians[j] + 0.05, 'N: ' + str(nobs[j]),
                horizontalalignment='center', size= "medium", color='black', weight='bold')
        
    plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
    plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
    plt.savefig(('Downloads/Solve/multivariate_results/egr_cooler_results/catxnum/'  + 'Rank' + str(catxnum['Rank'].tolist()[i]) + '_egr_cooler_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.png'),bbox_inches='tight', dpi = 900)
    plt.close()

# Now it also makes sense to plot a few charts which takes the features directly from the user, and creates the chart accordingly
data_new = df_no_onehot.drop(columns_to_remove, axis = 1)

data_new = data_new.rename(columns = {'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3/S':\
                          'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3S',\
                          'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3/S':\
                          'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3S'})

# Now normalize the numeric columns of this 'normalized' dataset
    
numeric_cols.remove('INS_TI_IDLE_TIME_PERCENTAGE')
data_new_numeric = data_new[numeric_cols]
# Take care of error values in the raw data
for col in tqdm(numeric_cols):
    if data_new_numeric[col].dtype not in ['float64', 'float32']:
        data_new_numeric[col] = data_new_numeric[col].str.replace(',','.')
        data_new_numeric[col] = data_new_numeric[col].astype(float)
    if 'PERCENT' in col:
        data_new_numeric[col][data_new_numeric[col] < 0] = float('NaN')
        data_new_numeric[col][data_new_numeric[col] > 100] = float('NaN')
    else:
        data_new_numeric[col][data_new_numeric[col] < -10**6] = float('NaN')
        data_new_numeric[col][data_new_numeric[col] > 10**6] = float('NaN')

data_new_numeric = normalize_columns(data_new_numeric)
data_new_cat_cols = data_new.drop(numeric_cols, axis = 1)
data_new = pd.concat([data_new_numeric, data_new_cat_cols], axis = 1)

data_new_x3 = data_new.loc[data_new['REL_ANALYSIS_RATE_CAT'] == 'X3 2017'].reset_index(drop = True) # Take only X3 data into consideration
data_new_x3.loc[data_new_x3['ESN'].isin(egr_df['ESN']), 'Label'] = 1
data_new_x3['Label'].fillna(0, inplace = True)

create_plot(df = df_plots, feature1 = 'OEPL_BRAKE_ENGINE:EB1207', feature2 = 'OEPL_MOUNTING_REF_COMPRESSOR:CF1072', label_column = 'Label', output_destination = 'Desktop')
