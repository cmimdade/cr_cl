#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 12:23:18 2018

@author: Nimit Patel
"""

"""
AIM

The aim of this code is to set up a skeleton which will conduct the residual analysis for the incidents.
Here the analysis means that after the output of Detect comes into Solve, this piece will look for any incidents
that have been mis-labeled. It will flag those incidents, and they will undergo an investigation by the SME
"""

"""
PROCESS

The process here will be to build a cross-validated classification model, which will try to predict in its
last fold wether a particular incident falls into a certain class/cluster or not. This will be done over a large
number of iterations, and each time the incidents whcih are labelled incorrectly will be flagged. Finally, 
the incidents which get mis-labelled a lot of times will be presented to an SME for investigation.
"""
import random
import math
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_predict
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import scale
from openpyxl import load_workbook
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from random import sample
from sklearn.model_selection import cross_val_score
from itertools import compress


# Read in the Fat file
df = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/fat_table_with_one_hot.csv")
#df = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/fat_table_with_fs.csv")

# Load new EGR Info
egr_df = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/egr_cooler_updated_esns.csv", names = ['ESN'])

# Load the file containing important features for the EGR cooler issue
egr_features = pd.ExcelFile("Downloads/Solve/OneDrive_1_3-11-2018/variable_results_egr_issue.xlsx")
egr_features.sheet_names

# Now from this file, pick up top 5 features for each of REL, MES, INS and OEPL
rel_top = list(egr_features.parse('REL').head(n = 5)['Variable'])
mes_top = list(egr_features.parse('MES').head(n = 5)['Variable'])
ins_top = list(egr_features.parse('INS').head(n = 5)['Variable'])
oepl_top = list(egr_features.parse('OEPL').head(n = 5)['Variable'])

# There is another file by Sanjay that gives the top important features, and take only non-MES ones from them
top_features = pd.ExcelFile("Downloads/Solve/OneDrive_1_3-11-2018/variable_results_egr_cooler_031318.xlsx")
features = list(top_features.parse('VAR').head(n = 40)['Variable'])
for word in features[:]:
    if word.startswith(tuple(['MES'] + ['EPAT'])):
        features.remove(word)

# Use the ESNs from the EGR file and mark those incidents as Label 1, while others as zero. This basically creates
# the label that we want to work with
df.loc[df['ESN'].isin(egr_df['ESN']), 'Label'] = 1
df['Label'].fillna(0, inplace = True)

# Split the dataframe into output variable (label) and other features
features = df.loc[:,rel_top + mes_top + oepl_top + ins_top]
labels = df['Label']

# Take a look at the number of NaNs in the feature dataset, and keep columns with no NaN values
features.isnull().sum()
mask = features.isnull().any()
features = df[mask.index[mask == False]]

# Split our data
train, test, train_labels, test_labels = train_test_split(features,
                                                          labels,
                                                          test_size = 0.20,
                                                          random_state = 42)

# Check number of actual issues in each of these sets
sum(train_labels == 1)
sum(test_labels == 1)

# Initialize our classifiers
gnb = GaussianNB()
logreg = LogisticRegression(class_weight = 'balanced', max_iter = 100)

# Train our classifiers
model1 = gnb.fit(train, train_labels)
model2 = logreg.fit(train, train_labels)

# Make predictions and see the accuracy
preds1 = gnb.predict(test)
preds2 = logreg.predict(test)

# Print accuracy scores
print(accuracy_score(test_labels, preds1))
print(accuracy_score(test_labels, preds2))

# Print the confusion matrices for both these methods
pd.crosstab(test_labels, preds1, rownames=['True'], colnames=['Predicted'], margins=True)
pd.crosstab(test_labels, preds2, rownames=['True'], colnames=['Predicted'], margins=True)

"""
It is clearly visible that the Naive Bayes performed better, as the number of False Positives was way less.
Also, there is very little to be gained by using the entire set of incidents as the negative set. So there needs
to be implemented a good way of under-sampling the rest of the incidents to find the most usable negative set.
That is where the spy algorithm will be extremely useful!
"""


# Implement the spy algorithm in order to find the best negative set to use here

# Create a master table which contains only the useful features as above, also all the incidents have a label already
#columns = list(features)
#columns.append("Label")
#df_master = df.loc[:,columns]
# Select the columns which are the set of top important features
columns = rel_top + mes_top + oepl_top + ins_top

# Select important features from the top_features file
columns = features

columns.extend(["Label", "ESN", "EARLIEST_INDICATION_DATE", "INCDT_ISSUE_NUMBER"])
df_master = df.loc[:,columns]
df_master = df_master.dropna(axis = 1, how = 'all')
df_master['index'] = df_master.index

# Create the issue and non-issue dataframes to play with
df_issue = df_master[df_master.Label == 1]
df_non_issue = df_master[df_master.Label == 0]

# Hyperparameters
# s - percentage of + examples to put in unlabeled set
# l - percentage of spy's to be below threshold t
s = 0.25
l = .05

# Randomly sample spy observations
spy_idx = sample(range(df_issue.shape[0]), int(df_issue.shape[0]*s))
spy_obs = df_issue.iloc[spy_idx,:]
non_spy_obs = df_issue[~df_issue.index.isin(spy_obs.index)]

#Add label to identify spy's
spy_obs['Spy'] = 1
non_spy_obs['Spy'] = 0
df_non_issue['Spy'] = 0
df_issue['Spy'] = 0

#Add spy observations to non-issue unlabeled set
df_non_issue = pd.concat([df_non_issue, spy_obs])
df_non_issue['Label'] = 0

#Concatenate everything!
df_all = pd.concat([non_spy_obs, df_non_issue]).reset_index(drop = True)

# Store the index column, in order to remember the order of incidents in this data
incidents_index = df_all['index']

y = df_all['Label']
spy = df_all['Spy']
df_all_other = df_all[['Label', 'ESN', 'Spy','index', 'EARLIEST_INDICATION_DATE', 'INCDT_ISSUE_NUMBER']]
del df_all['Label'], df_all['Spy'], df_all['ESN'], df_all['index'], df_all['EARLIEST_INDICATION_DATE'], df_all['INCDT_ISSUE_NUMBER']

for col in range(0,len(df_all.columns)):
    finite_vals = df_all[df_all.columns[col]].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        df_all.iloc[finite_vals.index.values, np.where(df_all.columns.values == df_all.columns[col])[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    df_all[df_all.columns[col]] = df_all[df_all.columns[col]].replace(np.nan, 0)


#Identify indices in unlabeled set that are NOT spys
#Will need to filter on these when identifying negative labels 
no_spy_no_issue = (spy == 0) == (y == 0)
no_spy_no_issue = no_spy_no_issue.index[no_spy_no_issue].values

## Fit Weighted Logistic Regression ##

clf = LogisticRegressionCV(penalty = 'l2', class_weight = 'balanced', max_iter = 10, cv = 5)
clf.fit(df_all, y)

# Get class probabilities for each observations
probs = clf.predict_proba(df_all)
probs = probs[:,1]

#Sort spy probabilities
sorted_spy_probs = np.sort(probs[np.where(spy == 1)[0]])

#Identify threshold under which we have reliable negatives
def findThreshold(l):
    thresholds = np.arange(0, 1, .001)
    for t in thresholds:
        if np.mean(sorted_spy_probs < t) > l:
            return(t)    
    
t = findThreshold(l)

# Now combine the probs to the overall dataframe, and see which spy incidents actually got a very low probability
df_all = pd.concat([df_all_other, df_all], axis = 1)
df_with_probs = df_all
df_with_probs['probs'] = probs
df_with_probs['original_index'] = incidents_index

# Now we can take a look at the spy datapoints and their corresponding probabilities
#temp = df_with_probs.loc[df_with_probs['original_index'].isin(spy_obs.index)]

# Identify Reliable Negative Incidents
reliable_N = np.where(probs < t)[0]
reliable_N = np.intersect1d(reliable_N, no_spy_no_issue)
df_reliable_N = df_all.iloc[reliable_N, :].reset_index(drop = True)

del df_issue['Spy']
df_reliable_N['Label'] = 0
df_master = pd.concat([df_issue, df_reliable_N]).reset_index(drop = True)
del df_master['index'], df_master['original_index'], df_master['probs']

# Split our data
# train, test, train_labels, test_labels = train_test_split(features,
#                                                         labels,
#                                                          test_size = 0.25,
#                                                          random_state = 42)

"""
This is the point where we start using the dataset obtaine das a result of the Spy algorithm process, and do our
crossvalidated prediction on the dataset, and try to run that for multiple iterations, in order to find the 
incidents which get mislabeled repeatedly. This should essentially give us an idea of which incidents are worth 
flagging in the output
"""

# Separate the features and the labels for the model
features = df_master[df_master.columns.difference(['Label', 'ESN', 'INCDT_ISSUE_NUMBER', 'EARLIEST_INDICATION_DATE'])]
labels = df_master['Label']

# Now these features need to undergo normalization, and get rid of the NAs by imputing the means
for col in range(0,len(features.columns)):
    finite_vals = features[features.columns[col]].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        features.iloc[finite_vals.index.values, np.where(features.columns.values == features.columns[col])[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    features[features.columns[col]] = features[features.columns[col]].replace(np.nan, 0)

# Initialize our classifier
logreg = LogisticRegression(class_weight = 'balanced', max_iter = 100, penalty = 'l2', solver = 'liblinear')
logregcv = LogisticRegressionCV(Cs = 10, cv = 4, penalty = "l2", solver = "liblinear", class_weight = 'balanced')

# Train our classifier over a 5-fold crossvalidation on the dataset, and make predictions
# For this we use the cross_val_predict function with cv = 5. As the output label here is binomial, the 
# default crossvalidation strategy chosen will be a Stratified K-fold crossvalidation, which will take care of the
# huge class imbalance we have. Essentially, it will maintain the ratio of negative to positive incidents in
# all of the folds that it generates!

preds = cross_val_predict(X = features, y = labels, estimator = logregcv, cv = 5)
df_master['prediction'] = pd.Series(preds, index=df_master.index)

# Now we need to highlight the data points which were predicted to be the opposite set by the algorithm
df_master.index[df_master['prediction'] != df_master['Label']].tolist()

# Print the confusion matrix
pd.crosstab(df_master['Label'], df_master['prediction'], rownames=['Actual'], colnames=['Predicted'], margins=True)

# Fit a simple logreg model with the specifications as above and try to see the coefficients
model1 = logregcv.fit(X = features, y = labels)
features.columns
model1.coef_
cross_val_score(model1, features, labels, cv = 5, scoring = 'roc_auc').mean()
print ('Max auc_roc:', logregcv.scores_[1].max())
probs = logregcv.predict_proba(features)
roc_auc_score(df_master['Label'], df_master['prediction'])

# Try fitting some random incidents into the negative set and see if the algorithm detects them

issues_index = df_master.index[df_master['Label'] == 1].tolist()
random_set = random.sample(issues_index, 5)
df_master.Label[random_set] = 0

preds = cross_val_predict(X = features, y = labels, estimator = logreg, cv = 5)
df_master['prediction'] = pd.Series(preds, index=df_master.index)

# Try this entire approach on the entire data, and not the subset given through spy algorithm
df_full = df.loc[:,columns]
df_full = df_full.dropna(axis = 1, how = 'all')

features = df_full[df_full.columns.difference(['Label', 'ESN', 'INCDT_ISSUE_NUMBER', 'EARLIEST_INDICATION_DATE'])]
labels = df_full['Label']

# Now these features need to undergo normalization, and get rid of the NAs by imputing the means
for col in range(0,len(features.columns)):
    finite_vals = features[features.columns[col]].dropna()
    if len(finite_vals) > 0:
        scaled_vals = scale(finite_vals)
        features.iloc[finite_vals.index.values, np.where(features.columns.values == features.columns[col])[0][0]] = scaled_vals
    
    #Replace NaN with 0's (Mean)
    features[features.columns[col]] = features[features.columns[col]].replace(np.nan, 0)

preds = cross_val_predict(X = features, y = labels, estimator = logregcv, cv = 5)
df_full['prediction'] = pd.Series(preds, index=df_full.index)
df_full.index[df_full['prediction'] != df_full['Label']].tolist()
pd.crosstab(df_full['Label'], df_full['prediction'], rownames=['True'], colnames=['Predicted'], margins=True)

# Try a simple model, with no cross validations, just train test split and model building
# Split our data
train, test, train_labels, test_labels = train_test_split(features,
                                                          labels,
                                                          test_size = 0.20,
                                                          random_state = 777)

# Train our classifier
model1 = logregcv.fit(train, train_labels)

# Make predictions and see the accuracy
preds1 = model1.predict(test)
pd.crosstab(test_labels, preds1, rownames=['True'], colnames=['Predicted'], margins=True)
