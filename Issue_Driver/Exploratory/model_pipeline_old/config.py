#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 15:07:41 2018

@author: pr891
"""
#path to write all files and where to read from
path_base = '/Users/pr891/Desktop/Data/'

#path to .parquet
#path_online_tracker = path_base + 'INS_FAULT_SNAPSHOT_MDA_FEATURES/'
path_online_tracker = path_base + 'QUALITY_FEATURES_PARQUET_NEW'

#path to online incident tracker
path_engine_options = path_base + 'RLD_ENGINE_OPTION_ASSEMBLY_x15'

#path to FAULT SNAPSHOT 
path_fault_snap = path_base + 'INS_FAULT_SNAPSHOT_MDA_FEATURES/'

#path option Description File
path_oepl_desc = path_base + 'ISX_Options.csv'

#path to fault code look up
path_fault_look = path_base + 'fault_code_lookup_solve.csv'

#path to extra data pickle file
path_extra_pickle = path_base + "universal_view_cols.p"

#define col name for fault codes
fault_code_col_name = 'GREEN_WRENCH_FAULT_CODE_LIST'

#path to full build population
path_to_build_pop = path_base + 'full_build_population.csv'

#set global data bounds
global_lower = -10**6
global_upper = 10**6