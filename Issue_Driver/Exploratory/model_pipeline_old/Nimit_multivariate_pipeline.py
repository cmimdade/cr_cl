#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  2 11:50:27 2018

@author: Nimit Patel
"""

"""
--- AIM ---

The aim of this code is to implement multivariate analysis on various issues (either coming out of the Detect phase or provided
by the SMEs), using the features available at the end of first layer selection and confounder analysis. The idea is to show
interaction between variables, and how that has an impact on the engine in terms of that particular issue (meaning does it
increase or decrease the chances of a particular issue happening) 

--- PROCESS ---

The process here will be to build a randomforest model, put it through the tree interpreter, and see which combination of features
turn up as important, and what is their combined impact on the probability of an issue occuring

--- HOW TO READ THIS CODE ---

This code consists of the following steps:
    1. Load the datasets needed (two datasets, one for modelling, other for plotting). Also load the output from Univariate analysis
    2. Clean the datasets to remove engines that ran less than 1500 miles (Ideally this should be done in the Univariate analysis)
    3. Train the randomforest classifier for this particular issue (here the model has horrible performance. Need to improve that)
    4. Build a tree interpeter without interactions in the output and show results of univariate contributions in form of table and plot
    5. Build a tree interpreter with interactions and show bivariate and trivariate interactions in a table
    6. Plot bivariate interaction plots for all combination of feature types (cat vs cat, num vs num, cat vs num)
    
"""


#%%
from treeinterpreter import treeinterpreter as ti, utils
from sklearn.metrics import r2_score
from sklearn.metrics import make_scorer
from sklearn.model_selection import cross_val_score
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestClassifier
import numpy as np
import pandas as pd
from sklearn.preprocessing import scale
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
import seaborn as sns
from tqdm import tqdm
from pylab import rcParams
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
import datetime


# In simple sense, there is only need of two datasets to do the analysis of multivariate
# 1. The dataset which has no spurious values, null values have been imputed, numerical columns are normalized
# 2. The dataset which has no spurious values, null values are not removed, and numerical columns are not normalized
# Both datasets only have the columns remaining after the first pass removal of columns

# Read in both above needed datasets
# Data which will go into analysis, which is normalized
df = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/EGR_Flow_one_hot_norm_reduced.csv")
# Data which will be used for plots
df_plots = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/EGR_Flow_one_hot_raw.csv")
# Read in file containing columns to be removed. These are features generated after the engine comes in for fixing, so they aren't useful for diagnosing an issue.
columns_to_remove = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/columns_to_remove.csv")['columns_to_remove'].tolist()
# Read in the output generated by Sanjay at the end of univariate analysis
uni_output = pd.ExcelFile("Downloads/Solve/OneDrive_1_3-11-2018/variable_results_egr_flow_040218.xlsx")


# Now filter out engines which are less than 1500 miles. This should technically be done in the Univariate analysis.
normalized_limit = (1500 - df_plots['INS_TI_ENGINE_DISTANCE_MILES'].mean())/df_plots['INS_TI_ENGINE_DISTANCE_MILES'].std()
df_plots = df_plots[(df_plots['INS_TI_ENGINE_DISTANCE_MILES'] >= 1500) | (df_plots['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]
df = df[(df['INS_TI_ENGINE_DISTANCE_MILES'] >= normalized_limit) | (df['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]

# Determine features in univariate output. Also create a color column which will be used to color the bars in a subsequent bar plot
# based on the source from which the feature is obtained
# It would be useful if the output generated through this next block of code is also generated at the end of the Univariate analysis
# done by Sanjay. Essentially, instead of having features divided in the excel sheets by Design, Usage and Manuf, for this piece of analysis
# we just need them all in one sheet. Otherwise we can just keep the below snippet of code as part of the multivariate, to generate 
# the table we need from the output from Univariate analysis.
design = pd.read_excel(uni_output, 'Design')
design['Source'] = 'Design'
manuf = pd.read_excel(uni_output, 'Manufacturing')
manuf['Source'] = 'Manufacturing'
usage = pd.read_excel(uni_output, 'Usage')
usage['Source'] = 'Usage'
all_uni_output = pd.concat([design, manuf, usage], axis = 0)
all_uni_output = all_uni_output[['Variable','Source']]
all_uni_output['Color'] = 'blue'
all_uni_output['Color'][all_uni_output['Source'] == 'Manufacturing'] = 'red'
all_uni_output['Color'][all_uni_output['Source'] == 'Usage'] = 'green'
all_uni_output = all_uni_output.rename(columns = {'Variable':'feature'})
all_uni_output = all_uni_output[['feature', 'Color']]

#%%
# Remove features not needed
df_master = remove_unwanted_features(input_data = df, exclude_features = ['MES','EPAT'], columns_to_remove = columns_to_remove)

# Initialize the randomforest classifier 
rf = RandomForestClassifier(random_state = 777, n_estimators = 100, verbose = True, class_weight = "balanced",
                            n_jobs = 8, min_samples_split = 15)

# See how this is going to perform for our data. This will print the confusion matrix and r-squared
test_model_performance(input_data = df_master, id_features = ['id', 'Label','ESN', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'],
                       label_column = 'Label', classifier = rf, cv = 5)


#%%
res_df_one = implement_tree_interpreter_univariate(input_data = df_master, id_features = ['id', 'Label','ESN', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'], 
                                      label_column = 'Label', classifier = rf)

# Generate the plot for this univariate analysis. The idea is to also have the bars color coded based on their source
contributions = res_df_one
contributions = contributions.nlargest(n = 100, columns = 'abs_contributions')
contributions = pd.merge(contributions, all_uni_output[['feature', 'Color']], on = ['feature'])
contri_list = pd.DataFrame(contributions.sort_values(by = 'contribution',ascending = True)['feature'].tolist(), columns = ["feature"])
contri_list['order'] = contri_list.index

contributions = pd.merge(contributions, contri_list, how = 'inner', on = ['feature'])
contributions = contributions.sort_values(by = "order")
contributions.index = contributions['feature']
color_order = contributions['Color'].tolist()
contributions = contributions.loc[:,'contribution']
plt.figure(figsize = (30,contributions.shape[0]*0.25))
((contributions)*100).sort_values(ascending=True).plot(kind='barh', color = color_order)
plt.ylabel('Feature Name')
plt.xlabel('contribution (%)')
custom_legend = [Line2D([0], [0], color='b', lw=6),
                Line2D([0], [0], color='g', lw=6),
                Line2D([0], [0], color='r', lw=6)]
plt.legend(custom_legend,['Design', 'Usage','Manufacturing'], loc = 'center right', markerscale = 50, fontsize = 'large', frameon = True)
plt.savefig("Downloads/Solve/multivariate_results/egr_flow_results/" + str(datetime.date.today()).replace("-","") + "_univariate_contributions.png",bbox_inches="tight")
plt.close()
#%%
# Generate the table outputs from multivariate analysis
res_df_two, res_df_three = implement_tree_interpreter_multivariate(input_data = df_master, id_features = ['id', 'Label','ESN', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'], 
                                        label_column = 'Label', classifier = rf)

res_df_one.to_csv("Downloads/Solve/multivariate_results/egr_flow_results/" + str(datetime.date.today()).replace("-","") + "_univariate_contributons.csv")
res_df_two.to_csv("Downloads/Solve/multivariate_results/egr_flow_results/" + str(datetime.date.today()).replace("-","") + "_bivariate_contributons.csv")
res_df_three.to_csv("Downloads/Solve/multivariate_results/egr_flow_results/" + str(datetime.date.today()).replace("-","") + "_trivariate_contributons.csv")

#%%
# Now the idea is to pick these top combination of features and see if there is something interesting going on
# Pick up all two way interactions between features and categorize them into num x mum, cat x num, or cat x cat
# Then generate plots based on the type of feature combination that exists

# categorical x categorical
# The results will be a stacked bar plot and a table which shows the same info along with a couple other features like sample size and
# failure rate

catxcat = res_df_two[(res_df_two['feature_1_type'] == 'categorical') & (res_df_two['feature_2_type'] == 'categorical')]

for i in range(catxcat.shape[0]):

    feature1 = catxcat['feature_1'].tolist()[i]
    feature2 = catxcat['feature_2'].tolist()[i]
    cat_df = create_categorical_table(df = df_plots, feature1 = feature1, feature2 = feature2, label_column = 'Label')
    
    x_avg = len(df_plots[df_plots['Label'] == 1])*100/df_plots.shape[0]
    fig, ax = plt.subplots()
    p1 = ax.barh(cat_df['feature_combination'], cat_df['incident_rate'], color = "red", alpha = 0.5)
    
    for j in range(cat_df.shape[0]):
        ax.text(cat_df.loc[j,'incident_rate'] + 0.1, j, 'N: ' + str(int(cat_df.loc[j,'sample_size'])), color='black', fontweight='bold')
    
    ax.set_xlabel('Incident Rate (%)')
    plt.axvline(x = x_avg, linestyle = 'dashed')
    plt.text(x_avg + 0.1,2.2,'average incident rate', rotation = 90)

    plt.savefig(('Downloads/Solve/multivariate_results/egr_flow_results/catxcat/' + 'Rank' + str(catxcat['Rank'].tolist()[i]) + '_egr_cooler_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.png'),bbox_inches='tight', dpi = 900)
    plt.close()
    cat_df.to_csv('Downloads/Solve/multivariate_results/egr_flow_results/catxcat/'  + 'Rank' + str(catxcat['Rank'].tolist()[i]) + '_egr_cooler_table_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.csv', na_rep = "NA")

# numerical vs numerical
# This will just be a scatter plot

numxnum = res_df_two[(res_df_two['feature_1_type'] == 'numerical') & (res_df_two['feature_2_type'] == 'numerical')]

for i in range(numxnum.shape[0]):
    feature1 = numxnum['feature_1'].tolist()[i]
    feature2 = numxnum['feature_2'].tolist()[i]
    
    fig, ax = plt.subplots()
    p1 = ax.scatter(df_plots.query("Label == 0")[feature1], 
           df_plots.query("Label == 0")[feature2], 
           c = 'blue', s = 10, marker = 'o')
    p2 = ax.scatter(df_plots.query("Label == 1")[feature1], 
           df_plots.query("Label == 1")[feature2], 
           c = 'red', s = 50, marker = 'X')
    
    count_p1 = sum((df_plots.query("Label == 0")[feature1].notnull()) & (df_plots.query("Label == 0")[feature2].notnull()))
    count_p2 = sum((df_plots.query("Label == 1")[feature1].notnull()) & (df_plots.query("Label == 1")[feature2].notnull()))
    

    plt.legend((p1, p2), ('Non-issue ' + '(Count:' + str(count_p1) + ')', 'Issue ' + '(Count:' + str(count_p2) + ')'), frameon = True)
    #plt.title('Plot of ' + str(feature1) + ' vs. ' + str(feature2), fontsize = 11)
    plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
    plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
    plt.savefig(('Downloads/Solve/multivariate_results/egr_flow_results/numxnum/' + 'Rank' + str(numxnum['Rank'].tolist()[i]) + '_egr_cooler_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.png'),bbox_inches='tight', dpi = 900)
    plt.close()


# numerical vs categorical
# This can be a violin plot or a boxplot
# Generate first all the boxplots

catxnum = res_df_two[((res_df_two['feature_1_type'] == 'numerical') & (res_df_two['feature_2_type'] == 'categorical')) | ((res_df_two['feature_1_type'] == 'categorical') & (res_df_two['feature_2_type'] == 'numerical')) ]

for i in range(catxnum.shape[0]):
    if(catxnum['feature_1_type'].tolist()[i] == 'numerical'):
        feature2 = catxnum['feature_1'].tolist()[i]
        feature1 = catxnum['feature_2'].tolist()[i]
    else:
        feature1 = catxnum['feature_1'].tolist()[i]
        feature2 = catxnum['feature_2'].tolist()[i]
    
    
    sns.set_style("darkgrid")
    df_plots_subset = df_plots.loc[(df_plots[feature1].notnull()) & (df_plots[feature2].notnull()), :]
    ax = sns.boxplot(x= feature1, y= feature2, data = df_plots_subset, hue = "Label", 
                     palette = ['blue','red'], saturation = 0.5, fliersize = 2)
    handles, labels = ax.get_legend_handles_labels()
    labels = ['Non-issue', 'Issue']
    plt.legend(handles[0:2], labels[0:2], frameon = True)
    medians = df_plots_subset.groupby([feature1,'Label'])[feature2].median().values.tolist()
    
    nobs = []
    for k in df_plots_subset[feature1].astype('category').cat.categories.tolist():
        
        if (len(df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].astype('category').cat.categories.tolist()) == 2):
            counts = df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
            nobs.extend(counts)
        elif ((df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].astype('category').cat.categories.tolist()[0] == 0)):
            counts = df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
            counts.append(0)
            nobs.extend(counts)
        else:
            counts = [0]
            element = df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
            counts.extend(element)
            nobs.extend(counts)
    
    x = [i for i, e in enumerate(nobs) if e == 0]
    for p in x:
        medians.insert(p,0.0)

    pos = list([-0.2, 0.2, 0.8,1.2])
  
    for j in range(len(nobs)):
        ax.text(pos[j], medians[j] + 0.05, 'N: ' + str(nobs[j]),
                horizontalalignment='center', size= "medium", color='black', weight='bold')
        
    plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
    plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
    plt.savefig(('Downloads/Solve/multivariate_results/egr_flow_results/catxnum/'  + 'Rank' + str(catxnum['Rank'].tolist()[i]) + '_egr_cooler_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") + '.png'),bbox_inches='tight', dpi = 900)
    plt.close()

#%%