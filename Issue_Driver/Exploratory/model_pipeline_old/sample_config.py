#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 20:23:29 2018

@author: pr891
"""

## Hyperparameters ##
issue_name = 'sample name'
engine_type = ['ISX1 2017', 'ISX3 2017']
esn_list = 'X15 2017 Infant Care PPS Issues.xlsx'
incident_path = "/Users/sanjay/Desktop/Data/{}"
esn_file_type = "excel"
esn_sheet = "sample sheet name" #leave pblank if it is a csv file or other non-excel format
path_to_results = '/Users/sanjay/Desktop/Solve_Results/'

incident_merge_col = ['ESN', 'Earliest Indication Date']
base_merge_col = ['ESN', 'EARLIEST_INDICATION_DATE']

max_num_cores = 8

#data cleaning thresholds
no_coverage_perc_thresh = 0.05
coef_of_var_thresh = 0.03
min_max_thresh = 0.0002

#min and max settings
mileage_min = 1500
mileage_col = 'INS_TI_ENGINE_DISTANCE_MILES'

#set global data bounds
global_lower = -10**6
global_upper = 10**6

#file paths for issues and non_issue
file_one_hot_raw = "/Users/sanjay/Desktop/Data/fat_table_with_one_hot_raw.csv"
file_no_one_hot_raw = "/Users/sanjay/Desktop/Data/fat_table_no_one_hot_raw.csv"

#paths for processed data
path_processed_norm = "/Users/sanjay/Downloads/Solve/OneDrive_1_3-11-2018/EGR_Flow_one_hot_norm_reduced.csv"
path_processed_raw = "/Users/sanjay/Downloads/Solve/OneDrive_1_3-11-2018/EGR_Flow_one_hot_raw.csv"
path_columns_to_remove = "/Users/sanjay/Downloads/Solve/OneDrive_1_3-11-2018/columns_to_remove.csv"
path_output = "/Users/sanjay/Downloads/Solve/OneDrive_1_3-11-2018/variable_results_egr_flow_040218.xlsx"
path_univariate_contributons = "/Users/sanjay/Desktop/Solve_Results/EGR_Cooler/Data/EGR_Cooler_univariate_contributons.csv"

#path to write all files and where to read from
path_base = '/Users/sanjay/Desktop/Data/'

#path to .parquet
#path_online_tracker = path_base + 'INS_FAULT_SNAPSHOT_MDA_FEATURES/'
path_online_tracker = path_base + 'QUALITY_FEATURES_PARQUET_NEW'

#path to online incident tracker
path_engine_options = path_base + 'RLD_ENGINE_OPTION_ASSEMBLY_x15'

#path to FAULT SNAPSHOT 
path_fault_snap = path_base + 'INS_FAULT_SNAPSHOT_MDA_FEATURES/'

#path option Description File
path_oepl_desc = path_base + 'ISX_Options.csv'

#path to fault code look up
path_fault_look = path_base + 'fault_code_lookup_solve.csv'

#path to extra data pickle file
path_extra_pickle = path_base + "universal_view_cols.p"

#define col name for fault codes
fault_code_col_name = 'GREEN_WRENCH_FAULT_CODE_LIST'

#path to full build population
path_to_build_pop = path_base + 'full_build_population.csv'