#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 15 13:25:25 2018

@author: Nimit Patel
"""
"""
This code mainly contains all the functions that need to be used for conducting the residual analysis
Preceding each function will be a short description of what is will be used for, and any other comments
"""

# This function will essentially help with selecting the set of features to use for the analysis, as well as any other
# informative features regarding the incidents which will not be used in the model building, but should be present in the 
# dataset just to enhance the understanding of the results while viewing them.

def remove_unwanted_features(input_data, exclude_features, columns_to_remove):
    
    # Remove columns about price and amounts
    df_master = input_data.drop(columns_to_remove, axis = 1)
    df_master['id'] = df_master.index
    
    # Keep only the features other than MES and EPAT
    features_to_keep = list(df_master.columns)
    # Remove above mentioned
    for word in features_to_keep[:]:
        if word.startswith(tuple(exclude_features)):
            features_to_keep.remove(word)
    
    # Keep only features other the ones removed above
    df_master = df_master.loc[:,features_to_keep]
    
    return(df_master)
    

#Identify threshold under which we have reliable negatives
def findThreshold(l, sorted_spy_probs):
    thresholds = np.arange(0, 1, .001)
    for t in thresholds:
        if np.mean(sorted_spy_probs < t) > l:
            return(t)
            

def normalize_columns(df):
    for col in range(0,len(df.columns)):
        finite_vals = df[df.columns[col]].dropna()
        if len(finite_vals) > 0:
            scaled_vals = scale(finite_vals)
            df.iloc[finite_vals.index.values, np.where(df.columns.values == df.columns[col])[0][0]] = scaled_vals
        
        #Replace NaN with 0's (Mean)
        df[df.columns[col]] = df[df.columns[col]].replace(np.nan, 0)
    
    return(df)
    
def spy_algorithm(s, l, df, label_column, all_columns, other_columns):
    
    df_master = df.loc[:,all_columns] # Select the features to actually use
    df_master = df_master.dropna(axis = 1, how = 'all') # Clear any columns that are completely NA
    
    # Create the issue and non-issue dataframes to play with
    df_issue = df_master[df_master[label_column] == 1]
    df_non_issue = df_master[df_master[label_column] == 0]

    # Hyperparameters
    # s - percentage of + examples to put in unlabeled set
    # l - percentage of spy's to be below threshold t

    # Randomly sample spy observations
    spy_idx = sample(range(df_issue.shape[0]), int(df_issue.shape[0]*s))
    spy_obs = df_issue.iloc[spy_idx,:]
    non_spy_obs = df_issue[~df_issue.index.isin(spy_obs.index)]

    #Add label to identify spy's
    spy_obs['Spy'] = 1
    non_spy_obs['Spy'] = 0
    df_non_issue['Spy'] = 0
    df_issue['Spy'] = 0

    #Add spy observations to non-issue unlabeled set
    df_non_issue = pd.concat([df_non_issue, spy_obs])
    df_non_issue[label_column] = 0

    #Concatenate everything!
    df_all = pd.concat([non_spy_obs, df_non_issue]).reset_index(drop = True)

    y = df_all[label_column]
    spy = df_all['Spy']
    df_all_other = df_all[other_columns]
    df_all.drop(other_columns, axis=1, inplace=True)
    
    # Now normalize the columns in this dataframe and impute mean(0) instead of NA everywhere
    df_all = normalize_columns(df = df_all)

    # Identify indices in unlabeled set that are NOT spys
    # Will need to filter on these when identifying negative labels 
    no_spy_no_issue = (spy == 0) == (y == 0)
    no_spy_no_issue = no_spy_no_issue.index[no_spy_no_issue].values

    ## Fit Weighted Logistic Regression ##
    
    clf = LogisticRegressionCV(penalty = 'l2', class_weight = 'balanced', max_iter = 10, cv = 5)
    clf.fit(df_all, y)

    # Get class probabilities for each observations
    probs = clf.predict_proba(df_all)
    probs = probs[:,1]

    #Sort spy probabilities
    sorted_spy_probs = np.sort(probs[np.where(spy == 1)[0]])

    t = findThreshold(l, sorted_spy_probs = sorted_spy_probs)
    
    # Combine the features used for the model building with the rest of the columns
    df_all = pd.concat([df_all_other, df_all], axis = 1)
    
    # Identify Reliable Negative Incidents
    reliable_N = np.where(probs < t)[0]
    reliable_N = np.intersect1d(reliable_N, no_spy_no_issue)
    df_reliable_N = df_all.iloc[reliable_N, :].reset_index(drop = True)
    
    df_reliable_N[label_column] = 0
    df_master = pd.concat([df_issue, df_reliable_N]).reset_index(drop = True)
    del df_master['Spy']
    
    return(df_master)



def find_mislabels(df_master, label_column, other_columns, id_column, loops, top_n, use_contributing_features_only, contributing_features):
    
    features = df_master[df_master.columns.difference(other_columns)]
    labels = df_master[label_column]
    
    # Normalize the features dataset before fitting a model on it
    # Determine which columns are numerical here, and normalize them
    
    features_categorical = features[features.columns.difference(numeric_cols)].reset_index(drop = True)
    features_numeric = features[features.columns.difference(features_categorical.columns.tolist())].reset_index(drop = True)

    features_numeric = normalize_columns(features_numeric)
    features = pd.concat([features_categorical, features_numeric], axis = 1)
    
    # Initialize empty lists for the false positives and false negatives
    false_positive = []
    false_negative = []
    #top_potential_fp = []
    #top_potential_fn = []
    
    for i in range(loops):
        # Initialize our classifier
        #logregcv = LogisticRegressionCV(Cs = 5, cv = 4, penalty = "l2", solver = "liblinear", class_weight = 'balanced')
        rf = RandomForestClassifier(random_state = 777, n_estimators = 100, verbose = True, class_weight = "balanced",
                            n_jobs = 8, min_samples_split = 15)
        
        # Keep only contributing features, if decided to do so
        if(use_contributing_features_only == True):
            features = features.loc[:, contributing_features]
        
        # Make predictions by taking a crossvalidated approach
        preds = cross_val_predict(X = features, y = labels, estimator = rf, cv = 5)
        df_master['prediction'] = preds
        probs = cross_val_predict(X = features, y = labels, estimator = rf, cv = 5, method = "predict_proba")
        probs_actual = [value[1] for value in probs]
        df_master['issue_probability'] = probs_actual
    
        # Now we need to highlight the data points which were predicted to be the opposite set by the algorithm
        # The definition of both false positive and false negative is important here
        # False Positive: Something which was marked as Positive (1) by the SMEs (infant care) but is marked as Negative (0) by the algorithm
        # False Negative: Something which was marked as Negative (0) by the SMEs (infant care) but is marked as Positive (1) by the algorithm
        
        loop_false_negatives = df_master[id_column][(df_master['prediction'] == 1) & (df_master['Label'] == 0)].tolist()
        loop_false_positives = df_master[id_column][(df_master['prediction'] == 0) & (df_master['Label'] == 1)].tolist()
        
        # Also, we basically might get a lot of these false positives and false negatives, or we might get none.
        # So we need to prepare for both situations
        
        if(len(loop_false_negatives) == 0):
            loop_false_negatives = df_master[(df_master['Label'] == 0)].sort_values(by = ['issue_probability'], ascending = False).head(n_top)[id_column].tolist()
        if(len(loop_false_negatives) >= top_n):
            loop_false_negatives = df_master[(df_master['Label'] == 0) & (df_master['prediction'] == 1)].sort_values(by = ['issue_probability'], ascending = False).head(top_n)[id_column].tolist()
        else:
            loop_false_negatives = loop_false_negatives
            
        if(len(loop_false_positives) == 0):
            loop_false_positives = df_master[(df_master['Label'] == 1)].sort_values(by = ['issue_probability'], ascending = True).head(n_top)[id_column].tolist()
        if(len(loop_false_positives) >= top_n):
            loop_false_positives = df_master[(df_master['Label'] == 1) & (df_master['prediction'] == 0)].sort_values(by = ['issue_probability'], ascending = True).head(top_n)[id_column].tolist()
        else:
            loop_false_positives = loop_false_positives
        
        
        # Append these results to the list we actually have
        false_positive.extend(loop_false_positives)
        false_negative.extend(loop_false_negatives)
        
    # Now filter the false positives and negatives to keep only ones that occur frequent enough.
    # Here only keeping ones greater than half of the number of loops
    
    fp = {x:false_positive.count(x) for x in false_positive}
    fn = {x:false_negative.count(x) for x in false_negative}
    
    # Keep only those incidents that appear atleast in half of the iterations
    fp = {k:v for k, v in fp.items() if v > math.floor(loops/2)}
    fp_final = list(fp.keys())
    fn = {k:v for k, v in fn.items() if v > math.floor(loops/2)}
    fn_final = list(fn.keys())
    
    # Make the prediction column depict only the above points as the false positives and negatives
    df_master['prediction'] = df_master[label_column]
    df_master['prediction'][df_master[id_column].isin(fp_final)] = 0
    df_master['prediction'][df_master[id_column].isin(fn_final)] = 1
    
    # Get the probabilities given by the model, in the output master dataframe
    rf = RandomForestClassifier(random_state = 777, n_estimators = 100, verbose = True, class_weight = "balanced",
                            n_jobs = 8, min_samples_split = 15)
    
    probs = cross_val_predict(X = features, y = labels, estimator = rf, cv = 5, method = "predict_proba")
    probs_actual = [value[1] for value in probs]
    df_master['issue_probability'] = probs_actual
    
    # Print the confusion matrix
    confusion_matrix = pd.crosstab(df_master[label_column], df_master['prediction'], rownames=['Actual'], colnames=['Predicted'], margins=True)
    
    # Return the various results
    return fp_final, fn_final, confusion_matrix, df_master


def generate_results(incident_ids, df, id_column, manual_features, top_n):
    
    final_data = df[df[id_column].isin(incident_ids)]
    features = ['probabilities']
    features.extend(manual_features)
    features.extend(list(set(feature_names_order) - set(manual_features)))
    if top_n <= len(incident_ids):
        output_data = final_data.sort_values(by = ['probabilities'], ascending  = False).head(top_n)
    else:
        output_data = final_data.sort_values(by = ['probabilities'], ascending  = False).head(len(incident_ids))
    
    output_data = output_data.loc[:,features]
    return(output_data)