#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 25 20:48:23 2018

@author: sanjay
"""


import pandas as pd
import numpy as np
import sys
import os
from tqdm import tqdm
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestClassifier
import multiprocessing
import matplotlib.pyplot as plt
import pickle
from scipy.optimize import fmin
from scipy.stats import *
from joblib import Parallel, delayed
from scipy.stats import anderson_ksamp, ks_2samp, ranksums, chi2_contingency, fisher_exact
from statsmodels.stats.contingency_tables import mcnemar
from importlib import import_module
#__import__("00_Load_and_Save_Data")

plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

# Hyperparameters #
issue_name = 'EGR_Cooler'
path_to_results = '/Users/sanjay/Desktop/Solve_Results'

def buildMABChart(universal_view, issue_name):
    '''
    build_population - table that contains overall 
    
    '''
    
    path = '/Users/sanjay/Desktop/Solve Use Case/00 Data/full_build_population.csv'
    build_population = pd.read_csv(path)
    
    #Extract month and year from build population
    build_population['build_date'] = pd.to_datetime(build_population['build_date'])
    build_population['build_month'] = [build_population['build_date'][x].month for x in range(build_population.shape[0])]
    build_population['build_year'] = [build_population['build_date'][x].year for x in range(build_population.shape[0])]

    #Group by number of engines built
    build_population = build_population[['build_year', 'build_month', 'count(engine_serial_num)']].\
                        groupby(['build_year', 'build_month']).sum().reset_index()

    #Drop duplicate rows on ID column
    universal_view = universal_view.drop_duplicates(subset = 'ID').\
                        query("Label == 1").reset_index(drop = True)


    #Extract Time to Incident Date
    universal_view['MONTHS_AFTER_BUILD'] = \
                    [round((universal_view.EARLIEST_INDICATION_DATE[x] - \
                            universal_view.REL_BUILD_DATE[x]).days/30) \
                            for x in range(universal_view.shape[0])]
    
    #Extract month and year for build date
    universal_view['REL_BUILD_MONTH'] = \
                    [universal_view['REL_BUILD_DATE'][x].month \
                     for x in range(universal_view.shape[0])]
    
    universal_view['REL_BUILD_YEAR'] = \
                    [universal_view['REL_BUILD_DATE'][x].year \
                     for x in range(universal_view.shape[0])]


    #Count number of ESNs by months after build
    universal_view['COUNT'] = 1
    universal_view_pivot = universal_view.\
                            pivot_table(index = ['REL_BUILD_YEAR', 'REL_BUILD_MONTH'], \
                                        columns = 'MONTHS_AFTER_BUILD', \
                                        values = 'COUNT', aggfunc='sum').\
                                        fillna(0).\
                                        reset_index()

    #Create empty RPH dataframe to input normalized RPH
    df_rph = pd.DataFrame(columns = universal_view_pivot.columns, \
                          index = universal_view_pivot.index)

    #Populate cumulative RPH by Build Date
    for i in range(universal_view_pivot.shape[0]):
        
        #Extract Build year and month
        year = universal_view_pivot['REL_BUILD_YEAR'][i]
        month = universal_view_pivot['REL_BUILD_MONTH'][i]
        df_rph['REL_BUILD_YEAR'][i] = year
        df_rph['REL_BUILD_MONTH'][i] = month
        
        #Iterate across months after build
        for j in range(2, universal_view_pivot.shape[1]):
            
            #Extract total build count for build month and year
            total_build = build_population.\
                        query("build_month == @month & build_year == @year")\
                        ['count(engine_serial_num)']
                        
            #Calculate RPH!!
            df_rph.iloc[i,j] = float(100*universal_view_pivot.iloc[i,j]/total_build)
    
        #Find cumulative RPH
        df_rph.iloc[i,2:] = np.cumsum(df_rph.iloc[i,2:])
    
    #Add Build Year + Month to RPH Data Frame
    df_rph['REL_BUILD_YEAR_MONTH'] = [str(df_rph['REL_BUILD_YEAR'][x]) + \
                                  "_" + str(df_rph['REL_BUILD_MONTH'][x]) \
                                  for x in range(df_rph.shape[0])]

    #Find number of months to failure for plotting
    months_after_build = np.sort(universal_view.MONTHS_AFTER_BUILD.unique())

    #Plot MAB Chart!!
    for month in months_after_build:
        plt.plot(df_rph[month], label = month)
    plt.xticks(np.arange(df_rph.shape[0]), df_rph['REL_BUILD_YEAR_MONTH'])
    plt.title("RPH of {} by Build Date".format(issue_name))
    plt.xlabel("Build Month")
    plt.ylabel("RPH")
    plt.legend(title = 'Months After Build')
    plt.savefig('/Users/sanjay/Desktop/Solve_Results/{}/Figs/{}/MAB_Chart.png'.format(issue_name, 'MAB'), bbox_inches="tight")
    plt.close()



def saveNumericalHist(col, desc, view, ranking, issue_name, issue_n, non_issue_n):

    
    range_1 = df_not_normalized.query("Label == 1")[col].dropna().max() - \
            df_not_normalized.query("Label == 1")[col].dropna().min()
            
    range_0 = df_not_normalized.query("Label == 0")[col].dropna().max() - \
            df_not_normalized.query("Label == 0")[col].dropna().min()
            
    if range_0 == 0 or range_1 == 0 or np.isnan(range_0) or np.isnan(range_1):
        return
    
    if np.abs(range_1 - range_0) > 10**4:
        fig, ax = plt.subplots(figsize=(12, 8))
        ax.hist(df_not_normalized.query("Label == 1")[col].dropna(),
        color='red', alpha=0.6, bins = 10, label = 'Issue [{}]'.format(issue_n), normed=True)
        ax.hist(df_not_normalized.query("Label == 0")[col].dropna(),
        color='blue', alpha=0.6, bins = 10, label = 'No Issue [{}]'.format(non_issue_n), normed=True)
        ax.set(title='Histogram',
           ylabel='Count', xlabel = col)
        plt.legend()
        plt.savefig('/Users/sanjay/Desktop/Solve_Results/{}/Figs/{}/{}_{}.png'.format(issue_name, view, ranking, col), bbox_inches="tight")
        plt.close()
        
    else:
            
    
        fig, ax = plt.subplots(figsize=(12, 8))
    
        range_both = np.array([range_0, range_1])
        range_min = np.argmin(range_both)
        small_bin = 10
        if range_min == 1:
            hist, bins = np.histogram(df_not_normalized.query("Label == 1")[col].dropna(), bins = small_bin)
            ax.bar(bins[:-1], 100*hist.astype(np.float32)/ hist.sum(), width=(bins[1]-bins[0]), \
                   color='red', alpha = 0.6, label = 'Issue [{}]'.format(issue_n))
            hist, bins = np.histogram(df_not_normalized.query("Label == 0")[col].dropna(), bins = int(small_bin*range_0/range_1))
            ax.bar(bins[:-1], 100*hist.astype(np.float32) / hist.sum(), width=(bins[1]-bins[0]), \
                   color='blue', alpha = 0.6, label = 'No Issue [{}]'.format(non_issue_n))
        if range_min == 0:
            hist, bins = np.histogram(df_not_normalized.query("Label == 1")[col].dropna(), bins = int(small_bin*range_1/range_0))
            ax.bar(bins[:-1], 100*hist.astype(np.float32)/ hist.sum(), width=(bins[1]-bins[0]), \
                   color='red', alpha = 0.6, label = 'Issue [{}]'.format(issue_n))
            hist, bins = np.histogram(df_not_normalized.query("Label == 0")[col].dropna(), bins = small_bin)
            ax.bar(bins[:-1], 100*hist.astype(np.float32) / hist.sum(), width=(bins[1]-bins[0]), \
                   color='blue', alpha = 0.6, label = 'No Issue [{}]'.format(non_issue_n))
        ax.set(title='Histogram',
               ylabel='Percentage', xlabel = col)
        plt.legend()
        plt.savefig('/Users/sanjay/Desktop/Solve_Results/{}/Figs/{}/{}_{}.png'.format(issue_name, view, ranking, col), bbox_inches="tight")
        plt.close()
   
def saveCategoricalPlot(df1, var_name, var_desc, view, ranking, issue_name, vline = None, sort=False, only_sig = False):
   #Forest Plot for categorical variables 

   dfpp = df1.groupby(var_name).Label.agg({'beta_mode':np.mean, 'ww': np.sum,
                                           'Not-ww': lambda x: np.sum(1-x),
                                           'count': lambda x: np.shape(x)[0]})
   if sort == True:
       dfpp = dfpp.sort_values(by='beta_mode', ascending=False)
      
   names = dfpp.index.values
   
   if only_sig == True:
       sig_names = []
       for i, val in enumerate(names):
           a = dfpp.iloc[i]['ww'] + 1
           b = dfpp.iloc[i]['Not-ww'] + 1
           hpd95 = 100*HDIofICDF(beta, credMass=0.95, a=a, b=b)
           if (vline > hpd95[0] and vline > hpd95[1]) or (vline < hpd95[0] and vline < hpd95[1]):
               sig_names.append(val)
       names = sig_names 
       dfpp = dfpp.loc[names]

   p = len(names)
   space = 0.5
   fig, ax = plt.subplots(figsize=(8,(p+1)*0.5))
   for i, val in enumerate(names):
       # parameters of beta distribution
       a = dfpp.iloc[i]['ww'] + 1
       b = dfpp.iloc[i]['Not-ww'] + 1
       
       #a = dfpp.loc[val]['ww'] + 1
       #b = dfpp.loc[val]['Not-ww'] + 1
       # plot most probable value (the mode)
       mode = (a - 1)/(a + b - 2)
       ax.plot( 100*mode, [-space -i*space],  marker='o', markersize=5, color='blue')
       # Plot the High Posterior Density regions
       hpd50 = 100*HDIofICDF(beta, credMass=0.50, a=a, b=b)
       hpd95 = 100*HDIofICDF(beta, credMass=0.95, a=a, b=b)
       ax.errorbar(x=(hpd50[0],hpd50[1]), y=(-space-i*space,-space-i*space), color='blue', 
                               linewidth= 2)
       ax.errorbar(x=(hpd95[0],hpd95[1]), y=(-space-i*space,-space-i*space), color='blue', 
                       linewidth= 1)
       
       
       
   issue_name_pres = issue_name.replace("_", " ")   
   if vline:
       ax.axvline(vline, color='k', linestyle=':', alpha=0.7, linewidth=1, label='Overall % of {} Issues'.format(issue_name_pres))
   ax.set_ylim(-space*(p+1),0)
   ax.set_xlim(-0.02*100,1.02*100)
   ax.set_yticks(-space-1*np.arange(p)*space)
   names_with_count = [str(names[x]) + ' [' + str(int(dfpp.loc[names[x]]['count'])) + ']' for x in range(len(names))]
   ax.set_yticklabels(names_with_count)
   ax.set_ylabel('{}'.format(var_desc))
   ax.set_xlabel('% of {} Issues'.format(issue_name_pres));
   ax.set_title('% of {} Issues by {}'.format(issue_name_pres, var_name));
   ax.grid(False)
   if vline:
       ax.legend(loc = 0, fontsize = 'medium')
   plt.savefig('/Users/sanjay/Desktop/Solve_Results/{}/Figs/{}/{}_{}.png'.format(issue_name, view, ranking, var_desc), bbox_inches="tight") 
   plt.close()
       
       
def HDIofICDF(dist_name, credMass=0.95, **args):
    # freeze distribution with given arguments
    distri = dist_name(**args)
    # initial guess for HDIlowTailPr
    incredMass =  1.0 - credMass

    def intervalWidth(lowTailPr):
        return distri.ppf(credMass + lowTailPr) - distri.ppf(lowTailPr)

    # find lowTailPr that minimizes intervalWidth
    HDIlowTailPr = fmin(intervalWidth, incredMass, ftol=1e-8, disp=False)[0]
    # return interval as array([low, high])
    return distri.ppf([HDIlowTailPr, credMass + HDIlowTailPr])





## Load Data ##
df_all = pd.read_csv("{}/{}/Data/{}_one_hot_norm_reduced.csv".format(path_to_results, issue_name, issue_name))
df_not_normalized = pd.read_csv("{}/{}/Data/{}_raw.csv".format(path_to_results, issue_name, issue_name))
df_all_not_normalized = pd.read_csv("{}/{}/Data/{}_one_hot_raw.csv".format(path_to_results, issue_name, issue_name))

__, __, __, numeric_cols, __ = getColumnTypes(get_oepl = False)


new_views = ['Design', 'Manufacturing', 'Usage']
design = ('VPCR', 'OEPL', 'ESN', 'REL_ANALYSIS_RATE_CAT', 'REL_CPL_NUM', \
          'REL_DESIGN_HSP_NUM', 'REL_DESIGN_RPM_NUM', 'REL_MKTG_HSP_NUM', \
          'REL_MKTG_RPM_NUM', 'REL_EMISSIONS_FAMILY_CODE')
manuf = ('MES', 'EPAT', 'ESN', 'REL_BUILT_ON_WEEKEND', 'REL_ECM_PART_NO', \
        'REL_USER_APPL_DESC', 'REL_CMP_PROGRAM_GROUP_NAME', \
         )
use = ('INS','FC', 'ESN', 'REL_OEM_TYPE', 'REL_OEM_GROUP', 'REL_BUILD_YEAR_MONTH') 

design_cols = [df_all.columns.values[x].startswith(design + ('Label',)) for x in range(df_all.shape[1])]
manuf_cols = [df_all.columns.values[x].startswith(manuf + ('Label',)) for x in range(df_all.shape[1])]
use_cols = [df_all.columns.values[x].startswith(use + ('Label',)) for x in range(df_all.shape[1])]

df_design = df_all.iloc[:,design_cols]
df_manuf = df_all.iloc[:,manuf_cols]
df_use = df_all.iloc[:,use_cols]

df_design = df_design.drop_duplicates('ESN').reset_index(drop = True)
df_manuf = df_manuf.drop_duplicates('ESN').reset_index(drop = True)

del df_design['ESN'], df_manuf['ESN'], df_use['ESN']

df_datasets = [df_design, df_manuf, df_use]

#df_final_cat_list = []
#df_final_num_list = []
df_final_list = []



for df_ in df_datasets:
    

    df_num_scores = pd.DataFrame(columns = ['Variable', 'Anderson-Darling', \
                                        'Kolmogorov–Smirnov', 'Wilcoxon Rank-Sum', \
                                        'Issue Sample Size', 'Non-Issue Sample Size', \
                                        'Issue Unique Vals', 'Non-Issue Unique Vals'], \
                 index = np.arange(len(numeric_cols)))
    df_num_scores['Variable'] = numeric_cols
    df_num_scores['Anderson-Darling'] = df_num_scores['Anderson-Darling'].astype(float)
    df_num_scores['Kolmogorov–Smirnov'] = df_num_scores['Kolmogorov–Smirnov'].astype(float)
    df_num_scores['Wilcoxon Rank-Sum'] = df_num_scores['Wilcoxon Rank-Sum'].astype(float)
    df_num_scores['Type'] = 'Numerical'

    for col in numeric_cols:
        if col in df_.columns.values:
            pos_val = df_all_not_normalized.query("Label == 1")[col].dropna().values.tolist()
            neg_val = df_all_not_normalized.query("Label == 0")[col].dropna().values.tolist() 
            
            df_num_scores.loc[df_num_scores['Variable'] == col, 'Issue Sample Size'] = len(pos_val)
            df_num_scores.loc[df_num_scores['Variable'] == col, 'Non-Issue Sample Size'] = len(neg_val)           
            df_num_scores.loc[df_num_scores['Variable'] == col, 'Issue Unique Vals'] = len(np.unique(pos_val))
            df_num_scores.loc[df_num_scores['Variable'] == col, 'Non-Issue Unique Vals'] = len(np.unique(pos_val))
    
            
            if len(np.unique(pos_val)) > 1 and len(np.unique(pos_val)) > 1:
                
                __, __, ad_test = anderson_ksamp([pos_val, neg_val])
                __, ks_test = ks_2samp(pos_val, neg_val)
                __, rs_test = ranksums(pos_val, neg_val)
                
                if ad_test > 1:
                    ad_test = 1
            
                df_num_scores.loc[df_num_scores['Variable'] == col, 'Anderson-Darling'] = ad_test
                df_num_scores.loc[df_num_scores['Variable'] == col, 'Kolmogorov–Smirnov'] = ks_test
                df_num_scores.loc[df_num_scores['Variable'] == col, 'Wilcoxon Rank-Sum'] = rs_test
    
    
    df_num_scores = df_num_scores[~np.isnan(df_num_scores['Kolmogorov–Smirnov'])]
    df_num_scores = df_num_scores.sort_values(by = 'Kolmogorov–Smirnov').reset_index(drop = True)
    
    
    
    cat_cols = df_.columns.values[[df_.columns.values[x] not in numeric_cols + ['Label'] for x in range(df_.shape[1])]]
    df_cat_scores = pd.DataFrame(columns = ['Variable', 'Chi-Sq Test', 'McNemar', 'Fishers_Exact', \
                                        'Issue Cat', 'Non-Issue Cat'], \
                 index = np.arange(len(cat_cols)))
    df_cat_scores['Variable'] = cat_cols
    df_cat_scores['Chi-Sq Test'] = df_cat_scores['Chi-Sq Test'].astype(float)
    df_cat_scores['McNemar'] = df_cat_scores['McNemar'].astype(float)
    df_cat_scores['Fishers_Exact'] = df_cat_scores['Fishers_Exact'].astype(float)

    df_cat_scores['Type'] = 'Categorical'
    
    for col in cat_cols:
        
        if col in df_.columns.values:
            cong_table = pd.crosstab(df_[col], df_['Label'])
            if cong_table.shape == (2,2):
                __, chisq_test, __, __ = chi2_contingency(cong_table)
                mcnemar_test = mcnemar(cong_table)
                __, fishers_exact = fisher_exact(cong_table)
                
                df_cat_scores.loc[df_cat_scores['Variable'] == col, 'Chi-Sq Test'] = chisq_test
                df_cat_scores.loc[df_cat_scores['Variable'] == col, 'McNemar'] = mcnemar_test.pvalue
                df_cat_scores.loc[df_cat_scores['Variable'] == col, 'Fishers_Exact'] = fishers_exact


                df_cat_scores.loc[df_cat_scores['Variable'] == col, 'Issue Cat'] = cong_table.iloc[1,1]
                df_cat_scores.loc[df_cat_scores['Variable'] == col, 'Non-Issue Cat'] = cong_table.iloc[1,0]
            
    df_cat_scores = df_cat_scores[~np.isnan(df_cat_scores['Fishers_Exact'])].\
                                    sort_values(by = 'Fishers_Exact').\
                                    reset_index(drop = True)
                            
    df_num_scores['Median of Issues'] = float('NaN')
    df_num_scores['Median of Non-Issues'] = float('NaN')
    df_num_scores['Mean of Issues'] = float('NaN')
    df_num_scores['Mean of Non-Issues'] = float('NaN')
    df_num_scores['SD of Issues'] = float('NaN')
    df_num_scores['SD of Non-Issues'] = float('NaN')
    df_num_scores['Sample Size of Issues'] = float('NaN')
    df_num_scores['Sample Size of Non-Issues'] = float('NaN')
    df_cat_scores['Rate of Issues'] = float('NaN')
    df_cat_scores['Rate of Non-Issues'] = float('NaN')
    df_cat_scores['Sample Size of Issues'] = float('NaN')
    df_cat_scores['Sample Size of Non-Issues'] = float('NaN')
                            
    
    def getSummaryStats(i, col_type):
        if col_type == "Categorical":
            df_cat_scores['Rate of Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==1][df_cat_scores.Variable[i]].mean()
            df_cat_scores['Rate of Non-Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==0][df_cat_scores.Variable[i]].mean()
            df_cat_scores['Sample Size of Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==1][df_cat_scores.Variable[i]].count()
            df_cat_scores['Sample Size of Non-Issues'][i] = df_all_not_normalized[df_all_not_normalized.Label==0][df_cat_scores.Variable[i]].count()
            return df_cat_scores[df_cat_scores.Variable == df_cat_scores.Variable[i]]
        
        if col_type == "Numerical":
            df_num_scores['Median of Issues'][i] = df_not_normalized[df_not_normalized.Label==1][df_num_scores.Variable[i]].dropna().median()
            df_num_scores['Median of Non-Issues'][i] = df_not_normalized[df_not_normalized.Label==0][df_num_scores.Variable[i]].dropna().median()
            df_num_scores['Mean of Issues'][i] = df_not_normalized[df_not_normalized.Label==1][df_num_scores.Variable[i]].dropna().mean()
            df_num_scores['Mean of Non-Issues'][i] = df_not_normalized[df_not_normalized.Label==0][df_num_scores.Variable[i]].dropna().mean()
            df_num_scores['SD of Issues'][i] = np.sqrt(df_not_normalized[df_not_normalized.Label==1][df_num_scores.Variable[i]].dropna().var())
            df_num_scores['SD of Non-Issues'][i] = np.sqrt(df_not_normalized[df_not_normalized.Label==0][df_num_scores.Variable[i]].dropna().var())
            df_num_scores['Sample Size of Issues'][i] = df_not_normalized[df_not_normalized.Label==1][df_num_scores.Variable[i]].dropna().count()
            df_num_scores['Sample Size of Non-Issues'][i] = df_not_normalized[df_not_normalized.Label==0][df_num_scores.Variable[i]].dropna().count()
            return df_num_scores[df_num_scores.Variable == df_num_scores.Variable[i]]
    
    num_cores = multiprocessing.cpu_count()-1
    
    summary_stat_cat = Parallel(n_jobs = num_cores)(delayed(getSummaryStats)(i, 'Categorical') for i in range(df_cat_scores.shape[0]))
    summary_stat_num = Parallel(n_jobs = num_cores)(delayed(getSummaryStats)(i, 'Numerical') for i in range(df_num_scores.shape[0]))
    
    
    if len(summary_stat_num) > 0:
        df_final_num = pd.concat(summary_stat_num)
        
        df_final_num['Significance'] = 1
        df_final_num.loc[df_final_num['Kolmogorov–Smirnov'] <= 0.10, 'Significance'] = 2
        df_final_num.loc[df_final_num['Kolmogorov–Smirnov'] <= 0.01, 'Significance'] = 3

    else:
        df_final_num = pd.DataFrame()
        
    if len(summary_stat_cat) > 0:
        df_final_cat = pd.concat(summary_stat_cat)
        
        df_final_cat['Significance'] = 1
        df_final_cat.loc[df_final_cat['Fishers_Exact'] <= 0.10, 'Significance'] = 2
        df_final_cat.loc[df_final_cat['Fishers_Exact'] <= 0.01, 'Significance'] = 3
    else:
        df_final_cat = pd.DataFrame()
    
    df_final = pd.concat([df_final_num, df_final_cat]).sort_values(by = 'Significance', ascending = False).reset_index(drop = True)

    
    #df_final_cat_list.append(df_final_cat)
    #df_final_num_list.append(df_final_num)
    df_final_list.append(df_final)


## Automate Visualizations ##

oepl_desc_df = pd.read_csv("/Users/sanjay/Desktop/Data/ISX_Options.csv")
oepl_desc_df['OPT_ITEM_DESC_NEW'] = [oepl_desc_df['OPT_ITEM_DESC'][x].\
             replace(",", "_") for x in range(oepl_desc_df.shape[0])]
oepl_desc_df['OPT_ITEM_DESC_NEW'] = [oepl_desc_df['OPT_ITEM_DESC_NEW'][x].\
             replace(" ", "_") for x in range(oepl_desc_df.shape[0])]
oepl_desc_df['OPT_ITEM_DESC_NEW'] = ['OEPL_' + \
            oepl_desc_df['OPT_ITEM_DESC_NEW'][x] for x in \
            range(oepl_desc_df.shape[0])]
oepl_desc_df['OPT_ITEM_DESC_NEW'] = [oepl_desc_df['OPT_ITEM_DESC_NEW'][x] + \
             ':' + oepl_desc_df['OPT_NBR'][x] \
             for x in range(oepl_desc_df.shape[0])]

#Option File (X15s)
path = '/Users/sanjay/Desktop/Data/RLD_ENGINE_OPTION_ASSEMBLY_x15'
df_oepl = pq.read_table(path).to_pandas()
df_oepl['engine_serial_num'] = df_oepl['engine_serial_num'].astype(int)

#Merge with description file
df_oepl = df_oepl.rename(columns = {'option_assembly_num': 'OPT_NBR'})
df_oepl = df_oepl.merge(oepl_desc_df, on = 'OPT_NBR', how = 'left')
df_oepl['option_count'] = 1


#Get OEPL
df_oepl_sub = df_oepl[['engine_serial_num', 'OPT_NBR']]
df_oepl_sub['OPT_CAT'] = df_oepl_sub['OPT_NBR'].apply(lambda x: x[:2])
df_oepl_sub = df_oepl_sub.rename(columns = {'engine_serial_num': 'ESN'})

df_oepl_sub = df_all_not_normalized[['ESN', 'EARLIEST_INDICATION_DATE', 'Label']].merge(df_oepl_sub, on = 'ESN', how = 'left')
df_oepl_sub = df_oepl_sub.drop_duplicates()

#VPCR
vpcr_idx = [df_all_not_normalized.columns.values[x].startswith(('ESN','VPCR')) \
            for x in range(df_all_not_normalized.shape[1])]
df_vpcr = df_all_not_normalized.iloc[:,vpcr_idx]
df_vpcr = df_vpcr.set_index('ESN').stack().reset_index().rename(columns = {'level_1': 'VPCR'})
df_vpcr = df_vpcr[df_vpcr[0] == 1].reset_index(drop = True)
df_vpcr['VPCR'] = [df_vpcr['VPCR'][x].split("_")[3] for x in range(df_vpcr.shape[0])]
del df_vpcr[0]

df_vpcr = df_all_not_normalized[['ESN', 'Label']].merge(df_vpcr, on = 'ESN', how = 'right')

#Fault Code
fc_idx = [df_all_not_normalized.columns.values[x].startswith(('ESN','FC')) \
            for x in range(df_all_not_normalized.shape[1])]
df_fc = df_all_not_normalized.iloc[:,fc_idx]
df_fc = df_fc.set_index('ESN').stack().reset_index().rename(columns = {'level_1': 'FC'})
df_fc = df_fc[df_fc[0] == 1].reset_index(drop = True)
df_fc['FC'] = [df_fc['FC'][x].split(":")[1] for x in range(df_fc.shape[0])]
df_fc['FC'] = df_fc['FC'].astype(int)
df_fc = df_all_not_normalized[['ESN', 'Label']].merge(df_fc, on = 'ESN', how = 'right')
df_fc = df_fc.drop_duplicates()

del df_fc[0]

fc_lookup = pd.read_csv("/Users/sanjay/Desktop/Data/fault_code_lookup_solve.csv", header = 1)
fc_lookup = fc_lookup[['Fault Code', 'Fault Code Category']]
fc_lookup['Fault Code Category'] = [fc_lookup['Fault Code Category'][x].replace(" ", "_") for x in range(fc_lookup.shape[0])]
fc_lookup = fc_lookup.rename(columns = {'Fault Code': 'FC'})
df_fc = df_fc.merge(fc_lookup, on = 'FC', how = 'left')

## Universal Views & Save Excel ##

df_extra = pickle.load(open("/Users/sanjay/Desktop/Data/universal_view_cols.p", "rb" ))
__, __, __, __, uv_cols = getColumnTypes(get_oepl = False)
df_not_normalized['EARLIEST_INDICATION_DATE'] = pd.to_datetime(df_not_normalized['EARLIEST_INDICATION_DATE'])
df_extra['EARLIEST_INDICATION_DATE'] = pd.to_datetime(df_extra['EARLIEST_INDICATION_DATE'])
df_extra = df_extra.drop_duplicates(subset = ['ESN', 'EARLIEST_INDICATION_DATE'])

df_sub_extra = df_not_normalized[['ESN', 'EARLIEST_INDICATION_DATE', 'Label']].merge(df_extra, on = ['ESN', 'EARLIEST_INDICATION_DATE'], how = 'left')

df_sub_extra.ID = df_sub_extra.index + 1

df_fault = df_sub_extra[['ID', 'GREEN_WRENCH_FAULT_CODE_LIST', 'REL_MINED_FAULT_CODES_LIST']]





df_fault['FC_LENGTH'] = [0 if df_fault['GREEN_WRENCH_FAULT_CODE_LIST'][x] \
         is None else len(df_fault['GREEN_WRENCH_FAULT_CODE_LIST'][x]) for x \
         in range(df_fault.shape[0])]


df_fault['FAULT_CODE_LIST'] = [np.unique(df_fault['REL_MINED_FAULT_CODES_LIST'][x]) \
        if df_fault['FC_LENGTH'][x] == 0 else np.unique(df_fault['GREEN_WRENCH_FAULT_CODE_LIST'][x]) \
        for x in range(df_fault.shape[0])]

df_fault_final = pd.DataFrame(columns = ['ID', 'FAULT_CODE'])
for i, line in enumerate(df_fault.FAULT_CODE_LIST):
    if len(line) == 0:
        fault_id = np.repeat(df_fault.ID[i], 1)
        line = ''
        df_temp = pd.DataFrame(data = {'ID': fault_id, 'FAULT_CODE': line})
        df_fault_final = pd.concat([df_fault_final, df_temp])        
    else:     
        fault_id = np.repeat(df_fault.ID[i], len(line))
        df_temp = pd.DataFrame(data = {'ID': fault_id, 'FAULT_CODE': line})
        df_fault_final = pd.concat([df_fault_final, df_temp])
    
df_fault_final.reset_index(drop = True)

df_universal_view = df_fault_final.merge(df_sub_extra, on = 'ID', how = 'left')
del df_universal_view['GREEN_WRENCH_FAULT_CODE_LIST'], df_universal_view['REL_MINED_FAULT_CODES_LIST']



for col in np.intersect1d(numeric_cols, df_universal_view.columns.values):
    df_universal_view[col] = df_universal_view[col].astype(float)
    df_universal_view[col][df_universal_view[col] < 0] = float('NaN')
    df_universal_view[col][df_universal_view[col] > 10**8] = float('NaN')

uv_col_order = ['FAULT_CODE', \
                'ID', \
                'ESN', \
                'EARLIEST_INDICATION_DATE', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_TIME', \
                'INS_DC_CMP_PERCENT_ZERO_OR_NEGATIVE_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_LOW_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LIGHT_LOAD_TIME', \
                'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', \
                'INS_DC_CMP_PERCENT_CLIMBING_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME', \
                'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_1', \
                'INS_EA_COOLANT_TMP_SEC_SEV_1', \
                'INS_EA_OIL_PRSR_SEC_SEV_1', \
                'INS_EA_OIL_TMP_SEC_SEV_1', \
                'INS_EA_SPEED_SEC_SEV_1', \
                'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_1', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_2', \
                'INS_EA_COOLANT_TMP_SEC_SEV_2', \
                'INS_EA_OIL_PRSR_SEC_SEV_2', \
                'INS_EA_OIL_TMP_SEC_SEV_2', \
                'INS_EA_SPEED_SEC_SEV_2', \
                'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_2', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_3', \
                'INS_EA_COOLANT_TMP_SEC_SEV_3', \
                'INS_EA_OIL_PRSR_SEC_SEV_3', \
                'INS_EA_OIL_TMP_SEC_SEV_3', \
                'INS_EA_SPEED_SEC_SEV_3', \
                'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_3', \
                'INS_EA_CRANKCASE_PRSR_SEC_SEV_TOTAL', \
                'INS_EA_COOLANT_TMP_SEC_SEV_TOTAL', \
                'INS_EA_OIL_PRSR_SEC_SEV_TOTAL', \
                'INS_EA_OIL_TMP_SEC_SEV_TOTAL', \
                'INS_EA_SPEED_SEC_SEV_TOTAL', \
                'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_TOTAL', \
                'INS_TI_ENGINE_DISTANCE_MILES', \
                'INS_TI_FUEL_USED_GALLON', \
                'INS_TI_IDLE_TIME_HOURS', \
                'INS_TI_IDLE_TIME_PERCENTAGE', \
                'Label', \
                'MES_DA_CMP_TPLOC_AS_MIN', \
                'MES_DA_CMP_TPLOC_FP_MIN', \
                'MES_DA_CMP_TPLOC_HD_MIN', \
                'MES_DA_CMP_TPLOC_SB_MIN', \
                'MES_DA_CMP_TPLOC_TS_MIN', \
                'MES_TSH_CMP_TOTAL_TIME_CANCEL_MIN', \
                'MES_TSH_CMP_TOTAL_TIME_FINISH_MIN', \
                'MES_TSH_CMP_TOTAL_TIME_HOLD_MIN', \
                'MES_TSH_CMP_TOTAL_TIME_PROD_MIN', \
                'MES_TSH_CMP_TOTAL_TIME_PURGED_MIN', \
                'MES_TSH_CMP_TOTAL_TIME_REPAIR_MIN', \
                'MES_TSH_CMP_TOTAL_TIME_SCHED_MIN', \
                'MES_TSH_CMP_TOTAL_TIME_SHIP_MIN', \
                'REL_ANALYSIS_RATE_CAT', \
                'REL_BUILD_DATE', \
                'REL_CMP_FAIL_CODE_LIST', \
                'REL_FAILURE_DATE', \
                'REL_IN_SERVICE_DATE', \
                'REL_OEM_GROUP', \
                'REL_TRUCK_MODEL']

df_universal_view = df_universal_view[uv_col_order]
    
buildMABChart(df_universal_view, issue_name)    

writer = pd.ExcelWriter('/Users/sanjay/Desktop/Solve_Results/{}/Results/variable_results_{}_041618.xlsx'.format(issue_name, issue_name), engine='xlsxwriter')

for i, view in enumerate(new_views):    
    #df_final_cat_list[i].to_excel(writer, sheet_name=view + "_Cat", index = False)
    #df_final_num_list[i].to_excel(writer, sheet_name=view + "_Num", index = False)
    df_final_list[i].to_excel(writer, sheet_name = view, index = False)


df_universal_view.to_excel(writer, sheet_name='Universal View', index = False)
#df_not_normalized.to_excel(writer, sheet_name = 'All Data', index = False)

# Close the Pandas Excel writer and output the Excel file.
writer.save()


for j, view in enumerate(new_views):
    #df_final_cat = df_final_cat_list[j]
    #df_final_num = df_final_num_list[j]
    df_final = df_final_list[j]

    for i in range(df_final.shape[0]):
        print(i)
        var = df_final.Variable[i]
        if df_final.Type[i] == "Numerical":
                saveNumericalHist(var, \
                                  var, \
                                  view, \
                                  i+1,
                                  issue_name,
                                  df_not_normalized[df_not_normalized.Label==1][var].dropna().count(),
                                  df_not_normalized[df_not_normalized.Label==0][var].dropna().count())
        var = var.replace("/", "_")
    
        if df_final.Type[i] == "Categorical":
            if 'OEPL' in df_final.Variable[i]:
                opt = var.split(":")[1][:2]
                df_sub = df_oepl_sub.query("OPT_CAT == @opt")
                df_sub = df_sub.rename(columns = {'OPT_NBR': var.split(":")[1]})
                saveCategoricalPlot(df_sub, \
                                    var.split(":")[1], \
                                    var.replace(":", "_"), \
                                    view, \
                                    i+1, \
                                    issue_name, \
                                    vline = 100*df_sub.Label.mean())
            elif 'VPCR' in df_final.Variable[i]:
                saveCategoricalPlot(df_vpcr, \
                                    'VPCR', \
                                    var.split("_")[3], \
                                    view, \
                                    i+1, \
                                    issue_name, \
                                    vline = 100*df_vpcr.Label.mean())
                
            elif 'FC' in df_final.Variable[i]:
                fc_cat = var.split(":")[0]
                if fc_cat != 'FC':
                    fc_cat = fc_cat.split("_", 1)[1]
                else:
                    continue
                df_fc_sub = df_fc[df_fc['Fault Code Category'] == fc_cat]
                saveCategoricalPlot(df_fc_sub, \
                                    'FC', \
                                    var.replace(":", "_"), \
                                    view, \
                                    i+1, \
                                    issue_name, \
                                    vline = 100*df_fc_sub.Label.mean(),
                                    sort = True)
    
            else:
                saveCategoricalPlot(df_not_normalized, \
                                    var.split(":")[0], \
                                    var.replace(":", "_"), \
                                    view, \
                                    i+1, \
                                    issue_name, \
                                    vline = 100*df_not_normalized.Label.mean())

    
