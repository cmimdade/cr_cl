#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 25 16:21:30 2018

@author: sanjay
"""


import pandas as pd
import numpy as np
import sys
import os
from tqdm import tqdm
from sklearn.preprocessing import MinMaxScaler
from importlib import import_module
__import__("00_Load_and_Save_Data")

pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

# Hyperparameters #
issue_name = 'EGR_Flow'
path_to_results = '/Users/sanjay/Desktop/Solve_Results'
no_coverage_perc_thresh = 0.05
coef_of_var_thresh = 0.03
min_max_thresh = 0.0002

# Load Data #

df_all = pd.read_csv("{}/{}/Data/{}_one_hot_norm.csv".format(path_to_results, issue_name, issue_name))
df_not_normalized = pd.read_csv("{}/{}/Data/{}_raw.csv".format(path_to_results, issue_name, issue_name))

#List to array for easy subsetting
__, __, cat_cols, numeric_cols, __ = getColumnTypes(get_oepl = False)

numeric_cols = np.array(numeric_cols)

## First Pass Feature Selection ##

def coverageAnalysis(vars_to_delete):

    #Assess Coverage
    df_coverage = pd.DataFrame({'Variable': numeric_cols})
    df_coverage['Coverage'] = float('NaN')
        
    for col in numeric_cols:
        df_coverage.iloc[df_coverage[df_coverage.Variable == col].index, 1] = \
            df_not_normalized[col].dropna().count()/df_not_normalized[col].shape[0]
        
    #Variables with no coverage
    no_coverage_vars = np.array(df_coverage.query("Coverage < @no_coverage_perc_thresh")['Variable'])
    
    print("The following variables have no coverage:")
    for var in no_coverage_vars:
        vars_to_delete.add(var)
        print(var)
        
    return vars_to_delete

def coefofVar(vars_to_delete):
    
    #Coefficient of Variation
    
    #Create empty dataframe for coefficient of variation calculation
    df_ss = pd.DataFrame({'Variable': numeric_cols})
    df_ss['Mean'] = float('NaN')
    df_ss['Variance'] = float('NaN')
    
    #Calculate mean and variance for numeric cols
    #Convert columns to float and remove extraneous values
    for col in tqdm(numeric_cols):    
        df_ss.iloc[df_ss[df_ss.Variable == col].index, 1] = \
        np.mean(df_not_normalized[col].dropna())
        df_ss.iloc[df_ss[df_ss.Variable == col].index, 2] = \
        np.var(df_not_normalized[col].dropna())
        
    #Coefficient of variation = sigma/mu
    df_ss['Coef_of_Var'] = np.abs(np.sqrt(df_ss['Variance'])/df_ss['Mean'])
    
    low_var_vars = np.array(df_ss.query("Coef_of_Var < @coef_of_var_thresh")['Variable'])
    
    print("The following variables have low coefficient of variation:")
    for var in low_var_vars:
        vars_to_delete.add(var)
        print(var)
        
    return vars_to_delete
        
def minmaxscaler(vars_to_delete):
        
    # Min/Max Scalar
    df_mms = pd.DataFrame({'Variable': numeric_cols})
    df_mms['scaled_var'] = float('NaN')
    
    min_max_scaler = MinMaxScaler()
    for col in numeric_cols:
        if len(df_not_normalized[col].dropna()) > 0:
            x_scaled = min_max_scaler.fit_transform(df_not_normalized[col].\
                                                dropna().values.reshape(-1, 1))
            df_mms.iloc[df_mms[df_mms.Variable == col].index, 1] = np.var(x_scaled)
        
    
    min_max_vars = df_mms.query("scaled_var < @min_max_thresh")['Variable']
    
    print("The following variables have low variance after scaling:")
    for var in min_max_vars:
        vars_to_delete.add(var)
        print(var)
        
    return vars_to_delete


def removeCategories(vars_to_delete):
    
    cat_idxs = [df_all.columns.values[x] not in numeric_cols for x in range(df_all.shape[1])]    
    
    df_sub = df_all.iloc[:,cat_idxs].groupby('Label')\
            .sum().transpose().reset_index()
    
    bad_cats = df_sub[(df_sub[1] == 0) & (df_sub[0] == 0)]['index']
    for cat in bad_cats:
        print(cat)
        vars_to_delete.add(cat)
        
    return vars_to_delete
        
    
#Create set to hold variables to delete
vars_to_delete = set()

vars_to_delete = coverageAnalysis(vars_to_delete)
vars_to_delete = coefofVar(vars_to_delete)
vars_to_delete = minmaxscaler(vars_to_delete)
vars_to_delete = removeCategories(vars_to_delete)

#Remove variables from above analysis
for var in vars_to_delete:
    del df_all[var]
    
df_all.to_csv("{}/{}/Data/{}_one_hot_norm_reduced.csv".format(path_to_results, issue_name, issue_name), index = False)


    
        
    
    
    
    
    

