#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 15:08:18 2018

@author: pr891
"""
#%%
########################################################################################################
#intial setting stuff that does not need to be run all the time 
#import os
#print(os.getcwd())
#os.chdir("/Users/pr891/Downloads/Solve/ds/Solve/Model_Pipeline")
import importlib
#importlib.reload(idlib)
#importlib.reload(isop)
importlib.reload(cf)
#import copy

#%%
#Librairies
import ID_lib as idlib
import config as cf
import issue_options as isop
import warnings
import numpy as np
import pandas as pd
import pickle
warnings.simplefilter('ignore')
########################################################################################################


#%%
### load and clean data

online_tracker = idlib.load_online_tracker(cf.path_online_tracker)

fault_snapshot = idlib.load_fault_snapshot(cf.path_fault_snap)

oepl_desc_df = idlib.load_options_data(cf.path_oepl_desc)

df = idlib.load_online_tracker_new(cf.path_engine_options)

df_oepl_option = idlib.merge_tracker_oepl_otions(df, oepl_desc_df)

df_pivot = idlib.pivot_tracker(df_oepl_option)

fat_table = idlib.merge_fualt(online_tracker, fault_snapshot)

fat_table = idlib.merge_oepl(fat_table, df_pivot)

fat_table = idlib.enhance_fat_table(fat_table)

##defining oepl cols here because we need the df - others are defined in lib
oepl_cols = df_oepl_option['OPT_ITEM_DESC_NEW'].unique()
## Remove NaN in OEPL Columns ##
float_idxs = np.array([type(oepl_cols[x]) == float for x in range(len(oepl_cols))])
oepl_cols = np.delete(oepl_cols, np.where(float_idxs)[0][0])


fat_table = idlib.vpcr_to_binary(idlib.vpcr_cols, fat_table)

unique_fault_codes = idlib.unique_fault(cf.fault_code_col_name, fat_table)

fc_df = idlib.fault_code_hot(cf.fault_code_col_name, fat_table, unique_fault_codes, cf.path_fault_look)

#this is taken care of later
#fat_table = idlib.clean_numeric(idlib.numeric_cols, fat_table)

# Get ESNs #
#needs to be fixed
oepl_cols = np.delete(oepl_cols, 540)

df_esn = fat_table[['ESN', 'EARLIEST_INDICATION_DATE']]

df_cat = fat_table[idlib.cat_cols]

df_num = fat_table[idlib.numeric_cols]

df_vpcr = fat_table[idlib.vpcr_cols]

df_oepl = fat_table[oepl_cols].fillna(0)

df_issue = fat_table[['INCDT_ISSUE_NUMBER']]

df_ot = pd.concat([df_esn, df_cat, df_num, df_oepl, df_issue, df_vpcr, fc_df], axis = 1)

df_ot.to_csv("/Users/pr891/Desktop/Data/fat_table_no_one_hot_raw.csv", index = False)

df_cat = idlib.cat_features(idlib.cat_cols, fat_table)

# Split Cols on important vals #
df_ot = pd.concat([df_esn, df_cat, df_num, df_oepl, df_issue, df_vpcr, fc_df], axis = 1)

#Save object (since that was a lot of work!)
df_ot.to_csv("/Users/pr891/Desktop/Data/fat_table_with_one_hot_raw.csv", index = False)

#Get extra cols for universal view#

df_extra = fat_table[idlib.extra_cols] 

pickle.dump(df_extra, open("/Users/pr891/Desktop/Data/universal_view_cols.p", "wb" ) )

#%%
### 01_Subset_On_Issue

# make folder for results
idlib.output_folder_mk(isop.path_to_results, isop.issue_name)

#read in some data - not really sure what this does
df_one_hot = idlib.read_and_filter(isop.file_one_hot, isop.mileage_min, isop.mileage_col)
df_no_one_hot = idlib.read_and_filter(isop.file_no_one_hot, isop.mileage_min, isop.mileage_col)

#read in some data for the issue
issue_df = idlib.load_esn_list(isop.incident_path, isop.esn_col, isop.esn_list)

df_all = idlib.subset_and_concat(df_one_hot, issue_df, df_no_one_hot, isop.engine_type, isop.esn_col)
df_not_normalized = idlib.subset_and_concat(df_no_one_hot, issue_df, df_no_one_hot, isop.engine_type, isop.esn_col)

#clean and normalize data
df_all_not_normalized = idlib.clean_and_standardize(df_all, cf.global_lower, cf.global_upper, standardize = False)
df_all_normalized = idlib.clean_and_standardize(df_all, cf.global_lower, cf.global_upper, standardize = True)
df_not_normalized = idlib.clean_and_standardize(df_not_normalized, cf.global_lower, cf.global_upper, standardize = False)

#write csv files
idlib.write_results(df_not_normalized, isop.path_to_results, isop.issue_name, data_folder = "Data", file_end = "raw")
idlib.write_results(df_all_not_normalized, isop.path_to_results, isop.issue_name, data_folder = "Data", file_end = "one_hot_raw")
idlib.write_results(df_all_normalized, isop.path_to_results, isop.issue_name, data_folder = "Data", file_end = "one_hot_norm")

#%%
### 02_First_Pass_Selection
# Load Data # - might be able to remove now that we have the df in the same funtion
df_all = pd.read_csv("{}/{}/Data/{}_one_hot_norm.csv".format(isop.path_to_results, isop.issue_name, isop.issue_name))
df_not_normalized = pd.read_csv("{}/{}/Data/{}_raw.csv".format(isop.path_to_results, isop.issue_name, isop.issue_name))

df_all = idlib.clean_sub_select(df_all, df_not_normalized, isop.no_coverage_perc_thresh, isop.coef_of_var_thresh, isop.min_max_thresh)

idlib.write_results(df_all, isop.path_to_results, isop.issue_name, data_folder = "Data", file_end = "one_hot_norm_reduced")  

#%%
### 03_Univariate_Analysis
## Load Data ##
df_all = pd.read_csv("{}/{}/Data/{}_one_hot_norm_reduced.csv".format(isop.path_to_results, isop.issue_name, isop.issue_name))
df_not_normalized = pd.read_csv("{}/{}/Data/{}_raw.csv".format(isop.path_to_results, isop.issue_name, isop.issue_name))
df_all_not_normalized = pd.read_csv("{}/{}/Data/{}_one_hot_raw.csv".format(isop.path_to_results, isop.issue_name, isop.issue_name))

df_datasets, new_views = idlib.create_datasets(df_all)

df_final_list = idlib.create_final_list(df_datasets, df_all_not_normalized, df_not_normalized)

df_oepl_sub = idlib.get_oepl(cf.path_engine_options, df_all_not_normalized, oepl_desc_df)

df_vpcr = idlib.get_vpcr(df_all_not_normalized)

df_fc = idlib.get_fault_code(df_all_not_normalized, cf.path_fault_look)

## Universal Views & Save Excel ##

df_universal_view = idlib.create_universal_view(df_not_normalized, cf.path_extra_pickle)
    
idlib.build_mab_chart(df_universal_view, isop.issue_name)    

idlib.univ_save_and_plot(isop.path_to_results, isop.issue_name, data_folder = "Data", new_views, df_final_list, df_universal_view, df_not_normalized, df_oepl_sub, df_vpcr, df_fc)

#%%
### 04_Multivariate_Analysis

# In simple sense, there is only need of two datasets to do the analysis of multivariate
# 1. The dataset which has no spurious values, null values have been imputed, numerical columns are normalized
# 2. The dataset which has no spurious values, null values are not removed, and numerical columns are not normalized
# Both datasets only have the columns remaining after the first pass removal of columns

# Read in raw data to be used in plots later
df_plots = idlib.load_raw_data(isop.path_processed_raw, isop.mileage_min)
normalized_limit = idlib.norm_miles(isop.mileage_min, df_plots)
df = idlib.load_norm_data(isop.path_processed_norm, normalized_limit)

# Read in file containing columns to be removed. These are features generated after the engine comes in for fixing, so they aren't useful for diagnosing an issue.
columns_to_remove = pd.read_csv(isop.path_columns_to_remove)['columns_to_remove'].tolist()

# Determine features in univariate output. Also create a color column which will be used to color the bars in a subsequent bar plot
all_uni_output = idlib.create_all_uni_output(isop.path_output)


# Remove features not needed
df_master = idlib.remove_unwanted_features(input_data = df, exclude_features = ['MES','EPAT'], columns_to_remove = columns_to_remove)

# Initialize the randomforest classifier 
rf = idlib.RandomForestClassifier(random_state = 777, n_estimators = 100, verbose = True, class_weight = "balanced",
                            n_jobs = 8, min_samples_split = 15)

# See how this is going to perform for our data. This will print the confusion matrix and r-squared
idlib.test_model_performance(input_data = df_master, id_features = ['id', 'Label','ESN', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'],
                       label_column = 'Label', classifier = rf, cv = 5)


idlib.univ_out(df_master, all_uni_output, isop.path_to_results, isop.issue_name, show_plot = True)

idlib.multivar_out(df_master, df_plots, all_uni_output, isop.path_to_results, isop.issue_name, show_plot = True)
#%%
### 05_Confounding_Analysis


#%%
### 06_Residual_Analysis

# Read in raw data to be used in plots later
df_plots = idlib.load_raw_data(isop.path_processed_raw, isop.mileage_min)
normalized_limit = idlib.norm_miles(isop.mileage_min, df_plots)
df = idlib.load_norm_data(isop.path_processed_norm, normalized_limit)

# Now the df_master has missing rows, so we will recreate it using df_plots
# This is just a mistake coming in from Sanjay. Ideally both df and df_plots should have same number of rows as they essentially the 
# same dataset, just that df_plots does not have numerical columns normalized and nulls imputed
df_plots = df_plots.loc[:,df.columns]

# Remove features not needed
df_plots = idlib.remove_unwanted_features(input_data = df_plots, exclude_features = ['MES','EPAT'], columns_to_remove = columns_to_remove)

all_avg_stats = idlib.create_all_avg_stats(isop.path_output)

res_df_one = idlib.create_res_df_one(isop.path_univariate_contributons)

# need to add back the initial_columns
idlib.calc_res(df_plots, res_df_one, all_avg_stats, isop.path_to_results, isop.issue_name)
#idlib.calc_res(df_plots, res_df_one, all_avg_stats, initial_columns, isop.path_to_results, isop.issue_name)

"""
TESTING BY CREATING FALSE NEGATIVES

Another way to test the Residual Analysis results is to delibrately create False Negatives and try to spot them. This basically
means that we will take a few EGR Cooler issues, and mark them as Not Issue (negative set) in the data. Then we build the model
and try to spot these issues which were falsely marked as negative issues
"""
#not sure what the rest of these are doing - need to check with Sanajy and/or Nimit
columns = input_features(top_feature_file = top_features, top_n = 40, exclude_features = tuple(['MES']), 
              other_columns = ["Label", "ESN", "EARLIEST_INDICATION_DATE", "INCDT_ISSUE_NUMBER", "id"],
              extra_features = ['INS_EA_OIL_TMP_SEC_SEV_1', 'REL_BUILD_YEAR_MONTH:2016_11'])

df_master = idlib.spy_algorithm(s = 0.25, l = 0.05, label_column = 'Label', df = df, all_columns = columns, 
             other_columns = ["Label", "ESN", "EARLIEST_INDICATION_DATE", "INCDT_ISSUE_NUMBER", "Spy", "id"])

# Now we will take a few issues and mark them as the negative set
issues_index = df_master.index[df_master['Label'] == 1].tolist()
random_set = random.sample(issues_index, 5)
random_set_id = df_master.id[random_set]
df_master.Label[random_set] = 0


fp_final, fn_final, confusion_matrix = find_mislabels(df_master = df_master, label_column = 'Label', id_column = 'id',
               other_columns = ['id', 'ESN', 'Label', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'],
               loops = 1)

#%%