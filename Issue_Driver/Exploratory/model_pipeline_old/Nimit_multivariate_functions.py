#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 09:52:44 2018

@author: Nimit Patel
"""

# Write a few functions that can be used for the multivariate analysis
def normalize_columns(df):
    for col in range(0,len(df.columns)):
        finite_vals = df[df.columns[col]].dropna()
        if len(finite_vals) > 0:
            scaled_vals = scale(finite_vals)
            df.iloc[finite_vals.index.values, np.where(df.columns.values == df.columns[col])[0][0]] = scaled_vals
        
        #Replace NaN with 0's (Mean)
        df[df.columns[col]] = df[df.columns[col]].replace(np.nan, 0)
    
    return(df)
    
def remove_unwanted_features(input_data, exclude_features, columns_to_remove):
    
    # Remove columns about price and amounts
    df_master = input_data.drop(columns_to_remove, axis = 1)
    df_master['id'] = df_master.index
    
    # Keep only the features other than MES and EPAT
    features_to_keep = list(df_master.columns)
    # Remove above mentioned
    for word in features_to_keep[:]:
        if word.startswith(tuple(exclude_features)):
            features_to_keep.remove(word)
    
    # Keep only features other the ones removed above
    df_master = df_master.loc[:,features_to_keep]
    
    return(df_master)


def test_model_performance(input_data, id_features, label_column, classifier, cv):
    
    # From the input data, keep only those columns which are actually features for modelling, others which are id columns should be removed
    features = input_data[input_data.columns.difference(id_features)].reset_index(drop = True)
    features = features.dropna(axis = 1, how = 'all') # Remove any columns that are completely NA
    labels = input_data[label_column]
    
    # Try a crossvalidated approach
    preds = cross_val_predict(X = features, y = labels, estimator = classifier, cv = 10)
    
    # Check model performance for crossvalidated approach
    confusion_matrix = pd.crosstab(labels, preds, rownames=['True'], colnames=['Predicted'], margins=True)
    
    # Also obtain the crossvalidated r-squared
    r2 = make_scorer(r2_score)
    r2 = cross_val_score(X = features, y = labels, estimator = classifier, cv = 10, scoring = r2).mean()
    return(confusion_matrix, r2)


def implement_tree_interpreter_univariate(input_data, id_features, label_column, classifier):
    
    # From the input data, keep only those columns which are actually features for modelling, others which are id columns should be removed
    features = input_data[input_data.columns.difference(id_features)].reset_index(drop = True)
    features = features.dropna(axis = 1, how = 'all') # Remove any columns that are completely NA
    labels = input_data[label_column]
    
    # Fir the classifier
    classifier.fit(X = features, y = labels)

    # Implement the tree interpeter univariate. Now here we are predicting on the entire same data that we trained on, but since
    # we are just trying to get the individual contributions of features, it should be okay
    prediction, bias, contributions = ti.predict(classifier, features)
    
    # Take raw contributions by the features
    raw_contri = pd.DataFrame(contributions[:,:,1], columns = features.columns, 
                                  index = features.index)
    # Consider only contributions for the issue observations
    issue_labels = np.where(labels == 1)
    issue_contri = raw_contri.loc[(issue_labels)]
    
    # Generate the table to provide as output
    res_df_one = pd.DataFrame(issue_contri.loc[:,:].mean(), columns = ['contribution'])
    res_df_one['abs_contributions'] = abs(res_df_one['contribution'])
    res_df_one['feature'] = res_df_one.index
    res_df_one['Rank'] = res_df_one['abs_contributions'].rank(method='dense', ascending=False)
    
    return(res_df_one)


def implement_tree_interpreter_multivariate(input_data, id_features, label_column, classifier):
    
    # From the input data, keep only those columns which are actually features for modelling, others which are id columns should be removed
    features = input_data[input_data.columns.difference(id_features)].reset_index(drop = True)
    features = features.dropna(axis = 1, how = 'all') # Remove any columns that are completely NA
    labels = input_data[label_column]
    
    # Fir the classifier
    classifier.fit(X = features, y = labels)
    
    # Now set up the tree interpreter for cross contributions, in order to spot most interesting interactions
    prediction, bias, contributions = ti.predict(rf, features, joint_contribution=True)
    aggregated_contributions = utils.aggregated_contribution(contributions)
    
    columns = np.asarray(list(features.columns))
    res = []
    for k in set(aggregated_contributions.keys()):
        res.append(([columns[index] for index in k], 
                    aggregated_contributions.get(k, 0)))  
    
    res = sorted(res, reverse = True)
    res_df = pd.DataFrame(np.array(res), columns = list(["feature_combination","contribution"]))
    res_df['feature_combination'] = res_df['feature_combination'].astype('str') 
    res_df['contribution'] = [el[1] for el in res_df['contribution']]
    res_df['comma_count'] = res_df.feature_combination.str.count(',')
    res_df['abs_contributions'] = res_df.contribution.abs()


    res_df_two = res_df[(res_df['comma_count'] == 1)].sort_values(by = ['abs_contributions'], ascending = False)
    res_df_three = res_df[(res_df['comma_count'] == 2)].sort_values(by = ['abs_contributions'], ascending = False)

    # Clean these contribution dataframes to make them shareable
    res_df_two = clean_contributions_df(contributions_df = res_df_two, feature_column = 'feature_combination', drop_columns = ['feature_combination', 'comma_count'])
    res_df_three = clean_contributions_df(contributions_df = res_df_three, feature_column = 'feature_combination', drop_columns = ['feature_combination', 'comma_count'])

    # The idea will be to take feature_1 and feature_2 columns and detect if they are Numerical or Categorical
    res_df_two.loc[res_df_two.feature_1.isin(numeric_cols), 'feature_1_type'] = 'numerical'
    res_df_two['feature_1_type'].fillna('categorical', inplace = True)
    res_df_two.loc[res_df_two.feature_2.isin(numeric_cols), 'feature_2_type'] = 'numerical'
    res_df_two['feature_2_type'].fillna('categorical', inplace = True)

    res_df_three.loc[res_df_three.feature_1.isin(numeric_cols), 'feature_1_type'] = 'numerical'
    res_df_three['feature_1_type'].fillna('categorical', inplace = True)
    res_df_three.loc[res_df_three.feature_2.isin(numeric_cols), 'feature_2_type'] = 'numerical'
    res_df_three['feature_2_type'].fillna('categorical', inplace = True)
    res_df_three.loc[res_df_three.feature_3.isin(numeric_cols), 'feature_3_type'] = 'numerical'
    res_df_three['feature_3_type'].fillna('categorical', inplace = True)

    res_df_two['Rank'] = res_df_two['abs_contributions'].rank(method='dense', ascending=False)
    res_df_three['Rank'] = res_df_three['abs_contributions'].rank(method='dense', ascending=False)
    
    return(res_df_two, res_df_three)
    
def clean_contributions_df(contributions_df, feature_column, drop_columns):
    column = contributions_df[feature_column]
    
    for i in range(len(column)):
        element = column.iloc[i]
        element = element.replace("'","")
        element = element.replace("[","")
        element = element.replace("]","")
        splits = [x.strip() for x in element.split(',')]
        
        for j in range(len(splits)):
            contributions_df.at[contributions_df.index[i], 'feature_' + str(j+1)] = splits[j]
        
    contributions_df = contributions_df.drop(drop_columns, axis = 1)
    return(contributions_df)


def create_categorical_table(df, feature1, feature2, label_column):
    #feature1_levels = df[feature1].astype('category').cat.categories.tolist()
    #feature2_levels = df[feature2].astype('category').cat.categories.tolist()
    
    feature1_levels = [0.0, 1.0]
    feature2_levels = [0.0, 1.0]

    cat_df = pd.DataFrame([])
    for i in range(len(feature1_levels)):
        for j in range(len(feature2_levels)):
            cat_df.loc[2*i + j,'non-issue'] = len(df[(df[feature1] == feature1_levels[i]) & (df[feature2] == feature2_levels[j]) & (df['Label'] == 0)].index)
            cat_df.loc[2*i + j,'issue'] = len(df[(df[feature1] == feature1_levels[i]) & (df[feature2] == feature2_levels[j]) & (df['Label'] == 1)].index)
            cat_df.loc[2*i + j,'feature_combination'] = str(feature1).replace(":","_") + '=' + str(feature1_levels[i]) + ' and ' + str(feature2).replace(":","_") + '=' + str(feature2_levels[j])
    
    cat_df = cat_df[['feature_combination', 'non-issue', 'issue']]
    cat_df['sample_size'] = cat_df['non-issue'] + cat_df['issue']
    cat_df['incident_rate'] = cat_df['issue']*100/cat_df['sample_size']
    cat_df = cat_df.replace(np.nan, 0)
        
    return(cat_df)

# It might also help to write a function which takes in the input from the user and plots the chart accordingly
def create_plot(df, feature1, feature2, label_column, output_destination):
    
    if(feature1 in numeric_cols):
        feature1_type = 'numeric'
    else:
        feature1_type = 'categorical'
        
    if(feature2 in numeric_cols):
        feature2_type = 'numeric'
    else:
        feature2_type = 'categorical'
    
    if((feature1_type == 'numeric') & (feature2_type == 'numeric')):
        fig, ax = plt.subplots()
        p1 = ax.scatter(df_plots.query("Label == 0")[feature1], 
                        df_plots.query("Label == 0")[feature2], 
                        c = 'blue', s = 10, marker = 'o')
        p2 = ax.scatter(df_plots.query("Label == 1")[feature1], 
                        df_plots.query("Label == 1")[feature2], 
                        c = 'red', s = 50, marker = 'X')
    
        count_p1 = sum((df_plots.query("Label == 0")[feature1].notnull()) & (df_plots.query("Label == 0")[feature2].notnull()))
        count_p2 = sum((df_plots.query("Label == 1")[feature1].notnull()) & (df_plots.query("Label == 1")[feature2].notnull()))
    

        plt.legend((p1, p2), ('Non-issue ' + '(Count:' + str(count_p1) + ')', 'Issue ' + '(Count:' + str(count_p2) + ')'), frameon = True)
        plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
        plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
        plt.savefig((output_destination + '/output_plot.png'),bbox_inches='tight')
        plt.close()
        
    elif((feature1_type == 'categorical') & (feature2_type == 'categorical')):
        df[feature1] = df[feature1].astype('category')
        df[feature2] = df[feature2].astype('category')
        cat_df = create_categorical_table(df = df_plots, feature1 = feature1, feature2 = feature2, label_column = 'Label')
    
        x_avg = len(df_plots[df_plots['Label'] == 1])*100/df_plots.shape[0]
        fig, ax = plt.subplots()
        p1 = ax.barh(cat_df['feature_combination'], cat_df['incident_rate'], color = "red", alpha = 0.5)
    
        for j in range(cat_df.shape[0]):
            ax.text(cat_df.loc[j,'incident_rate'] + 0.1, j, 'N: ' + str(int(cat_df.loc[j,'sample_size'])), color='black', fontweight='bold')
    
        ax.set_xlabel('Incident Rate (%)')
        plt.axvline(x = x_avg, linestyle = 'dashed')
        plt.text(x_avg + 0.1,2.2,'average incident rate', rotation = 90)
        plt.savefig((output_destination + '/output_plot.png'),bbox_inches='tight')
        plt.close()
        
    else:
        df[feature2] = df[feature2].astype('category')
        sns.set_style("darkgrid")
        df_plots_subset = df_plots.loc[(df_plots[feature1].notnull()) & (df_plots[feature2].notnull()), :]
        ax = sns.boxplot(x= feature1, y= feature2, data = df_plots_subset, hue = "Label", 
                         palette = ['blue','red'], saturation = 0.5, fliersize = 2)
        handles, labels = ax.get_legend_handles_labels()
        labels = ['Non-issue', 'Issue']
        plt.legend(handles[0:2], labels[0:2], frameon = True)
        medians = df_plots_subset.groupby([feature1,'Label'])[feature2].median().values.tolist()
    
        nobs = []
        for k in df_plots_subset[feature1].astype('category').cat.categories.tolist():
        
            if (len(df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].astype('category').cat.categories.tolist()) == 2):
                counts = df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
                nobs.extend(counts)
            elif ((df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].astype('category').cat.categories.tolist()[0] == 0)):
                counts = df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
                counts.append(0)
                nobs.extend(counts)
            else:
                counts = [0]
                element = df_plots_subset[(df_plots_subset[feature1] == k) & (df_plots_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
                counts.extend(element)
                nobs.extend(counts)
    
        x = [i for i, e in enumerate(nobs) if e == 0]
        for p in x:
            medians.insert(p,0.0)

        pos = list([-0.2, 0.2, 0.8,1.2])
  
        for j in range(len(nobs)):
            ax.text(pos[j], medians[j] + 0.05, 'N: ' + str(nobs[j]),
                    horizontalalignment='center', size= "medium", color='black', weight='bold')
        
        plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
        plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
        plt.savefig((output_destination + '/output_plot.png'),bbox_inches='tight')
        plt.close()
        
        

    
    