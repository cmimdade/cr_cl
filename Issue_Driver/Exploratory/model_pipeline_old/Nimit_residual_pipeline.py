                                                                                                                                    #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 10:29:47 2018

@author: Nimit Patel
"""

"""
--- AIM ---

This code is the pipeline for the residual analysis
It essentially is using the functions defined in the order that they need to be used, so that the process is completely clear,
and it can be tweaked easily as per requirement

--- PROCESS ---

The process here will be to build a randomforest classifier for a particular issue, and use it to find the predicted probabilities for
every incident. Using these probabilities, find the top false positives (Label = 1 but model gives low probability of being issue)
and false negatives(Label = 0 but model gives high probability of being issue). Do this process over a large number of iterations, and 
flag/display incidents which get marked repeatedly as false positive/negative.

Note: the definition of False Positive/Negative is from the user's perspective. Meaning a False Positive is something that the user
manually marked as an issue, but the model claims it was a mistake by the user and it should actually not be an issue (should be zero). 
So its a False Positive by the user.

"""

import random
import math
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_predict
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import scale
from openpyxl import load_workbook
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from random import sample
from sklearn.model_selection import cross_val_score
from itertools import compress
from sklearn.ensemble import RandomForestClassifier


# Data which will go into analysis, which is normalized
df = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/EGR_Cooler_one_hot_norm_reduced.csv")
# Data which will be used for plots
df_plots = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/EGR_Flow_one_hot_raw_numeric.csv")
# Read in file containing columns to be removed. These are features generated after the engine comes in for fixing, so they aren't useful for diagnosing an issue.
columns_to_remove = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/columns_to_remove.csv")['columns_to_remove'].tolist()
# Load the output from multivariate, so that we only use features here which have some controbution towards the prediction
res_df_one = pd.read_csv("Downloads/Solve/multivariate_results/egr_cooler_results/20180402_univariate_contributons.csv")
# Add columns about average values for each of these features to this data
uni_output = pd.ExcelFile("Downloads/Solve/OneDrive_1_3-11-2018/variable_results_egr_cooler_032518.xlsx")

# Now filter out engines which are less than 1500 miles
normalized_limit = (1500 - df_plots['INS_TI_ENGINE_DISTANCE_MILES'].mean())/df_plots['INS_TI_ENGINE_DISTANCE_MILES'].std()
df_plots = df_plots[(df_plots['INS_TI_ENGINE_DISTANCE_MILES'] >= 1500) | (df_plots['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]
df = df[(df['INS_TI_ENGINE_DISTANCE_MILES'] >= normalized_limit) | (df['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]

# Now the df_master has missing rows, so we will recreate it using df_plots
# This is just a mistake coming in from Sanjay. Ideally both df and df_plots should have same number of rows as they essentially the 
# same dataset, just that df_plots does not have numerical columns normalized and nulls imputed
df_plots = df_plots.loc[:,df.columns]

# Remove features not needed
df_plots = remove_unwanted_features(input_data = df_plots, exclude_features = ['MES','EPAT'], columns_to_remove = columns_to_remove)

# Take features which contribute
contributing_features = res_df_one[res_df_one['abs_contributions'] != 0]['Unnamed: 0']
features_order = res_df_one[res_df_one['abs_contributions'] != 0].sort_values(by = 'abs_contributions', ascending = False)['Unnamed: 0']

# Also, get together the average and other statistics for all the features, stuff that is generated during the Univariate Analysis
design = pd.read_excel(uni_output, 'Design')
manuf = pd.read_excel(uni_output, 'Manufacturing')
usage = pd.read_excel(uni_output, 'Usage')
columns_we_want = ['Variable', 'Median of Issues', 'Median of Non-Issues', 'Mean of Issues',
       'Mean of Non-Issues', 'SD of Issues', 'SD of Non-Issues',
       'Rate of Issues', 'Rate of Non-Issues', 'Sample Size of Issues',
       'Sample Size of Non-Issues']
design = design.loc[:, columns_we_want]
manuf = manuf.loc[:, columns_we_want]
usage = usage.loc[:, columns_we_want]
all_avg_stats = pd.concat([design, manuf, usage], axis = 0)
all_avg_stats = all_avg_stats.rename(columns = {'Variable':'feature_name'})


# Generate the list of false positives and false negatives 
fp_final, fn_final, confusion_matrix_result, df_master = find_mislabels(df_master = df_plots, label_column = 'Label', 
                                                                      id_column = 'id',
                                                                      other_columns = ['id', 'ESN', 'Label', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'],
                                                                      loops = 1, top_n = 20,
                                                                      use_contributing_features_only = True,
                                                                      contributing_features = contributing_features)


# Generate the data for these false positive and false negative incidents
fp_final_data = df_master[df_master.id.isin(fp_final)]
fn_final_data = df_master[df_master.id.isin(fn_final)]

# Show only those columns we care about, which were actually used for the prediction
identifier_columns = ['ESN', 'issue_probability', 'EARLIEST_INDICATION_DATE']
identifier_columns.extend(features_order)

# Reorder columns, showing important ones for prediction above
fp_final_data = fp_final_data.loc[:,identifier_columns]
fn_final_data = fn_final_data.loc[:,identifier_columns]

# Now it is necessary to get these into a state which can be interpreted
# Take a transpose
fp_final_transpose = pd.concat([pd.DataFrame(fp_final_data.columns, columns = ['feature_name']), pd.DataFrame(np.array(fp_final_data).T)], axis = 1)
fn_final_transpose = pd.concat([pd.DataFrame(fn_final_data.columns, columns = ['feature_name']), pd.DataFrame(np.array(fn_final_data).T)], axis = 1)

# Now we will only show those feature results which have been shown in the univariate output
fp_final_transpose = fp_final_transpose[(fp_final_transpose['feature_name'].isin(all_avg_stats['feature_name'].tolist()))|(fp_final_transpose['feature_name'].isin(initial_columns))]
fn_final_transpose = fn_final_transpose[(fn_final_transpose['feature_name'].isin(all_avg_stats['feature_name'].tolist()))|(fn_final_transpose['feature_name'].isin(initial_columns))]

fp_final_transpose = pd.merge(fp_final_transpose, all_avg_stats, how='left', on = ['feature_name'])
fn_final_transpose = pd.merge(fn_final_transpose, all_avg_stats, how='left', on = ['feature_name'])

# Now additionally also add the final contributions of these variables towards the end
res_df_one = res_df_one.rename(columns = {'Unnamed: 0':'feature_name'})
res_df_one = res_df_one.rename(columns = {'contribution':'Multivariate Importance Factor (contribution)'})

fp_final_transpose = pd.merge(fp_final_transpose, res_df_one[['feature_name', 'Multivariate Importance Factor (contribution)','Rank']], how='left', on = ['feature_name'])
fn_final_transpose = pd.merge(fn_final_transpose, res_df_one[['feature_name', 'Multivariate Importance Factor (contribution)','Rank']], how='left', on = ['feature_name'])

fp_final_transpose.to_csv("Downloads/Solve/residual_results/03292018_residual_results_false_positive.csv", na_rep = "NA")
fn_final_transpose.to_csv("Downloads/Solve/residual_results/03292018_residual_results_false_negative.csv", na_rep = "NA")

"""
TESTING BY CREATING FALSE NEGATIVES

Another way to test the Residual Analysis results is to delibrately create False Negatives and try to spot them. This basically
means that we will take a few EGR Cooler issues, and mark them as Not Issue (negative set) in the data. Then we build the model
and try to spot these issues which were falsely marked as negative issues
"""

columns = input_features(top_feature_file = top_features, top_n = 40, exclude_features = tuple(['MES']), 
              other_columns = ["Label", "ESN", "EARLIEST_INDICATION_DATE", "INCDT_ISSUE_NUMBER", "id"],
              extra_features = ['INS_EA_OIL_TMP_SEC_SEV_1', 'REL_BUILD_YEAR_MONTH:2016_11'])

df_master = spy_algorithm(s = 0.25, l = 0.05, label_column = 'Label', df = df, all_columns = columns, 
             other_columns = ["Label", "ESN", "EARLIEST_INDICATION_DATE", "INCDT_ISSUE_NUMBER", "Spy", "id"])

# Now we will take a few issues and mark them as the negative set
issues_index = df_master.index[df_master['Label'] == 1].tolist()
random_set = random.sample(issues_index, 5)
random_set_id = df_master.id[random_set]
df_master.Label[random_set] = 0


fp_final, fn_final, confusion_matrix = find_mislabels(df_master = df_master, label_column = 'Label', id_column = 'id',
               other_columns = ['id', 'ESN', 'Label', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'],
               loops = 1)

