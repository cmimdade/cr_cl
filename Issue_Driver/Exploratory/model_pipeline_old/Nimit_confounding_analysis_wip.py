#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 28 16:23:47 2018

@author: Nimit Patel
"""

"""
--- AIM ---

The aim of this piece of code is to find the top confounding variables after the first pass selection of features.

--- PROCESS ---

Based on the existing work done so far, different processes will be followed for finding top confounding variables based on the 
type of the features being compared.

1. Categorical vs categorical: Cramer's V value will be used in order to find the top confounded variables. The higher the value,
the higher the confounding.
- Another way of doing that is to build a logistic regression model, with each categorical feature being used as the response variable
turn by turn. The independed features with high p-values can be flagged as the confounders.

2. Numerical vs. numerical: Build a linear/ridge regression model, by taking each independent variables as the dependent variable
turn by turn, and use the 
    
3. Categorical vs. numerical: Build a logistic regression, taking each categorical feature one by one as the response variable, and
all the numerical features as the independent variables, and use the odds ratio to find top confounding features

--- HOW TO READ THIS CODE ---

This code has the following sections:
1. Initial section reads in the files needed. While you do this, also run the code called 'num_cat_cols.py'. That is basically just
the list of numerical and categorical columns for this dataset.

2. Perform the data alterations needed in order to do the analysis, for example select engines that have run above 1500 miles, 
remove categorical features having no variance (all zeros or ones) etc. Ideally all this should be done in the first pass feature selection.

3. Do the confounding analysis for cat vs cat, cat vs num, and num vs num.

"""
#%%
import numpy as np
import pandas as pd
from scipy.stats import chi2_contingency
from sklearn.linear_model import Ridge
from scipy import stats
from statsmodels.regression import linear_model
from matplotlib import pyplot as plt
from sklearn.linear_model import LogisticRegression
import datetime

# Load the top variables data, and filter out all categorical variables
# For this we basically need the data for the EGR cooler issue, which has only variables deemed as necessary after first pass removal
# of features

df = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/EGR_Cooler_one_hot_norm_reduced.csv")
# Data which will be used for plots
df_plots = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/EGR_Flow_one_hot_raw_numeric.csv")
# Read in file for columns to remove (they are features generated after the engine has come into the shop for fixing, they aren't useful drivers for analyzing the failure)
columns_to_remove = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/columns_to_remove.csv")['columns_to_remove'].tolist()
# Also read in the features with their importance score from the univariate analysis, in order to provide a rank against them
uni_output = pd.ExcelFile("Downloads/Solve/OneDrive_1_3-11-2018/variable_results_egr_cooler_032518.xlsx")


design = pd.read_excel(uni_output, 'Design')
manuf = pd.read_excel(uni_output, 'Manufacturing')
usage = pd.read_excel(uni_output, 'Usage')
all_uni_output = pd.concat([design, manuf, usage], axis = 0)
all_uni_output['Rank'] = all_uni_output['Importance Score'].rank(method='dense', ascending=False)

# Now filter out engines which are less than 1500 miles
normalized_limit = (1500 - df_plots['INS_TI_ENGINE_DISTANCE_MILES'].mean())/df_plots['INS_TI_ENGINE_DISTANCE_MILES'].std()
df_plots = df_plots[(df_plots['INS_TI_ENGINE_DISTANCE_MILES'] >= 1500) | (df_plots['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]
df = df[(df['INS_TI_ENGINE_DISTANCE_MILES'] >= normalized_limit) | (df['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]

# Remove some columns which are amount columns and are not needed
# Also remove columns about ESN, Earliest Indication Date, Label etc.
columns_to_remove.extend(['EARLIEST_INDICATION_DATE', 'ESN', 'Label','INCDT_ISSUE_NUMBER'])
df = df.drop(columns_to_remove, axis = 1)

"""
IMP NOTE: The list 'numeric_cols' (below) is a list of columns which are numerical in nature. The list has been created by Sanjay. Because of the tactical, ad-hoc
changes that kept on happening in the course of the study, this list kept on changing, so the list maintained by Sanjay and me might be
slightly different. This might create errors. So a good thing to invest time in is to create a finalized version of this list. 
"""

# Keep only categorical variables from this data
# numeric_cols here is the big list of features, given by Sanjay
cat_df = df[df.columns.difference(numeric_cols)]

# Remove columns which are completely zero throughout or is equal to the number of rows, because that basically means it also 
# has no variance at all
cat_df = cat_df.loc[:,cat_df.sum() != 0]
cat_df = cat_df.loc[:,cat_df.sum() != cat_df.shape[0]]

# Generate dataframe of just numerical variables, all normalized, nulls imputed, and spurious values removed
num_data = df.loc[:,df.columns.isin(numeric_cols)]

# There should also be an attempt made at running this analysis by removing all the missing values instead of imputing them
# df_plots here is the entire dataset with the missing values as well
# Keep only the columns of df_plots which are in the dataset 'num_data', as those are the ones reminaing after first pass filtering, and are numerical

num_data_raw = df_plots.loc[:,df_plots.columns.isin(num_data.columns.tolist())]
# We cannot drop all the nulls, as that might need up dropping all the rows
# So first lets see how many nulls each column has and choose ones which have less than 55% missing data
# This number 50% was came to by some trials, trying to not remove too many columns

num_data_raw = num_data_raw.loc[:,(num_data_raw.isnull().sum()/num_data_raw.shape[0] <= 0.5)]
num_data_raw = num_data_raw.dropna(how = "any")


"""
CATEGORICAL VS. CATEGORICAL
"""
#%%
# Do CATEGORICAL VS. CATEGORICAL USING CRAMER'S V
# Define the function for calculating the Cramer's V value for each pair of categorical variables here

def cramers_corrected_stat(confusion_matrix):
    
    """ 
    Calculate Cramers V statistic for categorial-categorial association
    uses bias correction from Bergsma and Wicher, 
    https://en.wikipedia.org/wiki/Cram%C3%A9r%27s_V
    """
    chi2 = chi2_contingency(confusion_matrix)[0]
    n = confusion_matrix.sum().sum()
    phi2 = chi2/n
    r,k = confusion_matrix.shape
    phi2corr = max(0, phi2 - ((k-1)*(r-1))/(n-1))    
    rcorr = r - ((r-1)**2)/(n-1)
    kcorr = k - ((k-1)**2)/(n-1)
    return np.sqrt(phi2corr / min( (kcorr-1), (rcorr-1)))

lis = []
for col1 in cat_df.columns:
    temp_lis = []
    for col2 in cat_df.columns:
        if col1 != col2:
            confusion_matrix = pd.crosstab(cat_df[col1], cat_df[col2])
            cv = cramers_corrected_stat(confusion_matrix)
            temp_lis.append(cv)
        else:
            temp_lis.append(np.nan)
    lis.append(temp_lis)

cramerv_df = pd.DataFrame(np.array(lis), columns = cat_df.columns, index = cat_df.columns)

# Now move across each row and pick top features confounded for each row value, but only in cases where the Cramer's V value is greater than 0.2

arr = cramerv_df.values
index_names = cramerv_df.index
col_names = cramerv_df.columns

R,C = np.where(np.triu(arr,1)>0.2)


out_arr = np.column_stack((index_names[R],col_names[C],arr[R,C]))
df_out = pd.DataFrame(out_arr,columns=['Feature','Top confounded feature','Cramers V value'])

# Now make the output in such a manner which can be presented as a result, by adding ranks and sorting based on the feature rank
df_out = pd.merge(df_out, all_uni_output[['Variable','Rank']], how = 'left', left_on = 'Feature', right_on = 'Variable')
df_out = df_out.rename(columns = {'Rank' : 'Feature rank'})
df_out = pd.merge(df_out, all_uni_output[['Variable','Rank']], how = 'left', left_on = 'Top confounded feature', right_on = 'Variable')
df_out = df_out.rename(columns = {'Rank' : 'Confounded feature rank'})
df_out = df_out.drop(['Variable_x','Variable_y'], axis = 1)

df_out = df_out.sort_values(by = ['Feature rank', 'Confounded feature rank'])
df_out = df_out.groupby('Feature').head(5)

# Reorder columns
df_out = df_out.loc[:,['Feature', 'Feature rank', 'Top confounded feature', 'Confounded feature rank', 'Cramers V value']]

# Get the result out in a csv
#df_out.to_csv("Downloads/Solve/confounding_results/egr_cooler_results/29032018_catxcat_confounding.csv")
df_out.to_csv("Downloads/Solve/confounding_results/egr_cooler_results/" + str(datetime.date.today()).replace("-","") + "_catxcat_cramers_confounding.csv")

# Do CATEGORICAL VS CATEGORICAL using logistic regression
top_n = 5
final_data = pd.DataFrame([]) # Filling out a dataframe in the loop for getting results is not optimal.
for col in cat_df.columns:
    X = cat_df.drop(col, axis = 1)
    y = cat_df[col]
    lr = LogisticRegression(penalty = 'l2').fit(X,y)
    
    odds = pd.Series(np.exp(lr.coef_)[0], index = X.columns)
    top_odds = pd.Series(odds.sort_values(ascending = False).head(top_n))
    top_vars = list(odds.sort_values(ascending = False).index[:top_n])
    
    temp = pd.DataFrame(top_odds, index = top_odds.index, columns = ['odds_ratio'])
    temp['Top confounded feature'] = temp.index
    temp['Feature'] = col
        
    final_data = pd.concat([final_data, temp], axis = 0)

# Get this raw result into a presentable format
# Add the rank information for features
final_data = pd.merge(final_data, all_uni_output[['Variable','Rank']], how = 'inner', left_on = 'Feature', right_on = 'Variable')
final_data = final_data.rename(columns = {'Rank' : 'Feature rank'})
final_data = pd.merge(final_data, all_uni_output[['Variable','Rank']], how = 'inner', left_on = 'Top confounded feature', right_on = 'Variable')
final_data = final_data.rename(columns = {'Rank' : 'Confounded feature rank'})
final_data = final_data.drop(['Variable_x','Variable_y'], axis = 1)

# Reorder columns and sort rows
final_data = final_data.loc[:,['Feature', 'Feature rank', 'Top confounded feature', 'Confounded feature rank', 'odds_ratio']]
final_data = final_data.sort_values(by = ['Feature rank', 'Confounded feature rank']) 

# Print out as csv
final_data.to_csv("Downloads/Solve/confounding_results/egr_cooler_results/" + str(datetime.date.today()).replace("-","") + "_catxcat_logistic_confounding.csv")

"""
NUMERICAL vs. NUMERICAL
"""
#%%
def get_numerical_confounders(df, top_n):
    final_data = pd.DataFrame([]) # Filling out a dataframe in the loop for getting results is not optimal.
    
    for col in df.columns:
        X = df.drop(col, axis = 1) # Take all columns except one column as the independent features
        y = df[col] # Create that one independent feature as the dependent feature
    
        lm = linear_model.OLS(y,X, missing = 'drop')
        lm = lm.fit(L1_wt = 0) # Fit a ridge regression to this above configuration of data
        
        p_values = pd.Series(lm.pvalues, index = lm.pvalues.index)
        top_p = pd.Series(p_values.sort_values().head(top_n))
        top_vars = list(p_values.sort_values().index[:top_n])
        
        temp = pd.DataFrame(top_p, index = top_p.index, columns = ['p-value'])
        temp['Top confounded feature'] = temp.index
        temp['Feature'] = col
        
        final_data = pd.concat([final_data, temp], axis = 0)
    
    # Add the rank information for features
    final_data = pd.merge(final_data, all_uni_output[['Variable','Rank']], how = 'inner', left_on = 'Feature', right_on = 'Variable')
    final_data = final_data.rename(columns = {'Rank' : 'Feature rank'})
    final_data = pd.merge(final_data, all_uni_output[['Variable','Rank']], how = 'inner', left_on = 'Top confounded feature', right_on = 'Variable')
    final_data = final_data.rename(columns = {'Rank' : 'Confounded feature rank'})
    final_data = final_data.drop(['Variable_x','Variable_y'], axis = 1)

    # Reorder columns and sort rows
    final_data = final_data.loc[:,['Feature', 'Feature rank', 'Top confounded feature', 'Confounded feature rank', 'p-value']]
    final_data = final_data.sort_values(by = ['Feature rank', 'Confounded feature rank']) 
    return(final_data)
    
"""
THIS PIECE OF CODE USES MATRIX MULTIPLICATION TO GET TO THE P-VALUES OF THE INDEPENDENT FEATURES. JUST HERE FOR FYI.
    
lm = Ridge()
params = np.append(lm.intercept_,lm.coef_)
predictions = lm.predict(X)
    

# Use the Mean Square Error as the metric for assessing the performance of the model here
#newX = pd.DataFrame({"Constant":np.ones(len(X))}).join(pd.DataFrame(X))
#MSE = (sum((y-predictions)**2))/(len(newX)-len(newX.columns))
# This method somehow introduces NAs into the newX, so the below two lines have been used to do the same thing, just a bit inefficien
# Note if you don't want to use a DataFrame replace the two lines above with
newX = np.append(np.ones((len(X),1)), X, axis=1)
newX = pd.DataFrame(newX)
MSE = (sum((y-predictions)**2))/(len(newX)-1)

# Calculate the variancd and standard deviation of the coefficients in order to use them for p-value calculation
if (np.linalg.det(np.dot(newX.T,newX)) != 0): # This check is to see if the matrix is singular or not. We can only invert the matrix if it is not singular
    var_b = MSE*(np.linalg.inv(np.dot(newX.T,newX)).diagonal())
    sd_b = np.sqrt(var_b)
    ts_b = params/ sd_b
    
    p_values =[2*(1-stats.t.cdf(np.abs(i),(len(newX)-1))) for i in ts_b]
    
    sd_b = np.round(sd_b,3)
    ts_b = np.round(ts_b,3)
    p_values = np.round(p_values,3)
    params = np.round(params,4)
    
    myDF3 = pd.DataFrame()
    myDF3["Coefficients"],myDF3["Standard Errors"],myDF3["t values"],myDF3["Probabilites"] = [params,sd_b,ts_b,p_values]
    #print(myDF3)
"""    

output1 = get_numerical_confounders(df = num_data, top_n = 5)
output2 = get_numerical_confounders(df = num_data_raw, top_n = 5)

output1 = output1.dropna(how = "any")
output2 = output2.dropna(how = "any")

# Get the result out in a csv
output1.to_csv("Downloads/Solve/confounding_results/egr_cooler_results/" + str(datetime.date.today()).replace("-","") + "_numxnum_imputed_confounding.csv")
output2.to_csv("Downloads/Solve/confounding_results/egr_cooler_results/" + str(datetime.date.today()).replace("-","") + "_numxnum_confounding.csv")

"""
CATEGORICAL VS. NUMERICAL
"""
#%%
# One way to find top numerical features confounded to a categorical feature will be a logistic regression.
# Each categorical feature will be taken as a response variable one by one, and all the numerical features can be the independent features

# cat_df and num_data are the two datasets we deal with. They have been created in the code above, and their index are aligned

final_data = pd.DataFrame([]) # Filling out a dataframe in the loop for getting results is not optimal.
for col in cat_df.columns:
    X = num_data
    y = cat_df[col]
    lr = LogisticRegression(penalty = 'l2').fit(X,y)
    
    odds = pd.Series(np.exp(lr.coef_)[0], index = X.columns)
    top_odds = pd.Series(odds.sort_values(ascending = False).head(top_n))
    top_vars = list(odds.sort_values(ascending = False).index[:top_n])
    
    temp = pd.DataFrame(top_odds, index = top_odds.index, columns = ['odds_ratio'])
    temp['Top confounded feature'] = temp.index
    temp['Feature'] = col
        
    final_data = pd.concat([final_data, temp], axis = 0)

# Get this raw result into a presentable format
# Add the rank information for features
final_data = pd.merge(final_data, all_uni_output[['Variable','Rank']], how = 'inner', left_on = 'Feature', right_on = 'Variable')
final_data = final_data.rename(columns = {'Rank' : 'Feature rank'})
final_data = pd.merge(final_data, all_uni_output[['Variable','Rank']], how = 'inner', left_on = 'Top confounded feature', right_on = 'Variable')
final_data = final_data.rename(columns = {'Rank' : 'Confounded feature rank'})
final_data = final_data.drop(['Variable_x','Variable_y'], axis = 1)

# Reorder columns and sort rows
final_data = final_data.loc[:,['Feature', 'Feature rank', 'Top confounded feature', 'Confounded feature rank', 'odds_ratio']]
final_data = final_data.sort_values(by = ['Feature rank', 'Confounded feature rank']) 

# Print out as csv
final_data.to_csv("Downloads/Solve/confounding_results/egr_cooler_results/" + str(datetime.date.today()).replace("-","") + "_catxnum_logistic_confounding.csv")
