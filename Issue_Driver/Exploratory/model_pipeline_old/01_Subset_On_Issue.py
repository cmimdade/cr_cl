#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 25 14:09:37 2018

@author: sanjay
"""

import pandas as pd
import numpy as np
import sys
import os
from tqdm import tqdm
from sklearn.preprocessing import scale

pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

## Hyperparameters ##
issue_name = 'EGR_Cooler'
engine_type = ['X3 2017']
esn_list = 'egr_cooler_esns.csv'
path_to_results = '/Users/sanjay/Desktop/Solve_Results'

## Create folder structure ##

#Remove Folder if exists
os.system("rm -r {}/{}".format(path_to_results, issue_name))

#Create structure
os.system("mkdir {}/{}".format(path_to_results, issue_name))
os.system("mkdir {}/{}/Figs".format(path_to_results, issue_name))
os.system("mkdir {}/{}/Figs/Usage".format(path_to_results, issue_name))
os.system("mkdir {}/{}/Figs/Design".format(path_to_results, issue_name))
os.system("mkdir {}/{}/Figs/Manufacturing".format(path_to_results, issue_name))
os.system("mkdir {}/{}/Figs/MAB".format(path_to_results, issue_name))
os.system("mkdir {}/{}/Figs/Confounding".format(path_to_results, issue_name))
os.system("mkdir {}/{}/Figs/Confounding/num_x_num".format(path_to_results, issue_name))
os.system("mkdir {}/{}/Figs/Confounding/cat_x_cat".format(path_to_results, issue_name))
os.system("mkdir {}/{}/Figs/Confounding/num_x_cat".format(path_to_results, issue_name))




os.system("mkdir {}/{}/Data".format(path_to_results, issue_name))
os.system("mkdir {}/{}/Results".format(path_to_results, issue_name))

## Load Data and Set Labels ##
df_one_hot = pd.read_csv("/Users/sanjay/Desktop/Data/fat_table_with_one_hot_raw.csv")
df_no_one_hot = pd.read_csv("/Users/sanjay/Desktop/Data/fat_table_no_one_hot_raw.csv")

df_one_hot = df_one_hot[(df_one_hot['INS_TI_ENGINE_DISTANCE_MILES'] > 1500) | \
                        (np.isnan(df_one_hot['INS_TI_ENGINE_DISTANCE_MILES']))]
df_no_one_hot = df_no_one_hot[(df_no_one_hot['INS_TI_ENGINE_DISTANCE_MILES'] > 1500) | \
                              (np.isnan(df_no_one_hot['INS_TI_ENGINE_DISTANCE_MILES']))]


def subsetAndConcat(df):

    #Load Issue ESN list
    issue_df = pd.read_csv("/Users/sanjay/Desktop/{}".format(esn_list), names = ['ESN'])
    issue_df = issue_df.merge(df, on = 'ESN', how = 'inner').copy()
    issue_df['Label'] = 1
    
    #Negative set ESNs
    engine_type_esns = df_no_one_hot[df_no_one_hot["REL_ANALYSIS_RATE_CAT"].isin(\
                                         engine_type)]['ESN'].unique()
    #Get unique indices
    esn_idxs = ~np.isin(engine_type_esns, issue_df.ESN.unique())
    
    #Subset on ESNs not in issue list
    engine_type_esns = engine_type_esns[esn_idxs]
    
    #Get non-issue lst
    df_not_issue = df.query("ESN in @engine_type_esns")
    df_not_issue['Label'] = 0
    
    #Concatenate!
    df_all = pd.concat([issue_df, df_not_issue]).reset_index(drop = True)
    
    return df_all

df_all = subsetAndConcat(df_one_hot)
df_not_normalized = subsetAndConcat(df_no_one_hot)



def cleanAndStandardize(df, standardize = True):
    
    #Remove spurious values
    for col in tqdm(numeric_cols):
        if df[col].dtype not in ['float64', 'float32']:
            df[col] = df[col].str.replace(',','.')
            df[col] = df[col].astype(float)
        if 'PERCENT' in col:
            df[col][df[col] < 0] = float('NaN')
            df[col][df[col] > 100] = float('NaN')
        else:
            df[col][df[col] < -10**6] = float('NaN')
            df[col][df[col] > 10**6] = float('NaN')
            
    if standardize == True:
    
        #Scale numeric features
        for col in tqdm(numeric_cols):
            finite_vals = df_all[col].dropna()
            if len(finite_vals) > 0:
                scaled_vals = scale(finite_vals)
                df.iloc[finite_vals.index.values, np.where(df.columns.values == col)[0][0]] = scaled_vals
            
            #Replace NaN with 0's (Mean)
            df[col] = df[col].replace(np.nan, 0)
            
    return df

df_all_not_normalized = cleanAndStandardize(df_all, standardize = False)
df_not_normalized = cleanAndStandardize(df_not_normalized, standardize = False)

df_not_normalized.to_csv("{}/{}/Data/{}_raw.csv".format(path_to_results, issue_name, issue_name), index = False)
df_all_not_normalized.to_csv("{}/{}/Data/{}_one_hot_raw.csv".format(path_to_results, issue_name, issue_name), index = False)


df_all_normalized = cleanAndStandardize(df_all, standardize = True)
df_all_normalized.to_csv("{}/{}/Data/{}_one_hot_norm.csv".format(path_to_results, issue_name, issue_name), index = False)
