#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 15:08:18 2018

@author: pr891
"""
#%%
########################################################################################################
#intial setting stuff that does not need to be run all the time 
import os
#print(os.getcwd())
os.chdir("/Users/sanjay/Desktop/ds/Solve/Model_Pipeline")
import importlib
importlib.reload(idlib)
#importlib.reload(isop)
#import copy

#%%
#Librairies
import ID_lib as idlib
#import Turbo_Rotor_config as isop
import Navistar_NOx_Sensor_config as isop
import warnings
import pandas as pd
warnings.simplefilter('ignore')
########################################################################################################

#%%
### define some columns where to take data from 
#esn_col, cat_cols, numeric_cols, vpcr_cols, mes_cols, good_cols, cols_to_split, extra_cols, fault_code_cols, duration_cols, ins_fs_cat_cols, numeric_fs_cols, uv_col_order, uv_cols = idlib.var_def(isop.path_to_col)

#%%
### load and clean data
#df_no_one_hot_raw, df_one_hot_raw, df_extra, oepl_desc_df = idlib.load_and_clean_data_main(path_online_tracker = isop.path_online_tracker, 
#                                                                          path_fault_snap = isop.path_fault_snap, 
#                                                                          path_oepl_desc = isop.path_oepl_desc, 
#                                                                          path_engine_options = isop.path_engine_options, 
#                                                                          fault_code_col_name = isop.fault_code_col_name, 
#                                                                          path_fault_look = isop.path_fault_look,
#                                                                          path_base = isop.path_base, 
#                                                                          duration_cols = duration_cols, 
#                                                                          vpcr_cols = vpcr_cols,
#                                                                          cat_cols = cat_cols,
#                                                                          numeric_cols = numeric_cols,
#                                                                          uv_cols = uv_cols,
#                                                                          extra_cols = extra_cols,
#                                                                          write_results = True)

#%%
### define some columns where to take data from 
esn_col, cat_cols,numeric_cols, vpcr_cols, mes_cols, good_cols, cols_to_split, extra_cols, fault_code_cols, duration_cols, ins_fs_cat_cols, numeric_fs_cols, uv_col_order, uv_cols = idlib.var_def(isop.path_to_col)

#load pre written data 
df_one_hot_raw = idlib.read_and_filter(file_name = isop.file_one_hot_raw,
                                         mileage_min = isop.mileage_min,
                                         mileage_col = isop.mileage_col)

df_no_one_hot_raw = idlib.read_and_filter(file_name = isop.file_no_one_hot_raw,
                                         mileage_min = isop.mileage_min,
                                         mileage_col = isop.mileage_col)

#%%
### 01_Subset_On_Issue

df_no_one_hot_raw, df_one_hot_raw, df_one_hot_norm = idlib.subset_on_issue_main(issue_name = isop.issue_name,
                                                                                path_to_results = isop.path_to_results,
                                                                                mileage_min = isop.mileage_min,
                                                                                mileage_col = isop.mileage_col,
                                                                                incident_path = isop.incident_path, 
                                                                                incident_merge_col = isop.incident_merge_col,
                                                                                base_merge_col = isop.base_merge_col,
                                                                                esn_list = isop.esn_list,
                                                                                esn_file_type = isop.esn_file_type,
                                                                                esn_sheet = isop.esn_sheet,
                                                                                engine_type = isop.engine_type,
                                                                                global_lower = isop.global_lower,
                                                                                global_upper = isop.global_upper, 
                                                                                numeric_cols = numeric_cols,
                                                                                file_one_hot_raw = isop.file_one_hot_raw, 
                                                                                file_no_one_hot_raw = isop.file_no_one_hot_raw,
                                                                                df_one_hot_raw = df_one_hot_raw,
                                                                                df_no_one_hot_raw = df_no_one_hot_raw,
                                                                                write_results = True)


#%%
### 02_First_Pass_Selection
df_one_hot_norm_reduced = idlib.first_pass_selection_main(issue_name = isop.issue_name,
                                                          path_to_results = isop.path_to_results,
                                                          no_coverage_perc_thresh = isop.no_coverage_perc_thresh,
                                                          coef_of_var_thresh = isop.coef_of_var_thresh, 
                                                          min_max_thresh = isop.min_max_thresh,
                                                          cat_cols = cat_cols,
                                                          numeric_cols = numeric_cols,
                                                          uv_cols = uv_cols,
                                                          df_one_hot_norm = df_one_hot_norm,
                                                          df_one_hot_raw = df_one_hot_raw)

#%%
### 03_Univariate_Analysis
new_views, df_final_list, df_universal_view, df_oepl_sub, df_vpcr, df_fc = idlib.univariate_analysis_main(issue_name = isop.issue_name, 
                                                                                                          path_to_results = isop.path_to_results, 
                                                                                                          path_engine_options = isop.path_engine_options,
                                                                                                          path_fault_look = isop.path_fault_look, 
                                                                                                          path_extra_pickle = isop.path_extra_pickle, 
                                                                                                          num_cores = isop.num_cores,
                                                                                                          cat_cols = cat_cols, 
                                                                                                          numeric_cols = numeric_cols, 
                                                                                                          uv_cols = uv_cols,
                                                                                                          path_to_build_pop = isop.path_to_build_pop,
                                                                                                          uv_col_order = uv_col_order, 
                                                                                                          oepl_desc_df = pd.DataFrame(),
                                                                                                          df_one_hot_norm_reduced = df_one_hot_norm_reduced, 
                                                                                                          df_one_hot_raw = df_one_hot_raw, 
                                                                                                          df_no_one_hot_raw = df_no_one_hot_raw,
                                                                                                          path_oepl_desc = isop.path_oepl_desc)

#%%
### 04_Multivariate_Analysis
#df_not_normalized is wsame as file end _one_hot_raw
#df is the same as df_all after categories have been reduced
df_master, all_uni_output, columns_to_remove = idlib.multivariate_analysis_main(issue_name = isop.issue_name, 
                                                                                path_to_results = isop.path_to_results, 
                                                                                path_processed_raw = isop.path_processed_raw, 
                                                                                mileage_min = isop.mileage_min, 
                                                                                path_processed_norm = isop.path_processed_norm, 
                                                                                path_columns_to_remove = isop.path_columns_to_remove,
                                                                                path_output = isop.path_output, 
                                                                                numeric_cols = numeric_cols,
                                                                                columns_to_remove = pd.DataFrame(), 
                                                                                df_one_hot_raw = df_one_hot_raw,
                                                                                df_one_hot_norm_reduced = df_one_hot_norm_reduced, 
                                                                                test_only = False)

#%%
### 05_Confounding_Analysis
idlib.confounding_analysis_main(issue_name = isop.issue_name,
                                path_to_results = isop.path_to_results,
                                cat_cols = cat_cols,
                                numeric_cols = numeric_cols,
                                uv_cols = uv_cols,
                                num_cores = isop.num_cores,
                                path_fault_look = isop.path_fault_look,
                                path_oepl_desc = isop.path_oepl_desc,
                                path_engine_options = isop.path_engine_options,
                                df_fc = pd.DataFrame(),
                                df_oepl_sub = pd.DataFrame(),
                                oepl_desc_df = pd.DataFrame(),
                                uni_output = pd.DataFrame(),
                                df_one_hot_norm_reduced = pd.DataFrame(),
                                df_no_one_hot_raw = pd.DataFrame(),
                                df_one_hot_raw = pd.DataFrame(),
                                plot_only = False)

#%%
### 06_Residual_Analysis
#df_one_hot_raw = pd.merge(df_master[['ESN', 'EARLIEST_INDICATION_DATE', 'ID']], df_one_hot_raw, how = 'right', on = ['ESN', 'EARLIEST_INDICATION_DATE'])
#df_one_hot_norm_reduced = pd.merge(df_master[['ESN', 'EARLIEST_INDICATION_DATE', 'ID']], df_one_hot_norm_reduced, how = 'right', on = ['ESN', 'EARLIEST_INDICATION_DATE'])
#df_master = df_master.rename(columns = {'id': 'ID'})

res_df_one, all_avg_stats = idlib.residual_analysis_main(issue_name = isop.issue_name,
                                                         path_to_results = isop.path_to_results, 
                                                         path_processed_raw = isop.path_processed_raw,
                                                         path_processed_norm = isop.path_processed_norm, 
                                                         mileage_min = isop.mileage_min, 
                                                         path_output = isop.path_output, 
                                                         path_univariate_contributons = isop.path_univariate_contributons, 
                                                         numeric_cols = numeric_cols,
                                                         columns_to_remove = pd.DataFrame(), 
                                                         df_one_hot_raw = pd.DataFrame(), 
                                                         df_one_hot_norm_reduced = pd.DataFrame(), 
                                                         test_run = False)

#%%