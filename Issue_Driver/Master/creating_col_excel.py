#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 20 11:55:12 2018

@author: pr891
"""
#%%
import pandas as pd
import numpy as np
import ID_lib as idlib
import Navistar_NOx_Sensor_config as isop

esn_col, cat_cols,numeric_cols, vpcr_cols, mes_cols, good_cols, cols_to_split, extra_cols, fault_code_cols, duration_cols, ins_fs_cat_cols, numeric_fs_cols, uv_col_order, uv_cols = idlib.var_def()
a = np.concatenate([esn_col, cat_cols, numeric_cols, vpcr_cols, mes_cols, good_cols, cols_to_split, extra_cols, fault_code_cols, duration_cols, ins_fs_cat_cols, numeric_fs_cols, uv_col_order, uv_cols])
a = np.unique(a)
b = pd.DataFrame(a)
b.columns = ['all_col']
b['esn_col'] = b['all_col'].isin(esn_col)
b['cat_cols'] = b['all_col'].isin(cat_cols)
b['numeric_cols'] = b['all_col'].isin(numeric_cols)
b['vpcr_cols'] = b['all_col'].isin(vpcr_cols)
b['mes_cols'] = b['all_col'].isin(mes_cols)
b['good_cols'] = b['all_col'].isin(good_cols)
b['cols_to_split'] = b['all_col'].isin(cols_to_split)
b['extra_cols'] = b['all_col'].isin(extra_cols)
b['fault_code_cols'] = b['all_col'].isin(fault_code_cols)
b['duration_cols'] = b['all_col'].isin(duration_cols)
b['ins_fs_cat_cols'] = b['all_col'].isin(ins_fs_cat_cols)
b['numeric_fs_cols'] = b['all_col'].isin(numeric_fs_cols)
b['uv_col_order'] = b['all_col'].isin(uv_col_order)
b['uv_cols'] = b['all_col'].isin(uv_cols)

b.to_csv('ID_columns')

#create code
#%%
c = ['esn_col', 'cat_cols', 'numeric_cols', 'vpcr_cols', 'mes_cols', 'good_cols', 'cols_to_split', 'extra_cols', 'fault_code_cols', 'duration_cols', 'ins_fs_cat_cols', 'numeric_fs_cols', 'uv_col_order', 'uv_cols']
for d in c: 
    print('b[\'' + d + '\'] = b[\'all_col\'].isin(' + d + ')')
    
for d in c: 
    print(d +' = col_def[\'all_col\'][col_def[\''+ d +'\']].unique().tolist()')
    
for d in c:
    print('test_match(col_new = ' + d + ', col_old = '+ d + '1)')
    
#%% test resutls to make sure it matches previsou code

### define some columns where to take data from 
esn_col1, cat_cols1, numeric_cols1, vpcr_cols1, mes_cols1, good_cols1, cols_to_split1, extra_cols1, fault_code_cols1, duration_cols1, ins_fs_cat_cols1, numeric_fs_cols1, uv_col_order1, uv_cols1 = idlib.var_def()
esn_col, cat_cols, numeric_cols, vpcr_cols, mes_cols, good_cols, cols_to_split, extra_cols, fault_code_cols, duration_cols, ins_fs_cat_cols, numeric_fs_cols, uv_col_order, uv_cols = idlib.var_def_new(isop.path_to_col)

def test_match(col_new, 
               col_old):
    temp = pd.DataFrame(col_new).isin(col_old)
    temp.columns = ['a']
    z = temp['a']
    return(all(z))

    
test_match(col_new = esn_col, col_old = esn_col1)
test_match(col_new = cat_cols, col_old = cat_cols1)
test_match(col_new = numeric_cols, col_old = numeric_cols1)
test_match(col_new = vpcr_cols, col_old = vpcr_cols1)
test_match(col_new = mes_cols, col_old = mes_cols1)
test_match(col_new = good_cols, col_old = good_cols1)
test_match(col_new = cols_to_split, col_old = cols_to_split1)
test_match(col_new = extra_cols, col_old = extra_cols1)
test_match(col_new = fault_code_cols, col_old = fault_code_cols1)
test_match(col_new = duration_cols, col_old = duration_cols1)
test_match(col_new = ins_fs_cat_cols, col_old = ins_fs_cat_cols1)
test_match(col_new = numeric_fs_cols, col_old = numeric_fs_cols1)
test_match(col_new = uv_col_order, col_old = uv_col_order1)
test_match(col_new = uv_cols, col_old = uv_cols1)
