#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 20:23:29 2018

@author: pr891
"""
import multiprocessing
from sys import platform

esn_list = 'IMV_ESNs.xlsx'
engine_type = ['X3 2017']
#esn_list = 'Fuel_Line_ESN_List.xlsx'
#engine_type = ['X1 2017', 'X3 2017', 'ISX1 2013', 'ISX2 2013', 'ISX3 2013']

if platform == 'darwin':
    #path to write all files and where to read from
    path_base = '/Users/sanjay/Desktop/Data/'
    incident_path = "/Users/sanjay/Desktop/Data/{}"
    path_to_results = '/Users/sanjay/Desktop/Solve_Results/'
elif platform == 'linux':
    path_base = '/u02/Data/'
    incident_path = '/u02/Data/{}'
    path_to_results = '/u02/Solve_Results/'
elif platform == 'win32':
    path_base = '/Users/pr916/Documents/X15/Data/'
    incident_path = '/Users/pr916/Documents/X15/Data/{}'
    path_to_results = '/Users/pr916/Documents/X15/Solve_Results/'

## Hyperparameters ##
issue_name = 'sample_name'
esn_file_type = "excel" #leave pblank if it is a csv file or other non-excel format
esn_sheet = "sample sheet name" 

max_num_cores = 8
num_cores = round(min([multiprocessing.cpu_count()-1, max_num_cores]))

#incident_merge_col = ['ESN', 'Earliest Indication Date']
#base_merge_col = ['ESN', 'EARLIEST_INDICATION_DATE']
incident_merge_col = ['ESN']
base_merge_col = ['ESN']

#data cleaning thresholds
no_coverage_perc_thresh = 0.05
coef_of_var_thresh = 0.03
min_max_thresh = 0.0002

#min and max settings
mileage_min = 1500
mileage_col = 'INS_TI_ENGINE_DISTANCE_MILES'

#set global data bounds
global_lower = -10**6
global_upper = 10**6

#file paths for issues and non_issue
file_one_hot_raw = path_base + "fat_table_one_hot_raw.csv"
file_no_one_hot_raw = path_base + "fat_table_no_one_hot_raw.csv"

#paths for processed data
path_down_base = "/Users/pr891/Desktop/Downloads/Solve/OneDrive_1_3-11-2018/"
path_processed_norm = path_down_base + "EGR_Flow_one_hot_norm_reduced.csv"
path_processed_raw = path_down_base + "EGR_Flow_one_hot_raw.csv"
path_columns_to_remove = path_down_base + "columns_to_remove.csv"
path_output = path_down_base + "variable_results_egr_flow_040218.xlsx"
path_univariate_contributons = path_to_results + "EGR_Cooler/Data/EGR_Cooler_univariate_contributons.csv"


#path to .parquet
#path_online_tracker = path_base + 'INS_FAULT_SNAPSHOT_MDA_FEATURES/'
path_online_tracker = path_base + 'QUALITY_FEATURES_PARQUET_PERSISTED_05092018'

#path to online incident tracker
#@Shantanu - will need to be removed after new master table with OEPL
path_engine_options = path_base + 'RLD_ENGINE_OPTION_ASSEMBLY_x15'

#path to FAULT SNAPSHOT 
#path_fault_snap = path_base + 'INS_FAULT_SNAPSHOT_MDA_FEATURES/'

#path option Description File
#@Shantanu - will need to be removed after new master table with OEPL
path_oepl_desc = path_base + 'ISX_Options.csv'

#path to fault code look up
path_fault_look = path_base + 'fault_code_lookup_solve.csv'

#path to extra data pickle file
path_extra_pickle = path_base + "universal_view_cols.p"

#define col name for fault codes
fault_code_col_name = 'GREEN_WRENCH_FAULT_CODE_LIST'

#path to full build population
path_to_build_pop = path_base + 'full_build_population.csv'

#path to column definitions
path_to_col = path_base + 'ID_columns.xlsx'

#path to column priority
path_column_priority = path_base + 'ID_columns_EVG_w_OEPL.csv'

#path to alias sheet
path_aliases = path_base + 'Feature_Aliases_04102018.xlsx'

