#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 15:08:56 2018

@author: pr891
"""

#%%  import Librairies ####
import os
import pickle
import random
import math
from tqdm import tqdm
import sys
import multiprocessing
from joblib import Parallel, delayed
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
import pyarrow.parquet as pq
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import scale
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import r2_score
from sklearn.metrics import make_scorer
from sklearn.model_selection import cross_val_score
import statsmodels.discrete.discrete_model as sm
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from importlib import import_module
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
#from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_auc_score
from scipy.optimize import fmin
from scipy.stats import *
from scipy.stats import anderson_ksamp, ks_2samp, ranksums, chi2_contingency, fisher_exact, pearsonr, spearmanr, f_oneway, kruskal
from statsmodels.stats.outliers_influence import variance_inflation_factor
from statsmodels.stats.contingency_tables import mcnemar
from treeinterpreter import treeinterpreter as ti, utils
import seaborn as sns
import datetime
from openpyxl import load_workbook
from random import sample
from itertools import compress

#%% ##########################

#%% Set styles for plots####
plt.style.use('bmh')
plt.rcParams['figure.figsize'] =(8, 6)
pd.set_option('display.max_columns', 50)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None
#%% ##########################

#%% functions to load and merge data####
def load_and_clean_data_main(path_online_tracker, #path to main table with all incidents set in cofiguration file
                             path_oepl_desc, #path to opel descriptions set in cofiguration file
                             path_engine_options, #path to file with engine options set in cofiguration file
                             fault_code_col_name, #column that contains fualt code - typically green wrench
                             path_fault_look, #path to fault code look up file set in cofiguration file
                             path_base, #base path set in config file
                             duration_cols, #list of duration columns from MES (manufactuing)
                             vpcr_cols, #list of vpcr (design changes) columns
                             cat_cols, #list of categorical columns
                             numeric_cols, #list of numeric columns
                             uv_cols, #list of columns for universal views
                             extra_cols, #list of extra columns for plots 
                             write_results = True #sets whether or not results will be written out to a file 
                             ):
    
    ## load main data and merge with fault snapshot data 
    #load main table with all incidents
    online_tracker = load_online_tracker(path_online_tracker = path_online_tracker)

    ## Prepare engine options for merge with 
    #load oepl descriptions
    oepl_desc_df = load_options_data(path_oepl_desc = path_oepl_desc)
    
    #load engine options
    df = load_engine_options(path_engine_options = path_engine_options)
    
    #merge engine options with their descriptions
    df_oepl_option = merge_tracker_oepl_options(df = df, 
                                               oepl_desc_df = oepl_desc_df)
    
    #pivot the engine options data to be merged with the main data frame 
    df_pivot = pivot_oepl(df_oepl_option = df_oepl_option)
    
    
    ##merge engine options back into the main data
    fat_table = merge_oepl(fat_table = online_tracker, 
                           df_pivot = df_pivot)
    
    #update some of the columns in the merged main table 
    fat_table = enhance_fat_table(fat_table = fat_table, 
                                  duration_cols = duration_cols, 
                                  vpcr_cols = vpcr_cols)
    
    # get the oepl columns based on the oepl data frame
    oepl_cols = get_column_types(df_oepl = df_oepl_option, 
                                 cat_cols = cat_cols,
                                 numeric_cols = numeric_cols, 
                                 uv_cols = uv_cols, 
                                 get_oepl = True, 
                                 oepl_only = True)
    
    #convert the vpcr columns to binary in the main merged table 
    fat_table = cols_to_binary(col_list = vpcr_cols, 
                               df = fat_table)
    
    ## create the one hot encoded data
    #prepare a dataframe of unique fault codes to be used in the one hot encoding    
    unique_fault_codes = unique_fault(fault_code_col_name = fault_code_col_name, 
                                      fat_table = fat_table)
    
    #perform the one hot encoding 
    fc_df = fault_code_hot(fault_code_col_name = fault_code_col_name, 
                           fat_table = fat_table, 
                           unique_fault_codes = unique_fault_codes, 
                           path_fault_look = path_fault_look)
    
    #this is taken care of later
    #fat_table = clean_numeric(numeric_cols, fat_table)
    
    #create a teble with the unique fields to be used for merging later
    df_esn = fat_table[['ESN', 'EARLIEST_INDICATION_DATE', 'REL_FAILURE_DATE']]
    
    #stelect categorical features from main merged table
    df_cat = fat_table[cat_cols]
    
    #stelect numerical features from main merged table
    df_num = fat_table[numeric_cols]
    
    #stelect vpcr features from main merged table
    df_vpcr = fat_table[vpcr_cols]
    
    #stelect oepl features from main merged table
    df_oepl = fat_table[oepl_cols].fillna(0)
    
    #stelect issue number feature from main merged table
    df_issue = fat_table[['INCDT_ISSUE_NUMBER']]
    
    #make the one hot encoded dataframe by concatinating all of the features above
    df_no_one_hot_raw = pd.concat([df_esn, df_cat, df_num, df_oepl, df_issue, df_vpcr, fc_df], axis = 1)
    
    #create one hot columns out of categorical features - need to check with Sanhay
    df_cat = cat_features(cat_cols, fat_table)
    
    # Split Cols on important vals #
    df_one_hot_raw = pd.concat([df_esn, df_cat, df_num, df_oepl, df_issue, df_vpcr, fc_df], axis = 1)
    
    #Get extra cols for universal view#
    df_extra = fat_table[extra_cols] 
    
    #Save object (since that was a lot of work!)
    if write_results:

        #write data that is one hot encoded
        write_results_fn(df = df_one_hot_raw,
                         path_to_results = path_base,
                         issue_name = "",
                         data_folder = "",
                         file_end = "fat_table_one_hot_raw")

        #write data that is not one hot encoded
        write_results_fn(df = df_no_one_hot_raw,
                         path_to_results = path_base,
                         issue_name = "",
                         data_folder = "",
                         file_end = "fat_table_no_one_hot_raw")
        
        #write a pickle file with the extra columns
        write_results_fn(df = df_extra,
                         path_to_results = path_base,
                         issue_name = "",
                         data_folder = "",
                         file_end = "universal_view_cols",
                         save_type = "pickle")
        
    return(df_no_one_hot_raw, df_one_hot_raw, df_extra, oepl_desc_df)

#load the main table with all of the columns
def load_online_tracker(path_online_tracker #file path to the online incident tracker
                        ):
    #read the parquet file with all of the data fields
    online_tracker = pq.read_table(path_online_tracker).to_pandas()
    
    #Convert the online esn column to an integer - @Sanjay - why do we do this?
    online_tracker['ESN'] = online_tracker['ESN'].astype(int)
    
    return(online_tracker)
    
#load column priority document
def load_column_priority(path_column_priority):
    
    #Read, subset, and rename columns
    df_column_priority = pd.read_csv(path_column_priority)
    df_column_priority = df_column_priority[['all_col', 'System', 'Sub System', 'SME Significance']]
    df_column_priority = df_column_priority.rename(columns = {'all_col': 'Base Feature'})
    
    return df_column_priority
    
    

#load the fualt snapshot data from the parquet file    
def load_fault_snapshot(path_fault_snap #file path to fualt snapshot data
                        ):
    #read the parquet file with all of the data fields
    fault_snapshot = pq.read_table(path_fault_snap).to_pandas()
    
    #Convert the online esn column to an integer - @Sanjay - why do we do this?
    fault_snapshot.ESN = fault_snapshot.ESN.astype(int)
    
    #delete the GREEN_WRENCH_FAULT_CODE_LIST feature - @Sanjay why do we do this here? seems a bit random and we should try to be consistent in our load
    del fault_snapshot['GREEN_WRENCH_FAULT_CODE_LIST']
    
    return(fault_snapshot)
    
#read in the engine options and format for one hot encoding @Sanjay - seems like we do this one hot encoding in a bunch of different places, I might combine
def load_options_data(path_oepl_desc #file path to options data
                      ):
    
    #load in oepl data
    oepl_desc_df = pd.read_csv(path_oepl_desc)
    
    #convert for one hot encoding
    oepl_desc_df = one_hot_fn(df = oepl_desc_df,
                              one_hot_col = 'OPT_NBR',
                              column = 'OPT_ITEM_DESC',
                              col_prefix = 'OEPL_')
    
    return(oepl_desc_df)

#generalized function for one hot encoding
def one_hot_fn(df, #data frame for one hot encoding
               column, #column to be the one hot encoded column
               one_hot_col, #column that is the value for the one hot encoding
               col_prefix, #prefix to add to the variables that will be column names
               col_suffix = "_NEW"
               ):
    #make new column name
    col_new = column + col_suffix
    
    #replace white space with _
    df[col_new] = df[column].str.replace(" ", "_")
    
    #replace comma with _
    df.loc[:,col_new] = df[col_new].str.replace(",", "_")
    
    #replace / with _
    df.loc[:,col_new] = df[col_new].str.replace("/", "_")
    
    #replace __ with _
    df.loc[:,col_new] = df[col_new].str.replace("__", "_")
    
    #replace ___ with _
    df.loc[:,col_new] = df[col_new].str.replace("___", "_")
    
    #replace __ with _
    df.loc[:,col_new] = df[col_new].str.replace("__", "_")
    
    #append name with the prefix for the one hot encoded features
    if not col_prefix == "":
        df[col_new] = [col_prefix + df[col_new][x] for x in range(df.shape[0])]
    
    #prepare the one hot encoding column names
    df[col_new] = [df[col_new][x] + ':' + str(df[one_hot_col][x]) for x in range(df.shape[0])]
    
    return(df)

#load the engine options data from the parquet file   
def load_engine_options(path_engine_options #path to online incident tracker
                        ):
    #read the parquet file with all of the data fields
    df = pq.read_table(path_engine_options).to_pandas()
    
    #Convert the online esn column to an integer - @Sanjay - why do we do this? some serial numbers might be mixed types, wouldn't strings be safer?
    df['engine_serial_num'] = df['engine_serial_num'].astype(int)
    
    #remane the columns to option number
    df = df.rename(columns = {'option_assembly_num': 'OPT_NBR'})
    
    return(df)
 
#merge the oepl data  into the tracker
def merge_tracker_oepl_options(df, #path to online incident tracker
                              oepl_desc_df #oepl descriotion dataframe
                              ):
    #merge the data
    df = df.merge(oepl_desc_df, on = 'OPT_NBR', how = 'left')
    
    #set up column for one hot encoding
    df['option_count'] = 1
    
    #get data types @Sanjay I am pretty sure that there is a pandas function to do this
    opt_val_type = df['shop_order_num'].apply(lambda x: type(x))
    
    #get the indecies of the string data types
    opt_str_idx = opt_val_type[opt_val_type == str].index
    
    #pull out columns with strings @Sanjay - i think that this can be done much simpler, lets review
    df = df.loc[opt_str_idx]
    
    return(df)

#pivot the oepl data
def pivot_oepl(df_oepl_option #data frame with online tracker after options merged
               ):
    #piviot the data set
    df_pivot = df_oepl_option.pivot_table(index = 'engine_serial_num', columns = 'OPT_ITEM_DESC_NEW', \
                              values = 'option_count', aggfunc='mean').fillna(0).reset_index()
    #name the pivoted columns
    df_pivot = df_pivot.rename(columns = {'engine_serial_num': 'ESN'})
    return(df_pivot)
 
# Merge with fault snapshot data
def merge_fault(online_tracker, #online tracker master data
                fault_snapshot #fault snapshot table
                ):
    # Merge with fault snapshot data
    fat_table = online_tracker.merge(fault_snapshot, how = 'left', on = ['ESN', 'EARLIEST_INDICATION_DATE'])
    return(fat_table)

# Merge with OEPL Dataset that has been pivoted  
def merge_oepl(fat_table, #fat table merged with fault snapshot data
                df_pivot #pivoted data merged with oepl
                ):
    # Merge with OEPL Dataset that has been pivoted
    fat_table = fat_table.merge(df_pivot, on = 'ESN', how = 'left')
    return(fat_table)
    
#enhance some of the columns in the fat table and create calculated fields @Sanjay does this need to move to data engineering?
def enhance_fat_table(fat_table, #fat table merged with fault snapshot data
                      duration_cols,
                      vpcr_cols
                      ):
    # Add build month to table #
    fat_table['REL_BUILD_MONTH'] = ['0' + str(fat_table['REL_BUILD_MONTH'][x]) \
                                     if len(str(fat_table['REL_BUILD_MONTH'][x])) \
                                     == 1 else str(fat_table['REL_BUILD_MONTH'][x]) \
                                     for x in range(fat_table.shape[0])]
    
    #create a column that is unique to both year and month for later build month analysis
    fat_table['REL_BUILD_YEAR_MONTH'] = fat_table['REL_BUILD_YEAR'].astype(str) + \
                            '_' + fat_table['REL_BUILD_MONTH'].astype(str)
                            
    ##Add computed miles table
    #logic for which column to select the miles from
    miles_temp = [fat_table['INS_TI_ENGINE_DISTANCE_MILES'][x] if \
                  ~np.isnan(fat_table['INS_TI_ENGINE_DISTANCE_MILES'][x]) \
                  else fat_table['REL_CMP_ENGINE_MILES_LIST'][x] \
                  for x in range(fat_table.shape[0])]
    
    #selecrt the miles if number makes sense
    miles_temp = [miles_temp[x][len(miles_temp[x])-1] if type(miles_temp[x]) \
                  is np.ndarray and len(miles_temp[x]) > 0 else miles_temp[x] for x in range(len(miles_temp))]
    
    for i in range(len(miles_temp)):
        if type(miles_temp[i]) is np.ndarray:
            miles_temp[i] = float('NaN')
        
    #create column in the fat table where the miles will be held
    fat_table['INS_TI_ENGINE_DISTANCE_MILES'] = miles_temp
                
    # Get idle time percentage
    fat_table['INS_TI_IDLE_TIME_PERCENTAGE'] = \
        100*fat_table['INS_TI_IDLE_TIME_HOURS']/ \
        fat_table['INS_TI_ECM_TIME_KEY_ON_TIME_HOURS']
        
    # Clean up to remove the forward slash from column names @Sanjay - should we just do this for all column names?
    fat_table = fat_table.rename(columns = {'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3/S': \
                                            'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_FT3S', \
                                            'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3/S': \
                                            'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3S'})
    
    
    ## All MES Duration columns should be negative ## - @Sanjay - need to take this out after the DE update from Manish
    #for col in tqdm(duration_cols):
    #    fat_table[col] = fat_table[col]*-1
    
    #hot encode all of the vpcr columns
    for col in tqdm(vpcr_cols):
        empty_col = np.zeros(fat_table.shape[0])
        empty_col[fat_table[col].dropna().index.values] = 1
        fat_table[col] = empty_col
        
    return(fat_table)

#%% ##########################
    
#%%  data conversion functions ####
    
#convert column values to binary
def cols_to_binary(col_list, #column names for features to convert 
                   df #data frame with features to be converted
                   ):
    #Turn Features into Binary#
    for col in tqdm(col_list):
        empty_col = np.zeros(df.shape[0])
        empty_col[df[col].dropna().index.values] = 1
        df[col] = empty_col
    return(df)

#get list of unique fault codes from the data   
def unique_fault(fault_code_col_name, #column name in fat table where the fault code list is
                 fat_table #enhanced fat table
                 ):
    #Get Unique Fault Codes#
    unique_fault_codes = set()
    for line in fat_table[fault_code_col_name]:
        if line is not None:
            for fc in line:
                unique_fault_codes.add(fc)
    return(unique_fault_codes)

#one hot enfoce the fault codes
def fault_code_hot(fault_code_col_name, #column name in fat table where the fault code list is
                 fat_table, #enhanced fat table
                 unique_fault_codes, # a uniqe set of fault codes defined in unique_fault function
                 path_fault_look, #path to fault code look up
                 fault_code_col_name2 = 'NON_GREEN_WRENCH_FAULT_CODE_LIST'
                 ):
    #Create empty fault code DF
    cmp_fault_codes = [None]*fat_table.shape[0]

    #perform the one hot encoding
    for i in tqdm(range(fat_table.shape[0])):
        if fat_table[fault_code_col_name][i] is None and fat_table[fault_code_col_name2][i] is None:
            cmp_fault_codes[i] = np.array([])
        elif fat_table[fault_code_col_name][i] is None and fat_table[fault_code_col_name2][i] is not None:
            cmp_fault_codes[i] = np.unique(fat_table[fault_code_col_name2][i])
        elif fat_table[fault_code_col_name][i] is not None and fat_table[fault_code_col_name2][i] is None:
            cmp_fault_codes[i] = np.unique(fat_table[fault_code_col_name][i])
        elif fat_table[fault_code_col_name][i] is not None and fat_table[fault_code_col_name2][i] is not None:
            cmp_fault_codes[i] = np.unique(np.concatenate((fat_table[fault_code_col_name][i], fat_table[fault_code_col_name2][i])))
    
    #Get Unique Fault Codes#
    unique_fault_codes = set()
    for line in cmp_fault_codes:
        if line is not None:
            for fc in line:
                unique_fault_codes.add(fc)
               
    #Create empty fault code DF
    fc_df = np.zeros(len(unique_fault_codes)*fat_table.shape[0]).\
            reshape(fat_table.shape[0], len(unique_fault_codes))
    
    #One hot coding for Fault Codes
    df_fc = pd.DataFrame(fc_df, columns = unique_fault_codes, index = fat_table.index)

    for i, line in tqdm(enumerate(cmp_fault_codes)):
        fc_df[i,np.isin(df_fc.columns.values, line)] = 1
    
    fc_df = pd.DataFrame(fc_df, columns = unique_fault_codes, index = fat_table.index)

    #One hot coding for Fault Codes
    #convert for one hot encoding
    fc_lookup = pd.read_csv(path_fault_look, header = 1)
    fc_lookup = fc_lookup[['Fault Code', 'Fault Code Category']]
    fc_lookup = one_hot_fn(df = fc_lookup,
                              column = 'Fault Code Category',
                              one_hot_col = 'Fault Code',
                              col_prefix = "", 
                              col_suffix = "")

    #perform the one hot encoding
    fc_cols = []
    for x in range(fc_df.shape[1]):
        if len(fc_lookup[fc_lookup['Fault Code'] == fc_df.columns.values[x]]) == 0:
            fc_cols.append('FC:{}'.format(fc_df.columns.values[x]))
        else:
            #fc_cols.append('FC_' + \
            #               fc_lookup[fc_lookup['Fault Code'] == fc_df.columns.values[x]]['Fault Code Category'].values[0].replace(" ", "_") + \
            #               ':' + str(fc_df.columns.values[x]))
            fc_cols.append('FC_' + \
                           fc_lookup[fc_lookup['Fault Code'] == fc_df.columns.values[x]]['Fault Code Category'].values[0].replace(" ", "_"))
    fc_df.columns = fc_cols
    return(fc_df)

#function to clean numeric data - @Sanjay, need to check if we have two seperate functions to do this
def clean_numeric(numeric_cols, #column names for numeric data
                 df, #data frame that contains the numeric columns to be cleaned
                 global_lower, #global lower bound for all data (values less than this will be removed)
                 global_upper #global upper bound for all data (values higher than this will be removed)
                  ):
    #Clean numeric columns
    #@Sanjay - might be able to simplify with .dtypes and not use a loop here
    for col in tqdm(numeric_cols):
        if df[col].dtype not in ['float64', 'float32']:
            df[col] = df[col].str.replace(',','.')
            df[col] = df[col].astype(float)
        if 'PERCENT' in col:
            df[col][df[col] < 0] = float('NaN')
            df[col][df[col] > 100] = float('NaN')
        else:
            df[col][df[col] < global_lower] = float('NaN')
            df[col][df[col] > global_upper] = float('NaN')
    return(df)
 
#change sign from positive to negative - will not need this once the DE task is done
def convert_duration(col_list, #column names for duration data
                     df #enhanced fat table
                     ):
    for col in tqdm(col_list):
        df[col] = df[col]*-1
    return(df)
  
##Create the categorical variables as one hot encoing - need to doulble check with Sanjay
def cat_features(cat_cols, #column names for categorical data
                 fat_table #enhanced fat table
                 ):
    # Categorical Features
    df_cat = pd.DataFrame()
    
    #prepare the one hot encoding column names
    for col in tqdm(cat_cols):
        df_temp = pd.get_dummies(fat_table[col])
        df_temp.columns = [col + ':' + df_temp.columns.values.astype(str)[x] for x in range(df_temp.shape[1])]
        df_cat = pd.concat([df_cat, df_temp], axis = 1)
    return(df_cat)

#%% ##########################

#%% functions subsetting issue####
def subset_on_issue_main(issue_name,
                         path_to_results,
                         mileage_min,
                         mileage_col,
                         incident_path,
                         incident_merge_col,
                         base_merge_col,
                         esn_list,
                         esn_file_type,
                         esn_sheet,
                         engine_type,
                         global_lower,
                         global_upper,
                         numeric_cols, #list of numeric columns
                         file_one_hot_raw = "",
                         file_no_one_hot_raw = "",
                         df_one_hot_raw = pd.DataFrame(),
                         df_no_one_hot_raw = pd.DataFrame(),
                         write_result = True, 
                         set_new = True):
    
    #clean and reset folder structure 
    if set_new: 
        # make folder for results
        output_folder_mk(path_to_results = path_to_results, 
                         issue_name = issue_name,
                         make_type = "new")
    # make folder for results
    output_folder_mk(path_to_results = path_to_results, 
                     issue_name = issue_name,
                     make_type = "data")
    
    
    
    #read in some data
    if df_one_hot_raw.size == 0:
        df_one_hot_raw = read_and_filter(file_name = file_one_hot_raw,
                                         mileage_min = mileage_min,
                                         mileage_col = mileage_col)
        
    if df_no_one_hot_raw.size == 0:
        df_no_one_hot_raw = read_and_filter(file_name = file_no_one_hot_raw,
                                            mileage_min = mileage_min, 
                                            mileage_col = mileage_col)
    
    #if df_one_hot_raw.size == 0:
       # df_one_hot_raw = read_and_filter(file_name = file_one_hot_raw, 
        #                                    mileage_min = mileage_min, 
        #                                    mileage_col = mileage_col)
            
    #read in some data for the issue
    issue_df = load_esn_list(incident_path = incident_path, 
                             incident_merge_col = incident_merge_col, 
                             esn_list = esn_list,
                             esn_file_type = esn_file_type,
                             esn_sheet =  esn_sheet)
    
    ##subset the main dataframe to contain both the positive (label = 1) and negative (label = 0) set
    #one hot encoeded raw data
    df_one_hot_raw = subset_and_concat(df = df_one_hot_raw, 
                                       issue_df = issue_df, 
                                       df_no_one_hot_raw = df_no_one_hot_raw, 
                                       engine_type = engine_type, 
                                       incident_merge_col = incident_merge_col,
                                       base_merge_col = base_merge_col)
    
    #non-one hot encoeded raw data
    df_no_one_hot_raw = subset_and_concat(df = df_no_one_hot_raw, 
                                          issue_df = issue_df, 
                                          df_no_one_hot_raw = df_no_one_hot_raw, 
                                          engine_type = engine_type, 
                                          incident_merge_col = incident_merge_col,
                                          base_merge_col = base_merge_col)
    
    ##clean and normalize data
    #clean only - no normilization for the one hot encoded data
    df_one_hot_raw = clean_and_standardize(df = df_one_hot_raw,
                                           global_lower = global_lower,
                                           global_upper = global_upper,
                                           numeric_cols = numeric_cols,
                                           standardize = False)
    
    #clean and normilize for the one hot encoded data
    df_one_hot_norm = clean_and_standardize(df = df_one_hot_raw, 
                                            global_lower = global_lower,
                                            global_upper = global_upper,
                                            numeric_cols = numeric_cols,
                                            standardize = True)
    
    #clean and normilize for the non-one hot encoded data
#    df_no_one_hot_norm = clean_and_standardize(df = df_no_one_hot_raw, 
#                                              global_lower = global_lower, 
#                                              global_upper = global_upper,
#                                              numeric_cols = numeric_cols,
#                                              standardize = True)
    
    #clean only - no normilization for the non-one hot encoded data
    df_no_one_hot_raw = clean_and_standardize(df = df_no_one_hot_raw, 
                                              global_lower = global_lower, 
                                              global_upper = global_upper,
                                              numeric_cols = numeric_cols,
                                              standardize = False)
    
    #write csv files
    if write_result:
        print("writing resutls")
        #write non-one hot encoded non-normalized data to a csv
        write_results_fn(df = df_no_one_hot_raw, 
                      path_to_results = path_to_results, 
                      issue_name = issue_name, 
                      data_folder = "Data/", 
                      file_end = "_no_one_hot_raw")
        
        #write one hot encoded non-normalized data to a csv
        write_results_fn(df = df_one_hot_raw, 
                      path_to_results = path_to_results, 
                      issue_name = issue_name, 
                      data_folder = "Data/", 
                      file_end = "_one_hot_raw")
        
        #write one hot encoded normalized data to a csv
        write_results_fn(df = df_one_hot_norm, 
                      path_to_results = path_to_results, 
                      issue_name = issue_name, 
                      data_folder = "Data/", 
                      file_end = "_one_hot_norm")
    
    return(df_no_one_hot_raw, df_one_hot_raw, df_one_hot_norm)

#apply mileage filter to the data bring loaded
def read_and_filter(file_name, #name of the file to be loaded including the path for absolute refrence
                    mileage_min, #filter everything below this mileage
                    mileage_col, #column that has the mileage information to filter on
                    load_file = True):
    
    #read data from csv file to pandas data frame
    df = pd.read_csv(file_name)

    #filter out data below the mileage min or if mileage is nan
    df = df[(df[mileage_col] > mileage_min) | \
            (np.isnan(df[mileage_col]))].reset_index(drop = True)
    
    return(df)
 
#load the list of 
def load_esn_list(incident_path, #path where there is a list of esn incidents
                  incident_merge_col, #columne name for the esn numbers
                  esn_list, #filename that contains the incident list
                  esn_file_type = "csv", #type of file to be loaded cba be excel or csv
                  esn_sheet = ""
                  ):
    
    #Load Issue ESN list
    if esn_file_type == "csv":
        #read in the csv file
        issue_df = pd.read_csv(incident_path.format(esn_list), names = incident_merge_col)
        #select the columns with the merge information
        issue_df = issue_df[incident_merge_col]
    elif esn_file_type == "excel" or esn_file_type == "xlsx" or esn_file_type == "xls":
        #read excel file
        issue_df = pd.ExcelFile(incident_path.format(esn_list))
        #select the sheet that contains the esn information
        issue_df = pd.read_excel(issue_df, esn_sheet)
        #select the columns with the merge information
        issue_df = issue_df[incident_merge_col]
    else: 
        return("file tpye must be csv or excel")
    
    return(issue_df)
    
#subset the main dataframe to contain both the positive (label = 1) and negative (label = 0) set
def subset_and_concat(df, 
                      issue_df,
                      df_no_one_hot_raw, 
                      engine_type, 
                      incident_merge_col, #columne name for the merge of incident selection
                      base_merge_col,  #columne name for the merge of base selection
                      base_esn_col = 'ESN',
                      neg_set_size = 3000
                      ):
    
    #convert the earliest indication date to a date time so that if can be merged with what is added from the esn file - @Sanjay, we need to talk about how we do this merge at some point
    df['EARLIEST_INDICATION_DATE'] = pd.to_datetime(df['EARLIEST_INDICATION_DATE'])
    df_no_one_hot_raw['EARLIEST_INDICATION_DATE'] = pd.to_datetime(df_no_one_hot_raw['EARLIEST_INDICATION_DATE'])
    
    #rename the columns to match the base_merge_col
    issue_df.columns = base_merge_col
    if len(base_merge_col) > 1:
        issue_df['EARLIEST_INDICATION_DATE'] = pd.to_datetime(issue_df['EARLIEST_INDICATION_DATE'])
    
    #merge esn list with data frame in one hot or no one hot
    issue_df = pd.merge(issue_df, df, left_on = base_merge_col, right_on = base_merge_col, how = 'inner').copy()
    issue_df['Label'] = 1
    
    #conver the indication date back to a string - @Sanjay should be changed with the merge comment above
    df['EARLIEST_INDICATION_DATE'] = df['EARLIEST_INDICATION_DATE'].astype(str)
    df_no_one_hot_raw['EARLIEST_INDICATION_DATE'] = df_no_one_hot_raw['EARLIEST_INDICATION_DATE'].astype(str)
    issue_df['EARLIEST_INDICATION_DATE'] = issue_df['EARLIEST_INDICATION_DATE'].astype(str)
    
    #Negative set ESNs
    #need to enhance the filter here to take different subsets of engine populations
    engine_type_esns = df_no_one_hot_raw[df_no_one_hot_raw["REL_ANALYSIS_RATE_CAT"].isin(\
                                         engine_type)][base_esn_col].unique()
    #Get unique indices
    esn_idxs = ~np.isin(engine_type_esns, issue_df.ESN.unique())
    
    #Subset on ESNs not in issue list
    engine_type_esns = engine_type_esns[esn_idxs]
    if len(engine_type_esns) > neg_set_size:
        engine_type_esns = pd.DataFrame(engine_type_esns).sample(n=neg_set_size, random_state=2).values
        engine_type_esns = engine_type_esns.ravel()
    #Get non-issue lst
    df_not_issue = df.query("ESN in @engine_type_esns")
    df_not_issue['Label'] = 0
    
    #Concatenate!
    df_all = pd.concat([issue_df, df_not_issue]).reset_index(drop = True)
    
    return(df_all)

#clean and normalize data
def clean_and_standardize(df, #data frame that contains the columns to be cleaned and normalized
                          global_lower, #global lower bound for data in all coumns
                          global_upper, #global upper bound for data in all coumns
                          numeric_cols, #list of numeric columns
                          standardize = True #will normalize the data if value is true
                          ):
    
    #Remove spurious values
    clean_numeric(numeric_cols = numeric_cols,
                 df = df, 
                 global_lower = global_lower, 
                 global_upper = global_upper)
            
    if standardize == True:
        #Scale numeric features
        for col in tqdm(numeric_cols):
            #remove the nan columns that can be used for the normization
            finite_vals = df[col].dropna()
            if len(finite_vals) > 0:
                #calculate the scaled values
                scaled_vals = scale(finite_vals)
                df.iloc[finite_vals.index.values, np.where(df.columns.values == col)[0][0]] = scaled_vals
            
            #Replace NaN with 0's (Mean)
            df[col] = df[col].replace(np.nan, 0)
            
    return(df)
    
#%% ##########################

#%% functions for initial selection####
#reduce the number of columns in a first pass  
def first_pass_selection_main(issue_name,
                              path_to_results,
                              no_coverage_perc_thresh, #minimum number (%) of rows in a column that need to be complete to keep this feature in the dataset
                              coef_of_var_thresh, #minimum variance of a feature that need to be complete to keep this feature in the dataset
                              min_max_thresh, #threshold to cut off the top and bottom of a distribution 
                              cat_cols, #column names for categorical data
                              numeric_cols, #list of numeric columns
                              uv_cols, #list of columns for universal views
                              df_one_hot_norm = pd.DataFrame(),
                              df_one_hot_raw = pd.DataFrame()
                              ):
    
    # Load Data if it is not passed in as a as a data frame
    if df_one_hot_norm.size == 0:
        df_one_hot_norm = pd.read_csv("{}{}/Data/{}_one_hot_norm.csv".format(path_to_results, issue_name, issue_name))

    # Load Data if it is not passed in as a as a data frame
    if df_one_hot_raw.size == 0:
        df_one_hot_raw = pd.read_csv("{}{}/Data/{}_no_one_hot_raw.csv".format(path_to_results, issue_name, issue_name))
    
    #sub-select columns based on coverage
    df_one_hot_norm_reduced = clean_sub_select(df = df_one_hot_norm, 
                                               df_one_hot_raw = df_one_hot_raw, 
                                               no_coverage_perc_thresh = no_coverage_perc_thresh, 
                                               coef_of_var_thresh = coef_of_var_thresh, 
                                               min_max_thresh = min_max_thresh, 
                                               cat_cols = cat_cols, 
                                               numeric_cols = numeric_cols, 
                                               uv_cols = uv_cols)
    
    #write out the reduced results after removing some of the columns
    write_results_fn(df = df_one_hot_norm, 
                     path_to_results = path_to_results, 
                     issue_name = issue_name, 
                     data_folder = "Data/", 
                     file_end = "_one_hot_norm_reduced")
    
    return(df_one_hot_norm_reduced)

#get a set of columns to remove based on the coverage of the column (how many rows have values)
def coverage_analysis(df, #data frame with columns to check the coverage of
                      vars_to_delete, #list of variables to be removed - the list will be appended in this function @Sanjay we would probably change how this works a bit
                      no_coverage_perc_thresh, #minimum number (%) of rows in a column that need to be complete to keep this feature in the dataset
                      numeric_cols #list of numeric columns
                      ):

    #Assess Coverage
    df_coverage = pd.DataFrame({'Variable': numeric_cols})
    df_coverage['Coverage'] = float('NaN')
        
    for col in numeric_cols:
        df_coverage.iloc[df_coverage[df_coverage.Variable == col].index, 1] = \
            df[col].dropna().count()/df[col].shape[0]
        
    #Variables with no coverage
    no_coverage_vars = np.array(df_coverage.query("Coverage < @no_coverage_perc_thresh")['Variable'])
    
    print("The following variables have no coverage:")
    for var in no_coverage_vars:
        vars_to_delete.add(var)
        print(var)
        
    return(vars_to_delete)

#get a subset of variables that do not have enough variance to keep
def coef_of_var(df, #data frame with columns to check the variance of
                vars_to_delete, #list of variables to be removed - the list will be appended in this function @Sanjay we would probably change how this works a bit
                coef_of_var_thresh, #minimum variance of a feature that need to be complete to keep this feature in the dataset
                numeric_cols #list of numeric columns
                ):
    
    #Coefficient of Variation
    
    #Create empty dataframe for coefficient of variation calculation
    df_ss = pd.DataFrame({'Variable': numeric_cols})
    df_ss['Mean'] = float('NaN')
    df_ss['Variance'] = float('NaN')
    
    #Calculate mean and variance for numeric cols
    #Convert columns to float and remove extraneous values
    for col in tqdm(numeric_cols):
        df_ss.iloc[df_ss[df_ss.Variable == col].index, 1] = \
        np.mean(df[col].dropna())
        df_ss.iloc[df_ss[df_ss.Variable == col].index, 2] = \
        np.var(df[col].dropna())
        
    #Coefficient of variation = sigma/mu
    df_ss['Coef_of_Var'] = np.abs(np.sqrt(df_ss['Variance'])/df_ss['Mean'])
    
    #select the columns that are below variance threshold
    low_var_vars = np.array(df_ss.query("Coef_of_Var < @coef_of_var_thresh")['Variable'])
    
    print("The following variables have low coefficient of variation:")
    for var in low_var_vars:
        vars_to_delete.add(var)
        print(var)
        
    return(vars_to_delete)
 
#remove the values that are the top or bottom percentages
#@Sanjay - can you help write a better description of this function - not sure what is happening inside
def min_max_scaler(df, #data frame with columns to check the min and max value
                   vars_to_delete, #list of variables to be removed - the list will be appended in this function @Sanjay we would probably change how this works a bit
                   min_max_thresh, #threshold to cut off the top and bottom of a distribution 
                   numeric_cols #list of numeric columns
                   ):
        
    # Min/Max Scalar
    df_mms = pd.DataFrame({'Variable': numeric_cols})
    df_mms['scaled_var'] = float('NaN')
    
    min_max_scaler = MinMaxScaler()
    for col in numeric_cols:
        if len(df[col].dropna()) > 0:
            x_scaled = min_max_scaler.fit_transform(df[col].\
                                                dropna().values.reshape(-1, 1))
            df_mms.iloc[df_mms[df_mms.Variable == col].index, 1] = np.var(x_scaled)
        
    
    min_max_vars = df_mms.query("scaled_var < @min_max_thresh")['Variable']
    
    print("The following variables have low variance after scaling:")
    for var in min_max_vars:
        vars_to_delete.add(var)
        print(var)
        
    return(vars_to_delete)

#find categorical variables to delete
def remove_categories(df, #main data frame with all of the features that may have categoical data
                      vars_to_delete, #list of variables to be removed - the list will be appended in this function @Sanjay we would probably change how this works a bit
                      numeric_cols #list of numeric columns
                      ):
    
    #select all non-numeric columns
    cat_idxs = [df.columns.values[x] not in numeric_cols for x in range(df.shape[1])]    
    
    #group the positive and negative group (both issue and non-issue), which is indicated by label column
    df_sub = df.iloc[:,cat_idxs].groupby('Label')\
            .sum().transpose().reset_index()
    
    #find the categories with all of the values are zero
    bad_cats = df_sub[(df_sub[1] == 0) & (df_sub[0] == 0)]['index']
    for cat in bad_cats:
        print(cat)
        vars_to_delete.add(cat)
        
    return(vars_to_delete)

#remove the columns from the dataframe that are selected for deletetion
def remove_vars(df, #data frame to remove the columns from 
                vars_to_delete #list of variables to be removed
                ):
    #loop over all of the features and remove the ones that are listed in vars_to_delete
    for var in vars_to_delete:
        del df[var]
    return(df)
 
#sub-select columns based on 
def clean_sub_select(df, #dataframe that will be sub-selected
                     df_one_hot_raw, #data frame that contains the one hot encoded data
                     no_coverage_perc_thresh, #minimum number (%) of rows in a column that need to be complete to keep this feature in the dataset - set in the options file
                     coef_of_var_thresh, #minimum variance of a feature that need to be complete to keep this feature in the dataset
                     min_max_thresh, #threshold to cut off the top and bottom of a distribution 
                     cat_cols, #list of categorical columns
                     numeric_cols, #list of numeric columns
                     uv_cols #list of columns for universal views
                     ):
    #List to array for easy subsetting - need to think about df_oepl, do we need to make as an option in the function to use that?
    __, __, cat_cols, numeric_cols, __ = get_column_types(df_oepl = "", cat_cols = cat_cols, numeric_cols = numeric_cols, uv_cols = uv_cols, get_oepl = False)
    
    #Create set to hold variables to delete
    vars_to_delete = set()
    
    #get a set of columns to remove based on the coverage of the column (how many rows have values)
    vars_to_delete = coverage_analysis(df = df_one_hot_raw, 
                                       vars_to_delete = vars_to_delete, 
                                       no_coverage_perc_thresh = no_coverage_perc_thresh, 
                                       numeric_cols = numeric_cols)
    #get a subset of variables that do not have enough variance to keep
    vars_to_delete = coef_of_var(df = df_one_hot_raw, 
                                 vars_to_delete = vars_to_delete, 
                                 coef_of_var_thresh = coef_of_var_thresh, 
                                 numeric_cols = numeric_cols)
    
    #remove the values that are the top or bottom percentages
    vars_to_delete = min_max_scaler(df = df_one_hot_raw, 
                                    vars_to_delete = vars_to_delete, 
                                    min_max_thresh = min_max_thresh, 
                                    numeric_cols = numeric_cols)

    #find categorical variables to delete
    vars_to_delete = remove_categories(df = df, 
                                       vars_to_delete = vars_to_delete, 
                                       numeric_cols = numeric_cols)
    
    #Remove variables from above analysis
    df = remove_vars(df, vars_to_delete)
    
    return(df)

#@Sanjay need to determine if we need this function
#get column names from different places
def get_column_types(df_oepl, #oepl dataframe to get unique column names
                     cat_cols, #column names for categorical data
                     numeric_cols, #list of numeric columns
                     uv_cols, #list of columns for universal views
                     get_oepl = True, #option of whether or not to get the oepl columns
                     oepl_only = False #option of whether or not to get only the oepl columns
                     ):

    # ESN Column - need to see if this is ever used
    esn_col = ['ESN']
    
    if get_oepl == True:
    
        # OEPL Columns
        oepl_cols = df_oepl['OPT_ITEM_DESC_NEW'].unique()
    
        ## Remove NaN in OEPL Columns ##
        float_idxs = np.array([type(oepl_cols[x]) == float for x in range(len(oepl_cols))])
        oepl_cols = np.delete(oepl_cols, np.where(float_idxs)[0][0])
        
        # Get ESNs #
        #needs to be fixed
        oepl_cols = np.delete(oepl_cols, 540)
        
        if oepl_only:
            return(oepl_cols)
        
    else:
        
        oepl_cols = []
    
    #convert list to a numpy array
    numeric_cols = np.array(numeric_cols)
    
    return(esn_col, oepl_cols, cat_cols, numeric_cols, uv_cols)
    
#%% ##########################

#%% functions for univariate analysis####
    
def univariate_analysis_main(issue_name,
                             path_to_results,
                             path_engine_options,
                             path_fault_look,
                             path_extra_pickle,
                             path_to_build_pop,
                             path_aliases,
                             path_base,
                             num_cores,
                             uv_col_order,
                             cat_cols, #column names for categorical data
                             numeric_cols, #list of numeric columns
                             uv_cols, #list of columns for universal views
                             writer,
                             df_column_priority,
                             oepl_desc_df = pd.DataFrame(),
                             df_one_hot_norm_reduced = pd.DataFrame(),
                             df_no_one_hot_raw = pd.DataFrame(),
                             df_one_hot_raw = pd.DataFrame(),
                             path_oepl_desc = ""
                             ):
    
    #make folder structure for 
    output_folder_mk(path_to_results = path_to_results, 
                     issue_name = issue_name,
                     make_type = "figs") 
    
    ## Load Data ##
    #read in some data
    if df_one_hot_norm_reduced.size == 0:
        df_one_hot_norm_reduced = pd.read_csv("{}{}/Data/{}_one_hot_norm_reduced.csv".format(path_to_results, issue_name, issue_name))

    if df_no_one_hot_raw.size == 0:    
        df_no_one_hot_raw = pd.read_csv("{}{}/Data/{}_no_one_hot_raw.csv".format(path_to_results, issue_name, issue_name))

    if df_one_hot_raw.size == 0:        
        df_one_hot_raw = pd.read_csv("{}{}/Data/{}_one_hot_raw.csv".format(path_to_results, issue_name, issue_name))
    
    #load oepl descriptions
    if oepl_desc_df.size == 0:
        oepl_desc_df = load_options_data(path_oepl_desc = path_oepl_desc)
    
        
    df_datasets, new_views = create_datasets(df_one_hot_norm_reduced = df_one_hot_norm_reduced,
                                             cat_cols = cat_cols, 
                                             numeric_cols = numeric_cols, 
                                             uv_cols = uv_cols)
    
    df_feature_aliases = pd.read_excel(path_aliases, sheet = '20180411_Feature Alias')
    
    df_final_list = create_final_list(df_datasets = df_datasets, 
                                      df_one_hot_raw = df_one_hot_raw, 
                                      df_no_one_hot_raw = df_no_one_hot_raw,
                                      df_column_priority = df_column_priority,
                                      df_feature_aliases = df_feature_aliases,
                                      num_cores = num_cores,
                                      numeric_cols = numeric_cols)
    
    df_oepl_sub = get_oepl(path_engine_options = path_engine_options, 
                           df_one_hot_raw = df_one_hot_raw, 
                           oepl_desc_df = oepl_desc_df)
    
    df_vpcr = get_vpcr(df_one_hot_raw)
    
    df_fc = get_fault_code(df_one_hot_raw = df_one_hot_raw, 
                           path_fault_look = path_fault_look)
    
    #Universal Views & Save Excel
    df_universal_view = create_universal_view(df_no_one_hot_raw = df_no_one_hot_raw, 
                                              path_extra_pickle = path_extra_pickle, 
                                              uv_col_order = uv_col_order, 
                                              cat_cols = cat_cols, 
                                              numeric_cols = numeric_cols, 
                                              uv_cols = uv_cols)
        
    #build_mab_chart(universal_view = df_universal_view, 
    #                issue_name = issue_name, 
    #                path_to_results = path_to_results,
    #                path_to_build_pop = path_to_build_pop)
    
    data_folder = "Data/"
    writer = univ_save_and_plot(path_to_results = path_to_results,
                                path_base = path_base,
                       issue_name = issue_name, 
                       data_folder = data_folder,
                       writer = writer,
                       new_views = new_views, 
                       df_final_list = df_final_list, 
                       df_universal_view = df_universal_view, 
                       df_no_one_hot_raw = df_no_one_hot_raw, 
                       df_oepl_sub = df_oepl_sub, 
                       df_vpcr = df_vpcr, 
                       df_fc = df_fc)

    return(new_views, df_final_list, df_universal_view, df_oepl_sub, df_vpcr, df_fc, writer)

def create_datasets(df_one_hot_norm_reduced, 
                    cat_cols, #column names for categorical data
                    numeric_cols, #list of numeric columns
                    uv_cols #list of columns for universal views
                    ):
    
    __, __, __, numeric_cols, __ = get_column_types(df_oepl = "", cat_cols = cat_cols, numeric_cols = numeric_cols, uv_cols = uv_cols, get_oepl = False)

    
    new_views = ['Usage', 'Design', 'Manufacturing']
    design = ('VPCR', 'OEPL', 'ESN', 'REL_ANALYSIS_RATE_CAT', 'REL_CPL_NUM', \
              'REL_DESIGN_HSP_NUM', 'REL_DESIGN_RPM_NUM', 'REL_MKTG_HSP_NUM', \
              'REL_MKTG_RPM_NUM', 'REL_EMISSIONS_FAMILY_CODE')
    manuf = ('MES', 'EPAT', 'ESN', 'REL_BUILT_ON_WEEKEND', 'REL_ECM_PART_NO', \
            'REL_USER_APPL_DESC', 'REL_CMP_PROGRAM_GROUP_NAME', \
             )
    use = ('INS','FC', 'ESN', 'REL_OEM_TYPE', 'REL_OEM_GROUP', 'REL_BUILD_YEAR_MONTH') 
    
    design_cols = [df_one_hot_norm_reduced.columns.values[x].startswith(design + ('Label',)) for x in range(df_one_hot_norm_reduced.shape[1])]
    manuf_cols = [df_one_hot_norm_reduced.columns.values[x].startswith(manuf + ('Label',)) for x in range(df_one_hot_norm_reduced.shape[1])]
    use_cols = [df_one_hot_norm_reduced.columns.values[x].startswith(use + ('Label',)) for x in range(df_one_hot_norm_reduced.shape[1])]
    
    df_design = df_one_hot_norm_reduced.iloc[:,design_cols]
    df_manuf = df_one_hot_norm_reduced.iloc[:,manuf_cols]
    df_use = df_one_hot_norm_reduced.iloc[:,use_cols]
    
    df_design = df_design.drop_duplicates('ESN').reset_index(drop = True)
    df_manuf = df_manuf.drop_duplicates('ESN').reset_index(drop = True)
    
    del df_design['ESN'], df_manuf['ESN'], df_use['ESN']
    
    df_datasets = [df_use, df_design, df_manuf]
    return(df_datasets, new_views)
    
def create_final_list(df_datasets,
                      df_one_hot_raw,
                      df_no_one_hot_raw,
                      df_column_priority,
                      df_feature_aliases,
                      num_cores,
                      numeric_cols #list of numeric columns
                      ):
    
    df_final_list = []

    for df_ in df_datasets:
    
        df_num_scores = pd.DataFrame(columns = ['Feature', 'Kolmogorov–Smirnov'], 
                                     index = np.arange(len(numeric_cols)))
        df_num_scores['Feature'] = numeric_cols
        df_num_scores['Kolmogorov–Smirnov'] = df_num_scores['Kolmogorov–Smirnov'].astype(float)
        df_num_scores['Type'] = 'Numerical'
    
        for col in numeric_cols:
            if col in df_.columns.values:
                pos_val = df_one_hot_raw.query("Label == 1")[col].dropna().values.tolist()
                neg_val = df_one_hot_raw.query("Label == 0")[col].dropna().values.tolist() 
                
                if len(np.unique(pos_val)) > 1 and len(np.unique(pos_val)) > 1:
                    
                    #__, __, ad_test = anderson_ksamp([pos_val, neg_val])
                    __, ks_test = ks_2samp(pos_val, neg_val)
                    #__, rs_test = ranksums(pos_val, neg_val)
                    
                    #if ad_test > 1:
                    #    ad_test = 1
                
                    df_num_scores.loc[df_num_scores['Feature'] == col, 'Kolmogorov–Smirnov'] = ks_test
                
        df_num_scores = df_num_scores[~np.isnan(df_num_scores['Kolmogorov–Smirnov'])]
        df_num_scores = df_num_scores.sort_values(by = 'Kolmogorov–Smirnov').reset_index(drop = True)
        
        cat_cols = df_.columns.values[[df_.columns.values[x] not in numeric_cols + ['Label'] for x in range(df_.shape[1])]]
        df_cat_scores = pd.DataFrame(columns = ['Feature', 'Fishers_Exact', \
                                            'Issue Cat', 'Non-Issue Cat'], \
                     index = np.arange(len(cat_cols)))
        df_cat_scores['Feature'] = cat_cols
        df_cat_scores['Fishers_Exact'] = df_cat_scores['Fishers_Exact'].astype(float)
    
        df_cat_scores['Type'] = 'Categorical'
        
        for col in cat_cols:
            
            if col in df_.columns.values:
                cong_table = pd.crosstab(df_[col], df_['Label'])
                if cong_table.shape == (2,2):
                    #__, chisq_test, __, __ = chi2_contingency(cong_table)
                    #mcnemar_test = mcnemar(cong_table)
                    __, fishers_exact = fisher_exact(cong_table)
                    
                    df_cat_scores.loc[df_cat_scores['Feature'] == col, 'Fishers_Exact'] = fishers_exact
    
                
        df_cat_scores = df_cat_scores[~np.isnan(df_cat_scores['Fishers_Exact'])].\
                                        sort_values(by = 'Fishers_Exact').\
                                        reset_index(drop = True)
                                
        df_num_scores['Sample Count'] = float('NaN')
        df_num_scores['Unique Count'] = float('NaN')
        df_num_scores['Issue Median'] = float('NaN')
        df_num_scores['Non-Issue Median'] = float('NaN')
        df_num_scores['Sample Median'] = float('NaN')
        df_num_scores['Sample Issue Rate'] = float('NaN')
        
        df_cat_scores['Sample Count'] = float('NaN')
        df_cat_scores['Issue Rate, Category = 1'] = float('NaN')
        df_cat_scores['Issue Rate, Category = 0'] = float('NaN')
        df_cat_scores['Categorical Rate, Issue = 1'] = float('NaN')
        df_cat_scores['Categorical Rate, Issue = 0'] = float('NaN')
        df_cat_scores['Sample Categorical Rate'] = float('NaN')
        df_cat_scores['Sample Issue Rate'] = float('NaN')
    
        #df_dir = df_cat_scores
        #summary_stat_cat = Parallel(n_jobs = num_cores)(delayed(get_summary_stats)(df_one_hot_raw, df_no_one_hot_raw, df_dir, i, 'Categorical') for i in range(df_cat_scores.shape[0]))
        #df_dir = df_num_scores
        #summary_stat_num = Parallel(n_jobs = num_cores)(delayed(get_summary_stats)(df_one_hot_raw, df_no_one_hot_raw, df_dir, i, 'Numerical') for i in range(df_num_scores.shape[0]))
        
        df_dir = df_cat_scores
        if len(df_dir) > 0:
            summary_stat_cat = Parallel(n_jobs = num_cores)(delayed(get_summary_stats)(df_one_hot_raw, df_no_one_hot_raw, df_dir, i, 'Categorical') for i in range(df_cat_scores.shape[0]))
        else:
            summary_stat_cat = []
        df_dir = df_num_scores
        if len(df_dir) > 0:
            summary_stat_num = Parallel(n_jobs = num_cores)(delayed(get_summary_stats)(df_one_hot_raw, df_no_one_hot_raw, df_dir, i, 'Numerical') for i in range(df_num_scores.shape[0]))
        else:
            summary_stat_num = []
            
        if len(summary_stat_num) > 0:
            df_final_num = pd.concat(summary_stat_num)
            
            df_final_num['Stat Significance'] = 3
            df_final_num.loc[df_final_num['Kolmogorov–Smirnov'] <= 0.10, 'Stat Significance'] = 2
            df_final_num.loc[df_final_num['Kolmogorov–Smirnov'] <= 0.01, 'Stat Significance'] = 1
            
            df_final_num = df_final_num.drop(columns = ['Kolmogorov–Smirnov'])
    
        else:
            df_final_num = pd.DataFrame()
            
        if len(summary_stat_cat) > 0:
            df_final_cat = pd.concat(summary_stat_cat)
            
            df_final_cat['Stat Significance'] = 3
            df_final_cat.loc[df_final_cat['Fishers_Exact'] <= 0.10, 'Stat Significance'] = 2
            df_final_cat.loc[df_final_cat['Fishers_Exact'] <= 0.01, 'Stat Significance'] = 1
            
            df_final_cat = df_final_cat.drop(columns = ['Fishers_Exact'])
        else:
            df_final_cat = pd.DataFrame()
            
        

        
        df_final = pd.concat([df_final_num, df_final_cat]).reset_index(drop = True)
        
        df_final['Sample Issue Rate'] = '{:.2%}'.format(df_no_one_hot_raw.Label.mean())
        
        df_final['Base Feature'] = str('NaN')

        for i in range(df_final.shape[0]):
            if df_final['Type'][i] == "Numerical":
                df_final.iloc[i,df_final.columns == 'Base Feature'] = df_final['Feature'][i]
            else:
                if df_final['Feature'][i].startswith('FC') or df_final['Feature'][i].startswith('OEPL'):
                    df_final.iloc[i,df_final.columns == 'Base Feature'] = df_final['Feature'][i]
                else:
                    df_final.iloc[i,df_final.columns == 'Base Feature'] = df_final['Feature'][i].split(":")[0]
        
        
        df_final = df_final.merge(df_column_priority, on = 'Base Feature', how = 'left')
        
        df_final['Combined Significance'] = float('NaN')

        for i in range(df_final.shape[0]):
            if df_final['Stat Significance'][i] == 1 and df_final['SME Significance'][i] == 1:
                df_final.iloc[i,df_final.columns == 'Combined Significance'] = 1
            if df_final['Stat Significance'][i] == 1 and df_final['SME Significance'][i] == 2:
                df_final.iloc[i,df_final.columns == 'Combined Significance'] = 3
            if df_final['Stat Significance'][i] == 1 and df_final['SME Significance'][i] == 3:
                df_final.iloc[i,df_final.columns == 'Combined Significance'] = 6
            if df_final['Stat Significance'][i] == 2 and df_final['SME Significance'][i] == 1:
                df_final.iloc[i,df_final.columns == 'Combined Significance'] = 2
            if df_final['Stat Significance'][i] == 2 and df_final['SME Significance'][i] == 2:
                df_final.iloc[i,df_final.columns == 'Combined Significance'] = 4
            if df_final['Stat Significance'][i] == 2 and df_final['SME Significance'][i] == 3:
                df_final.iloc[i,df_final.columns == 'Combined Significance'] = 8
            if df_final['Stat Significance'][i] == 3 and df_final['SME Significance'][i] == 1:
                df_final.iloc[i,df_final.columns == 'Combined Significance'] = 5
            if df_final['Stat Significance'][i] == 3 and df_final['SME Significance'][i] == 2:
                df_final.iloc[i,df_final.columns == 'Combined Significance'] = 7
            if df_final['Stat Significance'][i] == 3 and df_final['SME Significance'][i] == 3:
                df_final.iloc[i,df_final.columns == 'Combined Significance'] = 9
                
        df_final = df_final.merge(df_feature_aliases[['Feature', 'Alias', 'Description']], on = 'Feature', how = 'left')
                
        if 'Unique Count' in df_final.columns.values:
            df_final = df_final[['Combined Significance', 'Stat Significance', \
                             'SME Significance', 'Feature', 'Alias', 'System', 'Sub System', \
                             'Description', 'Type', \
                             'Sample Count', 'Unique Count', 'Sample Issue Rate', \
                             'Issue Median', 'Non-Issue Median', 'Sample Median', \
                             'Issue Rate, Category = 1', 'Issue Rate, Category = 0', \
                             'Categorical Rate, Issue = 1', 'Categorical Rate, Issue = 0', \
                             'Sample Categorical Rate']]
        else:
            df_final = df_final[['Combined Significance', 'Stat Significance', \
                             'SME Significance', 'Feature', 'Alias', 'System', 'Sub System', \
                             'Description', 'Type', \
                             'Sample Count', 'Sample Issue Rate', \
                             'Issue Rate, Category = 1', 'Issue Rate, Category = 0', \
                             'Categorical Rate, Issue = 1', 'Categorical Rate, Issue = 0', \
                             'Sample Categorical Rate']]
                                 
        df_final = df_final.sort_values(by = 'Combined Significance', ascending = True).reset_index(drop = True)
                
        df_final_list.append(df_final)
    
    return(df_final_list)
        
#
def get_summary_stats(df_one_hot_raw,
                      df_no_one_hot_raw,
                      df_dir,
                      i,
                      col_type):
    
    if col_type == "Categorical":
        df_dir['Sample Count'][i] = df_one_hot_raw[df_dir.Feature[i]].dropna().count()
        df_dir['Issue Rate, Category = 1'][i] = '{:.2%}'.format(df_one_hot_raw[df_one_hot_raw[df_dir.Feature[i]] == 1].Label.mean())
        df_dir['Issue Rate, Category = 0'][i] = '{:.2%}'.format(df_one_hot_raw[df_one_hot_raw[df_dir.Feature[i]] == 0].Label.mean())
        df_dir['Categorical Rate, Issue = 1'][i] = '{:.2%}'.format(df_one_hot_raw[df_one_hot_raw.Label==1][df_dir.Feature[i]].dropna().mean())
        df_dir['Categorical Rate, Issue = 0'][i] = '{:.2%}'.format(df_one_hot_raw[df_one_hot_raw.Label==0][df_dir.Feature[i]].dropna().mean())
        df_dir['Sample Categorical Rate'][i] = '{:.2%}'.format(df_one_hot_raw[df_dir.Feature[i]].dropna().mean())
    
    if col_type == "Numerical":
        df_dir['Sample Count'][i] = df_no_one_hot_raw[df_dir.Feature[i]].dropna().count()
        df_dir['Unique Count'][i] = len(np.unique(df_no_one_hot_raw[df_dir.Feature[i]].dropna()))
        df_dir['Issue Median'][i] = df_no_one_hot_raw[df_no_one_hot_raw.Label==1][df_dir.Feature[i]].dropna().median()
        df_dir['Non-Issue Median'][i] = df_no_one_hot_raw[df_no_one_hot_raw.Label==0][df_dir.Feature[i]].dropna().median()
        df_dir['Sample Median'][i] = df_no_one_hot_raw[df_dir.Feature[i]].dropna().median()

    return df_dir[df_dir.Feature == df_dir.Feature[i]]


def get_oepl(path_engine_options, #need to change this file name, path name does not make sense right now
             df_one_hot_raw,
             oepl_desc_df
             ):
    #Option File (X15s)
    df_oepl = pq.read_table(path_engine_options).to_pandas()
    df_oepl['engine_serial_num'] = df_oepl['engine_serial_num'].astype(int)
    
    #Merge with description file
    df_oepl = df_oepl.rename(columns = {'option_assembly_num': 'OPT_NBR'})
    df_oepl = df_oepl.merge(oepl_desc_df, on = 'OPT_NBR', how = 'left')
    df_oepl['option_count'] = 1
    
    
    #Get OEPL
    df_oepl_sub = df_oepl[['engine_serial_num', 'OPT_NBR']]
    df_oepl_sub['OPT_CAT'] = df_oepl_sub['OPT_NBR'].apply(lambda x: x[:2])
    df_oepl_sub = df_oepl_sub.rename(columns = {'engine_serial_num': 'ESN'})
    
    df_oepl_sub = df_one_hot_raw[['ESN', 'EARLIEST_INDICATION_DATE', 'Label']].merge(df_oepl_sub, on = 'ESN', how = 'left')
    df_oepl_sub = df_oepl_sub.drop_duplicates()
    return(df_oepl_sub)
    
def get_vpcr(df_one_hot_raw):
    #VPCR
    vpcr_idx = [df_one_hot_raw.columns.values[x].startswith(('ESN','VPCR')) \
                for x in range(df_one_hot_raw.shape[1])]
    df_vpcr = df_one_hot_raw.iloc[:,vpcr_idx]
    df_vpcr = df_vpcr.set_index('ESN').stack().reset_index().rename(columns = {'level_1': 'VPCR'})
    df_vpcr = df_vpcr[df_vpcr[0] == 1].reset_index(drop = True)
    df_vpcr['VPCR'] = [df_vpcr['VPCR'][x].split("_")[3] for x in range(df_vpcr.shape[0])]
    del df_vpcr[0]
    
    df_vpcr = df_one_hot_raw[['ESN', 'Label']].merge(df_vpcr, on = 'ESN', how = 'right')
    
    return(df_vpcr)
    
def get_fault_code(df_one_hot_raw,
                   path_fault_look
                   ):
    #Fault Code
    fc_idx = [df_one_hot_raw.columns.values[x].startswith(('ESN','FC')) \
                for x in range(df_one_hot_raw.shape[1])]
    df_fc = df_one_hot_raw.iloc[:,fc_idx]
    df_fc = df_fc.set_index('ESN').stack().reset_index().rename(columns = {'level_1': 'FC'})
    df_fc = df_fc[df_fc[0] == 1].reset_index(drop = True)
    df_fc['FC'] = [df_fc['FC'][x].split(":")[1] for x in range(df_fc.shape[0])]
    df_fc['FC'] = df_fc['FC'].astype(int)
    df_fc = df_one_hot_raw[['ESN', 'Label']].merge(df_fc, on = 'ESN', how = 'right')
    df_fc = df_fc.drop_duplicates()
    
    del df_fc[0]
    
    fc_lookup = pd.read_csv(path_fault_look, header = 1)
    fc_lookup = fc_lookup[['Fault Code', 'Fault Code Category']]
    fc_lookup['Fault Code Category'] = [fc_lookup['Fault Code Category'][x].replace(" ", "_") for x in range(fc_lookup.shape[0])]
    fc_lookup = fc_lookup.rename(columns = {'Fault Code': 'FC'})
    df_fc = df_fc.merge(fc_lookup, on = 'FC', how = 'left')
    
    return(df_fc)
    
def create_universal_view(df_no_one_hot_raw,
                          path_extra_pickle,
                          uv_col_order, 
                          cat_cols, #column names for categorical data
                          numeric_cols, #list of numeric columns
                          uv_cols #list of columns for universal views
                          ):
    df_extra = pickle.load(open(path_extra_pickle, "rb" ))
    __, __, __, __, uv_cols = get_column_types(df_oepl = "", cat_cols = cat_cols, numeric_cols = numeric_cols, uv_cols = uv_cols, get_oepl = False)
    df_no_one_hot_raw['EARLIEST_INDICATION_DATE'] = pd.to_datetime(df_no_one_hot_raw['EARLIEST_INDICATION_DATE'])
    df_extra['EARLIEST_INDICATION_DATE'] = pd.to_datetime(df_extra['EARLIEST_INDICATION_DATE'])
    df_extra = df_extra.drop_duplicates(subset = ['ESN', 'EARLIEST_INDICATION_DATE'])
    
    df_sub_extra = df_no_one_hot_raw[['ESN', 'EARLIEST_INDICATION_DATE', 'Label']].merge(df_extra, on = ['ESN', 'EARLIEST_INDICATION_DATE'], how = 'left')
    
    df_sub_extra.ID = df_sub_extra.index + 1
    
    df_fault = df_sub_extra[['ID', 'GREEN_WRENCH_FAULT_CODE_LIST', 'REL_MINED_FAULT_CODES_LIST']]
    
    df_fault['FC_LENGTH'] = [0 if df_fault['GREEN_WRENCH_FAULT_CODE_LIST'][x] \
             is None else len(df_fault['GREEN_WRENCH_FAULT_CODE_LIST'][x]) for x \
             in range(df_fault.shape[0])]
    
    
    df_fault['FAULT_CODE_LIST'] = [np.unique(df_fault['REL_MINED_FAULT_CODES_LIST'][x]) \
            if df_fault['FC_LENGTH'][x] == 0 else np.unique(df_fault['GREEN_WRENCH_FAULT_CODE_LIST'][x]) \
            for x in range(df_fault.shape[0])]
    
    df_fault_final = pd.DataFrame(columns = ['ID', 'FAULT_CODE'])
    for i, line in enumerate(df_fault.FAULT_CODE_LIST):
        if len(line) == 0:
            fault_id = np.repeat(df_fault.ID[i], 1)
            line = ''
            df_temp = pd.DataFrame(data = {'ID': fault_id, 'FAULT_CODE': line})
            df_fault_final = pd.concat([df_fault_final, df_temp])        
        else:     
            fault_id = np.repeat(df_fault.ID[i], len(line))
            df_temp = pd.DataFrame(data = {'ID': fault_id, 'FAULT_CODE': line})
            df_fault_final = pd.concat([df_fault_final, df_temp])
        
    df_fault_final.reset_index(drop = True)
    
    df_universal_view = df_fault_final.merge(df_sub_extra, on = 'ID', how = 'left')
    del df_universal_view['GREEN_WRENCH_FAULT_CODE_LIST'], df_universal_view['REL_MINED_FAULT_CODES_LIST']
    
    for col in np.intersect1d(numeric_cols, df_universal_view.columns.values):
        df_universal_view[col] = df_universal_view[col].astype(float)
        df_universal_view[col][df_universal_view[col] < 0] = float('NaN')
        df_universal_view[col][df_universal_view[col] > 10**8] = float('NaN')
    
    df_universal_view = df_universal_view[uv_col_order]
    return(df_universal_view)

def build_mab_chart(universal_view, 
                    issue_name, 
                    path_to_results,
                    path_to_build_pop
                    ):
    #build_population - table that contains overall build statistics
    build_population = pd.read_csv(path_to_build_pop)
    
    #Extract month and year from build population
    build_population['build_date'] = pd.to_datetime(build_population['REL_BUILD_DATE'])
    build_population['build_month'] = [build_population['build_date'][x].month for x in range(build_population.shape[0])]
    build_population['build_year'] = [build_population['build_date'][x].year for x in range(build_population.shape[0])]

    #Group by number of engines built
    build_population = build_population[['build_year', 'build_month', 'count(engine_serial_num)']].\
                        groupby(['build_year', 'build_month']).sum().reset_index()

    #Drop duplicate rows on ID column
    universal_view = universal_view.drop_duplicates(subset = 'ID').\
                        query("Label == 1").reset_index(drop = True)


    #Extract Time to Incident Date
    universal_view['MONTHS_AFTER_BUILD'] = \
                    [round((universal_view.EARLIEST_INDICATION_DATE[x] - \
                            universal_view.REL_BUILD_DATE[x]).days/30) \
                            for x in range(universal_view.shape[0])]
    
    #Extract month and year for build date
    universal_view['REL_BUILD_MONTH'] = \
                    [universal_view['REL_BUILD_DATE'][x].month \
                     for x in range(universal_view.shape[0])]
    
    universal_view['REL_BUILD_YEAR'] = \
                    [universal_view['REL_BUILD_DATE'][x].year \
                     for x in range(universal_view.shape[0])]


    #Count number of ESNs by months after build
    universal_view['COUNT'] = 1
    universal_view_pivot = universal_view.\
                            pivot_table(index = ['REL_BUILD_YEAR', 'REL_BUILD_MONTH'], \
                                        columns = 'MONTHS_AFTER_BUILD', \
                                        values = 'COUNT', aggfunc='sum').\
                                        fillna(0).\
                                        reset_index()

    #Create empty RPH dataframe to input normalized RPH
    df_rph = pd.DataFrame(columns = universal_view_pivot.columns, \
                          index = universal_view_pivot.index)

    #Populate cumulative RPH by Build Date
    for i in range(universal_view_pivot.shape[0]):
        
        #Extract Build year and month
        year = universal_view_pivot['REL_BUILD_YEAR'][i]
        month = universal_view_pivot['REL_BUILD_MONTH'][i]
        df_rph['REL_BUILD_YEAR'][i] = year
        df_rph['REL_BUILD_MONTH'][i] = month
        
        #Iterate across months after build
        for j in range(2, universal_view_pivot.shape[1]):
            
            #Extract total build count for build month and year
            total_build = build_population.\
                        query("build_month == @month & build_year == @year")\
                        ['count(engine_serial_num)']
                        
            #Calculate RPH!!
            df_rph.iloc[i,j] = float(100*universal_view_pivot.iloc[i,j]/total_build)
    
        #Find cumulative RPH
        df_rph.iloc[i,2:] = np.cumsum(df_rph.iloc[i,2:])
    
    #Add Build Year + Month to RPH Data Frame
    df_rph['REL_BUILD_YEAR_MONTH'] = [str(df_rph['REL_BUILD_YEAR'][x]) + \
                                  "_" + str(df_rph['REL_BUILD_MONTH'][x]) \
                                  for x in range(df_rph.shape[0])]

    #Find number of months to failure for plotting
    months_after_build = np.sort(universal_view.MONTHS_AFTER_BUILD.unique())

    #Plot MAB Chart!!
    for month in months_after_build:
        plt.plot(df_rph[month], label = month)
    plt.xticks(np.arange(df_rph.shape[0]), df_rph['REL_BUILD_YEAR_MONTH'])
    plt.title("RPH of {} by Build Date".format(issue_name))
    plt.xlabel("Build Month")
    plt.ylabel("RPH")
    plt.legend(title = 'Months After Build')
    write_results_fn(df = plt,
                     path_to_results = path_to_results,
                     issue_name = issue_name,
                     data_folder = "Figs/MAB/",
                     file_end = "MAB_Chart",
                     save_type = "fig")
    plt.close()
    return(True)


def save_numerical_hist(df_no_one_hot_raw, 
                        col, 
                        desc, 
                        view, 
                        ranking, 
                        issue_name,
                        path_to_results,
                        issue_n, 
                        non_issue_n
                        ):

    
    range_1 = df_no_one_hot_raw.query("Label == 1")[col].dropna().max() - \
            df_no_one_hot_raw.query("Label == 1")[col].dropna().min()
            
    range_0 = df_no_one_hot_raw.query("Label == 0")[col].dropna().max() - \
            df_no_one_hot_raw.query("Label == 0")[col].dropna().min()
            
    if range_0 == 0 or range_1 == 0 or np.isnan(range_0) or np.isnan(range_1):
        return
    
    if np.abs(range_1 - range_0) > 10**4:
        fig, ax = plt.subplots(figsize=(12, 8))
        ax.hist(df_no_one_hot_raw.query("Label == 1")[col].dropna(),
        color='red', alpha=0.6, bins = 10, label = 'Issue [{}]'.format(issue_n), normed=True)
        ax.hist(df_no_one_hot_raw.query("Label == 0")[col].dropna(),
        color='blue', alpha=0.6, bins = 10, label = 'No Issue [{}]'.format(non_issue_n), normed=True)
        ax.set(title='Histogram',
           ylabel='Count', xlabel = col)
        plt.legend()
        data_folder = "Figs/" + view + "/"
        write_results_fn(df = plt,
                     path_to_results = path_to_results,
                     issue_name = issue_name,
                     data_folder = data_folder,
                     file_end = "_" + col,
                     save_type = "fig",
                     file_pre = str(ranking) + "_")
        plt.close()
    else:
            
    
        fig, ax = plt.subplots(figsize=(12, 8))
    
        range_both = np.array([range_0, range_1])
        range_min = np.argmin(range_both)
        small_bin = 10
        if range_min == 1:
            hist, bins = np.histogram(df_no_one_hot_raw.query("Label == 1")[col].dropna(), bins = small_bin)
            ax.bar(bins[:-1], 100*hist.astype(np.float32)/ hist.sum(), width=(bins[1]-bins[0]), \
                   color='red', alpha = 0.6, label = 'Issue [{}]'.format(issue_n))
            hist, bins = np.histogram(df_no_one_hot_raw.query("Label == 0")[col].dropna(), bins = int(small_bin*range_0/range_1))
            ax.bar(bins[:-1], 100*hist.astype(np.float32) / hist.sum(), width=(bins[1]-bins[0]), \
                   color='blue', alpha = 0.6, label = 'No Issue [{}]'.format(non_issue_n))
        if range_min == 0:
            hist, bins = np.histogram(df_no_one_hot_raw.query("Label == 1")[col].dropna(), bins = int(small_bin*range_1/range_0))
            ax.bar(bins[:-1], 100*hist.astype(np.float32)/ hist.sum(), width=(bins[1]-bins[0]), \
                   color='red', alpha = 0.6, label = 'Issue [{}]'.format(issue_n))
            hist, bins = np.histogram(df_no_one_hot_raw.query("Label == 0")[col].dropna(), bins = small_bin)
            ax.bar(bins[:-1], 100*hist.astype(np.float32) / hist.sum(), width=(bins[1]-bins[0]), \
                   color='blue', alpha = 0.6, label = 'No Issue [{}]'.format(non_issue_n))
        ax.set(title='Histogram',
               ylabel='Percentage', xlabel = col)
        plt.legend()
        data_folder = "Figs/" + view + "/"
        write_results_fn(df = plt,
                         path_to_results = path_to_results,
                         issue_name = issue_name,
                         data_folder = data_folder,
                         file_end = "_" + col,
                         save_type = "fig",
                         file_pre = str(ranking) + "_")
        plt.close()
        return(True)
   
def save_categorical_plot(df1, 
                          var_name, 
                          var_desc, 
                          view, 
                          ranking, 
                          issue_name,
                          path_to_results,
                          vline = None, 
                          sort = False, 
                          only_sig = False
                          ):
   #Forest Plot for categorical variables 

   dfpp = df1.groupby(var_name).Label.agg({'beta_mode':np.mean, 'ww': np.sum,
                                           'Not-ww': lambda x: np.sum(1-x),
                                           'count': lambda x: np.shape(x)[0]})
   if sort == True:
       dfpp = dfpp.sort_values(by='beta_mode', ascending=False)
      
   names = dfpp.index.values
   
   if only_sig == True:
       sig_names = []
       for i, val in enumerate(names):
           a = dfpp.iloc[i]['ww'] + 1
           b = dfpp.iloc[i]['Not-ww'] + 1
           hpd95 = 100*hdi_of_idcf(beta, credMass=0.95, a=a, b=b)
           if (vline > hpd95[0] and vline > hpd95[1]) or (vline < hpd95[0] and vline < hpd95[1]):
               sig_names.append(val)
       names = sig_names 
       dfpp = dfpp.loc[names]

   p = len(names)
   space = 0.5
   fig, ax = plt.subplots(figsize=(8,(p+1)*0.5))
   for i, val in enumerate(names):
       # parameters of beta distribution
       a = dfpp.iloc[i]['ww'] + 1
       b = dfpp.iloc[i]['Not-ww'] + 1
       
       #a = dfpp.loc[val]['ww'] + 1
       #b = dfpp.loc[val]['Not-ww'] + 1
       # plot most probable value (the mode)
       mode = (a - 1)/(a + b - 2)
       ax.plot( 100*mode, [-space -i*space],  marker='o', markersize=5, color='blue')
       # Plot the High Posterior Density regions
       hpd50 = 100*hdi_of_idcf(beta, credMass=0.50, a=a, b=b)
       hpd95 = 100*hdi_of_idcf(beta, credMass=0.95, a=a, b=b)
       ax.errorbar(x=(hpd50[0],hpd50[1]), y=(-space-i*space,-space-i*space), color='blue', 
                               linewidth= 2)
       ax.errorbar(x=(hpd95[0],hpd95[1]), y=(-space-i*space,-space-i*space), color='blue', 
                       linewidth= 1)
       
       
       
   issue_name_pres = issue_name.replace("_", " ")   
   if vline:
       ax.axvline(vline, color='k', linestyle=':', alpha=0.7, linewidth=1, label='Overall % of {} Issues'.format(issue_name_pres))
   ax.set_ylim(-space*(p+1),0)
   ax.set_xlim(-0.02*100,1.02*100)
   ax.set_yticks(-space-1*np.arange(p)*space)
   names_with_count = [str(names[x]) + ' [' + str(int(dfpp.loc[names[x]]['count'])) + ']' for x in range(len(names))]
   ax.set_yticklabels(names_with_count)
   ax.set_ylabel('{}'.format(var_desc))
   ax.set_xlabel('% of {} Issues'.format(issue_name_pres));
   ax.set_title('% of {} Issues by {}'.format(issue_name_pres, var_name));
   ax.grid(False)
   if vline:
       ax.legend(loc = 0, fontsize = 'medium')
   data_folder = "Figs/" + view + "/"
   write_results_fn(df = plt,
                     path_to_results = path_to_results,
                     issue_name = issue_name,
                     data_folder = data_folder,
                     file_end = "_" + var_desc,
                     save_type = "fig",
                     file_pre = str(ranking) + "_")
   plt.close()
   return(True)
       
       
def hdi_of_idcf(dist_name, 
                credMass=0.95, 
                **args
                ):
    # freeze distribution with given arguments
    distri = dist_name(**args)
    incredMass =  1.0 - credMass

    def intervalWidth(lowTailPr):
        return distri.ppf(credMass + lowTailPr) - distri.ppf(lowTailPr)

    # find lowTailPr that minimizes intervalWidth
    HDIlowTailPr = fmin(intervalWidth, incredMass, ftol=1e-8, disp=False)[0]
    # return interval as array([low, high])
    return(distri.ppf([HDIlowTailPr, credMass + HDIlowTailPr]))
    
def add_image_to_excel(writer,
                       sheet_name,
                       path_to_image,
                       image_name
                       ):
    
    pd.DataFrame().to_excel(writer, sheet_name=sheet_name, index = False)
    
    workbook  = writer.book
    worksheet = writer.sheets[sheet_name]

    # Create a chart object.
    worksheet.insert_image('A1', '{}/{}.png'.format(path_to_image, image_name))
    
    return writer

    

def univ_save_and_plot(path_to_results,
                       path_base,
                       issue_name,
                       data_folder,
                       writer,
                       new_views,
                       df_final_list,
                       df_universal_view,
                       df_no_one_hot_raw,
                       df_oepl_sub,
                       df_vpcr,
                       df_fc
                       ):
    

    writer = add_image_to_excel(writer = writer,
                                sheet_name = '1 UNIVARIATE',
                                path_to_image = '{}/excel_images'.format(path_base),
                                image_name = 'univariate')
    
    sheet_views = ['1.1 Usage (INS & REL)', '1.2 Design (OEPL & VPCR)', '1.3 Manufacturing (MES & EPAT)']

    for i, view in enumerate(sheet_views):    
        df_final_list[i].to_excel(writer, sheet_name = view, index = False)
    
    df_universal_view.to_excel(writer, sheet_name='Universal View', index = False)
    #df_no_one_hot_raw.to_excel(writer, sheet_name = 'All Data', index = False)
    
    # Close the Pandas Excel writer and output the Excel file.
    #writer.save()
    
    
    for j, view in enumerate(new_views):
        df_final = df_final_list[j]
        for i in range(df_final.shape[0]):
            print("processing: " + view + " " + df_final.Feature[i])
            var = df_final.Feature[i]
            if df_final.Type[i] == "Numerical":
                    save_numerical_hist(df_no_one_hot_raw, \
                                        var, \
                                        var, \
                                        view, \
                                        i+1,
                                        issue_name,
                                        path_to_results,
                                        df_no_one_hot_raw[df_no_one_hot_raw.Label==1][var].dropna().count(),
                                        df_no_one_hot_raw[df_no_one_hot_raw.Label==0][var].dropna().count())
            var = var.replace("/", "_")
        
            if df_final.Type[i] == "Categorical":
                if 'OEPL' in df_final.Feature[i]:
                    opt = var.split(":")[1][:2]
                    df_sub = df_oepl_sub.query("OPT_CAT == @opt")
                    df_sub = df_sub.rename(columns = {'OPT_NBR': var.split(":")[1]})
                    save_categorical_plot(df_sub, \
                                        var.split(":")[1], \
                                        var.replace(":", "_"), \
                                        view, \
                                        i+1, \
                                        issue_name, \
                                        path_to_results, \
                                        vline = 100*df_sub.Label.mean())
                elif 'VPCR' in df_final.Feature[i]:
                    save_categorical_plot(df_vpcr, \
                                        'VPCR', \
                                        var.split("_")[3], \
                                        view, \
                                        i+1, \
                                        issue_name, \
                                        path_to_results, \
                                        vline = 100*df_vpcr.Label.mean())
                    
                elif 'FC' in df_final.Feature[i]:
                    fc_cat = var.split(":")[0]
                    if fc_cat != 'FC':
                        fc_cat = fc_cat.split("_", 1)[1]
                    else:
                        continue
                    df_fc_sub = df_fc[df_fc['Fault Code Category'] == fc_cat]
                    save_categorical_plot(df_fc_sub, \
                                        'FC', \
                                        var.replace(":", "_"), \
                                        view, \
                                        i+1, \
                                        issue_name, \
                                        path_to_results, \
                                        vline = 100*df_fc_sub.Label.mean(),
                                        sort = True)
        
                else:
                    save_categorical_plot(df_no_one_hot_raw, \
                                        var.split(":")[0], \
                                        var.replace(":", "_"), \
                                        view, \
                                        i+1, \
                                        issue_name, \
                                        path_to_results, \
                                        vline = 100*df_no_one_hot_raw.Label.mean())
        
    return(writer)
    
#%% ##########################

#%% functions for multivariate analysis####
    
def multivariate_analysis_main(issue_name,
                               path_base,
                               path_to_results,
                               path_processed_raw,
                               mileage_min,
                               path_processed_norm,
                               path_columns_to_remove,
                               df_final_list,
                               numeric_cols, #list of numeric columns
                               writer,
                               columns_to_remove = pd.DataFrame(),
                               df_one_hot_raw = pd.DataFrame(),
                               df_one_hot_norm_reduced = pd.DataFrame(),
                               test_only = False
                               ):
    
    # In simple sense, there is only need of two datasets to do the analysis of multivariate
    # 1. The dataset which has no spurious values, null values have been imputed, numerical columns are normalized
    # 2. The dataset which has no spurious values, null values are not removed, and numerical columns are not normalized
    # Both datasets only have the columns remaining after the first pass removal of columns
    
    # Read in raw data to be used in plots later
    if df_one_hot_raw.size == 0:
        df_one_hot_raw = pd.read_csv("{}{}/Data/{}_one_hot_raw.csv".format(path_to_results, issue_name, issue_name))
        #path_processed_raw = "{}/{}/Data/{}_one_hot_raw.csv".format(path_to_results, issue_name, issue_name)
        #df_one_hot_raw = load_raw_data(path_processed_raw, mileage_min)
        
    df_no_one_hot_raw = pd.read_csv("{}{}/Data/{}_no_one_hot_raw.csv".format(path_to_results, issue_name, issue_name))
    
    non_numeric_cols = np.setdiff1d(df_one_hot_raw.columns.values, numeric_cols)
    
    df_one_hot_raw = pd.concat([df_no_one_hot_raw[numeric_cols], df_one_hot_raw[non_numeric_cols]], axis = 1)
    #normalized_limit = norm_miles(mileage_min = mileage_min, path_processed_raw = path_processed_raw)
    
    if df_one_hot_norm_reduced.size == 0:
        df_one_hot_norm_reduced = pd.read_csv("{}{}/Data/{}_one_hot_norm_reduced.csv".format(path_to_results, issue_name, issue_name))
        #path_processed_norm = "{}/{}/Data/{}_one_hot_norm_reduced.csv".format(path_to_results, issue_name, issue_name)
        #df_one_hot_norm_reduced = load_norm_data(path_processed_norm, -999999)
    
    ## SANJAY UPDATED @Sanjay - this is probably ok, do you just want to kill?
    #df_one_hot_raw = df_one_hot_raw[df_one_hot_norm_reduced.columns].reset_index(drop = True)
    num_cols = df_one_hot_norm_reduced.columns.values[[df_one_hot_norm_reduced.columns.values[x] in numeric_cols for x in range(df_one_hot_norm_reduced.shape[1])]]

    df_one_hot_norm_reduced = df_one_hot_norm_reduced.iloc[df_one_hot_raw.index,:]    
    for col in tqdm(num_cols):
        
        df_one_hot_norm_reduced.iloc[df_one_hot_raw[col][df_one_hot_raw[col].isnull()].index, \
                                                    np.where(df_one_hot_norm_reduced.columns.values == col)[0][0]] = -99999
        
    df_one_hot_norm_reduced = df_one_hot_norm_reduced.fillna(-99999)
    
    # Read in file containing columns to be removed. These are features generated after the engine comes in for fixing, so they aren't useful for diagnosing an issue.
    if columns_to_remove.size == 0:
        #columns_to_remove = pd.read_csv(path_columns_to_remove)['columns_to_remove'].tolist()
        columns_to_remove = pd.DataFrame()
    
    # Determine features in univariate output. Also create a color column which will be used to color the bars in a subsequent bar plot
    all_uni_output = create_all_uni_output(df_final_list = df_final_list)
    
    
    # Remove features not needed
    df_master = remove_unwanted_features(input_data = df_one_hot_norm_reduced, 
                                         exclude_features = ['MES','EPAT'], 
                                         columns_to_remove = columns_to_remove)
    
    if test_only:
        # Initialize the randomforest classifier 
        rf = RandomForestClassifier(random_state = 777, 
                                    n_estimators = 100, 
                                    verbose = True, 
                                    class_weight = "balanced",
                                    min_samples_split = 15)
        
        # See how this is going to perform for our data. This will print the confusion matrix and r-squared
        return(test_model_performance(input_data = df_master, 
                                      id_features = ['ID', 'Label','ESN', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'],
                                      label_column = 'Label', 
                                      classifier = rf, 
                                      cv = 5))
        
        
    writer = add_image_to_excel(writer = writer,
                                sheet_name = '3 MULTIVARIATE',
                                path_to_image = '{}/excel_images'.format(path_base),
                                image_name = 'multivariate')
    
    df_master = df_master.drop(columns = ['ESN', 'EARLIEST_INDICATION_DATE', 'REL_FAILURE_DATE', 'ID'])
    
    res_df_one, writer = univ_out(df_master = df_master, 
                     all_uni_output = all_uni_output, 
                     path_to_results = path_to_results, 
                     writer = writer,
                     issue_name = issue_name, 
                     show_plot = True)
    
    writer = multivar_out(df_master = df_master, 
                         df_one_hot_raw = df_one_hot_raw, 
                         all_uni_output = all_uni_output, 
                         path_to_results = path_to_results, 
                         issue_name = issue_name,
                         numeric_cols = numeric_cols,
                         writer = writer,
                         show_plot = True)
    
    return(df_master, all_uni_output, columns_to_remove, res_df_one, writer)
        
#load data from previous analysis
def load_raw_data(path_processed_raw,
                  mileage_min
                  ):
    # Data which will be used for plots
    df_one_hot_raw = pd.read_csv(path_processed_raw)
    
    # Now filter out engines which are less than 1500 miles. This should technically be done in the Univariate analysis.
    df_one_hot_raw = df_one_hot_raw[(df_one_hot_raw['INS_TI_ENGINE_DISTANCE_MILES'] >= mileage_min) | (df_one_hot_raw['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]
    return(df_one_hot_raw)
    
def load_norm_data(path_processed_norm,
                   normalized_limit
                   ):
    # Data which will go into analysis, which is normalized
    df = pd.read_csv(path_processed_norm)
    df = df[(df['INS_TI_ENGINE_DISTANCE_MILES'] >= normalized_limit) | (df['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]
    return(df)
    
#helper function for normalizatoin to keep consistent
def norm_miles(mileage_min,
               raw_data_df
               ):
    normalized_limit = (mileage_min - raw_data_df['INS_TI_ENGINE_DISTANCE_MILES'].mean())/raw_data_df['INS_TI_ENGINE_DISTANCE_MILES'].std()
    return(normalized_limit)

# Determine features in univariate output. Also create a color column which will be used to color the bars in a subsequent bar plot
def create_all_uni_output(df_final_list
                          ):
    # Read in the output generated by Sanjay at the end of univariate analysis
    
    # Determine features in univariate output. Also create a color column which will be used to color the bars in a subsequent bar plot
    # based on the source from which the feature is obtained
    # It would be useful if the output generated through this next block of code is also generated at the end of the Univariate analysis
    # done by Sanjay. Essentially, instead of having features divided in the excel sheets by Design, Usage and Manuf, for this piece of analysis
    # we just need them all in one sheet. Otherwise we can just keep the below snippet of code as part of the multivariate, to generate 
    # the table we need from the output from Univariate analysis.
    usage = df_final_list[0]
    usage['Source'] = 'Usage'
    design = df_final_list[1]
    design['Source'] = 'Design'
    manuf = df_final_list[2]
    manuf['Source'] = 'Manufacturing'

    all_uni_output = pd.concat([design, manuf, usage], axis = 0)
    all_uni_output = all_uni_output[['Feature','Source']]
    all_uni_output['Color'] = 'blue'
    all_uni_output['Color'][all_uni_output['Source'] == 'Manufacturing'] = 'red'
    all_uni_output['Color'][all_uni_output['Source'] == 'Usage'] = 'green'
    all_uni_output = all_uni_output.rename(columns = {'Feature':'feature'})
    all_uni_output = all_uni_output[['feature', 'Color']]
    return(all_uni_output)

# Write a few functions that can be used for the multivariate analysis
#these need to be commented better
def test_model_performance(input_data, 
                           id_features, 
                           label_column, 
                           classifier, 
                           cv
                           ):
    
    # From the input data, keep only those columns which are actually features for modelling, others which are id columns should be removed
    features = input_data[input_data.columns.difference(id_features)].reset_index(drop = True)
    features = features.dropna(axis = 1, how = 'all') # Remove any columns that are completely NA
    labels = input_data[label_column]
    
    # Try a crossvalidated approach
    preds = cross_val_predict(X = features, y = labels, estimator = classifier, cv = 10)
    
    # Check model performance for crossvalidated approach
    confusion_matrix_data = pd.crosstab(labels, preds, rownames=['True'], colnames=['Predicted'], margins=True)
    
    # Also obtain the crossvalidated r-squared
    r2 = make_scorer(r2_score)
    r2 = cross_val_score(X = features, y = labels, estimator = classifier, cv = 10, scoring = r2).mean()
    return(confusion_matrix_data, r2)


def implement_tree_interpreter_univariate(input_data, 
                                          id_features, 
                                          label_column, 
                                          classifier
                                          ):
    
    # From the input data, keep only those columns which are actually features for modelling, others which are id columns should be removed
    features = input_data[input_data.columns.difference(id_features)].reset_index(drop = True)
    features = features.dropna(axis = 1, how = 'all') # Remove any columns that are completely NA
    labels = input_data[label_column]
    
    # Fir the classifier
    classifier.fit(X = features, y = labels)

    # Implement the tree interpeter univariate. Now here we are predicting on the entire same data that we trained on, but since
    # we are just trying to get the individual contributions of features, it should be okay
    prediction, bias, contributions = ti.predict(classifier, features)
    
    # Take raw contributions by the features
    raw_contri = pd.DataFrame(contributions[:,:,1], columns = features.columns, 
                                  index = features.index)
    # Consider only contributions for the issue observations
    issue_labels = np.where(labels == 1)
    issue_contri = raw_contri.loc[(issue_labels)]
    
    # Generate the table to provide as output
    res_df_one = pd.DataFrame(issue_contri.loc[:,:].mean(), columns = ['contribution'])
    res_df_one['abs_contributions'] = abs(res_df_one['contribution'])
    res_df_one['feature'] = res_df_one.index
    res_df_one['Rank'] = res_df_one['abs_contributions'].rank(method='dense', ascending=False)
    
    return(res_df_one)


def implement_tree_interpreter_multivariate(input_data, 
                                            id_features, 
                                            label_column, 
                                            classifier, 
                                            rf,
                                            numeric_cols #list of numeric columns
                                            ):
    
    # From the input data, keep only those columns which are actually features for modelling, others which are id columns should be removed
    features = input_data[input_data.columns.difference(id_features)].reset_index(drop = True)
    features = features.dropna(axis = 1, how = 'all') # Remove any columns that are completely NA
    labels = input_data[label_column]
    
    # Fir the classifier
    classifier.fit(X = features, y = labels)
    
    # Now set up the tree interpreter for cross contributions, in order to spot most interesting interactions
    prediction, bias, contributions = ti.predict(rf, features, joint_contribution=True)
    aggregated_contributions = utils.aggregated_contribution(contributions)
    
    columns = np.asarray(list(features.columns))
    res = []
    for k in set(aggregated_contributions.keys()):
        res.append(([columns[index] for index in k], 
                    aggregated_contributions.get(k, 0)))  
    
    res = sorted(res, reverse = True)
    res_df = pd.DataFrame(np.array(res), columns = list(["feature_combination","contribution"]))
    res_df['feature_combination'] = res_df['feature_combination'].astype('str') 
    res_df['contribution'] = [el[1] for el in res_df['contribution']]
    res_df['comma_count'] = res_df.feature_combination.str.count(',')
    res_df['abs_contributions'] = res_df.contribution.abs()


    res_df_two = res_df[(res_df['comma_count'] == 1)].sort_values(by = ['abs_contributions'], ascending = False)
    res_df_three = res_df[(res_df['comma_count'] == 2)].sort_values(by = ['abs_contributions'], ascending = False)

    # Clean these contribution dataframes to make them shareable
    res_df_two = clean_contributions_df(contributions_df = res_df_two, feature_column = 'feature_combination', drop_columns = ['feature_combination', 'comma_count'])
    res_df_three = clean_contributions_df(contributions_df = res_df_three, feature_column = 'feature_combination', drop_columns = ['feature_combination', 'comma_count'])

    # The idea will be to take feature_1 and feature_2 columns and detect if they are Numerical or Categorical
    res_df_two.loc[res_df_two.feature_1.isin(numeric_cols), 'feature_1_type'] = 'numerical'
    res_df_two['feature_1_type'].fillna('categorical', inplace = True)
    res_df_two.loc[res_df_two.feature_2.isin(numeric_cols), 'feature_2_type'] = 'numerical'
    res_df_two['feature_2_type'].fillna('categorical', inplace = True)

    res_df_three.loc[res_df_three.feature_1.isin(numeric_cols), 'feature_1_type'] = 'numerical'
    res_df_three['feature_1_type'].fillna('categorical', inplace = True)
    res_df_three.loc[res_df_three.feature_2.isin(numeric_cols), 'feature_2_type'] = 'numerical'
    res_df_three['feature_2_type'].fillna('categorical', inplace = True)
    res_df_three.loc[res_df_three.feature_3.isin(numeric_cols), 'feature_3_type'] = 'numerical'
    res_df_three['feature_3_type'].fillna('categorical', inplace = True)

    res_df_two['Rank'] = res_df_two['abs_contributions'].rank(method='dense', ascending=False)
    res_df_three['Rank'] = res_df_three['abs_contributions'].rank(method='dense', ascending=False)
    
    return(res_df_two, res_df_three)
    
def clean_contributions_df(contributions_df, 
                           feature_column, 
                           drop_columns
                           ):
    column = contributions_df[feature_column]
    
    for i in range(len(column)):
        element = column.iloc[i]
        element = element.replace("'","")
        element = element.replace("[","")
        element = element.replace("]","")
        splits = [x.strip() for x in element.split(',')]
        
        for j in range(len(splits)):
            contributions_df.at[contributions_df.index[i], 'feature_' + str(j+1)] = splits[j]
        
    contributions_df = contributions_df.drop(drop_columns, axis = 1)
    return(contributions_df)


def create_categorical_table(df, 
                             feature1, 
                             feature2, 
                             label_column
                             ):
    #feature1_levels = df[feature1].astype('category').cat.categories.tolist()
    #feature2_levels = df[feature2].astype('category').cat.categories.tolist()
    
    feature1_levels = [0.0, 1.0]
    feature2_levels = [0.0, 1.0]

    cat_df = pd.DataFrame([])
    for i in range(len(feature1_levels)):
        for j in range(len(feature2_levels)):
            cat_df.loc[2*i + j,'non-issue'] = len(df[(df[feature1] == feature1_levels[i]) & (df[feature2] == feature2_levels[j]) & (df['Label'] == 0)].index)
            cat_df.loc[2*i + j,'issue'] = len(df[(df[feature1] == feature1_levels[i]) & (df[feature2] == feature2_levels[j]) & (df['Label'] == 1)].index)
            cat_df.loc[2*i + j,'feature_combination'] = str(feature1).replace(":","_") + '=' + str(feature1_levels[i]) + ' and ' + str(feature2).replace(":","_") + '=' + str(feature2_levels[j])
    
    cat_df = cat_df[['feature_combination', 'non-issue', 'issue']]
    cat_df['sample_size'] = cat_df['non-issue'] + cat_df['issue']
    cat_df['incident_rate'] = cat_df['issue']*100/cat_df['sample_size']
    cat_df = cat_df.replace(np.nan, 0)
        
    return(cat_df)

# It might also help to write a function which takes in the input from the user and plots the chart accordingl
#this is not currently being used - not sure what it does need to figure out or delete
def create_plot(df_one_hot_raw, 
                feature1, 
                feature2, 
                label_column, 
                output_destination):
    
    if(feature1 in numeric_cols):
        feature1_type = 'numeric'
    else:
        feature1_type = 'categorical'
        
    if(feature2 in numeric_cols):
        feature2_type = 'numeric'
    else:
        feature2_type = 'categorical'
    
    if((feature1_type == 'numeric') & (feature2_type == 'numeric')):
        fig, ax = plt.subplots()
        p1 = ax.scatter(df_one_hot_raw.query("Label == 0")[feature1], 
                        df_one_hot_raw.query("Label == 0")[feature2], 
                        c = 'blue', s = 10, marker = 'o')
        p2 = ax.scatter(df_one_hot_raw.query("Label == 1")[feature1], 
                        df_one_hot_raw.query("Label == 1")[feature2], 
                        c = 'red', s = 50, marker = 'X')
    
        count_p1 = sum((df_one_hot_raw.query("Label == 0")[feature1].notnull()) & (df_one_hot_raw.query("Label == 0")[feature2].notnull()))
        count_p2 = sum((df_one_hot_raw.query("Label == 1")[feature1].notnull()) & (df_one_hot_raw.query("Label == 1")[feature2].notnull()))
    

        plt.legend((p1, p2), ('Non-issue ' + '(Count:' + str(count_p1) + ')', 'Issue ' + '(Count:' + str(count_p2) + ')'), frameon = True)
        plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
        plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
        
        write_results_fn(df = plt,
                          path_to_results = output_destination + "/",
                          issue_name = "",
                          data_folder = "",
                          file_end = "output_plot",
                          save_type = "fig")
        plt.close()
        
    elif((feature1_type == 'categorical') & (feature2_type == 'categorical')):
        df[feature1] = df[feature1].astype('category')
        df[feature2] = df[feature2].astype('category')
        cat_df = create_categorical_table(df = df_one_hot_raw, feature1 = feature1, feature2 = feature2, label_column = 'Label')
    
        x_avg = len(df_one_hot_raw[df_one_hot_raw['Label'] == 1])*100/df_one_hot_raw.shape[0]
        fig, ax = plt.subplots()
        p1 = ax.barh(cat_df['feature_combination'], cat_df['incident_rate'], color = "red", alpha = 0.5)
    
        for j in range(cat_df.shape[0]):
            ax.text(cat_df.loc[j,'incident_rate'] + 0.1, j, 'N: ' + str(int(cat_df.loc[j,'sample_size'])), color='black', fontweight='bold')
    
        ax.set_xlabel('Incident Rate (%)')
        plt.axvline(x = x_avg, linestyle = 'dashed')
        plt.text(x_avg + 0.1,2.2,'average incident rate', rotation = 90)
        write_results_fn(df = plt,
                          path_to_results = output_destination + "/",
                          issue_name = "",
                          data_folder = "",
                          file_end = "output_plot",
                          save_type = "fig")
        plt.close()
        
    else:
        df[feature2] = df[feature2].astype('category')
        sns.set_style("darkgrid")
        df_one_hot_raw_subset = df_one_hot_raw.loc[(df_one_hot_raw[feature1].notnull()) & (df_one_hot_raw[feature2].notnull()), :]
        ax = sns.boxplot(x= feature1, y= feature2, data = df_one_hot_raw_subset, hue = "Label", 
                         palette = ['blue','red'], saturation = 0.5, fliersize = 2)
        handles, labels = ax.get_legend_handles_labels()
        labels = ['Non-issue', 'Issue']
        plt.legend(handles[0:2], labels[0:2], frameon = True)
        medians = df_one_hot_raw_subset.groupby([feature1,'Label'])[feature2].median().values.tolist()
    
        nobs = []
        for k in df_one_hot_raw_subset[feature1].astype('category').cat.categories.tolist():
        
            if (len(df_one_hot_raw_subset[(df_one_hot_raw_subset[feature1] == k) & (df_one_hot_raw_subset[feature2].notnull())]['Label'].astype('category').cat.categories.tolist()) == 2):
                counts = df_one_hot_raw_subset[(df_one_hot_raw_subset[feature1] == k) & (df_one_hot_raw_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
                nobs.extend(counts)
            elif ((df_one_hot_raw_subset[(df_one_hot_raw_subset[feature1] == k) & (df_one_hot_raw_subset[feature2].notnull())]['Label'].astype('category').cat.categories.tolist()[0] == 0)):
                counts = df_one_hot_raw_subset[(df_one_hot_raw_subset[feature1] == k) & (df_one_hot_raw_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
                counts.append(0)
                nobs.extend(counts)
            else:
                counts = [0]
                element = df_one_hot_raw_subset[(df_one_hot_raw_subset[feature1] == k) & (df_one_hot_raw_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
                counts.extend(element)
                nobs.extend(counts)
    
        x = [i for i, e in enumerate(nobs) if e == 0]
        for p in x:
            medians.insert(p,0.0)

        pos = list([-0.2, 0.2, 0.8,1.2])
  
        for j in range(len(nobs)):
            ax.text(pos[j], medians[j] + 0.05, 'N: ' + str(nobs[j]),
                    horizontalalignment='center', size= "medium", color='black', weight='bold')
        
        plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
        plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
        write_results_fn(df = plt,
                          path_to_results = output_destination + "/",
                          issue_name = "",
                          data_folder = "",
                          file_end = "output_plot",
                          save_type = "fig")
        plt.close()
    
def univ_out(df_master,
             all_uni_output,
             path_to_results,
             issue_name,
             writer,
             show_plot = True,
             test_only = False):
    
    # Initialize the randomforest classifier 
    rf = RandomForestClassifier(random_state = 777, n_estimators = 100, verbose = True, class_weight = "balanced",
                                n_jobs = 8)
    
    res_df_one = implement_tree_interpreter_univariate(input_data = df_master, id_features = ['ID', 'Label','ESN', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'], 
                                      label_column = 'Label', classifier = rf)
    
    output_folder_mk(path_to_results = path_to_results, 
                     issue_name = issue_name,
                     make_type = "univariate")
    

    
    

    
    if(show_plot):
        # Generate the plot for this univariate analysis. The idea is to also have the bars color coded based on their source
        contributions = res_df_one
        contributions = contributions.nlargest(n = 100, columns = 'abs_contributions')
        contributions = pd.merge(contributions, all_uni_output[['feature', 'Color']], on = ['feature'])
        contri_list = pd.DataFrame(contributions.sort_values(by = 'contribution',ascending = True)['feature'].tolist(), columns = ["feature"])
        contri_list['order'] = contri_list.index
        
        contributions = pd.merge(contributions, contri_list, how = 'inner', on = ['feature'])
        contributions = contributions.sort_values(by = "order")
        contributions.index = contributions['feature']
        color_order = contributions['Color'].tolist()
        contributions = contributions.loc[:,'contribution']
        plt.figure(figsize = (30,contributions.shape[0]*0.25))
        ((contributions)*100).sort_values(ascending=True).plot(kind='barh', color = color_order)
        plt.ylabel('Feature Name')
        plt.xlabel('contribution (%)')
        custom_legend = [Line2D([0], [0], color='b', lw=6),
                        Line2D([0], [0], color='g', lw=6),
                        Line2D([0], [0], color='r', lw=6)]
        plt.legend(custom_legend,['Design', 'Usage','Manufacturing'], loc = 'center right', markerscale = 50, fontsize = 'large', frameon = True)
        write_results_fn(df = plt,
                         path_to_results = path_to_results,
                         issue_name = issue_name,
                         data_folder = "univariate_results/",
                         #file_end = "_" + str(datetime.date.today()).replace("-","") + "_univariate_contributons",
                         file_end = "_univariate_contributons",
                         save_type = "fig"
                         )
        plt.close()
        
    res_df_one = res_df_one.sort_values(by = 'abs_contributions', ascending = False)

    res_df_one = res_df_one[['Rank', 'feature', 'contribution', 'abs_contributions']]
    
    res_df_one = res_df_one.rename(columns = {'feature': 'Feature', 'contribution': 'Contribution', 'abs_contributions': 'Absolute Contributions'})
    res_df_one.to_excel(writer, sheet_name='3.1 Univariate', index = False)
    
        
    return res_df_one, writer
    
def multivar_out(df_master,
                 df_one_hot_raw,
                 all_uni_output,
                 path_to_results,
                 issue_name,
                 numeric_cols, #list of numeric columns
                 writer,
                 show_plot = True):
    
    # Initialize the randomforest classifier 
    rf = RandomForestClassifier(random_state = 777, n_estimators = 100, verbose = True, class_weight = "balanced",
                                n_jobs = 8, min_samples_split = 15)
    
    output_folder_mk(path_to_results = path_to_results, 
                     issue_name = issue_name,
                     make_type = "multivariate")
    
    # Generate the table outputs from multivariate analysis
    res_df_two, res_df_three = implement_tree_interpreter_multivariate(input_data = df_master, 
                                                                       id_features = ['ID', 'Label','ESN', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'],
                                                                       label_column = 'Label', 
                                                                       classifier = rf, 
                                                                       rf = rf,
                                                                       numeric_cols = numeric_cols)
    
    # Now the idea is to pick these top combination of features and see if there is something interesting going on
    # Pick up all two way interactions between features and categorize them into num x mum, cat x num, or cat x cat
    # Then generate plots based on the type of feature combination that exists
    
    # categorical x categorical
    # The results will be a stacked bar plot and a table which shows the same info along with a couple other features like sample size and
    # failure rate
    
    catxcat = res_df_two[(res_df_two['feature_1_type'] == 'categorical') & (res_df_two['feature_2_type'] == 'categorical')]
    
    for i in range(catxcat.shape[0]):
    
        feature1 = catxcat['feature_1'].tolist()[i]
        feature2 = catxcat['feature_2'].tolist()[i]
        cat_df = create_categorical_table(df = df_one_hot_raw, feature1 = feature1, feature2 = feature2, label_column = 'Label')
        
        x_avg = len(df_one_hot_raw[df_one_hot_raw['Label'] == 1])*100/df_one_hot_raw.shape[0]
        fig, ax = plt.subplots()
        p1 = ax.barh(cat_df['feature_combination'], cat_df['incident_rate'], color = "red", alpha = 0.5)
        
        for j in range(cat_df.shape[0]):
            ax.text(cat_df.loc[j,'incident_rate'] + 0.1, j, 'N: ' + str(int(cat_df.loc[j,'sample_size'])), color='black', fontweight='bold')
        
        ax.set_xlabel('Incident Rate (%)')
        plt.axvline(x = x_avg, linestyle = 'dashed')
        plt.text(x_avg + 0.1,2.2,'average incident rate', rotation = 90)
        file_end = "_" + 'Rank' + str(catxcat['Rank'].tolist()[i]) + '_' + issue_name + '_table_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") 
        write_results_fn(df = plt,
                         path_to_results = path_to_results,
                         issue_name = issue_name,
                         data_folder = "multivariate_results/catxcat/",
                         file_end = file_end,
                         save_type = "fig")
        plt.close()
        
        file_end = "_" + 'Rank' + str(catxcat['Rank'].tolist()[i]) + '_' + issue_name + '_table_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_") 
        write_results_fn(df = cat_df,
                     path_to_results = path_to_results,
                     issue_name = issue_name,
                     data_folder = "multivariate_results/catxcat/",
                     file_end = file_end)
        
    # numerical vs numerical
    # This will just be a scatter plot
    numxnum = res_df_two[(res_df_two['feature_1_type'] == 'numerical') & (res_df_two['feature_2_type'] == 'numerical')]
    
    for i in range(numxnum.shape[0]):
        feature1 = numxnum['feature_1'].tolist()[i]
        feature2 = numxnum['feature_2'].tolist()[i]
        
        fig, ax = plt.subplots()
        p1 = ax.scatter(df_one_hot_raw.query("Label == 0")[feature1], 
               df_one_hot_raw.query("Label == 0")[feature2], 
               c = 'blue', s = 10, marker = 'o')
        p2 = ax.scatter(df_one_hot_raw.query("Label == 1")[feature1], 
               df_one_hot_raw.query("Label == 1")[feature2], 
               c = 'red', s = 50, marker = 'X')
        
        count_p1 = sum((df_one_hot_raw.query("Label == 0")[feature1].notnull()) & (df_one_hot_raw.query("Label == 0")[feature2].notnull()))
        count_p2 = sum((df_one_hot_raw.query("Label == 1")[feature1].notnull()) & (df_one_hot_raw.query("Label == 1")[feature2].notnull()))
        
    
        plt.legend((p1, p2), ('Non-issue ' + '(Count:' + str(count_p1) + ')', 'Issue ' + '(Count:' + str(count_p2) + ')'), frameon = True)
        plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
        plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
        file_end = "_" + 'Rank' + str(numxnum['Rank'].tolist()[i]) + '_' + issue_name + '_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_")
        write_results_fn(df = plt,
                         path_to_results = path_to_results,
                         issue_name = issue_name,
                         data_folder = "multivariate_results/numxnum/",
                         file_end = file_end,
                         save_type = "fig")
        plt.close()
    
    
    # numerical vs categorical
    # This can be a violin plot or a boxplot
    # Generate first all the boxplots
    
    catxnum = res_df_two[((res_df_two['feature_1_type'] == 'numerical') & (res_df_two['feature_2_type'] == 'categorical')) | ((res_df_two['feature_1_type'] == 'categorical') & (res_df_two['feature_2_type'] == 'numerical')) ]
    
    for i in range(catxnum.shape[0]):
        if(catxnum['feature_1_type'].tolist()[i] == 'numerical'):
            feature2 = catxnum['feature_1'].tolist()[i]
            feature1 = catxnum['feature_2'].tolist()[i]
        else:
            feature1 = catxnum['feature_1'].tolist()[i]
            feature2 = catxnum['feature_2'].tolist()[i]
        
        
        sns.set_style("darkgrid")
        df_one_hot_raw_subset = df_one_hot_raw.loc[(df_one_hot_raw[feature1].notnull()) & (df_one_hot_raw[feature2].notnull()), :]
        ax = sns.boxplot(x= feature1, y= feature2, data = df_one_hot_raw_subset, hue = "Label", 
                         palette = ['blue','red'], saturation = 0.5, fliersize = 2)
        handles, labels = ax.get_legend_handles_labels()
        labels = ['Non-issue', 'Issue']
        plt.legend(handles[0:2], labels[0:2], frameon = True)
        medians = df_one_hot_raw_subset.groupby([feature1,'Label'])[feature2].median().values.tolist()
        
        nobs = []
        for k in df_one_hot_raw_subset[feature1].astype('category').cat.categories.tolist():
            
            if (len(df_one_hot_raw_subset[(df_one_hot_raw_subset[feature1] == k) & (df_one_hot_raw_subset[feature2].notnull())]['Label'].astype('category').cat.categories.tolist()) == 2):
                counts = df_one_hot_raw_subset[(df_one_hot_raw_subset[feature1] == k) & (df_one_hot_raw_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
                nobs.extend(counts)
            elif ((df_one_hot_raw_subset[(df_one_hot_raw_subset[feature1] == k) & (df_one_hot_raw_subset[feature2].notnull())]['Label'].astype('category').cat.categories.tolist()[0] == 0)):
                counts = df_one_hot_raw_subset[(df_one_hot_raw_subset[feature1] == k) & (df_one_hot_raw_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
                counts.append(0)
                nobs.extend(counts)
            else:
                counts = [0]
                element = df_one_hot_raw_subset[(df_one_hot_raw_subset[feature1] == k) & (df_one_hot_raw_subset[feature2].notnull())]['Label'].value_counts().values.tolist()
                counts.extend(element)
                nobs.extend(counts)
        
        x = [i for i, e in enumerate(nobs) if e == 0]
        for p in x:
            medians.insert(p,0.0)
    
        pos = list([-0.2, 0.2, 0.8,1.2])
      
        for j in range(len(nobs)):
            ax.text(pos[j], medians[j] + 0.05, 'N: ' + str(nobs[j]),
                    horizontalalignment='center', size= "medium", color='black', weight='bold')
            
        plt.xlabel(str(feature1).replace(":","_"), fontsize=9)
        plt.ylabel(str(feature2).replace(":","_"), fontsize=9)
        file_end = "_" + 'Rank' + str(catxnum['Rank'].tolist()[i]) + '_' + issue_name + '_' + str(feature1).replace(":","_") + '_&_' + str(feature2).replace(":","_")
        write_results_fn(df = plt,
                         path_to_results = path_to_results,
                         issue_name = issue_name,
                         data_folder = "multivariate_results/catxnum/",
                         file_end = file_end,
                         save_type = "fig")
        plt.close()
        
        res_df_two = res_df_two[['Rank', 'feature_1', 'feature_2', 'feature_1_type', \
                             'feature_2_type', 'contribution', 'abs_contributions']]
    res_df_two = res_df_two.rename(columns = {'feature_1': 'Feature 1', 'feature_2': 'Feature 2', \
                                 'feature_1_type': 'Feature 1 Type', 'feature_2_type': 'Feature 2 Type', \
                                 'contribution': 'Contribution', 'abs_contributions': 'Absolute Contributions'})
    
    res_df_two.to_excel(writer, sheet_name='3.2 Bivariate', index = False)
    
    res_df_three = res_df_three[['Rank', 'feature_1', 'feature_2', 'feature_3', \
                               'feature_1_type', 'feature_2_type', 'feature_3_type', \
                               'contribution', 'abs_contributions']]
    res_df_three = res_df_three.rename(columns = {'feature_1': 'Feature 1', 'feature_2': 'Feature 2', 'feature_3': 'Feature 3', \
                                 'feature_1_type': 'Feature 1 Type', 'feature_2_type': 'Feature 2 Type', 'feature_3_type': 'Feature 3 Type', \
                                 'contribution': 'Contribution', 'abs_contributions': 'Absolute Contributions'})

    res_df_three.to_excel(writer, sheet_name='3.3 Trivariate', index = False)
    
    return writer
        
#%% ##########################

#%% functions confounder analysis####
"""
--- AIM ---

The aim of this piece of code is to find the top confounding variables after the first pass selection of features.

--- PROCESS ---

Based on the existing work done so far, different processes will be followed for finding top confounding variables based on the 
type of the features being compared.

1. Categorical vs categorical: Cramer's V value will be used in order to find the top confounded variables. The higher the value,
the higher the confounding.
- Another way of doing that is to build a logistic regression model, with each categorical feature being used as the response variable
turn by turn. The independed features with high p-values can be flagged as the confounders.

2. Numerical vs. numerical: Build a linear/ridge regression model, by taking each independent variables as the dependent variable
turn by turn, and use the 
    
3. Categorical vs. numerical: Build a logistic regression, taking each categorical feature one by one as the response variable, and
all the numerical features as the independent variables, and use the odds ratio to find top confounding features

--- HOW TO READ THIS CODE ---

This code has the following sections:
1. Initial section reads in the files needed. While you do this, also run the code called 'num_cat_cols.py'. That is basically just
the list of numerical and categorical columns for this dataset.

2. Perform the data alterations needed in order to do the analysis, for example select engines that have run above 1500 miles, 
remove categorical features having no variance (all zeros or ones) etc. Ideally all this should be done in the first pass feature selection.

3. Do the confounding analysis for cat vs cat, cat vs num, and num vs num.

"""
def confounding_analysis_main(issue_name,
                              path_to_results,
                              path_base,
                              cat_cols, #column names for categorical data
                              numeric_cols, #list of numeric columns
                              uv_cols, #list of columns for universal views
                              num_cores,
                              writer,
                              uni_output,
                              path_fault_look = "",
                              path_oepl_desc = "",
                              path_engine_options = "",
                              df_fc = pd.DataFrame(),
                              df_oepl_sub = pd.DataFrame(),
                              oepl_desc_df = pd.DataFrame(),
                              df_one_hot_norm_reduced = pd.DataFrame(),
                              df_no_one_hot_raw = pd.DataFrame(),
                              df_one_hot_raw = pd.DataFrame(),
                              plot_only = False,
                              read_date = str(datetime.date.today()).replace("-","")
                              ):
    
    # make folder for results
    if not plot_only:
        output_folder_mk(path_to_results = path_to_results, 
                         issue_name = issue_name,
                         make_type = "confounding")
    
    #set today's date for saving files
    today = str(datetime.date.today()).replace("-","")
    
    # Load the top variables data, and filter out all categorical variables
    # For this we basically need the data for the EGR cooler issue, which has only variables deemed as necessary after first pass removal
    # of features
    
    ## Load Data ##
    #read in some data
    if df_one_hot_norm_reduced.size == 0:
        df_one_hot_norm_reduced = pd.read_csv("{}{}/Data/{}_one_hot_norm_reduced.csv".format(path_to_results, issue_name, issue_name))

    # Data which will be used for plots
    if df_no_one_hot_raw.size == 0:    
        df_no_one_hot_raw = pd.read_csv("{}{}/Data/{}_no_one_hot_raw.csv".format(path_to_results, issue_name, issue_name))
    
    # Data which will be used for plots
    if df_one_hot_raw.size == 0:        
        df_one_hot_raw = pd.read_csv("{}{}/Data/{}_one_hot_raw.csv".format(path_to_results, issue_name, issue_name))
    
    if df_fc.size == 0:
        df_fc = get_fault_code(df_one_hot_raw = df_one_hot_raw, 
                               path_fault_look = path_fault_look)
    
    if df_oepl_sub.size == 0: 
        if oepl_desc_df.size == 0:
            #load oepl descriptions
            oepl_desc_df = load_options_data(path_oepl_desc = path_oepl_desc)
            
        df_oepl_sub = get_oepl(path_engine_options = path_engine_options,
                               df_one_hot_raw = df_one_hot_raw,
                               oepl_desc_df = oepl_desc_df)
        
    
    
    # Read in file for columns to remove (they are features generated after the engine has come into the shop for fixing, they aren't useful drivers for analyzing the failure)
    #columns_to_remove = pd.read_csv("Downloads/Solve/OneDrive_1_3-11-2018/columns_to_remove.csv")['columns_to_remove'].tolist()
    # Also read in the features with their importance score from the univariate analysis, in order to provide a rank against them
    
    usage = uni_output[0]
    design = uni_output[1]
    manuf = uni_output[2]
    
    """
    CATEGORICAL VS. CATEGORICAL
    """
    
    # Do CATEGORICAL VS. CATEGORICAL USING CRAMER'S V
    # Define the function for calculating the Cramer's V value for each pair of categorical variables here
    
    results_df = pd.concat([design, manuf, usage])
    
    results_df = results_df.sort_values(by = 'Combined Significance', ascending = False)
    #num_results_df = pd.concat([design_num, manuf_num, usage_num])
    
    #results_df = results_df.sort_values(by = 'Chi-Sq Test').reset_index(drop = True)
    #num_results_df = num_results_df.sort_values(by = 'Anderson-Darling').reset_index(drop = True)
    
    results_df['Rank'] = np.arange(1, results_df.shape[0]+1)
    #num_results_df['Rank'] = np.arange(1, num_results_df.shape[0]+1)
    
    
    #all_uni_output = pd.concat([design, manuf, usage], axis = 0)
    #all_uni_output['Rank'] = all_uni_output['Importance Score'].rank(method='dense', ascending=False)
    
    # Now filter out engines which are less than 1500 miles
    #normalized_limit = (1500 - df_one_hot_raw['INS_TI_ENGINE_DISTANCE_MILES'].mean())/df_one_hot_raw['INS_TI_ENGINE_DISTANCE_MILES'].std()
    #df_one_hot_raw = df_one_hot_raw[(df_one_hot_raw['INS_TI_ENGINE_DISTANCE_MILES'] >= 1500) | (df_one_hot_raw['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]
    #df_one_hot_norm_reduced = df_one_hot_norm_reduced[(df_one_hot_norm_reduced['INS_TI_ENGINE_DISTANCE_MILES'] >= normalized_limit) | (df_one_hot_norm_reduced['INS_TI_ENGINE_DISTANCE_MILES'].isnull())]
    
    # Remove some columns which are amount columns and are not needed
    # Also remove columns about ESN, Earliest Indication Date, Label etc.
    #columns_to_remove.extend(['EARLIEST_INDICATION_DATE', 'ESN', 'Label','INCDT_ISSUE_NUMBER'])
    df_one_hot_norm_reduced = df_one_hot_norm_reduced.drop(['EARLIEST_INDICATION_DATE', 'ESN', 'Label','INCDT_ISSUE_NUMBER'], axis = 1)
    
    """
    IMP NOTE: The list 'numeric_cols' (below) is a list of columns which are numerical in nature. The list has been created by Sanjay. Because of the tactical, ad-hoc
    changes that kept on happening in the course of the study, this list kept on changing, so the list maintained by Sanjay and me might be
    slightly different. This might create errors. So a good thing to invest time in is to create a finalized version of this list. 
    """
    __, __, __, numeric_cols, __ = get_column_types(df_oepl = "", cat_cols = cat_cols, numeric_cols = numeric_cols, uv_cols = uv_cols, get_oepl = False)
    
    # Remove columns which are completely zero throughout or is equal to the number of rows, because that basically means it also 
    # has no variance at all
    #cat_df = cat_df.loc[:,cat_df.sum() != 0]
    #cat_df = cat_df.loc[:,cat_df.sum() != cat_df.shape[0]]
    
    # Generate dataframe of just numerical variables, all normalized, nulls imputed, and spurious values removed
    num_data = df_one_hot_raw.loc[:,df_one_hot_raw.columns.isin(numeric_cols)]
    
    # There should also be an attempt made at running this analysis by removing all the missing values instead of imputing them
    # df_one_hot_raw here is the entire dataset with the missing values as well
    # Keep only the columns of df_one_hot_raw which are in the dataset 'num_data', as those are the ones reminaing after first pass filtering, and are numerical
    
    num_data_raw = df_one_hot_raw.loc[:,df_one_hot_raw.columns.isin(num_data.columns.tolist())]
    # We cannot drop all the nulls, as that might need up dropping all the rows
    # So first lets see how many nulls each column has and choose ones which have less than 55% missing data
    # This number 50% was came to by some trials, trying to not remove too many columns
    
    #num_data_raw = num_data_raw.loc[:,(num_data_raw.isnull().sum()/num_data_raw.shape[0] <= 0.5)]
    #num_data_raw = num_data_raw.dropna(how = "any")
    
    
    # Keep only categorical variables from this data
    # numeric_cols here is the big list of features, given by Sanjay
    cat_df = df_one_hot_norm_reduced[df_one_hot_norm_reduced.columns.difference(numeric_cols)]
    
    writer = add_image_to_excel(writer = writer,
                                sheet_name = '2 CONFOUNDING',
                                path_to_image = '{}/excel_images'.format(path_base),
                                image_name = 'confounding')
    
    if plot_only:
        data_folder = "confounding_results/"
        file_end = "_cat_x_cat_" + today
        df_out = pd.read_csv("{}{}/{}{}{}.csv".format(path_to_results, issue_name, data_folder, issue_name, file_end))
        
    else:
        df_out = run_cat_and_save(cat_df = cat_df,
                                  results_df = results_df,
                                  num_cores = num_cores,
                                  n = 5)
        df_out = get_vif_for_cat(df_out = df_out,
                                 cat_df = cat_df)
        
        df_out = df_out.reset_index(drop = True)
    
        
        df_out.to_excel(writer, sheet_name='2.1 Cat-Cat', index = False)

        
    
    plot_all_heatmaps(issue_name = issue_name, 
                      path_to_results = path_to_results,
                      cat_df = cat_df,
                      df_fc = df_fc,
                      df_out = df_out,
                      df_oepl_sub = df_oepl_sub
                      )
    
    """
    NUMERICAL VS. NUMERICAL
    """
    if plot_only:
        file_end = "_num_x_num_" + today
        data_folder = "confounding_results/"
        df_out = pd.read_csv("{}{}/{}{}{}.csv".format(path_to_results, issue_name, data_folder, issue_name, file_end))
        
    else:
        corr_and_num = Parallel(n_jobs = num_cores)(delayed(run_num_v_num)(num_data, col, j) \
                           for j, col in enumerate(num_data.columns.values))
        
        corr_df = [corr_and_num[x][0] for x in range(len(corr_and_num))]
        
        num_intersect_df = [corr_and_num[x][1] for x in range(len(corr_and_num))]
        
        df_out = wide_to_long(corr_df = corr_df,
                              results_df = results_df,
                              num_data = num_data,
                              num_intersect_df = num_intersect_df)
        
        
        df_out = get_vif_for_num(df_out = df_out,
                                 num_data = num_data)
        
        df_out = df_out.reset_index(drop = True)
                
        #file_end = "_num_x_num_" + today
        
        df_out.to_excel(writer, sheet_name='2.2 Num-Num', index = False)
        
    #for col in tqdm(duration_cols):
    #    df_no_one_hot_raw[col] = -1*df_no_one_hot_raw[col]
    
    plot_all_pair_plots(issue_name = issue_name,
                        path_to_results = path_to_results,
                        df_one_hot_raw = df_no_one_hot_raw,
                        df_out = df_out)
    
    """
    CATEGORICAL VS. NUMERICAL
    """
    
    # One way to find top numerical features confounded to a categorical feature will be a logistic regression.
    # Each categorical feature will be taken as a response variable one by one, and all the numerical features can be the independent features
    
    # cat_df and num_data are the two datasets we deal with. They have been created in the code above, and their index are aligned
    
    #if plot_only:
    #    print("no plots for CATEGORICAL VS. NUMERICAL at the moment")
        #file_end = "_cat_x_num_" + today
        #data_folder = "confounding_results/"
        #df_out = pd.read_csv("{}{}/{}{}{}.csv".format(path_to_results, issue_name, data_folder, issue_name, file_end))
    #else:
        #df_out = run_anova_and_save(num_data = num_data,
        #                            cat_df = cat_df,
        #                            results_df = results_df,
        #                            num_cores = num_cores, 
        #                            n = 5)
            # Get the result out in a csv
            
        #df_out.to_excel(writer, sheet_name='2.3 Cat-Num', index = False)

    
    return(writer)
    
def cramers_corrected_stat(confusion_matrix):
    
    #Calculate Cramers V statistic for categorial-categorial association
    #uses bias correction from Bergsma and Wicher, 
    #https://en.wikipedia.org/wiki/Cram%C3%A9r%27s_V

    chi2 = chi2_contingency(confusion_matrix)[0]
    n = confusion_matrix.sum().sum()
    phi2 = chi2/n
    r,k = confusion_matrix.shape
    phi2corr = max(0, phi2 - ((k-1)*(r-1))/(n-1))    
    rcorr = r - ((r-1)**2)/(n-1)
    kcorr = k - ((k-1)**2)/(n-1)
    
    return np.sqrt(phi2corr / min( (kcorr-1), (rcorr-1)))

def run_cat_v_cat(cat_df,
                  col1, 
                  i):
    print("Processing: " + "run_cat_v_cat" + " " + str(i))
    cv_list = np.zeros(len(cat_df.columns))
    for i, col2 in enumerate(cat_df.columns):
        if col1 != col2:
            confusion_matrix = pd.crosstab(cat_df[col1], cat_df[col2])
            if confusion_matrix.shape == (2,2):
                cv = cramers_corrected_stat(confusion_matrix)
            else:
                cv = np.nan
        else:
            cv = np.nan
        cv_list[i] = cv
    return cv_list

def run_cat_and_save(cat_df,
                     results_df,
                     num_cores,
                     n = 5):
        
    #cv_df = Parallel(n_jobs = num_cores)(delayed(run_cat_v_cat)(cat_df, col, i) for i, col in enumerate(cat_df.columns.values))
    cv_df = Parallel(n_jobs = num_cores)(delayed(run_cat_v_cat)(cat_df, col, i) for i, col in enumerate(cat_df.columns.values))
    
    cv_df = np.vstack(cv_df)
    
    cramerv_df = pd.DataFrame(cv_df, columns = cat_df.columns.values, index = cat_df.columns.values)
    
    # Now move across each row and pick top features confounded for each row value, but only in cases where the Cramer's V value is greater than 0.2
    
    arr = cramerv_df.values
    index_names = cramerv_df.index
    col_names = cramerv_df.columns
    
    R,C = np.where(np.triu(arr,1)>0.2)
    
    
    out_arr = np.column_stack((index_names[R],col_names[C],arr[R,C]))
    df_out = pd.DataFrame(out_arr,columns=['Feature','Top Confounded Feature','Cramers V value'])
    
    # Now make the output in such a manner which can be presented as a result, by adding ranks and sorting based on the feature rank
    df_out = pd.merge(df_out, results_df[['Feature','Rank']], how = 'left', left_on = 'Feature', right_on = 'Feature')
    df_out = df_out.rename(columns = {'Rank' : 'Feature rank'})
    df_out = pd.merge(df_out, results_df[['Feature','Rank']], how = 'left', left_on = 'Top Confounded Feature', right_on = 'Feature')
    df_out = df_out.rename(columns = {'Rank' : 'Confounded Feature Rank', 'Feature_x': 'Feature', 'Cramers V value': 'Confounding Factor'})
    df_out = df_out.drop(['Feature_y'], axis = 1)
    
    df_out = df_out.sort_values(by = ['Feature rank', 'Confounded Feature Rank'])
    df_out = df_out.groupby('Feature').head(5)
    
    df_out = df_out[~np.isnan(df_out['Feature rank'])]
    
    # Reorder columns
    df_out = df_out.loc[:,['Feature', 'Feature rank', 'Top Confounded Feature', 'Confounded Feature Rank', 'Confounding Factor']]
    
        
    return df_out

def get_vif_for_cat(df_out,
                    cat_df,
                    ):

    def pseudo_r_squared_logit(i):
        logit = sm.Logit(all_data.iloc[:,i], all_data.iloc[:, all_data.columns.values != all_data.columns.values[i]])
        logit = logit.fit(method='bfgs', disp = False)
        return(1/(1-logit.prsquared))
    
    df_out['VIF'] = float('NaN')
    df_out['Highest_VIF'] = 0
    unique_features = df_out.Feature.unique()
    for col in unique_features:
        confounded_cols = df_out.query("Feature == @col")['Top Confounded Feature'].values
        all_cols = np.append(confounded_cols, col)
        all_data = cat_df[all_cols].dropna()
        try:
            vif_vals = [pseudo_r_squared_logit(i) for i in range(all_data.shape[1])]
        except:
            vif_vals = np.repeat(0, all_data.shape[1])
        df_out.loc[df_out['Feature'] == col, 'VIF'] = vif_vals[:len(vif_vals)-1]
        max_val = df_out[df_out['Feature'] == col]['VIF'].max()
        df_out.loc[(df_out['Feature'] == col) & (df_out['VIF'] == max_val),'Highest_VIF'] = 1
    df_out = df_out.drop(columns = ['VIF'])

        
    return df_out

def plot_heatmap(issue_name,
                 path_to_results,
                 df_sub_col1,
                 df_sub_col2,
                 cat_df,
                 i, 
                 col1, 
                 col2): 
    
    df_one_hot_norm_reduced = pd.DataFrame(0, columns = df_sub_col1['COL1'].unique(), index = df_sub_col2['COL2'].unique())
        
    for j in df_one_hot_norm_reduced.index:
        for k in df_one_hot_norm_reduced.columns:
            col_j = cat_df.columns.values[[str(j) in cat_df.columns.values[x] for x in range(cat_df.shape[1])]][0]
            col_k = cat_df.columns.values[[str(k) in cat_df.columns.values[x] for x in range(cat_df.shape[1])]][0]
            
            if col_j == col_k:
                df_one_hot_norm_reduced.loc[j,k] = 1
            else:
                cross_tab = pd.crosstab(cat_df[col_j], cat_df[col_k])
                if cross_tab.shape == (2,2):
                    cross_tab = cross_tab/(cross_tab.sum().sum())
                    df_one_hot_norm_reduced.loc[j,k] = cross_tab.iloc[1,1]
                else:
                    df_one_hot_norm_reduced.loc[j,k] = 0

    plt.imshow(df_one_hot_norm_reduced, cmap = "gist_heat", vmin=0, vmax=1)
    plt.colorbar()
    plt.xticks(np.arange(df_one_hot_norm_reduced.shape[1]), df_one_hot_norm_reduced.columns, rotation=90)
    plt.yticks(np.arange(df_one_hot_norm_reduced.shape[0]), df_one_hot_norm_reduced.index)
    file_end = "_" + "{}_{}_x_{}".format(i+1, col1.replace("/", "_"), col2.replace("/", "_"))
    write_results_fn(df = plt,
                     path_to_results = path_to_results,
                     issue_name = issue_name,
                     data_folder = "confounding_results/catxcat/",
                     file_end = file_end,
                     save_type = "fig")
    plt.close()
    
def plot_all_heatmaps(issue_name, 
                      path_to_results,
                      cat_df,
                      df_fc,
                      df_out,
                      df_oepl_sub
                      ):
    
    unique_features = df_out.Feature.unique()
    for i, col1 in tqdm(enumerate(unique_features)):
        col2 = df_out['Top Confounded Feature'][i]
        if 'OEPL' in col1 and 'OEPL' in col2:
            opt1 = col1.split(":")[1][:2]
            opt2 = col2.split(":")[1][:2]
            df_sub_col1 = df_oepl_sub.query("OPT_CAT == @opt1")
            df_sub_col2 = df_oepl_sub.query("OPT_CAT == @opt2")
            
            df_sub_col1 = df_sub_col1.rename(columns = {'OPT_NBR': 'COL1'})
            df_sub_col2 = df_sub_col2.rename(columns = {'OPT_NBR': 'COL2'})
            
            plot_heatmap(issue_name = issue_name,
                         path_to_results = path_to_results,
                         df_sub_col1 = df_sub_col1,
                         df_sub_col2 = df_sub_col2,
                         cat_df = cat_df,
                         i = i, 
                         col1 = col1, 
                         col2 = col2)
            
        elif 'FC' in col1 and 'FC' in col2:
            fc_cat = col1.split(":")[0]
            if fc_cat != 'FC':
                fc_cat = fc_cat.split("_", 1)[1]
            else:
                continue
            df_sub_col1 = df_fc[df_fc['Fault Code Category'] == fc_cat]
            df_sub_col1 = df_sub_col1.rename(columns = {'FC': 'COL1'})
            
            fc_cat = col2.split(":")[0]
            if fc_cat != 'FC':
                fc_cat = fc_cat.split("_", 1)[1]
            else:
                continue
            df_sub_col2 = df_fc[df_fc['Fault Code Category'] == fc_cat]
            df_sub_col2 = df_sub_col2.rename(columns = {'FC': 'COL2'})               
            
            plot_heatmap(issue_name = issue_name,
                         path_to_results = path_to_results,
                         df_sub_col1 = df_sub_col1,
                         df_sub_col2 = df_sub_col2,
                         cat_df = cat_df,
                         i = i, 
                         col1 = col1, 
                         col2 = col2)
            
        elif ('INS' in col1 or 'REL' in col1) and ('INS' in col2 or 'REL' in col2):
            col1_sub = col1.split(":")[0]
            col2_sub = col2.split(":")[0]
            
            col1_all_cols = cat_df.columns.values[[col1_sub in cat_df.columns.values[x] for x in range(cat_df.shape[1])]]
            col2_all_cols = cat_df.columns.values[[col2_sub in cat_df.columns.values[x] for x in range(cat_df.shape[1])]]
            
            df_sub_col1 = pd.DataFrame({'COL1': col1_all_cols})
            df_sub_col2 = pd.DataFrame({'COL2': col2_all_cols})
            
            plot_heatmap(issue_name = issue_name,
                         path_to_results = path_to_results,
                         df_sub_col1 = df_sub_col1,
                         df_sub_col2 = df_sub_col2,
                         cat_df = cat_df,
                         i = i, 
                         col1 = col1, 
                         col2 = col2)
            
        elif ('OEPL' in col1 and 'FC' in col2) or ('FC' in col1 and 'OEPL' in col2):
            if 'FC' in col1 and 'OEPL' in col2:
                col1_temp = col1
                col1 = col2
                col2 = col1_temp
            
            
            opt1 = col1.split(":")[1][:2]
            df_sub_col1 = df_oepl_sub.query("OPT_CAT == @opt1")
            df_sub_col1 = df_sub_col1.rename(columns = {'OPT_NBR': 'COL1'})
    
            fc_cat = col2.split(":")[0]
            if fc_cat != 'FC':
                fc_cat = fc_cat.split("_", 1)[1]
            else:
                continue
            df_sub_col2 = df_fc[df_fc['Fault Code Category'] == fc_cat]
            df_sub_col2 = df_sub_col2.rename(columns = {'FC': 'COL2'})
            plot_heatmap(issue_name = issue_name,
                         path_to_results = path_to_results,
                         df_sub_col1 = df_sub_col1,
                         df_sub_col2 = df_sub_col2,
                         cat_df = cat_df,
                         i = i, 
                         col1 = col1, 
                         col2 = col2)
    
        elif ('OEPL' in col1 and ('REL' in col2 or 'INS' in col2)) or (('REL' in col1 or 'INS' in col1) and 'OEPL' in col2):
            if (('REL' in col1 or 'INS' in col1) and 'OEPL' in col2):
                col1_temp = col1
                col1 = col2
                col2 = col1_temp
            
            opt1 = col1.split(":")[1][:2]
            df_sub_col1 = df_oepl_sub.query("OPT_CAT == @opt1")
            df_sub_col1 = df_sub_col1.rename(columns = {'OPT_NBR': 'COL1'})
            
            col2_sub = col2.split(":")[0]
            col2_all_cols = cat_df.columns.values[[col2_sub in cat_df.columns.values[x] for x in range(cat_df.shape[1])]]
            df_sub_col2 = pd.DataFrame({'COL2': col2_all_cols})
            plot_heatmap(issue_name = issue_name,
                         path_to_results = path_to_results,
                         df_sub_col1 = df_sub_col1,
                         df_sub_col2 = df_sub_col2,
                         cat_df = cat_df,
                         i = i, 
                         col1 = col1, 
                         col2 = col2)
            
        elif ('FC' in col1 and ('REL' in col2 or 'INS' in col2)) or (('REL' in col1 or 'INS' in col1) and 'FC' in col2):
            if (('REL' in col1 or 'INS' in col1) and 'FC' in col2):
                col1_temp = col1
                col1 = col2
                col2 = col1_temp
            
            fc_cat = col1.split(":")[0]
            if fc_cat != 'FC':
                fc_cat = fc_cat.split("_", 1)[1]
            else:
                continue
            df_sub_col1 = df_fc[df_fc['Fault Code Category'] == fc_cat]
            df_sub_col1 = df_sub_col1.rename(columns = {'FC': 'COL1'})
            
            col2_sub = col2.split(":")[0]
            col2_all_cols = cat_df.columns.values[[col2_sub in cat_df.columns.values[x] for x in range(cat_df.shape[1])]]
            df_sub_col2 = pd.DataFrame({'COL2': col2_all_cols})
            plot_heatmap(issue_name = issue_name,
                         path_to_results = path_to_results,
                         df_sub_col1 = df_sub_col1,
                         df_sub_col2 = df_sub_col2,
                         cat_df = cat_df,
                         i = i, 
                         col1 = col1, 
                         col2 = col2)
        
def run_num_v_num(num_data,
                  col1, 
                  j):
    print("Processing: " + "run_num_v_num" + " " + str(j))
    corr_list = np.zeros(len(num_data.columns))
    num_intersect = np.zeros(len(num_data.columns))
    for i, col2 in enumerate(num_data.columns):
        if col1 != col2:
            col1_vals = num_data[col1].dropna()
            col2_vals = num_data[col2].dropna()
            intersect_idx = np.intersect1d(col1_vals.index, col2_vals.index)
            
            intersect_vals_1 = num_data[col1][intersect_idx]
            intersect_vals_2 = num_data[col2][intersect_idx]
            if len(intersect_idx) > 1 and intersect_vals_1.nunique() > 1 and intersect_vals_2.nunique() > 1:
                corr = spearmanr(intersect_vals_1, intersect_vals_2)[0]
            else:
                corr = np.nan
        else:
            corr = np.nan
            intersect_idx = np.array([])
        corr_list[i] = corr
        num_intersect[i] = len(intersect_idx)
    return [corr_list, num_intersect]

def wide_to_long(corr_df, 
                 results_df,
                 num_data,
                 num_intersect_df):
    
    corr_df = np.vstack(corr_df)
    num_intersect_df = np.vstack(num_intersect_df)
    
    
    pearson_corr_df = pd.DataFrame(corr_df, columns = num_data.columns.values, index = num_data.columns.values)
    num_intersect_df = pd.DataFrame(num_intersect_df, columns = num_data.columns.values, index = num_data.columns.values)
    
    
    arr_p = pearson_corr_df.values
    index_names_p = pearson_corr_df.index
    col_names_p = pearson_corr_df.columns
    
    arr_i = num_intersect_df.values
    
    R,C = np.where(np.abs(np.triu(arr_p,1))>0.3)
    
    
    out_arr = np.column_stack((index_names_p[R],col_names_p[C],arr_p[R,C]))
    out_arr2 = np.column_stack((index_names_p[R],col_names_p[C],arr_i[R,C]))
    df_out2 = pd.DataFrame(out_arr2,columns=['Feature','Top Confounded Feature','Num of Samples'])

    
    df_out = pd.DataFrame(out_arr,columns=['Feature','Top Confounded Feature','Pearson Correlation'])
    df_out['Num of Samples'] = df_out2['Num of Samples']
    
    
    df_out = pd.merge(df_out, results_df[['Feature','Rank']], how = 'left', left_on = 'Feature', right_on = 'Feature')
    df_out = df_out.rename(columns = {'Rank' : 'Feature rank'})
    df_out = pd.merge(df_out, results_df[['Feature','Rank']], how = 'left', left_on = 'Top Confounded Feature', right_on = 'Feature')
    df_out = df_out.rename(columns = {'Rank' : 'Confounded Feature Rank', 'Feature_x': 'Feature', 'Pearson Correlation': 'Confounding Factor'})
    df_out = df_out.drop(['Feature_y'], axis = 1)
    
    df_out = df_out.sort_values(by = ['Feature rank', 'Confounded Feature Rank'])
    df_out = df_out.groupby('Feature').head(5)
    # Get the result out in a csv
    df_out = df_out[~np.isnan(df_out['Feature rank'])]
    df_out = df_out[~np.isnan(df_out['Confounded Feature Rank'])]
    
    df_out = df_out.loc[:,['Feature', 'Feature rank', 'Top Confounded Feature', 'Confounded Feature Rank', 'Confounding Factor']]


        
    return df_out

### VIF ###

def get_vif_for_num(df_out,
                    num_data
                    ):

    df_out['VIF'] = float('NaN')
    df_out['Highest_VIF'] = 0
    
    unique_features = df_out.Feature.unique()
    for col in unique_features:
        confounded_cols = df_out.query("Feature == @col")['Top Confounded Feature'].values
        all_cols = np.append(confounded_cols, col)
        all_data = num_data[all_cols].dropna()
        if all_data.shape[0] > 0:
            vif_vals = [variance_inflation_factor(all_data.values, i) for i in range(all_data.shape[1])]
        else:
            vif_vals = np.repeat(0, all_data.shape[1])
        df_out.loc[df_out['Feature'] == col, 'VIF'] = vif_vals[:len(vif_vals)-1]
        max_val = df_out[df_out['Feature'] == col]['VIF'].max()
        df_out.loc[(df_out['Feature'] == col) & (df_out['VIF'] == max_val),'Highest_VIF'] = 1
        
    df_out = df_out.drop(columns = ['VIF'])
    
    return df_out

def plot_all_pair_plots(issue_name,
                        path_to_results,
                        df_one_hot_raw,
                        df_out):
    def plot_num_x_num(issue_name, i, col1, col2):
        fig, ax = plt.subplots()
        p1 = ax.scatter(df_one_hot_raw.query("Label == 0")[col1], 
                        df_one_hot_raw.query("Label == 0")[col2], 
                        c = 'blue', s = 10, marker = 'o')
        p2 = ax.scatter(df_one_hot_raw.query("Label == 1")[col1], 
                        df_one_hot_raw.query("Label == 1")[col2], 
                        c = 'red', s = 50, marker = 'X')
        count_p1 = sum((df_one_hot_raw.query("Label == 0")[col1].notnull()) & (df_one_hot_raw.query("Label == 0")[col2].notnull()))
        count_p2 = sum((df_one_hot_raw.query("Label == 1")[col1].notnull()) & (df_one_hot_raw.query("Label == 1")[col2].notnull()))
        plt.legend((p1, p2), ('Non-issue ' + '(Count:' + str(count_p1) + ')', 'Issue ' + '(Count:' + str(count_p2) + ')'), frameon = True)
        plt.xlabel(str(col1).replace(":","_"), fontsize=9)
        plt.ylabel(str(col2).replace(":","_"), fontsize=9)
        file_end = "_" + "{}_{}_x_{}".format(i+1, col1.replace("/", "_"), col2.replace("/", "_"))
        write_results_fn(df = plt,
                         path_to_results = path_to_results,
                         issue_name = issue_name,
                         data_folder = "confounding_results/numxnum/",
                         file_end = file_end,
                         save_type = "fig")
        plt.close()
        
    for i, col1 in tqdm(enumerate(df_out.Feature)):
        col2 = df_out['Top Confounded Feature'][i]
        plot_num_x_num(issue_name, i, col1, col2)
    
def run_cat_v_num(num_data,
                  cat_df,
                  col1, 
                  j
                  ):
    print(j)
    anova_list = np.zeros(len(num_data.columns))
    for i, col2 in enumerate(num_data.columns):
        col1_vals = num_data[cat_df[col1] == 1][col2].dropna()
        col2_vals = num_data[cat_df[col1] == 0][col2].dropna()
        if len(col1_vals) > 5 and len(col2_vals) > 5 and col1_vals.nunique() > 1 and col2_vals.nunique() > 1:
            anova_val = kruskal(col1_vals, col2_vals)[1]
        else:
            anova_val = np.nan
        anova_list[i] = anova_val
    return anova_list

def run_anova_and_save(num_data,
                       cat_df,
                       results_df,
                       num_cores, 
                       n=5):

    anova_df = Parallel(n_jobs = num_cores)(delayed(run_cat_v_num)(num_data, cat_df, col, j) \
                       for j, col in enumerate(cat_df.columns.values))
    anova_df = np.vstack(anova_df)
    
    anova_df = pd.DataFrame(anova_df, columns = num_data.columns.values, index = cat_df.columns.values)
    
    df_out = pd.melt(anova_df)
    df_out['Feature'] = np.tile(anova_df.index.values, anova_df.shape[1])
    df_out = df_out.rename(columns = {'variable': 'Top Confounded Feature'})
    
    
    df_out = pd.merge(df_out, results_df[['Feature','Rank']], how = 'left', left_on = 'Feature', right_on = 'Feature')
    df_out = df_out.rename(columns = {'Rank' : 'Feature rank'})
    df_out = pd.merge(df_out, results_df[['Feature','Rank']], how = 'left', left_on = 'Top Confounded Feature', right_on = 'Feature')
    df_out = df_out.rename(columns = {'Rank' : 'Confounded Feature Rank', 'Feature_x': 'Feature', 'value': 'Confounding Factor'})
    df_out = df_out.drop(['Feature_y'], axis = 1)
    
    df_out = df_out.sort_values(by = ['Feature rank', 'Confounded Feature Rank'])
    df_out = df_out.groupby('Feature').head(5)
    
    df_out = df_out[~np.isnan(df_out['Feature rank'])]
    df_out = df_out[~np.isnan(df_out['Confounded Feature Rank'])]
    
    return df_out

#%% ##########################

#%% functions for residual analysis####
def residual_analysis_main(issue_name,
                           path_to_results,
                           path_processed_raw, 
                           path_processed_norm,
                           path_base,
                           mileage_min,
                           path_output,
                           path_univariate_contributons,
                           numeric_cols, #list of numeric columns
                           res_df_one,
                           writer,
                           columns_to_remove = pd.DataFrame(),
                           df_one_hot_raw = pd.DataFrame(),
                           df_one_hot_norm_reduced = pd.DataFrame(),
                           test_run = False
                           ):
    # Read in raw data to be used in plots later
    if df_one_hot_raw.size == 0:
        #df_one_hot_raw = pd.read_csv("{}{}/Data/{}_one_hot_raw.csv".format(path_to_results, issue_name, issue_name))
        df_one_hot_raw = pd.read_csv("{}{}/Data/{}_no_one_hot_raw.csv".format(path_to_results, issue_name, issue_name))
        #path_processed_raw = "{}/{}/Data/{}_one_hot_raw.csv".format(path_to_results, issue_name, issue_name)
        #df_one_hot_raw = load_raw_data(path_processed_raw, mileage_min)
    
    #normalized_limit = norm_miles(mileage_min = mileage_min, path_processed_raw = path_processed_raw)
    
    if df_one_hot_norm_reduced.size == 0:
        #path_processed_norm = "{}/{}/Data/{}_one_hot_norm_reduced.csv".format(path_to_results, issue_name, issue_name)
        #df_one_hot_norm_reduced = load_norm_data(path_processed_norm, -999999)
        df_one_hot_norm_reduced = pd.read_csv("{}{}/Data/{}_one_hot_norm_reduced.csv".format(path_to_results, issue_name, issue_name))
    #df_one_hot_raw = pd.merge(df_master[['ESN', 'EARLIEST_INDICATION_DATE', 'ID']], df_one_hot_raw, how = 'right', on = ['ESN', 'EARLIEST_INDICATION_DATE'])
    #df_one_hot_norm_reduced = pd.merge(df_master[['ESN', 'EARLIEST_INDICATION_DATE', 'ID']], df_one_hot_norm_reduced, how = 'right', on = ['ESN', 'EARLIEST_INDICATION_DATE'])
    
    ## SANJAY UPDATED
    #df_one_hot_raw = df_one_hot_raw[df_one_hot_norm_reduced.columns].reset_index(drop = True)
    num_cols = df_one_hot_norm_reduced.columns.values[[df_one_hot_norm_reduced.columns.values[x] in numeric_cols for x in range(df_one_hot_norm_reduced.shape[1])]]

    df_one_hot_norm_reduced = df_one_hot_norm_reduced.iloc[df_one_hot_raw.index,:]    
    for col in tqdm(num_cols):
        
        df_one_hot_norm_reduced.iloc[df_one_hot_raw[col][df_one_hot_raw[col].isnull()].index, \
                                                    np.where(df_one_hot_norm_reduced.columns.values == col)[0][0]] = -99999
        
    df_one_hot_norm_reduced = df_one_hot_norm_reduced.fillna(-99999)
    
    # Remove features not needed
    df_one_hot_norm_reduced = remove_unwanted_features(input_data = df_one_hot_norm_reduced,
                                              exclude_features = ['MES','EPAT'],
                                              columns_to_remove = columns_to_remove)
    
    #all_avg_stats = create_all_avg_stats(path_output = path_output,
    #                                    numeric_cols = numeric_cols)
    
    #res_df_one = create_res_df_one(path_univariate_contributons = path_univariate_contributons)
    
    writer = add_image_to_excel(writer = writer,
                                sheet_name = '4 RESIDUAL',
                                path_to_image = '{}/excel_images'.format(path_base),
                                image_name = 'residual')
    
    # need to add back the initial_columns
    writer = calc_res(df_one_hot_norm_reduced = df_one_hot_norm_reduced,
                      df_one_hot_raw = df_one_hot_raw,
                      res_df_one = res_df_one, 
                      all_avg_stats = [], 
                      path_to_results = path_to_results, 
                      issue_name = issue_name,
                      numeric_cols = numeric_cols,
                      writer = writer)
    #calc_res(df_one_hot_raw, res_df_one, all_avg_stats, initial_columns, path_to_results, issue_name)
    
    if test_run:
        return("testing not implemented yet")
        """
        TESTING BY CREATING FALSE NEGATIVES
        
        Another way to test the Residual Analysis results is to delibrately create False Negatives and try to spot them. This basically
        means that we will take a few EGR Cooler issues, and mark them as Not Issue (negative set) in the data. Then we build the model
        and try to spot these issues which were falsely marked as negative issues
        """
        """
        #not sure what the rest of these are doing - need to check with Sanajy and/or Nimit
        columns = input_features(top_feature_file = top_features, top_n = 40, exclude_features = tuple(['MES']), 
                      other_columns = ["Label", "ESN", "EARLIEST_INDICATION_DATE", "INCDT_ISSUE_NUMBER", "id"],
                      extra_features = ['INS_EA_OIL_TMP_SEC_SEV_1', 'REL_BUILD_YEAR_MONTH:2016_11'])
        
        df_master = spy_algorithm(s = 0.25, l = 0.05, label_column = 'Label', df = df, all_columns = columns, 
                     other_columns = ["Label", "ESN", "EARLIEST_INDICATION_DATE", "INCDT_ISSUE_NUMBER", "Spy", "id"])
        
        # Now we will take a few issues and mark them as the negative set
        issues_index = df_master.index[df_master['Label'] == 1].tolist()
        random_set = random.sample(issues_index, 5)
        random_set_id = df_master.id[random_set]
        df_master.Label[random_set] = 0
        
        
        fp_final, fn_final, confusion_matrix_data = find_mislabels(df_master = df_master, label_column = 'Label', id_column = 'id',
                       other_columns = ['id', 'ESN', 'Label', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'],
                       loops = 1)
        """

    return(res_df_one, writer)
    
# This function will essentially help with selecting the set of features to use for the analysis, as well as any other
# informative features regarding the incidents which will not be used in the model building, but should be present in the 
# dataset just to enhance the understanding of the results while viewing them.
    
#Identify threshold under which we have reliable negatives
def findThreshold(l, 
                  sorted_spy_probs):
    thresholds = np.arange(0, 1, .001)
    for t in thresholds:
        if np.mean(sorted_spy_probs < t) > l:
            return(t)

def create_res_df_one(path_univariate_contributons):
    # Load the output from multivariate, so that we only use features here which have some controbution towards the prediction
    res_df_one = pd.read_csv(path_univariate_contributons)
    # Now additionally also add the final contributions of these variables towards the end
    res_df_one = res_df_one.rename(columns = {'Unnamed: 0':'feature_name'})
    res_df_one = res_df_one.rename(columns = {'feature':'feature_name'})
    res_df_one = res_df_one.rename(columns = {'contribution':'Multivariate Importance Factor (contribution)'})
    return(res_df_one)

def spy_algorithm(s, 
                  l, 
                  df, 
                  label_column, 
                  all_columns, 
                  other_columns
                  ):
    
    df_master = df.loc[:,all_columns] # Select the features to actually use
    df_master = df_master.dropna(axis = 1, how = 'all') # Clear any columns that are completely NA
    
    # Create the issue and non-issue dataframes to play with
    df_issue = df_master[df_master[label_column] == 1]
    df_non_issue = df_master[df_master[label_column] == 0]

    # Hyperparameters
    # s - percentage of + examples to put in unlabeled set
    # l - percentage of spy's to be below threshold t

    # Randomly sample spy observations
    spy_idx = sample(range(df_issue.shape[0]), int(df_issue.shape[0]*s))
    spy_obs = df_issue.iloc[spy_idx,:]
    non_spy_obs = df_issue[~df_issue.index.isin(spy_obs.index)]

    #Add label to identify spy's
    spy_obs['Spy'] = 1
    non_spy_obs['Spy'] = 0
    df_non_issue['Spy'] = 0
    df_issue['Spy'] = 0

    #Add spy observations to non-issue unlabeled set
    df_non_issue = pd.concat([df_non_issue, spy_obs])
    df_non_issue[label_column] = 0

    #Concatenate everything!
    df_all = pd.concat([non_spy_obs, df_non_issue]).reset_index(drop = True)

    y = df_all[label_column]
    spy = df_all['Spy']
    df_all_other = df_all[other_columns]
    df_all.drop(other_columns, axis=1, inplace=True)
    
    # Now normalize the columns in this dataframe and impute mean(0) instead of NA everywhere
    df_all = normalize_columns(df = df_all)

    # Identify indices in unlabeled set that are NOT spys
    # Will need to filter on these when identifying negative labels 
    no_spy_no_issue = (spy == 0) == (y == 0)
    no_spy_no_issue = no_spy_no_issue.index[no_spy_no_issue].values

    ## Fit Weighted Logistic Regression ##
    
    clf = LogisticRegressionCV(penalty = 'l2', class_weight = 'balanced', max_iter = 10, cv = 5)
    clf.fit(df_all, y)

    # Get class probabilities for each observations
    probs = clf.predict_proba(df_all)
    probs = probs[:,1]

    #Sort spy probabilities
    sorted_spy_probs = np.sort(probs[np.where(spy == 1)[0]])

    t = findThreshold(l, sorted_spy_probs = sorted_spy_probs)
    
    # Combine the features used for the model building with the rest of the columns
    df_all = pd.concat([df_all_other, df_all], axis = 1)
    
    # Identify Reliable Negative Incidents
    reliable_N = np.where(probs < t)[0]
    reliable_N = np.intersect1d(reliable_N, no_spy_no_issue)
    df_reliable_N = df_all.iloc[reliable_N, :].reset_index(drop = True)
    
    df_reliable_N[label_column] = 0
    df_master = pd.concat([df_issue, df_reliable_N]).reset_index(drop = True)
    del df_master['Spy']
    
    return(df_master)

def create_all_avg_stats(path_output,
                         numeric_cols #list of numeric columns
                         ):
    # Add columns about average values for each of these features to this data
    uni_output = pd.ExcelFile(path_output)
    # Also, get together the average and other statistics for all the features, stuff that is generated during the Univariate Analysis
    design = pd.read_excel(uni_output, 'Design')
    manuf = pd.read_excel(uni_output, 'Manufacturing')
    usage = pd.read_excel(uni_output, 'Usage')
    columns_we_want = ['Variable', 'Median of Issues', 'Median of Non-Issues', 'Mean of Issues',
           'Mean of Non-Issues', 'SD of Issues', 'SD of Non-Issues',
           'Rate of Issues', 'Rate of Non-Issues', 'Sample Size of Issues',
           'Sample Size of Non-Issues']
    design = design.loc[:, columns_we_want]
    manuf = manuf.loc[:, columns_we_want]
    usage = usage.loc[:, columns_we_want]
    all_avg_stats = pd.concat([design, manuf, usage], axis = 0)
    all_avg_stats = all_avg_stats.rename(columns = {'Variable':'feature_name'})
    return(all_avg_stats)

def find_mislabels(df_one_hot_norm_reduced, 
                   label_column, 
                   other_columns, 
                   id_column, 
                   loops, 
                   top_n, 
                   use_contributing_features_only, 
                   contributing_features,
                   numeric_cols #list of numeric columns
                   ):
    
    features = df_one_hot_norm_reduced[df_one_hot_norm_reduced.columns.difference(other_columns)]
    labels = df_one_hot_norm_reduced[label_column]

    # Normalize the features dataset before fitting a model on it
    # Determine which columns are numerical here, and normalize them
    
    features_categorical = features[features.columns.difference(numeric_cols)].reset_index(drop = True)
    features_numeric = features[features.columns.difference(features_categorical.columns.tolist())].reset_index(drop = True)

    features_numeric = normalize_columns(features_numeric)
    features = pd.concat([features_categorical, features_numeric], axis = 1)
    
    contributing_features = contributing_features[contributing_features.isin(features.columns)]

    
    # Initialize empty lists for the false positives and false negatives
    false_positive = []
    false_negative = []
    #top_potential_fp = []
    #top_potential_fn = []
    
    for i in range(loops):
        # Initialize our classifier
        #logregcv = LogisticRegressionCV(Cs = 5, cv = 4, penalty = "l2", solver = "liblinear", class_weight = 'balanced')
        rf = RandomForestClassifier(random_state = 777, 
                                    n_estimators = 100, 
                                    verbose = True, 
                                    class_weight = "balanced",
                                    n_jobs = 8, 
                                    min_samples_split = 15)
        
        # Keep only contributing features, if decided to do so
        if(use_contributing_features_only == True):
            features = features.loc[:, contributing_features]
        
        # Make predictions by taking a crossvalidated approach
        preds = cross_val_predict(X = features, y = labels, estimator = rf, cv = 5)
        df_one_hot_norm_reduced['prediction'] = preds
        probs = cross_val_predict(X = features, y = labels, estimator = rf, cv = 5, method = "predict_proba")
        probs_actual = [value[1] for value in probs]
        df_one_hot_norm_reduced['issue_probability'] = probs_actual
    
        # Now we need to highlight the data points which were predicted to be the opposite set by the algorithm
        # The definition of both false positive and false negative is important here
        # False Positive: Something which was marked as Positive (1) by the SMEs (infant care) but is marked as Negative (0) by the algorithm
        # False Negative: Something which was marked as Negative (0) by the SMEs (infant care) but is marked as Positive (1) by the algorithm
        
        loop_false_negatives = df_one_hot_norm_reduced[id_column][(df_one_hot_norm_reduced['prediction'] == 1) & (df_one_hot_norm_reduced['Label'] == 0)].tolist()
        loop_false_positives = df_one_hot_norm_reduced[id_column][(df_one_hot_norm_reduced['prediction'] == 0) & (df_one_hot_norm_reduced['Label'] == 1)].tolist()
        
        # Also, we basically might get a lot of these false positives and false negatives, or we might get none.
        # So we need to prepare for both situations
        
        if(len(loop_false_negatives) == 0):
            loop_false_negatives = df_one_hot_norm_reduced[(df_one_hot_norm_reduced['Label'] == 0)].sort_values(by = ['issue_probability'], ascending = False).head(top_n)[id_column].tolist()
        if(len(loop_false_negatives) >= top_n):
            loop_false_negatives = df_one_hot_norm_reduced[(df_one_hot_norm_reduced['Label'] == 0) & (df_one_hot_norm_reduced['prediction'] == 1)].sort_values(by = ['issue_probability'], ascending = False).head(top_n)[id_column].tolist()
        else:
            loop_false_negatives = loop_false_negatives
            
        if(len(loop_false_positives) == 0):
            loop_false_positives = df_one_hot_norm_reduced[(df_one_hot_norm_reduced['Label'] == 1)].sort_values(by = ['issue_probability'], ascending = True).head(top_n)[id_column].tolist()
        if(len(loop_false_positives) >= top_n):
            loop_false_positives = df_one_hot_norm_reduced[(df_one_hot_norm_reduced['Label'] == 1) & (df_one_hot_norm_reduced['prediction'] == 0)].sort_values(by = ['issue_probability'], ascending = True).head(top_n)[id_column].tolist()
        else:
            loop_false_positives = loop_false_positives
        
        
        # Append these results to the list we actually have
        false_positive.extend(loop_false_positives)
        false_negative.extend(loop_false_negatives)
        
    # Now filter the false positives and negatives to keep only ones that occur frequent enough.
    # Here only keeping ones greater than half of the number of loops
    
    fp = {x:false_positive.count(x) for x in false_positive}
    fn = {x:false_negative.count(x) for x in false_negative}
    
    # Keep only those incidents that appear atleast in half of the iterations
    fp = {k:v for k, v in fp.items() if v > math.floor(loops/2)}
    fp_final = list(fp.keys())
    fn = {k:v for k, v in fn.items() if v > math.floor(loops/2)}
    fn_final = list(fn.keys())
    
    # Make the prediction column depict only the above points as the false positives and negatives
    df_one_hot_norm_reduced['prediction'] = df_one_hot_norm_reduced[label_column]
    df_one_hot_norm_reduced['prediction'][df_one_hot_norm_reduced[id_column].isin(fp_final)] = 0
    df_one_hot_norm_reduced['prediction'][df_one_hot_norm_reduced[id_column].isin(fn_final)] = 1
    
    # Get the probabilities given by the model, in the output master dataframe
    rf = RandomForestClassifier(random_state = 777, 
                                n_estimators = 100, 
                                verbose = True, 
                                class_weight = "balanced",
                                n_jobs = 8, 
                                min_samples_split = 15)
    
    probs = cross_val_predict(X = features, y = labels, estimator = rf, cv = 5, method = "predict_proba")
    probs_actual = [value[1] for value in probs]
    df_one_hot_norm_reduced['issue_probability'] = probs_actual
    
    # Print the confusion matrix
    confusion_matrix_data = pd.crosstab(df_one_hot_norm_reduced[label_column], df_one_hot_norm_reduced['prediction'], rownames=['Actual'], colnames=['Predicted'], margins=True)
    
    # Return the various results
    return fp_final, fn_final, confusion_matrix_data, df_one_hot_norm_reduced

def calc_res(df_one_hot_norm_reduced,
             df_one_hot_raw,
             res_df_one,
             all_avg_stats,
             #initial_columns, #need to fix this and add back
             path_to_results,
             issue_name,
             numeric_cols, #list of numeric columns
             writer
             ):
    
    output_folder_mk(path_to_results = path_to_results, 
                     issue_name = issue_name,
                     make_type = "residual")
    
    # Take features which contribute
    contributing_features = res_df_one[res_df_one['Absolute Contributions'] != 0]['Feature']
    features_order = res_df_one[res_df_one['Absolute Contributions'] != 0].sort_values(by = 'Absolute Contributions', ascending = False)['Feature']
    
    
    # Generate the list of false positives and false negatives 
    fp_final, fn_final, confusion_matrix_result, df_master = find_mislabels(df_one_hot_norm_reduced = df_one_hot_norm_reduced, label_column = 'Label', 
                                                                          id_column = 'ID',
                                                                          other_columns = ['ID', 'ESN', 'Label', 'INCDT_ISSUE_NUMBER','EARLIEST_INDICATION_DATE'],
                                                                          loops = 1, top_n = 20,
                                                                          use_contributing_features_only = True,
                                                                          contributing_features = contributing_features,
                                                                          numeric_cols = numeric_cols)
    
    num_cols = df_one_hot_norm_reduced.columns.values[[df_one_hot_norm_reduced.columns.values[x] in numeric_cols for x in range(df_one_hot_norm_reduced.shape[1])]]
    other_cols = df_one_hot_norm_reduced.columns.values[[df_one_hot_norm_reduced.columns.values[x] not in numeric_cols for x in range(df_one_hot_norm_reduced.shape[1])]]
    
    df_master = pd.concat([df_one_hot_raw[num_cols], df_master[other_cols]], axis = 1)
    
    # Generate the data for these false positive and false negative incidents
    fp_final_data = df_master[df_master.ID.isin(fp_final)]
    fn_final_data = df_master[df_master.ID.isin(fn_final)]
    
    # Show only those columns we care about, which were actually used for the prediction
    identifier_columns = ['ESN', 'issue_probability', 'EARLIEST_INDICATION_DATE']
    identifier_columns.extend(features_order)
    
    # Reorder columns, showing important ones for prediction above
    fp_final_data = fp_final_data.loc[:,identifier_columns]
    fn_final_data = fn_final_data.loc[:,identifier_columns]
    
    # Now it is necessary to get these into a state which can be interpreted
    # Take a transpose
    fp_final_transpose = pd.concat([pd.DataFrame(fp_final_data.columns, columns = ['Feature']), pd.DataFrame(np.array(fp_final_data).T)], axis = 1)
    fn_final_transpose = pd.concat([pd.DataFrame(fn_final_data.columns, columns = ['Feature']), pd.DataFrame(np.array(fn_final_data).T)], axis = 1)
    
    # Now we will only show those feature results which have been shown in the univariate output
    # need to fix this part of the code, not sure what this code does
    #fp_final_transpose = fp_final_transpose[(fp_final_transpose['feature_name'].isin(all_avg_stats['feature_name'].tolist()))|(fp_final_transpose['feature_name'].isin(initial_columns))]
    #fn_final_transpose = fn_final_transpose[(fn_final_transpose['feature_name'].isin(all_avg_stats['feature_name'].tolist()))|(fn_final_transpose['feature_name'].isin(initial_columns))]
    
    #fp_final_transpose = pd.merge(fp_final_transpose, all_avg_stats, how='left', on = ['Feature'])
    #fn_final_transpose = pd.merge(fn_final_transpose, all_avg_stats, how='left', on = ['Feature'])
    
    fp_final_transpose = pd.merge(fp_final_transpose, res_df_one[['Feature', 'Contribution','Rank']], how='left', on = ['Feature'])
    fn_final_transpose = pd.merge(fn_final_transpose, res_df_one[['Feature', 'Contribution','Rank']], how='left', on = ['Feature'])
    
    fp_final_transpose = fp_final_transpose.replace(-99999, float('NaN'))
    fn_final_transpose = fn_final_transpose.replace(-99999, float('NaN'))
    
    fp_final_transpose.to_excel(writer, sheet_name='4.1 False Positives', index = False)
    fn_final_transpose.to_excel(writer, sheet_name='4.2 False Negatives', index = False)

    return(writer)

#this function is currently not being used - need to figure out what to do with it
def generate_results(incident_ids, df, id_column, manual_features, top_n):
    
    final_data = df[df[id_column].isin(incident_ids)]
    features = ['probabilities']
    features.extend(manual_features)
    features.extend(list(set(feature_names_order) - set(manual_features)))
    if top_n <= len(incident_ids):
        output_data = final_data.sort_values(by = ['probabilities'], ascending  = False).head(top_n)
    else:
        output_data = final_data.sort_values(by = ['probabilities'], ascending  = False).head(len(incident_ids))
    
    output_data = output_data.loc[:,features]
    
    return(output_data)
    
#%% ##########################

#%% functions for similar ESN analysis####
    
#%% ##########################

#%% functions for weibull analysis####
    
#%% ##########################

#%% functions for plotting analysis####
    
#%% ##########################

#%% helper functions across multiple analyses####

#currently used for multivariate and residual analysis
def remove_unwanted_features(input_data, exclude_features, columns_to_remove):
    
    # Remove columns about price and amounts
    df_master = input_data.drop(columns_to_remove, axis = 1)
    df_master['ID'] = df_master.index
    
    # Keep only the features other than MES and EPAT
    features_to_keep = list(df_master.columns)
    # Remove above mentioned
    for word in features_to_keep[:]:
        if word.startswith(tuple(exclude_features)):
            features_to_keep.remove(word)
    
    # Keep only features other the ones removed above
    df_master = df_master.loc[:,features_to_keep]
    
    return(df_master)
    
def output_folder_mk(path_to_results, #path to folder where results will be stored
                     issue_name, #name of current issue
                     make_type):
    
    if make_type == "new":
        print("making new file structure")
        #Remove Folder if exists
        os.system("rm -r {}{}".format(path_to_results, issue_name))
        
        #Create structure
        os.system("mkdir {}{}".format(path_to_results, issue_name))
    
    if make_type == "data":
        print("setting up data folder")
        #Remove Folder if exists
        os.system("rm -r {}{}/Data".format(path_to_results, issue_name))

        #Create structure
        os.system("mkdir {}{}/Data".format(path_to_results, issue_name))
        
    if make_type == "figs":
        print("setting up figs folder")
        #Remove Folder if exists
        os.system("rm -r {}{}/Figs".format(path_to_results, issue_name))
        
        #Create structure
        os.system("mkdir {}{}/Figs".format(path_to_results, issue_name))
        os.system("mkdir {}{}/Figs/Usage".format(path_to_results, issue_name))
        os.system("mkdir {}{}/Figs/Design".format(path_to_results, issue_name))
        os.system("mkdir {}{}/Figs/Manufacturing".format(path_to_results, issue_name))
        os.system("mkdir {}{}/Figs/MAB".format(path_to_results, issue_name))
    
    if make_type == "univariate":
        print("setting up univariate folder")
        #Remove Folder if exists
        os.system("rm -r {}{}/univariate_results".format(path_to_results, issue_name))
        
        #Create structure
        os.system("mkdir {}{}/univariate_results".format(path_to_results, issue_name))
        
    if make_type == "multivariate":
        print("setting up multivariate folder")
        #Remove Folder if exists
        os.system("rm -r {}{}/multivariate_results/catxcat/".format(path_to_results, issue_name))
        os.system("rm -r {}{}/multivariate_results/numxnum/".format(path_to_results, issue_name))
        os.system("rm -r {}{}/multivariate_results/catxnum/".format(path_to_results, issue_name))
        
        #Create structure
        os.system("mkdir {}{}".format(path_to_results, issue_name))
        os.system("mkdir {}{}/multivariate_results/".format(path_to_results, issue_name))
        os.system("mkdir {}{}/multivariate_results/catxcat/".format(path_to_results, issue_name))
        os.system("mkdir {}{}/multivariate_results/numxnum/".format(path_to_results, issue_name))
        os.system("mkdir {}{}/multivariate_results/catxnum/".format(path_to_results, issue_name))
    
    if make_type == "residual":
        print("setting up residual folder")
        #Remove Folder if exists
        os.system("rm -r {}{}/residual_results".format(path_to_results, issue_name))
        
        #Create structure
        os.system("mkdir {}{}".format(path_to_results, issue_name))
        os.system("mkdir {}{}/residual_results".format(path_to_results, issue_name))
        
    if make_type == "confounding": 
        print("setting up confounding folder")
        #Remove Folder if exists
        os.system("rm -r {}{}/confounding_results".format(path_to_results, issue_name))
        os.system("rm -r {}{}/confounding_results/catxcat/".format(path_to_results, issue_name))
        os.system("rm -r {}{}/confounding_results/numxnum/".format(path_to_results, issue_name))
        os.system("rm -r {}{}/confounding_results/catxnum/".format(path_to_results, issue_name))
        
        #Create structure
        os.system("mkdir {}{}".format(path_to_results, issue_name))
        os.system("mkdir {}{}/confounding_results".format(path_to_results, issue_name))
        os.system("mkdir {}{}/confounding_results/catxcat/".format(path_to_results, issue_name))
        os.system("mkdir {}{}/confounding_results/numxnum/".format(path_to_results, issue_name))
        os.system("mkdir {}{}/confounding_results/catxnum/".format(path_to_results, issue_name))
    
    return(True)
    
def write_results_fn(df,
                     path_to_results,
                     issue_name,
                     data_folder,
                     file_end,
                     save_type = "csv",
                     file_pre = ""):
    
    if save_type == "csv":
        ext = "csv"
    elif save_type == "pickle":
        ext = "p"
    elif save_type == "fig":
        ext = "png"
    elif save_type == "excel":
        ext = "xlsx"
    
    if issue_name != "":
        issue_name_folder = issue_name + "/"
    else:
        issue_name_folder = ""
        
    path = "{}{}{}{}{}{}.{}".format(path_to_results, issue_name_folder, data_folder, file_pre, issue_name, file_end, ext)
        
    print("Saving: " + path)    
    
    if save_type == "csv":
        df.to_csv(path, index = False, na_rep = "NA")
    
    if save_type == "pickle":
        pickle.dump(df, open(path, "wb" ) )
        
    if save_type == "fig":
        df.savefig(path, bbox_inches="tight")
        
    if save_type == "excel":
        return(pd.ExcelWriter(path, engine='xlsxwriter'))

    return(True)

#currently used for multivariate and residual analysis
def normalize_columns(df):
    for col in range(0,len(df.columns)):
        finite_vals = df[df.columns[col]].dropna()
        if len(finite_vals) > 0:
            scaled_vals = scale(finite_vals)
            df.iloc[finite_vals.index.values, np.where(df.columns.values == df.columns[col])[0][0]] = scaled_vals
        
        #Replace NaN with 0's (Mean)
        df[df.columns[col]] = df[df.columns[col]].replace(np.nan, 0)
    
    return(df)
    
#%% ##########################

#%%  variable definitions ####
def var_def(path_to_col):
    col_def = pd.ExcelFile(path_to_col)
    col_def = pd.read_excel(col_def, 'ID_columns')
    
    esn_col = col_def['all_col'][col_def['esn_col']].unique().tolist()
    cat_cols = col_def['all_col'][col_def['cat_cols']].unique().tolist()
    numeric_cols = col_def['all_col'][col_def['numeric_cols']].unique().tolist()
    vpcr_cols = col_def['all_col'][col_def['vpcr_cols']].unique().tolist()
    mes_cols = col_def['all_col'][col_def['mes_cols']].unique().tolist()
    good_cols = col_def['all_col'][col_def['good_cols']].unique().tolist()
    cols_to_split = col_def['all_col'][col_def['cols_to_split']].unique().tolist()
    extra_cols = col_def['all_col'][col_def['extra_cols']].unique().tolist()
    fault_code_cols = col_def['all_col'][col_def['fault_code_cols']].unique().tolist()
    duration_cols = col_def['all_col'][col_def['duration_cols']].unique().tolist()
    ins_fs_cat_cols = col_def['all_col'][col_def['ins_fs_cat_cols']].unique().tolist()
    numeric_fs_cols = col_def['all_col'][col_def['numeric_fs_cols']].unique().tolist()
    uv_col_order = col_def['all_col'][col_def['uv_col_order']].unique().tolist()
    uv_cols = col_def['all_col'][col_def['uv_cols']].unique().tolist()
    
    return(esn_col, cat_cols, numeric_cols, vpcr_cols, mes_cols, good_cols, cols_to_split, extra_cols, fault_code_cols, duration_cols, ins_fs_cat_cols, numeric_fs_cols, uv_col_order, uv_cols)
    
def var_def_old():
    ## Extract Relevant Features ##
    esn_col = ['ESN']
    # Categorical Cols
    cat_cols = ['REL_CMP_PROGRAM_GROUP_NAME', \
                'REL_BUILT_ON_WEEKEND', \
                'REL_ANALYSIS_RATE_CAT', 'REL_CPL_NUM', \
                'REL_DESIGN_HSP_NUM', 'REL_DESIGN_RPM_NUM', \
                'REL_ECM_PART_NO','REL_MKTG_HSP_NUM', 'REL_MKTG_RPM_NUM', \
                'REL_OEM_TYPE', 'REL_OEM_GROUP', 'REL_BUILD_YEAR_MONTH', \
                'REL_USER_APPL_DESC', 'REL_EMISSIONS_FAMILY_CODE', \
                'INS_FS_AFTERTREATMENT_FUEL_INJECTOR_STATUS_NA', \
                'INS_FS_AFTERTREATMENT_FUEL_SHUTOFF_VALVE_STATUS_NA', \
                'INS_FS_AFTERTREATMENT_PURGE_AIR_SOLENOID_STATUS_NA', \
                'INS_FS_AIR_CONDITIONING_PRESSURE_SWITCH_NA', \
                'INS_FS_BRAKE_PEDAL_POSITION_SWITCH_NA', \
                'INS_FS_CLUTCH_PEDAL_POSITION_SWITCH_NA', \
                'INS_FS_CRANKCASE_VENTILATION_HEATER_COMMANDED_STATE_NA', \
                'INS_FS_CRUISE_CONTROL_ON_OFF_SWITCH_NA', \
                'INS_FS_EGR_FLOW_DERATE_NA', \
                'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_2_NA', \
                'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_3_NA', \
                'INS_FS_ENGINE_COOLANT_LEVEL_NA', \
                'INS_FS_ENGINE_OPERATING_STATE_NA', \
                'INS_FS_ENGINE_PROTECTION_DERATE_SUPPRESS_NA', \
                'INS_FS_ENGINE_PROTECTION_SHUTDOWN_OVERRIDE_SWITCH_NA', \
                'INS_FS_ENGINE_SPEED_MAIN_SENSOR_SIGNAL_STATE_NA', \
                'INS_FS_ENGINE_SPEED_MAIN_SYNCHRONIZATION_STATE_NA', \
                'INS_FS_ENGINE_TORQUE_MODE_NA', \
                'INS_FS_FAN_CONTROL_SWITCH_NA', \
                'INS_FS_PTO_ON_OFF_SWITCH_NA', \
                'INS_FS_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_SWITCH_NA', \
                'INS_FS_REMOTE_PTO_SWITCH_NA', \
                'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_DERATE_NA', \
                'INS_FS_TURBOCHARGER_SPEED_DERATE_ACTIVE_NA', \
                'INS_FS_WATER_IN_FUEL_STATE_NA', \
                'INS_FS_ENGINE_SPEED_STATUS_NA', \
                'INS_FS_FAN_DRIVE_STATE_NA', \
                'INS_FS_KEYSWITCH_NA'
                ]
    
    # Numeric Cols 
    numeric_cols = ['REL_CMP_SUM_NET_AMOUNT', \
                    'REL_CMP_SUM_MATERIALS_AMOUNT', \
                    'REL_CMP_SUM_MARKUP_AMOUNT', \
                    'REL_CMP_SUM_REPAIR_LABOR_AMOUNT', \
                    'REL_CMP_SUM_OTHER_EXPENSE_AMOUNT', \
                    'REL_CMP_SUM_DEDUCTIBLE_AMOUNT', \
                    'REL_LAGD', 'REL_LAGS','INS_EA_CRANKCASE_PRSR_SEC_SEV_1', \
                    'INS_EA_COOLANT_TMP_SEC_SEV_1', 'INS_EA_OIL_PRSR_SEC_SEV_1', \
                    'INS_EA_OIL_TMP_SEC_SEV_1', 'INS_EA_SPEED_SEC_SEV_1', \
                    'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_1', 'INS_EA_CRANKCASE_PRSR_SEC_SEV_2', \
                    'INS_EA_COOLANT_TMP_SEC_SEV_2', 'INS_EA_OIL_PRSR_SEC_SEV_2', \
                    'INS_EA_OIL_TMP_SEC_SEV_2', 'INS_EA_SPEED_SEC_SEV_2', \
                    'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_2', \
                    'INS_EA_CRANKCASE_PRSR_SEC_SEV_3', 'INS_EA_COOLANT_TMP_SEC_SEV_3', \
                    'INS_EA_OIL_PRSR_SEC_SEV_3', \
                    'INS_EA_OIL_TMP_SEC_SEV_3', 'INS_EA_SPEED_SEC_SEV_3', \
                    'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_3', \
                    'INS_EA_CRANKCASE_PRSR_SEC_SEV_TOTAL', 'INS_EA_COOLANT_TMP_SEC_SEV_TOTAL', \
                    'INS_EA_OIL_PRSR_SEC_SEV_TOTAL', \
                    'INS_EA_OIL_TMP_SEC_SEV_TOTAL', 'INS_EA_SPEED_SEC_SEV_TOTAL', \
                    'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_TOTAL', \
                    'INS_DC_CMP_PERCENT_LOW_SPEED_TIME', \
                    'INS_DC_CMP_PERCENT_HIGH_SPEED_TIME', \
                    'INS_DC_CMP_PERCENT_ZERO_OR_NEGATIVE_TORQUE_TIME', \
                    'INS_DC_CMP_PERCENT_LOW_SPEED_LOW_TORQUE_TIME', 'INS_DC_CMP_PERCENT_LIGHT_LOAD_TIME', \
                    'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', 'INS_DC_CMP_PERCENT_CLIMBING_TIME', \
                    'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME', 'INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME', \
                    'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME', 'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME', \
                    'INS_TI_AVERAGE_ENGINE_LOAD_PERCENT', 'INS_TI_AVERAGE_ENGINE_SPEED_RPM', 'INS_TI_AVERAGE_VEHICLE_SPEED_MILES', \
                    'INS_TI_IDLE_TIME_PERCENTAGE', 'INS_TI_ENGINE_BRAKE_DISTANCE_MILES', \
                    'INS_TI_ENGINE_BRAKE_TIME_HOURS', 'INS_TI_ENGINE_DISTANCE_MILES', 'INS_TI_ENGINE_RUN_TIME_HOURS', \
                    'INS_TI_FUEL_USED_GALLON', 'INS_TI_FULL_LOAD_OPERATION_TIME_HOURS', 'INS_TI_IDLE_FUEL_USED_GALLON', \
                    'INS_TI_ECM_TIME_KEY_ON_TIME_HOURS', 'INS_TI_IDLE_TIME_HOURS', \
                    'INS_FNP_CRUISE_CONTROL_LOWER_DROOP_MPH', \
                    'INS_FNP_CRUISE_CONTROL_UPPER_DROOP_MPH', \
                    'INS_FNP_GEAR_DOWN_MAXIMUM_VEHICLE_SPEED_HEAVY_ENGINE_LOAD_MPH', \
                    'INS_FNP_GEAR_DOWN_MAXIMUM_VEHICLE_SPEED_LIGHT_ENGINE_LOAD_MPH', \
                    'INS_FNP_ROAD_SPEED_GOVERNOR_LOWER_DROOP_MPH', \
                    'INS_FNP_ROAD_SPEED_GOVERNOR_UPPER_DROOP_MPH', \
                    'INS_FNP_LOW_IDLE_SPEED_RPM', \
                    'INS_FNP_TIME_BEFORE_SHUTDOWN_HOURS', \
                    'INS_FNP_MAXIMUM_ACCELERATOR_VEHICLE_SPEED_MPH', \
                    'INS_FNP_MAXIMUM_CRUISE_CONTROL_SPEED_MPH', \
                    'INS_FNP_TIRE_SIZE_REVSPERMILE', \
                    'INS_FNP_RPM_BREAKPOINT_RPM', \
                    'INS_FNP_TORQUE_RAMP_RATE_HP', \
                    'EPAT_TEST_CNT_FAIL', \
                    'EPAT_TEST_CNT_PASS', \
                    'EPAT_TEST_CNT_INCOMPLETE', \
                    'EPAT_TEST_CNT_MATERIAL_REVIEW', \
                    'EPAT_TEST_CNT_OF_TEST', \
                    'EPAT_TEST_SUM_ENGINE_CELL_TIME', \
                    'EPAT_TEST_CNT_SHIFT_CODE', \
                    'EPAT_TEST_CNT_ABOVE_LIMIT', \
                    'EPAT_TEST_CNT_BELOW_LIMIT', \
                    'EPAT_TEST_MAX_BELOW_LIMIT_PERCENT', \
                    'EPAT_TEST_MAX_ABOVE_LIMIT_PERCENT', \
                    'EPAT_TEST_FAILED_CATEGORY_ASSEMBLY', \
                    'EPAT_TEST_FAILED_CATEGORY_ASSY_M', \
                    'EPAT_TEST_FAILED_CATEGORY_ASSY_M_LEAK', \
                    'EPAT_TEST_FAILED_CATEGORY_ASSY_M_REP', \
                    'EPAT_TEST_FAILED_CATEGORY_ASSY_X', \
                    'EPAT_TEST_FAILED_CATEGORY_ASSY_X_ETS', \
                    'EPAT_TEST_FAILED_CATEGORY_ASSY_X_LEAK', \
                    'EPAT_TEST_FAILED_CATEGORY_ASSY_X_REP', \
                    'EPAT_TEST_FAILED_CATEGORY_ETS_STAND', \
                    'EPAT_TEST_FAILED_CATEGORY_FPM', \
                    'EPAT_TEST_FAILED_CATEGORY_FPM_REPAIR', \
                    'EPAT_TEST_FAILED_CATEGORY_FUEL_LEAK_CH', \
                    'EPAT_TEST_FAILED_CATEGORY_HOT_TEST', \
                    'EPAT_TEST_FAILED_CATEGORY_ISX_OFFLINE', \
                    'EPAT_TEST_FAILED_CATEGORY_MANTA_STAND', \
                    'EPAT_TEST_FAILED_CATEGORY_PROGRAMMING', \
                    'EPAT_TEST_FAILED_CATEGORY_QR_REPAIR', \
                    'EPAT_TEST_FAILED_CATEGORY_REPAIR', \
                    'EPAT_TEST_FAILED_CATEGORY_REPAIR_3', \
                    'EPAT_TEST_FAILED_CATEGORY_REWORK', \
                    'EPAT_TEST_FAILED_CATEGORY_TEST_REPAIR', \
                    'EPAT_TEST_FAILED_CATEGORY_UPFIT', \
                    'EPAT_TEST_FAILED_ENGINE_SERIAL_AUT', \
                    'EPAT_TEST_FAILED_ENGINE_SERIAL_CNV', \
                    'EPAT_TEST_FAILED_ENGINE_SERIAL_ETS', \
                    'EPAT_TEST_FAILED_ENGINE_SERIAL_SPC', \
                    'EPAT_TEST_FAILED_ENGINE_SERIAL_SQC', \
                    'EPAT_TEST_FAILED_ENGINE_SERIAL_STD', \
                    'MES_TSH_CMP_NUM_OF_STATES_TRANSITION', \
                    'MES_TSH_CMP_AVG_TIME_CANCEL_MIN', \
                    'MES_TSH_CMP_AVG_TIME_FINISH_MIN', 'MES_TSH_CMP_AVG_TIME_HOLD_MIN', \
                    'MES_TSH_CMP_AVG_TIME_PROD_MIN', 'MES_TSH_CMP_AVG_TIME_PURGED_MIN', \
                    'MES_TSH_CMP_AVG_TIME_REPAIR_MIN', 'MES_TSH_CMP_AVG_TIME_SCHED_MIN', \
                    'MES_TSH_CMP_AVG_TIME_SHIP_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_CANCEL_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_FINISH_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_HOLD_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_PROD_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_PURGED_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_REPAIR_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_SCHED_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_SHIP_MIN',  \
                    'MES_ES_CMP_FINISHED_COUNT', \
                    'MES_ES_CMP_LINESET_COUNT', 'MES_ES_CMP_SHIPPED_COUNT', \
                    'MES_DA_CMP_CNTSTATION_BY_ESN', 'MES_DA_CMP_DURATION_ESN_MIN', \
                    'MES_DA_CMP_TPLOC_AS_MIN', \
                    'MES_DA_CMP_TPLOC_FP_MIN', 'MES_DA_CMP_TPLOC_HD_MIN', \
                    'MES_DA_CMP_TPLOC_SB_MIN', 'MES_DA_CMP_TPLOC_TS_MIN', \
                    'MES_DEV_COUNT_CMP', \
                    'MES_DEV_SUMMED_ADDED_PARTS_QTY_CMP', \
                    'MES_DEV_SUMMED_OMITTED_PARTS_QTY_CMP', \
                    'MES_DEV_AVG_DURATION_CMP', \
                    'MES_DEV_TOTAL_DURATION_CMP', \
                    'MES_DEV_MAX_DURATION_CMP', \
                    'MES_DEV_MIN_DURATION_CMP', \
                    'MES_DEV_ACTION_ADD_SUMMED_ADDED_PARTS_QTY_CMP', \
                    'MES_DEV_ACTION_ADD_SUMMED_OMITTED_PARTS_QTY_CMP', \
                    'MES_DEV_ACTION_ADD_AVG_DURATION_CMP', \
                    'MES_DEV_ACTION_ADD_TOTAL_DURATION_CMP', \
                    'MES_DEV_ACTION_ADD_MAX_DURATION_CMP', \
                    'MES_DEV_ACTION_ADD_MIN_DURATION_CMP', \
                    'MES_DEV_ACTION_OMIT_COUNT_CMP', \
                    'MES_DEV_ACTION_OMIT_SUMMED_ADDED_PARTS_QTY_CMP', \
                    'MES_DEV_ACTION_OMIT_SUMMED_OMITTED_PARTS_QTY_CMP', \
                    'MES_DEV_ACTION_OMIT_AVG_DURATION_CMP', \
                    'MES_DEV_ACTION_OMIT_TOTAL_DURATION_CMP', \
                    'MES_DEV_ACTION_OMIT_MAX_DURATION_CMP', \
                    'MES_DEV_ACTION_OMIT_MIN_DURATION_CMP', \
                    'MES_DEV_ACTION_QTY_COUNT_CMP', \
                    'MES_DEV_ACTION_QTY_SUMMED_ADDED_PARTS_QTY_CMP', \
                    'MES_DEV_ACTION_QTY_SUMMED_OMITTED_PARTS_QTY_CMP', \
                    'MES_DEV_ACTION_QTY_AVG_DURATION_CMP', \
                    'MES_DEV_ACTION_QTY_TOTAL_DURATION_CMP', \
                    'MES_DEV_ACTION_QTY_MAX_DURATION_CMP', \
                    'MES_DEV_ACTION_QTY_MIN_DURATION_CMP', \
                    'MES_DEV_ACTION_REPL_COUNT_CMP', \
                    'MES_DEV_ACTION_REPL_SUMMED_ADDED_PARTS_QTY_CMP', \
                    'MES_DEV_ACTION_REPL_SUMMED_OMITTED_PARTS_QTY_CMP', \
                    'MES_DEV_ACTION_REPL_AVG_DURATION_CMP', \
                    'MES_DEV_ACTION_REPL_TOTAL_DURATION_CMP', \
                    'MES_DEV_ACTION_REPL_MAX_DURATION_CMP', \
                    'MES_DEV_ACTION_REPL_MIN_DURATION_CMP', \
                    'MES_DEV_ACTION_ADD_MIN_DEVIATION_TIME_CMP', \
                    'MES_DEV_ACTION_OMIT_MIN_DEVIATION_TIME_CMP', \
                    'MES_DEV_ACTION_REPL_MIN_DEVIATION_TIME_CMP', \
                    'MES_DEV_ACTION_QTY_MIN_DEVIATION_TIME_CMP', \
                    'MES_DEV_NUM_DISTINCT_PARTS_CMP', \
                    'INS_FS_ACCELERATOR_PEDAL_OR_LEVER_POSITION_SENSOR_SUPPLY_VOLTAGE_VOLTS', \
                   'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_PSI', \
                   'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_QUALITY_PERCENT', \
                   'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE_F', \
                   'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_PSI', \
                   'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_PSI', \
                   'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE_F', \
                   'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_REGENERATION_PERMIT_SWITCH_STATUS_NA', \
                   'INS_FS_AFTERTREATMENT_FUEL_PRESSURE_PSI', \
                   'INS_FS_AFTERTREATMENT_INTAKE_NOX_CORRECTED_PPM', \
                   'INS_FS_AFTERTREATMENT_OUTLET_NOX_CORRECTED_PPM', \
                   'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE_F', \
                   'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE_F', \
                   'INS_FS_BAROMETRIC_AIR_PRESSURE_PSI', \
                   'INS_FS_BATTERY_VOLTAGE_VOLTS', \
                   'INS_FS_CRANKCASE_PRESSURE_PSI', \
                   'INS_FS_ECM_TIME_KEY_ON_TIME_HOURS', \
                   'INS_FS_EGR_DIFFERENTIAL_PRESSURE_PSI', \
                   'INS_FS_EGR_TEMPERATURE_F', \
                   'INS_FS_EGR_VALVE_POSITION_COMMANDED_PERCENT', \
                   'INS_FS_EGR_VALVE_POSITION_MEASURED_PERCENT_OPEN_PERCENT', \
                   'INS_FS_ENGINE_COOLANT_TEMPERATURE_F', \
                   'INS_FS_ENGINE_OIL_PRESSURE_PSI', \
                   'INS_FS_ENGINE_OIL_TEMPERATURE_F', \
                   'INS_FS_ENGINE_SPEED_RPM', \
                   'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED_F', \
                   'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3PS', \
                   'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED_PSI', \
                   'INS_FS_FUEL_RAIL_PRESSURE_MEASURED_PSI', \
                   'INS_FS_INTAKE_AIR_THROTTLE_POSITION_COMMANDED_PERCENT', \
                   'INS_FS_INTAKE_AIR_THROTTLE_POSITION_PERCENT', \
                   'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE_F', \
                   'INS_FS_INTAKE_MANIFOLD_PRESSURE_PSI', \
                   'INS_FS_KEYSWITCH_OFF_COUNTS_NA', \
                   'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE_F', \
                   'INS_FS_PERCENT_ACCELERATOR_PEDAL_OR_LEVER_PERCENT', \
                   'INS_FS_PERCENT_LOAD_PERCENT', \
                   'INS_FS_PERCENT_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_PERCENT', \
                   'INS_FS_SENSOR_SUPPLY_2_VOLTS', \
                   'INS_FS_SENSOR_SUPPLY_4_VOLTS', \
                   'INS_FS_SENSOR_SUPPLY_6_VOLTS', \
                   'INS_FS_TRANSMISSION_GEAR_RATIO_NA', \
                   'INS_FS_TRANSMISSION_GEAR_RATIO_NONE', \
                   'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_COMMANDED_PERCENT_CLOSED_PERCENT', \
                   'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_MEASURED_PERCENT_CLOSED_PERCENT', \
                   'INS_FS_TURBOCHARGER_COMPRESSOR_INLET_AIR_TEMPERATURE_CALCULATED_F', \
                   'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_CALCULATED_F', \
                   'INS_FS_TURBOCHARGER_SPEED_RPM', 'INS_FS_VEHICLE_SPEED_MPH', \
                   'INS_FS_ECM_TIME_HOURS', \
                   'INS_FS_EGR_ORIFICE_PRESSURE_PSI', \
                   'INS_FS_ENGINE_HOURS_HOURS', \
                   'INS_FS_EXHAUST_GAS_PRESSURE_PSI', \
                   'INS_FS_SENSOR_SUPPLY_1_VOLTS', \
                   'INS_FS_KEYSWITCH_ON_COUNTS_NA', \
                   'INS_FS_SENSOR_SUPPLY_3_VOLTS', \
                   'INS_FS_SENSOR_SUPPLY_5_VOLTS', \
                    'INS_FNP_REAR_AXLE_RATIO_NA', \
                    'INS_FNP_GEAR_DOWN_TRANSMISSION_RATIO_NA', \
                    'INS_FNP_TOP_GEAR_TRANSMISSION_RATIO_NA', \
                    'INS_FNP_GEAR_RATIO_THRESHOLD_NA']
        
        
    
    ## Add Green Wrench in later 
    # VPCR Columns
    vpcr_cols = ['VPCR_CMP_PIVOTED_VPCR19454', \
                 'VPCR_CMP_PIVOTED_VPCR19795', \
                 'VPCR_CMP_PIVOTED_VPCR19801', \
                 'VPCR_CMP_PIVOTED_VPCR20493', \
                 'VPCR_CMP_PIVOTED_VPCR21717', \
                 'VPCR_CMP_PIVOTED_VPCR22005', \
                 'VPCR_CMP_PIVOTED_VPCR22588', \
                 'VPCR_CMP_PIVOTED_VPCR23934', \
                 'VPCR_CMP_PIVOTED_VPCR26341', \
                 'VPCR_CMP_PIVOTED_VPCR26400', \
                 'VPCR_CMP_PIVOTED_VPCR26770', \
                 'VPCR_CMP_PIVOTED_VPCR27232', \
                 'VPCR_CMP_PIVOTED_VPCR27809', \
                 'VPCR_CMP_PIVOTED_VPCR30446', \
                 'VPCR_CMP_PIVOTED_VPCR32125', \
                 'VPCR_CMP_PIVOTED_VPCR32486', \
                 'VPCR_CMP_PIVOTED_VPCR33725', \
                 'VPCR_CMP_PIVOTED_VPCR33948', \
                 'VPCR_CMP_PIVOTED_VPCR34483', \
                 'VPCR_CMP_PIVOTED_VPCR36804', \
                 'VPCR_CMP_PIVOTED_VPCR36808', \
                 'VPCR_CMP_PIVOTED_VPCR36930', \
                 'VPCR_CMP_PIVOTED_VPCR41042']
    mes_cols = [  \
                '''
                'MES_DA_CMP_DURATION_BY_STN_000_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_001_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_002_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_003_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_004_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_005_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_006_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_007_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_008_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_009_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_010_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_011_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_012_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_013_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_014_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_015_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_016_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_017_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_018_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_019_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_022_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_023_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_024_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_025_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_026_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_027_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_028_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_029_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_030_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_031_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_032_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_033_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_034_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_035_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_036_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_037_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_038_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_039_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_040_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_041_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_042_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_043_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_044_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_045_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_046_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_047_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_048_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_049_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_050_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_051_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_052_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_053_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_054_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_055_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_056_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_057_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_058_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_059_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_060_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_061_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_062_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_063_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_064_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_065_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_066_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_067_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_068_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_069_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_070_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_071_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_072_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_073_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_074_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_075_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_076_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_077_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_078_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_079_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_080_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_081_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_082_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_083_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_084_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_085_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_088_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_089_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_090_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_091_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_092_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_093_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_105_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_107_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_181_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_182_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_183_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_184_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_186_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_187_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_S01_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_S02_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_S03_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_S04_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_S05_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_S06_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_S07_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_S08_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN_S09_MIN', \
                   'MES_DA_CMP_DURATION_BY_STN__1_AUD_MIN',
                   '''
                    ]

    good_cols = ['REL_IS_OFF_HIGHWAY']
    
    cols_to_split = ['REL_CMP_ENGINE_MILES_LIST', 'GREEN_WRENCH_FAULT_CODE_LIST']
    
    extra_cols = ['ID', 'ESN', 'EARLIEST_INDICATION_DATE', \
                  'REL_BUILD_DATE', \
                  'REL_IN_SERVICE_DATE', \
                  'REL_FAILURE_DATE', \
                  'REL_TRUCK_MODEL', \
                  'GREEN_WRENCH_FAULT_CODE_LIST', \
                  'REL_MINED_FAULT_CODES_LIST', \
                  'REL_OEM_GROUP', \
                  'REL_ANALYSIS_RATE_CAT', \
                  'REL_CMP_FAIL_CODE_LIST', \
                  'INS_TI_ENGINE_DISTANCE_MILES', \
                  'MES_DA_CMP_TPLOC_AS_MIN', \
                  'MES_DA_CMP_TPLOC_FP_MIN', \
                  'MES_DA_CMP_TPLOC_HD_MIN', \
                  'MES_DA_CMP_TPLOC_SB_MIN', \
                  'MES_DA_CMP_TPLOC_TS_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_CANCEL_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_FINISH_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_HOLD_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_PROD_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_PURGED_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_REPAIR_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_SCHED_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_SHIP_MIN', \
                  'INS_EA_CRANKCASE_PRSR_SEC_SEV_1', \
                  'INS_EA_COOLANT_TMP_SEC_SEV_1', \
                  'INS_EA_OIL_PRSR_SEC_SEV_1', \
                  'INS_EA_OIL_TMP_SEC_SEV_1', \
                  'INS_EA_SPEED_SEC_SEV_1', \
                  'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_1', \
                  'INS_EA_CRANKCASE_PRSR_SEC_SEV_2', \
                  'INS_EA_COOLANT_TMP_SEC_SEV_2', \
                  'INS_EA_OIL_PRSR_SEC_SEV_2', \
                  'INS_EA_OIL_TMP_SEC_SEV_2', \
                  'INS_EA_SPEED_SEC_SEV_2', \
                  'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_2', \
                  'INS_EA_CRANKCASE_PRSR_SEC_SEV_3', \
                  'INS_EA_COOLANT_TMP_SEC_SEV_3', \
                  'INS_EA_OIL_PRSR_SEC_SEV_3', \
                  'INS_EA_OIL_TMP_SEC_SEV_3', \
                  'INS_EA_SPEED_SEC_SEV_3', \
                  'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_3', \
                  'INS_EA_CRANKCASE_PRSR_SEC_SEV_TOTAL', \
                  'INS_EA_COOLANT_TMP_SEC_SEV_TOTAL', \
                  'INS_EA_OIL_PRSR_SEC_SEV_TOTAL', \
                  'INS_EA_OIL_TMP_SEC_SEV_TOTAL', \
                  'INS_EA_SPEED_SEC_SEV_TOTAL', \
                  'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_TOTAL', \
                  'INS_TI_FUEL_USED_GALLON', \
                  'INS_TI_IDLE_TIME_HOURS', \
                  'INS_TI_IDLE_TIME_PERCENTAGE', \
                  'INS_DC_CMP_PERCENT_LOW_SPEED_TIME', \
                  'INS_DC_CMP_PERCENT_HIGH_SPEED_TIME', \
                  'INS_DC_CMP_PERCENT_ZERO_OR_NEGATIVE_TORQUE_TIME', \
                  'INS_DC_CMP_PERCENT_LOW_SPEED_LOW_TORQUE_TIME', \
                  'INS_DC_CMP_PERCENT_LIGHT_LOAD_TIME', \
                  'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', \
                  'INS_DC_CMP_PERCENT_CLIMBING_TIME', \
                  'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME', \
                  'INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME', \
                  'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME', \
                  'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME']
    
    fault_code_cols = ['GREEN_WRENCH_FAULT_CODE_LIST', \
                  'REL_MINED_FAULT_CODES_LIST']
    
    #All duration columns are negative 
    duration_cols = ['MES_DEV_AVG_DURATION_CMP', \
                     'MES_DEV_TOTAL_DURATION_CMP', \
                     'MES_DEV_MAX_DURATION_CMP', \
                     'MES_DEV_MIN_DURATION_CMP', \
                     'MES_DEV_ACTION_ADD_AVG_DURATION_CMP', \
                     'MES_DEV_ACTION_ADD_TOTAL_DURATION_CMP', \
                     'MES_DEV_ACTION_ADD_MAX_DURATION_CMP', \
                     'MES_DEV_ACTION_ADD_MIN_DURATION_CMP', \
                     'MES_DEV_ACTION_OMIT_AVG_DURATION_CMP', \
                     'MES_DEV_ACTION_OMIT_TOTAL_DURATION_CMP', \
                     'MES_DEV_ACTION_OMIT_MAX_DURATION_CMP', \
                     'MES_DEV_ACTION_OMIT_MIN_DURATION_CMP', \
                     'MES_DEV_ACTION_QTY_AVG_DURATION_CMP', \
                     'MES_DEV_ACTION_QTY_TOTAL_DURATION_CMP', \
                     'MES_DEV_ACTION_QTY_MAX_DURATION_CMP', \
                     'MES_DEV_ACTION_QTY_MIN_DURATION_CMP', \
                     'MES_DEV_ACTION_REPL_AVG_DURATION_CMP', \
                     'MES_DEV_ACTION_REPL_TOTAL_DURATION_CMP', \
                     'MES_DEV_ACTION_REPL_MAX_DURATION_CMP', \
                     'MES_DEV_ACTION_REPL_MIN_DURATION_CMP']
    
    ins_fs_cat_cols = ['INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_REGENERATION_PERMIT_SWITCH_STATUS_NA', \
                       'INS_FS_AFTERTREATMENT_FUEL_INJECTOR_STATUS_NA', \
                       'INS_FS_AFTERTREATMENT_FUEL_SHUTOFF_VALVE_STATUS_NA', \
                       'INS_FS_AFTERTREATMENT_PURGE_AIR_SOLENOID_STATUS_NA', \
                       'INS_FS_AIR_CONDITIONING_PRESSURE_SWITCH_NA', \
                       'INS_FS_BRAKE_PEDAL_POSITION_SWITCH_NA', \
                       'INS_FS_CLUTCH_PEDAL_POSITION_SWITCH_NA', \
                       'INS_FS_CRANKCASE_VENTILATION_HEATER_COMMANDED_STATE_NA', \
                       'INS_FS_CRUISE_CONTROL_ON_OFF_SWITCH_NA', \
                       'INS_FS_EGR_FLOW_DERATE_NA', \
                       'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_2_NA', \
                       'INS_FS_ENGINE_BRAKE_OUTPUT_CIRCUIT_3_NA', \
                       'INS_FS_ENGINE_COOLANT_LEVEL_NA', \
                       'INS_FS_ENGINE_OPERATING_STATE_NA', \
                       'INS_FS_ENGINE_PROTECTION_DERATE_SUPPRESS_NA', \
                       'INS_FS_ENGINE_PROTECTION_SHUTDOWN_OVERRIDE_SWITCH_NA', \
                       'INS_FS_ENGINE_SPEED_MAIN_SENSOR_SIGNAL_STATE_NA', \
                       'INS_FS_ENGINE_SPEED_MAIN_SYNCHRONIZATION_STATE_NA', \
                       'INS_FS_ENGINE_SPEED_STATUS_NA', \
                       'INS_FS_ENGINE_TORQUE_MODE_NA', \
                       'INS_FS_FAN_CONTROL_SWITCH_NA', \
                       'INS_FS_FAN_DRIVE_STATE_NA', \
                       'INS_FS_KEYSWITCH_NA', \
                       'INS_FS_PTO_ON_OFF_SWITCH_NA', \
                       'INS_FS_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_SWITCH_NA', \
                       'INS_FS_REMOTE_PTO_SWITCH_NA', \
                       'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_DERATE_NA',
                       'INS_FS_TURBOCHARGER_SPEED_DERATE_ACTIVE_NA',
                       'INS_FS_WATER_IN_FUEL_STATE_NA', \
                       
                       
                       ]
    
    numeric_fs_cols = ['INS_FS_ACCELERATOR_PEDAL_OR_LEVER_POSITION_SENSOR_SUPPLY_VOLTAGE_VOLTS',
           'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_PRESSURE_PSI',
           'INS_FS_AFTERTREATMENT_DIESEL_EXHAUST_FLUID_QUALITY_PERCENT',
           'INS_FS_AFTERTREATMENT_DIESEL_OXIDATION_CATALYST_INTAKE_TEMPERATURE_F',
           'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_DIFFERENTIAL_PRESSURE_PSI',
           'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_PRESSURE_PSI',
           'INS_FS_AFTERTREATMENT_DIESEL_PARTICULATE_FILTER_OUTLET_TEMPERATURE_F',
           'INS_FS_AFTERTREATMENT_FUEL_PRESSURE_PSI',
           'INS_FS_AFTERTREATMENT_FUEL_SHUTOFF_VALVE_STATUS_NA',
           'INS_FS_AFTERTREATMENT_INTAKE_NOX_CORRECTED_PPM',
           'INS_FS_AFTERTREATMENT_OUTLET_NOX_CORRECTED_PPM',
           'INS_FS_AFTERTREATMENT_SCR_INTAKE_TEMPERATURE_F',
           'INS_FS_AFTERTREATMENT_SCR_OUTLET_TEMPERATURE_F',
           'INS_FS_AIR_CONDITIONING_PRESSURE_SWITCH_NA',
           'INS_FS_BAROMETRIC_AIR_PRESSURE_PSI', \
           'INS_FS_BATTERY_VOLTAGE_VOLTS',
           'INS_FS_CRANKCASE_PRESSURE_PSI',
           'INS_FS_ECM_TIME_HOURS',
           'INS_FS_ECM_TIME_KEY_ON_TIME_HOURS',
           'INS_FS_EGR_DIFFERENTIAL_PRESSURE_PSI',
           'INS_FS_EGR_ORIFICE_PRESSURE_PSI',
           'INS_FS_EGR_TEMPERATURE_F', 
           'INS_FS_EGR_VALVE_POSITION_COMMANDED_PERCENT',
           'INS_FS_EGR_VALVE_POSITION_MEASURED_PERCENT_OPEN_PERCENT',
           'INS_FS_ENGINE_COOLANT_TEMPERATURE_F',
           'INS_FS_ENGINE_HOURS_HOURS', 
           'INS_FS_ENGINE_OIL_PRESSURE_PSI',
           'INS_FS_ENGINE_OIL_TEMPERATURE_F',      
           'INS_FS_ENGINE_SPEED_RPM',
           'INS_FS_EXHAUST_GAS_PRESSURE_PSI',
           'INS_FS_EXHAUST_GAS_TEMPERATURE_CALCULATED_F',
           'INS_FS_EXHAUST_VOLUMETRIC_FLOWRATE_M3PS',
           'INS_FS_FUEL_RAIL_PRESSURE_COMMANDED_PSI',
           'INS_FS_FUEL_RAIL_PRESSURE_MEASURED_PSI',
           'INS_FS_INTAKE_AIR_THROTTLE_POSITION_COMMANDED_PERCENT',
           'INS_FS_INTAKE_AIR_THROTTLE_POSITION_PERCENT',
           'INS_FS_INTAKE_MANIFOLD_AIR_TEMPERATURE_F',
           'INS_FS_INTAKE_MANIFOLD_PRESSURE_PSI',
           'INS_FS_KEYSWITCH_OFF_COUNTS_NA', 'INS_FS_KEYSWITCH_ON_COUNTS_NA',
           'INS_FS_OEM_AMBIENT_AIR_TEMPERATURE_F',
           'INS_FS_PERCENT_ACCELERATOR_PEDAL_OR_LEVER_PERCENT',
           'INS_FS_PERCENT_LOAD_PERCENT',
           'INS_FS_PERCENT_REMOTE_ACCELERATOR_PEDAL_OR_LEVER_PERCENT',
           'INS_FS_SENSOR_SUPPLY_1_VOLTS', 'INS_FS_SENSOR_SUPPLY_2_VOLTS',
           'INS_FS_SENSOR_SUPPLY_3_VOLTS', 'INS_FS_SENSOR_SUPPLY_4_VOLTS',
           'INS_FS_SENSOR_SUPPLY_5_VOLTS', 'INS_FS_SENSOR_SUPPLY_6_VOLTS',
           'INS_FS_TRANSMISSION_GEAR_RATIO_NA',
           'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_COMMANDED_PERCENT_CLOSED_PERCENT',
           'INS_FS_TURBOCHARGER_ACTUATOR_POSITION_MEASURED_PERCENT_CLOSED_PERCENT',
           'INS_FS_TURBOCHARGER_COMPRESSOR_INLET_AIR_TEMPERATURE_CALCULATED_F',
           'INS_FS_TURBOCHARGER_COMPRESSOR_OUTLET_AIR_TEMPERATURE_CALCULATED_F',
           'INS_FS_TURBOCHARGER_SPEED_RPM',
           'INS_FS_VEHICLE_SPEED_MPH'
           ]
    
    uv_col_order = ['FAULT_CODE', \
                    'ID', \
                    'ESN', \
                    'EARLIEST_INDICATION_DATE', \
                    'INS_DC_CMP_PERCENT_LOW_SPEED_TIME', \
                    'INS_DC_CMP_PERCENT_HIGH_SPEED_TIME', \
                    'INS_DC_CMP_PERCENT_ZERO_OR_NEGATIVE_TORQUE_TIME', \
                    'INS_DC_CMP_PERCENT_LOW_SPEED_LOW_TORQUE_TIME', \
                    'INS_DC_CMP_PERCENT_LIGHT_LOAD_TIME', \
                    'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', \
                    'INS_DC_CMP_PERCENT_CLIMBING_TIME', \
                    'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME', \
                    'INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME', \
                    'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME', \
                    'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME', \
                    'INS_EA_CRANKCASE_PRSR_SEC_SEV_1', \
                    'INS_EA_COOLANT_TMP_SEC_SEV_1', \
                    'INS_EA_OIL_PRSR_SEC_SEV_1', \
                    'INS_EA_OIL_TMP_SEC_SEV_1', \
                    'INS_EA_SPEED_SEC_SEV_1', \
                    'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_1', \
                    'INS_EA_CRANKCASE_PRSR_SEC_SEV_2', \
                    'INS_EA_COOLANT_TMP_SEC_SEV_2', \
                    'INS_EA_OIL_PRSR_SEC_SEV_2', \
                    'INS_EA_OIL_TMP_SEC_SEV_2', \
                    'INS_EA_SPEED_SEC_SEV_2', \
                    'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_2', \
                    'INS_EA_CRANKCASE_PRSR_SEC_SEV_3', \
                    'INS_EA_COOLANT_TMP_SEC_SEV_3', \
                    'INS_EA_OIL_PRSR_SEC_SEV_3', \
                    'INS_EA_OIL_TMP_SEC_SEV_3', \
                    'INS_EA_SPEED_SEC_SEV_3', \
                    'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_3', \
                    'INS_EA_CRANKCASE_PRSR_SEC_SEV_TOTAL', \
                    'INS_EA_COOLANT_TMP_SEC_SEV_TOTAL', \
                    'INS_EA_OIL_PRSR_SEC_SEV_TOTAL', \
                    'INS_EA_OIL_TMP_SEC_SEV_TOTAL', \
                    'INS_EA_SPEED_SEC_SEV_TOTAL', \
                    'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_TOTAL', \
                    'INS_TI_ENGINE_DISTANCE_MILES', \
                    'INS_TI_FUEL_USED_GALLON', \
                    'INS_TI_IDLE_TIME_HOURS', \
                    'INS_TI_IDLE_TIME_PERCENTAGE', \
                    'Label', \
                    'MES_DA_CMP_TPLOC_AS_MIN', \
                    'MES_DA_CMP_TPLOC_FP_MIN', \
                    'MES_DA_CMP_TPLOC_HD_MIN', \
                    'MES_DA_CMP_TPLOC_SB_MIN', \
                    'MES_DA_CMP_TPLOC_TS_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_CANCEL_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_FINISH_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_HOLD_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_PROD_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_PURGED_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_REPAIR_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_SCHED_MIN', \
                    'MES_TSH_CMP_TOTAL_TIME_SHIP_MIN', \
                    'REL_ANALYSIS_RATE_CAT', \
                    'REL_BUILD_DATE', \
                    'REL_CMP_FAIL_CODE_LIST', \
                    'REL_FAILURE_DATE', \
                    'REL_IN_SERVICE_DATE', \
                    'REL_OEM_GROUP', \
                    'REL_TRUCK_MODEL']
    
    #universal View Columns - need to see if we can use the ordered universal view columns instead
    uv_cols = ['ID', 'ESN', 'EARLIEST_INDICATION_DATE', \
                  'REL_BUILD_DATE', \
                  'REL_IN_SERVICE_DATE', \
                  'REL_FAILURE_DATE', \
                  'REL_TRUCK_MODEL', \
                  'GREEN_WRENCH_FAULT_CODE_LIST', \
                  'REL_MINED_FAULT_CODES_LIST', \
                  'REL_OEM_GROUP', \
                  'REL_ANALYSIS_RATE_CAT', \
                  'REL_CMP_FAIL_CODE_LIST', \
                  'INS_TI_ENGINE_DISTANCE_MILES', \
                  'MES_DA_CMP_TPLOC_AS_MIN', \
                  'MES_DA_CMP_TPLOC_FP_MIN', \
                  'MES_DA_CMP_TPLOC_HD_MIN', \
                  'MES_DA_CMP_TPLOC_SB_MIN', \
                  'MES_DA_CMP_TPLOC_TS_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_CANCEL_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_FINISH_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_HOLD_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_PROD_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_PURGED_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_REPAIR_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_SCHED_MIN', \
                  'MES_TSH_CMP_TOTAL_TIME_SHIP_MIN', \
                  'INS_EA_CRANKCASE_PRSR_SEC_SEV_1', \
                  'INS_EA_COOLANT_TMP_SEC_SEV_1', \
                  'INS_EA_OIL_PRSR_SEC_SEV_1', \
                  'INS_EA_OIL_TMP_SEC_SEV_1', \
                  'INS_EA_SPEED_SEC_SEV_1', \
                  'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_1', \
                  'INS_EA_CRANKCASE_PRSR_SEC_SEV_2', \
                  'INS_EA_COOLANT_TMP_SEC_SEV_2', \
                  'INS_EA_OIL_PRSR_SEC_SEV_2', \
                  'INS_EA_OIL_TMP_SEC_SEV_2', \
                  'INS_EA_SPEED_SEC_SEV_2', \
                  'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_2', \
                  'INS_EA_CRANKCASE_PRSR_SEC_SEV_3', \
                  'INS_EA_COOLANT_TMP_SEC_SEV_3', \
                  'INS_EA_OIL_PRSR_SEC_SEV_3', \
                  'INS_EA_OIL_TMP_SEC_SEV_3', \
                  'INS_EA_SPEED_SEC_SEV_3', \
                  'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_3', \
                  'INS_EA_CRANKCASE_PRSR_SEC_SEV_TOTAL', \
                  'INS_EA_COOLANT_TMP_SEC_SEV_TOTAL', \
                  'INS_EA_OIL_PRSR_SEC_SEV_TOTAL', \
                  'INS_EA_OIL_TMP_SEC_SEV_TOTAL', \
                  'INS_EA_SPEED_SEC_SEV_TOTAL', \
                  'INS_EA_MANIFOLD_AIR_TMP_SEC_SEV_TOTAL', \
                  'INS_TI_FUEL_USED_GALLON', \
                  'INS_TI_IDLE_TIME_HOURS', \
                  'INS_TI_IDLE_TIME_PERCENTAGE', \
                  'INS_DC_CMP_PERCENT_LOW_SPEED_TIME', \
                  'INS_DC_CMP_PERCENT_HIGH_SPEED_TIME', \
                  'INS_DC_CMP_PERCENT_ZERO_OR_NEGATIVE_TORQUE_TIME', \
                  'INS_DC_CMP_PERCENT_LOW_SPEED_LOW_TORQUE_TIME', \
                  'INS_DC_CMP_PERCENT_LIGHT_LOAD_TIME', \
                  'INS_DC_CMP_PERCENT_CRUISE_MODE_TIME', \
                  'INS_DC_CMP_PERCENT_CLIMBING_TIME', \
                  'INS_DC_CMP_PERCENT_HIGH_SPEED_LOW_TORQUE_TIME', \
                  'INS_DC_CMP_PERCENT_HIGH_SPEED_HIGH_TORQUE_TIME', \
                  'INS_DC_CMP_PERCENT_LOW_SPEED_MEDIUM_TORQUE_TIME', \
                  'INS_DC_CMP_PERCENT_LOW_SPEED_HIGH_TORQUE_TIME']
    return(esn_col, cat_cols, numeric_cols, vpcr_cols, mes_cols, good_cols, cols_to_split, extra_cols, fault_code_cols, duration_cols, ins_fs_cat_cols, numeric_fs_cols, uv_col_order, uv_cols)
#%% ##########################
