#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 19 12:56:52 2018

@author: pr891
"""
#%%

import os
#print(os.getcwd())
from sys import platform
import importlib
#importlib.reload(isop)
import global_config as isop
import pandas as pd
import multiprocessing

#%%
if platform == 'darwin':
    path = "/Users/sanjay/Desktop/ds/Issue_Driver/Master"
elif platform == 'linux':
    path = "/usr/users/quantumdev/ds/Issue_Driver/Master"
elif platform == 'win32':
    path = '/Users/pr916/Documents/X15/ds/Issue_Driver/Master'
os.chdir(path)
         
issue_df = pd.ExcelFile(isop.incident_path.format(isop.esn_list))

test = issue_df.sheet_names

n_parallel = 4
max_num_cores = round((multiprocessing.cpu_count()-1)/n_parallel - .5)

os.system("rm -rf tmp/; mkdir -p tmp/") #change for Windows machines
index = 0
string = "#!/bin/bash\n# file: run_file.sh\n\n"
for t in test: 
    #write config file for the list of issues
    index = index + 1
    s = open("global_config.py").read()
    s = s.replace('esn_sheet = \"sample sheet name\"', 'esn_sheet = \"' + t +'\"')
    t = t.replace(" ","_")
    s = s.replace('issue_name = \'sample_name\'', 'issue_name = \'' + t + '\'')
    s = s.replace('max_num_cores = 8', 'max_num_cores = ' + str(max_num_cores))
    f = open("tmp/" + t + "_config.py", 'w')
    f.write(s)
    f.close()
    #write main file for list of issues
    s = open("ID_main.py").read()
    s = s.replace('import sample_config as isop', 'import '+ t + '_config' + ' as isop')
    f = open("tmp/ID_main_" + t + "_temp.py", 'w')
    f.write(s)
    f.close()
    #wrtie command to execute this file
    if platform == 'darwin':
        string = string + "python " + "ID_main_" + t + "_temp.py" + " > " + "ID_main_" + t + "_temp.log " 
    elif platform == 'linux':
        string = string + "/anaconda/envs/py36/bin/python3.6 " + "ID_main_" + t + "temp.py" + " > " + "ID_main_" + t + "_temp.log "         
    if not index % n_parallel == 0: 
        string = string + " &\n"
    else:
        string = string + " \n"

#write file to write the command that execute all of the file 
f = open("tmp/run_all.sh", 'w')
f.write(string)
f.close()
#set file permissions so that it can be executed 
os.chmod("tmp/run_all.sh", 0o777)
