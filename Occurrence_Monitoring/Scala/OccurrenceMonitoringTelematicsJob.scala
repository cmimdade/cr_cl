package mck.qb.columbia.oneoff

import mck.qb.library.Job
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col

object PullTelematics extends Job {

	import spark.implicits._
	/**
	* Intended to be implemented for every job created.
	*/

	override def run(): Unit = {
		// STORE GEO-FILTERED TELEMATICS DATA
		val qualityESNDF = spark.sql("SELECT engine_serial_num from reference.x15_esn_list").distinct()

		val tdfSQL = """SELECT 
				engine_serial_number,
				occurrence_date_time,
				fault_code,
				latitude as fclatitude,
				longitude as fclongitude
			FROM telematics.telematics_new"""
		
		val tdf = spark.sql(tdfSQL).join(qualityESNDF, qualityESNDF("engine_serial_num") === ($"engine_serial_number"))

		val wdfSQL = """SELECT *,
				latitude  - .145 as lat_lower_limit,
				latitude  + .145 as lat_upper_limit,
				longitude - .145 as long_lower_limit,
				longitude + .145 as long_upper_limit
			FROM quality_reliability.rld_wwsps"""
		
		val wdf = spark.sql(wdfSQL)

		val joindf = tdf.
			join(wdf, tdf("fclatitude")  > wdf("lat_lower_limit") && 
					  tdf("fclatitude")  < wdf("lat_upper_limit") && 
					  tdf("fclongitude") > wdf("long_lower_limit") && 
					  tdf("fclongitude") < wdf("long_upper_limit")).
			withColumn("GeoFence", lit("Within 10 Miles"))
			
		val telelocation_join_df = tdf.
			join(joindf, Seq("engine_serial_number", "occurrence_date_time", "fault_code", "fclatitude", "fclongitude"), "left_outer").
			drop(joindf("engine_serial_num")).
			withColumn("occurrence_date", date_format($"occurrence_date_time", "MM/dd/yyyy"))

		val telesummary = telelocation_join_df.
			filter($"GeoFence".isNull).
			groupBy("engine_serial_number", "fault_code", "occurrence_date").
			agg(count($"occurrence_date") as "count_occurrence_date")
			
		// Create new table
		telesummary.write.mode("overwrite").
			saveAsTable("telematics.telematics_location_features_filtered")
		
		
		// CREATE OCCURRENCE MONITORING DATASET
		// Define SQL statement
		val sqlString = """SELECT 
				a.engine_serial_number,
				a.fault_code,
				a.occurrence_date,
				a.count_occurrence_date,
				b.oem_code,
				b.application_code,
				b.build_date,
				b.build_qtr, 
				b.build_month,
				b.service_engine_model 
			FROM telematics.telematics_location_features_filtered a 
			LEFT JOIN quality_reliability.rld_engine_detail_mv b 
				ON a.engine_serial_number = b.engine_serial_num """
		
		// Save Parquet file of results
		val tableLocation = "wasbs://quantumbrserver@quantumb.blob.core.windows.net/data/REPORTING_TELEMATICS_FAULTS"
		
		spark.sql(sqlString).
			repartition(8).
			write.format("parquet").
			mode(SaveMode.Overwrite).
			option("header", true).
			option("compression", "none").
			save(tableLocation)
			
		spark.sql(sqlString).
			coalesce(1).
			write.format("csv").
			mode(SaveMode.Overwrite).
			option("header", true).
			option("compression", "none").
			save(tableLocation)
	}
}