package mck.qb.columbia.oneoff

import mck.qb.library.Job
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col

object PullTelematics extends Job {

	import spark.implicits._
	/**
	* Intended to be implemented for every job created.
	*/

	override def run(): Unit = {
		// Get ESNs that may be in-service
		val distinctDF = spark.table("QUALITY_COMPUTE.FULL_FEATURES").
						 select("ESN", "REL_BUILD_DATE").
						 distinct().
						 where("year(REL_BUILD_DATE) > 2013")

		distinctDF.createOrReplaceTempView("distinctDFV")

		val explodeDFSQL = """SELECT 
				ESN, 
				REL_BUILD_DATE, 
				'NA' as CODE, 
				date_add(date_add(t.REL_BUILD_DATE,60),pe.i) as D_DATE, 
				'EDS' as source 
				FROM distinctDFV t 
				lateral view posexplode(split(space(datediff(cast('2017-12-31' as date), date_add(REL_BUILD_DATE,60))),' ')) pe as i, x"""
				
		val explodeDF = spark.sql(explodeDFSQL).distinct

		val allDF = spark.table("QUALITY_COMPUTE.FULL_FEATURES").
					where("year(REL_BUILD_DATE) > 2013")

		val explodeFaultDF = allDF.
							 select($"ESN", $"REL_BUILD_DATE", to_date($"DSID_CREATE_DATE").alias("D_DATE"), explode($"GREEN_WRENCH_FAULT_CODE_LIST").alias("Exploded_Fault_Code")).
							 filter("DSID_CREATE_DATE is not null").
							 distinct

		val joinedDF = explodeDF.
					   join(explodeFaultDF, Seq("ESN", "REL_BUILD_DATE", "D_DATE"), "left_outer").
					   withColumn("CODE", coalesce($"Exploded_Fault_Code", $"CODE")).
					   drop("Exploded_Fault_Code")

		val eds = joinedDF.
				  groupBy($"ESN", $"REL_BUILD_DATE", concat(year($"D_DATE"), lit(" "), month($"D_DATE"), lit(" "), date_format($"D_DATE", "W")).alias("WEEK") , $"CODE").
				  agg(count($"CODE").alias("CNT_CODE"))

		val additinalDim = allDF.
							select($"ESN", 
								quarter($"REL_BUILD_DATE").alias("REL_QUATER_BUILD_DATE"), 
								month($"REL_BUILD_DATE").alias("REL_MONTH_BUILD_DATE"), 
								year($"REL_BUILD_DATE").alias("REL_YEAR_BUILD_DATE"), 
								$"REL_OEM_NAME", 
								$"REL_DESIGN_APPLICATION_CODE", 
								$"REL_BUSINESS_UNIT_NAME", 
								$"REL_ENGINE_NAME_DESC", 
								when(substring($"REL_ENGINE_NAME_DESC", 1,1) === "I", regexp_replace(split($"REL_ENGINE_NAME_DESC", " ").getItem(0), "ISX", "")).otherwise(split($"REL_ENGINE_NAME_DESC", " ").getItem(1)).alias("REL_CMP_ENGINE_NAME_DESC")).
							distinct()

		val eds_joined = eds.join(additinalDim, Seq("ESN"), "left_outer")

		val eds_joined1 = eds_joined.join(allNamesDF1, "ESN", "left_outer")

		eds_joined1.
			repartition(8).
			write.
			mode("overwrite").
			format("parquet").
			saveAsTable("QUALITY_COMPUTE.FAULT_EDS_WEEK_MASTER")
	}
}