-- Creates a table to track the different runs of schema versions

USE [om]
GO

/****** Object:  Table [dbo].[schema_migration_log]    Script Date: 8/1/2017 12:25:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[schema_migration_log](
	[timestamp] [datetime] NULL,
	[schema_version] [numeric](18, 0) NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[schema_migration_log] ADD  CONSTRAINT [DF_schema_migration_log_timestamp]  DEFAULT (getdate()) FOR [timestamp]
GO

-- Sets current schema to version 1.0
INSERT INTO [dbo].[schema_migration_log] ([schema_version])
VALUES (1.0)
GO