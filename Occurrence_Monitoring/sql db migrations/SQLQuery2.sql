USE [om]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [om-telematics](
	[ESN] [nvarchar](55) NULL,
	[REL_BUILD_DATE] [nvarchar](55) NULL,
	[MONDAY_OF_WEEK] [nvarchar](55) NULL,
	[CODE] [nvarchar](55) NULL,
	[COUNT] [nvarchar](55) NULL,
	[REL_QUARTER_BUILD_DATE] [nvarchar](55) NULL,
	[REL_MONTH_BUILD_DATE] [nvarchar](55) NULL,
	[REL_YEAR_BUILD_DATE] [nvarchar](55) NULL,
	[REL_OEM_NAME] [nvarchar](55) NULL,
	[REL_OEM_GROUP] [nvarchar](55) NULL,
	[REL_USER_APPL_DESC] [nvarchar](55) NULL,
	[REL_ENGINE_NAME_DESC] [nvarchar](55) NULL,
	[REL_SERVICE_ENGINE_MODEL] [nvarchar](55) NULL,
	[REL_CUSTOMER_NAME] [nvarchar](55) NULL,
	[REL_CMP_ENGINE_NAME] [nvarchar](55) NULL,
	[REL_MILAGE] [nvarchar](55) NULL,
	[SOURCE] [nvarchar](55) NULL,

 )
GO

SET ANSI_PADDING OFF
GO


