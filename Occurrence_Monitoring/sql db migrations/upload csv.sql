DELETE FROM lbot.lplan_system;

BULK INSERT lbot.lplan_system
    FROM 'C:\Users\Nick Rosener\Development\LBOT\data\LPLan_Oct29_onOct24_v3.csv'
    WITH
    (
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
    TABLOCK
    )