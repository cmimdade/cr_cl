-- temptbl2 shows when a field test trucks (that had the production calibrations) began field test for Acadia. The date of commencement of field test is 
-- likely earlier than the day when the trucks got the production cals . 

with temptbl as 
(SELECT 
	   truckid,
	   truckname, 
       matfiledate, 
       odometerstart 
FROM   trenddata 
WHERE  truckname IN (SELECT DISTINCT truckname 
                     FROM   trenddata 
                     WHERE  calibration IN ( '60.60.70.1', '60.70.70.1', 
                                             '60.70.70.2', 
                                             '60.70.17.2', 
                                             '60.70.17.15', '60.70.17.15' )) 
       AND odometerstart = 0.0 ),

-- 

temptbl2 AS (

	select truckid, truckname, min(matfiledate) as startdate from temptbl group by truckname, truckid
	)
	
-- The result of the query is a table that shows the faults logged on trucks past the commencement of field test trucks.

select Fault.TruckID, temptbl2.truckname, Application, OEM, Faultcode, FaultDate, OdometerStart, OdometerStartAdj from Fault JOIN
temptbl2 on temptbl2.TruckID = Fault.TruckID and temptbl2.startdate <= Fault.FaultDate JOIN
extra_trend_OdometerStartAdj on extra_trend_OdometerStartAdj.TruckID = Fault.TruckID and Fault.FaultDate  = extra_trend_OdometerStartAdj.MatfileDate
LEFT OUTER JOIN TblTruck on Fault.TruckID = TblTruck.TruckID order by Faultcode, TruckID, FaultDate