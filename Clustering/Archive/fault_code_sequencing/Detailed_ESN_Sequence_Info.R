DetailedEsnSequenceInfo <- function (esnSequenceDetails, sequencesWithSupport, filteredMbaResults) {
  # Calculate days difference between each of fault code sequence w.r.t each ESN 
  # 
  # Args:
  #   esnSequenceDetails    : Dataframe consisting of mapping between fault code sequences & ESNs
  #   sequencesWithSupport  : Full data consisting records for all the ESN's. This data will be used to calculate 
  #                           days difference between each fault code sequence w.r.t each ESN
  #   filteredMbaResults    : Dataframe consisting of the sequences for which days difference need to be calculated
  #                           It is used as a filter to select the required fault code sequences
  #
  # Returns:
  #   Dataframe consisting of days difference between each fault code sequence w.r.t each ESN
  
  # Get the data into dataframes 
  esnSequenceMap <- esnSequenceDetails    # mapping between fault seq & ESNs
  esnFaultData   <- sequencesWithSupport  # full Data from where days difference will be calculated
  filteredData   <- filteredMbaResults    # contains sequences for which processing needs to the done
  
  # Filter data based on sequences present in filteredData
  esnSequenceMap <- filter(esnSequenceMap,X1 %in% filteredData$sequence)
  
  # Start working on the sequence dataset - Clean it first for further processing
  # Change the name of the first column
  names(esnSequenceMap)[1] <- paste("fault.sequence")
  
  # Convert into Character
  esnFaultData$FAULT.COMBINATION <- as.character(esnFaultData$FAULT.COMBINATION)
  
  # Remove the sepcial characters from the "seq" column
  esnSequenceMap$fault.sequence  <- gsub("<", "", esnSequenceMap$fault.sequence)
  esnSequenceMap$fault.sequence  <- gsub(">", "", esnSequenceMap$fault.sequence)
  
  # Replace "{" with "(" and "}" with ")"
  esnSequenceMap$fault.sequence  <- gsub("{", '(', esnSequenceMap$fault.sequence, fixed = T)
  esnSequenceMap$fault.sequence  <- gsub("}", ')', esnSequenceMap$fault.sequence, fixed = T)
  esnSequenceMap$fault.sequence  <- gsub("),(", '):(', esnSequenceMap$fault.sequence, fixed = T)
  
  row.number <- 1
  finalResults <- NULL
  esnFaultData$sequence.length <- as.numeric(esnFaultData$sequence.length)
  
  # Read through the fault code sequence pattern and find the days difference between them 
  for (fault.seq in esnSequenceMap$fault.sequence)
  {
    # Get the row data
    esnMapRow       <- esnSequenceMap[row.number, ]
    # Count the number of non NA columns in the row
    count.esn       <- length(esnMapRow[1, ][!is.na(esnMapRow[1, ])])
    # Get number of ESNs linked with this sequence of fault code
    esnMapRow       <- as.list(esnMapRow[, 2:count.esn])
    # Clean the fault code sequence data
    split.fault.seq <- str_split(fault.seq,":")
    
    # Processing the records for each ESN
    for (individual.esn in esnMapRow)
    {
      esnFaultCodeCombined <- NULL
      esnFaultCodeIndividual <- NULL
      faultSeqGroup <- 1
      
      # Split the fault sequence into individual fault code
      for (fault.code in split.fault.seq[[1]])
      {
        fault.code       <- gsub("[()]", "", fault.code)
        # individual.esn   <- gsub("[()]", "", individual.esn)
        split.fault.code <- str_split(fault.code,",")
        iteration        <- 1 
        
        # For each fault code, get the data into a dataframe and assign unique group number to it
        # For each individual fault code from a sequence, it will be assigned a unique group number
        for (l in split.fault.code[[1]])
        {
          # For the first fault code in the sequence, create a new table
          if (iteration == 1)
          {
            exact.match            <- paste0("^",l,"$")
            esnFaultCodeIndividual <- esnFaultData[grep(exact.match, esnFaultData$FAULT.COMBINATION), ]  
          }
          # For the second and other fault codes, append the data into the dataframe created for first fault code
          if (iteration != 1)
          {
            exact.match            <- paste0("^",l,"$")
            esnFaultCodeIndividual <- esnFaultCodeIndividual[grep(exact.match, esnFaultCodeIndividual$FAULT.COMBINATION), ]    
          }
          # Select only those records where ESN match is found
          esnFaultCodeIndividual   <- dplyr::filter(esnFaultCodeIndividual, ESN %in% individual.esn)
          iteration                <- iteration + 1
        }
        # Order by occurrence date
        esnFaultCodeIndividual       <- esnFaultCodeIndividual[order(esnFaultCodeIndividual$Earliest.Indication.Date), ]
        # Assign unique group to each of the fault codes
        esnFaultCodeIndividual$group <- faultSeqGroup
        
        # In each iteration, below dataframe will consist of data for a single ESN with multiple fault codes, as retrieved 
        # from the fault code sequence
        esnFaultCodeCombined <- rbind.data.frame(esnFaultCodeCombined, esnFaultCodeIndividual)
        faultSeqGroup        <- faultSeqGroup + 1
      }
      
      # This will create a unique identifier flag, which will be used eventually to identify a record uniquely
      esnFaultCodeCombined$unique.identifier <- paste(esnFaultCodeCombined$Earliest.Indication.Date, esnFaultCodeCombined$group, sep = "")
      # Remove duplicate records based on unique values in the below mentioned columns
      esnFaultCodeCombined <- esnFaultCodeCombined[!duplicated(esnFaultCodeCombined[c("ESN","Earliest.Indication.Date","FAULT.COMBINATION","group","unique.identifier")]),]
      flag <- FALSE
      # This loop will help identify getting the minimum days difference between the seuquence of fault codes/combinations
      while (flag==FALSE)
      {
        # Get the maximum date of the first fault code & minimum date of the other fault codes 
        groupData     <- esnFaultCodeCombined %>% dplyr::group_by(group) %>% dplyr::summarise(date = max(Earliest.Indication.Date))
        groupData     <- dplyr::filter(groupData, group == 1)
        groupData_min <- esnFaultCodeCombined %>% dplyr::group_by(group) %>% dplyr::summarise(date = min(Earliest.Indication.Date))
        groupData_min <- dplyr::filter(groupData_min, group != 1)
        groupData     <- rbind(groupData,groupData_min)
        # Introduce a lag date column to find the days difference between fault groups
        groupData$lag.date <- lag(groupData$date,1)
        groupData$days <- groupData$date - groupData$lag.date
        # If the days difference between fault code/combination is less than equal to zero, take corrective action as 
        # define below
        checkRecords <- dplyr::filter(groupData,days <= 0)
        if (nrow(checkRecords) > 0) 
        {
          # Take only the first record, where days difference is less than equal to zero. It will help understand and 
          # debug the code later
          checkRecords <- head(checkRecords, 1)
          # Take corrective action incase the days difference is less than equal to zero, between group 1 (fault code 1)
          # and group 2 (fault code 2)
          if (checkRecords$group == 2)
          {
            # Check for the number of records in group 1 i.e. fault code 1
            noRowsGrp1 <- filter(esnFaultCodeCombined,group == 1)
            # If there is only a single record in group 1, we have to delete the record from group 2
            if (nrow(noRowsGrp1) == 1 & checkRecords$days <= 0)
            {
              unique.identifier.1 <- paste(checkRecords$date, (checkRecords$group), sep = "")
              esnFaultCodeCombined <- dplyr::filter(esnFaultCodeCombined, !(unique.identifier %in% unique.identifier.1))    
            }
            
            # If the number of records in group 1 is more than 1 and days difference is less than zero (not equal to zero)
            if (nrow(noRowsGrp1) != 1 & checkRecords$days != 0)
            {
              # Get the group wise maximum dates 
              getMaxDatePerGroup <- filter(esnFaultCodeCombined) %>% group_by(group) %>% summarise(max.date = max(Earliest.Indication.Date))
              
              # If Maximum date of group 1 is less than group 2, remove the record from group 2
              if (getMaxDatePerGroup$max.date[1] < getMaxDatePerGroup$max.date[2])
              {
                unique.identifier.1 <- paste(checkRecords$date, (checkRecords$group), sep="")
                esnFaultCodeCombined <- dplyr::filter(esnFaultCodeCombined, !(unique.identifier %in% unique.identifier.1))        
              }
              # If Maximum date of group 1 is more than group 2, remove the record from group 1
              if (getMaxDatePerGroup$max.date[1] > getMaxDatePerGroup$max.date[2])
              {
                unique.identifier.1 <- paste(checkRecords$lag.date, (checkRecords$group-1), sep = "")
                esnFaultCodeCombined <- dplyr::filter(esnFaultCodeCombined, !(unique.identifier %in% unique.identifier.1))        
              }
              # If Maximum date of group 1 is equal to group 2, remove the record from group 1
              if (getMaxDatePerGroup$max.date[1] == getMaxDatePerGroup$max.date[2])
              {
                unique.identifier.1 <- paste(checkRecords$lag.date, (checkRecords$group-1), sep = "")
                esnFaultCodeCombined <- dplyr::filter(esnFaultCodeCombined, !(unique.identifier %in% unique.identifier.1))        
              }
            }
            
            # If the number of records in group 1 is more than 0 and days difference is equal than zero, remove the record
            # from group 1
            if (nrow(noRowsGrp1) != 1 & checkRecords$days == 0)
            {
              unique.identifier.1 <- paste(checkRecords$lag.date, (checkRecords$group-1), sep = "")
              esnFaultCodeCombined <- dplyr::filter(esnFaultCodeCombined, !(unique.identifier %in% unique.identifier.1))    
            }
          }
          
          # Take corrective action incase the days difference is less than equal to zero, between any group except group 1
          
          # If days difference is less than equal to 0, remove the record from same group
          if (checkRecords$group != 2 & checkRecords$days <= 0) 
          {
            unique.identifier.1 <- paste(checkRecords$date, (checkRecords$group), sep = "")
            esnFaultCodeCombined <- dplyr::filter(esnFaultCodeCombined, !(unique.identifier %in% unique.identifier.1))  
          }
        }
        
        # If there is no corrective action required and everything has been sorted out, make flag as TRUE and get out of loop 
        if (nrow(checkRecords) == 0)
        {
          flag <- TRUE
        }
      }
      
      # After the above processing, we still have multiple records in each group. Sort the remaining data as per the 
      # occurrence date
      esnFaultCodeCombined <- esnFaultCodeCombined[order(esnFaultCodeCombined$Earliest.Indication.Date), ]
      
      # Select the record with maximum date for group 1
      esnFaultCodeCombined_1 <- filter(esnFaultCodeCombined,group == 1) %>% dplyr::group_by(group) %>% dplyr::slice(n()) %>% ungroup()
      
      # Select the record with minimum date for all groups except 1
      esnFaultCodeCombined_2 <- filter(esnFaultCodeCombined,group != 1) %>% dplyr::group_by(group) %>% dplyr::slice(1) %>% ungroup()
      
      # Rbind the datasets 
      esnFaultCodeCombined <- rbind.data.frame(esnFaultCodeCombined_1, esnFaultCodeCombined_2)
      
      # For each ESN, we will create a flag that will help get the lag date specific to that ESN only 
      esnFaultCodeCombined$group.lag <- cumsum(c(0,as.numeric(diff(esnFaultCodeCombined$fault_code)) != 0))
      esnFaultCodeCombined <- esnFaultCodeCombined %>% dplyr::group_by(ESN,group.lag) %>% 
        dplyr::mutate(lag.date = dplyr::lag(Earliest.Indication.Date, n = 1, default = NA))
      
      # Create a flag that will help identify the records from a particular sequence
      esnFaultCodeCombined$main_group <- row.number
      
      # Create a flag that will help identify the records from a particular sequence
      esnFaultCodeCombined$original.seq <- fault.seq
      
      # Move this data to other dataframe, that will eventually contain the results
      finalResults <- rbind.data.frame(finalResults, esnFaultCodeCombined)
    }
      # Create a progress flag that will help identify the process running status
    progress <- round((row.number/nrow(esnSequenceMap) * 100),digits = 2)
    print(paste(progress," %"," process completed and ",row.number," row(s) processed" ,sep = ""))
    
    # Move to the next row for processing
    row.number <- row.number + 1
  }
  
  # Remove un-necessary column fields
  finalResults$unique.identifier <- NULL
  finalResults$group.lag <- NULL
  
  # Correct the original sequence field format
  finalResults$original.seq <- gsub("(", '{', finalResults$original.seq, fixed = T)
  finalResults$original.seq <- gsub(")", '}', finalResults$original.seq, fixed = T)
  finalResults$original.seq <- gsub("}:{", '},{', finalResults$original.seq, fixed = T)
  
  finalResults$original.seq <- paste("<",finalResults$original.seq,sep="")
  finalResults$original.seq <- paste(finalResults$original.seq,">",sep="")
  
  # Calculate days difference column and perfome other cleansing procesess
  finalResults$days.diff <- finalResults$Earliest.Indication.Date - finalResults$lag.date
  finalResults$days.diff[is.na(finalResults$days.diff)] <- "" 
  finalResults$lag.date <- NULL 
  
  # Write the results into a CSV file
  write.csv(finalResults,"results.csv",row.names = FALSE)
  
  # Return the finalResults data frame
  return(finalResults)
}
