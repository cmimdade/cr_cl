# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 15:27:29 2018

@author: pg911
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 15:51:59 2018

@author: pg911
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Jan 10 11:22:12 2018

@author: pg911
"""

#%%

################# import python libraries #############################################################

import pandas as pd

import numpy as np

import os

from scipy.spatial import distance

import matplotlib.pyplot as plt

import seaborn as sea

from collections import Counter  # same as table used in R

import operator

import random

import re   ### it is used for string/character search  (search on google "regular expression in python")

from datetime import datetime, timedelta

from stop_words import get_stop_words

from nltk.stem import PorterStemmer

from sklearn.metrics import confusion_matrix

all_words = get_stop_words('english').copy()


#%%

##### setting current working directory

os.chdir("set your working directory path here")

#%%

####################### working on new eds data sent by HITESH

####################### analyzing claims data

#%%

#################################################################################################################
master_file = pd.read_csv("new eds data.csv")


#### create dummy of issue (as 1) and not_issue (as 0) from column 'Issue Number'
master_file['issue_dummy'] = np.where(master_file['Issue Number']=='Not_Issue',0,1)


#### filtering Claims data only
master_file = master_file.loc[master_file['Incident Source']=="Claim"]

master_file.reset_index(inplace=True,drop=True)



#%%


#%%


##### adding 'Mileage column to data
master_file['Mileage'] = master_file['ENGINE MILES'].copy()

data = pd.concat([data,master_file[['Mileage']]],axis=1)

### boolen for missing value in Mileage
bool = list(data.Mileage.isnull())

#### dropping rows which having missing Mileage info
data = data.loc[np.logical_not(bool)]

data.reset_index(inplace=True,drop=True)

#%%

col = 'Fail Code'

OEM = pd.DataFrame(master_file.groupby(col)['issue_dummy'].agg(['count','sum']).reset_index())


OEM.sort('count',inplace=True)

OEM

    
OEM['Others'] = np.nan
for i in range(len(OEM[col])):
    if OEM['count'][i]<10:
        OEM.Others[i] = 'Others'
    else:
        OEM.Others[i] = OEM[col][i]
        
OEM
          
   
OEM1 = pd.DataFrame(OEM.groupby('Others')[['count','sum']].sum().reset_index()) 

OEM1['perct']  =  OEM1['sum']/OEM1['count']

OEM1.sort('perct',inplace=True,ascending=False)

OEM1.reset_index(inplace=True,drop=True)

#OEM1.drop(np.where(OEM1.Others=='Others')[0],axis=0,inplace=True)

#OEM1 = OEM1.reindex([1,0,3,4,2])

OEM1.rename(columns={'Others':col},inplace=True)

print(OEM1)

#%%


##### categorize fail codes into different categories.

#others = list(OEM['Fail Code'][OEM.Others=="Others"])

Fail_code_90 = list(OEM1['Fail Code'][OEM1.perct>=.9])

Fail_code_80_90 = list(OEM1['Fail Code'][(OEM1.perct>=.8) & (OEM1.perct<.9)])

Fail_code_60_80 = list(OEM1['Fail Code'][(OEM1.perct>=.6) & (OEM1.perct<.8)])

Fail_code_40_60 = list(OEM1['Fail Code'][(OEM1.perct>=.4) & (OEM1.perct<.6)])

Fail_code_20_40 = list(OEM1['Fail Code'][(OEM1.perct>=.2) & (OEM1.perct<.4)])

Fail_code_1_20 = list(OEM1['Fail Code'][(OEM1.perct>0) & (OEM1.perct<.2)])

Fail_code_0 = list(OEM1['Fail Code'][OEM1.perct==0])

Others = list(OEM['Fail Code'][OEM['Others']=='Others'])

Fail_code_1_20.pop(np.where(np.array(Fail_code_1_20)=="Unknown")[0])

Fail_code_20_40.pop(np.where(np.array(Fail_code_20_40)=="Others")[0])


#%%

####### creating new columns based on above category

master_file['Fail_code_90'] = np.where(master_file['Fail Code'].isin(Fail_code_90),1,0)

master_file['Fail_code_80_90'] = np.where(master_file['Fail Code'].isin(Fail_code_80_90),1,0)

master_file['Fail_code_60_80'] = np.where(master_file['Fail Code'].isin(Fail_code_60_80),1,0)

master_file['Fail_code_40_60'] = np.where(master_file['Fail Code'].isin(Fail_code_40_60),1,0)

master_file['Fail_code_20_40'] = np.where(master_file['Fail Code'].isin(Fail_code_20_40),1,0)

master_file['Fail_code_1_20'] = np.where(master_file['Fail Code'].isin(Fail_code_1_20),1,0)

master_file['Fail_code_0'] = np.where(master_file['Fail Code'].isin(Fail_code_0),1,0)

master_file['unknown'] = np.where(master_file['Fail Code']=='Unknown',1,0)

master_file['Others'] = np.where(master_file['Fail Code'].isin(Others),1,0)


#%%

## used below columns for modelling

col = ['issue_dummy','CAUSE','COMPLAINT','Fail_code_90','Fail_code_80_90',
       'Fail_code_60_80','Fail_code_40_60','Fail_code_20_40','Fail_code_1_20','Fail_code_0','unknown','Others']


data = master_file[col].copy()

data['CAUSE'] = data['CAUSE'].str.lower()
data['COMPLAINT'] = data['COMPLAINT'].str.lower()


data.fillna('',inplace=True)


data['final_text'] = data['CAUSE']+' '+data['COMPLAINT']
 
data['final_text'] = data['final_text'].str.replace(r'[^a-z0-9]+',' ') ## remove special charcters

data['final_text'] = data['final_text'].str.replace(r' [a-z]{,2} ',' ') ##remove words having length 2 at most, (removing word "on" too)

data['final_text'] = data['final_text'].str.replace(r'\s+ ',' ')  # removing multiple spaces by one space

data['final_text'] = data['final_text'].str.replace(r'not ([a-z]+)','not_\\1')

data['final_text'] = data['final_text'].str.replace(r'([a-z]+) off ','\\1_off ')

data['final_text'] = data['final_text'].str.replace(r'promotion other see text for description',' ')

data['final_text'] = data['final_text'].str.replace(r'technician prom id list [a-z][a-z][0-9]+',' ')

data['final_text'] = data['final_text'].str.replace(r'per engineer \b\w+\b ',' ')

data['final_text'] = data['final_text'].str.replace(r'per engineering \b\w+\b ',' ')

data['final_text'] = data['final_text'].str.replace(r' [0-9]+ ',' ')

data['final_text'] = data['final_text'].str.replace(r' [0-9]+$',' ')

data['final_text'] = data['final_text'].str.replace(r'\b[0-9]+\b',' ')

data['final_text'] = data['final_text'].str.replace(r'^[0-9]+ ','')

data['final_text'] = data['final_text'].str.replace(r'out of ([a-z]+)','out_of_\\1')

data['final_text'] = data['final_text'].str.replace(r'out of ([a-z]+)','out_of_\\1') 

data['final_text'] = data['final_text'].str.replace(r' [a-z][0-9]+',' ')  

data['final_text'] = data['final_text'].str.replace(r'\s+ ',' ') 

data['final_text'] = data['final_text'].str.strip()

data.reset_index(inplace=True,drop=True)


#%%


##### adding 'Mileage column to data
master_file['Mileage'] = master_file['ENGINE MILES'].copy()

data = pd.concat([data,master_file[['Mileage']]],axis=1)

### boolen for missing value in Mileage
bool = list(data.Mileage.isnull())

#### dropping rows which having missing Mileage info
data = data.loc[np.logical_not(bool)]

data.reset_index(inplace=True,drop=True)


#%%

###### normalizing (words stemming) words in Final_text column

ps = PorterStemmer()
 
data['final_text'] = data["final_text"].apply(lambda x: ' '.join([ps.stem(y) for y in x.split(' ')]))  


data.reset_index(inplace=True,drop=True)

#%%


######################## creating TFIDF matrix on Final text data

from sklearn.feature_extraction.text import TfidfVectorizer

## use bi-gram 
tfidf_Vectorizer = TfidfVectorizer(ngram_range=(2, 2),stop_words=all_words,max_features=500,use_idf=True)

model_tfidf = tfidf_Vectorizer.fit_transform(data['final_text'])

model_tfidf.shape

model_tfidf_feat = tfidf_Vectorizer.get_feature_names()  ### storing features names

                                                     
#%%


### combining tfidf data matrix with Mileage

data.reset_index(inplace=True,drop=True)


data1 = pd.DataFrame(model_tfidf.todense())


data1  = pd.concat([data1,data[['Mileage','Fail_code_90','Fail_code_80_90',
       'Fail_code_60_80','Fail_code_40_60','Fail_code_20_40','Fail_code_1_20','Fail_code_0','unknown','Others']]],axis=1)

#%%


### storing feature names
feature_names = model_tfidf_feat.copy()

feature_names.extend(['Mileage','Fail_code_90','Fail_code_80_90',
       'Fail_code_60_80','Fail_code_40_60','Fail_code_20_40','Fail_code_1_20','Fail_code_0','unknown','Others'])


#%%


### normalizing data

from sklearn import preprocessing

data1 = preprocessing.scale(data1)


#%%

#### splitting data into train (80%) and test (20%)

from sklearn.model_selection import train_test_split


X_train, X_validation, y_train, y_validation = train_test_split(data1, data.issue_dummy, train_size=0.80,random_state=425)


X_train = pd.DataFrame(X_train)

y_train = pd.Series(y_train)


#%%

##### use grid search to get optimal value of marameters using 5 fold cross validation

#### this iterations can be done multiple possible combination of set of parameters based on computer RAM and runtime


from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import GridSearchCV

#model_gb = GradientBoostingClassifier(learning_rate=.02, n_estimators = 500,).fit(X_train, y_train)

model_gb = GradientBoostingClassifier()

param = {'learning_rate':[.03],'n_estimators':[300,500],'max_depth':[3,4],'subsample':[.9],'max_features':[.8,.9]}


pred_gb = GridSearchCV(model_gb,param,cv=5)

pred_gb.fit(X_train,y_train)

print(pred_gb.best_estimator_)  ### it gives best estimater

print(pred_gb.best_score_)   ## it gives best accuracy with the optimal set of parameters

     
#%%

################### apply model on best parameters

model_gb = GradientBoostingClassifier(learning_rate = .03,n_estimators=500,max_depth=4,subsample=.9,max_features=.8)

model_gb.fit(X_train,y_train)


pred_gb_tst = model_gb.predict(X_validation)


print(confusion_matrix(y_validation,pred_gb_tst))  ## get confusion matrix



#%%
#### get features importances and plot



features_imp = list(model_gb.feature_importances_)

#feature_names = model_tfidf_feat.copy()

#feature_names.extend(['Mileage'])

features_imp_data = pd.DataFrame({'feature':feature_names,'score':features_imp})

features_imp_data['fscore'] = features_imp_data['score']/features_imp_data['score'].sum()

features_imp_data.sort(columns=['fscore'],ascending=True,inplace=True)

features_imp_data.reset_index(inplace=True,drop=True)



#### plot top 20 features

features_imp_data1 = features_imp_data.iloc[(features_imp_data.shape[0]-20):,:]

plt.figure()
features_imp_data1.plot()
features_imp_data1.plot(kind='barh', x='feature', y='fscore', legend=False, figsize=(6, 10))


plt.title('Gradient Boosting: Top 20 Feature Importance')
plt.xlabel('relative importance')

plt.show()


#%%


############################### AUC CURVE
######################################################################

from sklearn.metrics import roc_curve, auc

y_score  = model_gb.decision_function(X_validation)
# Compute ROC curve and ROC area for each class
fpr = dict()
tpr = dict()
roc_auc = dict()
#for i in range(2):
fpr[0], tpr[0], _ = roc_curve(y_validation, y_score)
roc_auc[0] = auc(fpr[0], tpr[0])

# Compute micro-average ROC curve and ROC area
fpr["micro"], tpr["micro"], _ = roc_curve(y_validation.ravel(), y_score.ravel())
roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])


plt.figure()
lw = 2
plt.plot(fpr[0], tpr[0], color='red',
         lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[0])
plt.plot([0, 1], [0, 1], color='green', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example \n (Gradient Boosting Classifier)')
plt.legend(loc="lower right")
plt.show()


#%%








