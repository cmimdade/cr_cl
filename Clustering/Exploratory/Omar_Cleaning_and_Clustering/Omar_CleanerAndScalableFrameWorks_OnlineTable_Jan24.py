#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 16 17:12:20 2018

@author: pg875
"""
########################################################################################################
#Librairies
from collections import Counter
from tqdm import tqdm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
import datetime
import copy
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.ensemble import RandomForestClassifier
from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import Birch
import time
from sklearn.model_selection import train_test_split
########################################################################################################









########################################################################################################
#This code is not optimized and the functions still contains global variables
########################################################################################################










########################################################################################################
#Plot style
plt.style.use('bmh')
plt.rcParams['figure.figsize'] = (8.0, 6.0)
    
#Import data    
incident_tracker = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/')

file_1 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00000-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_2 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00001-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_3 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00002-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_4 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00003-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_5 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00004-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_6 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00005-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_7 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00006-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_8 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00007-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()

online_tracker = pd.concat([file_1, file_2, file_3, file_4, file_5, file_6, file_7, file_8])
incident_tracker_df = online_tracker.copy()

#Fail Code
##Clean
def replace_null(string,df):
    df.loc[:, string] = [np.array([], dtype = 'object') if x else v \
                                            for x,v in zip(df[string].isnull(), df[string])]
    return(df)

string = 'CMP_FAIL_CODE_LIST'
incident_tracker_df = replace_null(string, incident_tracker_df)

##Build list
def concatenate(l):
    out = []
    for x in l:
        for y in x:
            out.append(y)
    return(out)
    

fail_code_count = Counter(concatenate(incident_tracker_df['CMP_FAIL_CODE_LIST'])).most_common() 

fail_code_count_df = pd.DataFrame(
    {'Fail_Code': [x[0] for x in fail_code_count],
     'Occurence': [x[1] for x in fail_code_count]
    })   
    
threshold = fail_code_count_df['Occurence'].quantile(.95)
fail_code_list = fail_code_count_df.query('Occurence > @threshold')['Fail_Code']    
fail_code_first_2_list = list(set(fail_code_list.apply(lambda x: x[:2])))
fail_code_last_2_list = list(set(fail_code_list.apply(lambda x: x[2:])))

##Build Columns
fail_code_col_names = []
###Build Fail Code

for fc in tqdm(fail_code_list):
    incident_tracker_df['Fail_Code_' + fc] = [list(x).count(fc)
                                                 for x in incident_tracker_df['CMP_FAIL_CODE_LIST']]
    fail_code_col_names.extend(['Fail_Code_' + fc])

ac = 'Unkwown'
incident_tracker_df['Fail_Code_' + ac] = [1 if len(x) == 0 else 0
                                           for x in incident_tracker_df['CMP_FAIL_CODE_LIST']]
fail_code_col_names.extend(['Fail_Code_' + ac])

###Build 2 first digits
for fc in tqdm(fail_code_first_2_list):
    incident_tracker_df['Fail_Code_Pre_' + fc] = [list(x).count(fc)
                                                 for x in incident_tracker_df['CMP_FAIL_CODE_LIST'].\
                                                     apply(lambda x: [y[:2] for y in x])]
    fail_code_col_names.extend(['Fail_Code_Pre_' + fc])
    
ac = 'Unkwown'
incident_tracker_df['Fail_Code_Pre_' + ac] = [1 if len(x) == 0 else 0
                                           for x in incident_tracker_df['CMP_FAIL_CODE_LIST']]
fail_code_col_names.extend(['Fail_Code_Pre_' + ac])

###Build 2 last digits    
for fc in tqdm(fail_code_last_2_list):
    incident_tracker_df['Fail_Code_App_' + fc] = [list(x).count(fc)
                                                 for x in incident_tracker_df['CMP_FAIL_CODE_LIST'].\
                                                     apply(lambda x: [y[2:] for y in x])]
    fail_code_col_names.extend(['Fail_Code_App_' + fc])
    
ac = 'Unkwown'
incident_tracker_df['Fail_Code_App_' + ac] = [1 if len(x) == 0 else 0
                                           for x in incident_tracker_df['CMP_FAIL_CODE_LIST']]
fail_code_col_names.extend(['Fail_Code_App_' + ac])

#Fault Code
##Clean

#string = 'NON_GREEN_WRENCH_FAULT_CODE_LIST'
#incident_tracker_df = replace_null(string, incident_tracker_df)
#incident_tracker_df['NON_GREEN_WRENCH_FAULT_CODE_LIST'] = incident_tracker_df['NON_GREEN_WRENCH_FAULT_CODE_LIST'].apply(lambda x: list(map(str,x)))

string = 'GREEN_WRENCH_FAULT_CODE_LIST'
incident_tracker_df = replace_null(string, incident_tracker_df)
incident_tracker_df['GREEN_WRENCH_FAULT_CODE_LIST'] = incident_tracker_df['GREEN_WRENCH_FAULT_CODE_LIST'].apply(lambda x: list(map(str,x)))

##Build list
    
#fault_code_count = Counter(concatenate(incident_tracker_df['NON_GREEN_WRENCH_FAULT_CODE_LIST'])).most_common() 

fault_code_count = Counter(concatenate(incident_tracker_df['GREEN_WRENCH_FAULT_CODE_LIST'])).most_common() 

fault_code_count_df = pd.DataFrame(
    {'Fault_Code': [x[0] for x in fault_code_count],
     'Occurence': [x[1] for x in fault_code_count]
    })   
    
threshold = fault_code_count_df['Occurence'].quantile(.95)
fault_code_list = fault_code_count_df.query('Occurence > @threshold')['Fault_Code']    

##Build Columns
fault_code_col_names = []
###Build Fault Code


#column = 'NON_GREEN_WRENCH_FAULT_CODE_LIST'
column = 'GREEN_WRENCH_FAULT_CODE_LIST'

for fc in tqdm(fault_code_list):
    incident_tracker_df['Fault_Code_' + fc] = [list(x).count(str(fc))
                                                 for x in incident_tracker_df[column]]
    fault_code_col_names.extend(['Fault_Code_' + fc])

ac = 'Unkwown'
incident_tracker_df['Fault_Code_' + ac] = [1 if len(x) == 0 else 0
                                           for x in incident_tracker_df[column]]
fail_code_col_names.extend(['Fault_Code_' + ac])


##Miles
incident_tracker_df = replace_null('CMP_ENGINE_MILES_LIST', incident_tracker_df)

incident_tracker_df['LAST_MILE_RECORDED'] = \
    incident_tracker_df.CMP_ENGINE_MILES_LIST.apply(lambda x:x[-1] if len(x) > 0 else None)

###impute missing by claims column

miles_mean = incident_tracker_df.LAST_MILE_RECORDED.mean()
incident_tracker_df['Miles_Imputed'] = incident_tracker_df.LAST_MILE_RECORDED.\
        apply(lambda x: float(x) if not(pd.isnull(x)) else miles_mean)

miles_treshold = incident_tracker_df.Miles_Imputed.quantile(.99)
incident_tracker_df.loc[incident_tracker_df.Miles_Imputed > miles_treshold, 'Miles_Imputed'] = np.nan

###Log Miles
incident_tracker_df["Miles_Log_Normalized"] = [np.nan if pd.isnull(m) else 0 if m == 0 else np.log(m) 
                                                  for m in incident_tracker_df["Miles_Imputed"]]

incident_tracker_df["Miles_Log_Normalized"].fillna(incident_tracker_df["Miles_Log_Normalized"].mean(), 
                                                      inplace = True)

incident_tracker_df["Miles_Log_Normalized"] = (incident_tracker_df["Miles_Log_Normalized"] - \
   incident_tracker_df["Miles_Log_Normalized"].mean()) / \
   incident_tracker_df["Miles_Log_Normalized"].var()**0.5
    

    
##Age
incident_tracker_df = replace_null('CMP_FAIL_DATE_LIST', incident_tracker_df)


incident_tracker_df['Last_Mile_Record_Time'] = \
    incident_tracker_df.CMP_FAIL_DATE_LIST.apply(lambda x:datetime.datetime.strptime(x[-1], '%Y-%m-%d') if len(x) > 0 else None)
     
incident_tracker_df['AGE'] = \
    (incident_tracker_df.Last_Mile_Record_Time - \
     incident_tracker_df.DSID_CREATE_DATE).apply(lambda x: x.days)

###Log Age  
incident_tracker_df["AGE_Log_Normalized"] = [np.nan if pd.isnull(m) else 0 if m <= 0 else np.log(m) 
                                                  for m in incident_tracker_df["AGE"]]

incident_tracker_df["AGE_Log_Normalized"].fillna(incident_tracker_df["AGE_Log_Normalized"].mean(), 
                                                      inplace = True)

incident_tracker_df["AGE_Log_Normalized"] = (incident_tracker_df["AGE_Log_Normalized"] - \
   incident_tracker_df["AGE_Log_Normalized"].mean()) / \
   incident_tracker_df["AGE_Log_Normalized"].var()**0.5
   
   
##Net Amount
mean_net_amount = incident_tracker_df.CMP_SUM_NET_AMOUNT.mean()
incident_tracker_df['Net_Amount_Imputed'] = incident_tracker_df.CMP_SUM_NET_AMOUNT.\
        apply(lambda x: float(x) if not(pd.isnull(x)) else mean_net_amount)
 
###Log Net Amount       
incident_tracker_df["Net_Amount_Log_Normalized"] = [np.nan if pd.isnull(m) else 0 if m == 0 else np.log(m) 
                                                  for m in incident_tracker_df["CMP_SUM_NET_AMOUNT"]]

incident_tracker_df["Net_Amount_Log_Normalized"].fillna(incident_tracker_df["Net_Amount_Log_Normalized"].mean(), 
                                                      inplace = True)

incident_tracker_df['Net_Amount_Imputed'] = (incident_tracker_df['Net_Amount_Imputed'] - \
   incident_tracker_df['Net_Amount_Imputed'].mean()) / \
   incident_tracker_df['Net_Amount_Imputed'].var()**0.5    
#########################################################################################









#########################################################################################
#EARLIEST DATE
#Missing count
incident_tracker_df['Missing'] = incident_tracker_df['DSID'].isnull()
incident_tracker_df.query('Missing == True')['CMP_FAIL_DATE_LIST'].apply(lambda x: len(set(x)) <= 1).mean()
#Clean
string = 'CMP_CLAIM_DATE_LIST'
incident_tracker_df = replace_null(string, incident_tracker_df)
incident_tracker_df['EARLIEST_DATE'] = incident_tracker_df['CMP_CLAIM_DATE_LIST'].apply(lambda x: min(x) if len(x) > 0 else None)
incident_tracker_df['EARLIEST_DATE'] = incident_tracker_df['EARLIEST_DATE'].apply(lambda x:datetime.datetime.strptime(str(x), '%Y-%m-%d') if x != None else None)
incident_tracker_df['EARLIEST_DATE'] = incident_tracker_df['EARLIEST_DATE'].where(incident_tracker_df['EARLIEST_DATE'].notnull(), incident_tracker_df['DSID_CREATE_DATE'])
#########################################################################################










##############################################################################################
#Clustering
clustering_features = []
clustering_features.extend(["Miles_Log_Normalized"])
clustering_features.extend(fault_code_col_names)
clustering_features.extend(fail_code_col_names)
clustering_features.extend(["AGE_Log_Normalized"])
clustering_features.extend(["Net_Amount_Imputed"])

X = incident_tracker_df.loc[:, clustering_features].values

#Model
def clustering_model(data=X, weights=[1,1,1,1,1,1,1], k = 10, algorithm = 'hierarchical'): #Weights = [Miles,Fault Code,Fail Code, First 2 digits, Last 2 Digits]
    #data should be = X or one its subsets
    Xcf = copy.copy(data)
    nfault = len(fault_code_list)
    nfail = len(fail_code_list)+1
    nfirst = len(fail_code_first_2_list)+1
    nlast = len(fail_code_last_2_list)+1
    Xcf[:,0] = weights[0]*Xcf[:,0]
    Xcf[:,1:(nfault+1)] = weights[1]*Xcf[:,1:(nfault+1)]
    Xcf[:,(nfault+1):(nfault+nfail+1)] = weights[2]*Xcf[:,(nfault+1):(nfault+nfail+1)]
    Xcf[:,(nfault+nfail+1):(nfault+nfail+nfirst+1)] = weights[3]*Xcf[:,(nfault+nfail+1):(nfault+nfail+nfirst+1)] 
    Xcf[:,(nfault+nfail+nfirst+1):(nfault+nfail+nfirst+nlast+1)] = weights[4]*Xcf[:,(nfault+nfail+nfirst+1):(nfault+nfail+nfirst+nlast+1)]
    Xcf[:,nfault+nfail+nfirst+nlast+1] = weights[5]*Xcf[:,nfault+nfail+nfirst+nlast+1]
    Xcf[:,-1] = weights[6]*Xcf[:,-1]
    if algorithm == 'hierarchical':
        model = AgglomerativeClustering(linkage='average', n_clusters = k)
    if algorithm == 'dbscan':
        model = DBSCAN()
    if algorithm == 'kmeans':
        model = KMeans(n_clusters=k, random_state=42)
    if algorithm == 'birch':
        model = Birch(branching_factor=50, n_clusters=k, threshold=0.5,compute_labels=True)
    if algorithm == 'minibatch':
        model = MiniBatchKMeans(n_clusters=k)
    model.fit(Xcf)
    #cluster_test = cluster.k_means(Xcf, n_clusters=k)
    #return(cluster_test)
    return(model)

#Table    
def get_clustering_table(df= incident_tracker_df, weights=[1,1,1,1,1,1,1], n_cluster=25, algorithm = 'hierarchical'):
    #df sould be = incident_tracker_df
    clustering_result = df.loc[:, ['DSIS','ESN', 'CMP_FAIL_DATE_LIST', 'CMP_FAIL_CODE_LIST'
                                                       'NON_GREEN_WRENCH_FAULT_CODE_LIST', 'Miles_Imputed','Net_Amount_Imputed','AGE']]
    clustering_result['Cluster_ID'] = clustering_model(weights=weights,k=n_cluster, algorithm = algorithm).labels_
    return(clustering_result)
######################################################################################### 















    
######################################################################################### 
##Sumsample and predict   
def subsample_predict(X=X, df=incident_tracker_df, m=20000, k=80, weights=[1.5, 4.25, 4.25, 0.25, 2.75, 1.5, 0.5]):
    #X = X
    #df = incident_tracker_df
    #subsample
    X_train = X[0:m,:]
    #cluster
    clustering_results = clustering_model(data=X_train,k=k,weights=weights)
    clustering_table = df.loc[:, ['DSIS','ESN', 'CMP_FAIL_DATE_LIST', 'CMP_FAIL_CODE_LIST'
                                                   'GREEN_WRENCH_FAULT_CODE_LIST', 'Miles_Imputed','Net_Amount_Imputed','AGE']]
    clustering_table['Cluster_ID'] = 0
    clustering_table['Cluster_ID'][0:m] = clustering_results.labels_
    #predict
    predict_model = RandomForestClassifier()
    predict_model.fit(X_train, clustering_results.labels_)
    predicted_clusters= predict_model.predict(X)
    return(predicted_clusters)


#Accuracy Test
m = 20000
X_sub = X[0:m,:]
clustering_results = clustering_model(data=X_sub,k=80,weights=[1.5, 4.25, 4.25, 0.25, 2.75, 1.5, 0.5])
X_train_, X_test_, y_train_, y_test_ = train_test_split(X_sub, clustering_results.labels_, test_size=0.2)
predict_model_ = RandomForestClassifier()
predict_model_.fit(X_train_, y_train_)
#Out-of-sample R^2
predicted_clusters_= predict_model_.predict(X_test_)
print((predicted_clusters_ == y_test_).mean())
#########################################################################################   









##The next two section are not cleaned and are obselete - one can use the clustering_model() function directly
######################################################################################### 
##Birch
#Threshold tuning 
#
size = 20000
l = []
for i in range(1,20):
    brc = Birch(branching_factor=50, n_clusters=None, threshold=float(i/4),compute_labels=True)
    start = time.time()
    brc.partial_fit(X[0:size,:])
    s = (len(set(brc.predict(X[0:size,:]))))
    l.append(s)
    end = time.time()
    print(end-start)
print(l) #We choose threshold = 2.75

#Partial fit - speed & size
brc = Birch(branching_factor=50, n_clusters=None, threshold=2.75,compute_labels=True)
size = 20000
step = 10000
cursor = 0
for i in range(10):
    start = time.time()
    brc.partial_fit(X[cursor:(cursor+size),:])
    print(len(set(brc.predict(X[cursor:(cursor+size),:]))))
    end = time.time()
    cursor = cursor + step
    print(end-start)
######################################################################################### 










######################################################################################### 
##Mini batch
m_batch = MiniBatchKMeans(n_clusters=80)
m = 100000
start = time.time()
m_batch.fit(X[0:m,:])
end = time.time()
print(end-start)
######################################################################################### 















