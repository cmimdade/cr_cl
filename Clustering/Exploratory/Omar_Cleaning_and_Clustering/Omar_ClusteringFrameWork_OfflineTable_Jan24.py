#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 14:16:10 2018

@author: pg875
"""
########################################################################################################
#Libraries
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import copy
import random
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import KMeans
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
from sklearn.cluster import DBSCAN
#from sklearn import manifold
from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import Birch
import time
########################################################################################################









########################################################################################################
#This code is not optimized and the functions still contains global variables
########################################################################################################










########################################################################################################
#Plot Style
plt.style.use('bmh')
plt.rcParams['figure.figsize'] = (8.0, 6.0)

#Import Data
os.chdir("/Users/pg875/Desktop/Columbia_quality/")
incident_trakcer_df = pd.read_csv(filepath_or_buffer='incidents_tracker_cleaned_Jan09th.csv')

#Clean Data
incident_clustering_df = incident_trakcer_df.loc[:, ['Incident #','DSID', 'ESN', 'Fail Code', 'Fail Code.1', 
                                                     'Mileage/ Engine Hours', 'ENGINE MILES', 
                                                     'FAULT COMBINATION', 
                                                     #'Issue Number'
                                                     'Issue_Label', 'OEM', 'total_net_amount', 'Earliest Indication Date', 'IN SERVICE DATE']]

incident_clustering_df.columns = ['Incident_Number','DSID', 'ESN', 'Fail_Code_manual', 'Fail_Code_Claim', 
                                  'Miles_EDS', 'Miles_Claims', 'Fault_Codes', 'Issue_Number','OEM', 'Net_Amount', 'Claim_Date','Build_Date']

incident_clustering_df.loc[incident_clustering_df.OEM.isnull(),'OEM'] = 'UNASSIGNED'

#Clean date
#def clean_date(x):
 #   try:
  #      return(datetime.datetime.strptime(x[0:10], '%Y-%m-%d'))
   # except:
    #    return('unknown')

        
incident_clustering_df.loc[incident_clustering_df.Issue_Number == ' ', 'Issue_Number'] = 'Not_Issue'
incident_clustering_df['Issue_Number'] = incident_clustering_df['Issue_Number'].\
    apply(lambda x: x.lower().replace(' ',''))
    
incident_clustering_df['OEM'] = incident_clustering_df['OEM'].\
    apply(lambda x: x.lower().replace(' ','_').replace(',','_'))

##Fail Code    
incident_clustering_df['Fail_Code_Cleaned'] = [fcc if fcm == 'Unknown' and not pd.isnull(fcc) else fcm 
                                               for fcm, fcc in zip(incident_clustering_df.Fail_Code_manual,
                                                                   incident_clustering_df.Fail_Code_Claim)]

(incident_clustering_df['Fail_Code_Cleaned'] != incident_clustering_df['Fail_Code_manual']).sum()

fail_code_list = incident_clustering_df.Fail_Code_Cleaned.value_counts().\
    reset_index().query('Fail_Code_Cleaned >= 10')['index']


    
fail_code_first_2_list = list(set(fail_code_list.apply(lambda x: x[:2] if x != 'Unknown' else x)))
fail_code_last_2_list = list(set(fail_code_list.apply(lambda x: x[2:] if x != 'Unknown' else x)))

fail_code_col_names = []
for fc in fail_code_list:
    incident_clustering_df['Fail_Code_' + fc] = [1 if fc in x else 0 
                                                 for x in incident_clustering_df['Fail_Code_Cleaned']]
    fail_code_col_names.extend(['Fail_Code_' + fc])

for fc in fail_code_first_2_list:
    incident_clustering_df['Fail_Code_Pre_' + fc] = [1 if fc in x else 0 
                                                 for x in incident_clustering_df['Fail_Code_Cleaned'].\
                                                     apply(lambda x: x[:2] if x != 'Unknown' else x)]
    fail_code_col_names.extend(['Fail_Code_Pre_' + fc])
    
for fc in fail_code_last_2_list:
    incident_clustering_df['Fail_Code_App_' + fc] = [1 if fc in x else 0 
                                                 for x in incident_clustering_df['Fail_Code_Cleaned'].\
                                                     apply(lambda x: x[2:] if x != 'Unknown' else x)]
    fail_code_col_names.extend(['Fail_Code_App_' + fc])
    
    
##OEM
oem_list = incident_clustering_df.OEM.value_counts().\
    reset_index().query('OEM >= 5')['index']
    
oem_col_names = []
for oem in oem_list:
    incident_clustering_df['OEM_' + oem] = [1 if oem in x else 0 
                                                 for x in incident_clustering_df['OEM']]
    fail_code_col_names.extend(['OEM_' + oem])

    
    
    
##Fault Code
fault_code_list = [5867, 559, 6766, 4572, 1713, 687, 195, 4691, 285, 1867, 4658, 3383, 1685, 3567, 
                   425, 3574, 1942, 3526, 4277, 2375, 2222, 1683, 197, 3232, 3238, 6339,
                   3563, 125, 6767, 1682, 3845, 3559, 3382, 2387, 3237, 6255, 4732, 3151, 
                   2288, 3572, 3241, 272, 'ENGINE LEAK Oil Leak', 'ENGINE LEAK External Coolant', 
                   'ENGINE PERFORMANCE SYMPTOM Low Power', 'ENGINE LEAK Internal Coolant Leak', 
                   'ENGINE STARTING SYMPTOM Crank No Start', 'AFTERTREATMENT SYMPTOM Excessive DEF Consumption', 
                   'ENGINE LEAK External Fuel Leak']

fault_code_list = [str(x) for x in fault_code_list]

fault_code_list_manual_gp = list()
fault_code_list_manual_gp.extend([[2375, 1867]])
fault_code_list_manual_gp.extend([[5867, 1713, 195, 3237, 4572]])
fault_code_list_manual_gp.extend([[5989, 125, 687, 2288]])
fault_code_list_manual_gp.extend([[4616, 125, 6432]])
fault_code_list_manual_gp.extend([[3316, 3315, 3318]])
fault_code_list_manual_gp.extend([[5935, 1887, 3568]])
fault_code_list_manual_gp.extend([["ENGINE LEAK Oil Leak External"]])
fault_code_list_manual_gp.extend([[425]])
fault_code_list_manual_gp.extend([[687, 2288]])
fault_code_list_manual_gp.extend([['ENGINE STARTING SYMPTOM Crank No Start', '4691', '559']])
fault_code_list_manual_gp = [[str(i) for i in j] for j in fault_code_list_manual_gp]

fault_code_list_manual_gp_set = set([x for y in fault_code_list_manual_gp for x in y])

fault_code_list_filtered = list(set(fault_code_list) - fault_code_list_manual_gp_set)

fault_code_col_names = []
for fc in fault_code_list_filtered:
    fault_code_col_names.extend(['Fault_Code_' + fc])
    incident_clustering_df['Fault_Code_' + fc] = [1 if not pd.isnull(x) and fc in x else 0 

                                                  for x in incident_clustering_df['Fault_Codes']]

##Net Amount
###Impute net amount

mean_net_amount = incident_clustering_df.Net_Amount.mean()
incident_clustering_df['Net_Amount_Imputed'] = incident_clustering_df.Net_Amount.\
        apply(lambda x: float(x) if not(pd.isnull(x)) else mean_net_amount)
        
incident_clustering_df["Net_Amount_Log_Normalized"] = [np.nan if pd.isnull(m) else 0 if m == 0 else np.log(m) 
                                                  for m in incident_clustering_df["Net_Amount"]]

incident_clustering_df["Net_Amount_Log_Normalized"].fillna(incident_clustering_df["Net_Amount_Log_Normalized"].mean(), 
                                                      inplace = True)

incident_clustering_df['Net_Amount_Imputed'] = (incident_clustering_df['Net_Amount_Imputed'] - \
   incident_clustering_df['Net_Amount_Imputed'].mean()) / \
   incident_clustering_df['Net_Amount_Imputed'].var()**0.5
  
#incident_clustering_df["Net_Amount_Imputed"] = (incident_clustering_df["Net_Amount_Imputed"] - \
 #                     incident_clustering_df["Net_Amount_Imputed"].min()) / \
  #                    (incident_clustering_df["Net_Amount_Imputed"].max() - incident_clustering_df["Net_Amount_Imputed"].min())

##Miles
###impute miles in EDS column
    
incident_clustering_df['Miles_Imputed'] = incident_clustering_df.Miles_EDS.\
        apply(lambda x: ("".join(_ for _ in x.split('/')[0] if _ in ".1234567890")).replace(' ','') if isinstance(x, str)
                  else np.nan).replace('', np.nan, regex=True).\
        apply(lambda x: float(x))

###impute missing by claims column
incident_clustering_df['Miles_Imputed'] = [m_imputed if ~pd.isnull(m_imputed) else float(mc) 
                                           for m_imputed, mc in zip(incident_clustering_df.Miles_Imputed, 
                                                                    incident_clustering_df.Miles_Claims)]

###select a cutoff for miles
miles_treshold = incident_clustering_df.Miles_Imputed.quantile(.99)
incident_clustering_df.loc[incident_clustering_df.Miles_Imputed > miles_treshold, 'Miles_Imputed'] = np.nan
incident_clustering_df["Miles_Imputed"].hist(bins = 100)
plt.show()

###Log Miles
incident_clustering_df["Miles_Log_Normalized"] = [np.nan if pd.isnull(m) else 0 if m == 0 else np.log(m) 
                                                  for m in incident_clustering_df["Miles_Imputed"]]

incident_clustering_df["Miles_Log_Normalized"].fillna(incident_clustering_df["Miles_Log_Normalized"].mean(), 
                                                      inplace = True)

incident_clustering_df["Miles_Log_Normalized"] = (incident_clustering_df["Miles_Log_Normalized"] - \
   incident_clustering_df["Miles_Log_Normalized"].mean()) / \
   incident_clustering_df["Miles_Log_Normalized"].var()**0.5
  
#incident_clustering_df["Miles_Log_Normalized"] = (incident_clustering_df["Miles_Log_Normalized"] - \
 #                     incident_clustering_df["Miles_Log_Normalized"].min()) / \
  #                    (incident_clustering_df["Miles_Log_Normalized"].max() - incident_clustering_df["Miles_Log_Normalized"].min())
  


#Greenwrench
incident_clustering_df['Green_Wrench_Order'] = incident_trakcer_df['Green Wrench'].apply(lambda x: 'n' if pd.isnull(x) else str(x)[0])

green_wrench_list = list(set(incident_clustering_df['Green_Wrench_Order']))
green_wrench_col_names = []
for gw in green_wrench_list:
    incident_clustering_df['GREEN_WRENCH_' + gw] = [1 if gw == x else 0 
                                                 for x in incident_clustering_df['Green_Wrench_Order']]
    green_wrench_col_names.extend(['GREEN_WRENCH_' + gw])
########################################################################################################










########################################################################################################
#Clustering
clustering_features = []
clustering_features.extend(["Miles_Log_Normalized"])
clustering_features.extend(fault_code_col_names)
clustering_features.extend(fail_code_col_names)
clustering_features.extend(oem_col_names)
clustering_features.extend(["Net_Amount_Imputed"])
clustering_features.extend(green_wrench_col_names)

X = incident_clustering_df.loc[:, clustering_features].values
########################################################################################################










########################################################################################################
def clustering_model(data = X, weights=[1,1,1,1,1,1,1,1], k = 10, algorithm = 'hierarchical'): #Weights = [Miles,Fault Code,Fail Code, First 2 digits, Last 2 Digits]
    Xcf = copy.copy(X)
    nfault = len(fault_code_list_filtered)
    nfail = len(fail_code_list)
    nfirst = len(fail_code_first_2_list)
    nlast = len(fail_code_last_2_list)
    noem = len(oem_list)
    #ngw = len(green_wrench_list)
    Xcf[:,0] = weights[0]*Xcf[:,0]
    Xcf[:,1:(nfault+1)] = weights[1]*Xcf[:,1:(nfault+1)]
    Xcf[:,(nfault+1):(nfault+nfail+1)] = weights[2]*Xcf[:,(nfault+1):(nfault+nfail+1)]
    Xcf[:,(nfault+nfail+1):(nfault+nfail+nfirst+1)] = weights[3]*Xcf[:,(nfault+nfail+1):(nfault+nfail+nfirst+1)] 
    Xcf[:,(nfault+nfail+nfirst+1):(nfault+nfail+nfirst+nlast+1)] = weights[4]*Xcf[:,(nfault+nfail+nfirst+1):(nfault+nfail+nfirst+nlast+1)]
    Xcf[:,(nfault+nfail+nfirst+nlast+1):(nfault+nfail+nfirst+nlast+noem+1)] = weights[5]*Xcf[:,(nfault+nfail+nfirst+nlast+1):(nfault+nfail+nfirst+nlast+noem+1)]
    Xcf[:,(nfault+nfail+nfirst+nlast+noem+1):(nfault+nfail+nfirst+nlast+noem+2)] = weights[6]*Xcf[:,(nfault+nfail+nfirst+nlast+noem+1):(nfault+nfail+nfirst+nlast+noem+2)]
    Xcf[:,(nfault+nfail+nfirst+nlast+noem+2):] = weights[7]*Xcf[:,(nfault+nfail+nfirst+nlast+noem+2):]
    if algorithm == 'hierarchical':
        model = AgglomerativeClustering(linkage='average', n_clusters = k)
    if algorithm == 'dbscan':
        model = DBSCAN()
    if algorithm == 'kmeans':
        model = KMeans(n_clusters=k, random_state=42)
    if algorithm == 'birch':
        model = Birch(branching_factor=50, n_clusters=k, threshold=0.5,compute_labels=True)
    if algorithm == 'minibatch':
        model = MiniBatchKMeans(n_clusters=k)
    model.fit(Xcf)
    #cluster_test = cluster.k_means(Xcf, n_clusters=k)
    #return(cluster_test)
    return(model)
########################################################################################################










########################################################################################################
def get_kpi1(clustering_result):
    avg_num_issues = 0
    for i in np.unique(clustering_result.Cluster_ID):
        cur_issues = set(clustering_result.query('Cluster_ID == @i').Issue_Number)
        avg_num_issues = avg_num_issues + len(cur_issues)
        if 'not_issue' in cur_issues:
            avg_num_issues = avg_num_issues -1
            
    return(avg_num_issues / len(set(clustering_result.Cluster_ID)))

def get_kpi2(clustering_result, k = 5):         
    significant_clusters = clustering_result.Cluster_ID.value_counts().reset_index().query('Cluster_ID > @k')['index']
    return(clustering_result.query('Cluster_ID in @significant_clusters').query('Issue_Number != "not_issue"').groupby(['Cluster_ID','Issue_Number']).size().reset_index().groupby(['Cluster_ID'])[0].apply(lambda x: max(x)/sum(x)).mean())
    
def get_kpi3(clustering_result, k=5):   
    avg_num_clusters = 0
    top_issues = clustering_result.Issue_Number.value_counts().reset_index().query('Issue_Number > @k')['index']
    top_issues = set(top_issues) - {'not_issue'}
    for cur_issue in top_issues:
        cur_clusters = set(clustering_result.query('Issue_Number == @cur_issue').Cluster_ID)
        avg_num_clusters = avg_num_clusters + len(cur_clusters)
        
    avg_num_clusters = avg_num_clusters / len(top_issues)
    return(avg_num_clusters)         
    
def get_kpi4(clustering_result, k=5):  
    top_issues = clustering_result.Issue_Number.value_counts().reset_index().query('Issue_Number > @k')['index']
    top_issues = set(top_issues) - {'not_issue'}
    return(clustering_result.query('Issue_Number in @top_issues').groupby(['Cluster_ID','Issue_Number']).size().reset_index().groupby(['Issue_Number'])[0].apply(lambda x: max(x)/sum(x)).mean())
    
def penalization(clustering_result):
    return(max(clustering_result.groupby(['Cluster_ID']).count()['ESN'])/(clustering_result.groupby(['Cluster_ID']).count()['ESN']).sum())
    #return((clustering_result.groupby(['Cluster_ID']).count()['ESN'])/(clustering_result.groupby(['Cluster_ID']).count()['ESN']).sum())
 
def penalization2(clustering_result):
    return(max(clustering_result.groupby(['Cluster_ID']).count()['ESN'])/(clustering_result.groupby(['Cluster_ID']).count()['ESN']).sum())
    
    
def summarize(clustering_result, kpi2_k=5, kpi3_k=5, kpi4_k=5):
    print("KPI1 = " + str(get_kpi1(clustering_result)))
    print("KPI2 = " + str(get_kpi2(clustering_result, k=kpi2_k)))
    print("KPI3 = " + str(get_kpi3(clustering_result, k=kpi3_k)))
    print("KPI4 = " + str(get_kpi4(clustering_result, k=kpi4_k)))
    print("Penalization = " + str(penalization(clustering_result)))


def get_clustering_table(weights=[1,1,1,1,1,1,1,1], n_cluster=25, algorithm = 'hierarchical'):
    start = time.time()
    clustering_result = incident_clustering_df.loc[:, ['DSIS','ESN', 'Fault_Codes', 
                                                       'Fail_Code_Cleaned', 'Miles_Imputed', 'Issue_Number','Net_Amount_Imputed','OEM']]
    clustering_result['Cluster_ID'] = clustering_model(weights=weights,k=n_cluster, algorithm = algorithm).labels_
    end = time.time()
    print(end-start)
    return(clustering_result)
    
def get_clustering_table2(weights=[1,1,1,1,1,1,1,1], n_cluster=25, algorithm = 'hierarchical'):
    start = time.time()
    clustering_result = copy.copy(incident_trakcer_df)
    clustering_result['Cluster_ID'] = clustering_model(weights=weights,k=n_cluster, algorithm = algorithm).labels_
    end = time.time()
    print(end-start)
    return(clustering_result)
########################################################################################################









 
########################################################################################################
def update(initial=[1,1,1,1,1,1,1,1,1], index=0, n_cluster=25, kpi1=0,kpi2=-1,kpi3=0,kpi4=-1,pen=1, pen2=0,kpi2_k = 5, kpi3_k = 5,  kpi4_k=5,prin=True,plot=True
           , algorithm = 'hierarchical'):
    clustering_result = incident_clustering_df.loc[:, ['ESN', 'Fault_Codes', 
                                                       'Fail_Code_Cleaned', 'Miles_Imputed', 'Issue_Number']]
    if index == 8:
        x = []
        kpi1_list = []    
        kpi2_list = []
        kpi3_list = []
        kpi4_list = []
        p_list = []
        p2_list = []
        for s in range(10,100,5):
            x = x + [s]

            clustering_result['Cluster_ID'] = clustering_model(weights=initial,k=s, algorithm= algorithm).labels_
        
            kpi1_list = kpi1_list + [get_kpi1(clustering_result)]
            kpi2_list = kpi2_list + [get_kpi2(clustering_result, k= kpi2_k)]
            kpi3_list = kpi3_list + [get_kpi3(clustering_result, k= kpi3_k)]
            kpi4_list = kpi4_list + [get_kpi4(clustering_result, k= kpi4_k)]
            p_list = p_list + [penalization(clustering_result)]
            p2_list = p2_list + [penalization2(clustering_result)]
            if prin:
                print(s)

        y = list(kpi1*np.array(kpi1_list) + kpi2*np.array(kpi2_list) + kpi3*np.array(kpi3_list) + kpi4*np.array(kpi4_list) + pen*np.array(p_list) + pen2*np.array(p2_list))
        if plot:
            plot_kpis(x,kpi1_list,kpi2_list,kpi3_list,kpi4_list,y)

        new_n_cluster = x[y.index(min(y))]

        return((initial, new_n_cluster))
               
    else:
        x = []
        kpi1_list = []    
        kpi2_list = []
        kpi3_list = []
        kpi4_list = []
        p_list = []
        p2_list = []
        output = initial
        for s in [x * 0.25 for x in range(0, 21)]:
            x = x + [s]
            output[index] = s 
            clustering_result['Cluster_ID'] = clustering_model(weights=output,k=n_cluster, algorithm=algorithm).labels_
        
            kpi1_list = kpi1_list + [get_kpi1(clustering_result)]
            kpi2_list = kpi2_list + [get_kpi2(clustering_result, k= kpi2_k)]
            kpi3_list = kpi3_list + [get_kpi3(clustering_result, k= kpi3_k)]
            kpi4_list = kpi4_list + [get_kpi4(clustering_result, k= kpi4_k)]
            p_list = p_list + [penalization(clustering_result)]
            p2_list = p2_list + [penalization2(clustering_result)]
            if prin:
                print(s)

        y = list(kpi1*np.array(kpi1_list) + kpi2*np.array(kpi2_list) + kpi3*np.array(kpi3_list) + kpi4*np.array(kpi4_list) + pen*np.array(p_list) + pen2*np.array(p2_list))
        if plot:
            plot_kpis(x,kpi1_list,kpi2_list,kpi3_list,kpi4_list,y)

        output[index] = x[y.index(min(y))]
        
        return((output, n_cluster))
    
def total_update(initial=[1,1,1,1,1,1,1,1], sequence=[8,0,1,2,3,4,5,6,7,8], n_cluster=25, kpi1=0,kpi2=-1,kpi3=0,kpi4=-1,pen=1, pen2=0, kpi2_k=5, kpi3_k =5, kpi4_k=5, prin=True,plot=True, algorithm= 'hierarchical'):
    optimal_weights = initial
    optimal_n_clusters = n_cluster
    for i in sequence:
        if i != 8 or algorithm != 'dbscan':
            optimal_weights, optimal_n_clusters = update(initial=optimal_weights, index=i, n_cluster=optimal_n_clusters, kpi1=kpi1,kpi2=kpi2,kpi3=kpi3,kpi4=kpi4,pen=pen,pen2=0, kpi2_k=kpi2_k, kpi3_k=kpi3_k, kpi4_k= kpi4_k, prin=prin, plot=plot, algorithm=algorithm)
            print(i)
            print(optimal_weights)
    return((optimal_weights, optimal_n_clusters))
    

def random_update(initial=[1,1,1,1,1,1,1], n_cluster=25, kpi1=0,kpi2=-1,kpi3=0,kpi4=-1,pen=1, pen2=0, kpi2_k=5, kpi3_k =5, kpi4_k=5, prin=True,plot=True, n_iterations=30, algorithm = 'hierarchical'):
    optimal_weights = initial
    optimal_n_clusters = n_cluster
    iterations_done = 0
    current_iteration = -1
    while iterations_done < n_iterations:
        i = random.randint(0,8)
        if i != current_iteration:
            if i != 8 or algorithm != 'dbscan':
                optimal_weights, optimal_n_clusters = update(initial=optimal_weights, index=i, n_cluster=optimal_n_clusters, kpi1=kpi1,kpi2=kpi2,kpi3=kpi3,kpi4=kpi4,pen=pen, pen2=0, kpi2_k=kpi2_k, kpi3_k=kpi3_k, kpi4_k=kpi4_k, prin=prin, plot=plot)
                print(i)
                print(optimal_weights)
                iterations_done = iterations_done + 1
                current_iteration = i
    return((optimal_weights, optimal_n_clusters))  
########################################################################################################










########################################################################################################
def plot_kpis(x,kpi1,kpi2,kpi3,kpi4,y):
    ax1 = host_subplot(111, axes_class=AA.Axes)
    ax2 = ax1.twinx()
    ax3 = ax1.twinx()
    offset = 60
    new_fixed_axis = ax2.get_grid_helper().new_fixed_axis
    ax3.axis["right"] = new_fixed_axis(loc="right", axes=ax3,
                                        offset=(offset, 0))
    ax2.axis["right"] = new_fixed_axis(loc="right", axes=ax2)
    lns1=ax1.plot(x,kpi1, label= "KPI1", color='r')
    lns2=ax1.plot(x,kpi3, label= "KPI3", color='m')
    lns3=ax2.plot(x,kpi2, label= "KPI2", color='b')
    lns4=ax2.plot(x,kpi4, label= "KPI4", color='c')
    lns5=ax3.plot(x,y,label="Objective function", color='g')
    lns = lns1+lns2+lns3+lns4+lns5
    labs = [l.get_label() for l in lns]
    ax1.legend(lns, labs, loc=0)
    ax1.set_ylabel('Average (KPI1 and KPI3)', color='r')
    ax2.set_ylabel('Percentage (KPI2 and KPI4)', color='b')
    ax3.set_ylabel('Objective', color='g')
    plt.show()
########################################################################################################










########################################################################################################
#########Examples of functions use    
#total_update(initial=[1,2,2,0,4,1,1], sequence=[7,0,1,2,3,4,5,6,7], n_cluster=25, kpi1=0,kpi2=-1,kpi3=0,kpi4=-1,pen=1, pen2=0) 
#Sequential framework: Start with initial weights [1,2,2,0,4,1,1] and 25 clusters, kpi*k* is the weight of the corresponding kpi in the objective function, and "sequence" is the order of optimization (7 being the number of clusters)
#Returns a couple ([W], n) where W is a list of weights and n the number of clusters (Except for dbscan)

#random_update(algorithm = 'dbscan', n_iterations = 10)
#Random optimization: initial parameters set to default, 10 iterations, using the dbscan algorithm (Default is hierarchical)
#Returns a couple ([W], n) where W is a list of weights and n the number of clusters (Except for dbscan)

#get_clustering_table(weights=[1,1,1,1,1,1,1], n_cluster=40, algorithm='kmeans')
#Return the cleaned table with the extra "Cluster_ID" given the weights and the method given
########################################################################################################











########################################################################################################
########Example of T-SNE plot

#tsne = manifold.TSNE(n_components=2).fit_transform(X=X)
#clustering_result = get_clustering_table(weights=[3.25, 4.25, 4.25, 0.75, 5.0, 4.75, 0.75], n_cluster=40, algorithm='hierarchical') #replace with the right clustering (weights, n_cluster,algorithm)
#plt.scatter(tsne[:,0], tsne[:,1], c=clustering_result['Cluster_ID'])
#plt.show()
########################################################################################################










