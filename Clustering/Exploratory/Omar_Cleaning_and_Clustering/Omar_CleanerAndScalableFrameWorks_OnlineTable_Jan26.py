#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 16 17:12:20 2018

@author: pg875
"""
########################################################################################################
#Librairies
from collections import Counter
from tqdm import tqdm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pyarrow.parquet as pq
import datetime
import copy
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.ensemble import RandomForestClassifier
from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import Birch
import time
from sklearn.model_selection import train_test_split
import calendar
from scipy import spatial
########################################################################################################









########################################################################################################
#This code is not optimized and the functions still contain global variables
########################################################################################################










########################################################################################################
#Plot style
plt.style.use('bmh')
plt.rcParams['figure.figsize'] = (8.0, 6.0)
    
#Import data    
incident_tracker = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/')

file_1 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00000-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_2 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00001-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_3 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00002-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_4 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00003-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_5 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00004-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_6 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00005-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_7 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00006-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()
file_8 = pq.read_table('/Users/pg875/Downloads/QUALITY_FEATURES_PARQUET_NEW-3/part-00007-eaca2b13-51fa-4c04-a12d-9d8d808921c7.parquet').to_pandas()


online_tracker = pd.concat([file_1, file_2, file_3, file_4, file_5, file_6, file_7, file_8])
incident_tracker_df = online_tracker.loc[online_tracker['INCDT_ISSUE_LABEL'].apply(lambda x: x is not None),:]

del file_1
del file_2
del file_3
del file_4
del file_5
del file_6
del file_7
del file_8

#Fail Code
##Clean
def replace_null(string,df):
    df.loc[:, string] = [np.array([], dtype = 'object') if x else v \
                                            for x,v in zip(df[string].isnull(), df[string])]
    return(df)

string = 'CMP_FAIL_CODE_LIST'
incident_tracker_df = replace_null(string, incident_tracker_df)

##Build list
def concatenate(l):
    out = []
    for x in l:
        for y in x:
            out.append(y)
    return(out)
    

fail_code_count = Counter(concatenate(incident_tracker_df['CMP_FAIL_CODE_LIST'])).most_common() 

fail_code_count_df = pd.DataFrame(
    {'Fail_Code': [x[0] for x in fail_code_count],
     'Occurence': [x[1] for x in fail_code_count]
    })   
    
threshold = fail_code_count_df['Occurence'].quantile(.25)
fail_code_list = fail_code_count_df.query('Occurence > @threshold')['Fail_Code']    
fail_code_first_2_list = list(set(fail_code_list.apply(lambda x: x[:2])))
fail_code_last_2_list = list(set(fail_code_list.apply(lambda x: x[2:])))

##Build Columns
fail_code_col_names = []
###Build Fail Code

for fc in tqdm(fail_code_list):
    incident_tracker_df['Fail_Code_' + fc] = [list(x).count(fc)
                                                 for x in incident_tracker_df['CMP_FAIL_CODE_LIST']]
    fail_code_col_names.extend(['Fail_Code_' + fc])

ac = 'Unkwown'
incident_tracker_df['Fail_Code_' + ac] = [1 if len(x) == 0 else 0
                                           for x in incident_tracker_df['CMP_FAIL_CODE_LIST']]
fail_code_col_names.extend(['Fail_Code_' + ac])

###Build 2 first digits
for fc in tqdm(fail_code_first_2_list):
    incident_tracker_df['Fail_Code_Pre_' + fc] = [list(x).count(fc)
                                                 for x in incident_tracker_df['CMP_FAIL_CODE_LIST'].\
                                                     apply(lambda x: [y[:2] for y in x])]
    fail_code_col_names.extend(['Fail_Code_Pre_' + fc])
    
ac = 'Unkwown'
incident_tracker_df['Fail_Code_Pre_' + ac] = [1 if len(x) == 0 else 0
                                           for x in incident_tracker_df['CMP_FAIL_CODE_LIST']]
fail_code_col_names.extend(['Fail_Code_Pre_' + ac])

###Build 2 last digits    
for fc in tqdm(fail_code_last_2_list):
    incident_tracker_df['Fail_Code_App_' + fc] = [list(x).count(fc)
                                                 for x in incident_tracker_df['CMP_FAIL_CODE_LIST'].\
                                                     apply(lambda x: [y[2:] for y in x])]
    fail_code_col_names.extend(['Fail_Code_App_' + fc])
    
ac = 'Unkwown'
incident_tracker_df['Fail_Code_App_' + ac] = [1 if len(x) == 0 else 0
                                           for x in incident_tracker_df['CMP_FAIL_CODE_LIST']]
fail_code_col_names.extend(['Fail_Code_App_' + ac])

#Fault Code
##Clean

#string = 'NON_GREEN_WRENCH_FAULT_CODE_LIST'
#incident_tracker_df = replace_null(string, incident_tracker_df)
#incident_tracker_df['NON_GREEN_WRENCH_FAULT_CODE_LIST'] = incident_tracker_df['NON_GREEN_WRENCH_FAULT_CODE_LIST'].apply(lambda x: list(map(str,x)))

string = 'GREEN_WRENCH_FAULT_CODE_LIST'
incident_tracker_df = replace_null(string, incident_tracker_df)
incident_tracker_df['GREEN_WRENCH_FAULT_CODE_LIST'] = incident_tracker_df['GREEN_WRENCH_FAULT_CODE_LIST'].apply(lambda x: list(map(str,x)))

##Build list
    
#fault_code_count = Counter(concatenate(incident_tracker_df['NON_GREEN_WRENCH_FAULT_CODE_LIST'])).most_common() 

fault_code_count = Counter(concatenate(incident_tracker_df['GREEN_WRENCH_FAULT_CODE_LIST'])).most_common() 

fault_code_count_df = pd.DataFrame(
    {'Fault_Code': [x[0] for x in fault_code_count],
     'Occurence': [x[1] for x in fault_code_count]
    })   
    
threshold = fault_code_count_df['Occurence'].quantile(.25)
fault_code_list = fault_code_count_df.query('Occurence > @threshold')['Fault_Code']    

##Build Columns
fault_code_col_names = []
###Build Fault Code


#column = 'NON_GREEN_WRENCH_FAULT_CODE_LIST'
column = 'GREEN_WRENCH_FAULT_CODE_LIST'

for fc in tqdm(fault_code_list):
    incident_tracker_df['Fault_Code_' + fc] = [list(x).count(str(fc))
                                                 for x in incident_tracker_df[column]]
    fault_code_col_names.extend(['Fault_Code_' + fc])

ac = 'Unkwown'
incident_tracker_df['Fault_Code_' + ac] = [1 if len(x) == 0 else 0
                                           for x in incident_tracker_df[column]]
fail_code_col_names.extend(['Fault_Code_' + ac])


##Miles
incident_tracker_df = replace_null('CMP_ENGINE_MILES_LIST', incident_tracker_df)

incident_tracker_df['LAST_MILE_RECORDED'] = \
    incident_tracker_df.CMP_ENGINE_MILES_LIST.apply(lambda x:x[-1] if len(x) > 0 else None)

###impute missing by claims column

miles_mean = incident_tracker_df.LAST_MILE_RECORDED.mean()
incident_tracker_df['Miles_Imputed'] = incident_tracker_df.LAST_MILE_RECORDED.\
        apply(lambda x: float(x) if not(pd.isnull(x)) else miles_mean)

miles_treshold = incident_tracker_df.Miles_Imputed.quantile(.99)
incident_tracker_df.loc[incident_tracker_df.Miles_Imputed > miles_treshold, 'Miles_Imputed'] = np.nan

###Log Miles
incident_tracker_df["Miles_Log_Normalized"] = [np.nan if pd.isnull(m) else 0 if m == 0 else np.log(m) 
                                                  for m in incident_tracker_df["Miles_Imputed"]]

incident_tracker_df["Miles_Log_Normalized"].fillna(incident_tracker_df["Miles_Log_Normalized"].mean(), 
                                                      inplace = True)

incident_tracker_df["Miles_Log_Normalized"] = (incident_tracker_df["Miles_Log_Normalized"] - \
   incident_tracker_df["Miles_Log_Normalized"].mean()) / \
   incident_tracker_df["Miles_Log_Normalized"].var()**0.5
    

    
##Age
incident_tracker_df = replace_null('CMP_FAIL_DATE_LIST', incident_tracker_df)


incident_tracker_df['Last_Mile_Record_Time'] = \
    incident_tracker_df.CMP_FAIL_DATE_LIST.apply(lambda x:datetime.datetime.strptime(x[-1], '%Y-%m-%d') if len(x) > 0 else None)
     
incident_tracker_df['AGE'] = \
    (incident_tracker_df.Last_Mile_Record_Time - \
     incident_tracker_df.DSID_CREATE_DATE).apply(lambda x: x.days)

###Log Age  
incident_tracker_df["AGE_Log_Normalized"] = [np.nan if pd.isnull(m) else 0 if m <= 0 else np.log(m) 
                                                  for m in incident_tracker_df["AGE"]]

incident_tracker_df["AGE_Log_Normalized"].fillna(incident_tracker_df["AGE_Log_Normalized"].mean(), 
                                                      inplace = True)

incident_tracker_df["AGE_Log_Normalized"] = (incident_tracker_df["AGE_Log_Normalized"] - \
   incident_tracker_df["AGE_Log_Normalized"].mean()) / \
   incident_tracker_df["AGE_Log_Normalized"].var()**0.5
   
   
##Net Amount
mean_net_amount = incident_tracker_df.CMP_SUM_NET_AMOUNT.mean()
incident_tracker_df['Net_Amount_Imputed'] = incident_tracker_df.CMP_SUM_NET_AMOUNT.\
        apply(lambda x: float(x) if not(pd.isnull(x)) else mean_net_amount)
 
###Log Net Amount       
incident_tracker_df["Net_Amount_Log_Normalized"] = [np.nan if pd.isnull(m) else 0 if m == 0 else np.log(m) 
                                                  for m in incident_tracker_df["CMP_SUM_NET_AMOUNT"]]

incident_tracker_df["Net_Amount_Log_Normalized"].fillna(incident_tracker_df["Net_Amount_Log_Normalized"].mean(), 
                                                      inplace = True)

incident_tracker_df['Net_Amount_Imputed'] = (incident_tracker_df['Net_Amount_Imputed'] - \
   incident_tracker_df['Net_Amount_Imputed'].mean()) / \
   incident_tracker_df['Net_Amount_Imputed'].var()**0.5    
#########################################################################################









#########################################################################################
#EARLIEST DATE
#Missing count
incident_tracker_df['Missing'] = incident_tracker_df['DSID'].isnull()
incident_tracker_df.query('Missing == True')['CMP_FAIL_DATE_LIST'].apply(lambda x: len(set(x)) <= 1).mean()
#Clean
string = 'CMP_CLAIM_DATE_LIST'
incident_tracker_df = replace_null(string, incident_tracker_df)
incident_tracker_df['EARLIEST_DATE'] = incident_tracker_df['CMP_CLAIM_DATE_LIST'].apply(lambda x: min(x) if len(x) > 0 else None)
incident_tracker_df['EARLIEST_DATE'] = incident_tracker_df['EARLIEST_DATE'].apply(lambda x:datetime.datetime.strptime(str(x), '%Y-%m-%d') if x != None else None)
incident_tracker_df['EARLIEST_DATE'] = incident_tracker_df['EARLIEST_DATE'].where(incident_tracker_df['EARLIEST_DATE'].notnull(), incident_tracker_df['DSID_CREATE_DATE'])
#########################################################################################










##############################################################################################
#Clustering
clustering_features = []
clustering_features.extend(["Miles_Log_Normalized"])
clustering_features.extend(fault_code_col_names)
clustering_features.extend(fail_code_col_names)
clustering_features.extend(["AGE_Log_Normalized"])
clustering_features.extend(["Net_Amount_Imputed"])

X = incident_tracker_df.loc[:, clustering_features].values

#Model
def clustering_model(data, weights=[1,1,1,1,1,1,1], k = 10, algorithm = 'hierarchical', fixed_nclust = False, threshold = 2.5): #Weights = [Miles,Fault Code,Fail Code, First 2 digits, Last 2 Digits]
    #data should be = X or one its subsets
    Xcf = copy.copy(data)
    nfault = len(fault_code_list)
    nfail = len(fail_code_list)+1
    nfirst = len(fail_code_first_2_list)+1
    nlast = len(fail_code_last_2_list)+1
    Xcf[:,0] = weights[0]*Xcf[:,0]
    Xcf[:,1:(nfault+1)] = weights[1]*Xcf[:,1:(nfault+1)]
    Xcf[:,(nfault+1):(nfault+nfail+1)] = weights[2]*Xcf[:,(nfault+1):(nfault+nfail+1)]
    Xcf[:,(nfault+nfail+1):(nfault+nfail+nfirst+1)] = weights[3]*Xcf[:,(nfault+nfail+1):(nfault+nfail+nfirst+1)] 
    Xcf[:,(nfault+nfail+nfirst+1):(nfault+nfail+nfirst+nlast+1)] = weights[4]*Xcf[:,(nfault+nfail+nfirst+1):(nfault+nfail+nfirst+nlast+1)]
    Xcf[:,nfault+nfail+nfirst+nlast+1] = weights[5]*Xcf[:,nfault+nfail+nfirst+nlast+1]
    Xcf[:,-1] = weights[6]*Xcf[:,-1]
    if algorithm == 'hierarchical':
        model = AgglomerativeClustering(linkage='average', n_clusters = k)
    if algorithm == 'dbscan':
        model = DBSCAN()
    if algorithm == 'kmeans':
        model = KMeans(n_clusters=k, random_state=42)
    if algorithm == 'birch' and not(fixed_nclust):
        model = Birch(branching_factor=50, n_clusters=None, threshold=threshold,compute_labels=True)
    if algorithm == 'birch' and fixed_nclust:
        model = Birch(branching_factor=50, n_clusters=k, threshold=threshold,compute_labels=True)
    if algorithm == 'minibatch':
        model = MiniBatchKMeans(n_clusters=k)
    model.fit(Xcf)
    #cluster_test = cluster.k_means(Xcf, n_clusters=k)
    #return(cluster_test)
    return(model)

#Table    
def get_clustering_table(data, df, weights=[1,1,1,1,1,1,1], n_cluster=25, algorithm = 'hierarchical'):
    #df sould be = incident_tracker_df
    clustering_result = df.loc[:, ['DSIS','ESN', 'CMP_FAIL_DATE_LIST', 'CMP_FAIL_CODE_LIST'
                                                       'NON_GREEN_WRENCH_FAULT_CODE_LIST', 'Miles_Imputed','Net_Amount_Imputed','AGE']]
    clustering_result['Cluster_ID'] = clustering_model(data=data, weights=weights,k=n_cluster, algorithm = algorithm).labels_
    return(clustering_result)
######################################################################################### 















    
######################################################################################### 
##Sumsample and predict   
def subsample_predict(X, df, m=20000, k=80, weights=[1.5, 4.25, 4.25, 0.25, 2.75, 1.5, 0.5]):
    #X = X
    #df = incident_tracker_df
    #subsample
    X_train = X[0:m,:]
    #cluster
    clustering_results = clustering_model(data=X_train,k=k,weights=weights)
    clustering_table = df.loc[:, ['DSIS','ESN', 'CMP_FAIL_DATE_LIST', 'CMP_FAIL_CODE_LIST'
                                                   'GREEN_WRENCH_FAULT_CODE_LIST', 'Miles_Imputed','Net_Amount_Imputed','AGE']]
    clustering_table['Cluster_ID'] = 0
    clustering_table['Cluster_ID'][0:m] = clustering_results.labels_
    #predict
    predict_model = RandomForestClassifier()
    predict_model.fit(X_train, clustering_results.labels_)
    predicted_clusters= predict_model.predict(X)
    return(predicted_clusters)


#Accuracy Test
m = 20000
X_sub = X[0:m,:]
clustering_results = clustering_model(data=X_sub,k=80,weights=[1.5, 4.25, 4.25, 0.25, 2.75, 1.5, 0.5])
X_train_, X_test_, y_train_, y_test_ = train_test_split(X_sub, clustering_results.labels_, test_size=0.2)
predict_model_ = RandomForestClassifier()
predict_model_.fit(X_train_, y_train_)
#Out-of-sample R^2
predicted_clusters_= predict_model_.predict(X_test_)
print((predicted_clusters_ == y_test_).mean())
#########################################################################################   









##The next two section are not cleaned and are obselete - one can use the clustering_model() function directly
######################################################################################### 
##Birch
#Threshold tuning 
#
size = 20000
l = []
for i in range(1,20):
    brc = Birch(branching_factor=50, n_clusters=None, threshold=float(i/4),compute_labels=True)
    start = time.time()
    brc.partial_fit(X[0:size,:])
    s = (len(set(brc.predict(X[0:size,:]))))
    l.append(s)
    end = time.time()
    print(end-start)
print(l) #We choose threshold = 2.75

#Partial fit - speed & size
brc = Birch(branching_factor=50, n_clusters=None, threshold=2.75,compute_labels=True)
size = 20000
step = 10000
cursor = 0
for i in range(10):
    start = time.time()
    brc.partial_fit(X[cursor:(cursor+size),:])
    print(len(set(brc.predict(X[cursor:(cursor+size),:]))))
    end = time.time()
    cursor = cursor + step
    print(end-start)
######################################################################################### 










######################################################################################### 
##Mini batch
m_batch = MiniBatchKMeans(n_clusters=80)
m = 100000
start = time.time()
m_batch.fit(X[0:m,:])
end = time.time()
print(end-start)
######################################################################################### 










######################################################################################### 
#increase a datetime object by n months
def increment_months(sourcedate,months=1):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    hour = sourcedate.hour
    minute = sourcedate.minute
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return datetime.datetime(year,month,day,hour,minute)

#increase a datetime object by m days
def increment_days(sourcedate, days=1):
    date = sourcedate + datetime.timedelta(days=days)
    year = date.year
    month = date.month
    day = date.day
    hour = date.hour
    minute = date.minute
    return datetime.datetime(year,month,day,hour,minute)

#def compute_means(X, df, clustering_labels, start_window, end_window):
 #   means = {}
  #  for cluster in set(clustering_labels):
   #     means[cluster] = X[df['EARLIEST_DATE'].apply(lambda x: start_window <= x < end_window),:][clustering_labels == cluster].mean(axis=0)
    #return(means)
    
#threshold tuning for birch algorithm used to determine nclusters
def birch_treshold(X, df, weights=[2.0, 4.25, 4.25, 0.25, 2.75, 3.5, 0.5], monthly_size=True, monthly_step=True, size=2, step=1):
    initial = df['EARLIEST_DATE'].min()
    final = df['EARLIEST_DATE'].max()
    start_window = [increment_months(initial,0).replace(day=1)]
    if monthly_size:
        end_window = [increment_months(start_window[-1], size)]
    else:
        end_window = [increment_days(start_window[-1], size)]
    i = 0
    max_size = -1
    max_size_index = 0
    ploter = []
    while end_window[-1] < final:
        print(start_window[-1])
        start = time.time()
        sizeXcf = X[df['EARLIEST_DATE'].apply(lambda x: start_window[-1] <= x < end_window[-1]),:].shape[0]
        if sizeXcf > max_size:
            max_size_index = i
        if monthly_step:
            start_window.append(increment_months(start_window[-1], step))
            end_window.append(increment_months(end_window[-1], step))
        else:
            start_window.append(increment_days(start_window[-1], step))
            end_window.append(increment_days(end_window[-1], step)) 
        end = time.time()
        print(end-start)
        i = i+1
    Xcf = X[df['EARLIEST_DATE'].apply(lambda x: start_window[max_size_index] <= x < end_window[max_size_index]),:]
    for j in range(55):
        ploter.append(len(set(clustering_model(data=Xcf, weights=weights, algorithm='birch', fixed_nclust=False, threshold=0.25+(j/4)).labels_)))
    return(ploter)

#select the right threshold to get a given order of magnitude
def select_treshold(largest_nclust, ploter):
    values= list(abs(np.array(ploter) - largest_nclust))
    j_opt = values.index(min(values))
    return(0.25+(j_opt/4))
    
    
#Moving window framework   
def window_framework(X, df, weights=[2.0, 4.25, 4.25, 0.25, 2.75, 3.5, 0.5], algorithm = 'hierarchical', monthly_size=True, monthly_step=True,size=2, step=1, birch_treshold=10, bis=True, nclust_bis=80):
    clustering_results = {}
    initial = df['EARLIEST_DATE'].min()
    final = df['EARLIEST_DATE'].max()
    start_window = [increment_months(initial,0).replace(day=1)]
    if monthly_size:
        end_window = [increment_months(start_window[-1], size)]
    else:
        end_window = [increment_days(start_window[-1], size)]
    anchor = {}
    i = 0
    while end_window[-1] < final:
        print(start_window[-1])
        start = time.time()
        Xcf = X[df['EARLIEST_DATE'].apply(lambda x: start_window[-1] <= x < end_window[-1]),:]
        if bis:
            k = len(set(clustering_model(data=Xcf, weights=weights, algorithm='birch', fixed_nclust=False, threshold=birch_treshold).labels_))
        else:
            k = nclust_bis
        clustering_results[i] = clustering_model(data=Xcf, weights=weights, k=k, algorithm = algorithm)
        clustering_labels = clustering_results[i].labels_
        print(k)
        anchor[i] = {}
        for cluster in set(clustering_labels):
            anchor[i][cluster] = Xcf[clustering_labels == cluster].mean(axis=0)
        if monthly_step:
            start_window.append(increment_months(start_window[-1], step))
            end_window.append(increment_months(end_window[-1], step))
        else:
            start_window.append(increment_days(start_window[-1], step))
            end_window.append(increment_days(end_window[-1], step)) 
        end = time.time()
        print(end-start)
        i = i+1
    return((clustering_results, anchor, start_window, end_window))

#Tracking algorithm

#Track with centroid
def track_next(cluster_number, step, anchor, tnext = 1):
    target = anchor[step][cluster_number]
    possibilities = list(anchor[step+tnext].values())
    tree = spatial.KDTree(possibilities)
    output = tree.query(target)
    return(output[1]) #(output[1] -1) if DBSCAN

#Track witn count
def track_next_count(df, cluster_label, step_label, clustering_results, start_window, end_window, tnext=1, percentage_treshold=.5):
    total = df.loc[df['EARLIEST_DATE'].apply(lambda x: start_window[step_label] <= x < end_window[step_label+tnext]),:]
    total['New_Cluster'] = -1
    total.loc[total['EARLIEST_DATE'].apply(lambda x: start_window[step_label+tnext] <= x < end_window[step_label+tnext]),'New_Cluster'] = clustering_results[step_label+tnext].labels_
    filtered = total.loc[total['EARLIEST_DATE'].apply(lambda x: start_window[step_label] <= x < end_window[step_label]),:][clustering_results[step_label].labels_ == cluster_label]
    filtered = filtered.loc[filtered['EARLIEST_DATE'].apply(lambda x: start_window[step_label+tnext] <= x < end_window[step_label]),:]
    if filtered.shape[0] > 0:
        output = filtered.groupby(['New_Cluster'])['New_Cluster'].value_counts()
        if output.max()/output.sum() > percentage_treshold:
            return(output.idxmax()[1])
        else:
            return(-1)
    else:
        return(-1)

#Track with both centroids and counts using thresholds
def track_next_hybrid(df, cluster_label, step_label, clustering_results, anchor, start_window, end_window, centroid_treshold, percentage_treshold, tnext=1):
    target = anchor[step_label][cluster_label]
    possibilities = list(anchor[step_label+tnext].values())
    tree = spatial.KDTree(possibilities)
    output = tree.query(target)
    if output[0] < centroid_treshold:
        return(output[1])
    else:
        return(track_next_count(df=df, cluster_label=cluster_label, step_label=step_label, clustering_results=clustering_results, start_window=start_window, end_window=end_window, percentage_treshold=percentage_treshold))
        
#Aggregate function for centroids
def total_path(cluster_number,step,anchor):
    path = [cluster_number]
    current_step = step
    current_cluster = cluster_number
    for i in tqdm(range((len(anchor)-step-1))):
        current_cluster = track_next(cluster_number, current_step,anchor)
        path.append(current_cluster)
        current_step = current_step + 1
    return(path)

#Calculate path with centroids based on the first observation only    
def total_spread(cluster_number, step, anchor):
    path = [cluster_number]
    for i in tqdm(range((len(anchor)-step-1))):
        next_cluster = track_next(cluster_number, step,anchor, tnext=i+1)
        path.append(next_cluster)
    return(path)   

#Aggregate function for counts
def total_count(df, cluster_label,step_label,clustering_results, start_window, end_window):
    path = [cluster_label]
    current_step = step_label
    previous_cluster = cluster_label
    current_cluster = cluster_label
    for i in tqdm(range(len(start_window)-step_label-2)):
        if current_cluster != -1:
            previous_cluster = current_cluster
            current_cluster = track_next_count(df, current_cluster, current_step, clustering_results, start_window, end_window)
            path.append(current_cluster)
            current_step = current_step + 1
        else:
            path.append(current_cluster)
            current_cluster = previous_cluster
            current_step = current_step + 1
    return(path)    

#Aggregate function for hybrid    
def total_hybrid(df, cluster_label, step_label, clustering_results, anchor, start_window, end_window, centroid_treshold, percentage_treshold, tnext=1):
    path = [cluster_label]
    current_step = step_label
    previous_cluster = cluster_label
    current_cluster = cluster_label
    for i in tqdm(range(len(start_window)-step_label-2)):
        if current_cluster != -1:
            previous_cluster = current_cluster
            current_cluster = track_next_hybrid(df=df, current_cluster=current_cluster, current_step=current_step, clustering_results=clustering_results, anchor=anchor, start_window=start_window, end_window=end_window, centroid_treshold=centroid_treshold, percentage_treshold=percentage_treshold)
            path.append(current_cluster)
            current_step = current_step + 1
        else:
            path.append(current_cluster)
            current_cluster = previous_cluster
            current_step = current_step + 1
    return(path) 

#Visualtion and tabulation for performance estimate       
def view_clusters(df, clustering_results, start_window, end_window, step_label, cluster_label):
    return((df.loc[df['EARLIEST_DATE'].apply(lambda x: start_window[step_label] <= x < end_window[step_label]),:][clustering_results[step_label].labels_ == cluster_label])[['INCDT_ISSUE_LABEL','EARLIEST_DATE']])
    
def view_path(df, clustering_results, start_window, end_window, step_label, cluster_label, path_type = 'total', path_provided=False, path=[], centroid_treshold=1,percentage_treshold=0.5):
    output = view_clusters(df, clustering_results, start_window, end_window, step_label, cluster_label)
    output['Step_ID'] =  step_label
    output['Cluster_ID'] = cluster_label
    if path_provided:
        path = path
    else:
        if path_type == 'total':
            path = total_path(cluster_number=cluster_label, step=step_label, anchor=anchor)
        if path_type == 'spread':
            path = total_spread(cluster_number=cluster_label, step=step_label, anchor=anchor)
        if path_type == 'count':
            path = total_count(df=df, cluster_label=cluster_label,step_label=step_label,clustering_results=clustering_results, start_window=start_window, end_window=end_window)
        if path_type == 'hybrid':
            path = total_hybrid(df=df, cluster_label=cluster_label, step_label=step_label, clustering_results=clustering_results, anchor=anchor, start_window=start_window, end_window=end_window, centroid_treshold=centroid_treshold, percentage_treshold=percentage_treshold)
    for i in tqdm(range(1,len(path))):
        update = view_clusters(df, clustering_results, start_window, end_window, step_label+i, path[i])
        update['Step_ID'] =  step_label+i
        update['Cluster_ID'] = path[i]
        output = output.append(update, ignore_index=True)
    return(output)
######################################################################################### 






    



#########################################################################################
#Example use
ploter = birch_treshold(X, incident_tracker_df)
ths = select_treshold(100, ploter)
clustering_results, anchor, start_window, end_window = window_framework(X, incident_tracker_df, birch_treshold=ths)  

cluster_label, step_label = 1,0

#Even day-by-day on a 2-month window size, centroids are bad
ploter = birch_treshold(X, incident_tracker_df, monthly_size=True,monthly_step=False, size=2, step=1)
ths = select_treshold(100, ploter)
clustering_results, anchor, start_window, end_window = window_framework(X, incident_tracker_df, birch_treshold=ths, monthly_size=True, monthly_step=False, size=2, step=1)

##total centroid path
path = total_path(cluster_number=cluster_label,step=step_label,anchor=anchor)
path_table = view_path(incident_tracker_df, clustering_results, start_window, end_window, cluster_label=1,step_label=0)

p = list(path_table.groupby('Step_ID')['Cluster_ID'].count())
t  = [i for i in range(len(p))]
plt.savefig('centroidsarebad.png')
plt.plot(t,p)

##spread centroid path
path = total_spread(cluster_number=cluster_label,step=step_label,anchor=anchor)
path_table = view_path(incident_tracker_df, clustering_results, start_window, end_window, cluster_label=1,step_label=0,  path_type= 'spread')

p = list(path_table.groupby('Step_ID')['Cluster_ID'].count())
t  = [i for i in range(len(p))]
plt.plot(t,p)

##total count path
path = total_count(df=incident_tracker_df, cluster_label=cluster_label,step_label=step_label,clustering_results=clustering_results, start_window=start_window, end_window=end_window)
path_table = view_path(incident_tracker_df, clustering_results, start_window, end_window, cluster_label=cluster_label,step_label=step_label,  path_type= 'count', path_provided=True, path=path)

##total hybrid path
path = total_hybrid(df=incident_tracker_df, cluster_label=cluster_label, step_label=step_label, clustering_results=clustering_results, anchor=anchor, start_window=start_window, end_window=end_window, centroid_treshold=0.5, percentage_treshold=.5)
path_table = view_path(incident_tracker_df, clustering_results, start_window, end_window, cluster_label=cluster_label,step_label=step_label,  path_type= 'hybrid', path_provided=True, path=path)


###Plot
p = [path_table.groupby('Step_ID')['Cluster_ID'].count()[i+step_label] if path[i] != -1 else -10 for i in range(len(path))]
t  = [i for i in range(len(p))]
plt.title('Tracking of cluster '+str(cluster_label)+' in step '+str(step_label))
plt.xlabel('Step (window)')
plt.ylabel('Number of incidents')
plt.savefig('Tracking of cluster '+str(cluster_label)+' in step '+str(step_label)+'.png')
plt.plot(t,p)

#########################################################################################









#########################################################################################
#Visualize size and nclusters per step
nobs = [len(clustering_results[i].labels_) for i in range(len(clustering_results))]
t = [i for i in range(len(clustering_results))]
plt.title('Total size of the clusters per Step')
plt.xlabel('Step (window)')
plt.ylabel('Number of incidents')
plt.plot(t,nobs, color='red')

nclust = [len(set(clustering_results[i].labels_)) for i in range(len(clustering_results))]
t = [i for i in range(len(clustering_results))]
plt.title('Number of clusters per step')
plt.xlabel('Step (window)')
plt.ylabel('Number of clusters')
plt.plot(t,nclust)
#########################################################################################









#########################################################################################
#how well does the clustering do to capture issues?
total = []
for j in range(3):
    cluster_label, step_label = j,200
    path = total_hybrid(df=incident_tracker_df, cluster_label=cluster_label, step_label=step_label, clustering_results=clustering_results, anchor=anchor, start_window=start_window, end_window=end_window, centroid_treshold=1, percentage_treshold=.5)
    path_table = view_path(incident_tracker_df, clustering_results, start_window, end_window, cluster_label=cluster_label,step_label=step_label,  path_type= 'count', path_provided=True, path=path)

###Plot
    p = [path_table.groupby('Step_ID')['Cluster_ID'].count()[i+step_label] if path[i] != -1 else -10 for i in range(len(path))]
    total.append(p)

t  = [i for i in range(len(p))]
plt.title('Tracking of cluster 0 to 5 in step '+str(step_label))
plt.xlabel('Step (window)')
plt.ylabel('Number of incidents')
for j in range(6):
    plt.plot(t,total[j])
    
#
def plot_cluster_graph(df,
                       clustering_results,
                       selected_issue, 
                       start_window,
                       end_window,
                       ):    
    plot_issue_cluster_size = []
    plot_issue_size = []
    for step_label in tqdm(range(len(clustering_results))):
        total = df.loc[df['EARLIEST_DATE'].apply(lambda x: start_window[step_label] <= x < end_window[step_label]),:]
        total['New_Cluster'] = -1
        total.loc[total['EARLIEST_DATE'].apply(lambda x: start_window[step_label] <= x < end_window[step_label]),'New_Cluster'] = clustering_results[step_label].labels_
        filtered = total.query('INCDT_ISSUE_NUMBER == @selected_issue')
        ninc = filtered.shape[0]
        if ninc > 0:
            issue_cluster = filtered.groupby(['New_Cluster'])['New_Cluster'].value_counts().idxmax()
            nobs = np.array(clustering_results[step_label].labels_ == issue_cluster[1]).sum()
        else:
            nobs = 0
        plot_issue_cluster_size.append(nobs)
        plot_issue_size.append(ninc)
    return((plot_issue_cluster_size, plot_issue_size))


top_issues = incident_tracker_df.groupby(['INCDT_ISSUE_NUMBER'])['ESN'].count().sort_values(ascending=False)[1:10].reset_index()['INCDT_ISSUE_NUMBER']
plot_issue_cluster_size, plot_issue_size = plot_cluster_graph(df=incident_tracker_df, clustering_results=clustering_results, selected_issue=top_issues[1], start_window=start_window, end_window=end_window)

t = [i for i in range(len(plot_issue_cluster_size))]
plt.plot(t,plot_issue_cluster_size)
plt.plot(t,plot_issue_size)
#########################################################################################        
    









######################################################################################### 
#Results for DBSCAN optimal
    ##Number of clusters per period
a = [1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,2,4,3,4,11,18,22,25,20,34,43,46,60,64,77,92,102,100,108,109,112,137,136,132,172,156,181,210,227,228,238,217,241,285,277,273,290,285,294,304,332,347,366,343,332,336,343,352,352,334,335,354,336,365,336,326,328, 277,320,311,292,271,148,4]
t = [x for x in range(0,len(a))]

plt.plot(t,a)
plt.xlabel('Step (2-month window)')
plt.ylabel('Number of DBSCAN clusters')
plt.title('Number of clusters per window between 2011 and 2017')
plt.savefig('clustersvstime')

    ##Number of incidents per period
a_2 = []
start_window = datetime.datetime(2011,6,1,0,0) 
end_window = increment_months(start_window, 2)
for i in range(79):
    print(start_window)
    start = time.time()
    Xcf = X[incident_tracker_df['EARLIEST_DATE'].apply(lambda x: start_window <= x < end_window),:]
    a_2.append(Xcf.shape[0])
    start_window = increment_months(start_window, 1)
    end_window = increment_months(end_window, 1)
    end = time.time()
    print(end-start)

plt.plot(t,a_2, color = 'red')
plt.xlabel('Step (2-month window)')
plt.ylabel('Number of incidents')
plt.title('Number of incidents per window between 2011 and 2017')
plt.savefig('incidentsvstime')

    ##Number of incident for one cluster tracked through time
dic, anchor = window_framework(X=X, df=incident_tracker_df,algorithm='dbscan', weights=[4.5, 0.5, 0.5, 0.75, 0.0, 0.75, 4.25],n_clustering=24)
path = total_path(-1,0,anchor,n_clustering=22)
s = [(dic[i].labels_ == path[i]).sum() for i in range(len(path))]
t = [i for i in range(len(path))]

plt.plot(t,s)
plt.xlabel('Step (2-month window)')
plt.ylabel('Number of incident in the tracked cluster')
plt.title('Evolution of cluster 0 through the first 2 years')
plt.savefig('cluster1evolution')
######################################################################################### 