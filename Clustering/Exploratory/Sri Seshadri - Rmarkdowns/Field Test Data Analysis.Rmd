---
title: "Field test leading indicator of issues"
author: "Sri Seshadri"
date: "January 9, 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = F, message = F, warning = F,tidy = T, tidy.opts = list(width.cutoff = 60))
library(ggplot2)
library(forcats)
library(dplyr)
source('cleanFaults.R')
source('readsymptom.R')
source('parseGreenWrench.R')
source('retreiveMiles.R')
library(grid)
library(gridExtra)
library(kableExtra)
library(cdparcoord)
```

# Objective:

Test the hypothesis that field test data is a leading indicator for field issues.

# Data

The data was obtained from FPA database in "CIAZSWPBISQL02.ced.corp.cummins.com\RDBS" for those software builds that range from limited production to post production revisons.The earliest data for the Limited production calibration was August 2016. It is a concern that this sample does not include sufficient samples of winter duty cycles. Assuming that that most of the trucks are run in the US. We might have to include field test data dating late 2015. 

Following is the breakdown of the trucks that are in field test for ISX 15 2017. 

```{r}
# The copy of the Field test data which is the result of the query below is stored in ->

"https://cummins365-my.sharepoint.com/personal/pp630_cummins_com/_layouts/15/guestaccess.aspx?guestaccesstoken=xnI8cHXfW0vbLtdOEVVJEaOk5EBnBVlbWxWUVVGOsMQ%3D&docid=2_1bc0f816256294874964ac8bab5143d3c&rev=1&e=Emc61R"

SQL_Script <-  paste(
                     "SELECT ROW_Number() OVER (PARTITION BY [Fault].[TruckID], Faultcode ORDER BY [Fault].[TruckID], Faultcode,FaultDate) as  ROWID",
	  ", [Fault].[TruckID]",
	  ", [TblTruck].[TruckName]",
	  ", [ESN]",
	  ", [Customer]",
	  ", [OEM]",
	  ", [Application]",
	  ", [Model_Year]",
      ",[Faultcode]",
      ",[FaultDate]",
	  ", datename(dw,[FaultDate]) as WeekDay",
	  ",TrendData.[OdometerStart]",
	  ",[OdometerStartAdj]",
      ",Fault.[Calibration]",
      ",[ECM_Code]",
      ",[Status]",
  "FROM [FPA].[dbo].[Fault]",
  "LEFT JOIN TblTruck on Fault.TruckID = TblTruck.TruckID",
  "JOIN TrendData on Fault.TruckID = TrendData.TruckID and Fault.FaultDate = TrendData.MatfileDate",
  "JOIN extra_trend_OdometerStartAdj",
	"ON Fault.TruckID = extra_trend_OdometerStartAdj.TruckID",
	"AND Fault.FaultDate = extra_trend_OdometerStartAdj.MatfileDate",
	"AND TrendData.OdometerStart = extra_trend_OdometerStartAdj.OdometerStart", 
  "where Fault.Calibration in ('60.60.70.1','60.70.70.1', '60.70.70.2', '60.70.17.2','60.70.17.15','60.70.17.15')",
  "order by [Fault].[TruckID], Faultcode, FaultDate")

ch <- RODBC::odbcConnect("FPA")
FPA.data <- RODBC::sqlQuery(ch,SQL_Script)
write.csv(FPA.data, file = "FPA_Data.csv")

knitr::kable(FPA.data %>% dplyr::summarise(Trucks = dplyr::n_distinct(TruckID),Customers = n_distinct(Customer), SoftwareBuilds = n_distinct(Calibration), Application = n_distinct(Application)))

```
The ESNs of engineering trucks seem to be weird. They have single and double digit numbers... why? 

# Exploratory data analysis

The plot below shows the break down of trucks by application. There are 11 trucks whose applications are unknown. The unknown applications are attributed to Engineering, Cycle efficiency management and PPS(?) trucks. However there is one PeterBuilt truck that has an unknown duty cycle



```{r}
FPA.data %>% dplyr::group_by(Application) %>% dplyr::summarise(TruckCount = n_distinct(TruckID)) %>%
            ggplot(.,mapping = aes(x = Application, y = TruckCount)) + geom_col() + coord_flip()

knitr::kable(FPA.data %>% dplyr::filter(Application %in% c("NA", "No Info")) %>% dplyr::group_by(Application,TruckName) %>% dplyr::summarise(rows = n()))


# Extract MDL
# Data stored in "https://cummins365-my.sharepoint.com/personal/pp630_cummins_com/_layouts/15/guestaccess.aspx?guestaccesstoken=ZonXoydRUZckWt34u1ZO2ZnhQ3MufYTYOEsCTZtM3gg%3D&docid=2_1dae5af94af26493c9293f7f9c46606d2&rev=1&e=fHh3K8"
MDL_Extract <- readxl::read_xlsx("MDL_Extract.xlsx")

```


```{r}
FaultCounts <- FPA.data %>% 
                  dplyr::group_by(Faultcode) %>% 
                  dplyr::summarise(Counts = n())
Top50Faults <- FaultCounts %>% dplyr::arrange(desc(Counts))
Top50Faults <- Top50Faults[1:50,]
Top50Faults$Faultcode <- as.factor(Top50Faults$Faultcode)
ggplot(data = Top50Faults,mapping = aes(x = fct_reorder(Faultcode,Counts), y = Counts)) + geom_col() + coord_flip() + xlab("Fault Codes")

```

# Generate incidents from filed test data

The rows of data with fault code active may not be unique triggers of the fault. The fault may active for some time. The challenge is to extract the unique instances. The following assumptions are made in order to generate unique "incidents" out of the data:

1. Consecutive days with a particular fault code active, is a result of an incident that triggered the fault.
2. Odometer reading, weekdays would be used to check if vehicle operated between days. May have to use a rule...say fault active over span of multiple days with additonal miles < 10 is still a result of the original incident that triggered the fault.

## Rule for unique incident

When difference in time between logs (rows) for a Fault and truck grouping is greater than 2 days AND when the difference in odometer is greater than 10 miles, the log is deemed to be a new "incident"

```{r,fig.height=6}
library(lubridate)
FPA.data <- FPA.data %>% dplyr::group_by(TruckID, Faultcode) %>% dplyr::mutate(Milesdiff = OdometerStartAdj - lag(OdometerStartAdj)) %>% 
                     dplyr::mutate(Eng.Truck = ifelse(substr(TruckName,1,3) %in% c("ENG","CEM"),1,0))

FPA.data$FaultDate <- lubridate::as_date(FPA.data$FaultDate)
FPA.data <- FPA.data %>% dplyr::mutate(timediff = as.difftime(FaultDate - lag(FaultDate,1))) %>% 
        dplyr::mutate(incident = ifelse(is.na(timediff) & is.na(Milesdiff),1,ifelse(timediff > 2 & Milesdiff > 10, 1,0)))

FPA.incidents <- FPA.data %>% dplyr::filter(incident > 0 ) 
FPA.incidents$ID <- seq(1,nrow(FPA.incidents))

# Data stored in "https://cummins365-my.sharepoint.com/personal/pp630_cummins_com/_layouts/15/guestaccess.aspx?guestaccesstoken=GFH5Iup8ZkNmh5CfPjFlAPCWA5SKKzxgmjs9n9CdByM%3D&docid=2_110475986fea2489fa8531a7f17cb8391&rev=1&e=NeHHOv"
write.csv(FPA.incidents, "FPA_incidents.csv")

FaultCts <- FPA.incidents %>% dplyr::group_by(Faultcode) %>%
                  dplyr::summarise(Counts = n())

Top50Faults <- FaultCts %>% dplyr::arrange(desc(Counts))
Top50Faults <- Top50Faults[1:50,]
Top50Faults$Faultcode <- as.factor(Top50Faults$Faultcode)
ggplot(data = Top50Faults,mapping = aes(x = fct_reorder(Faultcode,Counts), y = Counts)) + geom_col() + coord_flip() + xlab("Fault Codes") + theme_bw() + theme(text = element_text(size=10))
```


## How does these Faults compare the ones seen in the Infant care incident tracker?

```{r}
# cleaned incident tracker in https://cummins365-my.sharepoint.com/personal/pp630_cummins_com/_layouts/15/guestaccess.aspx?guestaccesstoken=PNIRpeex8FnN5TqhR18nP2nFsP6NswxK4uQtjUWCIeg%3D&docid=2_1bb3afe7e71fa4100885fa6e9d76bdb37&rev=1&e=UyFt0c

Incidents.Jiaju <- readxl::read_xlsx("incidents_tracker_cleaned_Dec14th.xlsx",sheet = 1) 
Incidents.Jiaju <- Incidents.Jiaju %>%  dplyr::mutate(rowid = 1:nrow(Incidents.Jiaju))

Faultsdf.j <- purrr::map2_df(.x = Incidents.Jiaju$`FAULT COMBINATION`,.y = Incidents.Jiaju$rowid,.f = cleanFaults)    

area1 <- length(unique(Faultsdf.j$Faults))
area2 <- length(unique(FaultCts$Faultcode))
cross.area <- length(dplyr::intersect(unique(Faultsdf.j$Faults),unique(FaultCts$Faultcode)))
Venn <- VennDiagram::draw.pairwise.venn(area1,area2,cross.area, c("Incident Tracker", "Data logger"),fill = c("blue", "red"))

grid.arrange(gTree(Venn), top="Fault code incidents between Incident tracker and data logger data", bottom="# ESNs incident tracker = 3609; # ESNs Data logger  = 50")

# grid.arrange(gTree(children=Venn), top="Fault code incidents between Incident tracker and data logger data", bottom="# ESNs incident tracker = 3609; # ESNs Data logger  = 50")
#title(main = "Fault code incidents between Incident tracker and data logger data", sub = "# ESNs incident tracker = 3609; # ESNs Data logger  = 50")

```

The field test data has experienced 50% of the field issues with just 50 trucks. It would be interesting to see how much of the overlap in the sets are incidents which has been deemed "Issues"?

The parallel coordinate plot shown below is identical to that of clustering by system and components.

```{r}
MDL_Extract <- readxl::read_xlsx("MDL_Extract.xlsx")
con <- RODBC::odbcConnect("Capability")
Fault_SE <- RODBC::sqlQuery(con,"SELECT FaultCode,SystemErrorName, Component from tblErrorTable where SoftwareVersion = 60701710")

MDL_Extract <- dplyr::left_join(MDL_Extract,Fault_SE,by = c("SE Name"= 'SystemErrorName'))
test <- dplyr::left_join(FPA.incidents,MDL_Extract[MDL_Extract$Program == "Acadia X3 2017",], by = c("Faultcode" = "FaultCode"))

requiredcols <- c("Faultcode", "ID", "IM Purpose", "IM System", "IM SubSystem", "IM Components", "IM Component Type", "IM Dianostic Type", "IM Failure Mode","Application")

test <- test %>% dplyr::select(requiredcols) %>% dplyr::distinct()

cluster.df <-  makeFactor(test, c("IM SubSystem", "IM Components","IM Component Type", "IM Failure Mode", "IM Dianostic Type"))
discparcoord(cluster.df[,c(-1:-5)], k = 100)
```

# How do the rate of failures compare between field test and off line incident tracker?

To answer the question above, the data can be set up for recurrent analysis modeling - MCF. The key variable(s) required for setting up the analysis are:

1. Start date of field test actual; since when did the truck begin to accumulate miles? -> https://cummins365-my.sharepoint.com/personal/pp630_cummins_com/_layouts/15/guestaccess.aspx?guestaccesstoken=E5jtYuaxYzFufKeW2%2BKmC%2BU8NFkZ6PFdBvBQndZIy6U%3D&docid=2_129bf4b81f4774e76b2c44c9b468d18fd&rev=1&e=YnJ0vu
2. When did diagnostics' tuning mature? in other words when did diagnostics become production ready? - From what date can the data be considered as representative sample for recurrent analysis.  -> https://cummins365-my.sharepoint.com/personal/pp630_cummins_com/_layouts/15/guestaccess.aspx?guestaccesstoken=grubR50KatOAvnx8MQNgJFSpx3R1e%2Fjdmh3wLDUfjmc%3D&docid=2_1d155d9600bfe42c59fee4e078a5102fb&rev=1&e=MGHxdf
  

The calibration maturity date of diagnostics  would be used as the constraint for sample selection. Below for each fault code the MSF (Milestone Final review) date is matched. The assumption is most of the field test trucks would have the calibration used for the review prior to the review date. Choosing data after the review date would be a conservative approach for analysis.

## While on that point, does calibration maturity have an effect of incidents

It is seen below t

```{r}
MDL_with_Date <- readxl::read_xlsx(path = "MDL_with_Dates.xlsx",sheet = "MSF_Fault")
MDL_with_Date <- MDL_with_Date %>% dplyr::mutate(DaysMature = as.Date("2017-01-01") - as.Date(as.character(Overall)) )
UniqueFaultsinTracker <- as.numeric(unique(Faultsdf.j$Faults))
IncidentFaults <- data.frame(Faultcode = UniqueFaultsinTracker)
FaultDate.incidents <-  dplyr::left_join(x = IncidentFaults,y = MDL_with_Date, by = c("Faultcode" = "FaultCode"))
FaultDate.Not.incidents <- MDL_with_Date[!MDL_with_Date$FaultCode %in% UniqueFaultsinTracker,]
par(mfrow = c(1,2))
hist(as.numeric(FaultDate.incidents$DaysMature[FaultDate.incidents$DaysMature < 40000]),xlab = "Days Mature", main = "faults in incident tracker")
hist(as.numeric(FaultDate.Not.incidents$DaysMature[FaultDate.Not.incidents$DaysMature < 40000]),xlab = "Days Mature", main = "faults NOT in incident tracker")

MDL_with_Date$incident <- as.factor(ifelse(MDL_with_Date$FaultCode %in% UniqueFaultsinTracker,1,0))

p1 <- ggplot(MDL_with_Date[MDL_with_Date$DaysMature < 40000,], mapping = aes(x=DaysMature,fill = incident, color = incident)) + geom_density(alpha = 0.5)

p2 <- ggplot(MDL_with_Date[MDL_with_Date$DaysMature < 40000 & !is.na(MDL_with_Date$DaysMature),], mapping = aes(y = DaysMature, x = incident)) + geom_boxplot()

grid.arrange(p1,p2,ncol = 2)

kruskal.test(formula = DaysMature ~ incident, data = MDL_with_Date[MDL_with_Date$DaysMature < 40000 & !is.na(MDL_with_Date$DaysMature),] )

knitr::kable(MDL_with_Date[MDL_with_Date$DaysMature < 40000 & !is.na(MDL_with_Date$DaysMature),] %>% dplyr::group_by(incident) %>% dplyr::summarise(medianMature = median(DaysMature)))
```
# Recurrence analysis.

## Data set up

Fault logs is retrieved for those trucks that had production cal since their commencement of field tests. In the previous section we retrieved data since the time the trucks had production calibrations uploaded on to them.

The fault logs for data is queried using the SQL code in -> https://cummins365-my.sharepoint.com/:u:/g/personal/pp630_cummins_com/EYL-5rR1Q_NEuUuybEUD2DEBDT4T5sPTfxxyaqBqziTprA?e=zIlOi6

The maximum files accumulated for these trucks as of 1/25/18 10:05 EST is retrieved using the query "Select TruckID, truckname, max(OdometerStartAdj) as Miles from extra_trend_OdometerStartAdj group by TruckID, truckname"

The data is stored in this location fron the above are stored in separate worksheets of a workbook located here -> https://cummins365-my.sharepoint.com/:x:/g/personal/pp630_cummins_com/ES2iCF-cqP1Jkn_k5eWbOlUBl-onCrK9s3tta4SP3mUFpg?e=1ygYie

As mentioned above, the active fault codes are logged every day, irrespective of whether the fault was triggered the previous day(s) and remained active at the time of logging. Hence the data needs to be filtered for unique triggers or occurence of a fault. We'll use the rule mentioned above to tag unique instances of faults.


```{r}
Faultlogs <- readxl::read_xlsx(path = "MCF_Setup.xlsx", sheet = "FaultLogs")
Censor <- readxl::read_xlsx(path = "MCF_Setup.xlsx", sheet = "MaxMiles")

# Get unique incidents from Faultlogs 
Faultlogs.data <- Faultlogs%>% dplyr::group_by(TruckID, Faultcode) %>% dplyr::mutate(Milesdiff = OdometerStartAdj - lag(OdometerStartAdj)) %>% 
                     dplyr::mutate(Eng.Truck = ifelse(substr(truckname,1,3) %in% c("ENG","CEM"),1,0))

Faultlogs.data$FaultDate <- lubridate::as_date(Faultlogs.data$FaultDate)
Faultlogs.data <- Faultlogs.data %>% dplyr::mutate(timediff = as.difftime(FaultDate - lag(FaultDate,1))) %>% 
        dplyr::mutate(incident = ifelse(is.na(timediff) & is.na(Milesdiff),1,ifelse(timediff > 2 & Milesdiff > 10, 1,0)))

Faultlogs.incidents <- Faultlogs.data %>% dplyr::filter(incident > 0 ) 
Faultlogs.incidents$ID <- seq(1,nrow(Faultlogs.incidents))

Faultlogs.incidents <- merge(Faultlogs.incidents,Censor[,c(1,3)],by.x = "TruckID", by.y = "TruckID")
Faultlogs.incidents$censor <- 1
# Faultlogs.incidents$censor <- 0

F5867 <- Faultlogs.incidents %>% dplyr::filter(Faultcode == 5867)
df <- F5867 %>% dplyr::select(TruckID, OdometerStartAdj,censor,Application)
censor.rows <- F5867 %>% dplyr::group_by(TruckID,Application) %>% dplyr::summarise(OdometerStartAdj = max(Miles))
#censor.rows <- censor.rows %>% dplyr::mutate(censor = rep(1,nrow(censor.rows)))
censor.rows$censor <- rep(1,nrow(censor.rows))
df <- rbind.data.frame(df,censor.rows) %>% dplyr::arrange(TruckID)
df <- df %>% mutate(censor.rev = ifelse(censor == 1,0,1))
#reda::Survr(df$TruckID,df$OdometerStartAdj,df$censor.rev)
library(reda)
F5867.intercept <- mcf(Survr(TruckID,OdometerStartAdj,censor.rev) ~ 1, data = df)
 plot(F5867.intercept, conf.int = TRUE, mark.time = TRUE, addOrigin = TRUE, col = 2) + ggplot2::xlab("Miles") + ggplot2::theme_bw() + ggtitle("Fault 5867")
 
 F3488 <-  Faultlogs.incidents %>% dplyr::filter(Faultcode == 3488)
df.3488 <- F3488 %>% dplyr::select(TruckID, OdometerStartAdj,censor,Application)
censor.rows.3488 <- F3488 %>% dplyr::group_by(TruckID,Application) %>% dplyr::summarise(OdometerStartAdj = max(Miles))
#censor.rows.3488 <- censor.rows.3488 %>% dplyr::mutate(censor = rep(1,nrow(censor.rows.3488)))
censor.rows.3488$censor <- rep(1,nrow(censor.rows.3488))
df.3488 <- rbind.data.frame(df.3488,censor.rows.3488) %>% dplyr::arrange(TruckID)
df.3488 <- df.3488 %>% mutate(censor.rev = ifelse(censor == 1,0,1))

F3488.intercept <- mcf(Survr(TruckID,OdometerStartAdj,censor.rev) ~ 1, data = df.3488)
 plot(F3488.intercept, conf.int = TRUE, mark.time = TRUE, addOrigin = TRUE, col = 2) + ggplot2::xlab("Miles") + ggplot2::theme_bw() + ggtitle("Fault 3488") + ylim(0,16)

```


