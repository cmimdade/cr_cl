# Import required modules
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from nltk.stem import PorterStemmer
from sklearn.metrics import confusion_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import GridSearchCV
from sklearn import preprocessing
from sklearn.model_selection import train_test_split

# Read the filtered and cleaned data
master_file = pd.read_csv("../Data/new eds data.csv")

# Create dummy of issue (as 1) and not_issue (as 0) from column 'Issue Number'
master_file['issue_dummy'] = np.where(master_file['Issue Number']=='Not_Issue', 0, 1)

# Filtering EDS data only
master_file = master_file.loc[master_file['Incident Source']=="EDS"]
master_file.reset_index(inplace = True, drop = True)

# Get Mileage column and convert all values to numeric
master_file['Mileage'] = master_file['Mileage/ Engine Hours'].copy()
master_file['Mileage'] = master_file['Mileage'].str.replace(', ', '')
master_file['Mileage'] =  master_file['Mileage'].apply(pd.to_numeric, errors = 'coerce')

# Join 'Green Wrench' and 'Fault Combination' text columns for final text
# Clean all text - rid the special characters, convert to lowercase, fill missing values, etc.
col = ['issue_dummy', 'FAULT COMBINATION', 'Green Wrench']
data = master_file[col].copy()
data['FAULT COMBINATION'] = data['FAULT COMBINATION'].str.lower()
data['Green Wrench'] = data['Green Wrench'].str.lower()
data['FAULT COMBINATION'] = data['FAULT COMBINATION'].str.replace(r'fault codes ([0-9]+)', 'fc\\1')
data['FAULT COMBINATION'] = data['FAULT COMBINATION'].str.replace(r', ', ' ')
data['FAULT COMBINATION'] = data['FAULT COMBINATION'].str.replace(r'\s+', ' ')
data['Green Wrench'] = data['Green Wrench'].str.replace(r'[a-z][0-9]+', ' ') 
data['Green Wrench'] = data['Green Wrench'].str.replace(r' [0-9]+ ', ' ')
data['Green Wrench'] = data['Green Wrench'].str.replace(r'[0-9]+', '')
data.fillna('', inplace = True)
data['final_text'] = data['FAULT COMBINATION'] + data['Green Wrench']
data['final_text'] = data['final_text'].str.replace(r'[^a-z0-9]+', ' ')
data['final_text'] = data['final_text'].str.replace(r' [a-z]{, 2} ', ' ')
data['final_text'] = data['final_text'].str.replace(r'\s+ ', ' ')
data['final_text'] = data['final_text'].str.replace(r'not ([a-z]+)', 'not_\\1')
data['final_text'] = data['final_text'].str.replace(r'([a-z]+) off ', '\\1_off ')
data['final_text'] = data['final_text'].str.replace(r'promotion other see text for description', ' ')
data['final_text'] = data['final_text'].str.replace(r'\b[0-9]+\b', ' ')
data['final_text'] = data['final_text'].str.replace(r'^[0-9]+ ', '')
data['final_text'] = data['final_text'].str.replace(r'out of ([a-z]+)', 'out_of_\\1')
data['final_text'] = data['final_text'].str.replace(r'out of ([a-z]+)', 'out_of_\\1') 
data['final_text'] = data['final_text'].str.replace(r' [a-z][0-9]+', ' ')  
data['final_text'] = data['final_text'].str.replace(r'\s+ ', ' ') 
data['final_text'] = data['final_text'].str.strip()
data.reset_index(inplace = True, drop = True)

# Add Mileage column to data and drop rows which having missing values in Mileage column
data = pd.concat([data, master_file[['Mileage']]], axis = 1)
bool = list(data.Mileage.isnull())
data = data.loc[np.logical_not(bool)]
data.reset_index(inplace = True, drop = True)

# Normalize (words stemming) words in Final_text column
ps = PorterStemmer()
data['final_text'] = data['final_text'].apply(lambda x: ' '.join([ps.stem(y) for y in x.split(' ')]))  
data.reset_index(inplace = True, drop = True)

# Create TFIDF matrix on Final text data
# Note that bi-grams are used alone for features
tfidf_Vectorizer = TfidfVectorizer(ngram_range = (2, 2), stop_words = 'english', use_idf = True)
tfidf_matrix = tfidf_Vectorizer.fit_transform(data['final_text'])

# Combine tfidf data matrix with Mileage and normalize all features
final_features = pd.concat([pd.DataFrame(tfidf_matrix.todense()), data[['Mileage']]], axis = 1)
final_features = preprocessing.scale(final_features)

# Split data into train (80%) and test (20%)
X_train, X_validation, y_train, y_validation = train_test_split(final_features, data.issue_dummy, train_size = 0.80, random_state = 42)

# Use grid search to get optimal value of parameters using 5 fold cross validation on the train data only, leave validation data unseen
# A larger set of parameters may be used for tuning by grid search based on computer RAM and runtime
param = {'learning_rate':[.03], 'n_estimators':[300, 500], 'max_depth':[3, 4], 'subsample':[.9], 'max_features':[.8, .9]}
grid_search_obj = GridSearchCV(GradientBoostingClassifier(), param, cv = 5)
grid_search_obj.fit(X_train, y_train)
print(grid_search_obj.best_estimator_)
print(grid_search_obj.best_score_)

# Apply model on best parameters
best_model = grid_search_obj.best_estimator_
best_model.fit(X_train, y_train)
pred_gb_tst = best_model.predict(X_validation)
print(confusion_matrix(y_validation, pred_gb_tst))

# Get features importances
feature_names = tfidf_Vectorizer.get_feature_names()
feature_names.extend(['Mileage'])
features_imp_data = pd.DataFrame({'feature':feature_names, 'score':list(best_model.feature_importances_)})
features_imp_data.sort_values(by = ['score'], ascending = True, inplace = True)
features_imp_data.reset_index(inplace = True, drop = True)

# Plot top 20 features
feature_imps_top_20 = features_imp_data.iloc[(features_imp_data.shape[0]-20):,:]
feature_imps_top_20.plot(kind = 'barh', x = 'feature', y = 'score', legend = False, figsize = (6, 10))
plt.title('Gradient Boosting: Top 20 Features\' Importance')
plt.xlabel('Importance')
plt.show()
