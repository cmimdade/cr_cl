
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import re  
from sklearn.metrics import confusion_matrix
from sklearn.cross_validation import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.cross_validation import KFold
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, auc

os.chdir("C:\\Users\\ou245\\Documents\\2018\\Product quality\\modelling\python")
###############################################################################################################

def plot_auc(y_score,y_label):
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    
    fpr[0], tpr[0], _ = roc_curve(y_label, y_score)
    roc_auc[0] = auc(fpr[0], tpr[0])
    
    fpr["micro"], tpr["micro"], _ = roc_curve(y_label.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    
    plt.figure()
    lw = 2
    plt.plot(fpr[0], tpr[0], color='red',
             lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[0])
    plt.plot([0, 1], [0, 1], color='green', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example \n (Gradient Boosting Classifier)')
    plt.legend(loc="lower right")
    plt.show()

    
#################################################################################################################    
#################################################################################################################
# Used cleaned incident tracker file as input
master_file = pd.read_csv("new_eds_data_hitesh_1.csv")  
master_file['issue_dummy'] = np.where(master_file['Issue Number']=='Not_Issue',0,1)
master_file = master_file.loc[master_file['Incident Source']=="EDS"]
master_file.reset_index(inplace=True,drop=True)
master_file['Green Wrench'].fillna('', inplace = True)
master_file['Green Wrench']=master_file['Green Wrench'].apply(lambda x: ' '.join(re.findall("K[0-9]+", x)))
master_file['FAULT COMBINATION'] = master_file['FAULT COMBINATION'].str.lower()
master_file['FAULT COMBINATION'] = master_file['FAULT COMBINATION'].str.replace(r'fault codes ([0-9]+)','fc\\1')

############################### CREATING  FEATURES  #############################################################         

master_file['Mileage'] = master_file['Mileage/ Engine Hours'].copy()
master_file['Mileage'] = master_file['Mileage'].str.replace(',','')
master_file['Mileage'] =  master_file['Mileage'].apply(pd.to_numeric, errors='coerce')
master_file = master_file.loc[master_file['Mileage'].apply(str) != 'nan', :]
master_file.reset_index(inplace=True,drop=True)

col = ['issue_dummy','FAULT COMBINATION','Green Wrench','Build Phase','Fail Code','Mileage']
data = master_file[col].copy()
data['FAULT COMBINATION'][data['FAULT COMBINATION'].apply(str) == 'nan'] = 'unknown'
data['Green Wrench'][data['Green Wrench'].apply(str) == ''] = 'unknown'

data1 = pd.get_dummies(data,columns = ['FAULT COMBINATION','Green Wrench','Build Phase','Fail Code'])
del(data1['issue_dummy'])
data.issue_dummy = data.issue_dummy.astype('int')

#######################  SPLITTING DATA INTO TRAIN AND TEST######################################################

X_train, X_validation, y_train, y_validation = train_test_split(data1, data.issue_dummy, train_size=0.80)
X_train = pd.DataFrame(X_train)
y_train = pd.Series(y_train)

######################### GRADIENT BOOSTING CLASSIFIER  ############################################################

model_gb = GradientBoostingClassifier(learning_rate = .03,n_estimators=500,max_depth=4,subsample=.9,max_features=.8)
model_gb.fit(X_train,y_train)

pred_gb_tst = model_gb.predict(X_validation)
print(confusion_matrix(y_validation,pred_gb_tst))

######################################### Feature Importance  #######################################################

features_imp = list(model_gb.feature_importances_)
feature_names = list(X_train.columns)

features_imp_data = pd.DataFrame({'feature':feature_names,'score':features_imp})
features_imp_data['fscore'] = features_imp_data['score']/features_imp_data['score'].sum()
features_imp_data.sort_values(by=['fscore'],ascending=True,inplace=True)
features_imp_data.reset_index(inplace=True,drop=True)

#### plot top 20 features##########
features_imp_data1 = features_imp_data.iloc[(features_imp_data.shape[0]-20):,:]
plt.figure()
features_imp_data1.plot()
features_imp_data1.plot(kind='barh', x='feature', y='fscore', legend=False, figsize=(6, 10))
plt.title('Gradient Boosting: Top 20 Feature Importance')
plt.xlabel('relative importance')
plt.show()

############################### AUC CURVE###############################################################################
y_score  = pd.Series(model_gb.decision_function(X_validation))
plot_auc(y_score,y_validation)


##################################################################################################################################
####################################CROSS VALIDATION###############################################################################

kf = KFold(len(data.issue_dummy),n_folds=5,shuffle=True)

y_preds = pd.DataFrame(np.zeros(data.shape[0]))
y_preds_proba = pd.DataFrame(np.zeros(data.shape[0]))

for train_index, test_index in kf:
   X_train, X_validation = data1.loc[train_index,:], data1.loc[test_index,:]
   y_train, y_validation = data['issue_dummy'][train_index], data['issue_dummy'][test_index]
   X_train = pd.DataFrame(X_train)
   y_train = pd.Series(y_train)
   model_gb = GradientBoostingClassifier(learning_rate = .03,n_estimators=500,max_depth=4,subsample=.9,max_features=.8)
   model_gb.fit(X_train,y_train)
   pred_gb_tst =model_gb.predict(X_validation)
   pred_gb_proba =model_gb.predict_proba(X_validation)
   y_preds.loc[test_index,0] = pred_gb_tst
   y_preds_proba.loc[test_index,0] = pred_gb_proba[:,1]

print(confusion_matrix(data.issue_dummy,y_preds))
print(roc_auc_score(data.issue_dummy, y_preds_proba))

############################### AUC CURVE#################################
plot_auc(y_preds_proba.loc[:,0],data.issue_dummy)

##########################################################################







