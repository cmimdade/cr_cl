# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 12:26:08 2018

@author: ps640
"""

import pandas as pd

def cal_clustering_eval_kpis(clusters, purity_threshold = 0.8):
    """KPI's for evaluation of clustering results
    
    kpi5 - The ratio of pure clusters in all clusters. A cluster is said to be pure if more instances than the threshold parameter
    are of the same class.
    
    kpi6 - Ratio of incidents classified in pure clusters out of all incidents
    
    Parameters
    ----------
    clusters: pandas DataFrame
        DataFrame with actual class and predicted cluster ids for each sample
    
    purity_threshold: float, between 0 and 1, optional
        threshold to determine whether a cluster is pure or not
      
    Returns
    -------
    kpi5: float
        calculated score of kpi5
        
    kpi6: float
        calculated score of kpi6
    
    kpi_df: dataframe
        all clusters, most frequent class within each cluster and its frequency
    """
    all_clusters = sorted(pd.unique(clusters['Cluster_ID']))
    n_clusters = len(all_clusters)
    
    pure_incidents_count = 0
    kpi_df = pd.DataFrame(columns = ['Cluster_ID', 'most_frequent','total_incidents', 'frequent_incidents_num', 'frequency'])
    for cluster_num in all_clusters:
        cluster_data = clusters[clusters['Cluster_ID'] == cluster_num]
        class_freqs = cluster_data['Issue Number'].value_counts()
        max_freq = class_freqs[0]*1.0/len(cluster_data)
        most_freq_class = class_freqs.index[0]
        if max_freq > purity_threshold:
            pure_incidents_count = pure_incidents_count + class_freqs[0]
        kpi_df = kpi_df.append({'Cluster_ID': cluster_num, 'most_frequent': most_freq_class, 'total_incidents': len(cluster_data), 
                                'frequent_incidents_num': class_freqs[0], 'frequency': max_freq}, ignore_index = True)
    
    kpi5 = sum(kpi_df['frequency'] > purity_threshold)*1.0/n_clusters
    kpi6 = pure_incidents_count*1.0/len(clusters)
    return(kpi5, kpi6, kpi_df)
    

path_to_clustering_results = '../Data/Clustering Results McKinsey/'

writer=pd.ExcelWriter(path_to_clustering_results + 'clustering_evaluation_kpi5_kpi6.xlsx')

results_summary = pd.DataFrame(columns = ['file', 'threshold', 'kpi5', 'kpi6'])
for ver in [2, 3]:
    clustering_results = pd.read_csv(path_to_clustering_results + 'complete_initial_clustering_v'+str(ver)+'_Jan05.csv')
    clusters = clustering_results[['Issue Number', 'Cluster_ID']]
    
    for threshold in [0.8, 0.9]:    
        kpi5_score, kpi6_score, pure_clusters_df = cal_clustering_eval_kpis(clusters, purity_threshold=threshold)
        
        issues_clusters_pure = pure_clusters_df[pure_clusters_df['frequency'] > threshold].groupby(by = 'most_frequent')['Cluster_ID'].apply(list)
        
        issues_clusters_pure.to_excel(writer,sheet_name='v'+str(ver) + "_issues_clusters_"+str(threshold))
        
        results_summary = results_summary.append({'file': 'v' + str(ver), 'threshold': threshold, 'kpi5': kpi5_score, 'kpi6': kpi6_score}, 
                                                  ignore_index = True)
    
    pure_clusters_df.to_excel(writer,sheet_name='v'+str(ver)+"_clusters",index=False)
        
results_summary.to_excel(writer, sheet_name = 'KPIs', index = False)
writer.save()
