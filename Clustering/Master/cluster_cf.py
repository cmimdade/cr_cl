#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  9 17:51:41 2018

@author: pp630
"""

# %% Run time options

run_load_pickle = False
run_optimal_weights = False
run_plot_kpis = False
run_cumulative_counts = False
run_plot_tsne = False
run_tsne_data = not run_plot_tsne
run_offline = False

# %% Global parameters
n_processes = 6

data_start_date = '2016-01-01'
data_end_date = '2020-01-01'
filter_engines = '2017'
save_data = False
export_concat_tables = False
export_window_details = False
export_flft = True

# optimal outputs for the year-split version:
optimal_weights, _ = \
    [4.875, 2.75, 3.375, 3.0, 2.125, 2.5, 2.5, 3.625, 4.0, 5.375], 80

# optimal outputs for the 2017-only version:
# optimal_weights, _ = \
#     [4.75, 2.75, 3.5, 1.0, 2.0, 3.75, 0.0, 5.25, 4.0, 4.75], 80

test_range = [0.25 + (s / 4) for s in range(39)]
min_n_clusters = 300
max_n_clusters = 10000

# %% optimal weight parameters
warm_start = ([4.0, 2.5, 3.5, 3.0, 2.5, 2.5, 2.5, 3.5, 4.0, 5.75], 150)
order = 'robust'
n_iterations = 3
n_clusters_range = [s for s in range(100, 200, 10)]
weight_range = [s * 0.25 for s in range(25)]
kpi2_weight = -1
kpi4_weight = 0
penalization_weight = 0.75
affinity = 'default'

# %% Hyperparameters
window_size, window_step = (1, 0), (0, 7)
algorithm, n_clusters_algorithm = 'hierarchical', 'birch_variable'

# %% base RPH settings
cat_columns_list = ['REL_ANALYSIS_RATE_CAT_X1',
                    'REL_ANALYSIS_RATE_CAT_X2',
                    'REL_ANALYSIS_RATE_CAT_X3']

# %% set up some paths
base_path = "/Users/pg875/"
data_path = base_path + "Downloads/"
work_path = base_path + "Desktop/workspace/data/"

online_table_location = \
    data_path + "QUALITY_FEATURES_PARQUET_PERSISTED-2/"

component_table_location = \
    "/Users/pg875/Desktop/workspace/data/component_data_v2/"

# folder path to save data
folder = work_path + 'pickles/'

# path to bath esn table
path_esn_table = data_path + 'OCM_REL_MASTER/' + \
    'part-00000-88c61cbb-f588-4b18-a3c5-0730beacc0e8.csv'

path_eds_table = data_path + 'OCM_EDS_MASTER/' + \
    'part-00000-50337cd6-48b9-4c12-844c-18680283b68e.csv'

path_db_export = work_path + '/database/' + 'aFile.db'

# path to load pickle files
path_pickle_load = work_path + "pickles/"

# path to load esn table
path_esn_table = data_path + '/ADHOC_REL_ENGINE_FEATURES/'

# %% review strategy columns
multivariate_deviation = False
flagging_threshold = 0.3

trim_columns = (['ID',
                 'CLUSTER_ID',
                 'SCORE'] if not(multivariate_deviation) else
                ['ID',
                 'CLUSTER_ID',
                 'SCORE',
                 'PROBABILITY_LIST'])

# %% set some column name parameters

# define features
feature_dict = {'REL_CMP_FAIL_CODE_LIST': 200,  # 200
                'GREEN_WRENCH_FAULT_CODE_LIST': 200,  # 400
                'REL_OEM_NAME': 20,  # 20
                'GREEN_WRENCH_LIST': 200,  # 200
                'REL_ANALYSIS_RATE_CAT': 'specific',  # 3
                'REL_CMP_ENGINE_MILES_LIST': 'specific',
                'REL_CMP_FAIL_DATE_LIST': 'specific',
                'REL_CMP_SUM_NET_AMOUNT': 'impute_take_log',
                # 'CMP_SUM_MATERIALS_AMOUNT':'impute_take_log'
                }

# define flagging column list
col_list_flag = ['ID',
                 'ESN',
                 'REL_CMP_FAIL_CODE_LIST',
                 'GREEN_WRENCH_FAULT_CODE_LIST',
                 'REL_OEM_NAME',
                 'GREEN_WRENCH_LIST',
                 'REL_ANALYSIS_RATE_CAT',
                 'REL_CMP_ENGINE_MILES_LIST',
                 'REL_CMP_FAIL_DATE_LIST',
                 'REL_CMP_SUM_NET_AMOUNT',
                 'EARLIEST_INDICATION_DATE',
                 'EARLIEST_RECORD_DATE',
                 'INCDT_ISSUE_LABEL', 'CLUSTER_ID']

# Define input columns
col_names = [
        'ID',
        'ESN',
        'DSID',
        'INCDT_ISSUE_NUMBER',
        'REL_CMP_SUM_NET_AMOUNT',
        'REL_CMP_PROGRAM_GROUP_CODE',
        'REL_CMP_PROGRAM_GROUP_NAME',
        'REL_CMP_PROGRAM_ACCOUNT_CODE',
        'REL_CMP_FAIL_CODE_LIST',
        'REL_CMP_FAIL_CODE_DESC_LIST',
        'REL_CMP_ENGINE_MILES_LIST',
        'LAST_MILE_RECORDED',
        'GREEN_WRENCH_FAULT_CODE_LIST',
        'GREEN_WRENCH_NUM_LIST',
        'GREEN_WRENCH_LIST',
        'NON_GREEN_WRENCH_FAULT_CODE_LIST',
        'INS_FI_FAULT_CODE_LST',
        'INS_TI_ENGINE_DISTANCE_MILES',
        'REL_BUILD_YEAR',
        'REL_BUILD_MONTH',
        'REL_BUILD_DATE',
        'REL_COVERAGE_CODE',
        'REL_DESIGN_RPM_NUM',
        'REL_MKTG_CONFIG_NUM',
        'REL_MKTG_HSP_NUM',
        'REL_OEM_NAME',
        'REL_OEM_TYPE',
        'REL_OEM_GROUP',
        'REL_SERVICE_ENGINE_MODEL',
        'REL_USER_APPL_DESC',
        'REL_EMISSION_YEAR',
        'REL_ENGINE_NAME_DESC',
        'REL_EMISSIONS_FAMILY_CODE',
        'DSID_CREATE_DATE',
        'EARLIEST_INDICATION_DATE',
        'EARLIEST_RECORD_DATE',
        'INCDT_ISSUE_LABEL',
        'REL_ANALYSIS_RATE_CAT',
        'REL_CMP_CLAIM_DATE_LIST',
        'REL_CMP_FAIL_DATE_LIST']

# %%
