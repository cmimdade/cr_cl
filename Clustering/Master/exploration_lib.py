#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  5 07:52:59 2018

@author: pg875
"""
# %% Librairies
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm
import cluster_cf as cf
import sqlite3
import numpy as np
import clustering_lib as clustlib
import pandas as pd
from collections import Counter
import operator
import random
from scipy import spatial
from plotly.offline import init_notebook_mode, plot
from sklearn.manifold import TSNE
import copy
import itertools
import datetime
import pyarrow.parquet as pq
import pyarrow as pa
tqdm.monitor_interval = 0

# Plot style
plt.style.use('bmh')
plt.rcParams['figure.figsize'] = (8.0, 6.0)


# %% Functions
# Takes as arguments a dataframe, the start window, end_window, results,
# a step ip that the function should extract, and the name of date column
# Returns the labeled subset of the dateframe corresponding the the
# step_id of interest
def extract_window(df,  # data frame containing date_col_name column
                   start_window,  # list of datetimes
                   end_window,  # list of datetimes
                   clustering_results,  # clustering_results (list of labels)
                   step_id,  # step_id that should be extracted (integer)
                   date_col_name='EARLIEST_RECORD_DATE',
                   col_names=None):  # date column name
    sub_df = df[(start_window[step_id] <= df[date_col_name])
                & (df[date_col_name] < end_window[step_id])]
    sub_df['CLUSTER_ID'] = clustering_results[step_id]
    if col_names is not None:
        sub_df = sub_df.loc[:, col_names]
    return(sub_df)

# Takes as arguments a dataframe, the start window, end_window, results,
    # the path range, and the optional name of the date column
# Returns a dictionary of labeled subsets to avoid recomputing them


def map_windows(df,  # data frame containing at least the date_col_name column
                start_window,  # list of datetimes
                end_window,  # list of datetimes
                clustering_results,  # clustering_results (list of labels)
                path_range,
                universal_windows=False,
                # (a,b) where a is the first window index and b the last one
                date_col_name='EARLIEST_RECORD_DATE',
                col_names=None,
                save_data=True,
                folder=cf.folder):  # date column name
    windows = {}
    for i in tqdm(range(path_range[0], path_range[1] + 1)):
        windows[int(i)] = extract_window(df=df,
                                         start_window=start_window,
                                         end_window=end_window,
                                         clustering_results=clustering_results,
                                         step_id=i,
                                         date_col_name=date_col_name,
                                         col_names=col_names)
    if save_data:
        if universal_windows:
            trim_universal_windows = trim_windows(windows,
                                                  columns=['ID',
                                                           'CLUSTER_ID'])
            clustlib.save(trim_universal_windows,
                          filename=folder + 'trim_universal_windows.pckl')

        else:
            trim_windows_new = trim_windows(windows,
                                            columns=['ID',
                                                     'CLUSTER_ID'])
            clustlib.save(trim_windows_new,
                          filename=folder + 'trim_windows.pckl')
    return(windows)

# Takes as arguments the windows dictionary (use previous function),
# an issue_label and an optional path range and date column name
    # if no path_range is given, the range of the windows will be by default
# Returns the optimal path for the given issue_label within the path_range
    # optimal means the cluster that contains the most of this label at each
    # step)


def get_optimal_path(windows,  # dictionary of dataframes
                     issue_label,  # issue label (string)
                     path_range=None,
                     # (a,b) where a is the first window and b the last one
                     date_col_name='EARLIEST_RECORD_DATE'):  # date column name
    path = []
    if path_range is None:
        start_index = min(list(windows.keys()))
        path_length = len(windows.keys())
    else:
        start_index = path_range[0]
        path_length = path_range[1] - path_range[0] + 1
    for i in tqdm(range(path_length)):
        filtered_window = windows[i + start_index].query(
            'INCDT_ISSUE_LABEL == @issue_label')['CLUSTER_ID']
        if filtered_window.shape[0] == 0:
            path.append(-1)
        else:
            path.append(filtered_window.value_counts().idxmax())
    return(path)

# Takes as arguments the windows dictionary (use map_windows function),
    # the path used for evaluation
    # the corresponding start_index, the selected_issue that counts
    # in final count (list of strings)
    # whether or not all labels are taken into consideration
    # and the date column name
# Returns the count of the selected_issues in the corresponding cluster at
# each step of the given path (list of integers of the same length of
# path)


def get_incidents_count(windows,  # dictionary of dataframes
                        path,  # path: list of cluster_ids
                        start_index,  # integer for the first step_id in path
                        selected_issues=None,  # list of strings for the issues
                        count_all=False,  # whether all issues should b
                                          # taken into consideration
                        cumulative=False,
                        date_col_name='EARLIEST_RECORD_DATE'):  # date column
    count = []
    for i in tqdm(range(len(path))):
        if count_all:
            current_cluster = path[i]
            filtered_window = windows[i + start_index].query(
                'CLUSTER_ID == @current_cluster')['ID']
            if not(cumulative) or i == 0:
                if filtered_window.shape[0] == 0:
                    count.append(0)
                else:
                    count.append(filtered_window.count())
            else:
                previous_window = windows[i + start_index - 1]['ID']
                count.append(len(set(filtered_window) - set(previous_window)))
        else:
            if selected_issues is None:
                print(
                    'Please indicate which issue labels to count or ' +
                    'specify count_all = "True"')
            else:
                current_cluster = path[i]
                current_cluster = current_cluster
                filtered_window = windows[i + start_index].query(
                    'INCDT_ISSUE_LABEL in @selected_issues').\
                    query('CLUSTER_ID == @current_cluster')['ID']
                if not(cumulative) or i == 0:
                    if filtered_window.shape[0] == 0:
                        count.append(0)
                    else:
                        count.append(filtered_window.count())
                else:
                    previous_window = windows[i + start_index - 1]['ID']
                    count.append(
                        len(set(filtered_window) - set(previous_window)))
    return(count)

# Takes as arguments the windows dictionary (use map_windows function),
    # an issue_label and an optional path range and date column name
    # if no path_range is given, the range of the windows will be taken
# Returns the total number of the given issue_label at each step_id of the
# path_range


def get_issue_count(windows,  # dictionary of dataframes
                    issue_label,  # issue label (string)
                    path_range=None,
                    # (a,b) where a is the first window and b the last one
                    cumulative=False,
                    date_col_name='EARLIEST_RECORD_DATE'):  # date column name
    count = []
    if path_range is None:
        start_index = min(list(windows.keys()))
        path_length = len(windows.keys())
    else:
        start_index = path_range[0]
        path_length = path_range[1] - path_range[0] + 1
    for i in tqdm(range(path_length)):
        filtered_window = windows[i + start_index].query(
            'INCDT_ISSUE_LABEL == @issue_label')['ID']
        if not(cumulative) or i == 0:
            if filtered_window.shape[0] == 0:
                count.append(0)
            else:
                count.append(filtered_window.count())
        else:
            previous_window = windows[i + start_index - 1]['ID']
            count.append(len(set(filtered_window) - set(previous_window)))
    return(count)

# Takes as arguments the windows dictionary (use map_windows function),
    # the path of interest, the corresponding start_index,
    # the list of issues to be filtered (['not_issue'] or [] generally)
    # and the date column name
# Returns the most common issue number in average in the given path except
# the ones that were in the filter_list


def get_top_issue(windows,  # dictionary of dataframes
                  path,  # path of interest: list of integers (clusters)
                  start_index,  # corresponding start_index
                  filter_list=['not_issue'],  # list of issues to be filtered
                  date_col_name='EARLIEST_RECORD_DATE'):  # date column name
    most_common = []
    for i in tqdm(range(len(path))):
        filtered_window = windows[i + start_index].query(
            'INCDT_ISSUE_LABEL not in @filter_list')['INCDT_ISSUE_LABEL']
        if filtered_window.shape[0] != 0:
            most_common.append(filtered_window.value_counts().idxmax())

    return(max(set(most_common), key=most_common.count))

# %% Takes as argument the entire dataframe, the start window,
    # end window lists, the clustering results,
    # the path range to be considered, the issue label,
    # the windows dictionary if given (otherwise it will be computed),
    # whether or not to include the "not_issue" labels in the computations,
    # and whether or not to use the cumulative functions
# Plots the capture ratio for the clustering


def clustering_capture_ratio(df,  # dataframe
                             start_window,  # list of datetimes
                             end_window,  # list of datetimes
                             clustering_results,  # list of clustering labels
                                                  # for each step
                             path_range,
                             # (a,b) where a is the first window
                             # and b the last one
                             issue_label,  # issue label (string)
                             windows=None,  # windows dictionary
                             include_not_issue=False,  # whether to include
                                                       # the "not_issue"s
                             cumulative=False,  # whether or not or not
                                                # to use cumulative functions
                             alternative_cumulative=False,
                             date_col_name='EARLIEST_RECORD_DATE',
                             # date column name
                             plot_date=True):  # whether to use date as x axis

    if windows is None:

        windows = map_windows(df,
                              start_window,
                              end_window,
                              clustering_results,
                              path_range,
                              date_col_name)

    total_issue = get_issue_count(windows,
                                  issue_label,
                                  path_range,
                                  alternative_cumulative,
                                  date_col_name)

    optimal_path = get_optimal_path(windows,
                                    issue_label,
                                    path_range,
                                    date_col_name)

    start_index = path_range[0]

    captured_issue = get_incidents_count(
        windows=windows,
        path=optimal_path,
        start_index=start_index,
        selected_issues=(
            [
                issue_label,
                'not_issue'] if include_not_issue else [issue_label]),
        count_all=False,
        cumulative=alternative_cumulative,
        date_col_name=date_col_name)

    index_list = [t + start_index for t in range(len(captured_issue))]

    if not(cumulative):
        if plot_date:
            plt.title('Number of incidents over time')
            plt.xlabel('Window index')
            plt.ylabel('Number of incidents')
            plt.plot_date(end_window[start_index:(start_index +
                                                  len(captured_issue))],
                          total_issue,
                          label='Total number of incidents labeled ' +
                          str(issue_label),
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            plot_label = (
                str(issue_label) +
                ' or not_issue' if include_not_issue else str(issue_label))
            plt.plot_date(end_window[start_index:(start_index +
                                                  len(captured_issue))],
                          captured_issue,
                          label='Number of incidents labeled ' +
                          plot_label + ' in top cluster',
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            plt.legend()
            plt.show()
        else:
            plt.title('Number of incidents over time')
            plt.xlabel('Window index')
            plt.ylabel('Number of incidents')
            plt.plot(
                index_list,
                total_issue,
                label='Total number of incidents labeled ' +
                str(issue_label))
            plot_label = (
                str(issue_label) +
                ' or not_issue' if include_not_issue else str(issue_label))
            plt.plot(
                index_list,
                captured_issue,
                label='Number of incidents labeled ' +
                plot_label +
                ' in top cluster')
            plt.legend()
            plt.show()
    else:
        if plot_date:
            plt.title('Cumulative number of incidents over time')
            plt.xlabel('Window index')
            plt.ylabel('Number of incidents')
            plt.plot_date(end_window[start_index:(start_index +
                                                  len(captured_issue))],
                          np.cumsum(total_issue),
                          label='Total number of incidents labeled ' +
                          str(issue_label),
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            plot_label = (
                str(issue_label) +
                ' or not_issue' if include_not_issue else str(issue_label))
            plt.plot_date(end_window[start_index:(start_index +
                                                  len(captured_issue))],
                          np.cumsum(captured_issue),
                          label='Number of incidents labeled ' +
                          plot_label + ' in top cluster',
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            plt.legend()
            plt.show()
        else:
            plt.title('Cumulative number of incidents over time')
            plt.xlabel('Window index')
            plt.ylabel('Number of incidents')
            plt.plot(
                index_list,
                np.cumsum(total_issue),
                label='Total number of incidents labeled ' +
                str(issue_label))
            plot_label = (
                str(issue_label) +
                ' or not_issue' if include_not_issue else str(issue_label))
            plt.plot(
                index_list,
                np.cumsum(captured_issue),
                label='Number of incidents labeled ' +
                plot_label +
                ' in top cluster')
            plt.legend()
            plt.show()

# Takes as argument the entire dataframe, the start window, end window lists,
    # the clustering results,
    # the path range to be considered, the issue label,
    # the windows dictionary if given (otherwise it will be computed),
    # whether or not to plot the total occurences of the issue for comparision,
    # and whether or not to use the cumulative functions
# Plots the purity ratio for the clustering


def clustering_purity_ratio(df,  # dataframe
                            start_window,  # list of datetimes
                            end_window,  # list of datetimes
                            clustering_results,  # list of clustering labels
                                                 # for each step
                            path_range,
                            # (a,b) where a is the first window
                            # and b the last one
                            issue_label,  # issue label (string)
                            windows=None,  # windows dictionary
                            plot_issue_scale=True,  # whether or not to plot
                            # the total occurences of the issue for comparision
                            cumulative=False,  # whether or not or not
                            # to use cumulative functions
                            alternative_cumulative=False,
                            date_col_name='EARLIEST_RECORD_DATE',
                            # date column name
                            plot_date=True):  # whether to set x axis to date

    if windows is None:

        windows = map_windows(df,
                              start_window,
                              end_window,
                              clustering_results,
                              path_range,
                              date_col_name)

    optimal_path = get_optimal_path(windows,
                                    issue_label,
                                    path_range,
                                    date_col_name)

    start_index = path_range[0]

    total_incidents = get_incidents_count(windows=windows,
                                          path=optimal_path,
                                          start_index=start_index,
                                          count_all=True,
                                          cumulative=alternative_cumulative,
                                          date_col_name=date_col_name)

    imputable_incidents = get_incidents_count(
        windows=windows,
        path=optimal_path,
        start_index=start_index,
        selected_issues=[
            issue_label,
            'not_issue'],
        cumulative=alternative_cumulative,
        date_col_name=date_col_name)

    if plot_issue_scale:
        total_issue = get_issue_count(windows,
                                      issue_label,
                                      path_range,
                                      alternative_cumulative,
                                      date_col_name)

    index_list = [t + start_index for t in range(len(imputable_incidents))]

    if not(cumulative):
        if plot_date:
            plt.title('Number of incidents over time')
            plt.xlabel('Window End Date')
            plt.ylabel('Number of incidents')
            plt.plot_date(end_window[start_index:(start_index +
                                                  len(imputable_incidents))],
                          total_incidents,
                          label='Total number of incidents in top cluster',
                          lw=1.5, ls='solid', fillstyle='none', markersize=0)
            plt.plot_date(end_window[start_index:(start_index +
                                                  len(imputable_incidents))],
                          imputable_incidents,
                          label='Number of incidents labeled ' +
                          str(issue_label) + ' or "not_issue" in top cluster',
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            if plot_issue_scale:
                plt.plot_date(
                        end_window[start_index:(start_index +
                                                len(imputable_incidents))],
                        total_issue,
                        label='Total number of incidents labeled ' +
                        str(issue_label),
                        lw=1.5, ls='solid', fillstyle='none',
                        markersize=0)
            plt.legend()
            plt.show()
        else:
            plt.title('Number of incidents over time')
            plt.xlabel('Window index')
            plt.ylabel('Number of incidents')
            plt.plot(
                index_list,
                total_incidents,
                label='Total number of incidents in top cluster')
            plt.plot(
                index_list,
                imputable_incidents,
                label='Number of incidents labeled ' +
                str(issue_label) +
                ' or "not_issue" in top cluster')
            if plot_issue_scale:
                plt.plot(
                    index_list,
                    total_issue,
                    label='Total number of incidents labeled ' +
                    str(issue_label))
            plt.legend()
            plt.show()
    else:
        if plot_date:
            plt.title('Cumulative number of incidents over time')
            plt.xlabel('Window End Date')
            plt.ylabel('Number of incidents')
            plt.plot_date(
                    end_window[start_index:(start_index +
                                            len(imputable_incidents))],
                    np.cumsum(total_incidents),
                    label='Total number of incidents in top cluster',
                    lw=1.5, ls='solid', fillstyle='none', markersize=0)
            plt.plot_date(end_window[start_index:(start_index +
                                                  len(imputable_incidents))],
                          np.cumsum(imputable_incidents),
                          label='Number of incidents labeled ' +
                          str(issue_label) + ' or "not_issue" in top cluster',
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            if plot_issue_scale:
                plt.plot_date(
                        end_window[start_index:(start_index +
                                                len(imputable_incidents))],
                        np.cumsum(total_issue),
                        label='Total number of incidents labeled ' +
                        str(issue_label),
                        lw=1.5, ls='solid', fillstyle='none', markersize=0)
            plt.legend()
            plt.show()
        else:
            plt.title('Cumulative number of incidents over time')
            plt.xlabel('Window index')
            plt.ylabel('Number of incidents')
            plt.plot(index_list, np.cumsum(total_incidents),
                     label='Total number of incidents in top cluster')
            plt.plot(
                index_list,
                np.cumsum(imputable_incidents),
                label='Number of incidents labeled ' +
                str(issue_label) +
                ' or "not_issue" in top cluster')
            if plot_issue_scale:
                plt.plot(
                    index_list,
                    np.cumsum(total_issue),
                    label='Total number of incidents labeled ' +
                    str(issue_label))
            plt.legend()
            plt.show()

# Takes as argument the entire dataframe, the start window,
    # end window lists, the clustering results,
    # the path to be considered, the start index, the issue label to compute,
    # the windows dictionary if given (otherwise it will be computed),
    # whether or not to use the cumulative functions, whether
    # or not "not_issue" can be the top issue in the path
    # and whether or not "not_issue" should be counted in the totals
# Plots the capture ratio for the tracking


def tracking_capture_ratio(df,
                           start_window,
                           end_window,
                           clustering_results,
                           path,
                           start_index,
                           issue_label=None,
                           windows=None,
                           cumulative=False,
                           accept_not_issue=False,
                           count_not_issue=False,
                           alternative_cumulative=False,
                           date_col_name='EARLIEST_RECORD_DATE',
                           plot_date=True):

    path_range = (start_index, start_index + len(path) - 1)

    if windows is None:
        windows = map_windows(df,
                              start_window,
                              end_window,
                              clustering_results,
                              path_range,
                              date_col_name)

    if issue_label is None:
        issue_label = get_top_issue(
            windows,
            path,
            start_index,
            filter_list=(
                ['not_issue'] if not(accept_not_issue) else []),
            date_col_name='EARLIEST_RECORD_DATE')

    optimal_path = get_optimal_path(windows,
                                    issue_label,
                                    path_range,
                                    date_col_name)

    captured_issue = get_incidents_count(
        windows=windows,
        path=optimal_path,
        start_index=start_index,
        selected_issues=(
            [
                issue_label,
                'not_issue'] if count_not_issue else [issue_label]),
        count_all=False,
        cumulative=alternative_cumulative,
        date_col_name=date_col_name)

    tracked_issue = get_incidents_count(
        windows=windows,
        path=path,
        start_index=start_index,
        selected_issues=(
            [
                issue_label,
                'not_issue'] if count_not_issue else [issue_label]),
        count_all=False,
        cumulative=alternative_cumulative,
        date_col_name=date_col_name)

    index_list = [t + start_index for t in range(len(tracked_issue))]
    label_plot = (str(issue_label) if issue_label == 'not_issue' or not(
        count_not_issue) else str(issue_label) + ' or "not_issue"')

    if not(cumulative):
        if plot_date:
            plt.title('Number of incidents over time')
            plt.xlabel('Window End Date')
            plt.ylabel('Number of incidents')
            plt.plot_date(end_window[start_index:(start_index +
                                                  len(path))], captured_issue,
                          label='Number of incidents labeled ' +
                          label_plot + ' in top cluster',
                          lw=1.5, ls='solid', fillstyle='none', markersize=0)
            plt.plot_date(end_window[start_index:(start_index +
                                                  len(path))], tracked_issue,
                          label='Number of incidents labeled ' +
                          label_plot + ' in tracked cluster',
                          lw=1.5, ls='solid', fillstyle='none', markersize=0)
            plt.legend()
            plt.show()
        else:
            plt.title('Number of incidents over time')
            plt.xlabel('Window index')
            plt.ylabel('Number of incidents')
            plt.plot(
                index_list,
                captured_issue,
                label='Number of incidents labeled ' +
                label_plot +
                ' in top cluster')
            plt.plot(
                index_list,
                tracked_issue,
                label='Number of incidents labeled ' +
                label_plot +
                ' in tracked cluster')
            plt.legend()
            plt.show()
    else:
        if plot_date:
            plt.title('Cumulative number of incidents over time')
            plt.xlabel('Window End Date')
            plt.ylabel('Number of incidents')
            plt.plot_date(end_window[start_index:(start_index + len(path))],
                          np.cumsum(captured_issue),
                          label='Number of incidents labeled ' +
                          label_plot + ' in top cluster',
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            plt.plot_date(end_window[start_index:(start_index + len(path))],
                          np.cumsum(tracked_issue),
                          label='Number of incidents labeled ' +
                          label_plot + ' in tracked cluster',
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            plt.legend()
            plt.show()
        else:
            plt.title('Cumulative number of incidents over time')
            plt.xlabel('Window index')
            plt.ylabel('Number of incidents')
            plt.plot(
                index_list,
                np.cumsum(captured_issue),
                label='Number of incidents labeled ' +
                label_plot +
                ' in top cluster')
            plt.plot(
                index_list,
                np.cumsum(tracked_issue),
                label='Number of incidents labeled ' +
                label_plot +
                ' in tracked cluster')
            plt.legend()
            plt.show()

# Takes as argument the entire dataframe, the start window, end window lists,
    # the clustering results,
    # the path to be considered, the start index, the issue label to compute,
    # the windows dictionary if given (otherwise it will be computed),
    # whether or not the total for the optimal path should be
    # plotted for comparision,
    # whether or not to use the cumulative functions,
    # whether or not "not_issue" can be the top issue in the path
    # and whether or not "not_issue" should be counted in the totals
# Plots the purity ratio for the tracking


def tracking_purity_ratio(df,
                          start_window,
                          end_window,
                          clustering_results,
                          path,
                          start_index,
                          issue_label=None,
                          windows=None,
                          plot_optimal_path_scale=True,
                          cumulative=False,
                          accept_not_issue=False,
                          count_not_issue=True,
                          alternative_cumulative=False,
                          date_col_name='EARLIEST_RECORD_DATE',
                          plot_date=True):

    path_range = (start_index, start_index + len(path) - 1)

    if windows is None:

        windows = map_windows(df,
                              start_window,
                              end_window,
                              clustering_results,
                              path_range,
                              date_col_name)

    if issue_label is None:

        issue_label = get_top_issue(
            windows,
            path,
            start_index,
            filter_list=(
                ['not_issue'] if not(accept_not_issue) else []),
            date_col_name='EARLIEST_RECORD_DATE')

    optimal_path = get_optimal_path(windows,
                                    issue_label,
                                    path_range,
                                    date_col_name)

    captured_issue = get_incidents_count(
        windows=windows,
        path=optimal_path,
        start_index=start_index,
        selected_issues=(
            [
                issue_label,
                'not_issue'] if count_not_issue else [issue_label]),
        count_all=False,
        cumulative=alternative_cumulative,
        date_col_name=date_col_name)

    tracked_imputable_incidents = get_incidents_count(
        windows=windows,
        path=path,
        start_index=start_index,
        selected_issues=(
            [
                issue_label,
                'not_issue'] if count_not_issue else [issue_label]),
        count_all=False,
        cumulative=alternative_cumulative,
        date_col_name=date_col_name)

    tracked_total_incidents = get_incidents_count(
        windows=windows,
        path=path,
        start_index=start_index,
        count_all=True,
        cumulative=alternative_cumulative,
        date_col_name=date_col_name)

    index_list = [
        t +
        start_index for t in range(
            len(tracked_imputable_incidents))]
    label_plot = (str(issue_label) if issue_label == 'not_issue' or not(
        count_not_issue) else str(issue_label) + ' or "not_issue"')

    if not(cumulative):
        if plot_date:
            plt.title('Number of incidents over time')
            plt.xlabel('Window End Date')
            plt.ylabel('Number of incidents')
            plt.plot_date(end_window[start_index:(start_index + len(path))],
                          tracked_total_incidents,
                          label='Total number of incidents in tracked cluster',
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            plt.plot_date(end_window[start_index:(start_index + len(path))],
                          tracked_imputable_incidents,
                          label='Number of incidents labeled ' +
                          label_plot + ' in tracked cluster',
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            if plot_optimal_path_scale:
                plt.plot_date(
                        end_window[start_index:(start_index +
                                                len(path))], captured_issue,
                        label='Number of incidents labeled ' +
                        label_plot + ' in top cluster',
                        lw=1.5, ls='solid', fillstyle='none', markersize=0)
            plt.legend()
            plt.show()
        else:
            plt.title('Number of incidents over time')
            plt.xlabel('Window index')
            plt.ylabel('Number of incidents')
            plt.plot(
                index_list,
                tracked_total_incidents,
                label='Total number of incidents in tracked cluster')
            plt.plot(
                index_list,
                tracked_imputable_incidents,
                label='Number of incidents labeled ' +
                label_plot +
                ' in tracked cluster')
            if plot_optimal_path_scale:
                plt.plot(
                    index_list,
                    captured_issue,
                    label='Number of incidents labeled ' +
                    label_plot +
                    ' in top cluster')
            plt.legend()
            plt.show()
    else:
        if plot_date:
            plt.title('Cumulative number of incidents over time')
            plt.xlabel('Window End Date')
            plt.ylabel('Number of incidents')
            plt.plot_date(end_window[start_index:(start_index + len(path))],
                          np.cumsum(tracked_total_incidents),
                          label='Total number of incidents in tracked cluster',
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            plt.plot_date(end_window[start_index:(start_index + len(path))],
                          np.cumsum(tracked_imputable_incidents),
                          label='Number of incidents labeled ' +
                          label_plot + ' in tracked cluster',
                          lw=1.5,
                          ls='solid',
                          fillstyle='none',
                          markersize=0)
            if plot_optimal_path_scale:
                plt.plot_date(end_window[start_index:(start_index +
                                                      len(path))],
                              np.cumsum(captured_issue),
                              label='Number of incidents labeled ' +
                              label_plot + ' in top cluster',
                              lw=1.5,
                              ls='solid',
                              fillstyle='none',
                              markersize=0)
            plt.legend()
            plt.show()
        else:
            plt.title('Cumulative number of incidents over time')
            plt.xlabel('Window index')
            plt.ylabel('Number of incidents')
            plt.plot(index_list, np.cumsum(tracked_total_incidents),
                     label='Total number of incidents in tracked cluster')
            plt.plot(
                index_list,
                np.cumsum(tracked_imputable_incidents),
                label='Number of incidents labeled ' +
                label_plot +
                ' in tracked cluster')
            if plot_optimal_path_scale:
                plt.plot(
                    index_list,
                    np.cumsum(captured_issue),
                    label='Number of incidents labeled ' +
                    label_plot +
                    ' in top cluster')
            plt.legend()
            plt.show()

# Takes as argument the labeled window of interest (dataframe),
        # (optional) the cluster_id if it needs to be selected,
        # whether or not "not_issue" should be filtered or not
        # (optional) the name of the "not_issue" label
# Returns the bar plot of the main issues for the given window


def get_main_issues(window,  # labeled window of interest (dataframe)
                    cluster_id=None,
                    # cluster_id if it should be considered only
                    filter_not_issue=False,
                    # whether or not "not_issue" should be filtered
                    not_issue_label='not_issue'):  # not_issue label name
    if cluster_id is None:
        filtered_df = window
    else:
        filtered_df = window[window['CLUSTER_ID'] == cluster_id]

    if filter_not_issue:
        filtered_df = filtered_df[window['INCDT_ISSUE_LABEL']
                                  != not_issue_label]

    if filtered_df.shape[0] == 0:
        print('No issues found in this cluster')
    else:
        sns.countplot(y='INCDT_ISSUE_LABEL', data=filtered_df)
    return(filtered_df['INCDT_ISSUE_LABEL'].value_counts().idxmax())


def get_top_issues(df,
                   min_val=2):
    df['INCDT_ISSUE_LABEL'].value_counts()[min_val:].index
    return(df)


def plot_kpis(transformed_table,
              start_window,
              end_window,
              clustering_results,
              tracking_path,
              windows,
              kpi2_threshold=0,
              kpi3_threshold=0,
              kpi4_threshold=0,
              include_not_issue=False,
              date_col_name='EARLIEST_RECORD_DATE',
              plot_moving_kpis=True,
              plot_capture_ratio=True,
              plot_culative=True,
              plot_purity_ratio=True
              ):

    top_issues = get_top_issues(df=transformed_table)
    if plot_moving_kpis:
        moving_kpis(transformed_table,
                    start_window,
                    end_window,
                    clustering_results,
                    tracking_path,
                    windows=windows,
                    combine_plots=True,
                    kpi2_threshold=0,
                    kpi3_threshold=0,
                    kpi4_threshold=0)

    # Clustering capture ratio
    if plot_capture_ratio:
        for tracked_issue in top_issues:
                    clustering_capture_ratio(
                            transformed_table,
                            start_window,
                            end_window,
                            clustering_results,
                            tracking_path,
                            tracked_issue,
                            windows,
                            include_not_issue=False,
                            cumulative=False,
                            alternative_cumulative=False,
                            date_col_name='EARLIEST_RECORD_DATE')

    # Cumulative
    if plot_culative:
            for tracked_issue in top_issues:
                clustering_capture_ratio(transformed_table,
                                         start_window,
                                         end_window,
                                         clustering_results,
                                         tracking_path,
                                         tracked_issue,
                                         windows,
                                         cumulative=True,
                                         alternative_cumulative=True)

    # Clustering purity ratio
    if plot_purity_ratio:
            for tracked_issue in top_issues:
                clustering_purity_ratio(transformed_table,
                                        start_window,
                                        end_window,
                                        clustering_results,
                                        tracking_path,
                                        tracked_issue,
                                        windows)

    # Cumulative
    if plot_culative:
            for tracked_issue in top_issues:
                clustering_purity_ratio(transformed_table,
                                        start_window,
                                        end_window,
                                        clustering_results,
                                        tracking_path,
                                        tracked_issue,
                                        windows,
                                        plot_issue_scale=True,
                                        cumulative=True,
                                        alternative_cumulative=True)

    return(True)

# Takes as argument the entire dataframe, the start window,
    # end window lists, the clustering results,
    # the path range to be considered,
    # the windows dictionary if given (otherwise it will be computed),
    # whether or not "the kpis should be ploted
    # in the same graph or on 5 different graphs
    # the kpis thresholds and the date columm name
# Plots the kpis evolution over time for the given path_range


def moving_kpis(df,  # dataframe
                start_window,  # start window: list of datetimes
                end_window,  # end window: list of datetimes
                clustering_results,
                # clustering results for the moving windows: list of lists of
                # labels
                path_range,
                # path range (a,b) where a <= b and a the start index of the
                # considered windows
                windows=None,
                # windows dictionary if given (otherwise it will be computed)
                combine_plots=True,
                # whether or not "the kpis should be ploted in the same graph
                # or on 5 different graphs
                kpi2_threshold=0,  # kpi2 thresholds
                kpi3_threshold=0,  # kpi3 thresholds
                kpi4_threshold=0,  # kpi4 thresholds
                date_col_name='EARLIEST_RECORD_DATE',  # date column name
                plot_date=True):  # set x axis to be date

    if windows is None:
        windows = map_windows(df,
                              start_window,
                              end_window,
                              clustering_results,
                              path_range,
                              date_col_name)
    if plot_date:
        grid = end_window[path_range[0]:(path_range[1] + 1)]
    else:
        grid = [i + path_range[0]
                for i in range(path_range[1] - path_range[0] + 1)]
    print('Computing KPI1 over time')
    kpi1_list = [clustlib.get_kpi(windows[i + path_range[0]], kpi_number=1)
                 for i in range(path_range[1] - path_range[0] + 1)]
    print('Computing KPI2 over time')
    kpi2_list = [clustlib.get_kpi(windows[i + path_range[0]], kpi_number=2,
                                  threshold=kpi2_threshold)
                 for i in range(path_range[1] - path_range[0] + 1)]
    print('Computing KPI3 over time')
    kpi3_list = [clustlib.get_kpi(windows[i + path_range[0]], kpi_number=3,
                                  threshold=kpi3_threshold)
                 for i in range(path_range[1] - path_range[0] + 1)]
    print('Computing KPI4 over time')
    kpi4_list = [clustlib.get_kpi(windows[i + path_range[0]], kpi_number=4,
                                  threshold=kpi4_threshold)
                 for i in range(path_range[1] - path_range[0] + 1)]
    print('Computing Penalization over time')
    penalization_list = [clustlib.get_kpi(
        windows[i + path_range[0]], kpi_number=0)
        for i in range(path_range[1] - path_range[0] + 1)]

    if combine_plots:
        clustlib.plot_kpis(grid,
                           kpi1_list,
                           kpi2_list,
                           kpi3_list,
                           kpi4_list,
                           penalization_list,
                           plot_objective=False,
                           plot_optimal=False,
                           xlabel='Window index',
                           date_type_grid=plot_date)

    elif plot_date:
        plt.plot_date(
            grid,
            kpi1_list,
            lw=1.5,
            ls='solid',
            fillstyle='none',
            markersize=0)
        plt.title('Evolution of KPI1 through time')
        plt.xlabel('Window End Date')
        plt.ylabel('Average number of issues')
        plt.show()

        plt.plot_date(
            grid,
            kpi2_list,
            lw=1.5,
            ls='solid',
            fillstyle='none',
            markersize=0)
        plt.title('Evolution of KPI2 through time')
        plt.xlabel('Window End Date')
        plt.ylabel('Percentage')
        plt.show()

        plt.plot_date(
            grid,
            kpi3_list,
            lw=1.5,
            ls='solid',
            fillstyle='none',
            markersize=0)
        plt.title('Evolution of KPI3 through time')
        plt.xlabel('Window End Date')
        plt.ylabel('Average number of clusters')
        plt.show()

        plt.plot_date(
            grid,
            kpi4_list,
            lw=1.5,
            ls='solid',
            fillstyle='none',
            markersize=0)
        plt.title('Evolution of KPI4 through time')
        plt.xlabel('Window End Date')
        plt.ylabel('Percentage')
        plt.show()

        plt.plot_date(
            grid,
            penalization_list,
            lw=1.5,
            ls='solid',
            fillstyle='none',
            markersize=0)
        plt.title('Evolution of Penalization through time')
        plt.xlabel('Window End Date')
        plt.ylabel('Percentage')
        plt.show()
    else:
        plt.plot(grid, kpi1_list)
        plt.title('Evolution of KPI1 through time')
        plt.xlabel('Window index')
        plt.ylabel('Average number of issues')
        plt.show()

        plt.plot(grid, kpi2_list)
        plt.title('Evolution of KPI2 through time')
        plt.xlabel('Window index')
        plt.ylabel('Percentage')
        plt.show()

        plt.plot(grid, kpi3_list)
        plt.title('Evolution of KPI3 through time')
        plt.xlabel('Window index')
        plt.ylabel('Average number of clusters')
        plt.show()

        plt.plot(grid, kpi4_list)
        plt.title('Evolution of KPI4 through time')
        plt.xlabel('Window index')
        plt.ylabel('Percentage')
        plt.show()

        plt.plot(grid, penalization_list)
        plt.title('Evolution of Penalization through time')
        plt.xlabel('Window index')
        plt.ylabel('Percentage')
        plt.show()

# Takes as argument the total dataframe, the clustering features list,
        # the windows, the step_id and the cluster_id to be charachterized
        # optional thresholds for the mean difference/standard deviation rate:
        # one for the top (positive) features, and one for the bottom features
# Returns a table of the most relevant features and the corresponding score


def characterization(df,
                     clustering_features,
                     windows,
                     step_id,
                     cluster_id,
                     treshold_top=1,
                     treshold_bottom=1,
                     precomputed=False,
                     mean=None,
                     std=None):
    if not precomputed:
        characteristics_list = ((windows[step_id].loc[:,
                                 clustering_features +
                                 ['CLUSTER_ID']].
                                 query('CLUSTER_ID == @cluster_id').
                                 mean(axis=0) -
                                 df.loc[:, clustering_features].mean(axis=0)) /
                                (df.loc[:, clustering_features].
                                 var(axis=0)**0.5)).\
                                 sort_values(ascending=False)

    else:
        characteristics_list = ((windows[step_id].loc[:,
                                 clustering_features +
                                 ['CLUSTER_ID']].
                                 query('CLUSTER_ID == @cluster_id').
                                 mean(axis=0) -
                                 mean) /
                                std).sort_values(ascending=False)

    output = characteristics_list[characteristics_list > treshold_top].\
        append(characteristics_list[characteristics_list <
                                    -treshold_bottom])

    if output.shape[0] == 0:
        output = pd.Series(0, index=['None'])
    return(output)

# Delete redundant columns in the output windows


def trim_windows(detail_review_info,
                 columns=['ID', 'CLUSTER_ID', 'Score', 'Action', 'Reason']):
    output = {}
    for i in detail_review_info.keys():
        output[i] = detail_review_info[i].loc[:, columns]
    return(output)

# Cluster-size-based detection graph for a given issue and given certain
# clustering results


def detection_plot(df,
                   issue_label,
                   windows,
                   tracking_path,
                   start_window,
                   end_window,
                   clustering_results,
                   anchor,
                   tracking_type='hybrid',
                   centroids_threshold=0.05,
                   percentage_threshold=0.8,
                   alt_window=True):

    if not(alt_window):
        b = get_issue_count(windows, issue_label, tracking_path, True)
        step_id = (b.index(max(b)) + tracking_path[0])
        cluster_id = get_optimal_path(
            windows, issue_label, (step_id, step_id))[0]
        tracking_example = clustlib.track_path(
            df=df,
            cluster_id=cluster_id,
            step_id=step_id,
            tracking_path=tracking_path,
            start_window=start_window,
            end_window=end_window,
            clustering_results=clustering_results,
            tracking_type=tracking_type,
            centroids_threshold=centroids_threshold,
            percentage_threshold=percentage_threshold,
            anchor=anchor,
            fixed_source=False)

        a = get_incidents_count(
            windows,
            tracking_example,
            tracking_path[0],
            count_all=True,
            cumulative=True)
        d = get_incidents_count(
            windows,
            tracking_example,
            tracking_path[0],
            selected_issues=[
                issue_label,
                'not_issue'],
            count_all=False,
            cumulative=True)

    else:
        universal_windows = windows
        b = get_issue_count(
            universal_windows,
            issue_label,
            tracking_path,
            True)
        step_id = (b.index(max(b)) + tracking_path[0])
        cluster_id = get_optimal_path(
            universal_windows, issue_label, (step_id, step_id))[0]
        tracking_example = [
            cluster_id for i in range(
                tracking_path[0],
                tracking_path[1] + 1)]
        a = get_incidents_count(
            universal_windows,
            tracking_example,
            tracking_path[0],
            count_all=True,
            cumulative=True)
        d = get_incidents_count(
            universal_windows,
            tracking_example,
            tracking_path[0],
            selected_issues=[
                issue_label,
                'not_issue'],
            count_all=False,
            cumulative=True)

    t = max(a)
    a = np.cumsum(a)
    b = np.cumsum(b)
    d = np.cumsum(d)
    grid = end_window[tracking_path[0]:tracking_path[1] + 1]
    try:
        item = np.nonzero(np.array(a) > t)[0][0]

    except IndexError:
        pass
    item2 = np.nonzero(np.array(b) > 0)[0][0]
    item3 = item2 + 100
    plt.title('Detection plot for issue ' + issue_label)
    plt.xlabel('Window date')
    plt.ylabel('Cumulative number of incidents')

    plt.plot_date(
        grid,
        a,
        label='Number of incidents in the tracked cluster',
        lw=1.5,
        ls='solid',
        fillstyle='none',
        markersize=0)
    plt.plot_date(
        grid,
        b,
        label='Total number of incidents labeled ' +
        issue_label,
        lw=1.5,
        ls='solid',
        fillstyle='none',
        markersize=0)
    plt.plot_date(
        grid,
        d,
        label='Number of incidents in the tracked cluster labeled ' +
        issue_label +
        ' or not_issue',
        color='tab:purple',
        linestyle='-',
        lw=1.5,
        ls='solid',
        fillstyle='none',
        markersize=0)
    try:
        plt.plot_date(
            end_window[item],
            0,
            'r*',
            markersize=20,
            label='Clustering Detection date')
    except NameError:
        pass
    plt.plot_date(
        end_window[item2],
        0,
        'g*',
        markersize=20,
        label='Infant-Care Detection date')
    plt.plot_date(
        end_window[item3],
        0,
        'b*',
        markersize=20,
        label='Current mean Detection date')
    plt.legend()
    plt.show()
    try:
        c = end_window[item2] - end_window[item]
    except NameError:
        c = 0
    print(c)
    return(a, b, c)

# %% Flags and properties
# Flag the incidents for reviews based on the cluster size
    # Returns the flags, the step it was flagged (or not) and the dataframe to
    # describe to zoom in the cluster of interest


def get_flag(cluster_id,
             universal_windows,
             tracking_path,
             column_list,
             flag_threshold=5):
    df = universal_windows[tracking_path[0]].query(
        'CLUSTER_ID == @cluster_id')[column_list]
    output = list(df['INCDT_ISSUE_LABEL'])
    i = tracking_path[0]
    while len(output) < flag_threshold and i <= tracking_path[1]:
        df = pd.concat([df, universal_windows[i].query(
            'CLUSTER_ID == @cluster_id')[column_list]]).drop_duplicates('ID')
        output = list(df['INCDT_ISSUE_LABEL'])
        i = i + 1
    if i > tracking_path[1]:
        i = -1
    return(output, i, df)

# Descriptive statistics of the flagging strategy for the clustering results:
    # - The flagged clusters (list of strings)
    # - The detection time for each flagged issue
    # - Dictionary showing the corresponding dataframe for each flagged cluster


def describe_flags(universal_ids,
                   universal_windows,
                   tracking_path,
                   column_list,
                   end_window,
                   flag_threshold=5):
    max_label = 0
    for i in range(tracking_path[0], tracking_path[1] + 1):
        max_label = max(max_label, max(list(universal_ids[i].values())))

    final = []
    detect_time = {}
    flag_dic = {}
    for cluster_id in tqdm(range(max_label + 1)):
        results, i, df = get_flag(
            cluster_id, universal_windows,
            tracking_path, column_list, flag_threshold)
        if i == -1:
            final.append('not_flagged')
        else:
            results_filtered = set(results) - set(['not_issue'])
            if len(results_filtered) == 0:
                final.append('not_issue')
                if 'not_issue' in flag_dic.keys():
                    flag_dic['not_issue'][cluster_id] = df
                else:
                    flag_dic['not_issue'] = {}
                    flag_dic['not_issue'][cluster_id] = df
            if len(results_filtered) == 1:
                issue = list(results_filtered)[0]
                issue_path = get_issue_count(
                    universal_windows, issue, (tracking_path[0], i), True)
                infant_care_index = np.nonzero(
                    np.array(np.cumsum(issue_path)) > 0)[0][0]
                timedelta = (
                    end_window[infant_care_index +
                               tracking_path[0]] - end_window[i]).days + 100
                if issue in detect_time.keys():
                    detect_time[issue] = max(detect_time[issue], timedelta)
                else:
                    detect_time[issue] = timedelta
                if issue in flag_dic.keys():
                    flag_dic[issue][cluster_id] = df
                else:
                    flag_dic[issue] = {}
                    flag_dic[issue][cluster_id] = df
                final.append(issue)
            else:
                final.append('flagged 2+ issues')
                if 'flagged 2+ issues' in flag_dic.keys():
                    flag_dic['flagged 2+ issues'][cluster_id] = df
                else:
                    flag_dic['flagged 2+ issues'] = {}
                    flag_dic['flagged 2+ issues'][cluster_id] = df
    return(final, detect_time, flag_dic)

# %% Returns all clusters containing at least one occurence of a given fail
# code within the tracking path


def extract_clusters_from_failcodes(df,
                                    string_list,
                                    tracking_path,
                                    universal_windows):
    ids_ofi = set()
    for i in range(tracking_path[0], tracking_path[1] + 1):
        ids_ofi = ids_ofi.union(set(
                universal_windows[i].
                loc[universal_windows[i]['REL_CMP_FAIL_CODE_LIST'].apply(
                    lambda x: not set(x).
                    isdisjoint(string_list)), :]['CLUSTER_ID']))
    return(ids_ofi)

# Similar to "get_incidents_count()" function, return the count of a given
    # list of fail codes (can be all) for a given tracked path
    # the ploth can be cumulative or not


def get_incidents_count_from_failcodes(windows,  # dictionary of dataframes
                                       path,  # path: list of cluster_ids
                                       start_index,
                                       # integer for the first step_id
                                       # in the path
                                       selected_failcodes=None,
                                       # list of strings for the Fail Code
                                       # of interest
                                       count_all=False,
                                       # whether all issues should be
                                       # taken into consideration
                                       cumulative=False,
                                       # whether the count is cumulative or not
                                       date_col_name='EARLIEST_RECORD_DATE'):
                                        # date column name
    count = []
    for i in tqdm(range(len(path))):
        if count_all:
            current_cluster = path[i]
            filtered_window = windows[i + start_index].query(
                'CLUSTER_ID == @current_cluster')['ID']
            if not(cumulative) or i == 0:
                if filtered_window.shape[0] == 0:
                    count.append(0)
                else:
                    count.append(filtered_window.count())
            else:
                previous_window = windows[i + start_index - 1]['ID']
                count.append(len(set(filtered_window) - set(previous_window)))
        else:
            if selected_failcodes is None:
                print(
                    'Please indicate which issue labels to count or ' +
                    'specify count_all = "True"')
            else:
                current_cluster = path[i]
                current_cluster = current_cluster
                filtered_window = windows[i + start_index].\
                    loc[windows[i +
                                start_index]['REL_CMP_FAIL_CODE_LIST'].apply(
                        lambda x:
                            not set(x).isdisjoint(selected_failcodes)), :].\
                    query('CLUSTER_ID == @current_cluster')['ID']

                if not(cumulative) or i == 0:
                    if filtered_window.shape[0] == 0:
                        count.append(0)
                    else:
                        count.append(filtered_window.count())
                else:
                    previous_window = windows[i + start_index - 1]['ID']
                    count.append(
                        len(set(filtered_window) - set(previous_window)))
    return(count)


# Similar to the "characterization()" function, but the describes the
# entirer universal cluster id, without consideration for step id


def universal_characterization(df,
                               clustering_features,
                               windows,
                               tracking_path,
                               cluster_id,
                               treshold_top=1,
                               treshold_bottom=1,
                               universal_labels=None,
                               sort=True):
    if isinstance(cluster_id, float):
        if universal_labels is None:
            id_list = set()
            for i in range(tracking_path[0], tracking_path[1] + 1):
                id_list = id_list.union(
                    set(windows[i].query('CLUSTER_ID == @cluster_id')['ID']))
        else:
            id_list = set(universal_labels[cluster_id])
        if sort:
            characteristics_list = ((df.loc[:, clustering_features +
                                            ['ID']].
                                    query('ID in @id_list').mean(axis=0) -
                                     df.loc[:, clustering_features].
                                     mean(axis=0)) /
                                    (df.loc[:, clustering_features].
                                     var(axis=0)**0.5)).\
                                    sort_values(ascending=False)

        else:
            characteristics_list = ((df.loc[:, clustering_features +
                                            ['ID']].query('ID in @id_list').
                                    mean(axis=0) -
                                     df.loc[:, clustering_features].
                                     mean(axis=0)) /
                                    (df.loc[:, clustering_features].
                                     var(axis=0)**0.5))

    elif isinstance(cluster_id, list) and universal_labels is not None:
        print('lol')

    return(characteristics_list[characteristics_list >= treshold_top].
           append(characteristics_list[characteristics_list <=
                                       treshold_bottom]))

# Plot of the evolution of the total cluster size (and purity (optional))
# vs. the number of incidents with given fail codes


def plot_evolution(cluster_id,
                   universal_windows,
                   tracking_path,
                   end_window,
                   df,
                   clustering_features,
                   selected_failcodes=None,
                   cumulative=False):
    path = [cluster_id for i in range(tracking_path[0], tracking_path[1] + 1)]
    count = get_incidents_count_from_failcodes(
        universal_windows,
        path,
        tracking_path[0],
        selected_failcodes=selected_failcodes,
        count_all=True,
        cumulative=cumulative)

    if selected_failcodes is not None:
        count2 = get_incidents_count_from_failcodes(
            universal_windows,
            path,
            tracking_path[0],
            selected_failcodes=selected_failcodes,
            count_all=False,
            cumulative=cumulative)

    if not cumulative:
        plt.plot_date(end_window[tracking_path[0]:(tracking_path[1] + 1)],
                      count,
                      label='Number of incidents in cluster ' +
                      str(cluster_id),
                      lw=1.5, ls='solid', fillstyle='none',
                      markersize=0)
        if selected_failcodes is not None:

            plt.plot_date(end_window[tracking_path[0]:(tracking_path[1] +
                                                       1)], count2,
                          label='Number of incidents in cluster ' +
                          'with Fail Code in ' +
                          str(selected_failcodes) +
                          ' in ' +
                          str(cluster_id), lw=1.5, ls='solid',
                          fillstyle='none', markersize=0)
    else:
        plt.plot_date(end_window[tracking_path[0]:(tracking_path[1] + 1)],
                      np.cumsum(count),
                      label='Number of incidents in cluster ' +
                      str(cluster_id),
                      lw=1.5,
                      ls='solid',
                      fillstyle='none',
                      markersize=0)
        if selected_failcodes is not None:

            plt.plot_date(end_window[tracking_path[0]:(tracking_path[1] +
                                                       1)], np.cumsum(count2),
                          label='Number of incidents in cluster with ' +
                          'Fail Code in ' +
                          str(selected_failcodes) +
                          ' in ' +
                          str(cluster_id), lw=1.5, ls='solid',
                          fillstyle='none', markersize=0)

    plt.legend()
    plt.title('Evolution of cluster ' + str(cluster_id) + ' through time')
    plt.xlabel('Window date')
    plt.ylabel('Size')
    plt.show()

    print(str(universal_characterization(df,
                                         clustering_features,
                                         universal_windows,
                                         tracking_path,
                                         cluster_id,
                                         treshold_top=1,
                                         treshold_bottom=1)))


def get_closest_cluster(universal_anchor,
                        cluster_id):
    centroid = universal_anchor[cluster_id]
    candidates = list(universal_anchor.values())
    tree = spatial.KDTree(candidates)
    target = tree.query(centroid, k=2)
    return(target[1][1], target[0][1])


def compare_clusters(cluster_1,
                     cluster_2,
                     df,
                     clustering_features,
                     windows,
                     tracking_path,
                     universal_labels=None):
    features_1 = pd.DataFrame(
        universal_characterization(
            df,
            clustering_features,
            windows,
            tracking_path,
            cluster_1,
            treshold_top=0,
            treshold_bottom=0,
            universal_labels=universal_labels,
            sort=False)).reset_index().sort_values(
        ['index'])

    features_2 = pd.DataFrame(
        universal_characterization(
            df,
            clustering_features,
            windows,
            tracking_path,
            cluster_2,
            treshold_top=0,
            treshold_bottom=0,
            universal_labels=universal_labels,
            sort=False)).reset_index().sort_values(
        ['index'])

    output = pd.concat(
        [
            features_1.rename(
                columns={
                    0: 'CLUSTER ' +
                    str(cluster_1),
                    'index': 'FEATURE'}),
            features_2.rename(
                columns={
                    0: 'CLUSTER ' +
                    str(cluster_2)})[
                'CLUSTER ' +
                str(cluster_2)]],
        axis=1)
    return(output[[a or b for a, b
                   in zip(output['CLUSTER ' + str(cluster_1)].
                          apply(lambda x: x >= 1 or x <= -1),
                          output['CLUSTER ' + str(cluster_2)].
                          apply(lambda x: x >= 1 or x <= -1))]])

# %% Helpful ploting functions


def _angle_to_point(point, centre):
    '''calculate angle in 2-D between points and x axis'''
    delta = point - centre
    res = np.arctan(delta[1] / delta[0])
    if delta[0] < 0:
        res += np.pi
    return res


def _draw_triangle(p1, p2, p3, **kwargs):
    tmp = np.vstack((p1, p2, p3))
    x, y = [x[0] for x in zip(tmp.transpose())]
    plt.fill(x, y, **kwargs)


def area_of_triangle(p1, p2, p3):
    '''calculate area of any triangle given co-ordinates of the corners'''
    return np.linalg.norm(np.cross((p2 - p1), (p3 - p1))) / 2.


def convex_hull(points, graphic=False, smidgen=0.0075):
    '''
    Calculate subset of points that make a convex hull around points
    Recursively eliminates points that lie inside
    two neighbouring points until only convex hull is remaining.

    :Parameters:
    points : ndarray (2 x m)
    array of points for which to find hull
    graphic : bool
    use pylab to show progress?
    smidgen : float
    offset for graphic number labels - useful values depend on your data range

    :Returns:
    hull_points : ndarray (2 x n)
    convex hull surrounding points
    '''

    if graphic:
        plt.clf()
        plt.plot(points[0], points[1], 'ro')
    n_pts = points.shape[1]
    # assert(n_pts > 5)
    centre = points.mean(1)
    if graphic:
        plt.plot((centre[0],), (centre[1],), 'bo')
    angles = np.apply_along_axis(_angle_to_point, 0, points, centre)
    pts_ord = points[:, angles.argsort()]
    if graphic:
        for i in range(n_pts):
            plt.text(pts_ord[0, i] + smidgen, pts_ord[1, i] + smidgen,
                     '%d' % i)
    pts = [x[0] for x in zip(pts_ord.transpose())]
    prev_pts = len(pts) + 1
    k = 0
    while prev_pts > n_pts:
        prev_pts = n_pts
        n_pts = len(pts)
        if graphic:
            plt.gca().patches = []
        i = -2
        while i < (n_pts - 2):
            Aij = area_of_triangle(centre, pts[i], pts[(i + 1) % n_pts])
            Ajk = area_of_triangle(centre, pts[(i + 1) % n_pts],
                                   pts[(i + 2) % n_pts])
            Aik = area_of_triangle(centre, pts[i], pts[(i + 2) % n_pts])
            if graphic:
                _draw_triangle(centre, pts[i], pts[(i + 1) % n_pts],
                               facecolor='blue', alpha=0.2)
                _draw_triangle(centre, pts[(i + 1) % n_pts],
                               pts[(i + 2) % n_pts],
                               facecolor='green', alpha=0.2)
                _draw_triangle(centre, pts[i], pts[(i + 2) % n_pts],
                               facecolor='red', alpha=0.2)
            if Aij + Ajk < Aik:
                if graphic:
                    plt.plot((pts[i + 1][0],), (pts[i + 1][1],), 'go')
                del pts[i + 1]
            i += 1
            n_pts = len(pts)
        k += 1
    return np.asarray(pts)

# Extract top clusters given universal ids results


def get_top_clusters(tracking_path,
                     transformed_results,
                     n=50):
    counter = []
    for i in range(tracking_path[0], tracking_path[1] + 1):
        counter.extend(list(transformed_results[i]))

    counter = Counter(np.array(counter))

    counter = sorted(counter.items(), key=operator.itemgetter(1), reverse=True)
    top_clusters = [counter[i][0] for i in range(50)]
    return(top_clusters)

# main function to run the tsne and get resutls


def main_tsne(transformed_table,
              transformed_results,
              clustering_features,
              clustering_ranges,
              optimal_weights,
              data_only=False,
              tracking_path=None,
              universal_windows=None,
              end_window=None,
              detail_review_info=None,
              save_data=True,
              folder=cf.folder,
              col_names=cf.col_names
              ):
    # Compute the feature matrix in 2D with T-SNE
    X_transformed, transformed_table = tsne_results(
        transformed_table=transformed_table,
        clustering_features=clustering_features,
        clustering_ranges=clustering_ranges,
        optimal_weights=optimal_weights,
        seed=123,
        n_components=2,
        perplexity=30.0,
        learning_rate=200.0,
        n_iter=1000,
        n_iter_without_progress=200)

    if save_data:
        print('saving T-SNE results to pickle')
        clustlib.save(
            X_transformed,
            filename=folder + 'X_transformed.pckl')

        trim_table = transformed_table.loc[:, col_names+['index']]
        clustlib.save(
            trim_table,
            filename=folder + 'trim_transformed_table.pckl')

    if not data_only:

        # Get top 50 clusters
        top_clusters = get_top_clusters(
                    tracking_path=tracking_path,
                    transformed_results=transformed_results,
                    n=50)

        # Compute the windows, display the demo, and save it to working file
        display_2D_demo(top_clusters=top_clusters,
                        universal_windows=universal_windows,
                        tracking_path=tracking_path,
                        X_transformed=X_transformed,
                        transformed_table=transformed_table,
                        end_window=end_window,
                        detail_review_info=detail_review_info,
                        frames=1,
                        size=8,
                        filename='demo_80k.html',
                        cumulative=True,
                        cumulative_count=False)

    return(X_transformed, transformed_table)

# Transform the data into 2D T-sne projection


def tsne_results(transformed_table,
                 clustering_features,
                 clustering_ranges,
                 optimal_weights,
                 seed=123,
                 n_components=2,
                 perplexity=30.0,
                 learning_rate=200.0,
                 n_iter=1000,
                 n_iter_without_progress=200):
    X = transformed_table.loc[:, clustering_features].values
    pointer = 0
    for i in tqdm(range(len(clustering_ranges))):
        X[:, pointer:(pointer + clustering_ranges[i])] = optimal_weights[i] * \
            X[:, pointer:(pointer + clustering_ranges[i])]
        pointer = pointer + clustering_ranges[i]

    random.seed(seed)
    tsne = TSNE(n_components=n_components,
                perplexity=perplexity,
                learning_rate=learning_rate,
                n_iter=n_iter,
                n_iter_without_progress=n_iter_without_progress)
    X_transformed = tsne.fit_transform(X)
    transformed_table['index'] = [
            i for i in tqdm(range(transformed_table.shape[0]))]
    return(X_transformed, transformed_table)


def unique_based(a, reverse=True):
    if reverse:
        n = a.shape[0]
        output = []
        rows = []
        for i in reversed(range(n)):
            if not(list(a[i, :]) in output):
                output.append(list(a[i, :]))
                rows.append(True)
            else:
                rows.append(False)
        rows = list(reversed(rows))
        return(rows)
    else:
        n = a.shape[0]
        output = []
        rows = []
        for i in range(n):
            if not(list(a[i, :]) in output):
                output.append(list(a[i, :]))
                rows.append(True)
            else:
                rows.append(False)
        return(rows)

# Create the animation of the 2D clustering results through a tracking_path
    # frames is the frequency of windows shown
    # (5 indicates we show results for each 5 windows)
    # size is the size of the points in the animations
    # filename is the name of the file where the animation will be saved


def display_2D_demo(top_clusters,
                    universal_windows,
                    tracking_path,
                    X_transformed,
                    transformed_table,
                    end_window,
                    detail_review_info,
                    frames=1,
                    size=6,
                    filename='demo.html',
                    cumulative=False,
                    cumulative_count=True):

    init_notebook_mode(connected=True)
    transformed_table['index'] = [i for i in range(transformed_table.shape[0])]
    id_list = {}
    total_id = []

    if cumulative_count:
        info_list = ['INCDT_ISSUE_LABEL',
                     'REL_CMP_FAIL_CODE_LIST',
                     'GREEN_WRENCH_FAULT_CODE_LIST',
                     'REL_OEM_NAME',
                     'GREEN_WRENCH_NUM_LIST',
                     'REL_ANALYSIS_RATE_CAT',
                     'REL_CMP_ENGINE_MILES_LIST',
                     'REL_CMP_FAIL_DATE_LIST',
                     'REL_CMP_SUM_NET_AMOUNT',
                     'Action',
                     'Reason',
                     'Score',
                     'CUM_INCDT_IN_CLUSTER']

        display_info_list = [
            'Issue',
            'Fail Code',
            'Fault Code',
            'OEM',
            'Green Wrench',
            'Engine Category',
            'Miles',
            'Fail Date',
            'Total Cost',
            'Action',
            'Reason',
            'Score',
            'Number of incidents in cluster up to incident date']
    else:

        info_list = ['INCDT_ISSUE_LABEL',
                     'REL_CMP_FAIL_CODE_LIST',
                     'GREEN_WRENCH_FAULT_CODE_LIST',
                     'REL_OEM_NAME',
                     'GREEN_WRENCH_NUM_LIST',
                     'REL_ANALYSIS_RATE_CAT',
                     'REL_CMP_ENGINE_MILES_LIST',
                     'REL_CMP_FAIL_DATE_LIST',
                     'REL_CMP_SUM_NET_AMOUNT',
                     'Action',
                     'Reason',
                     'Score']

        display_info_list = ['Issue',
                             'Fail Code',
                             'Fault Code',
                             'OEM',
                             'Green Wrench',
                             'Engine Category',
                             'Miles',
                             'Fail Date',
                             'Total Cost',
                             'Action',
                             'Reason',
                             'Score']

    for cluster_id in top_clusters:
        id_list[cluster_id] = []
        for i in range(tracking_path[0], tracking_path[1] + 1):
            id_list[cluster_id].append(
                set(universal_windows[i].
                    query('CLUSTER_ID == @cluster_id')['ID']))

    # total_id_list = []
    for i in range(tracking_path[0], tracking_path[1] + 1):
        current_id_list = set()
        for cluster_id in top_clusters:
            current_id_list = current_id_list.union(id_list[cluster_id][i])
        total_id.append(set(universal_windows[i]['ID']) - current_id_list)

    x = {}
    y = {}
    x_prime = {}
    y_prime = {}
    label = {}
    label_prime = {}
    marker = {}
    marker_prime = {}
    info = {}
    info_prime = {}
    for i in tqdm(range(tracking_path[0], tracking_path[1] + 1, frames)):
        label[i] = {}
        X_plot = {}
        x[i] = {}
        y[i] = {}
        marker[i] = {}
        info[i] = {}
        for cluster_id in sorted(list(top_clusters)):
            X_plot[cluster_id] = \
                X_transformed[transformed_table[transformed_table['ID'].apply(
                        lambda x: x in id_list[cluster_id][i])].
                sort_values(['ID'])['index'].tolist(), :]
            x[i][cluster_id] = X_plot[cluster_id][:, 0]
            y[i][cluster_id] = X_plot[cluster_id][:, 1]
            label[i][cluster_id] = np.array(
                ['Incident ID: ' + str(string)
                    for string in sorted(list(id_list[cluster_id][i]))])
            marker[i][cluster_id] = \
                np.array(detail_review_info[str(end_window[i])[
                                             :10]].sort_values('ID').
                         query('CLUSTER_ID == @cluster_id')['Reason'])
            marker[i][cluster_id] = np.array(
                list(
                    map(
                        lambda x: "circle" if x ==
                        'Old_Incident / Checked_Before' else(
                            "circle-open-dot" if x ==
                            'Belongs to Reviewed Cluster and Close to Centroid'
                            else (
                                "square-open-dot" if x ==
                                'Belongs to Reviewed Cluster but Far away ' +
                                'from Centroid' else "diamond-open-dot")),
                        marker[i][cluster_id])))
            info[i][cluster_id] = np.array(
                        detail_review_info[str(end_window[i])[:10]].
                        sort_values('ID').
                        query('CLUSTER_ID == @cluster_id').loc[:, info_list])
            df = np.array(
                    detail_review_info[str(end_window[i])[:10]].
                    sort_values('ID')[detail_review_info[str(
                            end_window[i])[:10]].
                            sort_values('ID')['CLUSTER_ID'] ==
                            cluster_id].loc[:, info_list])
            label[i][cluster_id] = \
                np.array([''.join([label[i][cluster_id][j] + ' '] +
                                  ['</br> ' + display_info_list[k] +
                                   ': ' + str(
                                           df[j, k]) +
                                   ' ' for k in range(len(info_list))])
                          for j in range(len(label[i][cluster_id]))])
        X_reference = \
            X_transformed[transformed_table[transformed_table['ID'].apply(
                    lambda x: x in total_id[i])].
                sort_values(['ID'])['index'].tolist(), :]
        x_prime[i] = X_reference[:, 0]
        y_prime[i] = X_reference[:, 1]
        label_prime[i] = np.array(['Incident ID: ' + str(string)
                                   for string in sorted(list(total_id[i]))])
        marker_prime[i] = np.array(detail_review_info[
                str(end_window[i])[:10]][detail_review_info[str(
                        end_window[i])[:10]]['ID'].
            apply(lambda x: x in total_id[i])].sort_values('ID')['Reason'])

        marker_prime[i] = np.array(list(map(lambda x: "circle" if x ==
                                   'Old_Incident / Checked_Before' else (
                                           301 if x ==
                                           'Belong to Reviewed Cluster and ' +
                                           'Close to Centriod' else 302),
                                           marker_prime[i])))
        info_prime[i] = \
            np.array(detail_review_info[str(end_window[i])[:10]].query(
                    'ID in @total_id[@i]').sort_values('ID').loc[:, info_list])

        df = \
            np.array(detail_review_info[str(end_window[i])[:10]].
                     loc[detail_review_info[str(
                             end_window[i])[:10]]['ID'].isin(total_id[i]), :].
                     sort_values('ID').loc[:, info_list])
        label_prime[i] = np.array([''.join([label_prime[i][j] + ' '] +
                                  ['</br> ' + display_info_list[k] + ': ' +
                                   str(
                                   df[j, k]) + ' '
                                   for k in range(len(info_list))])
                                   for j in range(len(label_prime[i]))])

    steps = [i for i in range(tracking_path[0], tracking_path[1] + 1, frames)]

    if cumulative:
        for i in tqdm(range(tracking_path[0], tracking_path[1] + 1, frames)):
            if i != tracking_path[0]:

                x_prime[i] = np.concatenate((x_prime[i - frames], x_prime[i]))
                y_prime[i] = np.concatenate((y_prime[i - frames], y_prime[i]))
                nrows = unique_based(
                        np.array([[a, b] for a, b in zip(x_prime[i],
                                  y_prime[i])]), reverse=False)
                ord_rows = unique_based(
                        np.array([[a, b] for a, b in zip(x_prime[i],
                                  y_prime[i])]), reverse=True)
                x_prime[i] = x_prime[i][nrows]
                y_prime[i] = y_prime[i][nrows]
                label_prime[i] = np.concatenate(
                    (label_prime[i - frames], label_prime[i]))[ord_rows]
                marker_prime[i] = np.concatenate(
                    (marker_prime[i - frames], marker_prime[i]))[ord_rows]
                for cluster_id in sorted(list(top_clusters)):
                    x[i][cluster_id] = np.concatenate(
                        (x[i - frames][cluster_id], x[i][cluster_id]))
                    y[i][cluster_id] = np.concatenate(
                        (y[i - frames][cluster_id], y[i][cluster_id]))
                    nrows = unique_based(np.array([[a, b] for a, b in zip(
                        x[i][cluster_id], y[i][cluster_id])]), reverse=False)
                    ord_rows = unique_based(np.array([[a, b] for a, b in zip(
                        x[i][cluster_id], y[i][cluster_id])]), reverse=True)
                    x[i][cluster_id] = x[i][cluster_id][nrows]
                    y[i][cluster_id] = y[i][cluster_id][nrows]
                    label[i][cluster_id] = np.concatenate(
                        (label[i - frames][cluster_id],
                         label[i][cluster_id]))[ord_rows]
                    marker[i][cluster_id] = np.concatenate(
                        (marker[i - frames][cluster_id],
                         marker[i][cluster_id]))[ord_rows]

    # make figure
    figure = {
        'data': [],
        'layout': {},
        'frames': []
    }

    # fill in most of layout
    figure['layout']['title'] = '2D T-SNE Animation of the clustering results'
    figure['layout']['xaxis'] = {'range': [-100, 100]}
    figure['layout']['yaxis'] = {'range': [-100, 100]}
    figure['layout']['hovermode'] = 'closest'
    figure['layout']['sliders'] = {
        'args': [
            'transition', {
                'duration': 200,
                'easing': 'cubic-in-out'
            }
        ],
        'initialValue': str(tracking_path[0]),
        'plotlycommand': 'animate',
        'values': steps,
        'visible': True
    }
    figure['layout']['updatemenus'] = [
        {
            'buttons': [
                {
                    'args': [None, {'frame': {'duration': 10, 'redraw': True},
                                    'fromcurrent': True,
                                    'transition': {'duration': 5}}],
                    'label': 'Play',
                    'method': 'animate'
                },
                {
                    'args': [[None], {'frame': {'duration': 0, 'redraw': True},
                                      'mode': 'immediate',
                                      'transition': {'duration': 5}}],
                    'label': 'Pause',
                    'method': 'animate'
                }
            ],
            'direction': 'left',
            'pad': {'r': 10, 't': 87},
            'showactive': False,
            'type': 'buttons',
                    'x': 0.1,
                    'xanchor': 'right',
                    'y': 0,
                    'yanchor': 'top'
        }
    ]

    sliders_dict = {
        'active': 0,
        'yanchor': 'top',
        'xanchor': 'left',
        'currentvalue': {
            'font': {'size': 20},
            'prefix': 'Window:',
            'visible': True,
            'xanchor': 'right'
        },
        'transition': {'duration': 200, 'easing': 'cubic-in-out'},
        'pad': {'b': 10, 't': 50},
        'len': 0.9,
        'x': 0.1,
        'y': 0,
        'steps': []
    }

    # make data
    step = tracking_path[0]
    for cluster_id in sorted(list(top_clusters)):

        data_dict = {
            'x': x[step][cluster_id],
            'y': y[step][cluster_id],
            'mode': 'markers',
            'text': label[step][cluster_id],
            'marker': {
                'sizemode': 'area',
                'sizeref': 200000,
                'size': size,
                'symbol': marker[step][cluster_id]


                # 'color': color_dic[cluster_id]
            },
            'name': 'Cluster ' + str(cluster_id)
        }

        figure['data'].append(data_dict)

    data_dict = {
        'x': x_prime[step],
        'y': y_prime[step],
        'mode': 'markers',
        'text': label_prime[step],
        'marker': {
            'sizemode': 'area',
            'sizeref': 200000,
            'size': size,
            'color': 'grey',
            'opacity': 0.10,
            'symbol': marker_prime[step]
        },
        'name': 'Other clusters',
    }

    figure['data'].append(data_dict)

    # make frames
    for step in steps:
        frame = {'data': [], 'name': str(step)}
        for cluster_id in sorted(list(top_clusters)):

            data_dict = {
                'x': x[step][cluster_id],
                'y': y[step][cluster_id],
                'mode': 'markers',
                'text': label[step][cluster_id],
                'marker': {
                    'sizemode': 'area',
                    'sizeref': 200000,
                    'size': size,
                    'symbol': marker[step][cluster_id]
                    # 'color': color_dic[cluster_id]
                },
                'name': 'Cluster ' + str(cluster_id)
            }

            frame['data'].append(data_dict)

        data_dict = {
            'x': x_prime[step],
            'y': y_prime[step],
            'mode': 'markers',
            'text': label_prime[step],
            'marker': {
                'sizemode': 'area',
                'sizeref': 200000,
                'size': size,
                'color': 'grey',
                'opacity': 0.10,
                'symbol': marker_prime[step]
            },
            'name': 'Other clusters',
        }

        frame['data'].append(data_dict)

        figure['frames'].append(frame)
        slider_step = {'args': [
            [step],
            {'frame': {'duration': 200, 'redraw': True},
             'transition': {'duration': 50}}
        ],
            'label': str(end_window[step])[:10],
            'method': 'animate'}
        sliders_dict['steps'].append(slider_step)

    figure['layout']['sliders'] = [sliders_dict]

    plot(figure, filename=filename)

# Calculates for each universal cluster id, the cumulative number of incidents
    # belonging to this cluster for each window, up to the corresponding
    # end window date


def cumulative_counter(universal_windows,
                       universal_labels,
                       tracking_path):
    cumulative_ids = {}
    len_ids = {}
    window_clusters = {}
    for i in tqdm(range(tracking_path[0], tracking_path[1] + 1)):
        universal_windows[i]['CUM_INCDT_IN_CLUSTER'] = 0
        cumulative_ids[i] = {}
        len_ids[i] = {}
        window_clusters[i] = set(universal_windows[i]['CLUSTER_ID'])
        for j in universal_labels.keys():
            if j in window_clusters[i]:
                cumulative_ids[i][j] = set(
                    universal_windows[i].query('CLUSTER_ID == @j')['ID'])
            else:
                cumulative_ids[i][j] = set()

    for j in universal_labels.keys():
        len_ids[tracking_path[0]][j] = len(cumulative_ids[tracking_path[0]][j])

    universal_windows[tracking_path[0]].loc[:, 'CUM_INCDT_IN_CLUSTER'] = \
        universal_windows[tracking_path[0]]['CLUSTER_ID']. \
        apply(lambda x: len_ids[tracking_path[0]][x])

    for i in tqdm(range(tracking_path[0] + 1, tracking_path[1] + 1)):
        for j in universal_labels.keys():
            cumulative_ids[i][j] = cumulative_ids[i][j].union(
                cumulative_ids[i - 1][j])
            len_ids[i][j] = len(cumulative_ids[i][j])
        universal_windows[i].loc[:, 'CUM_INCDT_IN_CLUSTER'] = \
            universal_windows[i]['CLUSTER_ID'].apply(
            lambda x: len_ids[i][x])
    return(universal_windows, cumulative_ids)

# Display only the incidents that have a particular Fail code


def display_filtered_2D_demo(top_clusters,
                             universal_windows,
                             tracking_path,
                             X_transformed,
                             transformed_table,
                             end_window,
                             detail_review_info,
                             frames=1,
                             size=9,
                             filename='demo_full_Mar20.html',
                             fail_code_filter=None,
                             fault_code_filter=None):
    filter_indices = None
    if fail_code_filter is not None:
        filter_indices = transformed_table.REL_CMP_FAIL_CODE_LIST.apply(
            lambda x: fail_code_filter in x)
    if fault_code_filter is not None:
        if filter_indices is None:
            filter_indices = transformed_table.\
                GREEN_WRENCH_FAULT_CODE_LIST.apply(
                        lambda x: fault_code_filter in x)
        else:
            filter_indices = filter_indices & transformed_table.\
                GREEN_WRENCH_FAULT_CODE_LIST.apply(
                        lambda x: fault_code_filter in x)

    universal_windows_rep = copy.deepcopy(universal_windows)
    detail_review_info_rep = detail_review_info.copy()
    if filter_indices is None:
        X_transformed_rep = X_transformed
        transformed_table_rep = transformed_table

    elif np.sum(filter_indices) > 0:
        X_transformed_rep = X_transformed[filter_indices, :]
        transformed_table_rep = transformed_table.loc[filter_indices, :]
        filtered_ID_list = transformed_table_rep.ID.values
        for cur_key in universal_windows_rep.keys():
            universal_windows_rep[cur_key] = universal_windows_rep[cur_key].\
                loc[universal_windows_rep[cur_key].ID.
                    isin(filtered_ID_list), :]
        for cur_key in detail_review_info_rep.keys():
            detail_review_info_rep[cur_key] = detail_review_info_rep[cur_key].\
                loc[detail_review_info_rep[cur_key].ID.
                    isin(filtered_ID_list), :]
            # debug use
#             if 'ID' not in detail_review_info_rep[cur_key].columns:
#                 print(cur_key)
#                 print(detail_review_info_rep[cur_key])
    else:
        print('No incident left after filtering')
        return

    display_2D_demo(top_clusters=top_clusters,
                    universal_windows=universal_windows_rep,
                    tracking_path=tracking_path,
                    X_transformed=X_transformed_rep,
                    transformed_table=transformed_table_rep,
                    end_window=end_window,
                    detail_review_info=detail_review_info_rep,
                    frames=frames,
                    size=size,
                    filename=filename)

# %% Build percentages dataframe


def get_percentages_df(feature_dict,
                       clustering_ranges,
                       clustering_features,
                       universal_labels,
                       transformed_table,
                       clean_specific=True):
    feature_list = clustlib.reproduce_features(feature_dict,
                                               clustering_features,
                                               clustering_ranges)
    oem_columns_list = ['REL_OEM_NAME_' + feature_list['REL_OEM_NAME'][i]
                        for i in range(len(feature_list['REL_OEM_NAME']))]
    cat_columns_list = ['REL_ANALYSIS_RATE_CAT_X1',
                        'REL_ANALYSIS_RATE_CAT_X2',
                        'REL_ANALYSIS_RATE_CAT_X3']
    year_columns_list = ['REL_ANALYSIS_RATE_CAT_2013',
                         'REL_ANALYSIS_RATE_CAT_2017']

    percentages_df = pd.DataFrame()
    feature_columns_list = [oem_columns_list,
                            cat_columns_list,
                            year_columns_list]
    new_col_names = [('OEM_PERC', 'OEM_NAME'),
                     ('CAT_PERC', 'CAT_NAME'),
                     ('YEAR_PERC', 'YEAR')]
    for j in range(len(feature_columns_list)):
        value = []
        for i in tqdm(universal_labels.keys()):
            value.append(transformed_table.query(
                    'ID in @universal_labels[@i]')[feature_columns_list[j]].
                mean(axis=0).max())
        index = []
        for i in tqdm(universal_labels.keys()):
            index.append(transformed_table.query(
                    'ID in @universal_labels[@i]')[feature_columns_list[j]].
                    mean(axis=0).idxmax())
        percentages_df[new_col_names[j][0]] = value
        percentages_df[new_col_names[j][1]] = index

    if clean_specific:
        percentages_df['REL_ANALYSIS_RATE_CAT'] = percentages_df['CAT_NAME'].\
            apply(lambda x: 'X1' if x.find('X1') >= 0
                  else ('X2' if x.find('X2') >= 0
                        else 'X3'))

        percentages_df['REL_OEM_NAME'] = percentages_df['OEM_NAME'].\
            apply(lambda x:  x[len('REL_OEM_NAME_'):])
        percentages_df['REL_ANALYSIS_YEAR'] = percentages_df['YEAR'].\
            apply(lambda x:  x[len('REL_ANALYSIS_RATE_CAT_'):])
    return(percentages_df, feature_list)


# Computing RPH detailed table


def get_rph_detailed_table(transformed_table,
                           percentages_df,
                           universal_labels,
                           path_esn_table,
                           path_eds_table,
                           feature_list,
                           cat_columns_list
                           ):
    total_table = pd.read_csv(path_esn_table)
    eds_table = pd.read_csv(path_eds_table)
    total_table = total_table.append(eds_table)
#    oem_name_list = feature_list['REL_OEM_NAME']

    total_table_filtered = total_table.groupby('ESN').first().reset_index()
    del total_table

    total_table_filtered['REL_BUILD_DATE'] = \
        total_table_filtered['REL_BUILD_DATE'].apply(
                lambda x: datetime.datetime.strptime(x, '%Y-%m-%d')
                if isinstance(x, str) else x)

    amin = clustlib.increment_date(
            transformed_table['EARLIEST_RECORD_DATE'].min(), -24, 0)
    amax = transformed_table['EARLIEST_RECORD_DATE'].max()
    boolmin = total_table_filtered['REL_BUILD_DATE'] > amin
    boolmax = total_table_filtered['REL_BUILD_DATE'] < amax
    boolrange = [a and b for a, b in zip(boolmin, boolmax)]
    total_table_filtered = total_table_filtered.loc[boolrange, :]

    # Computing RPH detailed table

    percentages_df_total, total_table_filtered = get_rph(
            percentages_df=percentages_df,
            total_table_filtered=total_table_filtered,
            count_dict=[len(universal_labels[i])
                        for i in range(percentages_df.shape[0])],
            threshold=0.8)
    return(percentages_df_total, total_table_filtered)

# Compute detailed RPH table


def get_rph(percentages_df,
            total_table_filtered,
            count_dict,
            threshold=.8):

    total_table_filtered['REL_ANALYSIS_RATE_CAT'] = total_table_filtered[
            'REL_ENGINE_NAME_DESC'].apply(lambda x: 'X1' if x.find('1 ') >= 0
                                          else ('X2' if x.find('2 ') >= 0
                                          else 'X3'))

    total_table_filtered['REL_ANALYSIS_YEAR'] = total_table_filtered[
            'REL_ENGINE_NAME_DESC'].apply(
            lambda x: '2013' if x.find('2013') >= 0
            else '2017')

    count_table = pd.DataFrame()

    all_features = ['REL_ANALYSIS_YEAR',
                    'REL_ANALYSIS_RATE_CAT',
                    'REL_OEM_NAME']

    for i in range(len(all_features)):
        for j in list(itertools.combinations(all_features, i+1)):
            count_table = count_table.append(
                    total_table_filtered.groupby(j)['ESN'].count().
                    reset_index())

    count_table = count_table.fillna('All')
    new_row = [total_table_filtered.shape[0], 'All', 'All', 'All']
    count_table = count_table.append(pd.DataFrame(data=[new_row],
                                                  columns=count_table.columns))

    scores = []
    oem_count_list = set(count_table['REL_OEM_NAME'])
    cat_count_list = set(count_table['REL_ANALYSIS_RATE_CAT'])
    year_count_list = set(count_table['REL_ANALYSIS_YEAR'])
    for i in tqdm(range(percentages_df.shape[0])):
        row = percentages_df.iloc[i, :]
        oem = (row['REL_OEM_NAME']
               if row['OEM_PERC'] > threshold
               and row['REL_OEM_NAME'] in oem_count_list
               else 'All')
        engine_cat = (row['REL_ANALYSIS_RATE_CAT']
                      if row['CAT_PERC'] > threshold
                      and row['REL_ANALYSIS_RATE_CAT'] in cat_count_list
                      else 'All')

        year = (row['REL_ANALYSIS_YEAR']
                if row['YEAR_PERC'] > threshold
                and row['REL_ANALYSIS_YEAR'] in year_count_list
                else 'All')

        oem, engine_cat, year = oem, engine_cat, year
        output = count_table.query('REL_OEM_NAME == @oem').\
            query('REL_ANALYSIS_RATE_CAT == @engine_cat').\
            query('REL_ANALYSIS_YEAR == @year')['ESN']

        if output.shape[0] != 0:
            score = int(count_table.query('REL_OEM_NAME == @oem').
                        query('REL_ANALYSIS_RATE_CAT == @engine_cat').
                        query('REL_ANALYSIS_YEAR == @year')['ESN'])
        else:
            score = 0
        scores.append(score)

    percentages_df['Subpopulation'] = scores
    percentages_df['Size'] = count_dict
    percentages_df['RPH'] = [100*(a/b) if b != 0
                             else None for a, b
                             in zip(percentages_df['Size'],
                                    percentages_df['Subpopulation'])]

    return(percentages_df,
           total_table_filtered)

# %% Export data into reporting layer


def export_results(db_name,
                   transformed_table,
                   detail_review_info,
                   col_names,
                   export_df,
                   export_concat_windows,
                   export_tsne=False,
                   X_transformed=None,
                   export_window_drivers=False,
                   clustering_features=None,
                   universal_windows=None,
                   tracking_path=None,
                   export_window_details=False,
                   export_flft=True):
    # Connect to the database
    db = sqlite3.connect(db_name)
    
    # Main incident list
    # Subset fields
    x_transformed_table = transformed_table[col_names]
    x_transformed_table['INCDT_ID'] = x_transformed_table['ID']

    # Convert columns to string
    # (need to explicity convert all non-numeric fields)
    non_numeric_cols = x_transformed_table.dtypes[x_transformed_table.dtypes == "object"].index.tolist()
    x_transformed_table[non_numeric_cols] = x_transformed_table[non_numeric_cols].astype(str)

    for i in list(x_transformed_table.dtypes[x_transformed_table.dtypes ==
                                             'datetime64[ns]'].index):
        x_transformed_table[i] = x_transformed_table[i].apply(lambda x: str(x))

    for i in list(x_transformed_table.dtypes[x_transformed_table.dtypes ==
                                             'float64'].index):
        x_transformed_table[i] = x_transformed_table[i].apply(
                lambda x: round(x, 8))

    # Write to database
    import pyarrow as pa
    pa.Table.from_pandas(x_transformed_table).parquet.write_table(x, 'incidents.parquet')

    
    pq.write_table(x_transformed_table, 'incidents.parquet')
    x_transformed_table.to_sql("incident_details", db, if_exists="replace")
    x_transformed_pyarrow = pa.Table.from_pandas(x_transformed_table)
    pq.write_table(x_transformed_pyarrow, 'incidents_details.parquet')

    if export_tsne:
        # T-SNE coordinates
        X_transformed_df = pd.DataFrame(data=X_transformed,
                                        index=transformed_table["ID"],
                                        columns=['X', 'Y'])
        X_transformed_df = X_transformed_df.reset_index().\
            rename(columns={'ID': 'INCDT_ID'})
        X_transformed_df.to_sql("incident_coordinates",
                                db,
                                if_exists="replace")


#     Cluster drivers (in total)
#     ??? Still need to turn this into a loop
#     xx = explib.universal_characterization(
#             df=transformed_table,
#             clustering_features=clustering_features,
#             windows=universal_windows,
#             tracking_path=tracking_path,
#             cluster_id=10)

        X_transformed_df_pyarrow = pa.Table.from_pandas(X_transformed_df)
        pq.write_table(X_transformed_df_pyarrow,
                       'incident_coordinates.parquet')

    if export_window_drivers:
        # Cluster drivers (by window)
        # Window by window
        c_drivers = pd.DataFrame()
        c_drivers = list()

        mean = transformed_table.loc[:, clustering_features].mean(axis=0)
        std = transformed_table.loc[:, clustering_features].var(axis=0)**0.5
        
        for i in tqdm(range(0, len(universal_windows))):
            for c in list(set(universal_windows[i]["CLUSTER_ID"])):
                # get values
                temp_d = characterization(
                        df=transformed_table,
                        clustering_features=clustering_features,
                        windows=universal_windows,
                        step_id=i,
                        cluster_id=c,
                        precomputed=True,
                        mean=mean,
                        std=std)
                print(temp_d)
                
                temp_d = temp_d.to_frame('SCORE')
                temp_d['FEATURE'] = temp_d.index
                temp_d['WINDOW_DATE'] = end_window[i]
                temp_d['CLUSTER_ID'] = c
                temp_d = temp_d.reset_index(drop=True)

                # Append to DataFrame
                c_drivers.append(temp_d)
                print("window:{}  ||  cluster:{}".format(str(i), str(c)))

        c_drivers_final = pd.concat(c_drivers)
        c_drivers_final.to_sql("window_cluster_drivers",
                               db,
                               if_exists="replace")

        c_drivers_final['WINDOW_DATE'] = c_drivers_final[
                'WINDOW_DATE'].apply(lambda x: str(x))
        c_drivers_final_pyarrow = pa.Table.from_pandas(c_drivers_final)
        pq.write_table(c_drivers_final_pyarrow,
                       'window_cluster_drivers.parquet')


    # Get window details
    ## Define columns to export
    cols_to_get = ['ID',
                   'CLUSTER_ID',
                   'Score',
                   'Size',
                   'Growth',
                   'Prev Size',
                   'Delta',
                   'Avg Cost Delta',
                   'Curr Size',
                   'Action',
                   'Reason',
                   'New Cluster']

    if export_window_details:
        temp_w_final = pd.DataFrame()


    ## Create temporary web
    temp_w_final = pd.DataFrame()

    ## Loop through windows and add the columns to the new DF
    for d in detail_review_info.keys():
        # get values
        temp_w = detail_review_info[d]
        temp_w = temp_w[temp_w.columns.intersection(cols_to_get)]
        temp_w['WINDOW_DATE'] = d
        temp_w = temp_w.reset_index(drop=True)

        # Append to DataFrame
        temp_w_final = temp_w_final.append(temp_w)
        print("window:{}".format(str(d)))
    else:
        for feature in list(export_df.dtypes[(export_df.dtypes ==
                                              'float64')].index):
            export_df[feature] = export_df[feature].apply(
                    lambda x: round(x, 8))
        export_df['RUN_DATE'] = export_df['RUN_DATE'].apply(
                lambda x: str(x)[:10])
        export_concat_windows['INCDT_SCORE'] = export_concat_windows[
                'INCDT_SCORE'].apply(lambda x: round(x, 8))
        export_concat_windows['FIRST_RECORD_DATE'] = export_concat_windows[
                'FIRST_RECORD_DATE'].apply(lambda x: str(x)[:10])
        export_concat_windows['LAST_RECORD_DATE'] = export_concat_windows[
                'LAST_RECORD_DATE'].apply(lambda x: str(x)[:10])
        for i in list(export_df.dtypes[export_df.dtypes == 'float64'].index):
            export_df[i] = export_df[i].apply(lambda x: round(x, 8))

        export_concat_windows.to_sql('concat_windows',
                                     db, if_exists='replace')
        export_concat_windows_pyarrow = pa.Table.from_pandas(
                export_concat_windows)
        pq.write_table(export_concat_windows_pyarrow, 'concat_windows.parquet')

        export_df.to_sql('concat_clusters', db, if_exists='replace')
        export_df_pyarrow = pa.Table.from_pandas(export_df)
        pq.write_table(export_df_pyarrow, 'concat_clusters.parquet')

    ## Write new DF to output
    temp_w_final.fillna(value=np.nan, inplace=True)
    temp_w_final_pyarrow = pa.Table.from_pandas(temp_w_final)
    pq.write_table(temp_w_final_pyarrow, 'window_details.parquet')


    # Fail codes for each incident
    fl = transformed_table[['ID','REL_CMP_FAIL_CODE_LIST']].set_index(['ID'])['REL_CMP_FAIL_CODE_LIST'].apply(pd.Series).stack()
    fl = fl.reset_index()
    fl = fl.drop(['level_1'], axis=1)
    fl.columns = ['INCDT_ID','FAIL_CODE']
    fl = fl.groupby(['INCDT_ID','FAIL_CODE']).count()
    fl.reset_index(level=0, inplace=True)
    
    # Write data   
    # Fault codes for each incident
    ## Create data 
    ft = transformed_table[['ID','GREEN_WRENCH_FAULT_CODE_LIST']].set_index(['ID'])['GREEN_WRENCH_FAULT_CODE_LIST'].apply(pd.Series).stack()
    ft = ft.reset_index()
    ft = ft.drop(['level_1'], axis=1)
    ft.columns = ['INCDT_ID','FAULT_CODE']
    ft = ft.groupby(['INCDT_ID','FAULT_CODE']).count()
    ft.reset_index(level=0, inplace=True)
    
    ## Write data
    # Aggregations
    fl = transformed_table[['ID', 'REL_CMP_FAIL_CODE_LIST']].\
        set_index(['ID'])['REL_CMP_FAIL_CODE_LIST'].apply(pd.Series).stack()
    fl = fl.reset_index()
    fl = fl.drop(['level_1'], axis=1)
    fl.columns = ['INCDT_ID', 'FAIL_CODE']
    fl = fl.groupby(['INCDT_ID', 'FAIL_CODE']).count()
    fl.reset_index(level=0, inplace=True)
    fl = fl.reset_index()

    # Write data
    fl.to_sql("incident_fail_codes", db, if_exists="replace")
    fl_pyarrow = pa.Table.from_pandas(fl)
    pq.write_table(fl_pyarrow, 'incident_fail_codes.parquet')

    # Fault codes for each incident
    # Create data
    ft = transformed_table[['ID', 'GREEN_WRENCH_FAULT_CODE_LIST']].\
        set_index(['ID'])['GREEN_WRENCH_FAULT_CODE_LIST'].\
        apply(pd.Series).stack()
    ft = ft.reset_index()
    ft = ft.drop(['level_1'], axis=1)
    ft.columns = ['INCDT_ID', 'FAULT_CODE']
    ft = ft.groupby(['INCDT_ID', 'FAULT_CODE']).count()
    ft.reset_index(level=0, inplace=True)
    ft = ft.reset_index()
    ft['FAULT_CODE'] = ft['FAULT_CODE'].apply(lambda x: str(x))

    # Write data
    ft.to_sql("incident_fault_codes", db, if_exists="replace")
    ft_pyarrow = pa.Table.from_pandas(ft)
    pq.write_table(ft_pyarrow, 'incident_fault_codes.parquet')

    # Close database connection
    db.close()

# %%