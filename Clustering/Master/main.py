#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 16 17:12:20 2018

@author: pg875
"""

# %%
# initial setting stuff that does not need to be run all the time
import os
from sys import platform
# print(os.getcwd())
if platform == 'darwin':
    os.chdir("/Users/pg875/Desktop/ds_repo_bis/ds/Clustering/")
elif platform == 'linux':
    os.chdir("/usr/users/quantumdev/ds/Clustering/")

# %% Librairies
import os
import preprocessing_lib as plib
import clustering_lib as clustlib
import exploration_lib as explib
import cluster_cf as cf
import warnings
import imp
import math
import numpy as np
imp.reload(clustlib)
imp.reload(plib)
imp.reload(explib)
imp.reload(cf)
warnings.simplefilter('ignore')

# %% Start up
# Preprocessing
transformed_table, clustering_features, clustering_ranges, data = \
    plib.main_load_data(online_table_location=cf.online_table_location,
                        component_table_location=cf.component_table_location,
                        feature_dict=cf.feature_dict,
                        col_names=cf.col_names,
                        run_offline=cf.run_offline,
                        save_data=cf.save_data,
                        folder=cf.folder,
                        data_start_date=cf.data_start_date,
                        data_end_date=cf.data_end_date)

# %% Load pickle
if cf.run_load_pickle:
    X_transformed = clustlib.load_pickle(cf.path_pickle_load +
                                         "X_transformed.pckl")
    top_issues = clustlib.load_pickle(cf.path_pickle_load +
                                      "top_issues.pckl")
    windows = clustlib.load_pickle(cf.path_pickle_load +
                                   "trim_windows.pckl")
    clustering_features = clustlib.load_pickle(cf.path_pickle_load +
                                               "clustering_features.pckl")
    tracking_path = clustlib.load_pickle(cf.path_pickle_load +
                                         "tracking_path.pckl")
    universal_anchor = clustlib.load_pickle(cf.path_pickle_load +
                                            "universal_anchor.pckl")
    clustering_ranges = clustlib.load_pickle(cf.path_pickle_load +
                                             "clustering_ranges.pckl")
    transformed_results = clustlib.load_pickle(cf.path_pickle_load +
                                               "transformed_results.pckl")
    universal_ids = clustlib.load_pickle(cf.path_pickle_load +
                                         "universal_ids.pckl")
    end_window = clustlib.load_pickle(cf.path_pickle_load +
                                      "end_window.pckl")
    trim_review_info = clustlib.load_pickle(cf.path_pickle_load +
                                            "trim_review_info.pckl")
    universal_labels = clustlib.load_pickle(cf.path_pickle_load +
                                            "universal_labels.pckl")
    optimal_weights = clustlib.load_pickle(cf.path_pickle_load +
                                           "optimal_weights.pckl")
    transformed_table = clustlib.load_pickle(cf.path_pickle_load +
                                             "trim_transformed_table.pckl")
    start_window = clustlib.load_pickle(cf.path_pickle_load +
                                        "start_window.pckl")
    universal_windows = clustlib.load_pickle(cf.path_pickle_load +
                                             "trim_universal_windows.pckl")

# %% Clustering
# Optimization framework
if cf.run_optimal_weights:
    optimal_weights, n_clusters = \
        clustlib.optimization_framework(
                data=data,
                df=transformed_table,
                clustering_ranges=clustering_ranges,
                warm_start=cf.warm_start,
                order=cf.order,
                n_iterations=cf.n_iterations,
                n_clusters_range=cf.n_clusters_range,
                kpi2_weight=cf.kpi2_weight,
                kpi4_weight=cf.kpi4_weight,
                penalization_weight=cf.penalization_weight,
                affinity=cf.affinity,
                do_plot=False)

else:
    optimal_weights = cf.optimal_weights
    clustlib.save(optimal_weights,
                  filename=cf.folder + 'optimal_weights.pckl')

# %% Moving windows framework
# Weight data
weighted_data = clustlib.weights_apply(data=data,
                                       clustering_ranges=clustering_ranges,
                                       optimal_weights=optimal_weights)

# %% Optimal threshold for the moving windows framework
print('calculating optimal threshold')
optimal_threshold = clustlib.threshold_tuning(
    data=weighted_data,
    df=transformed_table,
    clustering_ranges=clustering_ranges,
    weights=optimal_weights,
    algorithm=cf.n_clusters_algorithm,
    size=cf.window_size,
    step=cf.window_step,
    test_range=cf.test_range,
    order_of_magnitude=None,
    do_print=True,
    do_plot=True,
    preweighted=True,
    min_n_clusters=cf.min_n_clusters,
    max_n_clusters=cf.max_n_clusters)

# %% Perform the clustering
print('performing moving window clustering')
clustering_results, start_window, end_window, anchor, tracking_path = \
    clustlib.parallel_moving_windows_framework(
        data=data,
        df=transformed_table,
        clustering_ranges=clustering_ranges,
        weights=optimal_weights,
        size=cf.window_size,
        step=cf.window_step,
        algorithm=cf.algorithm,
        n_clusters_algorithm=cf.n_clusters_algorithm,
        threshold=optimal_threshold,
        reset=True,
        do_plot=False,
        n_processes=cf.n_processes)

# %% Exploration
# Windows dictionary (optional: convert time complexity to space memory)
print('creating windows')
windows = explib.map_windows(df=transformed_table,
                             start_window=start_window,
                             end_window=end_window,
                             clustering_results=clustering_results,
                             path_range=tracking_path)

# %% Plot KPIs
if cf.run_plot_kpis:
    explib.plot_kpis(transformed_table=transformed_table,
                     start_window=start_window,
                     end_window=end_window,
                     clustering_results=clustering_results,
                     tracking_path=tracking_path,
                     windows=windows)

# %% Universal tracking
# Perform universal clustering
print('creating universal cluster IDs')

last_id_given = clustlib.Singleton()
last_id_given.index = 0

anchor_std = math.sqrt(np.mean(np.array(
                [np.linalg.norm(np.var(list(anchor[i].values()), axis=0))
                 for i in anchor.keys()])))

centroids_threshold = optimal_threshold + anchor_std

universal_ids, universal_labels, universal_anchor = \
    clustlib.parallel_universal_ids(
        windows=windows,
        anchor=anchor,
        clustering_results=clustering_results,
        tracking_path=tracking_path,
        last_id_given=last_id_given,
        tracking_type='hybrid',
        centroids_threshold=centroids_threshold,
        optimal_threshold=optimal_threshold,
        count_threshold=0.5,
        n_threads=6,
        universal_anchor=None,
        universal_labels=None,
        universal_ids=None,
        use_tqdm=True)

# %% Get new clustering_results
transformed_results = clustlib.transform_clustering_results(
    clustering_results=clustering_results,
    new_labels=universal_ids,
    tracking_path=tracking_path)

# %% Get new windows
print('creating universal windows')
universal_windows = explib.map_windows(df=transformed_table,
                                       start_window=start_window,
                                       end_window=end_window,
                                       clustering_results=transformed_results,
                                       path_range=tracking_path,
                                       universal_windows=True)

# %% Reviews
# Intermediate computation
if cf.run_cumulative_counts:
    print('calculating cumulative counts')
    universal_windows, cumulative_ids = explib.cumulative_counter(
        universal_windows,
        universal_labels,
        tracking_path)

# %% Reviewing strategy
print('performing reviewing strategy')
universal_windows_info, visited_ids, visited_clusters, \
    universal_mean, universal_std = \
    clustlib.main_flagging(transformed_table=transformed_table,
                           universal_anchor=universal_anchor,
                           universal_labels=universal_labels,
                           clustering_features=clustering_features,
                           clustering_ranges=clustering_ranges,
                           optimal_weights=optimal_weights,
                           universal_windows=universal_windows,
                           tracking_path=tracking_path,
                           threshold=cf.flagging_threshold,
                           end_window=end_window,
                           columns_list=cf.col_names,
                           col_list_flag=cf.col_list_flag,
                           deviation=cf.multivariate_deviation)

# %% 2D T-SNE demo
if cf.run_plot_tsne:
    X_transformed, transformed_table = explib.main_tsne(
            transformed_table,
            transformed_results,
            clustering_features,
            clustering_ranges,
            optimal_weights,
            data_only=False,
            tracking_path=tracking_path,
            universal_windows=universal_windows,
            end_window=end_window,
            detail_review_info=universal_windows_info)

elif cf.run_tsne_data:
    X_transformed, transformed_table = explib.main_tsne(
            transformed_table=transformed_table,
            transformed_results=transformed_results,
            clustering_features=clustering_features,
            clustering_ranges=clustering_ranges,
            optimal_weights=optimal_weights,
            data_only=True)

# %% Creating export dataframe
export_concat_windows, export_df = clustlib.build_export_table(
        universal_windows,
        end_window,
        transformed_table,
        universal_windows_info,
        incdt_threshold=.25,
        folder_path=cf.path_esn_table,
        start_date=None,
        end_date=None,
        time_step=None,
        do_export=cf.export_concat_tables)

# %% Export results
explib.export_results(cf.path_db_export,
                      transformed_table,
                      universal_windows_info,
                      cf.col_names,
                      export_df,
                      export_concat_windows,
                      export_tsne=True,
                      X_transformed=X_transformed,
                      export_window_drivers=False,
                      clustering_features=clustering_features,
                      universal_windows=universal_windows,
                      tracking_path=tracking_path,
                      export_window_details=cf.export_window_details,
                      export_flft=cf.export_flft)
# %%
