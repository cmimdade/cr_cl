#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 08:41:29 2018

@author: pg875
"""
# %% Librairies
import copy
import numpy as np
import pandas as pd
import cluster_cf as cf
import random
import datetime
import calendar
import time
import pickle
import sys
from tqdm import tqdm
from scipy import spatial
from scipy.stats import norm
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import Birch
import matplotlib.pyplot as plt
import math
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
from scipy.spatial.distance import pdist, squareform
from concurrent.futures import ThreadPoolExecutor
import exploration_lib as explib
import preprocessing_lib as plib
from scipy.cluster.hierarchy import linkage
from functools import reduce
import itertools
import pyarrow.parquet as pq
import pyarrow as pa

# %% Clustering functions
# Takes as argument the data matrix with the selected features,
# the clustering ranges (i.e the number of variables for each feature),
# the distance weights and the metrics for numerical and categorical variables
# Returns the computed mixed type distance matrix


def mixed_distance_matrix(data,
                          # Data matrix with the selected features (np.array)
                          clustering_ranges,
                          # clustering ranges (list of integers)
                          weights,
                          # weights of the distance (list of floats), should
                          # have the same length as clustering_ranges
                          metric_num='euclidean',  # metric for numerical type
                          metric_cat='jaccard'):  # metric for jaccard type
    pointer = 0
    n_rows = data.shape[0]
    n_col_numeric = sum(np.array(clustering_ranges) == 1)
    total_distance = [0 for i in range(int(n_rows * (n_rows - 1) / 2))]
    for range_index in range(len(clustering_ranges)):
        if clustering_ranges[range_index] == 1:
            total_distance += (weights[range_index] * pdist(
                data[:, pointer:pointer + 1],
                metric=metric_num)) / n_col_numeric
            pointer += 1
        else:
            total_distance += weights[range_index] * np.nan_to_num(pdist(
                data[:, pointer:pointer + clustering_ranges[range_index]],
                metric=metric_cat))
            pointer += clustering_ranges[range_index]
    distance_matrix = squareform(total_distance)
    return(distance_matrix)

# Takes as argument the data matrix with the selected features,
    # the clustering ranges (i.e the number of variables for each feature),
    # the distance weights, the algorithm type,
    # its affinity and an optional threshold
# Returns the fitted clustering model


def clustering_model(data,  # Data matrix with the selected features (np.array)
                     clustering_ranges,  # clustering ranges (list of integers)
                     weights=None,
                     # weights of the distance (list of floats), should have
                     # the same length as clustering_ranges
                     n_clusters=10,  # number of clusters if fixed
                     algorithm='hierarchical',  # algorithm type (string)
                     # can take the following values: 'hierarchical', 'dbscan',
                     # 'kmeans', 'birch_variable', 'birch_fixed', and
                     # 'minibatch'
                     affinity='default',  # affinity of the clustering model
                     distance_matrix=None,
                     # distance matrix if already computed
                     metric_num='euclidean',
                     # metric for numerical type if affinity = precomputed
                     metric_cat='jaccard',
                     # metric for categorical type if affinity = precomputed
                     threshold=2,
                     # threshold for distance clustering
                     # if 'birch_variable' is selected
                     preweighted=False):  # whether the data is preweighted

    if affinity == 'default':
        if not(preweighted):
            X = copy.copy(data)
            pointer = 0

            for i in range(len(clustering_ranges)):
                X[:,
                  pointer:(pointer + clustering_ranges[i])] = \
                      weights[i] * X[:,
                                     pointer:(pointer + clustering_ranges[i])]
                pointer = pointer + clustering_ranges[i]
        else:
            X = data

        if algorithm == 'hierarchical':
            model = AgglomerativeClustering(
                linkage='average', n_clusters=n_clusters)
        if algorithm == 'dbscan':
            model = DBSCAN(eps=threshold)
        if algorithm == 'kmeans':
            model = KMeans(n_clusters=n_clusters, random_state=42)
        if algorithm == 'birch_variable':
            model = Birch(
                branching_factor=50,
                n_clusters=None,
                threshold=threshold,
                compute_labels=True)
        if algorithm == 'birch_fixed':
            model = Birch(
                branching_factor=50,
                n_clusters=n_clusters,
                threshold=threshold,
                compute_labels=True)
        if algorithm == 'minibatch':
            model = MiniBatchKMeans(n_clusters=n_clusters)
        model.fit(X)
        return(model)

    if affinity == 'precomputed' and algorithm == 'hierarchical':
        if distance_matrix is None:
            distance_matrix = mixed_distance_matrix(
                                    data=data,
                                    clustering_ranges=clustering_ranges,
                                    weights=weights,
                                    metric_num=metric_num,
                                    metric_cat=metric_cat)

        model = AgglomerativeClustering(
            linkage='average',
            affinity='precomputed',
            n_clusters=n_clusters)
        model.fit(distance_matrix)
        return(model)

    else:
        print('Error: affinity not implemented for the given algorithm')


# Takes as argument a dataframe containing both the cluster labels
    # and the issue numbers, the kpi number that must computed
    # and an optional threshold depending on which kpi as been selected
# Returns the calculated kpi for this dataframe
def get_kpi(df,  # data frame, must contain at least these two columns:
            # 'CLUSTER_ID' for the cluster id
            # and 'INCDT_ISSUE_LABEL' for the issue number label
            kpi_number=1,   # kpi number, from 1 to 4 - 0 is penalization
            threshold=5):   # threshold for the kpi - only used
                            # if kpi_number = 2,3 or 4
    if kpi_number == 0:     # penalization function,
                            # namely maximum number of incidents in 1 cluster
        return(max(df.groupby(['CLUSTER_ID']).count()['INCDT_ISSUE_LABEL']) /
               (df.groupby(['CLUSTER_ID']).count()['INCDT_ISSUE_LABEL']).sum())

    if kpi_number == 1:
        # average number of different issues in a cluster
        # except the ones classified as "not_issue"
        avg_num_issues = 0
        for i in np.unique(df['CLUSTER_ID']):
            cur_issues = set(df.query('CLUSTER_ID == @i')['INCDT_ISSUE_LABEL'])
            avg_num_issues = avg_num_issues + len(cur_issues)
            if 'not_issue' in cur_issues:
                avg_num_issues = avg_num_issues - 1

        return(avg_num_issues / len(set(df.CLUSTER_ID)))

    if kpi_number == 2:  # average percentage of the most common issue
        #  within each cluster except the ones classified as "not_issue"
        significant_clusters = df.CLUSTER_ID.value_counts(
        ).reset_index().query('CLUSTER_ID >= @threshold')['index']
        significant_clusters = significant_clusters
        return(df.query('CLUSTER_ID in @significant_clusters').
               query('INCDT_ISSUE_LABEL != "not_issue"').
               groupby(['CLUSTER_ID', 'INCDT_ISSUE_LABEL']).
               size().reset_index().groupby(['CLUSTER_ID'])[0].
               apply(lambda x: max(x) / sum(x)).mean())

    if kpi_number == 3:
        # average number of clusters containing the same issue
        # except the ones classified as "not_issue"
        avg_num_clusters = 0
        top_issues = df.INCDT_ISSUE_LABEL.value_counts().reset_index().query(
            'INCDT_ISSUE_LABEL >= @threshold')['index']
        top_issues = set(top_issues) - {'not_issue'}
        for cur_issue in top_issues:
            cur_clusters = set(
                df.query('INCDT_ISSUE_LABEL == @cur_issue').CLUSTER_ID)
            avg_num_clusters = avg_num_clusters + len(cur_clusters)
        if len(top_issues) > 0:
            avg_num_clusters = avg_num_clusters / len(top_issues)
        else:
            avg_num_clusters = 0
        return(avg_num_clusters)

    if kpi_number == 4:
        # average percentage of a each issue appearing in
        # its most common cluster except the ones classified as "not_issue"
        top_issues = df.INCDT_ISSUE_LABEL.value_counts().reset_index().query(
            'INCDT_ISSUE_LABEL >= @threshold')['index']
        top_issues = set(top_issues) - {'not_issue'}
        return(df.query('INCDT_ISSUE_LABEL in @top_issues').
               groupby(['CLUSTER_ID', 'INCDT_ISSUE_LABEL']).
               size().reset_index().groupby(['INCDT_ISSUE_LABEL'])[0].
               apply(lambda x: max(x) / sum(x)).mean())

# Takes as argument 7 lists: the weights, the kpis lists,
        # the penalization list, and the list for the objective function,
        # and 2 booleans: whether or not objective function
        # and optimal point is given or not
# Doesn't return anything - plots the graph of the Weights vs. the
# different KPIs


def plot_kpis(grid,  # tested weights
              kpi1_list,  # kpi1 list for each of these weights
              kpi2_list,  # kpi2 list for each of these weights
              kpi3_list,  # kpi3 list for each of these weights
              kpi4_list,  # kpi4 list for each of these weights
              penalization_list,
              # penalization values list for each of these weights
              objective_function=None,
              # objective function values for each of these weights
              optimal=None,  # optimal point for the given objective function
              plot_objective=True,
              # whether or not the objective function is given
              plot_optimal=True,  # whether or not the optimal point is given
              xlabel='Weights',
              # couple (a,b) where a is the optimal weight and b the optimal
              # value
              date_type_grid=False):  # whether the grid is date type

    ax1 = host_subplot(111, axes_class=AA.Axes)
    ax2 = ax1.twinx()
    ax3 = ax1.twinx()
    offset = 60
    new_fixed_axis = ax2.get_grid_helper().new_fixed_axis
    if plot_objective:
        ax3.axis["right"] = new_fixed_axis(loc="right", axes=ax3,
                                           offset=(offset, 0))
    ax2.axis["right"] = new_fixed_axis(loc="right", axes=ax2)
    if date_type_grid:
        lns1 = ax1.plot_date(
            grid,
            kpi1_list,
            label="KPI1",
            color='r',
            lw=1.5,
            ls='solid',
            fillstyle='none',
            markersize=0)
        lns2 = ax2.plot_date(
            grid,
            kpi2_list,
            label="KPI2",
            color='b',
            lw=1.5,
            ls='solid',
            fillstyle='none',
            markersize=0)
        lns3 = ax1.plot_date(
            grid,
            kpi3_list,
            label="KPI3",
            color='m',
            lw=1.5,
            ls='solid',
            fillstyle='none',
            markersize=0)
        lns4 = ax2.plot_date(
            grid,
            kpi4_list,
            label="KPI4",
            color='c',
            lw=1.5,
            ls='solid',
            fillstyle='none',
            markersize=0)
        lns5 = ax2.plot_date(
            grid,
            penalization_list,
            label="Penalization",
            color='y',
            lw=1.5,
            ls='solid',
            fillstyle='none',
            markersize=0)
    else:
        lns1 = ax1.plot(grid, kpi1_list, label="KPI1", color='r')
        lns2 = ax2.plot(grid, kpi2_list, label="KPI2", color='b')
        lns3 = ax1.plot(grid, kpi3_list, label="KPI3", color='m')
        lns4 = ax2.plot(grid, kpi4_list, label="KPI4", color='c')
        lns5 = ax2.plot(
            grid,
            penalization_list,
            label="Penalization",
            color='y')
    if plot_objective:
        if date_type_grid:
            lns6 = ax3.plot_date(
                grid,
                objective_function,
                label="Objective Function",
                color='g')
        else:
            lns6 = ax3.plot(
                grid,
                objective_function,
                label="Objective Function",
                color='g')
        lns = lns1 + lns2 + lns3 + lns4 + lns5 + lns6
    else:
        lns = lns1 + lns2 + lns3 + lns4 + lns5
    labs = [l.get_label() for l in lns]
    ax1.legend(lns, labs, loc=0)
    ax1.set_ylabel('Average [KPI1 and KPI3]', color='r')
    ax2.set_ylabel('Percentage (%) [KPI2, KPI4 and Penalization]', color='b')
    if plot_objective:
        ax3.set_ylabel('Aggregate [Objective Function] ', color='g')
        plt.plot(optimal[0], optimal[1], 'g*', markersize=20)
    plt.xlabel(xlabel)
    plt.title(xlabel + ' vs. the different KPIs')
    plt.show()

# %% Optimization Framework
# Takes as arguments the matrix of the features for each incident,
    # the dataframe, the clustering ranges, the weights,
    # the index of the weight to be updated
    # the weight range to be explored, the kpis weights in the objective,
    # the kpis threshold to be used,
    # the algortihm to optimize over and its corresponding thresholds
    # and affinities (if needed), and if the functions needs to
    # print the statement and plot the kpis graphs
# Returns a couple (a,b) where a is the updated weights and b the updated
# number of clusters


def update_weight(data,  # np.array of the clustering features
                  df,  # dataframe containing at least the issue number label
                  clustering_ranges,  # size of the features variables (list)
                  weights,
                  # weights of the features (same size as clustering_ranges)
                  weight_index,
                  # weight to be udpated - -1 is the number of clusters
                  weight_range,  # weights to be tested for grid exploration
                  kpi1_weight=0,  # kpi1 weight in the objective function
                  kpi2_weight=-1,  # kpi2 weight in the objective function
                  kpi3_weight=0,  # kpi3 weight in the objective function
                  kpi4_weight=-1,
                  # kpi4 weight in the objective function
                  penalization_weight=1,
                  # penalization weight in the objective function
                  kpi2_threshold=0,  # kpi2 threshold
                  kpi3_threshold=0,  # kpi3 threshold
                  kpi4_threshold=0,  # kpi4 threshold
                  algorithm='hierarchical',
                  # algorithm to be used: 'hierarchical', 'dbscan', 'kmeans',
                  # 'birch_variable', 'birch_fixed', or 'minibatch'
                  algorithm_threshold=2,  # threshold if birch_variable is used
                  affinity='default',  # affinity of the clustering model
                  distance_matrix=None,  # distance matrix if already computed
                  metric_num='euclidean',
                  # metric for numerical type if affinity = precomputed
                  metric_cat='jaccard',
                  # metric for categorical type if affinity = precomputed
                  do_print=True,  # boolean if steps statements are printed
                  do_plot=True):  # boolean if plots are printed
    clustering_result = df.loc[:, ['INCDT_ISSUE_LABEL']]
    grid = []
    kpi1_list = []
    kpi2_list = []
    kpi3_list = []
    kpi4_list = []
    penalization_list = []
    for test in tqdm(weight_range):
        grid.append(test)
        # if do_print:
        #     print('current testing weight: ' + str(test))
        if weight_index == -1:
            clustering_result['CLUSTER_ID'] = clustering_model(
                data=data,
                clustering_ranges=clustering_ranges,
                weights=weights[0],
                n_clusters=test,
                algorithm=algorithm,
                affinity=affinity,
                distance_matrix=distance_matrix,
                metric_num=metric_num,
                metric_cat=metric_cat,
                threshold=algorithm_threshold).labels_
        else:
            update, n_clusters = weights
            update[weight_index] = test
            clustering_result['CLUSTER_ID'] = clustering_model(
                data=data,
                clustering_ranges=clustering_ranges,
                weights=update,
                n_clusters=n_clusters,
                algorithm=algorithm,
                affinity=affinity,
                distance_matrix=distance_matrix,
                metric_num=metric_num,
                metric_cat=metric_cat,
                threshold=algorithm_threshold).labels_

        if kpi1_weight != 0 or do_plot:
            kpi1_list.append(get_kpi(df=clustering_result, kpi_number=1))
        else:
            kpi1_list.append(0)
        if kpi2_weight != 0 or do_plot:
            kpi2_list.append(
                get_kpi(
                    df=clustering_result,
                    kpi_number=2,
                    threshold=kpi2_threshold))
        else:
            kpi2_list.append(0)
        if kpi3_weight != 0 or do_plot:
            kpi3_list.append(
                get_kpi(
                    df=clustering_result,
                    kpi_number=3,
                    threshold=kpi3_threshold))
        else:
            kpi3_list.append(0)
        if kpi4_weight != 0 or do_plot:
            kpi4_list.append(
                get_kpi(
                    df=clustering_result,
                    kpi_number=4,
                    threshold=kpi4_threshold))
        else:
            kpi4_list.append(0)
        if penalization_weight != 0 or do_plot:
            penalization_list.append(
                get_kpi(
                    df=clustering_result,
                    kpi_number=0))
        else:
            penalization_list.append(0)

    objective_function = list(
        kpi1_weight *
        np.array(kpi1_list) +
        kpi2_weight *
        np.array(kpi2_list) +
        kpi3_weight *
        np.array(kpi3_list) +
        kpi4_weight *
        np.array(kpi4_list) +
        penalization_weight *
        np.array(penalization_list))

    min_objective = min(objective_function)
    optimal_weight = grid[objective_function.index(min_objective)]

    if do_plot:
        plot_kpis(grid=grid,
                  kpi1_list=kpi1_list,
                  kpi2_list=kpi2_list,
                  kpi3_list=kpi3_list,
                  kpi4_list=kpi4_list,
                  penalization_list=penalization_list,
                  objective_function=objective_function,
                  optimal=(optimal_weight, min_objective))

    if weight_index == -1:
        return((weights[0], optimal_weight))
    else:
        update, n_clusters = weights
        update[weight_index] = optimal_weight
        return((update, n_clusters))

# Takes as arguments the matrix of the features for each incident,
    # the dataframe, the clustering ranges, the initial weights,
    # the weight and number of cluster ranges to be explored, the order type,
    # the number of iterations to be done if random,
    # the kpis weights in the objective, the kpis threshold to be used,
    # the algortihm to optimize over
    # and its corresponding threshold (if needed),
    # whether the backtracking history is provided for random updates,
    # the backtracking history if False,
    # and if the functions needs to  print the statement
    # and plot the kpis graphs
# Returns a couple (a,b) where a is the optimal weights and b the optimal
# number of clusters


def optimization_framework(data,  # np.array of the clustering features
                           df,  # dataframe conast the issue number label
                           clustering_ranges,
                           # size of the features vartaining at leiables (list)
                           warm_start,
                           # initial parameters of the features (a,b) where a
                           # is a list of the same size as clustering_ranges
                           # and b a integer
                           # weights to be tested for grid exploration
                           weight_range=[s * 0.25 for s in range(0, 21)],
                           # number of clusters to be tested for grid
                           # exploration
                           n_clusters_range=[s for s in range(10, 105, 5)],
                           order='sequential',
                           # order of optimization: 'sequential' or 'random'
                           n_iterations=10,
                           # number of iterations if order is random
                           kpi1_weight=0,
                           # kpi1 weight in the objective function
                           kpi2_weight=-1,
                           # kpi2 weight in the objective function
                           kpi3_weight=0,
                           # kpi3 weight in the objective function
                           kpi4_weight=-1,
                           # kpi4 weight in the objective function
                           penalization_weight=1,
                           # penalization weight in the objective function
                           kpi2_threshold=5,  # kpi2 threshold
                           kpi3_threshold=0,  # kpi3 threshold
                           kpi4_threshold=5,  # kpi4 threshold
                           algorithm='hierarchical',
                           # algorithm to be used: 'hierarchical', 'dbscan',
                           # 'kmeans', 'birch_variable', 'birch_fixed', or
                           # 'minibatch'
                           affinity='default',
                           # affinity of the clustering model
                           metric_num='euclidean',
                           # metric for num type if affinity = precomputed
                           metric_cat='jaccard',
                           # metric for cat type if affinity = precomputed
                           algorithm_threshold=2,
                           # threshold if birch_variable is used
                           use_backtracking_history=False,
                           # whether or not backtracking_history is provided
                           # for random updates
                           backtracking_history=None,
                           # backtracking history for random updates
                           do_print=True,
                           # boolean if steps statements are printed
                           do_plot=True):  # boolean if plots are printed

    optimal_parameters = warm_start
    # sequential optimization
    if order == 'sequential':
        sequence = [-1]
        sequence.extend([x for x in range(len(warm_start[0]))])
        sequence.append(-1)
        for i in sequence:
            if i != -1 or algorithm != 'dbscan':
                print('Feature ' + str(i) + ' is selected for update')
                optimal_parameters = update_weight(
                    data=data,
                    df=df,
                    clustering_ranges=clustering_ranges,
                    weights=optimal_parameters,
                    weight_index=i,
                    weight_range=(
                        n_clusters_range if i == -
                        1 else weight_range),
                    kpi1_weight=kpi1_weight,
                    kpi2_weight=kpi2_weight,
                    kpi3_weight=kpi3_weight,
                    kpi4_weight=kpi4_weight,
                    penalization_weight=penalization_weight,
                    kpi2_threshold=kpi2_threshold,
                    kpi3_threshold=kpi3_threshold,
                    kpi4_threshold=kpi4_threshold,
                    algorithm=algorithm,
                    affinity=affinity,
                    metric_num=metric_num,
                    metric_cat=metric_cat,
                    algorithm_threshold=algorithm_threshold,
                    do_print=do_print,
                    do_plot=do_plot)
                print('Current optimal weights are:')
                print(optimal_parameters[0])
                print('Current optimal number of clusters is:')
                print(optimal_parameters[1])

    # random optimization
    if order == 'random':
        weights_mapping = [str(x) for x in range(len(warm_start[0]) + 1)]
        optimal_weights, n_clusters = optimal_parameters
        parameter_records = []
        parameter_records.extend(optimal_weights)
        parameter_records.extend([n_clusters])
        # backtracking
        if not use_backtracking_history:
            backtracking_history = pd.DataFrame([[0] * len(weights_mapping)],
                                                columns=weights_mapping,
                                                index=[0])
            backtracking_history.index = [
                '_'.join(["{0:.2f}".format(tmp) for tmp in parameter_records])]
        iterations_done = 0
        while iterations_done < n_iterations:
            parameter_records = []
            parameter_records.extend(optimal_weights)
            parameter_records.extend([n_clusters])
            current_idx = '_'.join(["{0:.2f}".format(tmp)
                                    for tmp in parameter_records])
            if current_idx not in backtracking_history.index:
                i = (i if 0 <= i else len(warm_start[0]))
                backtracking_history.loc[current_idx, :] = 0
                backtracking_history.loc[current_idx, weights_mapping[i]] = 1

            if len(
                    set(np.where(backtracking_history.
                                 loc[current_idx, :] == 0)[0])) == 0:
                print('Early stop: optimal found')
                return(optimal_parameters)
            else:
                i = random.sample(
                    set(np.where(backtracking_history.
                                 loc[current_idx, :] == 0)[0]), 1)[0]
                backtracking_history.loc[current_idx, weights_mapping[i]] = 1
            print('Feature ' + str(i) + ' is selected for update')

            if i != len(warm_start[0]) or algorithm != 'dbscan':
                i = (i if i < len(warm_start[0]) else -1)
                optimal_parameters = update_weight(
                    data=data,
                    df=df,
                    clustering_ranges=clustering_ranges,
                    weights=optimal_parameters,
                    weight_index=i,
                    weight_range=(
                        n_clusters_range if i == -
                        1 else weight_range),
                    kpi1_weight=kpi1_weight,
                    kpi2_weight=kpi2_weight,
                    kpi3_weight=kpi3_weight,
                    kpi4_weight=kpi4_weight,
                    penalization_weight=penalization_weight,
                    kpi2_threshold=kpi2_threshold,
                    kpi3_threshold=kpi3_threshold,
                    kpi4_threshold=kpi4_threshold,
                    algorithm=algorithm,
                    affinity=affinity,
                    metric_num=metric_num,
                    metric_cat=metric_cat,
                    algorithm_threshold=algorithm_threshold,
                    do_print=do_print,
                    do_plot=do_plot)
                print('Current optimal weights are:')
                print(optimal_parameters[0])
                print('Current optimal number of clusters is:')
                print(optimal_parameters[1])
                iterations_done = iterations_done + 1
        # robust optimization
    if order == 'robust':
        optimal_parameters_sequential = optimization_framework(
            data=data,
            df=df,
            clustering_ranges=clustering_ranges,
            warm_start=warm_start,
            weight_range=weight_range,
            n_clusters_range=n_clusters_range,
            order='sequential',
            n_iterations=n_iterations,
            kpi1_weight=kpi1_weight,
            kpi2_weight=kpi2_weight,
            kpi3_weight=kpi3_weight,
            kpi4_weight=kpi4_weight,
            penalization_weight=penalization_weight,
            kpi2_threshold=kpi2_threshold,
            kpi3_threshold=kpi3_threshold,
            kpi4_threshold=kpi4_threshold,
            algorithm=algorithm,
            affinity=affinity,
            metric_num=metric_num,
            metric_cat=metric_cat,
            algorithm_threshold=algorithm_threshold,
            use_backtracking_history=use_backtracking_history,
            backtracking_history=backtracking_history,
            do_print=do_print,
            do_plot=do_plot)

        print('Sequential framework done')
        optimal_parameters_random = optimization_framework(
            data=data,
            df=df,
            clustering_ranges=clustering_ranges,
            warm_start=optimal_parameters_sequential,
            weight_range=weight_range,
            n_clusters_range=n_clusters_range,
            order='random',
            n_iterations=n_iterations,
            kpi1_weight=kpi1_weight,
            kpi2_weight=kpi2_weight,
            kpi3_weight=kpi3_weight,
            kpi4_weight=kpi4_weight,
            penalization_weight=penalization_weight,
            kpi2_threshold=kpi2_threshold,
            kpi3_threshold=kpi3_threshold,
            kpi4_threshold=kpi4_threshold,
            algorithm=algorithm,
            affinity=affinity,
            metric_num=metric_num,
            metric_cat=metric_cat,
            algorithm_threshold=algorithm_threshold,
            use_backtracking_history=use_backtracking_history,
            backtracking_history=backtracking_history,
            do_print=do_print,
            do_plot=do_plot)
        print('Random framework done')
        optimal_parameters = (
            [
                (x + y) / 2 for x,
                y in zip(
                    optimal_parameters_sequential[0],
                    optimal_parameters_random[0])],
            (optimal_parameters_sequential[1] +
             optimal_parameters_random[1]) / 2)
    return(optimal_parameters)

# %% Moving windows framework
# Takes as arguments a date (datetime format) and a number of months
# Returns the datetime corresponding to the input date + the number of months


def increment_months(sourcedate, months=1):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    hour = sourcedate.hour
    minute = sourcedate.minute
    day = min(sourcedate.day, calendar.monthrange(year, month)[1])
    return(datetime.datetime(year, month, day, hour, minute))

# Takes as arguments a date (datetime format),
    # a number of months and number of days (optional: hours and minutes)
# Returns the datetime corresponding to the input date + the given time values


def increment_date(sourcedate, months, days, hours=0, minutes=0):
    date = sourcedate + \
        datetime.timedelta(days=days, hours=hours, minutes=minutes)
    year = date.year
    month = date.month
    day = date.day
    hour = date.hour
    minute = date.minute
    return(increment_months(datetime.datetime(year, month,
                                              day, hour, minute), months))

# Takes as argument a data matrix with the features,
    # a dataframe (the online table or a subset of it), the clustering ranges,
    # the weights of the clustering, the window size,
    # the step of the increment, the algorithm for the n_clusters,
    # the list of values to be tested,
    # the order of magnitude for the number of clusters for the largest window,
    # whether or not we start counting by day 1 of the first month appearing
    # in the dataset or not,
    # whether the function should print statements, should plot graphs,
    # and the name of the date column
# returns the optimal threshold for the given parameters


def threshold_tuning(data,  # data matrix with the features
                     df,  # dataframe (the online table or a subset of it)
                     clustering_ranges,  # clustering ranges (list of integers)
                     weights,
                     # the weights of the clustering (list of floats, same
                     # length as clustering_ranges)
                     size,
                     # window size (m,d) where m is the number of months and d
                     # the number of days
                     step,
                     # step size (m,d) where m is the number of months and d
                     # the number of days
                     algorithm='birch_variable',
                     # the algorithm for the n_clusters: 'birch_variable' or
                     # 'dbscan'
                     # the list of values to be tested
                     test_range=[0.25 + (s / 4) for s in range(39)],
                     order_of_magnitude=200,
                     # order of magnitude for the number of clusters for the
                     # largest window
                     reset=True,  # whether or not we start counting by day 1
                     # of the first month appearing in the dataset or not
                     do_print=True,
                     # whether the function should print statements (bool)
                     do_plot=True,
                     # whether the function should plot summary graph (bool)
                     date_col_name='EARLIEST_RECORD_DATE',
                     preweighted=False,  # name of the date column (string)
                     min_n_clusters=100,
                     max_n_clusters=1000):
    min_date = df[date_col_name].min()
    max_date = df[date_col_name].max()
    reset = reset and (size[0] > 0)
    if reset:
        start_window = [increment_date(min_date, 0, 0).replace(day=1)]
    else:
        start_window = [increment_date(min_date, 0, 0)]
    end_window = [increment_date(start_window[-1], size[0], size[1])]
    i = 0
    max_size = -1
    max_size_index = 0
    n_clusters_list = []
    while end_window[-1] < max_date:
        if do_print:
            print('Current window: ' + str(start_window[-1]))
        start = time.time()
        n_incidents = data[df[date_col_name].apply(
            lambda x: start_window[-1] <= x < end_window[-1]), :].shape[0]
        if n_incidents > max_size:
            max_size_index = i
            max_size = n_incidents
        start_window.append(increment_date(start_window[-1], step[0], step[1]))
        end_window.append(increment_date(end_window[-1], step[0], step[1]))
        end = time.time()
        if do_print:
            print('Time to compute: ' + str(end - start))
        i = i + 1
    print('Largest window: ' + str(start_window[max_size_index]))
    window = data[df[date_col_name].apply(
        lambda x:
            start_window[max_size_index] <= x < end_window[max_size_index]), :]

    if order_of_magnitude is None:
        Z = linkage(window, 'average')
        last = Z[:, 2]
        last_rev = last[::-1]
        idxs = np.arange(1, len(last) + 1)

        if do_plot:
            plt.title('Average within cluster distance vs number of clusters')
            plt.xlabel('Number of clusters')
            plt.ylabel('Average within cluster distance')
            plt.plot(idxs, last_rev)
            plt.show()

        acceleration = np.diff(last, 2)  # 2nd derivative of the distances
        acceleration_rev = acceleration[::-1]

        if do_plot:
            plt.title('Acceleration vs number of clusters')
            plt.xlabel('Number of clusters')
            plt.ylabel(
                'Acceleration of reduction in average within cluster distance')
            plt.plot(idxs[:-2] + 1, acceleration_rev)
            plt.show()
#        ipdb.set_trace()
        order_of_magnitude = acceleration_rev[
                min_n_clusters:max_n_clusters].argmax() + 2 + min_n_clusters

    print('Order of magnitude: ' + str(order_of_magnitude))
    for s in test_range:
        n_clusters_list.append(len(set(clustering_model(
                                        data=window,
                                        clustering_ranges=clustering_ranges,
                                        weights=weights,
                                        algorithm=algorithm,
                                        threshold=s,
                                        preweighted=preweighted).labels_)))

    gap = list(abs(np.array(n_clusters_list) - order_of_magnitude))
    if do_plot:
        plt.title('Threshold vs. number of clusters')
        plt.xlabel('Threshold')
        plt.ylabel('Number of clusters')
        plt.plot(test_range, n_clusters_list)
        plt.plot(test_range[gap.index(min(gap))],
                 n_clusters_list[gap.index(min(gap))],
                 'b*',
                 markersize=20)
        plt.show()

    return(test_range[gap.index(min(gap))])

# Takes as arguments a vector of length n, a vector of weights,
    # and a vector of clustering ranges
    # length of the weight vector should be equal
    #  to the length of the clustering ranges vector
    # sum of the clustering ranges should be equal to n
# Returns the weighted array based on the clustering ranges


def weight_vector(array,
                  weights,
                  clustering_ranges):
    pointer = 0
    X = copy.deepcopy(array)
    for i in range(len(clustering_ranges)):
        X[pointer:(pointer + clustering_ranges[i])] = weights[i] * \
            X[pointer:(pointer + clustering_ranges[i])]
        pointer = pointer + clustering_ranges[i]
    return(X)


def weights_apply(data,
                  clustering_ranges,
                  optimal_weights):
    weighted_data = copy.copy(data)
    pointer = 0
    for i in range(len(clustering_ranges)):
        weighted_data[:, pointer:(pointer + clustering_ranges[i])] = \
            optimal_weights[i] * weighted_data[:, pointer:
                                               (pointer +
                                                clustering_ranges[i])]
        pointer = pointer + clustering_ranges[i]
    return(weighted_data)


# Takes as argument a data matrix with the features,
    # a dataframe (the online table or a subset of it), the clustering ranges,
    # the weights of the clustering, the window size,
    # the step of the increment, the algorithm for the final clustering,
    # the algorithm for the n_clusters, the threshold to be used
    # for the latter,
    # whether or not we start counting by day 1 of the first month appearing in
    # the dataset or not,
    # wheter or not the function should compute the centroids (anchor)
    # whether the function should print statements, should plot graphs,
    # and the name of the date column
# returns clustering results (dictionary[step_id][size_id]), the start
# windows list, the end windows list and the anchor


def moving_windows_framework(data,  # data matrix with the features
                             df,
                             # dataframe (the online table or a subset of it)
                             clustering_ranges,
                             # clustering ranges (list of integers)
                             weights,
                             # the weights of the clustering (list of floats,
                             # same length as clustering_ranges)
                             size,
                             # window size (m,d) where m is the number of
                             # months and d the number of days
                             step,
                             # step size (m,d) where m is the number of months
                             # and d the number of days
                             algorithm='hierarchical',
                             # the algorithm for the final clustering
                             n_clusters_algorithm='birch_variable',
                             # the algorithm for the n_clusters:
                             # 'birch_variable' or 'dbscan'
                             threshold=5,
                             # the threshold to use for n_clusters_algorithm
                             reset=True,
                             # whether or not we start counting by day 1 of
                             # the first month appearing in the dataset or not
                             compute_centroids=True,
                             # whether the function should compute the
                             # centroids (anchor)
                             do_print=True,
                             # whether the function should print statements
                             # (bool)
                             do_plot=True,
                             # whether the function should plot summary graph
                             # (bool)
                             date_col_name='EARLIEST_RECORD_DATE',
                             preweighted=False):
    # name of the date column (string)
    clustering_results = {}
    min_date = df[date_col_name].min()
    max_date = df[date_col_name].max()
    reset = reset and (size[0] > 0)
    if reset:
        start_window = [increment_date(min_date, 0, 0).replace(day=1)]
    else:
        start_window = [increment_date(min_date, 0, 0)]
    end_window = [increment_date(start_window[-1], size[0], size[1])]
    anchor = {}
    n_clusters_list = []
    index_list = []
    i = 0
    total_time_spent = 0
    while end_window[-1] < max_date:
        index_list.append(i)
        if do_print:
            print('Current window: ' + str(start_window[-1]))
        start = time.time()
        window = data[df[date_col_name].apply(
            lambda x: start_window[-1] <= x < end_window[-1]), :]
        n_clusters = max(int(np.sqrt(window.shape[0])),
                         len(set(clustering_model(
                                 data=window,
                                 clustering_ranges=clustering_ranges,
                                 weights=weights,
                                 algorithm=n_clusters_algorithm,
                                 threshold=threshold,
                                 preweighted=preweighted).labels_)))
        n_clusters_list.append(n_clusters)
        if window.shape[0] > 1:
            clustering_results[i] = clustering_model(
                data=window,
                clustering_ranges=clustering_ranges,
                weights=weights,
                n_clusters=n_clusters,
                algorithm=algorithm,
                preweighted=preweighted).labels_
        elif window.shape[0] == 1:
            clustering_results[i] = [0]
        else:
            clustering_results[i] = []

        clustering_labels = clustering_results[i]
        if do_print:
            print('Number of clusters in the window: ' + str(n_clusters))
        if compute_centroids:
            anchor[i] = {}
            for cluster_id in set(clustering_labels):
                if window.shape[0] != 1:
                    anchor[i][cluster_id] = window[clustering_labels ==
                                                   cluster_id].mean(axis=0)
                    if not(preweighted):
                        anchor[i][cluster_id] = weight_vector(
                            array=anchor[i][cluster_id], weights=weights,
                            clustering_ranges=clustering_ranges)
                else:
                    anchor[i][cluster_id] = window.mean(axis=0)
                    if not(preweighted):
                        anchor[i][cluster_id] = weight_vector(
                            array=anchor[i][cluster_id], weights=weights,
                            clustering_ranges=clustering_ranges)

            start_window.append(increment_date(
                start_window[-1], step[0], step[1]))
            end_window.append(increment_date(end_window[-1], step[0], step[1]))
        end = time.time()
        total_time_spent = total_time_spent + end - start
        if do_print:
            print('Time to compute :' + str(end - start))
        i = i + 1
    if do_print:
        print('Total time spent : ' + str(total_time_spent))

    if do_plot:
        plt.title('Evolution of number of clusters through time')
        plt.xlabel('Window index')
        plt.ylabel('Number of clusters')
        # plt.plot(index_list, n_clusters_list)
        plt.plot_date(end_window[:-1], n_clusters_list,
                      lw=1.5, ls='solid', fillstyle='none', markersize=0)
        plt.show()
    return((clustering_results, start_window, end_window, anchor))

# %% Functions to perform parallel clustering
# This function is used to random shuffle data to chunks


def chunk(xs, n):
    ys = list(xs)
    random.shuffle(ys)
    ylen = len(ys)
    size = int(ylen / n)
    chunks = [ys[0 + size * i: size * (i + 1)] for i in range(n)]
    leftover = ylen - size * n
    edge = size * n
    for i in range(leftover):
        chunks[i % n].append(ys[edge + i])
    return chunks


# run clustering on single window
def single_window_clustering(window,
                             clustering_ranges,
                             weights,
                             n_clusters_algorithm,
                             algorithm,
                             threshold,
                             pos,
                             preweighted=False):
    # get number of clusters by Birch
    n_clusters = max(int(np.sqrt(window.shape[0])),
                     len(set(clustering_model(
                             data=window,
                             clustering_ranges=clustering_ranges,
                             weights=weights,
                             algorithm=n_clusters_algorithm,
                             threshold=threshold,
                             preweighted=preweighted).labels_)))
    if window.shape[0] > 1:
        cur_clustering_results = clustering_model(
            data=window,
            clustering_ranges=clustering_ranges,
            weights=weights,
            n_clusters=n_clusters,
            algorithm=algorithm,
            preweighted=preweighted).labels_
    elif window.shape[0] == 1:
        cur_clustering_results = [0]
    else:
        cur_clustering_results = []

    cur_anchor = {}
    for cluster_id in set(cur_clustering_results):
        if window.shape[0] != 1:
            cur_anchor[cluster_id] = window[cur_clustering_results ==
                                            cluster_id].mean(axis=0)
            cur_anchor[cluster_id] = weight_vector(
                        array=cur_anchor[cluster_id],
                        weights=weights,
                        clustering_ranges=clustering_ranges)
        else:
            cur_anchor[cluster_id] = window.mean(axis=0)
            cur_anchor[cluster_id] = weight_vector(
                    array=cur_anchor[cluster_id],
                    weights=weights,
                    clustering_ranges=clustering_ranges)
    return pos, cur_clustering_results, cur_anchor, n_clusters


# run clustering on a list of windows within a thread
def multiple_windows_helper(window_list,
                            indice_list,
                            clustering_ranges,
                            weights,
                            n_clusters_algorithm,
                            algorithm,
                            threshold,
                            preweighted=False):
    anchor = {}
    n_clusters_dict = {}
    clustering_results = {}
    for i in indice_list:
        # print('Current loop ' + str(i))
        cur_pos, cur_clustering_results, cur_anchor, n_clusters = \
            single_window_clustering(window=window_list[i],
                                     clustering_ranges=clustering_ranges,
                                     weights=weights,
                                     n_clusters_algorithm=n_clusters_algorithm,
                                     algorithm=algorithm,
                                     threshold=threshold,
                                     pos=i,
                                     preweighted=preweighted)
        anchor[i] = cur_anchor
        n_clusters_dict[i] = n_clusters
        clustering_results[i] = cur_clustering_results

    return clustering_results, anchor, n_clusters_dict


# parallelization version of moving windows framework
def parallel_moving_windows_framework(data,  # data matrix with the features
                                      df,
                                      # dataframe (the online table or a subset
                                      # of it)
                                      clustering_ranges,
                                      # clustering ranges (list of integers)
                                      weights,
                                      # the weights of the clustering (list of
                                      # floats, same length as
                                      # clustering_ranges)
                                      size,
                                      # window size (m,d) where m is the number
                                      # of months and d the number of days
                                      step,
                                      # step size (m,d) where m is the number
                                      # of months and d the number of days
                                      algorithm='hierarchical',
                                      # the algorithm for the final clustering
                                      n_clusters_algorithm='birch_variable',
                                      # the algorithm for the n_clusters:
                                      # 'birch_variable' or 'dbscan'
                                      threshold=5,
                                      # the threshold to use for
                                      # n_clusters_algorithn
                                      reset=True,
                                      # whether or not we start counting by
                                      # day 1 of the first month appearing
                                      # in the dataset or not
                                      # compute_centroids=True,
                                      # whether the function should compute
                                      # the centroids (anchor)
                                      # do_print=True,
                                      # whether the function
                                      # should print statements (bool)
                                      do_plot=True,
                                      # whether the function should
                                      # should plot summary graph (bool)
                                      date_col_name='EARLIEST_RECORD_DATE',
                                      # name of the date column (string)
                                      start_date=None,
                                      n_processes=6,
                                      preweighted=False):

    clustering_results = {}
    min_date = df[date_col_name].min()
    if start_date is not None:
        min_date = start_date
    max_date = df[date_col_name].max()
    reset = reset and (size[0] > 0)
    if reset:
        start_window = [increment_date(min_date, 0, 0).replace(day=1)]
    else:
        start_window = [increment_date(min_date, 0, 0)]
    end_window = [increment_date(start_window[-1], size[0], size[1])]
    anchor = {}
    n_clusters_dict = {}
    # n_clusters_list = []
    index_list = []
    i = 0
    window_list = {}
    temp = int(end_window[-1] >= max_date)
    while temp < 2 or i == 0:
        index_list.append(i)
        window_list[i] = data[df[date_col_name].apply(
            lambda x: start_window[-1] <= x < end_window[-1]), :]
        start_window.append(increment_date(start_window[-1], step[0], step[1]))
        end_window.append(increment_date(end_window[-1], step[0], step[1]))
        i = i + 1
        temp += int(end_window[-1] >= max_date)

    window_indices = [i for i in range(len(window_list))]
    random.seed(17)
    # random shuffle indices into chunks to reduce the correlation between
    # closes indices
    window_indices_splits = chunk(xs=window_indices, n=n_processes)

    # setup a list of processes
    pool = ThreadPoolExecutor(n_processes)
    process_output = {}
    for j in range(n_processes):
        process_output[j] = pool.submit(multiple_windows_helper,
                                        window_list,
                                        window_indices_splits[j],
                                        clustering_ranges,
                                        weights,
                                        n_clusters_algorithm,
                                        algorithm,
                                        threshold,
                                        preweighted)

    # process results
    # print('Start to get results from processes')
    for i in process_output:
        clustering_results.update(process_output[i].result()[0])
        anchor.update(process_output[i].result()[1])
        n_clusters_dict.update(process_output[i].result()[2])
    # print('Result gathered')

    # print(len(n_clusters_dict))
    n_clusters_list = [
        value for (
            key, value) in sorted(
            n_clusters_dict.items())]

    if do_plot:
        plt.title('Evolution of number of clusters through time')
        plt.xlabel('Window index')
        plt.ylabel('Number of clusters')
        # plt.plot(index_list, n_clusters_list)
        plt.plot_date(end_window[:-1], n_clusters_list,
                      lw=1.5, ls='solid', fillstyle='none', markersize=0)
        plt.show()

    tracking_path = (0, len(start_window) - 2)
    return((clustering_results,
            start_window,
            end_window,
            anchor,
            tracking_path))

# %% Takes as arguments a data frame (the online table or a subset of it),
    # the cluster_id that should be tracked,
    # the corresponding step_id, the target step_id where we should
    # look for the next cluster, the start windows lists,
    # the end windows lists, the clustering results (dictionary),
    # the tracking type, the threshold for centroids, the threshold for counts,
    # the anchor, and the date column name
# Returns the tracked cluster in the target step_id if found, -1 otherwise


def track_cluster(df,  # data matrix with the features
                  cluster_id,  # the cluster_id that should be tracked
                  step_id,  # the corresponding step_id
                  target_step_id,  # the target step_id
                                   # where we should look for the next cluster
                  start_window,  # the start windows lists (list of datetimes)
                  end_window,  # the end windows lists (list of datetimes)
                  clustering_results=None,
                  # clustering results dictionary (needed only if tracking_type
                  # = 'count' or 'hybrid')
                  tracking_type='centroids',
                  # tracking type = 'centroids', 'hybrid' or 'count'
                  centroids_threshold=None,
                  # threshold for centroids (only needed if tracking_type =
                  # 'centroids' or 'hybrid')
                  percentage_threshold=0.5,
                  # threshold for count (only needed if tracking_type = 'count'
                  # or 'hybrid')
                  anchor=None,
                  # anchor of centroids (only needed if tracking_type =
                  # 'centroids' or 'hybrid')
                  date_col_name='EARLIEST_RECORD_DATE',  # date column name
                  k=1):
    if tracking_type == 'centroids':
        centroid = anchor[step_id][cluster_id]
        candidates = list(anchor[target_step_id].values())
        if len(candidates) == 1 and k == 1:
            output = (
                np.linalg.norm(
                    np.array(
                        candidates[0]) -
                    np.array(centroid)),
                candidates[0])
        else:
            tree = spatial.KDTree(candidates)
            output = tree.query(centroid, k=k)
            if k == 1:
                if output[0] < centroids_threshold:
                    return(output[1])
                else:
                    return(-1)
            else:
                if output[0][1] < centroids_threshold:
                    return(output[1][1])
                else:
                    return(-1)
    if tracking_type == 'count':
        union = df.loc[df[date_col_name].apply(lambda x: (start_window[step_id]
                                               <= x < end_window[step_id]) or (
            start_window[target_step_id] <= x < end_window[target_step_id])),
                :]
        union['CLUSTER_ID'] = -1
        union.loc[union[date_col_name].apply(lambda x:
                  start_window[target_step_id] <= x <
                                             end_window[target_step_id]),
                  'CLUSTER_ID'] = clustering_results[target_step_id]
        if len(clustering_results[step_id]) > 1:
            intersection = union.loc[union[date_col_name].apply(
                lambda x:
                    start_window[step_id] <= x <
                    end_window[step_id]), :][clustering_results[step_id] ==
                                             cluster_id]
        else:
            intersection = union.loc[union[date_col_name].apply(
                lambda x: start_window[step_id] <= x < end_window[step_id]), :]
        intersection = intersection.loc[intersection[date_col_name].apply(
            lambda x: start_window[target_step_id]
            <= x < end_window[target_step_id]), :]
        if intersection.shape[0] > 0:
            output = intersection.groupby(
                ['CLUSTER_ID'])['CLUSTER_ID'].value_counts()
            if output.max() / output.sum() > percentage_threshold:
                return(output.idxmax()[1])
            else:
                return(-1)
        else:
            return(-1)
    if tracking_type == 'hybrid':
        centroid_output = track_cluster(
            df=df,
            cluster_id=cluster_id,
            step_id=step_id,
            target_step_id=target_step_id,
            start_window=start_window,
            end_window=end_window,
            clustering_results=clustering_results,
            tracking_type='centroids',
            centroids_threshold=centroids_threshold,
            anchor=anchor,
            date_col_name=date_col_name)
        if centroid_output == -1:
            hybrid_output = track_cluster(
                df=df,
                cluster_id=cluster_id,
                step_id=step_id,
                target_step_id=target_step_id,
                start_window=start_window,
                end_window=end_window,
                clustering_results=clustering_results,
                tracking_type='count',
                percentage_threshold=percentage_threshold,
                date_col_name=date_col_name)
        else:
            hybrid_output = centroid_output
        return(hybrid_output)

# Takes as arguments a data frame (the online table or a subset of it),
    # the cluster_id that should be tracked,
    # the corresponding step_id, the window of step_ids that the path should be
    # tracked over, the start windows lists,
    # the end windows lists, the clustering results (dictionary),
    # the tracking type, the threshold for centroids, the threshold for counts,
    # the anchor, whether or not the compared cluster is followed through time
    # or fixed, and the date column name
# Returns the tracked cluster over the given tracking_path (list of ids or
# -1 if not found a particular step)


def track_path(df,  # data matrix with the features
               cluster_id,  # the cluster_id that should be tracked
               step_id,  # the corresponding step_id
               tracking_path,
               # the path where the cluster should be tracked over (a,b) where
               # a <= step_id <= b
               start_window,  # the start windows lists (list of datetimes)
               end_window,  # the end windows lists (list of datetimes)
               clustering_results=None,
               # clustering results dictionary (needed only if tracking_type =
               # 'count' or 'hybrid')
               tracking_type='centroids',
               # tracking type = 'centroids', 'hybrid' or 'count'
               centroids_threshold=None,
               # threshold for centroids (only needed if tracking_type =
               # 'centroids' or 'hybrid')
               percentage_threshold=0.5,
               # threshold for count (only needed if tracking_type = 'count' or
               # 'hybrid')
               anchor=None,
               # anchor of centroids (only needed if tracking_type =
               # 'centroids' or 'hybrid')
               fixed_source=False,
               date_col_name='EARLIEST_RECORD_DATE'):  # date column name
    # Vdalidating Input

    first_step = tracking_path[0]
    last_step = tracking_path[1]
    if step_id < first_step or step_id > last_step:
        sys.exit('Step ID is not in range of tracking_path')
    previous_step = step_id
    current_step = step_id
    current_cluster = cluster_id
    path = []
    for target_step_id in tqdm(list(reversed(range(first_step, step_id)))):
        if current_cluster != -1:
            previous_cluster = current_cluster
            previous_step = current_step
            current_cluster = track_cluster(
                df=df,
                cluster_id=(
                    current_cluster if not(fixed_source) else cluster_id),
                step_id=(
                    current_step if not(fixed_source) else step_id),
                target_step_id=target_step_id,
                start_window=start_window,
                end_window=end_window,
                clustering_results=clustering_results,
                tracking_type=tracking_type,
                centroids_threshold=centroids_threshold,
                percentage_threshold=percentage_threshold,
                anchor=anchor,
                date_col_name=date_col_name)
            path = path + [current_cluster]
            current_step = target_step_id
        else:
            path = path + [current_cluster]
            current_cluster = previous_cluster
            current_step = previous_step

    if len(path) > 0:
        path = list(reversed(path))

    path = path + [cluster_id]

    current_step = step_id
    current_cluster = cluster_id
    for target_step_id in tqdm(range(step_id + 1, last_step + 1)):
        if current_cluster != -1:
            previous_cluster = current_cluster
            previous_step = current_step
            current_cluster = track_cluster(
                df=df,
                cluster_id=(
                    current_cluster if not(fixed_source) else cluster_id),
                step_id=(
                    current_step if not(fixed_source) else step_id),
                target_step_id=target_step_id,
                start_window=start_window,
                end_window=end_window,
                clustering_results=clustering_results,
                tracking_type=tracking_type,
                centroids_threshold=centroids_threshold,
                percentage_threshold=percentage_threshold,
                anchor=anchor,
                date_col_name=date_col_name)
            path = path + [current_cluster]
            current_step = target_step_id
        else:
            path = path + [current_cluster]
            current_cluster = previous_cluster
            current_step = previous_step
    return(path)

# %%Universal Cluster IDs

# Track an incident in the existing universal clusters


def universal_tracking(universal_anchor,
                       universal_labels,
                       windows,
                       anchor,
                       step_id,
                       cluster_id,
                       tracking_type='hybrid',
                       centroids_threshold=0.25,
                       count_threshold=0.5,
                       k=1):
    if tracking_type == 'count':
        current_ids = set(windows[step_id].query(
            'CLUSTER_ID == @cluster_id')['ID'])
        size = len(current_ids)
        target = -1
        list_index = list(universal_labels)
        for i in list_index:
            intersection = len(current_ids.intersection(
                set(universal_labels[i])))
            if intersection / size > count_threshold:
                target = i
                break
        if target == -1:
            return(-1, 0, 0)
        else:
            return(target, intersection / size,
                   np.linalg.norm(np.array(universal_anchor[target]) -
                                  np.array(anchor[step_id][cluster_id])))
    if tracking_type == 'centroids':
        centroid = anchor[step_id][cluster_id]
        candidates = list(universal_anchor.values())
        if len(candidates) == 1 and k == 1:
            target = (
                np.linalg.norm(
                    np.array(
                        candidates[0]) -
                    np.array(centroid)),
                candidates[0])
        else:
            tree = spatial.KDTree(candidates)
            target = tree.query(centroid, k=k)
            if k == 1:
                if target[0] < centroids_threshold:
                    return(target[1], 0, target[0])
                else:
                    return(-1, 0, 0)
            else:
                if target[0][1] < centroids_threshold:
                    return(target[1][1], 0, target[0][1])
                else:
                    return(-1, 0, 0)
    if tracking_type == 'hybrid':
        target, percentage, distance = \
            universal_tracking(
                    universal_anchor=universal_anchor,
                    universal_labels=universal_labels,
                    windows=windows,
                    anchor=anchor,
                    step_id=step_id,
                    cluster_id=cluster_id,
                    tracking_type='count',
                    centroids_threshold=centroids_threshold,
                    count_threshold=count_threshold,
                    k=k)

        if target == -1:
            target, percentage, distance = \
                universal_tracking(
                    universal_anchor=universal_anchor,
                    universal_labels=universal_labels,
                    windows=windows,
                    anchor=anchor,
                    step_id=step_id,
                    cluster_id=cluster_id,
                    tracking_type='centroids',
                    centroids_threshold=centroids_threshold,
                    count_threshold=count_threshold,
                    k=k)

        return(target, percentage, distance)
    if tracking_type == 'reversed_hybrid':
        target, percentage, distance = \
            universal_tracking(
                    universal_anchor=universal_anchor,
                    universal_labels=universal_labels,
                    windows=windows,
                    anchor=anchor,
                    step_id=step_id,
                    cluster_id=cluster_id,
                    tracking_type='centroids',
                    centroids_threshold=centroids_threshold,
                    count_threshold=count_threshold,
                    k=k)

        if target == -1:
            target, percentage, distance = \
                universal_tracking(
                    universal_anchor=universal_anchor,
                    universal_labels=universal_labels,
                    windows=windows,
                    anchor=anchor,
                    step_id=step_id,
                    cluster_id=cluster_id,
                    tracking_type='count',
                    centroids_threshold=centroids_threshold,
                    count_threshold=count_threshold,
                    k=k)

        return(target, percentage, distance)

# Update universal variables when a new incident is assigned to a
# universal cluster


def update_universal_variables(universal_anchor,
                               universal_labels,
                               windows,
                               anchor,
                               step_id,
                               cluster_id,
                               target):
    try:
        current_len = len(universal_labels[target])
    except KeyError:
        target = 0
        current_len = len(universal_labels[target])
    new_ids = set(windows[step_id].query('CLUSTER_ID == @cluster_id')['ID'])
    new_len = len(new_ids)
    universal_labels[target] = list(
        set(universal_labels[target]).union(new_ids))
    universal_anchor[target] = \
        ((current_len) * np.array(universal_anchor[target]) + (
                new_len) * np.array(
                        anchor[step_id][cluster_id])) / (current_len + new_len)
    return((universal_anchor, universal_labels))

# Track an incident in the existing cluster,
    # given a certain history and a backtracking
    # Also returns if multiple clusters in a given window were assigned to the
    # same universal cluster id


def dynamic_universal_tracking(windows,
                               anchor,
                               universal_anchor,
                               universal_labels,
                               universal_ids,
                               assigned,
                               last_id_given,
                               i,
                               j,
                               tracking_path=(0, 457),
                               tracking_type='hybrid',
                               centroids_threshold=0.25,
                               count_threshold=0.5,
                               k=1):
    target, percentage, distance = \
        universal_tracking(
                universal_anchor=universal_anchor,
                universal_labels=universal_labels,
                windows=windows,
                anchor=anchor,
                step_id=i,
                cluster_id=j,
                tracking_type=(tracking_type if k == 1 else 'centroids'),
                centroids_threshold=centroids_threshold,
                count_threshold=count_threshold,
                k=k)
    j_ = j
    k_ = k
    stop = False
    if target == -1:
        universal_labels[last_id_given +
                         1] = list(set(windows[i].
                                   query('CLUSTER_ID == @j')['ID']))
        universal_anchor[last_id_given + 1] = anchor[i][j]
        assigned[i][last_id_given + 1] = (j, 0)
        universal_ids[i][j] = last_id_given + 1
        last_id_given += 1
        stop = True
        k_ = 1
    else:
        if target in assigned[i].keys():
            if distance < assigned[i][target][1]:
                universal_anchor, universal_labels = \
                    update_universal_variables(
                            universal_anchor=universal_anchor,
                            universal_labels=universal_labels,
                            windows=windows,
                            anchor=anchor,
                            step_id=i,
                            cluster_id=j,
                            target=target)
                j_ = assigned[i][target][0]
                assigned[i][target] = (j, distance)
                universal_ids[i][j] = target
                k_ = 2
            else:
                if k == 2:
                    universal_labels[last_id_given +
                                     1] =\
                                     list(set(windows[i].
                                              query('CLUSTER_ID == @j')['ID']))
                    universal_anchor[last_id_given + 1] = anchor[i][j]
                    assigned[i][last_id_given + 1] = (j, 0)
                    universal_ids[i][j] = last_id_given + 1
                    last_id_given += 1
                    stop = True
                else:
                    k_ = 2
        else:
            universal_anchor, universal_labels = update_universal_variables(
                universal_anchor, universal_labels,
                windows, anchor, i, j, target)
            assigned[i][target] = (j, 0)
            universal_ids[i][j] = target
            stop = True

    return(universal_anchor, universal_labels,
           universal_ids, assigned, j_, k_, stop, last_id_given)

# Returns a mapping of the cluster ids per window with the universal
# clusters ids, and the related parameters


def get_universal_ids(windows,
                      anchor,
                      clustering_results,
                      tracking_path=(0, 457),
                      tracking_type='hybrid',
                      centroids_threshold=0.25,
                      count_threshold=0.5,
                      allow_duplicates=False,
                      reassign_duplicates=True):
    if allow_duplicates:
        universal_anchor = {}
        universal_labels = {}
        universal_ids = {}
        universal_ids[tracking_path[0]] = {}
        for i in set(clustering_results[tracking_path[0]]):
            universal_labels[i] = []
            universal_anchor[i] = 0
            universal_ids[tracking_path[0]][i] = i
            universal_anchor, universal_labels = \
                update_universal_variables(
                        universal_anchor,
                        universal_labels,
                        windows, anchor,
                        tracking_path[0],
                        i,
                        i)

        last_id_given = max(clustering_results[tracking_path[0]])
        for i in tqdm(range(tracking_path[0] + 1, tracking_path[1] + 1)):
            universal_ids[i] = {}
            for j in set(clustering_results[i]):
                target, _, _ = universal_tracking(
                        universal_anchor,
                        universal_labels,
                        windows,
                        anchor,
                        i,
                        j,
                        tracking_type=tracking_type,
                        centroids_threshold=centroids_threshold,
                        count_threshold=count_threshold,
                        k=1)
                if target == -1:
                    universal_labels[last_id_given +
                                     1] = list(
                                     set(windows[i].
                                         query('CLUSTER_ID == @j')['ID']))
                    universal_anchor[last_id_given + 1] = anchor[i][j]
                    universal_ids[i][j] = last_id_given + 1
                    last_id_given += 1
                else:
                    universal_anchor, universal_labels = \
                        update_universal_variables(
                                universal_anchor, universal_labels,
                                windows, anchor, i, j, target)
                    universal_ids[i][j] = target
        return(universal_ids, universal_labels, universal_anchor)
    else:
        if reassign_duplicates:
            universal_anchor = {}
            universal_labels = {}
            universal_ids = {}
            assigned = {}
            universal_ids[tracking_path[0]] = {}
            for i in set(clustering_results[tracking_path[0]]):
                universal_labels[i] = []
                universal_anchor[i] = 0
                universal_ids[tracking_path[0]][i] = i
                universal_anchor, universal_labels = \
                    update_universal_variables(
                            universal_anchor,
                            universal_labels,
                            windows, anchor,
                            tracking_path[0], i, i)
            last_id_given = max(clustering_results[tracking_path[0]])
            for i in tqdm(range(tracking_path[0] + 1, tracking_path[1] + 1)):
                universal_ids[i] = {}
                assigned[i] = {}
                for j in set(clustering_results[i]):
                    k_ = 1
                    j_ = j
                    stop = False
                    s = 0
                    while not(stop):
                        s += 1
                        universal_anchor, universal_labels, universal_ids,
                        assigned, j_, k_, stop, last_id_given = \
                            dynamic_universal_tracking(windows,
                                                       anchor,
                                                       universal_anchor,
                                                       universal_labels,
                                                       universal_ids,
                                                       assigned,
                                                       last_id_given,
                                                       i,
                                                       j_,
                                                       tracking_path,
                                                       tracking_type,
                                                       centroids_threshold,
                                                       count_threshold,
                                                       k_)
            return(universal_ids, universal_labels, universal_anchor)
        else:
            universal_anchor = {}
            universal_labels = {}
            universal_ids = {}
            assigned = {}
            universal_ids[tracking_path[0]] = {}
            for i in set(clustering_results[tracking_path[0]]):
                universal_labels[i] = []
                universal_anchor[i] = 0
                universal_ids[tracking_path[0]][i] = i
                universal_anchor, universal_labels = \
                    update_universal_variables(
                        universal_anchor, universal_labels,
                        windows, anchor, tracking_path[0], i, i)
            last_id_given = max(clustering_results[tracking_path[0]])
            for i in tqdm(range(tracking_path[0] + 1, tracking_path[1] + 1)):
                universal_ids[i] = {}
                assigned[i] = {}
                for j in set(clustering_results[i]):
                    target, percentage, distance = \
                        universal_tracking(
                                universal_anchor=universal_anchor,
                                universal_labels=universal_labels,
                                windows=windows,
                                anchor=anchor,
                                step_id=i,
                                cluster_id=j,
                                tracking_type=tracking_type,
                                centroids_threshold=centroids_threshold,
                                count_threshold=count_threshold)
                    if target == -1:
                        universal_anchor[last_id_given + 1] = anchor[i][j]
                        universal_labels[last_id_given +
                                         1] = list(
                            set(windows[i].
                                query('CLUSTER_ID == @j')['ID']))
                        universal_ids[i][j] = last_id_given + 1
                        assigned[i][last_id_given + 1] = (j, 0)
                        last_id_given += 1
                    else:
                        if target in assigned[i].keys():
                            if distance < assigned[i][target][1]:
                                previous_id = assigned[i][target][0]
                                universal_ids[i][previous_id] = \
                                    last_id_given + 1
                                universal_anchor[last_id_given +
                                                 1] = anchor[i][previous_id]
                                universal_labels[last_id_given + 1] = \
                                    list(
                                        set(windows[i].
                                            query('CLUSTER_ID == @previous_id')
                                            ['ID']))
                                assigned[i][last_id_given +
                                            1] = (previous_id, 0)
                                last_id_given += 1

                                universal_ids[i][j] = target
                                assigned[i][target] = (j, distance)

                            else:
                                universal_anchor[last_id_given +
                                                 1] = anchor[i][j]
                                universal_labels[last_id_given + 1] = list(
                                    set(windows[i].
                                        query('CLUSTER_ID == @j')['ID']))
                                universal_ids[i][j] = last_id_given + 1
                                assigned[i][last_id_given + 1] = (j, 0)
                                last_id_given += 1
                        else:
                            universal_ids[i][j] = target
                            assigned[i][target] = (j, distance)
                for j in set(clustering_results[i]):
                    universal_anchor, universal_labels = \
                        update_universal_variables(
                                universal_anchor,
                                universal_labels,
                                windows, anchor,
                                i,
                                j,
                                universal_ids[i][j])
            return(universal_ids, universal_labels, universal_anchor)

# %% Track an incident in the existing universal clusters


def parralel_universal_tracking(universal_anchor,
                                universal_labels,
                                window,
                                anchor,
                                step_id,
                                cluster_id,
                                tracking_type='hybrid',
                                centroids_threshold=0.25,
                                count_threshold=0.5,
                                k=1):
    if tracking_type == 'count':
        current_ids = set(window.query('CLUSTER_ID == @cluster_id')['ID'])
        size = len(current_ids)
        target = -1
        list_index = list(universal_labels)
        for i in list_index:
            intersection = len(current_ids.intersection(
                set(universal_labels[i])))
            if intersection / size > count_threshold:
                target = i
                break
        if target == -1:
            return(-1, 0, 0)
        else:
            return(target, intersection / size,
                   np.linalg.norm(np.array(universal_anchor[target]) -
                                  np.array(anchor)))
    elif tracking_type == 'centroids':
        centroid = anchor
        candidates = list(universal_anchor.values())
        if len(candidates) == 1 and k == 1:
            distance = np.linalg.norm(
                np.array(
                    candidates[0]) -
                np.array(centroid))
            if distance < centroids_threshold:
                return(list(universal_anchor.keys())[0], 0, distance)
            else:
                return(-1, 0, 0)
        else:
            tree = spatial.KDTree(candidates)
            target = tree.query(centroid, k=k)
            if k == 1:
                if target[0] < centroids_threshold:
                    return(target[1], 0, target[0])
                else:
                    return(-1, 0, 0)
            else:
                if target[0][1] < centroids_threshold:
                    return(target[1][1], 0, target[0][1])
                else:
                    return(-1, 0, 0)
    elif tracking_type == 'hybrid':
        target, percentage, distance = parralel_universal_tracking(
                universal_anchor,
                universal_labels,
                window,
                anchor,
                step_id,
                cluster_id,
                'count',
                centroids_threshold,
                count_threshold,
                k)
        if target == -1:
            target, percentage, distance = parralel_universal_tracking(
                    universal_anchor,
                    universal_labels,
                    window,
                    anchor,
                    step_id,
                    cluster_id,
                    'centroids',
                    centroids_threshold,
                    count_threshold,
                    k)
        return(target, percentage, distance)
    elif tracking_type == 'reversed_hybrid':
        target, percentage, distance = parralel_universal_tracking(
                universal_anchor,
                universal_labels,
                window,
                anchor,
                step_id,
                cluster_id,
                'centroids',
                centroids_threshold,
                count_threshold,
                k)
        if target == -1:
            target, percentage, distance = parralel_universal_tracking(
                    universal_anchor,
                    universal_labels,
                    window,
                    anchor,
                    step_id,
                    cluster_id,
                    'count',
                    centroids_threshold,
                    count_threshold,
                    k)

        return(target, percentage, distance)

# Update universal variables when a new incident is assigned to a
# universal cluster


def parralel_update_universal_variables(universal_anchor,
                                        universal_labels,
                                        window,
                                        anchor,
                                        step_id,
                                        cluster_id,
                                        target):
    try:
        current_len = len(universal_labels[target])
    except KeyError:
        target = 0
        current_len = len(universal_labels[target])
    new_ids = set(window.query('CLUSTER_ID == @cluster_id')['ID'])
    new_len = len(new_ids)
    universal_labels[target] = list(
        set(universal_labels[target]).union(new_ids))
    universal_anchor[target] = ((current_len) *
                                np.array(universal_anchor[target]) + (
        new_len) * np.array(anchor)) / (current_len + new_len)
    return((universal_anchor, universal_labels))


# Creates a singleton instance
_Singleton__instance = None

# Defiens a singleton class for parallelized indexors


class Singleton(object):

    __instance = None

    def __new__(cls):
        if cls.__instance is None:
            cls.__instance = object.__new__(cls)
            cls.__instance.index = 0
        return cls.__instance

# Updated the universal variables after iteration of the tracking process
# on cluster j of step i


def perform_update(universal_anchor,
                   universal_labels,
                   window,
                   anchor,
                   i,
                   j,
                   tracking_type,
                   centroids_threshold,
                   count_threshold,
                   last_id_given,
                   universal_ids):
    target, _, _ = parralel_universal_tracking(universal_anchor,
                                               universal_labels,
                                               window,
                                               anchor,
                                               i,
                                               j,
                                               tracking_type,
                                               centroids_threshold,
                                               count_threshold,
                                               1)
    if target == -1:

        fix_id = last_id_given.index + 1
        last_id_given.index += 1
        universal_labels[fix_id] = list(
            set(window.query('CLUSTER_ID == @j')['ID']))
        universal_anchor[fix_id] = anchor
        universal_ids[i][j] = fix_id

    else:
        universal_anchor, universal_labels = \
            parralel_update_universal_variables(
                    universal_anchor,
                    universal_labels,
                    window, anchor,
                    i,
                    j,
                    target)
        universal_ids[i][j] = target
    return(universal_labels, universal_anchor, universal_ids[i], last_id_given)

# Returns a mapping of the cluster ids per window with the universal
    # clusters ids, and the related parameters
    # Computed in parallel


def parallel_universal_ids(windows,
                           anchor,
                           clustering_results,
                           last_id_given,
                           tracking_path,
                           optimal_threshold,
                           tracking_type='hybrid',
                           centroids_threshold=None,
                           count_threshold=0.5,
                           n_threads=6,
                           universal_anchor=None,
                           universal_labels=None,
                           universal_ids=None,
                           use_tqdm=True,
                           save_data=True,
                           folder=cf.folder):

    if centroids_threshold is None:

        anchor_std = math.sqrt(np.mean(np.array(
                [np.linalg.norm(np.var(list(anchor[i].values()), axis=0))
                 for i in anchor.keys()])))

        centroids_threshold = optimal_threshold + anchor_std + 3

        print(centroids_threshold)
    if universal_anchor is None \
        or universal_labels is None \
            or universal_ids is None:

        universal_anchor = {}
        universal_labels = {}
        universal_ids = {}
        universal_ids[tracking_path[0]] = {}
        window = windows[tracking_path[0]].loc[:, ['ID', 'CLUSTER_ID']]
        for i in set(clustering_results[tracking_path[0]]):
            universal_labels[i] = []
            universal_anchor[i] = 0
            universal_ids[tracking_path[0]][i] = i
            universal_anchor, universal_labels = \
                parralel_update_universal_variables(
                        universal_anchor,
                        universal_labels,
                        window,
                        anchor[tracking_path[0]][i],
                        tracking_path[0],
                        i,
                        i)

        last_id_given.index = max(clustering_results[tracking_path[0]])
    else:
        last_id_given.index = max(universal_labels.keys())

    if use_tqdm:
        range_list = tqdm(range(tracking_path[0] + 1,
                                tracking_path[1] + 1))
    else:
        range_list = range(tracking_path[0] + 1, tracking_path[1] + 1)

    pool = ThreadPoolExecutor(n_threads)

    for i in range_list:
        universal_ids[i] = {}
        futures = {}
        window = windows[i].loc[:, ['ID', 'CLUSTER_ID']]
        for j in set(clustering_results[i]):

            futures[j] = pool.submit(perform_update,
                                     universal_anchor,
                                     universal_labels,
                                     window,
                                     anchor[i][j],
                                     i,
                                     j,
                                     tracking_type,
                                     centroids_threshold,
                                     count_threshold,
                                     last_id_given,
                                     universal_ids)

        for t in futures:
            result = futures[t].result()
            universal_labels.update(result[0])
            universal_anchor.update(result[1])
            universal_ids[i].update(result[2])
            last_id_given.index = max(last_id_given.index, result[3].index)

    pool = ThreadPoolExecutor()
    futures = {}
    del pool
    del futures
    return(universal_ids, universal_labels, universal_anchor)

# %% returns the equivalent of clustering_results where cluster ids are
# replaced with new universal cluster ids


def transform_clustering_results(clustering_results,
                                 new_labels,
                                 tracking_path):
    transformed_results = copy.deepcopy(clustering_results)
    for i in tqdm(range(tracking_path[0], tracking_path[1] + 1)):
        for j in range(len(transformed_results[i])):
            transformed_results[i][j] = \
                new_labels[i][transformed_results[i][j]]
    return(transformed_results)

# %% Format Conversion


def table_to_windows(df,
                     tracking_path,
                     end_window):
    windows = {}
    for i in range(tracking_path[0], tracking_path[1] + 1):
        print(i)
        column_name = str(end_window[i])
        windows[i] = df.loc[df[column_name] != -
                            1, ['ID', 'INCDT_ISSUE_LABEL', column_name]]
        windows[i].columns[-1] = 'CLUSTER_ID'
    return(windows)


def windows_to_table(df,
                     tracking_path,
                     start_window,
                     end_window,
                     windows):
    df_ = copy.deepcopy(df)
    for i in tqdm(range(tracking_path[0], tracking_path[1] + 1)):
        df_[str(end_window[i])[:10]] = -1
        df_.loc[(start_window[i] <= df_['EARLIEST_RECORD_DATE']) &
                (df_['EARLIEST_RECORD_DATE']
                < end_window[i]),
                [str(end_window[i])[:10]]] = list(windows[i]['CLUSTER_ID'])
    return(df_)

# Save variables


def save(obj,
         filename='store.pckl'):
    f = open(filename, 'wb')
    pickle.dump(obj, f)
    f.close()

# Load variables


def load(filename='store.pckl'):
    f = open(filename, 'rb')
    obj = pickle.load(f)
    f.close()
    return(obj)

# %%


def get_universal_variations(df,
                             universal_labels,
                             universal_anchor,
                             clustering_features,
                             clustering_ranges,
                             weights,
                             universal_ids_list=None,
                             matrix=False,
                             save_data=True,
                             folder=cf.folder):
    if universal_ids_list is None:
        universal_ids_list = list(universal_labels.keys())

    universal_mean = {}
    universal_std = {}
    for universal_cluster in tqdm(universal_ids_list):
        pointer = 0
        center = universal_anchor[universal_cluster]
        X = np.array(df.query(
            'ID in @universal_labels[@universal_cluster]').
            loc[:, clustering_features])
        for i in range(len(clustering_ranges)):
            X[:, pointer:(pointer + clustering_ranges[i])] = weights[i] * \
                X[:, pointer:(pointer + clustering_ranges[i])]
            pointer = pointer + clustering_ranges[i]
        if not(matrix):
            universal_mean[universal_cluster] = np.apply_along_axis(
                lambda y: np.linalg.norm(y), 1, (X - center)).mean()
            universal_std[universal_cluster] = (np.apply_along_axis(
                lambda y: np.linalg.norm(y), 1, (X - center)).var())**0.5
        else:
            universal_mean[universal_cluster] = center
            universal_std[universal_cluster] = ((X-center).var(axis=0))**0.5

    if save_data:
        print('saving universal variables to pickle')
        save(
            universal_mean,
            filename=folder + 'universal_mean.pckl')
        save(
            universal_mean,
            filename=folder + 'universal_std.pckl')

    return(universal_mean, universal_std)

# Takes as arguments a dataframe, an incident id, a universal cluster id,
    # the dictionary of centroids, the universal labels
    # the clustering features (list of column names),
    # the clustering ranges (list of integers of the length of the features),
    # the weights, and the score type
# Return the computed score for a given incident compared to a given cluster id


def get_score(df,
              incident_id,
              universal_cluster,
              universal_anchor,
              universal_labels,
              clustering_features,
              clustering_ranges,
              weights,
              var='standard',  # can = 'standard', 'uniform' or 'gaussian'
              precomputed=False,
              universal_mean=None,
              universal_std=None,):
    centroid = universal_anchor[universal_cluster]
    t = np.array(
        np.array(df.query('ID == @incident_id').loc[:, clustering_features]))
    if not(precomputed):
        pointer = 0
        X = np.array(df.query(
            'ID in @universal_labels[@universal_cluster]').
            loc[:, clustering_features])
        for i in range(len(clustering_ranges)):
            X[:, pointer:(pointer + clustering_ranges[i])] = weights[i] * \
                X[:, pointer:(pointer + clustering_ranges[i])]
            t[:, pointer:(pointer + clustering_ranges[i])] = weights[i] * \
                t[:, pointer:(pointer + clustering_ranges[i])]
            pointer = pointer + clustering_ranges[i]
    else:
        pointer = 0
        for i in range(len(clustering_ranges)):
            t[:, pointer:(pointer + clustering_ranges[i])] = weights[i] * \
                t[:, pointer:(pointer + clustering_ranges[i])]
            pointer = pointer + clustering_ranges[i]

    if var == 'standard':
        if not(precomputed):
            return((np.linalg.norm(X.var(axis=0))**0.5) / np.linalg.norm(t -
                   np.array(centroid)))
        else:
            return(universal_std[universal_cluster] / np.linalg.norm(t -
                   np.array(centroid)))
    else:
        if var == 'uniform':
            return(1 / np.linalg.norm(t - np.array(centroid)))
        else:
            if not(precomputed):
                d = norm(
                    loc=np.apply_along_axis(
                        lambda y: np.linalg.norm(y),
                        1,
                        (X - centroid)).mean(),
                    scale=np.apply_along_axis(
                        lambda y: np.linalg.norm(y),
                        1,
                        (X - centroid)).var()**0.5)
            else:
                d = norm(
                    loc=universal_mean[universal_cluster],
                    scale=universal_std[universal_cluster])
            output = 1 - d.cdf(np.linalg.norm(t - np.array(centroid)))
            if math.isnan(output):
                return(0)
            else:
                return(output)


def get_specificity(df,
                    incident_id,
                    universal_cluster,
                    clustering_features,
                    clustering_ranges,
                    weights,
                    universal_mean,
                    universal_std,
                    threshold,
                    product=False):
    centroid, deviation = universal_mean[universal_cluster],\
                           universal_std[universal_cluster]

    t = np.array(
        np.array(df.query('ID == @incident_id').loc[:, clustering_features]))

    pointer = 0
    for i in range(len(clustering_ranges)):
        t[:, pointer:(pointer + clustering_ranges[i])] = weights[i] * \
            t[:, pointer:(pointer + clustering_ranges[i])]
        pointer = pointer + clustering_ranges[i]

    t = t[0, :]
    dif = list(t-centroid)
    probability = []
    for i in range(len(deviation)):
        if dif[i] == 0:
            probability.append(2)
        elif deviation[i] == i:
            probability.append(0)
        else:
            d = norm(
                    loc=centroid[i],
                    scale=deviation[i])
            probability.append(1 - d.cdf(t[i]))
    if product:
        score = reduce(lambda x, y: x*y, probability)
    else:
        sublist = list(
                map(lambda x: False if x == 2 else True, probability))
        if len(sublist) > 0:
            score = np.mean(np.array(probability)[sublist])
        else:
            score = 1

    return(score, probability)

# A manual input function flagging a particular combination of variables


def test_monitor_oem(df,
                     universal_anchor,
                     universal_labels,
                     clustering_features,
                     clustering_ranges,
                     weights,
                     universal_windows,
                     tracking_path,
                     threshold,
                     end_window,
                     top_issues,
                     universal_windows_review,
                     step_date,
                     oem='KENWORTH MOTOR TRUCK CO'):
    universal_windows_review[step_date].\
        loc[universal_windows_review[step_date]['REL_OEM_NAME'].
            apply(lambda x: x == oem),
            'Status'] = 'Monitoring'
    return(universal_windows_review)

# Takes as argument a dataframe, the universal anchor (dic of centroids),
    # the universal labels, the clustering features,
    # the clustering ranges, the weights, the universal_windows,
    # the tracking path (=(a,b) where a<b), the flag threshold
    # the end_window dictionary, the top_issues to evaluate over,
    # and the score type
    # Whether the universal deviations are precomputed or not
    # If precomputed=True, universal_mean and universal_std are universal
    # deviations
    # column list to keep in the detail_review_info,
    # whether or not to filter incidents without fail codes,
    # the already visited incidents ids (list), the visited clusters (list),
    # whether to use multivariate gaussian instead of univariate,
    # if deviation=True, the universal deviationss should be matrices,
    # the way to aggregate the probability results (product=True or False),
    # columns to flag based on their individual deviations, and based on the
    # sub_threhsold, the manual input list of functions to apply on the
    # results, and finally whether or not to build and return the build
    # detection date for labeled issues
# Return a description of the reviewing strategy:
    # - Total number of new incidents per window
    # - Total number of incidents flagged for review
    # - IDs of the incidents flagged for review
    # - IDs of the incidents flagged for review
    # and sampled so that we only look at 1 per cluster
    # - Dataframes describing the incidents flagged for review
    # (dictionary of dataframes)
    # - Detection date for each issue (dictionary)
    # - Summary of detection dates for each issue (dataframes)


def describe_reviews(
        df,
        universal_anchor,
        universal_labels,
        clustering_features,
        clustering_ranges,
        weights,
        universal_windows,
        tracking_path,
        threshold,
        end_window,
        top_issues,
        var='gaussian',
        # can = 'standard', 'uniform' or 'gaussian'
        precomputed=False,
        universal_mean=None,
        universal_std=None,
        columns_list=['ID',
                      'ESN',
                      'REL_CMP_FAIL_CODE_LIST',
                      'GREEN_WRENCH_FAULT_CODE_LIST',
                      'REL_OEM_NAME',
                      'GREEN_WRENCH_LIST',
                      'REL_ANALYSIS_RATE_CAT',
                      'REL_CMP_ENGINE_MILES_LIST',
                      'REL_CMP_FAIL_DATE_LIST',
                      'REL_CMP_SUM_NET_AMOUNT',
                      'EARLIEST_INDICATION_DATE',
                      'EARLIEST_RECORD_DATE',
                      'INCDT_ISSUE_LABEL', 'CLUSTER_ID'],
        filter_fail=False,
        flag_fail=False,
        flag_fault=False,
        visited_ids=None,
        visited_clusters=None,
        deviation=False,
        product=False,
        cols_to_flag=['LAST_MILE_RECORDED_IMPUTED_NORMALIZED',
                      'REL_CMP_SUM_NET_AMOUNT_IMPUTED_NORMALIZED'],
        sub_threshold=0.1,
        manual_input=[],
        build_detection_date_df=True):

    reviewed_ids = []
    universal_windows_review_info = {}
    total_new_incidents = []
    total_incidents_to_review = []
    if visited_ids is None or visited_clusters is None:
        visited_ids = set([])
        visited_clusters = set([])
    sampled_ids = {}
    sampled_scores = {}
    issue_detection_date = {}
    issue_detection_incdt_id = {}
    for i in tqdm(range(tracking_path[0], tracking_path[1] + 1)):
        universal_windows_review_info[str(end_window[i])[:10]] = \
            universal_windows[i].loc[:, columns_list]
        new_incidents = universal_windows[i].query('not ID in @visited_ids')
        new_incidents_ids = set(new_incidents['ID'])

        universal_windows_review_info[str(
            end_window[i])[:10]]['Action'] = 'Do_Not_Review'

        universal_windows_review_info[str(end_window[i])[:10]]['Score'] = 1
        universal_windows_review_info[str(
            end_window[i])[:10]]['Reason'] = 'Old_Incident / Checked_Before'
        universal_windows_review_info[str(
            end_window[i])[:10]]['Specificity'] =\
            'Old_Incident / Checked_Before'
        total_new_incidents.append(len(new_incidents_ids))
        review_count = 0
        fail_code_index = [0]
        fault_code_index = [0]
        if flag_fail:
            fail_code_columns = list(map(lambda x: x.find(
                'REL_CMP_FAIL_CODE_LIST_') >= 0, universal_windows[i].columns))
            fail_code_index = [i for i, x in enumerate(fail_code_columns) if x]
        if flag_fault:
            fault_code_columns = list(map(lambda x: x.find(
                'GREEN_WRENCH_FAULT_CODE_LIST_') >= 0,
                universal_windows[i].columns))
            fault_code_index = [
                i for i, x in enumerate(fault_code_columns) if x]

        if manual_input != []:
            universal_windows_review_info[str(
                end_window[i])[:10]]['Status'] = 'Inactive'

        for incident in new_incidents_ids:
            inc_ = universal_windows[i].query('ID == @incident')
            cluster_id = list(inc_['CLUSTER_ID'])[0]
            inc_len = len(list(inc_['REL_CMP_FAIL_CODE_LIST'])[0])

            if not(deviation):
                score = get_score(df,
                                  incident,
                                  cluster_id,
                                  universal_anchor,
                                  universal_labels,
                                  clustering_features,
                                  clustering_ranges,
                                  weights,
                                  var='gaussian',
                                  precomputed=precomputed,
                                  universal_mean=universal_mean,
                                  universal_std=universal_std)

            else:
                score, probability = get_specificity(df,
                                                     incident,
                                                     cluster_id,
                                                     clustering_features,
                                                     clustering_ranges,
                                                     weights,
                                                     universal_mean,
                                                     universal_std,
                                                     threshold,
                                                     product=product)

                specificity = list(
                        np.array(clustering_features)[np.array(
                                probability) < threshold])

            if cluster_id in visited_clusters:
                boolean = False
                if deviation:
                    for feature in cols_to_flag:
                        boolean = boolean \
                            or probability[clustering_features.index(
                                            feature)] < sub_threshold

                if (score < threshold
                    or boolean)\
                        and (not(filter_fail) or inc_len != 0):

                    reviewed_ids.append(incident)
                    review_count += 1
                    belonging_issue = universal_windows_review_info[str(
                            end_window[i])[:10]].\
                        loc[universal_windows_review_info[str(
                                end_window[i])[:10]].ID == incident,
                            'INCDT_ISSUE_LABEL'].values[0]
                    if belonging_issue != 'not_issue':
                        if belonging_issue not in issue_detection_date.keys():
                            issue_detection_date[belonging_issue] = str(
                                    end_window[i])[:10]
                            issue_detection_incdt_id[belonging_issue] = \
                                incident
                    universal_windows_review_info[str(
                            end_window[i])[:10]].\
                        loc[universal_windows_review_info[str(
                                end_window[i])[:10]].ID ==
                            incident, 'Action'] \
                        = 'Flagged_for_Review'
                    if score < threshold:
                        universal_windows_review_info[str(
                                end_window[i])[:10]].\
                            loc[universal_windows_review_info[str(
                                    end_window[i])[:10]].ID ==
                                incident, 'Reason'] \
                            = 'Belongs to Reviewed Cluster but ' +\
                              'Far away from Centroid'
                    else:
                        universal_windows_review_info[str(
                                end_window[i])[:10]].\
                            loc[universal_windows_review_info[str(
                                    end_window[i])[:10]].ID ==
                                incident, 'Reason'] \
                            = 'Belongs to Reviewed Cluster but ' +\
                              'Strong Deviation for one or more of' +\
                              str(cols_to_flag)

                    if deviation:
                        universal_windows_review_info[str(
                                end_window[i])[:10]].\
                            loc[universal_windows_review_info[str(
                                    end_window[i])[:10]].ID == incident,
                                'Specificity'] \
                            = str(specificity)

                    universal_windows_review_info[str(end_window[i])[:10]].\
                        loc[universal_windows_review_info[str(
                                end_window[i])[:10]].ID == incident, 'Score'] \
                        = score
                elif flag_fail or flag_fault:
                    if (not(flag_fail)
                        or (inc_.iloc[:, fail_code_index].sum())[0] == 0) \
                        and (not(flag_fault)
                             or (inc_.iloc[:,
                                           fault_code_index].sum())[0] == 0) \
                            and (not(filter_fail) or inc_len != 0):
                        reviewed_ids.append(incident)
                        review_count += 1
                        belonging_issue = universal_windows_review_info[str(
                                end_window[i])[:10]].\
                            loc[universal_windows_review_info[str(
                                    end_window[i])[:10]].ID == incident,
                                'INCDT_ISSUE_LABEL'].values[0]
                        if belonging_issue != 'not_issue':
                            if belonging_issue not\
                                    in issue_detection_date.keys():
                                issue_detection_date[belonging_issue] = str(
                                        end_window[i])[:10]
                                issue_detection_incdt_id[belonging_issue] = \
                                    incident
                        universal_windows_review_info[str(
                                end_window[i])[:10]].\
                            loc[universal_windows_review_info[str(
                                    end_window[i])[:10]].ID ==
                                incident, 'Action'] \
                            = 'Flagged_for_Review'
                        universal_windows_review_info[str(
                                end_window[i])[:10]].\
                            loc[universal_windows_review_info[str(
                                    end_window[i])[:10]].ID ==
                                incident, 'Reason'] \
                            = 'Unseen Fail Code or Fault Code'
                        universal_windows_review_info[str(
                                end_window[i])[:10]].\
                            loc[universal_windows_review_info[str(
                                    end_window[i])[:10]].ID ==
                                incident, 'Score'] \
                            = score
                else:
                    universal_windows_review_info[str(end_window[i])[:10]].\
                        loc[universal_windows_review_info[str(
                                end_window[i])[:10]].ID ==
                            incident, 'Reason'] \
                        = 'Belongs to Reviewed Cluster and Close to Centroid'

                    universal_windows_review_info[str(
                            end_window[i])[:10]].\
                        loc[universal_windows_review_info[str(
                                end_window[i])[:10]].ID == incident,
                            'Specificity'] \
                        = 'No Significant Deviation from Cluster'

                    universal_windows_review_info[str(end_window[i])[:10]].\
                        loc[universal_windows_review_info[str(
                                end_window[i])[:10]].ID == incident, 'Score'] \
                        = score

            elif (not(filter_fail) or inc_len != 0):
                review_count += 1
                reviewed_ids.append(incident)
                belonging_issue = universal_windows_review_info[str(
                        end_window[i])[:10]].\
                    loc[universal_windows_review_info[str(
                            end_window[i])[:10]].ID == incident,
                        'INCDT_ISSUE_LABEL'].values[0]
                if belonging_issue != 'not_issue':
                    if belonging_issue not in issue_detection_date.keys():
                        issue_detection_date[belonging_issue] = str(
                                end_window[i])[:10]
                        issue_detection_incdt_id[belonging_issue] = incident
                universal_windows_review_info[str(end_window[i])[:10]].\
                    loc[universal_windows_review_info[str(
                            end_window[i])[:10]].ID == incident, 'Action'] \
                    = 'Flagged_for_Review'

                universal_windows_review_info[str(
                        end_window[i])[:10]].\
                    loc[universal_windows_review_info[str(
                                end_window[i])[:10]].ID == incident,
                        'Specificity'] \
                    = 'Belongs to Unreviewed Cluster'

                universal_windows_review_info[str(end_window[i])[:10]].\
                    loc[universal_windows_review_info[str(
                            end_window[i])[:10]].ID == incident, 'Reason'] \
                    = 'Belongs to Unreviewed Cluster'

                universal_windows_review_info[str(end_window[i])[:10]].\
                    loc[universal_windows_review_info[str(
                            end_window[i])[:10]].ID == incident, 'Score'] \
                    = 0
                if cluster_id not in sampled_ids.keys():
                    sampled_ids[cluster_id] = incident
                    sampled_scores[cluster_id] = score
                else:
                    if sampled_scores[cluster_id] < score:
                        sampled_ids[cluster_id] = incident
                        sampled_scores[cluster_id] = score

        total_incidents_to_review.append(review_count)
        visited_ids = visited_ids.union(new_incidents_ids)
        visited_clusters = visited_clusters.union(
            set(new_incidents['CLUSTER_ID']))

        if len(manual_input) > 0:
            # print('Applying statusing functions')
            for func in manual_input:
                universal_windows_review_info = func(
                    df=df,
                    universal_anchor=universal_anchor,
                    universal_labels=universal_labels,
                    clustering_features=clustering_features,
                    clustering_ranges=clustering_ranges,
                    weights=weights,
                    universal_windows=universal_windows,
                    tracking_path=tracking_path,
                    threshold=threshold,
                    end_window=end_window,
                    top_issues=top_issues,
                    universal_windows_review=universal_windows_review_info,
                    step_date=str(
                        end_window[i])[
                        :10])

        universal_windows_review_info[str(end_window[i])[:10]] = \
            universal_windows_review_info[str(
                    end_window[i])[:10]].sort_values(['Score'])

    # build return dataframe
    if build_detection_date_df:
        detection_date_df = pd.DataFrame.from_dict(
            issue_detection_date, 'index').reset_index()
        detection_date_df.columns = [
            'INCDT_ISSUE_LABEL',
            'Reviewing_Detection_Date']
        detection_date_df_tmp = pd.DataFrame.from_dict(
            issue_detection_incdt_id, 'index').reset_index()
        detection_date_df_tmp.columns = [
            'INCDT_ISSUE_LABEL',
            'Detection_Incident_ID']
        detection_date_df = detection_date_df.merge(detection_date_df_tmp)
        detection_date_df = df.\
            query('INCDT_ISSUE_LABEL in @top_issues').\
            sort_values('EARLIEST_INDICATION_DATE').\
            groupby('INCDT_ISSUE_LABEL').first().\
            loc[:, ['EARLIEST_INDICATION_DATE']].reset_index().\
            merge(df.
                  query('INCDT_ISSUE_LABEL in @top_issues').
                  sort_values('EARLIEST_RECORD_DATE').
                  groupby('INCDT_ISSUE_LABEL').first().
                  loc[:, ['EARLIEST_RECORD_DATE', 'ID']].reset_index()).\
            merge(detection_date_df).\
            merge(df.query('INCDT_ISSUE_LABEL in @top_issues').
                  groupby('INCDT_ISSUE_LABEL').count()['ID'].
                  reset_index().rename(columns={'ID': 'Number_of_Incidents'}))
        detection_date_df['Days_Detected_After_First_Incident'] = [
            (datetime.datetime.strptime(
                d_dt,
                '%Y-%m-%d') - e_dt).days for d_dt,
            e_dt in zip(
                detection_date_df.Reviewing_Detection_Date,
                detection_date_df.EARLIEST_INDICATION_DATE)]

    else:
        detection_date_df = pd.DataFrame.from_dict(
            issue_detection_date, 'index').reset_index()

    return((total_new_incidents,
            total_incidents_to_review,
            reviewed_ids,
            list(sampled_ids.values()),
            universal_windows_review_info,
            issue_detection_date,
            detection_date_df,
            visited_ids,
            visited_clusters))

# same function as before, but using flagging the clusters instead of
# incidents, based on their growth rate or size


def describe_clusters(df,
                      universal_labels,
                      universal_windows,
                      tracking_path,
                      threshold,
                      end_window,
                      top_issues,
                      flagsize=False):

    previous_size = {}
    flag_dates = {}
    list_index = list(universal_labels)
    for cluster in list_index:
        previous_size[cluster] = list(
            universal_windows[tracking_path[0]]['CLUSTER_ID']).count(cluster)
        flag_dates[cluster] = []

    total_new_incidents = []
    total_incidents_to_review = []
    visited_ids = set([])
    total_clusters = []
    total_clusters_to_review = []

    universal_windows_review_info = {}
    issue_detection_date = {}
    issue_detection_cluster_id = {}
    summary_windows = {}
    cumulative_size = {}
    reviewed_clusters = []
    clusters_list = list(universal_labels.keys())
    for i in tqdm(range(tracking_path[0], tracking_path[1] + 1)):
        universal_windows_review_info[str(end_window[i])[:10]] = \
            universal_windows[i].loc[:, ['ID', 'ESN', 'REL_CMP_FAIL_CODE_LIST',
                                         'GREEN_WRENCH_FAULT_CODE_LIST',
                                         'REL_OEM_NAME',
                                         'GREEN_WRENCH_LIST',
                                         'REL_ANALYSIS_RATE_CAT',
                                         'REL_CMP_ENGINE_MILES_LIST',
                                         'REL_CMP_FAIL_DATE_LIST',
                                         'REL_CMP_SUM_NET_AMOUNT',
                                         'EARLIEST_INDICATION_DATE',
                                         'EARLIEST_RECORD_DATE',
                                         'INCDT_ISSUE_LABEL', 'CLUSTER_ID']]
        universal_windows_review_info[str(
            end_window[i])[:10]]['Action'] = 'Do_Not_Review'
        universal_windows_review_info[str(end_window[i])[:10]]['Score'] = 0
        universal_windows_review_info[str(end_window[i])[:10]]['Reason'] = (
            'Stable Cluster' if not(flagsize) else 'Small Cluster')

        cumulative_size[i] = {}
        current_list = set(universal_windows[i]['CLUSTER_ID'])
        for cluster in clusters_list:
            if cluster in current_list:
                if not(flagsize):
                    current_size = list(
                        universal_windows[i]['CLUSTER_ID']).count(cluster)
                    if previous_size[cluster] == 0 or i == tracking_path[0]:
                        score = float("inf")
                    else:
                        score = current_size / previous_size[cluster] - 1
                    previous_size[cluster] = current_size

                    if score > threshold:
                        flag_dates[cluster].append(str(end_window[i])[:10])
                        belonging_issues = set(
                                universal_windows_review_info[str(
                                        end_window[i])[:10]].loc[
                                    universal_windows_review_info[str(
                                            end_window[i])[:10]].CLUSTER_ID ==
                                            cluster,
                                            'INCDT_ISSUE_LABEL'].values)
                        for issue in belonging_issues:
                            if issue not in issue_detection_date.keys():
                                issue_detection_date[issue] = str(
                                    end_window[i])[:10]
                                issue_detection_cluster_id[issue] = cluster
                        universal_windows_review_info[str(
                                end_window[i])[:10]].loc[
                                universal_windows_review_info[str(
                                        end_window[i])[:10]].CLUSTER_ID ==
                                cluster,
                                'Action'] = 'Flagged_for_Review'
                        if score == float('inf'):
                            universal_windows_review_info[str(
                                    end_window[i])[:10]].\
                                    loc[universal_windows_review_info[str(
                                        end_window[i])[:10]].CLUSTER_ID ==
                                        cluster, 'Reason'] = 'Just Appeared'

                        else:
                            universal_windows_review_info[str(
                                    end_window[i])[:10]].\
                                    loc[universal_windows_review_info[str(
                                        end_window[i])[:10]].CLUSTER_ID ==
                                        cluster,
                                        'Reason'] = 'Is Growing Quickly'
                else:
                    current_size = len(set(universal_windows[i].query(
                        'CLUSTER_ID == @cluster')['ID']) - visited_ids)
                    if i == tracking_path[0]:
                        cumulative_size[i][cluster] = current_size
                    else:
                        cumulative_size[i][cluster] = \
                            cumulative_size[i - 1][cluster] + current_size

                    score = cumulative_size[i][cluster]

                    if cluster in reviewed_clusters:
                        universal_windows_review_info[str(
                                end_window[i])[:10]].\
                                    loc[universal_windows_review_info[str(
                                            end_window[i])[:10]].
                                        CLUSTER_ID == cluster,
                                        'Reason'] = 'Cluster previously ' + \
                                                    'flagged'

                    elif score > threshold:
                        flag_dates[cluster].append(str(end_window[i])[:10])
                        belonging_issues = \
                            set(universal_windows_review_info[str(
                                end_window[i])[:10]].loc[
                                    universal_windows_review_info[str(
                                            end_window[i])[:10]].CLUSTER_ID ==
                                    cluster, 'INCDT_ISSUE_LABEL'].
                                values)
                        for issue in belonging_issues:
                            if issue not in issue_detection_date.keys():
                                issue_detection_date[issue] = str(
                                    end_window[i])[:10]
                                issue_detection_cluster_id[issue] = cluster
                        universal_windows_review_info[str(
                                end_window[i])[:10]].\
                            loc[universal_windows_review_info[str(
                                    end_window[i])[:10]].CLUSTER_ID == cluster,
                                'Action'] = 'Flagged_for_Review'

                        universal_windows_review_info[str(
                                end_window[i])[:10]].\
                            loc[universal_windows_review_info[str(
                                    end_window[i])[:10]].CLUSTER_ID == cluster,
                                'Reason'] = 'Cluster size over ' + \
                                            str(threshold)

                        reviewed_clusters.append(cluster)

                universal_windows_review_info[str(
                        end_window[i])[:10]]. \
                    loc[universal_windows_review_info[str(
                        end_window[i])[:10]].CLUSTER_ID == cluster,
                        'Score'] = score
            else:
                if flagsize:
                    if i == tracking_path[0]:
                        cumulative_size[i][cluster] = 0
                    else:
                        cumulative_size[i][cluster] = \
                            cumulative_size[i - 1][cluster]

        new_ids = set(
            universal_windows_review_info[str(end_window[i])[:10]]['ID'])
        total_new_incidents.append(len(new_ids - visited_ids))
        total_incidents_to_review.append(len(
                set(universal_windows_review_info[str(
                    end_window[i])[:10]].query(
                    'Action == "Flagged_for_Review"')['ID']) - visited_ids))
        visited_ids = visited_ids.union(new_ids)

        universal_windows_review_info[str(
                end_window[i])[:10]] = universal_windows_review_info[str(
                        end_window[i])[:10]].sort_values(['Score'],
                                                         ascending=False)
        summary_windows[str(
                end_window[i])[:10]] = universal_windows_review_info[str(
                        end_window[i])[:10]].groupby('CLUSTER_ID').agg(
            {'EARLIEST_RECORD_DATE': 'min', 'ID': 'count',
             'REL_CMP_SUM_NET_AMOUNT': 'mean', 'Action': 'first',
             'Reason': 'first', 'Score': 'first'}).sort_values(['Score'],
                                                               ascending=False
                                                               ).reset_index()

        total_clusters.append(
            len(summary_windows[str(end_window[i])[:10]]['CLUSTER_ID']))
        total_clusters_to_review.append(len(summary_windows[str(end_window[i])[
                                        :10]].query(
            'Action == "Flagged_for_Review"')['CLUSTER_ID']))

    # build return dataframe
    detection_date_df = pd.DataFrame.from_dict(
        issue_detection_date, 'index').reset_index()
    detection_date_df.columns = [
        'INCDT_ISSUE_LABEL',
        'Reviewing_Detection_Date']
    detection_date_df_tmp = pd.DataFrame.from_dict(
        issue_detection_cluster_id, 'index').reset_index()
    detection_date_df_tmp.columns = [
        'INCDT_ISSUE_LABEL', 'Detection_Cluster_ID']
    detection_date_df = detection_date_df.merge(detection_date_df_tmp)
    detection_date_df = df.\
        query('INCDT_ISSUE_LABEL in @top_issues').\
        sort_values('EARLIEST_INDICATION_DATE').\
        groupby('INCDT_ISSUE_LABEL').first().\
        loc[:, ['EARLIEST_INDICATION_DATE']].reset_index().\
        merge(df.
              query('INCDT_ISSUE_LABEL in @top_issues').
              sort_values('EARLIEST_RECORD_DATE').
              groupby('INCDT_ISSUE_LABEL').first().
              loc[:, ['EARLIEST_RECORD_DATE', 'ID']].reset_index()).\
        merge(detection_date_df).\
        merge(df.query('INCDT_ISSUE_LABEL in @top_issues').
              groupby('INCDT_ISSUE_LABEL').count()['ID'].
              reset_index().rename(columns={'ID': 'Number_of_Incidents'}))
    detection_date_df['Days_Detected_After_First_Incident'] = [
        (datetime.datetime.strptime(
            d_dt,
            '%Y-%m-%d') - e_dt).days for d_dt,
        e_dt in zip(
            detection_date_df.Reviewing_Detection_Date,
            detection_date_df.EARLIEST_INDICATION_DATE)]

    return(total_new_incidents,
           total_incidents_to_review,
           total_clusters,
           total_clusters_to_review,
           universal_windows_review_info,
           summary_windows,
           issue_detection_date,
           detection_date_df)

# run the flagging strategy:


def main_flagging(transformed_table,
                  universal_anchor,
                  universal_labels,
                  clustering_features,
                  clustering_ranges,
                  optimal_weights,
                  universal_windows,
                  tracking_path,
                  end_window,
                  columns_list,
                  col_list_flag,
                  threshold=0.3,
                  var='gaussian',
                  precomputed=True,
                  visited_ids=None,
                  visited_clusters=None,
                  deviation=False,
                  product=False,
                  save_data=True,
                  folder=cf.folder,
                  trim_columns=cf.trim_columns):

    universal_mean, universal_std = get_universal_variations(
            df=transformed_table,
            universal_labels=universal_labels,
            universal_anchor=universal_anchor,
            clustering_features=clustering_features,
            clustering_ranges=clustering_ranges,
            weights=optimal_weights,
            matrix=deviation)

    universal_windows_info, visited_ids, visited_clusters = \
        flagging_wrapup(transformed_table,
                        universal_anchor,
                        universal_labels,
                        clustering_features,
                        clustering_ranges,
                        optimal_weights,
                        universal_windows,
                        tracking_path,
                        threshold=threshold,
                        end_window=end_window,
                        var=var,
                        precomputed=precomputed,
                        universal_mean=universal_mean,
                        universal_std=universal_std,
                        columns_list=col_list_flag,
                        visited_ids=visited_ids,
                        visited_clusters=visited_clusters,
                        deviation=deviation,
                        product=product)

    if save_data:
        print('saving reviewing strategy variables to pickle')
        trim_review_info = explib.trim_windows(universal_windows_info,
                                               columns=trim_columns)

        save(trim_review_info,
             filename=folder + 'trim_review_info.pckl')

    return(universal_windows_info,
           visited_ids,
           visited_clusters,
           universal_mean,
           universal_std)

# Flagging strategy wrap-up function based on
    # Probability of belonging to the assigned cluster
    # Cluster size
    # Cluster growth
# Takes as argument a dataframe, the universal anchor (dic of centroids),
    # the universal labels, the clustering features,
    # the clustering ranges, the weights, the universal_windows,
    # the tracking path (=(a,b) where a<b), the flag threshold
    # the end_window dictionary, the top_issues to evaluate over,
    # and the score type
    # Whether the universal deviations are precomputed or not
    # If precomputed=True, universal_mean and universal_std are universal
    # deviations
    # column list to keep in the detail_review_info,
    # whether or not to filter incidents without fail codes,
    # the already visited incidents ids (list), the visited clusters (list),
    # whether to use multivariate gaussian instead of univariate,
    # if deviation=True, the universal deviationss should be matrices,
    # the way to aggregate the probability results (product=True or False)
# Returns the dictionary of detailed reviews and the count of the unique
    # incidents flagged for score, growth or size


def flagging_wrapup(df,
                    universal_anchor,
                    universal_labels,
                    clustering_features,
                    clustering_ranges,
                    weights,
                    universal_windows,
                    tracking_path,
                    threshold,
                    end_window,
                    var='gaussian',
                    # can = 'standard', 'uniform' or 'gaussian'
                    precomputed=False,
                    universal_mean=None,
                    universal_std=None,
                    columns_list=['ID',
                                  'ESN',
                                  'REL_CMP_FAIL_CODE_LIST',
                                  'GREEN_WRENCH_FAULT_CODE_LIST',
                                  'REL_OEM_NAME',
                                  'GREEN_WRENCH_LIST',
                                  'REL_ANALYSIS_RATE_CAT',
                                  'REL_CMP_ENGINE_MILES_LIST',
                                  'REL_CMP_FAIL_DATE_LIST',
                                  'REL_CMP_SUM_NET_AMOUNT',
                                  'EARLIEST_INDICATION_DATE',
                                  'EARLIEST_RECORD_DATE',
                                  'INCDT_ISSUE_LABEL', 'CLUSTER_ID'],
                    visited_ids=None,
                    visited_clusters=None,
                    deviation=False,
                    product=False):

    if visited_ids is None or visited_clusters is None:
        visited_ids = set([])
        visited_clusters = set([])

    universal_windows_review_info = {}
    for i in tqdm(range(tracking_path[0], tracking_path[1] + 1)):

        universal_windows_review_info[str(end_window[i])[:10]] = \
            universal_windows[i].loc[:, columns_list]
        universal_windows_review_info[str(end_window[i])[:10]]['SCORE'] = None

        if deviation:
            universal_windows_review_info[str(
                    end_window[i])[:10]]['PROBABILITY_LIST'] = None
        new_incidents = universal_windows[i].query('not ID in @visited_ids')
        new_incidents_ids = set(new_incidents['ID'])
        for incident in new_incidents_ids:
            inc_ = universal_windows[i].query('ID == @incident')
            cluster_id = list(inc_['CLUSTER_ID'])[0]

            if not(deviation):
                score = get_score(df,
                                  incident,
                                  cluster_id,
                                  universal_anchor,
                                  universal_labels,
                                  clustering_features,
                                  clustering_ranges,
                                  weights,
                                  var='gaussian',
                                  precomputed=precomputed,
                                  universal_mean=universal_mean,
                                  universal_std=universal_std)

            else:
                score, probability = get_specificity(df,
                                                     incident,
                                                     cluster_id,
                                                     clustering_features,
                                                     clustering_ranges,
                                                     weights,
                                                     universal_mean,
                                                     universal_std,
                                                     threshold,
                                                     product=product)

            universal_windows_review_info[str(end_window[i])[:10]].\
                loc[universal_windows_review_info[str(
                            end_window[i])[:10]].ID == incident, 'SCORE'] \
                = score
            if deviation:
                universal_windows_review_info[str(
                        end_window[i])[:10]]['PROBABILITY_LIST'] = \
                                             str(probability)

        visited_ids = visited_ids.union(new_incidents_ids)
        visited_clusters = visited_clusters.union(
            set(new_incidents['CLUSTER_ID']))

    return(universal_windows_review_info,
           visited_ids,
           visited_clusters)

# %% Update results with new live-stream data

# Reproduce used features


def reproduce_features(feature_dict,
                       clustering_features,
                       clustering_ranges):
    new_features_dic = copy.deepcopy(feature_dict)
    cm_clustering_ranges = np.cumsum(clustering_ranges)
    fail_done = False
    cat_done = False
    dict_keys = feature_dict.keys()
    for feature in dict_keys:
        if (isinstance(
                feature_dict[feature],
                float) and feature_dict[feature] >= 0
            and feature_dict[feature] <= 1) \
                       or isinstance(feature_dict[feature], int):
            indx = list(feature_dict.keys()).index(feature)
            len_feature = len(feature)
            if fail_done or cat_done:
                indx += 1
            if indx == 0:

                new_features_dic[feature] = list(map(
                    lambda x: x[len_feature + 1:],
                    clustering_features[:cm_clustering_ranges[min(
                            indx, len(clustering_ranges))]]))
            else:
                new_features_dic[feature] = list(
                        map(lambda x: x[len_feature + 1:],
                            clustering_features[cm_clustering_ranges[indx -
                                                                     1]:
                            cm_clustering_ranges[indx]]))
            if feature == 'REL_CMP_FAIL_CODE_LIST':
                fail_done = True
            if feature == 'REL_ANALYSIS_RATE_CAT':
                cat_done = True

    return(new_features_dic)

# %% Run the model on new live-stream data and integrate it to previously
# computed results


def update_preprocessing(feature_dict,
                         clustering_features,
                         clustering_ranges,
                         new_raw_table,
                         component_data_ori=None,
                         verbose=False,
                         impute_fault=True,
                         impute_fail=True,
                         impute_fail_threshold=0.7):

    new_feature_dict = reproduce_features(feature_dict,
                                          clustering_features,
                                          clustering_ranges)
    new_transformed_table, clustering_features_, clustering_ranges_ = \
        plib.preprocessing(new_raw_table,
                           new_feature_dict,
                           verbose=verbose,
                           impute_fault=impute_fault,
                           impute_fail=impute_fail,
                           component_df=component_data_ori,
                           impute_fail_threshold=impute_fail_threshold)

    return(new_feature_dict,
           new_transformed_table)


def update_clustering(start_window,
                      end_window,
                      anchor,
                      windows,
                      clustering_results,
                      window_step,
                      window_size,
                      optimal_weights,
                      optimal_threshold,
                      old_transformed_table,
                      new_transformed_table,
                      clustering_features,
                      clustering_ranges,
                      algorithm='hierarchical',
                      n_clusters_algorithm='birch_variable',
                      do_plot=True,
                      n_processes=6,
                      preweighted=True):

    last_start_window = start_window[-2]
    new_start_date = increment_date(
        last_start_window,
        window_step[0],
        window_step[1])

    truncated_old_transformed_table = \
        old_transformed_table. \
        loc[new_start_date <=
            old_transformed_table['EARLIEST_RECORD_DATE']]
    new_transformed_table = \
        new_transformed_table. \
        loc[new_start_date <=
            new_transformed_table['EARLIEST_RECORD_DATE']]
    total_new_transformed_table = truncated_old_transformed_table.append(
        new_transformed_table)
    total_new_transformed_table = total_new_transformed_table.drop_duplicates(
        subset=['ID'], keep="first")

    total_new_data = \
        total_new_transformed_table.loc[:,
                                        clustering_features].values

    new_weighted_data = copy.copy(total_new_data)

    pointer = 0
    for i in range(len(clustering_ranges)):
        new_weighted_data[:, pointer:(
                pointer + clustering_ranges[i])] = optimal_weights[i] * \
            new_weighted_data[:, pointer:(pointer + clustering_ranges[i])]
        pointer = pointer + clustering_ranges[i]

    new_clustering_results, new_start_window, new_end_window, new_anchor = \
        parallel_moving_windows_framework(
                data=new_weighted_data,
                df=total_new_transformed_table,
                clustering_ranges=clustering_ranges,
                weights=optimal_weights,
                size=window_size,
                step=window_step,
                algorithm=algorithm,
                n_clusters_algorithm=n_clusters_algorithm,
                threshold=optimal_threshold,
                reset=False,
                do_plot=False,
                start_date=new_start_date,
                n_processes=n_processes,
                preweighted=preweighted)

    new_tracking_path = (0, len(new_start_window) - 2)

    new_windows = explib.map_windows(total_new_transformed_table,
                                     new_start_window,
                                     new_end_window,
                                     new_clustering_results,
                                     new_tracking_path)

    start_window = start_window[:-1]
    start_window.extend(new_start_window)
    end_window = end_window[:-1]
    end_window.extend(new_end_window)

    max_old_windows = max(windows.keys())
    initial_list = list(new_windows.keys())
    for i in initial_list:
        new_windows[i + max_old_windows + 1] = new_windows[i]
        new_anchor[i + max_old_windows + 1] = new_anchor[i]
        new_clustering_results[i + max_old_windows +
                               1] = new_clustering_results[i]
        del new_windows[i]
        del new_anchor[i]
        del new_clustering_results[i]

    windows.update(new_windows)
    anchor.update(new_anchor)
    clustering_results.update(new_clustering_results)

    new_tracking_path = (max_old_windows, len(start_window) - 2)

    return(new_transformed_table,
           total_new_transformed_table,
           total_new_data,
           new_weighted_data,
           new_clustering_results,
           start_window,
           end_window,
           anchor,
           windows,
           new_tracking_path)


def update_tracking(universal_labels,
                    universal_ids,
                    universal_anchor,
                    universal_windows,
                    start_window,
                    end_window,
                    windows,
                    anchor,
                    clustering_results,
                    new_clustering_results,
                    new_tracking_path,
                    old_transformed_table,
                    total_new_transformed_table,
                    last_id_given,
                    tracking_type='hybrid',
                    centroids_threshold=5.0,
                    count_threshold=0.5):

    universal_ids, universal_labels, universal_anchor = \
        parallel_universal_ids(windows,
                               anchor,
                               new_clustering_results,
                               last_id_given=last_id_given,
                               tracking_path=new_tracking_path,
                               tracking_type=tracking_type,
                               centroids_threshold=centroids_threshold,
                               count_threshold=count_threshold,
                               universal_labels=universal_labels,
                               universal_ids=universal_ids,
                               universal_anchor=universal_anchor)

    new_transformed_results = transform_clustering_results(
        new_clustering_results, universal_ids, tracking_path=(
            new_tracking_path[0] + 1, new_tracking_path[1]))

    new_universal_windows = explib.map_windows(
        total_new_transformed_table,
        start_window,
        end_window,
        new_transformed_results,
        (new_tracking_path[0] + 1,
         new_tracking_path[1]))

    universal_windows.update(new_universal_windows)

    total_transformed_table = old_transformed_table.append(
        total_new_transformed_table).\
        drop_duplicates(subset=['ID'], keep="first")

    return(universal_ids,
           universal_labels,
           universal_anchor,
           new_transformed_results,
           universal_windows,
           total_transformed_table)


def update_flagging(total_transformed_table,
                    universal_labels,
                    universal_anchor,
                    clustering_features,
                    clustering_ranges,
                    optimal_weights,
                    universal_windows,
                    new_tracking_path,
                    end_window,
                    top_issues,
                    new_raw_table,
                    visited_ids,
                    visited_clusters,
                    tracking_path,
                    cumulative_ids,
                    n_new_incidents_per_window=None,
                    n_reviewed_incidents_per_window=None,
                    ids_reviewed_incidents=None,
                    ids_sampled_incidents=None,
                    detail_review_info=None,
                    issue_detection_date=None,
                    detection_date_df=None,
                    var='gaussian',
                    # can = 'standard', 'uniform' or 'gaussian'
                    filter_fail=False,
                    flag_fail=False,
                    flag_fault=False,
                    deviation=False,
                    product=False,
                    cols_to_flag=['LAST_MILE_RECORDED_IMPUTED_NORMALIZED',
                                  'REL_CMP_SUM_NET_AMOUNT_IMPUTED_NORMALIZED'],
                    manual_input=[],
                    flag_threshold=0.3,
                    sub_threshold=0.1,
                    build_detection_date_df=False,
                    bis=True,
                    flag_growth=True,
                    flag_size=True):

    universal_mean, universal_std = \
        get_universal_variations(total_transformed_table,
                                 universal_labels,
                                 universal_anchor,
                                 clustering_features,
                                 clustering_ranges,
                                 optimal_weights,
                                 matrix=deviation)
    if bis:
        detail_review_info_update = flagging_wrapup(
                        total_transformed_table,
                        universal_anchor,
                        universal_labels,
                        clustering_features,
                        clustering_ranges,
                        optimal_weights,
                        universal_windows,
                        tracking_path,
                        flag_threshold,
                        end_window,
                        top_issues,
                        var='gaussian',
                        # can = 'standard', 'uniform' or 'gaussian'
                        precomputed=True,
                        universal_mean=universal_mean,
                        universal_std=universal_std,
                        columns_list=['ID',
                                      'ESN',
                                      'REL_CMP_FAIL_CODE_LIST',
                                      'GREEN_WRENCH_FAULT_CODE_LIST',
                                      'REL_OEM_NAME',
                                      'GREEN_WRENCH_LIST',
                                      'REL_ANALYSIS_RATE_CAT',
                                      'REL_CMP_ENGINE_MILES_LIST',
                                      'REL_CMP_FAIL_DATE_LIST',
                                      'REL_CMP_SUM_NET_AMOUNT',
                                      'EARLIEST_INDICATION_DATE',
                                      'EARLIEST_RECORD_DATE',
                                      'INCDT_ISSUE_LABEL',
                                      'CLUSTER_ID'],
                        flag_growth=flag_growth,
                        flag_size=flag_size,
                        visited_ids=visited_ids,
                        visited_clusters=visited_clusters,
                        deviation=deviation,
                        product=product)

    else:
        n_new_incidents_per_window_update, \
            n_reviewed_incidents_per_window_update, \
            ids_reviewed_incidents_update, \
            ids_sampled_incidents_update, detail_review_info_update, \
            issue_detection_date_update, detection_date_df_update, \
            visited_ids_update, visited_clusters_update = \
            describe_reviews(total_transformed_table,
                             universal_anchor,
                             universal_labels,
                             clustering_features,
                             clustering_ranges,
                             optimal_weights,
                             universal_windows,
                             new_tracking_path,
                             flag_threshold,
                             end_window=end_window,
                             top_issues=top_issues,
                             precomputed=True,
                             universal_mean=universal_mean,
                             universal_std=universal_std,
                             columns_list=list(new_raw_table.columns) +
                             ['CLUSTER_ID'] +
                             ['CUM_INCDT_IN_CLUSTER'] +
                             ['EARLIEST_RECORD_DATE'],
                             sub_threshold=sub_threshold,
                             filter_fail=filter_fail,
                             visited_ids=visited_ids,
                             visited_clusters=visited_clusters,
                             deviation=deviation,
                             product=product,
                             cols_to_flag=cols_to_flag,
                             manual_input=manual_input,
                             var=var,
                             build_detection_date_df=build_detection_date_df)

    total_tracking_path = (tracking_path[0], new_tracking_path[1])

    if not(cumulative_ids is None):

        universal_windows, partial_cumulative_ids = explib.cumulative_counter(
            universal_windows, universal_labels, new_tracking_path)
        cumulative_ids.update(partial_cumulative_ids)

    if not(bis):
        n_new_incidents_per_window.extend(n_new_incidents_per_window_update)
        n_reviewed_incidents_per_window.extend(
            n_reviewed_incidents_per_window_update)
        ids_reviewed_incidents.extend(ids_reviewed_incidents_update)
        ids_sampled_incidents.extend(ids_sampled_incidents_update)
        detail_review_info.update(detail_review_info_update)
        issue_detection_date_update.update(issue_detection_date)
        issue_detection_date = issue_detection_date_update

        if build_detection_date_df:
            detection_date_df = detection_date_df.append(
                detection_date_df_update).drop_duplicates(subset=['ID'],
                                                          keep="first")
    else:
        detail_review_info.update(detail_review_info_update)

    visited_ids = visited_ids_update
    visited_clusters = visited_clusters_update

    return(universal_mean,
           universal_std,
           n_new_incidents_per_window,
           n_reviewed_incidents_per_window,
           ids_reviewed_incidents,
           ids_sampled_incidents,
           detail_review_info,
           issue_detection_date,
           detection_date_df,
           visited_ids,
           visited_clusters,
           total_tracking_path)


def update_windows(feature_dict,
                   clustering_features,
                   clustering_ranges,
                   new_raw_table,
                   start_window,
                   end_window,
                   anchor,
                   windows,
                   clustering_results,
                   window_step,
                   window_size,
                   optimal_weights,
                   optimal_threshold,
                   old_transformed_table,
                   universal_labels,
                   universal_ids,
                   universal_anchor,
                   universal_windows,
                   top_issues,
                   visited_ids,
                   visited_clusters,
                   tracking_path,
                   cumulative_ids,
                   n_new_incidents_per_window,
                   n_reviewed_incidents_per_window,
                   ids_reviewed_incidents,
                   ids_sampled_incidents,
                   detail_review_info,
                   issue_detection_date,
                   detection_date_df,
                   last_id_given,
                   component_data_ori=None,
                   verbose=False,
                   impute_fault=True,
                   impute_fail=True,
                   impute_fail_threshold=0.7,
                   algorithm='hierarchical',
                   n_clusters_algorithm='birch_variable',
                   do_plot=True,
                   n_processes=6,
                   tracking_type='hybrid',
                   centroids_threshold=5.0,
                   count_threshold=0.5,
                   var='gaussian',
                   # can = 'standard', 'uniform' or 'gaussian'
                   filter_fail=False,
                   flag_fail=False,
                   flag_fault=False,
                   deviation=False,
                   product=False,
                   cols_to_flag=['LAST_MILE_RECORDED_IMPUTED_NORMALIZED',
                                 'REL_CMP_SUM_NET_AMOUNT_IMPUTED_NORMALIZED'],
                   manual_input=[],
                   flag_threshold=0.3,
                   build_detection_date_df=False,
                   preweighted=True):

    new_feature_dict, \
           new_transformed_table = update_preprocessing(
                   feature_dict=feature_dict,
                   clustering_features=clustering_features,
                   clustering_ranges=clustering_ranges,
                   new_raw_table=new_raw_table,
                   component_data_ori=component_data_ori,
                   verbose=verbose,
                   impute_fault=impute_fault,
                   impute_fail=impute_fail,
                   impute_fail_threshold=impute_fail_threshold)

    upd_start_window = copy.deepcopy(start_window)
    upd_end_window = copy.deepcopy(end_window)
    upd_anchor = copy.deepcopy(anchor)
    upd_windows = copy.deepcopy(windows)
    upd_clustering_results = copy.deepcopy(clustering_results)

    new_transformed_table, \
        total_new_transformed_table, \
        total_new_data, \
        new_weighted_data, \
        new_clustering_results, \
        upd_start_window, \
        upd_end_window, \
        upd_anchor, \
        upd_windows, \
        new_tracking_path = update_clustering(
                      start_window=upd_start_window,
                      end_window=upd_end_window,
                      anchor=upd_anchor,
                      windows=upd_windows,
                      clustering_results=upd_clustering_results,
                      window_step=window_step,
                      window_size=window_size,
                      optimal_weights=optimal_weights,
                      optimal_threshold=optimal_threshold,
                      old_transformed_table=old_transformed_table,
                      new_transformed_table=new_transformed_table,
                      clustering_features=clustering_features,
                      clustering_ranges=clustering_ranges,
                      algorithm=algorithm,
                      n_clusters_algorithm=n_clusters_algorithm,
                      do_plot=do_plot,
                      n_processes=n_processes,
                      preweighted=preweighted)

    upd_universal_ids = copy.deepcopy(universal_ids)
    upd_universal_labels = copy.deepcopy(universal_labels)
    upd_universal_anchor = copy.deepcopy(universal_anchor)
    upd_universal_windows = copy.deepcopy(universal_windows)

    upd_universal_ids, \
        upd_niversal_labels, \
        upd_universal_anchor, \
        new_transformed_results, \
        upd_universal_windows, \
        total_transformed_table = update_tracking(
                   upd_universal_labels,
                   upd_universal_ids,
                   upd_universal_anchor,
                   upd_universal_windows,
                   upd_start_window,
                   upd_end_window,
                   upd_windows,
                   upd_anchor,
                   upd_clustering_results,
                   new_clustering_results,
                   new_tracking_path,
                   old_transformed_table,
                   total_new_transformed_table,
                   last_id_given,
                   centroids_threshold=centroids_threshold,
                   tracking_type=tracking_type,
                   count_threshold=count_threshold)

    upd_n_new_incidents_per_window = copy.deepcopy(n_new_incidents_per_window)
    upd_n_reviewed_incidents_per_window = \
        copy.deepcopy(n_reviewed_incidents_per_window)
    upd_ids_reviewed_incidents = copy.deepcopy(ids_reviewed_incidents)
    upd_ids_sampled_incidents = copy.deepcopy(ids_sampled_incidents)
    upd_detail_review_info = copy.deepcopy(detail_review_info)
    upd_cumulative_ids = copy.deepcopy(cumulative_ids)
    upd_issue_detection_date = copy.deepcopy(issue_detection_date)
    upd_detection_date_df = copy.deepcopy(detection_date_df)
    upd_visited_ids = copy.deepcopy(visited_ids)
    upd_visited_clusters = copy.deepcopy(visited_clusters)

    universal_mean, \
        universal_std, \
        upd_n_new_incidents_per_window, \
        upd_n_reviewed_incidents_per_window, \
        upd_ids_reviewed_incidents, \
        upd_ids_sampled_incidents, \
        upd_detail_review_info, \
        upd_issue_detection_date, \
        upd_detection_date_df, \
        upd_visited_ids, \
        upd_visited_clusters, \
        total_tracking_path = update_flagging(
                   total_transformed_table,
                   upd_universal_labels,
                   upd_universal_anchor,
                   clustering_features,
                   clustering_ranges,
                   optimal_weights,
                   upd_universal_windows,
                   new_tracking_path,
                   upd_end_window,
                   top_issues,
                   new_raw_table,
                   upd_visited_ids,
                   upd_visited_clusters,
                   tracking_path,
                   upd_cumulative_ids,
                   upd_n_new_incidents_per_window,
                   upd_n_reviewed_incidents_per_window,
                   upd_ids_reviewed_incidents,
                   upd_ids_sampled_incidents,
                   upd_detail_review_info,
                   upd_issue_detection_date,
                   upd_detection_date_df,
                   var=var,
                   # can = 'standard', 'uniform' or 'gaussian'
                   filter_fail=filter_fail,
                   flag_fail=flag_fail,
                   flag_fault=flag_fault,
                   deviation=deviation,
                   product=product,
                   cols_to_flag=cols_to_flag,
                   manual_input=manual_input,
                   flag_threshold=flag_threshold,
                   build_detection_date_df=build_detection_date_df)

    return(total_transformed_table,
           upd_start_window,
           upd_end_window,
           upd_windows,
           upd_anchor,
           upd_clustering_results,
           total_tracking_path,
           upd_universal_labels,
           upd_universal_ids,
           upd_universal_anchor,
           upd_universal_windows,
           top_issues,
           upd_n_new_incidents_per_window,
           upd_n_reviewed_incidents_per_window,
           upd_ids_reviewed_incidents,
           upd_ids_sampled_incidents,
           upd_detail_review_info,
           upd_issue_detection_date,
           upd_detection_date_df,
           upd_visited_ids,
           upd_visited_clusters)

# %% Helper functions


def load_pickle(file_name):
    import pickle

    # open a file, where you stored the pickled data
    file = open(file_name, 'rb')

    # dump information to that file
    data = pickle.load(file)

    # close the file
    file.close()

    return(data)

# %%


def build_postprocessing_table(universal_windows,
                               universal_windows_info,
                               end_window,
                               transformed_table,
                               threshold=.8):
    date_list = list(universal_windows_info.keys())
    for i in tqdm(universal_windows.keys()):
        universal_windows[i]['INCDT_SCORE'] = universal_windows_info[
                date_list[i]]['SCORE']
    concat_windows = copy.deepcopy(universal_windows)
    for i in tqdm(universal_windows.keys()):
        concat_windows[i]['WINDOW_DATE'] = end_window[i]
    concat_windows = pd.concat([
            concat_windows[i] for i in universal_windows.keys()])

    agg_dictionary = \
        {"WINDOW_DATE":
            {"FIRST_RECORD_DATE": 'first', "LAST_RECORD_DATE": 'last'},
            "INCDT_SCORE": 'first'}
    concat_windows = concat_windows.groupby(['ID', 'CLUSTER_ID']).\
        agg(agg_dictionary)
    concat_windows.columns = concat_windows.columns.droplevel(0)
    concat_windows = concat_windows.reset_index().\
        merge(transformed_table, on='ID', how='left')
    agg_dictionary = {}
    agg_dictionary['ID'] = 'count'
    agg_dictionary['REL_CMP_SUM_NET_AMOUNT_IMPUTED'] = 'sum'
    agg_dictionary['LAST_MILE_RECORDED_IMPUTED'] = 'mean'
    agg_dictionary['CLUSTER_ID'] = 'first'
    # agg_dictionary['REL_CMP_SUM_REPAIR_LABOR_AMOUNT'] = 'sum'
    build_features = ['REL_OEM_NAME_',
                      'REL_ANALYSIS_RATE_CAT_X',
                      'REL_ANALYSIS_RATE_CAT_20']
    other_features = ['GREEN_WRENCH_FAULT_CODE_LIST_',
                      'GREEN_WRENCH_LIST_',
                      'REL_CMP_FAIL_CODE_LIST_']

    cols_name_map = ['REL_OEM_NAME',
                     'ENGINE_CATEGORY',
                     'MODEL_YEAR']
    cols_build_features = {}
    cols_other_features = {}
    for feature in build_features:
        cols_build_features[feature] = list(
                concat_windows.columns[list(
                        map(lambda x: x.find(feature) >= 0, list(
                                concat_windows.columns)))])
        for instance in cols_build_features[feature]:
            agg_dictionary[instance] = 'mean'

    for feature in other_features:
        cols_other_features[feature] = list(
                concat_windows.columns[list(
                        map(lambda x: x.find(feature) >= 0, list(
                                concat_windows.columns)))])
        for instance in cols_other_features[feature]:
            agg_dictionary[instance] = 'mean'

    return(concat_windows,
           agg_dictionary,
           build_features,
           other_features,
           cols_build_features,
           cols_other_features,
           cols_name_map)


def get_engines_table(folder_path='/Users/pg875/Downloads/' +
                      'ADHOC_REL_ENGINE_FEATURES/'):
    engines_table = plib.read_online_table(folder_path,
                                           None)

    engines_table['MODEL_YEAR'] = engines_table['REL_ENGINE_NAME_DESC'].apply(
                lambda x: '2013' if x.find('2013') >= 0
                else '2017')

    engines_table['ENGINE_CATEGORY'] = engines_table['REL_ENGINE_NAME_DESC'].\
        apply(lambda x: 'X1' if x.find('1 ') >= 0
              else ('X2' if x.find('2 ') >= 0
                    else 'X3'))

    return(engines_table)


def get_scores(concat_windows,
               engines_table,
               agg_dictionary,
               start_date,
               end_date,
               cols_build_features,
               cols_other_features,
               cols_name_map,
               build_features,
               other_features,
               threshold=0.8):

    count_table = pd.DataFrame()

    for i in range(len(cols_name_map)):
        for j in list(itertools.combinations(cols_name_map, i+1)):
            count_table = count_table.append(
                    engines_table[[a and b for a, b
                                  in zip(
                                          engines_table['REL_BUILD_DATE'] >=
                                          increment_months(start_date, -48),
                                         engines_table['REL_BUILD_DATE'] <
                                         end_date)]].groupby(j)[
                                          'ENGINE_DETAILS_ESN'].count().
                    reset_index())

    df = concat_windows.query(
            'EARLIEST_RECORD_DATE >= @start_date').\
        query('EARLIEST_RECORD_DATE < @end_date').\
        groupby('CLUSTER_ID').agg(agg_dictionary)
    for feature in range(len(build_features)):
        df[cols_name_map[feature]] = \
            df.loc[:,
                   cols_build_features[build_features[feature]]].\
            idxmax(axis=1).apply(lambda x: x[x.rfind('_')+1:])
        df[cols_name_map[feature] + '_PERCENTAGE'] = \
            df.loc[:, cols_build_features[build_features[feature]]].max(axis=1)
        df[cols_name_map[feature] + '_FILTER'] = \
            df.apply(lambda x: x[cols_name_map[feature]]
                     if x[cols_name_map[feature] + '_PERCENTAGE'] > threshold
                     else 'ALL', axis=1)

    for feature in range(len(other_features)):
        df[other_features[feature]] = df.loc[
                :, cols_other_features[other_features[feature]]].\
                idxmax(axis=1).apply(lambda x: x[x.rfind('_')+1:])
        df[other_features[feature] + '_PERCENTAGE'] = df.loc[
                :, cols_other_features[other_features[feature]]].max(axis=1)

    count_table = count_table.fillna('ALL')
    new_row = [engines_table.shape[0], 'ALL', 'ALL', 'ALL']
    count_table = count_table.append(pd.DataFrame(data=[new_row],
                                                  columns=count_table.columns))

    df = df.merge(count_table, how='left', left_on=[
            x + '_FILTER' for x in cols_name_map], right_on=cols_name_map, )
    to_drop = [x for x in df if x.endswith('_y')]
    df.drop(to_drop, axis=1, inplace=True)
    df.rename(columns={'ENGINE_DETAILS_ESN': 'SUBPOPULATION_ESN_COUNT',
                       'ID': 'INCDT_COUNT'}, inplace=True)
    df['SUBPOPULATION_ESN_COUNT'] = df['SUBPOPULATION_ESN_COUNT'].\
        fillna(engines_table.shape[0])
    df['RPH'] = 100*(df['INCDT_COUNT']/df['SUBPOPULATION_ESN_COUNT'])
    return(df)


def jitter(a_series,
           noise_reduction=100000):
    return (np.random.random(len(a_series)) * a_series.std(
            )/noise_reduction)-(a_series.std()/(2*noise_reduction))


def build_export_table(universal_windows,
                       end_window,
                       transformed_table,
                       universal_windows_info,
                       incdt_threshold=.25,
                       folder_path=cf.path_esn_table,
                       start_date=None,
                       end_date=None,
                       time_step=None,
                       do_export=cf.export_concat_tables):

    if start_date is None:
        start_date = transformed_table['EARLIEST_RECORD_DATE'].min()
    if end_date is None:
        end_date = end_window[0]
    concat_windows, \
        agg_dictionary, \
        build_features, \
        other_features, \
        cols_build_features, \
        cols_other_features, \
        cols_name_map = build_postprocessing_table(universal_windows,
                                                   universal_windows_info,
                                                   end_window,
                                                   transformed_table,
                                                   threshold=.8)

    export_concat_windows = concat_windows.loc[:,
                                               ['CLUSTER_ID',
                                                'ID',
                                                'FIRST_RECORD_DATE',
                                                'LAST_RECORD_DATE',
                                                'first']]
    export_concat_windows = export_concat_windows.rename(columns={
            'first': 'INCDT_SCORE'})
    export_concat_windows['FLAG'] = export_concat_windows[
            'INCDT_SCORE'].apply(lambda x: x < incdt_threshold)

    engines_table = get_engines_table(folder_path=folder_path)

    if time_step is None:
        time_step = 7

    existing_clusters = set()
    rank = {}
    for i in tqdm(range(len(universal_windows.keys()))):
        print(end_date)
        rank[i] = {}
        df = get_scores(concat_windows,
                        engines_table,
                        agg_dictionary,
                        start_date,
                        end_date,
                        cols_build_features,
                        cols_other_features,
                        cols_name_map,
                        build_features,
                        other_features)
        nq = min(df.shape[0], 10)
        if nq > 1:
            df['RPH_QUANTILE'] = pd.qcut(df['RPH'] + jitter(df['RPH']),
                                         nq, range(nq)).astype('int64')
            df['COST_QUANTILE'] = pd.qcut(
                    df['REL_CMP_SUM_NET_AMOUNT_IMPUTED'] +
                    jitter(df['REL_CMP_SUM_NET_AMOUNT_IMPUTED']),
                    nq, range(nq)).astype('int64')
        else:
            df['RPH_QUANTILE'] = 1
            df['COST_QUANTILE'] = 1
        df['TOTAL_SCORE'] = df['RPH_QUANTILE'] + df['COST_QUANTILE']
        df = df.sort_values(['TOTAL_SCORE'], ascending=False)
        df['RANK'] = df['TOTAL_SCORE'].rank(
                method='min', ascending=False).apply(lambda x: int(x))
        current_clusters = df['CLUSTER_ID']
        for j in range(len(df)):
            rank[i][current_clusters.iloc[j]] = df['RANK'].iloc[j]
        if i != 0:
            df['PREV_RANK'] = [rank[i-1][s] if s in rank[i-1].keys()
                               else -1 for s in current_clusters]
        else:
            df['PREV_RANK'] = -1
        df['NEW_CLUSTER'] = [s not in existing_clusters
                             for s in current_clusters]
        existing_clusters = existing_clusters.union(current_clusters)

        df['RUN_DATE'] = str(end_date)[:10]

        df = df.loc[:, ['CLUSTER_ID',
                        'RUN_DATE',
                        'INCDT_COUNT',
                        'SUBPOPULATION_ESN_COUNT',
                        'RPH',
                        'RPH_QUANTILE',
                        'REL_CMP_SUM_NET_AMOUNT_IMPUTED',
                        'COST_QUANTILE',
                        'TOTAL_SCORE',
                        'LAST_MILE_RECORDED_IMPUTED'] +
                    [s + '_x' for s in cols_name_map] + other_features +
                    [s + '_PERCENTAGE' for s in cols_name_map +
                     other_features] + ['NEW_CLUSTER', 'RANK', 'PREV_RANK']]
        if i == 0:
            export_df = df
        else:
            export_df = export_df.append(df)

        end_date = increment_date(end_date, 0, time_step)

    if do_export:
        for feature in list(export_df.dtypes[(export_df.dtypes ==
                                              'float64')].index):
            export_df[feature] = export_df[feature].\
                apply(lambda x: round(x, 8))
        export_df['RUN_DATE'] = export_df['RUN_DATE'].\
            apply(lambda x: str(x)[:10])
        export_concat_windows['INCDT_SCORE'] = export_concat_windows[
                'INCDT_SCORE'].apply(lambda x: round(x, 8))
        export_concat_windows['FIRST_RECORD_DATE'] = export_concat_windows[
                'FIRST_RECORD_DATE'].apply(lambda x: str(x)[:10])
        export_concat_windows['LAST_RECORD_DATE'] = export_concat_windows[
                'LAST_RECORD_DATE'].apply(lambda x: str(x)[:10])
        for i in list(export_df.dtypes[export_df.dtypes == 'float64'].index):
            export_df[i] = export_df[i].apply(lambda x: round(x, 8))

        export_concat_windows_pyarrow = pa.Table.from_pandas(
                export_concat_windows)
        pq.write_table(export_concat_windows_pyarrow, 'concat_windows.parquet')

        export_df_pyarrow = pa.Table.from_pandas(export_df)
        pq.write_table(export_df_pyarrow, 'concat_clusters.parquet')

    return(export_concat_windows,
           export_df)

# %%
