#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 13:50:29 2018

@author: ph697
"""
########################################################################################################
#Librairies
import copy
import numpy as np
import pandas as pd
import random
import datetime
import calendar
import time
import sys
from tqdm import tqdm
from scipy import spatial
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import Birch
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
from scipy.spatial.distance import pdist, squareform
########################################################################################################



########################################################################################################
# This function builds the weighted data for Birch clustering
# Output: the weighted data in the same format of input data
def get_weighted_features(data, # Input Feature Matrix
                          clustering_ranges, # Input range of each feature
                          optimal_weights): # weight of each feature
    weighted_data = copy.copy(data)
    pointer = 0
    for i in range(len(clustering_ranges)):
        weighted_data[:, pointer:(pointer + clustering_ranges[i])] = \
            optimal_weights[i] * weighted_data[:, pointer:(pointer + clustering_ranges[i])]
        pointer = pointer + clustering_ranges[i]

    return weighted_data

# This function gives a list of dates starting from start_date and end prior to end_date with certain days_skip
# Output: a list of dates
def daterange(start_date, # start date of the range
              end_date, # end date of the range
              days_skip = 7): # number of days to skip in each window
    n = 0
    while start_date + datetime.timedelta(n + days_skip) < end_date:
        n = n + days_skip
        yield start_date + datetime.timedelta(n)

# This function is the main function to run the birch clustering
# Output: clustering result in a dataframe format and the list of time stamps in string format
def birch_clustering(weighted_data, # the weighted data for clustering
                     transformed_table, # the original feature table
                     start_date, # the start date of the window
                     end_date, # the end date of the window
                     days_skip, # the spand in days of the window
                     branching_factor = 10000, # braching factor in Birch
                     maximun_cluster_num = None, # maximum number of clusters in Birch
                     birch_threshold = 2.125 # split threhsold for Birch
                     ):

    clustering_time_stamps = []

    for single_date in daterange(start_date, end_date, days_skip):
        clustering_time_stamps.extend([single_date.strftime("%Y-%m-%d")])

    #Start Birch Clustering
    model = Birch(branching_factor = branching_factor, 
        n_clusters = maximun_cluster_num, 
        threshold = birch_threshold, 
        compute_labels=True)

    model.fit(weighted_data[transformed_table.EARLIEST_RECORD_DATE.apply(lambda x:x.date()) <= start_date, :])

    birch_clustering_records = transformed_table.loc[:, ['ID', 'ESN']]
    # Start online learning process for Birch
    for cur_ind in tqdm(range(len(clustering_time_stamps))):
        cur_date = clustering_time_stamps[cur_ind]
        if cur_ind == 0:
            clustering_indice = (transformed_table.EARLIEST_RECORD_DATE.apply(lambda x:x.date()) <= \
                datetime.datetime.strptime(cur_date, '%Y-%m-%d').date()) & \
            (transformed_table.EARLIEST_RECORD_DATE.apply(lambda x:x.date()) > start_date)
        else:
            clustering_indice = (transformed_table.EARLIEST_RECORD_DATE.apply(lambda x:x.date()) <= \
                datetime.datetime.strptime(cur_date, '%Y-%m-%d').date()) & \
            (transformed_table.EARLIEST_RECORD_DATE.apply(lambda x:x.date()) > \
                datetime.datetime.strptime(clustering_time_stamps[cur_ind - 1], '%Y-%m-%d').date())

        all_observation_indice = transformed_table.EARLIEST_RECORD_DATE.apply(lambda x:x.date()) <= \
        datetime.datetime.strptime(cur_date, '%Y-%m-%d').date()

        if np.sum(clustering_indice) > 0:
            model.partial_fit(weighted_data[clustering_indice, :])
        birch_clustering_records[str(cur_date)] = -1
        birch_clustering_records.loc[all_observation_indice, str(cur_date)] = \
        model.predict(weighted_data[all_observation_indice, :])

    return birch_clustering_records, clustering_time_stamps

# This function plots the number of clusters given by birch and average number of incidents in each cluster
def birch_cluster_size_plt(birch_clustering_records, # clustering result in a dataframe format
                           clustering_time_stamps): # a list of time stamps which can help us locate the clustering result at that date
    cluster_num_count = birch_clustering_records.loc[:, clustering_time_stamps].\
        apply(lambda x: pd.Series.nunique(x) - 1)
    incident_num_count = birch_clustering_records.loc[:, clustering_time_stamps].\
        apply(lambda x: np.sum(x != -1))
    plt.plot_date([datetime.datetime.strptime(x, '%Y-%m-%d') for x in cluster_num_count.index], 
                   cluster_num_count.values, ls = 'solid', lw = 1.5, fillstyle = 'none', markersize = 0,
                  label = 'Number of Clusters')
    plt.plot_date([datetime.datetime.strptime(x, '%Y-%m-%d') for x in cluster_num_count.index], 
                   incident_num_count.values / cluster_num_count.values, ls = 'solid', lw = 1.5, fillstyle = 'none', markersize = 0,
                  label = 'Average Number of Incidents Per Cluster')
    #plt.title('Number of Clusters over Time')
    plt.xlabel('Date')
    plt.legend()
    plt.ylabel('Value')
    plt.show()


# This function plots the corresponding cluster for a given issue and see how it envolves over time
def plot_cluster_graph(birch_clustering_records,
                       transformed_table,
                       clustering_time_stamps,
                       selected_issue, 
                       start_date,
                       end_date,
                       tracking_type = 'final', 
                       verbose = False):
    
    birch_clustering_records = birch_clustering_records.\
        merge(transformed_table.loc[:, ['ID', 'EARLIEST_RECORD_DATE', 'INCDT_ISSUE_LABEL']])
    issue_incidents = birch_clustering_records.\
        loc[birch_clustering_records.INCDT_ISSUE_LABEL == selected_issue, 
            ['ESN', 'EARLIEST_RECORD_DATE']].sort_values('EARLIEST_RECORD_DATE')
    issue_incidents['Incident_Count'] = 1
    issue_incidents['Cumulative_Count'] = issue_incidents['Incident_Count'].cumsum()
    # Calculate the size of clusters over time
    cluster_change_over_time = birch_clustering_records.\
        loc[:, clustering_time_stamps].\
        apply(lambda x:pd.Series(x).value_counts()).fillna(0)
    # Calculate the purity of clusters over time
    cluster_purity_change_over_time = birch_clustering_records.\
        loc[birch_clustering_records.INCDT_ISSUE_LABEL.isin(['not_issue', selected_issue]), 
            clustering_time_stamps].\
        apply(lambda x:pd.Series(x).value_counts()).fillna(0)
        
    plt.plot_date(issue_incidents.EARLIEST_RECORD_DATE, 
                  issue_incidents.Cumulative_Count, 
                  ls = 'solid', lw = 1.5,
                  label = 'Number of Incidents for Issue ' + selected_issue)
    
    issue_earliest_date = issue_incidents.iloc[0, 1].date()
#     print('The earliest detect date for incidents belong to this issue is ' + \
#               str(issue_earliest_date))
    if issue_earliest_date < start_date:
        issue_earliest_date = start_date

    for cur_ts in clustering_time_stamps:
        cur_ts = datetime.datetime.strptime(cur_ts, '%Y-%m-%d').date()
        if cur_ts >= issue_earliest_date:
          issue_earliest_date = str(cur_ts)[:10]
          break
    
    if tracking_type in ['final', 'first']:
        if tracking_type == 'final':
            tmp_df = birch_clustering_records.\
                loc[birch_clustering_records.INCDT_ISSUE_LABEL == selected_issue, 
                    :].sort_values('EARLIEST_RECORD_DATE')[str(end_date)[:10]].value_counts()
            top_cluster_row = tmp_df.index[0] + 1
        elif tracking_type == 'first':
            tmp_df = birch_clustering_records.\
                loc[birch_clustering_records.INCDT_ISSUE_LABEL == selected_issue, 
                    :].sort_values('EARLIEST_RECORD_DATE')[issue_earliest_date].\
                value_counts()
            top_cluster_row = tmp_df.index[1] + 1

        plt.plot_date([datetime.datetime.strptime(x, '%Y-%m-%d') for x in cluster_change_over_time.columns], 
                      cluster_change_over_time.iloc[top_cluster_row, :], 
                      ls = 'solid', lw = 1.5, fillstyle = 'none', 
                      markersize = 0,
                      label = 'Number of Incidents in Top Cluster with ID ' + \
                          str(top_cluster_row - 1))
        plt.plot_date([datetime.datetime.strptime(x, '%Y-%m-%d') for x in cluster_purity_change_over_time.columns], 
                      cluster_purity_change_over_time.loc[[top_cluster_row - 1]].values[0], 
                      ls = 'solid', lw = 1.5, fillstyle = 'none', 
                      markersize = 0,
                      label = 'Number of Incidents Belong to Current Issue and not_issue in Cluster ' + \
                          str(top_cluster_row - 1))
        detection_indices = np.where(cluster_purity_change_over_time.loc[[top_cluster_row - 1]] > 3)[1]
        if len(detection_indices) > 0:
            detection_date = datetime.datetime.strptime(cluster_purity_change_over_time.columns[detection_indices[0]], 
                                                        '%Y-%m-%d')
            plt.plot_date(detection_date, 0, 'r*', markersize=20, label='Clustering Detection date')
    else:
        # update cluster ID at each iteration
        cluster_size_record = []
        incident_num_in_cluster = []
        total_col_num = birch_clustering_records.shape[1]
        # print(np.where(online_tracker_info_records.columns == issue_earliest_date)[0][0])
        start_col_num = np.where(birch_clustering_records.columns == issue_earliest_date)[0][0]
        if verbose:
            print('Earlest Date Col is ' + issue_earliest_date + ', and column indice is ' + str(start_col_num))
        negative_filtered_df = birch_clustering_records.\
                                query('INCDT_ISSUE_LABEL == @selected_issue')
        cluster_evaluation_df = birch_clustering_records.\
                                loc[birch_clustering_records.INCDT_ISSUE_LABEL.isin([selected_issue, 
                                                                                            'not_issue']), :].\
                                copy()
        while start_col_num < total_col_num:
            negative_filtered_df_tmp = negative_filtered_df.loc[negative_filtered_df.iloc[:, start_col_num] != -1, :]

            if negative_filtered_df_tmp.shape[0] > 0:
                cur_cluster_id = negative_filtered_df_tmp.iloc[:, start_col_num].value_counts().index[0]
                cluster_size_record.append(np.sum(birch_clustering_records.\
                                           iloc[:, start_col_num] == cur_cluster_id))
                incident_num_in_cluster.append(np.sum(cluster_evaluation_df.\
                                               iloc[:, start_col_num] == cur_cluster_id))
            else:
                cluster_size_record.append(0)
                incident_num_in_cluster.append(0)
            start_col_num = start_col_num + 1
            
        plt.plot_date([datetime.datetime.strptime(x, '%Y-%m-%d') for x in \
                       birch_clustering_records.\
                           columns[np.where(birch_clustering_records.columns == issue_earliest_date)[0][0]:]], 
                      cluster_size_record, 
                      ls = 'solid', lw = 1.5, fillstyle = 'none', 
                      markersize = 0,
                      label = 'Number of Incidents in Top Cluster')
        
        plt.plot_date([datetime.datetime.strptime(x, '%Y-%m-%d') for x in \
                       birch_clustering_records.\
                           columns[np.where(birch_clustering_records.columns == issue_earliest_date)[0][0]:]], 
                      incident_num_in_cluster, 
                      ls = 'solid', lw = 1.5, fillstyle = 'none', 
                      markersize = 0,
                      label = 'Number of Incidents Belong to Current Issue and not_issue in Cluster')
    
    plt.plot_date(issue_incidents.loc[issue_incidents['Cumulative_Count'] == 1, 
                                      'EARLIEST_RECORD_DATE'], 
                  0, 'g*', markersize = 20, label = 'First Issue Incident date')
    plt.plot_date(issue_incidents.loc[issue_incidents['Cumulative_Count'] == 1, 
                                      'EARLIEST_RECORD_DATE'] + datetime.timedelta(days=100), 
                  0, 'b*', markersize = 20, label = 'Expected Current Detection date')
    plt.xlabel('Date')
    plt.legend()
    plt.ylabel('Number of Incidents')
    plt.show()
    
    if detection_date is not None:
        return (issue_incidents.loc[issue_incidents['Cumulative_Count'] == 1, 
                                      'EARLIEST_RECORD_DATE'].iloc[0] + datetime.timedelta(days=100) - detection_date).days



# This function simulates the reviewing strategy based on the threshold on the size of the cluster
def clustering_review_simulation(birch_clustering_records,
                                 transformed_table,
                                 clustering_time_stamps,
                                 top_issues,
                                 incident_detection_size = 5,
                                 revisit_size = 15, 
                                 verbose = True):
    # initialization
    baseline_review_number = []
    fully_review_number = []
    cluster_reviewed = {}
    if revisit_size > incident_detection_size:
        cluster_revisited = set()
    elif verbose:
        print('Not revisiting clusters')
    detected_issues = {}
    issues_detection_date = {}
    
    # build table
    online_tracker_w_cluster_info = birch_clustering_records.\
        merge(transformed_table.loc[:, ['ID', 'INCDT_ISSUE_LABEL', 'EARLIEST_INDICATION_DATE']])
    
    # initialize the result for first window
    initial_clustering_result = online_tracker_w_cluster_info.\
        loc[online_tracker_w_cluster_info[clustering_time_stamps[0]] >= 0, 
            ['ID', 'INCDT_ISSUE_LABEL', clustering_time_stamps[0]]]
    initial_clustering_result_modi = initial_clustering_result.copy()
    initial_clustering_result_modi['Focused_Issues'] = initial_clustering_result_modi.INCDT_ISSUE_LABEL.\
        apply(lambda x: x if x in top_issues else 'to_be_reviewed')

    baseline_review_number.append(initial_clustering_result_modi.shape[0])
    
    # build the set of reviewed clusters
    selected_cluster_info = initial_clustering_result_modi.groupby(clustering_time_stamps[0]).count()['ID'].\
            reset_index().query('ID > @incident_detection_size')
    selected_cluster_info.columns = ['Cluster_ID', 'Cluster_Size']
    
    for cur_cluster_id in selected_cluster_info.Cluster_ID:
        cluster_reviewed[cur_cluster_id] = [(clustering_time_stamps[0],
            selected_cluster_info.\
            loc[selected_cluster_info.Cluster_ID == cur_cluster_id, 
            'Cluster_Size'].values[0])]
            
    if revisit_size > incident_detection_size:
        cluster_revisited = cluster_revisited.union(
            set(initial_clustering_result_modi.groupby(clustering_time_stamps[0]).count()['ID'].\
                reset_index().query('ID > @revisit_size').index))
    
    
    fully_review_number.append(initial_clustering_result_modi[clustering_time_stamps[0]].isin(cluster_reviewed.keys()).sum())
    
    # review each cluster
    for cur_cluster in cluster_reviewed.keys():
        tmp_result = initial_clustering_result_modi.\
            loc[(initial_clustering_result_modi.INCDT_ISSUE_LABEL.isin(top_issues)) & 
                (initial_clustering_result_modi[clustering_time_stamps[0]] == cur_cluster), 
               :]
        if tmp_result.shape[0] > 0:
            if tmp_result.INCDT_ISSUE_LABEL.nunique() == 1:
                cur_issue = tmp_result.INCDT_ISSUE_LABEL.iloc[0]
                if cur_issue in detected_issues.keys():
                    detected_issues[cur_issue].append(cur_cluster)
                else:
                    detected_issues[cur_issue] = [cur_cluster]
                if cur_issue not in issues_detection_date.keys():
                    issues_detection_date[cur_issue] = clustering_time_stamps[0]
            elif tmp_result.INCDT_ISSUE_LABEL.nunique() >= 2:
                if 'multiple_top_issues' in detected_issues.keys():
                    detected_issues['multiple_top_issues'] = detected_issues['multiple_top_issues'] + 1
                else:
                    detected_issues['multiple_top_issues'] = 1
        else:
            if 'to_be_reviewed' in detected_issues.keys():
                detected_issues['to_be_reviewed'] = detected_issues['to_be_reviewed'] + 1
            else:
                detected_issues['to_be_reviewed'] = 1
    
    prev_clustering_result = initial_clustering_result.copy()
    
    for cur_date in tqdm(clustering_time_stamps[1:]):
        if verbose:
            print('reviewing ' + cur_date)
        # get clustering result at this time
        current_clustering_result = online_tracker_w_cluster_info.\
            loc[online_tracker_w_cluster_info[cur_date] >= 0, :].copy()
        # define focused issues
        current_clustering_result['Focused_Issues'] = current_clustering_result.INCDT_ISSUE_LABEL.\
            apply(lambda x: x if x in top_issues else 'to_be_reviewed')

        # number of new incidents
        baseline_review_number.append(current_clustering_result.shape[0] - prev_clustering_result.shape[0])
        # filter out reviewed clusters
        if verbose:
            print('Number of new incidents is ' + str(current_clustering_result.shape[0] - prev_clustering_result.shape[0]))
        current_clustering_result_filtered = current_clustering_result.\
            loc[~current_clustering_result[cur_date].isin(cluster_reviewed.keys()), :].copy()
        #if verbose and current_clustering_result_filtered.shape[0] > 0:
        #    print('Number of incidents belong to unreviewed cluster is ' + str(current_clustering_result_filtered.shape[0]))
        # get clusters to review
        selected_clusters = set(current_clustering_result_filtered.groupby(cur_date).count()['ID'].\
            reset_index().query('ID > @incident_detection_size')[cur_date])
        if revisit_size > incident_detection_size:
            revisit_clusters = set(current_clustering_result.\
                                   loc[~current_clustering_result[cur_date].isin(cluster_revisited), :].\
                                   groupby(cur_date).count()['ID'].\
                                   reset_index().query('ID > @revisit_size')[cur_date])
        #if verbose:
            #print('Number of new cluster to be reviewed is ' + str(len(selected_clusters)))
            #print(selected_clusters)
            #print('Reviewed Clusters')
            #print(cluster_reviewed)

        current_clustering_result_filtered = current_clustering_result_filtered.\
            loc[current_clustering_result_filtered[cur_date].isin(selected_clusters), :].copy()
        if verbose and current_clustering_result_filtered.shape[0] > 0:
            print('Number of incidents left in new cluster is ' + str(current_clustering_result_filtered.shape[0]))

        

        if revisit_size > incident_detection_size:
            cur_selected_clusters = selected_clusters.union(revisit_clusters)
        else:
            cur_selected_clusters = selected_clusters

        for cur_cluster in cur_selected_clusters:
            tmp_result = current_clustering_result.\
                loc[(current_clustering_result.INCDT_ISSUE_LABEL.isin(top_issues)) & 
                    (current_clustering_result[cur_date] == cur_cluster), 
                   :]
            if tmp_result.shape[0] > 0:
                if tmp_result.INCDT_ISSUE_LABEL.nunique() == 1:
                    cur_issue = tmp_result.INCDT_ISSUE_LABEL.iloc[0]
                    if cur_issue in detected_issues.keys():
                        detected_issues[cur_issue].append(cur_cluster)
                    else:
                        detected_issues[cur_issue] = [cur_cluster]
                    if cur_issue not in issues_detection_date.keys():
                        issues_detection_date[cur_issue] = cur_date
                elif tmp_result.INCDT_ISSUE_LABEL.nunique() >= 2:
                    if 'multiple_top_issues' in detected_issues.keys():
                        detected_issues['multiple_top_issues'] = detected_issues['multiple_top_issues'] + 1
                    else:
                        detected_issues['multiple_top_issues'] = 1
                    for tmp_issue in tmp_result.INCDT_ISSUE_LABEL.unique():
                        if tmp_issue not in issues_detection_date.keys():
                            issues_detection_date[tmp_issue] = cur_date
            else:
                if 'to_be_reviewed' in detected_issues.keys():
                    detected_issues['to_be_reviewed'] = detected_issues['to_be_reviewed'] + 1
                else:
                    detected_issues['to_be_reviewed'] = 1
                    
        # summary for this window
        prev_clustering_result = current_clustering_result.copy()
        tmp_review_incdt_count = 0
        for cur_cluster in cur_selected_clusters:
            if cur_cluster in cluster_reviewed.keys():
                if verbose:
                    print('Rechecking Cluster ' + str(cur_cluster))
                cluster_reviewed[cur_cluster].append((cur_date, (current_clustering_result[cur_date] == cur_cluster).sum()))
                tmp_len = len(cluster_reviewed[cur_cluster])
                tmp_review_incdt_count = tmp_review_incdt_count + \
                    cluster_reviewed[cur_cluster][tmp_len - 1][1] - \
                    cluster_reviewed[cur_cluster][tmp_len - 2][1]
            else:
                cluster_reviewed[cur_cluster] = [(cur_date, (current_clustering_result[cur_date] == cur_cluster).sum())]
                tmp_review_incdt_count = tmp_review_incdt_count + (current_clustering_result[cur_date] == cur_cluster).sum()

        # number of new incidents in new cluster
        fully_review_number.append(tmp_review_incdt_count)

        if revisit_size > incident_detection_size:
            cluster_revisited = cluster_revisited.union(revisit_clusters)
    
    # build return dataframe
    detection_date_df = pd.DataFrame.from_dict(issues_detection_date, 'index').reset_index()
    detection_date_df.columns = ['INCDT_ISSUE_LABEL', 'Birch_Detection_Date']
    detection_date_df = online_tracker_w_cluster_info.\
        query('INCDT_ISSUE_LABEL in @top_issues').sort_values('EARLIEST_INDICATION_DATE').\
        groupby('INCDT_ISSUE_LABEL').first()['EARLIEST_INDICATION_DATE'].reset_index().\
        merge(detection_date_df).\
        merge(online_tracker_w_cluster_info.query('INCDT_ISSUE_LABEL in @top_issues').\
              groupby('INCDT_ISSUE_LABEL').count()['ID'].reset_index().rename(columns = {'ID':'Number_of_Incidents'}))
    detection_date_df['Average_Detection_Date'] = detection_date_df.EARLIEST_INDICATION_DATE.\
        apply(lambda x: x + datetime.timedelta(days=100))
    detection_date_df['Days_Detected_Early'] = [(a_dt - datetime.datetime.strptime(d_dt, '%Y-%m-%d')).days 
                                                for d_dt, a_dt in zip(detection_date_df.Birch_Detection_Date, 
                                                                      detection_date_df.Average_Detection_Date)]
    
    return baseline_review_number, fully_review_number, cluster_reviewed, detected_issues, detection_date_df


