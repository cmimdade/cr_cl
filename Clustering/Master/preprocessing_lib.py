#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 08:41:29 2018

@author: pg875
"""
# %% Librairies
import os
import numpy as np
import pandas as pd
import pyarrow.parquet as pq
from collections import Counter
from tqdm import tqdm
import datetime
import cluster_cf as cf
import clustering_lib as clustlib


# %% Functions
# Takes as argument the location of the parquet files (string)
# return the combined dataframe
def read_online_table(online_table_location, cols_to_load):
    print('reading from ' + online_table_location)
    result_df = None
    for file_name in os.listdir(online_table_location):
        if file_name[:8] == 'part-000':
            if result_df is None:
                result_df = pq.read_table(
                        online_table_location + file_name,
                        columns=cols_to_load).to_pandas()
            else:
                tmp_file = pq.read_table(
                        online_table_location + file_name,
                        columns=cols_to_load).to_pandas()
                result_df = pd.concat([result_df, tmp_file])

    return result_df
# Takes as arguments a dataframe and a column name that
# corresponds to a column that contains lists
# Returns the dataframe with empty lists instead of None
# for this particular column


def replace_null(df,  # Dataframe
                 col_name):     # Column name that corresponds to a
                                # column that contains lists (string)
    df.loc[:, col_name] = \
        [np.array([], dtype='object') if x else v
         for x, v in zip(df[col_name].isnull(), df[col_name])]
    return(df)

# Takes as argument a list of lists l
# Returns a list containing all elements in all the lists in l


def concatenate(l):
    out = []
    for x in l:
        for y in x:
            out.append(y)
    return(out)

# Takes as argument a dataframe, a column name that corresponds to a column
# that contains lists, and a number q between 0 and 1 that determines threshold
# of occurences for most common issues
# Returns a list of the most common instances in the lists in that column


def get_top_occurences(df,  # Dataframe
                       col_name,  # Column name that corresponds
                                  # to a column that contains lists (string)
                       quantile=0.8,
                       fixed_n=None):
    if df[col_name].apply(lambda x: isinstance(x, str)).sum() == 0:
        col_count = Counter(concatenate(df[col_name])).most_common()
        # gets the lists of all fail codes with occurences

        col_count_df = pd.DataFrame(
            # count the occurences of each instance
            {'Instance': [x[0] for x in col_count],
             'Occurence': [x[1] for x in col_count]
             })
    else:
        col_count = Counter(df[col_name]).most_common()
        col_count_df = pd.DataFrame(col_count)
        col_count_df.columns = ['Instance', 'Occurence']

    if fixed_n is not None and col_count_df.shape[0] >= fixed_n:
        instance_list = col_count_df. \
            sort_values('Occurence', ascending=False)[:fixed_n]['Instance']

    else:
        threshold = col_count_df['Occurence'].quantile(quantile)
        # compute the value that corresponds to the chosen threshold
        threshold = threshold
        instance_list = col_count_df.query('Occurence>@threshold')['Instance']
        # filter most common instances
    return(instance_list)

# Takes as argument a dataframe, a column name that corresponds to a column
# that contains lists, an list of instances that we want to binarize,
# an instance name to give the corresponding columns in the new dataframe
# a string value of wheter we want to take the entire value in the columns
# or just a subset n_subset first or n_subset last charachters
# where n_subset in a integer > 0
# Returns the modified dataframe with the new binary features
# and the list of corresponding column names


def dummification(df,  # dataframe
                  col_name,  # Column name of a column that contains lists
                  instance_list,  # list of instances that we want to binarize
                  instance_name,  # string of the name of each binary feature
                  subset='complete',  # takes 3 values:
                                      # 'complete', 'first' and 'last',
                                      # or wheter we consider the entire string
                                      # in the lists or just a part of it
                  n_subset=2,
                  # if subset is different than 'complete',
                  # how much characters should we consider
                  add_unknown=False):  # whether to add unknown as a column
    instance_col_names = []
    if subset == 'complete':
        for instance in tqdm(instance_list):
            df[instance_name + str(instance)] = [1 if instance in x else 0
                                                 for x in df[col_name]]
            instance_col_names.extend([instance_name + str(instance)])

    elif subset == 'first':
        for instance in tqdm(instance_list):
            df[instance_name + str(instance)] = [1 if instance in x else 0
                                                 for x in df[col_name].
                                                 apply(lambda x: [y[:n_subset]
                                                                  for y in x])]
            instance_col_names.extend([instance_name + str(instance)])
    elif subset == 'last':
        for instance in tqdm(instance_list):
            df[instance_name + str(instance)] = [1 if instance in x else 0
                                                 for x in df[col_name].
                                                 apply(lambda x: [y[n_subset:]
                                                                  for y in x])]
            instance_col_names.extend([instance_name + str(instance)])

    elif subset == 'specific_category':
        for instance in tqdm(instance_list):
            df[instance_name + str(instance)] = [1 if instance ==
                                                 x[x.find('X'):
                                                     (x.find('X') + 2)]
                                                 else 0 for x in df[col_name]]
            instance_col_names.extend([instance_name + str(instance)])

    elif subset == 'specific_year':
        for instance in tqdm(instance_list):
            df[instance_name + str(instance)] = [1 if instance ==
                                                 x[x.find(instance):
                                                     (x.find(instance) + 4)]
                                                 else 0 for x in df[col_name]]
            instance_col_names.extend([instance_name + str(instance)])

    if add_unknown:
        if isinstance(df[col_name].iloc[0], (list, tuple, np.ndarray)):
            df[instance_name + 'Unknown_in_df'] = [1 if len(x) == 0 else 0
                                                   for x in df[col_name]]
            instance_col_names.extend([instance_name + 'Unknown_in_df'])
        else:
            df[instance_name + 'Unknown_in_df'] = [1 if x == 'Unknown' else 0
                                                   for x in df[col_name]]
            instance_col_names.extend([instance_name + 'Unknown_in_df'])
    return((df, instance_col_names))

# Takes as argument a dataframe and column name that corresponds to a column
# that contains lists
# Converts the entries in the lists in this column to a string


def to_string(df,  # dataframe
              col_name):    # column name that corresponds
                            # to a column that contains lists (string)
    df[col_name] = df[col_name].apply(lambda x: list(map(str, x)))
    return(df)

# Takes as argument a dataframe and a column name that contains numeric values
# Return the dataframe with a new column where the input column missing
# (or abnormal) values were imputed with the mean


def impute(df,  # dataframe
           col_name):  # column name with numeric values
    col_mean = df[col_name].mean()
    col_name_imputed = col_name + '_IMPUTED'
    df[col_name_imputed] = df[col_name].\
        apply(lambda x: float(x)
              if not(pd.isnull(x)) else col_mean)
    col_treshold = df[col_name_imputed].quantile(.99)
    df.loc[df[col_name_imputed] > col_treshold, col_name_imputed] = col_mean
    return((df, col_name_imputed))

# Takes as argument a dataframe, a column name that contains numeric values,
# and 3 booleans (optional) of whether the user wants to take the logarithm,
# scaling mean_variance and scale between 0 and 1


def normalize(df,  # dataframe
              col_name,  # column name that contains only numeric values
              log_normalize=True,  # whether it takes the log of the values
              mean_var_scaling=True,  # whether it normalizes with its mean/var
              zero_one_scaling=True):  # whether it is scaled between 0 and 1
    col_name_normalized = col_name + '_NORMALIZED'
    df[col_name_normalized] = df[col_name]
    if log_normalize:
        df[col_name_normalized] = [np.nan if pd.isnull(m)
                                   else 0 if m == 0 else np.log(abs(m))
                                   for m in df[col_name]]
        df[col_name_normalized].fillna(df[col_name_normalized].mean(),
                                       inplace=True)
    if mean_var_scaling:
        df[col_name_normalized] = (df[col_name_normalized] -
                                   df[col_name_normalized].mean()) / \
            df[col_name_normalized].var()**0.5
    if zero_one_scaling:
        df[col_name_normalized] = (df[col_name_normalized] -
                                   df[col_name_normalized].min()) / \
                                  (df[col_name_normalized].max() -
                                   df[col_name_normalized].min())
    return((df, col_name_normalized))


def get_fail_code_dict(df):
    fail_code_dict = {}
    for cur_list in df.REL_CMP_FAIL_CODE_LIST:
        for cur_fail in np.unique(cur_list):
            if cur_fail in fail_code_dict.keys():
                fail_code_dict[cur_fail] = fail_code_dict[cur_fail] + 1
            else:
                fail_code_dict[cur_fail] = 1
    return(fail_code_dict)


def filter_list(component_data_filtered,
                component_names):
    return(component_data_filtered.loc[component_data_filtered.
                                       component == component_names, :])


def case_id_filter_list(component_data_filtered,
                        case_id_name):
    return(component_data_filtered.loc[component_data_filtered.
                                       case_id == case_id_name, :])


def get_predictive_components(component_data_filtered,
                              count_threshold=100,
                              acc_threshold=0.7):
    component_counts = component_data_filtered.component.value_counts()
    all_acc = {}
    all_fail = {}
    for i in range(np.sum(component_counts > count_threshold)):
        cur_component = component_counts.index[i]
        tmp_dict = get_fail_code_dict(filter_list(component_data_filtered,
                                                  cur_component))
        top_acc = np.max([v / component_counts[i]
                          for (k, v) in tmp_dict.items()])
        top_fail = [k for (k, v) in tmp_dict.items()
                    if v / component_counts[i] >= top_acc]
        all_acc[cur_component] = top_acc
        all_fail[cur_component] = top_fail[0]

    predictive_components = list({k: v for (k, v) in all_acc.items()
                                  if v > acc_threshold}.keys())
    fail_imput_dict = {k: v for (k, v) in all_fail.items()
                       if k in predictive_components}
    return(fail_imput_dict)


def bucket(df,
           col_name,
           n_buckets=5):
    thr = 0
    quantiles = []
    buckets_col_names = []
    for i in range(n_buckets):
        quantiles.append(df[col_name].quantile(thr))
        thr += 1/n_buckets
    for j in tqdm(range(n_buckets)):
        col_name_bucket = col_name + '_' + str(j+1)
        buckets_col_names.append(col_name_bucket)
        if j != n_buckets-1:
            df[col_name_bucket] = df[col_name].apply(
                    lambda x: 0 if pd.isnull(x)
                    else int(quantiles[j] <= x
                             and x < quantiles[j+1]))
        else:
            df[col_name_bucket] = df[col_name].apply(
                    lambda x: 0 if pd.isnull(x)
                    else int(quantiles[j] <= x))
    return(df,
           buckets_col_names)

# %% Wrap-up
# Takes as an argument a dataframe, a feature dictionary
# {Feature: 'method_to_apply_to_this_feature'}
# and (optional) whether or not statements are printed

# Returns the cleaned dataframe with the new columns that will be used
# as features and the list of the column names features
# as well as the corresponding ranges for these columns
# (if they belong to the same "category")


def preprocessing(raw_df,  # raw data frame - online table or a subset of it
                  feature_dict,  # a feature dictionary in the form of
                                 # {Feature: 'method_to_apply_to_this_feature'}
                  add_unknown=False,
                  # whether to add unknown category in dummification
                  verbose=True,
                  # whether or not the function prints statements
                  impute_fault=True,
                  impute_fail=False,
                  component_df=None,
                  impute_fail_threshold=0.7):
    # parameters
    fail_date_col_name = 'REL_CMP_FAIL_DATE_LIST'
    claim_date_col_name = 'REL_CMP_CLAIM_DATE_LIST'
    dsid_create_date_col_name = 'DSID_CREATE_DATE'
    miles_col_name = 'REL_CMP_ENGINE_MILES_LIST'

    # Output table
    df = raw_df.copy()

    # Impute related string/list columns

    if impute_fail:

        if component_df is None:
            print('Please provide a component dataframe')

        else:
            if verbose:
                print('Imputing Fail Code from component')

            component_data_filtered = component_df.\
                loc[(component_df.REL_CMP_FAIL_CODE_LIST.
                     apply(lambda x: x is not None)) &
                    (component_df.component.apply(lambda x: x is not None)), :]

            fail_imput_dict = get_predictive_components(
                component_data_filtered=component_data_filtered,
                acc_threshold=impute_fail_threshold)
            df = df.\
                merge(component_data_filtered.loc[:, ['ID', 'component']].
                      groupby('ID').first().reset_index(),
                      on='ID', how='left')

            imputation_filter = (df['REL_CMP_FAIL_CODE_LIST'].
                                 apply(lambda x: x is None) & df['component'].
                                 apply(lambda x: x in fail_imput_dict))

            df.loc[imputation_filter, 'REL_CMP_FAIL_CODE_LIST'] = \
                df.loc[imputation_filter, 'component'].\
                apply(lambda x: [fail_imput_dict[x]])

    if verbose:
        print('Start main table preprocessing')
        print('Number of features to create is ' + str(len(feature_dict)))

    insite_notnull = df['INS_FI_FAULT_CODE_LST'].\
        apply(lambda x: x is not None).mean() != 0
    non_greenwrench_notnull = \
        df['NON_GREEN_WRENCH_FAULT_CODE_LIST'].\
        apply(lambda x: x is not None).mean() != 0

    list_columns_to_fill_null = \
        ['REL_CMP_FAIL_CODE_LIST', 'REL_CMP_FAIL_DATE_LIST',
         'REL_CMP_CLAIM_DATE_LIST', 'GREEN_WRENCH_FAULT_CODE_LIST',
         'REL_CMP_ENGINE_MILES_LIST', 'REL_ANALYSIS_RATE_CAT',
         'GREEN_WRENCH_NUM_LIST']

    if insite_notnull:
        list_columns_to_fill_null.append('INS_FI_FAULT_CODE_LST')

    if non_greenwrench_notnull:
        list_columns_to_fill_null.append('NON_GREEN_WRENCH_FAULT_CODE_LIST')

    for col_name, approach in feature_dict.items():
        list_columns_to_fill_null.append(col_name)
    # remove duplicates
    list_columns_to_fill_null = list(set(list_columns_to_fill_null))

    for col_name in list_columns_to_fill_null:
        if raw_df[col_name].dtype == np.object:
            if raw_df[col_name].apply(lambda x: isinstance(x, str)).sum() == 0:
                if verbose:
                    print('Replaced Null in list type column ' + col_name)
                df = replace_null(df=df, col_name=col_name)
            else:
                if verbose:
                    print('Replaced Null in string type column ' +
                          col_name + ' to Unknown')
                df[col_name].fillna('Unknown', inplace=True)

    list_columns_to_string = ['GREEN_WRENCH_FAULT_CODE_LIST']
    for col_name in list_columns_to_string:
        df = to_string(df=df, col_name=col_name)

    if impute_fault:
        if non_greenwrench_notnull:
            df['GREEN_WRENCH_FAULT_CODE_LIST'].\
                loc[df['GREEN_WRENCH_FAULT_CODE_LIST'].
                    apply(lambda x: len(x) == 0)] \
                = df['NON_GREEN_WRENCH_FAULT_CODE_LIST'].\
                loc[df['GREEN_WRENCH_FAULT_CODE_LIST'].
                    apply(lambda x: len(x) == 0)].\
                apply(lambda x: [x[0]] if len(x) > 0 else [])
        if insite_notnull:
            df['GREEN_WRENCH_FAULT_CODE_LIST'].\
                loc[df['GREEN_WRENCH_FAULT_CODE_LIST'].
                    apply(lambda x: len(x) == 0)] \
                = df['INS_FI_FAULT_CODE_LST'].\
                loc[df['GREEN_WRENCH_FAULT_CODE_LIST'].
                    apply(lambda x: len(x) == 0)].\
                apply(lambda x: [x[0]] if len(x) > 0 else [])

    # Build some helper columns
    earliest_date_col_name = 'EARLIEST_RECORD_DATE'
    df['MISSING'] = df['DSID'].isnull()
    df.query('MISSING == True')[fail_date_col_name].\
        apply(lambda x: len(set(x)) <= 1).mean()
    df[earliest_date_col_name] = df[claim_date_col_name].\
        apply(lambda x: min(x) if len(x) > 0 else None)
    df[earliest_date_col_name] = df[earliest_date_col_name].\
        apply(lambda x: datetime.datetime.strptime(str(x), '%Y-%m-%d')
              if x is not None else None)

    df[earliest_date_col_name] = df[earliest_date_col_name].\
        where(df[earliest_date_col_name].notnull(),
              df[dsid_create_date_col_name])

    last_mile_col_name = 'LAST_MILE_RECORDED'
    df[last_mile_col_name] = \
        df[miles_col_name].apply(lambda x: x[-1] if len(x) > 0 else None)

    last_time_col_name = 'LAST_MILE_RECORD_TIME'
    df[last_time_col_name] = \
        df[fail_date_col_name].apply(lambda x: datetime.datetime.
                                     strptime(x[-1], '%Y-%m-%d')
                                     if len(x) > 0 else None)

    df['REL_CMP_ENGINE_MILES_LIST'].loc[df['REL_CMP_ENGINE_MILES_LIST'].
                                        apply(lambda x: len(x) == 0)] = \
        df['INS_TI_ENGINE_DISTANCE_MILES'].\
        loc[df['REL_CMP_ENGINE_MILES_LIST']
            .apply(lambda x: len(x) == 0)]. \
        apply(lambda x: [x] if ~np.isnan(x) else [])

    # Start processing the feature dictionary
    if verbose:
        print('Start Processing Features')

    clustering_features, clustering_ranges = [], []
    for col_name, approach in feature_dict.items():
        if approach == 'specific':
            if col_name == 'REL_CMP_FAIL_DATE_LIST':
                if verbose:
                    print('Create Log Normalized Age Feature')

                engine_age_col_name = 'ENGINE_AGE'
                df[engine_age_col_name] = \
                    (df[last_time_col_name] -
                     df[earliest_date_col_name]).apply(lambda x: x.days)
                df, age_imputed_col_name = impute(
                    df=df, col_name=engine_age_col_name)
                df, age_normalized_col_name = normalize(
                    df=df, col_name=age_imputed_col_name)

                clustering_features.append(age_normalized_col_name)
                clustering_ranges.append(1)
                clustering_ranges
            elif col_name == 'REL_CMP_ENGINE_MILES_LIST':
                if verbose:
                    print('Create Log Miles Feature')
                df, miles_imputed_col_name = impute(
                    df=df, col_name=last_mile_col_name)
                df, miles_normalized_col_name = normalize(
                    df=df, col_name=miles_imputed_col_name)
                clustering_features.append(miles_normalized_col_name)
                clustering_ranges.append(1)

            elif col_name == 'REL_ANALYSIS_RATE_CAT':
                if verbose:
                    print('Create Engine Category Feature')
                engine_category_list = ['X1', 'X2', 'X3']
                df, engine_category_col_names = \
                    dummification(df=df,
                                  col_name=col_name,
                                  instance_list=engine_category_list,
                                  instance_name=col_name + '_',
                                  subset='specific_category',
                                  add_unknown=add_unknown)
                clustering_features.extend(engine_category_col_names)
                clustering_ranges.append(len(engine_category_col_names))

                engine_category_list = ['2013', '2017']
                df, engine_category_col_names = \
                    dummification(df=df,
                                  col_name=col_name,
                                  instance_list=engine_category_list,
                                  instance_name=col_name + '_',
                                  subset='specific_year',
                                  add_unknown=add_unknown)
                clustering_features.extend(engine_category_col_names)
                clustering_ranges.append(len(engine_category_col_names))

            else:
                print(col_name + ' feature needs to further code')

        elif approach == 'impute_take_log':
            if verbose:
                print('Imput and take log on ' + col_name)
            df, imputed_col_name = impute(df=df, col_name=col_name)
            df, normalized_col_name = normalize(
                df=df, col_name=imputed_col_name)
            clustering_features.append(normalized_col_name)
            clustering_ranges.append(1)
            if verbose:
                print(
                    'Column name of the new feature is ' +
                    normalized_col_name)

        elif approach == 'impute_bucket':
            if verbose:
                print('Imput ' + col_name)
            df, imputed_col_name = impute(df=df, col_name=col_name)
            df, buckets_col_names = bucket(
                df=df, col_name=imputed_col_name)
            clustering_features.extend(buckets_col_names)
            clustering_ranges.append(len(buckets_col_names))

        elif (isinstance(approach, float) or
              isinstance(approach, int)) and approach >= 0:

            if approach <= 1:

                if verbose:
                    print(
                        'Dummify feature ' +
                        col_name +
                        ' using quantile ' +
                        str(approach))

                category_list = get_top_occurences(df=df,
                                                   col_name=col_name,
                                                   quantile=approach)

            else:

                if verbose:
                    print(
                        'Dummify feature ' +
                        col_name +
                        ' using ' +
                        str(approach) +
                        ' variables')

                category_list = get_top_occurences(df=df,
                                                   col_name=col_name,
                                                   fixed_n=approach)

            df, dummy_col_names = dummification(df=df,
                                                col_name=col_name,
                                                instance_list=category_list,
                                                instance_name=col_name + '_',
                                                add_unknown=add_unknown)
            clustering_features.extend(dummy_col_names)
            clustering_ranges.append(len(dummy_col_names))

            # Special case for fail code
            if col_name == 'REL_CMP_FAIL_CODE_LIST':
                if verbose:
                    print('Create extra features on fail code')
                fail_code_first_2_list = list(
                    set(category_list.apply(lambda x: x[:2])))
                df, fail_code_first_2_col_names = \
                    dummification(df=df,
                                  col_name=col_name,
                                  instance_list=fail_code_first_2_list,
                                  instance_name='FAIL_CODE_PRE_',
                                  subset='first',
                                  n_subset=2,
                                  add_unknown=add_unknown)
                clustering_features.extend(fail_code_first_2_col_names)
                clustering_ranges.append(len(fail_code_first_2_col_names))

        elif isinstance(approach, list):
            if verbose:
                print('Dummify feature ' + col_name + ' using predefined list')

            category_list = pd.DataFrame(approach)[0]

            df, dummy_col_names = dummification(df=df,
                                                col_name=col_name,
                                                instance_list=category_list,
                                                instance_name=col_name + '_',
                                                add_unknown=add_unknown)
            clustering_features.extend(dummy_col_names)
            clustering_ranges.append(len(dummy_col_names))

            # Special case for fail code
            if col_name == 'REL_CMP_FAIL_CODE_LIST':
                if verbose:
                    print('Create extra features on fail code')
                fail_code_first_2_list = list(
                    set(category_list.apply(lambda x: x[:2])))
                df, fail_code_first_2_col_names = \
                    dummification(df=df,
                                  col_name=col_name,
                                  instance_list=fail_code_first_2_list,
                                  instance_name='FAIL_CODE_PRE_',
                                  subset='first',
                                  n_subset=2,
                                  add_unknown=add_unknown)
                clustering_features.extend(fail_code_first_2_col_names)
                clustering_ranges.append(len(fail_code_first_2_col_names))

    return((df, clustering_features, clustering_ranges))

# %% functions to help load and pre-process data


def main_load_data(online_table_location,
                   component_table_location,
                   feature_dict,
                   col_names,
                   run_offline=False,
                   save_data=True,
                   folder=cf.folder,
                   data_start_date=cf.data_start_date,
                   data_end_date=cf.data_end_date,
                   filter_engines=cf.filter_engines):
    # Import data
    print('reading parquet files')
    online_tracker_ori = read_online_table(online_table_location,
                                           cols_to_load=col_names)
    if run_offline:
        # Labeled part of the online table
        incident_tracker_df = \
            online_tracker_ori.loc[online_tracker_ori['INCDT_ISSUE_LABEL'].
                                   apply(lambda x: x is not None), :]
        incident_tracker_df_filtered = incident_tracker_df.\
            loc[(incident_tracker_df.REL_CMP_PROGRAM_GROUP_NAME.isnull()) |
                (incident_tracker_df.REL_CMP_PROGRAM_GROUP_NAME !=
                 'CAMP/TRP'), :]
        tracker_df = incident_tracker_df_filtered
    else:

        online_tracker_ori_filtered = online_tracker_ori.\
            loc[(online_tracker_ori.REL_CMP_PROGRAM_GROUP_NAME.isnull()) |
                (online_tracker_ori.REL_CMP_PROGRAM_GROUP_NAME !=
                 'CAMP/TRP'), :]

        tracker_df = online_tracker_ori_filtered

    # Component data
    component_data_ori = read_online_table(component_table_location,
                                           cols_to_load=None)

    # filter 2017 engines
    if filter_engines != 'ALL':
        tracker_df = tracker_df[tracker_df[
                'REL_ANALYSIS_RATE_CAT'].apply(
                lambda x: x.find(filter_engines) >= 0)]

    # Preprocessed offline table
    print('preprocessing tables')
    transformed_table, clustering_features, clustering_ranges = \
        preprocessing(raw_df=tracker_df,
                      feature_dict=feature_dict,
                      verbose=False,
                      impute_fault=True,
                      impute_fail=True,
                      component_df=component_data_ori,
                      impute_fail_threshold=0.75)

    # Filter by missing fail codes
    transformed_table = \
        transformed_table[transformed_table['REL_CMP_FAIL_CODE_LIST'].
                          apply(lambda x: len(x) != 0)]

    # Filter the date range
    transformed_table = \
        transformed_table[[a and b
                           for a, b in
                           zip(transformed_table['EARLIEST_RECORD_DATE']
                               >= datetime.datetime.strptime(
                               data_start_date, '%Y-%m-%d'),
                               transformed_table['EARLIEST_RECORD_DATE']
                               < datetime.datetime.strptime(
                               data_end_date, '%Y-%m-%d'))]]

    # Matrix of features values
    data = transformed_table.loc[:, clustering_features].values

    if save_data:
        # Saving intermediate results
        print('saving clustering variables to pickle')
        clustlib.save(
            clustering_features,
            filename=folder + 'clustering_features.pckl')
        clustlib.save(
            clustering_ranges,
            filename=folder + 'clustering_ranges.pckl')

        trim_table = transformed_table.loc[:, col_names]
        clustlib.save(
            trim_table,
            filename=folder + 'trim_transformed_table.pckl')

    return(transformed_table, clustering_features, clustering_ranges, data)

# %%
