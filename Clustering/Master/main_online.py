#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 16 17:12:20 2018

@author: pg875
"""
##########################################################################
# Librairies
import os
import preprocessing_lib as plib
import clustering_lib as clustlib
import exploration_lib as explib
import warnings
import math
import numpy as np
import copy
from tqdm import tqdm
tqdm.monitor_interval = 0
os.chdir("/home/pr891/workspace/code")
warnings.simplefilter('ignore')
##########################################################################


##########################################################################
# Preprocessing
# Import data
online_table_location = \
    "/home/pr891/workspace/data/QUALITY_FEATURES_PARQUET_NEW-2/"
print('reading parquet files')
# online_table_location = \
#    "/home/pr891/workspace/data/QUALITY_FEATURES_PARQUET_NEW/"
online_tracker_ori = plib.read_online_table(online_table_location)
online_tracker_ori_filtered = online_tracker_ori.\
    loc[(online_tracker_ori.REL_CMP_PROGRAM_GROUP_NAME.isnull()) |
        (online_tracker_ori.REL_CMP_PROGRAM_GROUP_NAME != 'CAMP/TRP'), :]

# Component data
component_table_location = \
    '/home/pr891/workspace/data/component_data_v2/'
# component_table_location = '/home/pr891/workspace/data/component_data_v2/'
component_data_ori = plib.read_online_table(component_table_location)

# Preprocessed offline table
feature_dict = {'REL_CMP_FAIL_CODE_LIST': 200,  # 200
                'GREEN_WRENCH_FAULT_CODE_LIST': 200,  # 400
                'REL_OEM_NAME': 20,  # 20
                'GREEN_WRENCH_LIST': 200,  # 200
                'REL_ANALYSIS_RATE_CAT': 'specific',  # 3
                'REL_CMP_ENGINE_MILES_LIST': 'specific',
                'REL_CMP_FAIL_DATE_LIST': 'specific',
                'REL_CMP_SUM_NET_AMOUNT': 'impute_take_log',
                # 'CMP_SUM_MATERIALS_AMOUNT':'impute_take_log'
                }

print('preprocessing tables')
transformed_table, clustering_features, clustering_ranges = \
    plib.preprocessing(raw_df=online_tracker_ori_filtered,
                       feature_dict=feature_dict,
                       verbose=False,
                       impute_fault=True,
                       impute_fail=True,
                       component_df=component_data_ori,
                       impute_fail_threshold=0.75)

# Filter by missing fail codes
transformed_table = \
    transformed_table[transformed_table['REL_CMP_FAIL_CODE_LIST'].
                      apply(lambda x: len(x) != 0)]

# Matrix of features values
data = transformed_table.loc[:, clustering_features].values
##########################################################################


##########################################################################
# Clustering
# Optimization framework

# optimal_weights, n_clusters = \
#    clustlib.optimization_framework(data=data,
#                                    df=transformed_table,
#                                    clustering_ranges=clustering_ranges,
#                                    warm_start=([3.75, 2.0, 2.75, 2.0, 4.0,
#                                                 1.25, 3.75, 4.0, 4.0,
#                                                 5.0], 80),
#                                    order='robust',
#                                    # n_iterations=2*len(clustering_ranges),
#                                    n_iterations=3,
#                                    n_clusters_range=[s for s
#                                                      in range(30, 95, 5)],
#                                    weight_range=[s * 0.25 for s
#                                                  in range(25)],
#                                    kpi2_weight=-1,
#                                    kpi4_weight=0,
#                                    penalization_weight=0.75,
#                                    affinity='default',
#                                    do_plot=False)

# Moving windows framework
# Global parameters
# optimal outputs for the filtered version
optimal_weights, n_clusters = \
    [4.0, 1.75, 4.25, 2.0, 4.0, 1.5, 3.75, 5.5, 3.25, 4.75], 90

# optimal outputs for the unfiltered version
# optimal_weights, n_clusters = \
#    [3.75, 2.0, 2.75, 2.5, 4.0, 1.25, 3.75, 4.0, 4.0, 5.0], 80

# Weight data
weighted_data = copy.copy(data)
pointer = 0
for i in range(len(clustering_ranges)):
    weighted_data[:, pointer:(pointer + clustering_ranges[i])] = \
        optimal_weights[i] * weighted_data[:, pointer:
                                           (pointer + clustering_ranges[i])]
    pointer = pointer + clustering_ranges[i]

# Hyperparameters
window_size, window_step = (2, 0), (0, 15)
algorithm, n_clusters_algorithm = 'hierarchical', 'birch_variable'

# Optimal threshold for the moving windows framework
print('calculating optimal threshold')
optimal_threshold = clustlib.threshold_tuning(
    data=weighted_data,
    df=transformed_table,
    clustering_ranges=clustering_ranges,
    weights=optimal_weights,
    algorithm=n_clusters_algorithm,
    size=window_size,
    step=window_step,
    test_range=[0.25 + (s / 4) for
                s in range(39)],
    order_of_magnitude=None,
    do_print=True,
    do_plot=False,
    preweighted=True)

# Perform the clustering
print('performing moving window clustering')
clustering_results, start_window, end_window, anchor = \
    clustlib.parallel_moving_windows_framework(
        data=data,
        df=transformed_table,
        clustering_ranges=clustering_ranges,
        weights=optimal_weights,
        size=window_size,
        step=window_step,
        algorithm=algorithm,
        n_clusters_algorithm=n_clusters_algorithm,
        threshold=optimal_threshold,
        reset=True,
        do_plot=False,
        n_processes=6)

# Tracking path (range to be tracked)
tracking_path = (0, len(start_window) - 2)

print('saving clustering variables to pickle')
clustlib.save(
    clustering_features,
    filename='/home/pr891/workspace/data/output/clustering_features.pckl')
clustlib.save(
    clustering_ranges,
    filename='/home/pr891/workspace/data/output/clustering_ranges.pckl')
clustlib.save(
    optimal_weights,
    filename='/home/pr891/workspace/data/output/optimal_weights.pckl')
clustlib.save(
    end_window,
    filename='/home/pr891/workspace/data/output/end_window.pckl')
clustlib.save(
    start_window,
    filename='/home/pr891/workspace/data/output/start_window.pckl')
##########################################################################


##########################################################################
# Exploration
# Windows dictionary (optional: convert time complexity to space memory)
print('creating windows')
windows = explib.map_windows(df=transformed_table,
                             start_window=start_window,
                             end_window=end_window,
                             clustering_results=clustering_results,
                             path_range=tracking_path)

print('saving transformed table and tracking path to pickle')
clustlib.save(
    tracking_path,
    filename='/home/pr891/workspace/data/output/tracking_path.pckl')
clustlib.save(
    transformed_table,
    filename='/home/pr891/workspace/data/output/transformed_table.pckl')

# Plot KPIs
# All-in-one
# explib.moving_kpis(transformed_table,
#                    start_window,end_window,
#                    clustering_results,
#                    tracking_path,
#                    windows=windows,
#                    combine_plots=True,
#                    kpi2_threshold=0,
#                    kpi3_threshold=0,
#                    kpi4_threshold=0)

# top issues
top_issues = transformed_table['INCDT_ISSUE_LABEL'].value_counts()[2:].index

# Clustering capture ratio
# for tracked_issue in top_issues:
#     explib.clustering_capture_ratio(transformed_table,
#                                     start_window,
#                                     end_window,
#                                     clustering_results,
#                                     tracking_path,
#                                     tracked_issue,
#                                     windows,
#                                     include_not_issue=False,
#                                     cumulative=False,
#                                     alternative_cumulative=False,
#                                     date_col_name='EARLIEST_RECORD_DATE')
#
# Cumulative
# for tracked_issue in top_issues:
#     explib.clustering_capture_ratio(transformed_table,
#                                     start_window,
#                                     end_window,
#                                     clustering_results,
#                                     tracking_path,
#                                     tracked_issue,
#                                     windows,
#                                     cumulative=True,
#                                     alternative_cumulative=True)
#
# Clustering purity ratio
# for tracked_issue in top_issues:
#     explib.clustering_purity_ratio(transformed_table,
#                                    start_window,
#                                    end_window,
#                                    clustering_results,
#                                    tracking_path,
#                                    tracked_issue,
#                                    windows)
#
# Cumulative
# for tracked_issue in top_issues:
#     explib.clustering_purity_ratio(transformed_table,
#                                    start_window,
#                                    end_window,
#                                    clustering_results,
#                                    tracking_path,
#                                    tracked_issue,
#                                    windows,
#                                    plot_issue_scale=True,
#                                    cumulative=True,
#                                    alternative_cumulative=True)
##########################################################################


##########################################################################
# Universal tracking

# Perform universal clustering
print('creating universal cluster IDs')
last_id_given = clustlib.Singleton()
last_id_given.index = 0
anchor_std = math.sqrt(np.mean(np.array(
    [np.linalg.norm(np.var(list(anchor[i].values()), axis=0))
     for i in anchor.keys()])))

centroids_threshold = optimal_threshold + anchor_std
universal_ids, universal_labels, universal_anchor = \
    clustlib.parallel_universal_ids(
        windows=windows,
        anchor=anchor,
        clustering_results=clustering_results,
        last_id_given=last_id_given,
        tracking_path=tracking_path,
        tracking_type='hybrid',
        centroids_threshold=centroids_threshold,
        count_threshold=0.5,
        n_threads=6,
        universal_anchor=None,
        universal_labels=None,
        universal_ids=None,
        use_tqdm=True)

# Get new clustering_results
transformed_results = clustlib.transform_clustering_results(
    clustering_results=clustering_results,
    new_labels=universal_ids,
    tracking_path=tracking_path)

# Get new windows
print('creating universal windows')
universal_windows = explib.map_windows(df=transformed_table,
                                       start_window=start_window,
                                       end_window=end_window,
                                       clustering_results=transformed_results,
                                       path_range=tracking_path)

print('saving universal variables to pickle')
clustlib.save(
    transformed_results,
    filename='/home/pr891/workspace/data/output/transformed_results.pckl')
clustlib.save(
    universal_labels,
    filename='/home/pr891/workspace/data/output/universal_labels.pckl')
clustlib.save(
    universal_anchor,
    filename='/home/pr891/workspace/data/output/universal_anchor.pckl')
clustlib.save(
    universal_ids,
    filename='/home/pr891/workspace/data/output/universal_ids.pckl')

# Plot detection dates (defintion 1)
# for tracked_issue in top_issues:
#     _,_,_ = explib.detection_plot(transformed_table,
#                                   tracked_issue,
#                                   universal_windows,
#                                   tracking_path,
#                                   start_window,
#                                   end_window,
#                                   clustering_results,
#                                   anchor)
##########################################################################


##########################################################################
# Reviews
# Intermediate computation
# print('calculating cumulative counts')
# universal_windows, cumulative_ids = explib.cumulative_counter(
#    universal_windows,
#    universal_labels,
#    tracking_path)


universal_mean, universal_std = clustlib.get_universal_variations(
    df=transformed_table,
    universal_labels=universal_labels,
    universal_anchor=universal_anchor,
    clustering_features=clustering_features,
    clustering_ranges=clustering_ranges,
    weights=optimal_weights)

# Reviewing strategy
print('performing reviewing strategy')
n_new_incidents_per_window, n_reviewed_incidents_per_window, \
    ids_reviewed_incidents, ids_sampled_incidents, detail_review_info, \
    issue_detection_date, detection_date_df, \
    visited_ids, visited_clusters = \
    clustlib.describe_reviews(
        transformed_table,
        universal_anchor,
        universal_labels,
        clustering_features,
        clustering_ranges,
        optimal_weights,
        universal_windows,
        tracking_path,
        0.2,
        end_window=end_window,
        top_issues=top_issues,
        precomputed=True,
        universal_mean=universal_mean,
        universal_std=universal_std,
        # columns_list=list(incident_tracker_df_filtered.columns)
        # + ['CLUSTER_ID'] + ['CUM_INCDT_IN_CLUSTER'] +
        # ['EARLIEST_RECORD_DATE'],
        columns_list=list(online_tracker_ori_filtered.columns)
        + ['CLUSTER_ID'] + ['EARLIEST_RECORD_DATE'],
        filter_fail=False)
#           manual_input=[clustlib.test_monitor_oem])

# print(detection_date_df)

print('saving reviewing strategy variables to pickle')
clustlib.save(
    top_issues,
    filename='/home/pr891/workspace/data/output/top_issues.pckl')
clustlib.save(
    n_new_incidents_per_window,
    filename='/home/pr891/workspace/data/output/n_new_incidents.pckl')
clustlib.save(
    n_reviewed_incidents_per_window,
    filename='/home/pr891/workspace/data/output/n_reviewed_incidents.pckl')
clustlib.save(
    detection_date_df,
    filename='/home/pr891/workspace/data/output/detection_date_df.pckl')
clustlib.save(
    issue_detection_date,
    filename='/home/pr891/workspace/data/output/issue_detection_date.pckl')
clustlib.save(
    detail_review_info,
    filename='/home/pr891/workspace/data/output/detail_review_info.pckl')
##########################################################################


##########################################################################
# 2D T-SNE demo
# Get top 50 clusters
# top_clusters = explib.get_top_clusters(tracking_path=tracking_path,
#                                        transformed_results=transformed_results,
#                                        n=50)

# Compute the feature matrix in 2D with T-SNE
# X_transformed, transformed_table = explib.tsne_results(
#         transformed_table=transformed_table,
#         clustering_features=clustering_features,
#         clustering_ranges=clustering_ranges,
#         optimal_weights=optimal_weights)

# Compute the windows, display the demo, and save it to working file
# explib.display_2D_demo(top_clusters=top_clusters,
#                        universal_windows=universal_windows,
#                        tracking_path=tracking_path,
#                        X_transformed=X_transformed,
#                        transformed_table=transformed_table,
#                        end_window=end_window,
#                        detail_review_info=detail_review_info,
#                        frames=1,
#                        size=8,
#                        filename='demo_final_2017.html',
#                        cumulative=True,
#                        cumulative_count=False)
##########################################################################
